<?php
	session_start();
	if ($_SESSION['af_id']) {
		$return_url = "Location:./signin_af";
	} else if ($_SESSION['acm_id']) {
		$return_url = "Location:./signin_acm";
	} else {
		$return_url = "Location:../index";
	}
	// 最終的に、セッションを破壊する
	session_destroy();
	header($return_url);
    exit();
?>