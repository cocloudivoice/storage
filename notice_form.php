<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/commentcontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$comment_con = new comment_control();

//変数の宣言
$company_id = $_SESSION['user_id'];

$title = htmlspecialchars($_REQUEST['title'],ENT_QUOTES);
$comment = htmlspecialchars($_REQUEST['comment'],ENT_QUOTES);
$company_id = htmlspecialchars($_REQUEST['company_id'],ENT_QUOTES);

if (isset($title)) {
	$sql = "
	INSERT INTO `COMMENT_TABLE`(
	`comment`, `title`, `company_id`
	) VALUES (
	'".$comment."','".$title."',".$company_id."
	)";
	$comment_con -> comment_sql($pdo,$sql);
}

?>
	
<!DOCTYPE html>

<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
		<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
		<title>お知らせ入力フォーム - [Cloud Invoice]</title>
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//hikaku.jp/common/images/logo.jpg" />
		<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
		<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://storage.cloudinvoice.co.jp/" />
</head>
<body>
	<h1>お知らせ入力フォーム</h1>
	<form action="" method="post">
		<p>タイトル<br/><input name="title" type="text" /></p>
		<p>内　　容<br/><textarea name="comment" cols="40" rows="4" /></textarea></p>
		<p>あて先（全体はNULLか0）<br/><input name="company_id" type="text" value="0" /></p>
		<p><input type="submit" value="送信" /></p>
	</form>
</body>
</html>