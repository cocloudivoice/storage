<?php
session_start();

//controlクラスファイルの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/sitecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$sitedb = new site_control();

//変数
$user_id;
$site_id;
$site_update_arr = array();

//user_id,site_id
if (isset($_REQUEST['site_id'])) {
	$site_id = $site_update_arr['site_id'] = $_REQUEST['site_id'];
} else if (isset($_SESSION['site_id'])) {
	$site_id = $site_update_arr['site_id'] = $_SESSION['site_id'];
}
if (isset($_REQUEST['user_id'])) {
	$user_id = $site_update_arr['user_id']  = $_REQUEST['user_id'];
} else if (isset($_SESSION['user_id'])) {
	$user_id = $site_update_arr['user_id']  = $_SESSION['user_id'];
}

//タグのアップデートデータ取得
if (isset($_REQUEST['tag1'])) {
	$tag1 = $site_update_arr['tag1'] = htmlspecialchars($_REQUEST['tag1'],ENT_QUOTES);
	$tag2 = $site_update_arr['tag2'] = htmlspecialchars($_REQUEST['tag2'],ENT_QUOTES);
	$tag3 = $site_update_arr['tag3'] = htmlspecialchars($_REQUEST['tag3'],ENT_QUOTES);
	$tag4 = $site_update_arr['tag4'] = htmlspecialchars($_REQUEST['tag4'],ENT_QUOTES);
	$tag5 = $site_update_arr['tag5'] = htmlspecialchars($_REQUEST['tag5'],ENT_QUOTES);
	$tag6 = $site_update_arr['tag6'] = htmlspecialchars($_REQUEST['tag6'],ENT_QUOTES);
	$tag7 = $site_update_arr['tag7'] = htmlspecialchars($_REQUEST['tag7'],ENT_QUOTES);
	$tag8 = $site_update_arr['tag8'] = htmlspecialchars($_REQUEST['tag8'],ENT_QUOTES);
	$tag9 = $site_update_arr['tag9'] = htmlspecialchars($_REQUEST['tag9'],ENT_QUOTES);
	$tag10 = $site_update_arr['tag10'] = htmlspecialchars($_REQUEST['tag10'],ENT_QUOTES);
}

//var_dump($site_update_arr);
if($site_update_arr['tag1'] != NULL) {
	//タグのアップデートの処理
	$sitedb -> site_tag_update($pdo,$site_update_arr);
	//ウィンドウを閉じる処理。自動で閉じられなかった時のために閉じるボタンも生成する。
	echo '<input type="button" value="ウィンドウを閉じる" style="magin-left:auto;margin-right:auto;" onclick="window.close();" />';
	echo '<script type="text/javascript">window.close();</script>';
	exit();
} else {
	//セレクトする。
	$site_update_arr = $sitedb -> site_select($pdo,$user_id,$site_id);
	$tag1 = $site_update_arr['tag1'];
	$tag2 = $site_update_arr['tag2'];
	$tag3 = $site_update_arr['tag3'];
	$tag4 = $site_update_arr['tag4'];
	$tag5 = $site_update_arr['tag5'];
	$tag6 = $site_update_arr['tag6'];
	$tag7 = $site_update_arr['tag7'];
	$tag8 = $site_update_arr['tag8'];
	$tag9 = $site_update_arr['tag9'];
	$tag10 = $site_update_arr['tag10'];

}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $site_title_data['site_title']; ?>比較,<?php echo $site_title_data['titletag01'];echo ',';echo $site_title_data['titletag02'];echo ',';echo $site_title_data['titletag03'];echo ',';echo $site_title_data['titletag04'];echo ',';echo $site_title_data['titletag05'];echo ',';echo $tag1;echo ',';echo $tag2;echo ',';echo $tag3;echo ',';echo $tag4;echo ',';echo $tag5;echo ',';echo $tag6;echo ',';echo $tag7;echo ',';echo $tag8;echo ',';echo $tag9;echo ',';echo $tag10;echo ','; ?>,ランキング,カフェ,家電,テレビ,買い物,ショッピング,パソコン,ノートパソコン,タブレット,iphone,食品,食べ物,果物,特産品,服,インテリア,雑貨,小物,金融,証券,FX,投資,冷蔵庫,洗濯機" /> 
		<meta name="description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />		
		<title><?php echo $site_title_data['site_title']; ?>比較 - [比較.jp]</title>
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link rel="shortcut icon" href="http://hikaku.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://hikaku.jp/" />
		<script src="http://hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>
		<script src="http://hikaku.jp/sp/common/js/common.js" type="text/javascript" charset="UTF-8"></script>
		<meta name="viewport" content="width=device-width">
		<script type="text/javascript">
			$(function(){
				$("input#change").each(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
				$("input#change").click(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
			});
		</script>
	<!--暫定のスタイルシート20140212報告用▽-->
	<style type="text/css">
	body {
		width:100%;
		height:100%;
	}
	#wrap {
		width:100%;
		height:100%;
	}
	#container {
		padding:10px;
		width:480;
		height:480;
		margin-left:auto;
		margin-right:auto;
	}
	.display_rank {
		text-align:center;
	}
	.display_rank_data{
		text-align:center;
	}
	.image-box{
		text-align:center;
	}
	.input_form{
		padding:5px;
	}
	.closeBtn {
		text-align:left;
		margin-left:auto;
		margin-right:auto;
		margin-top:10px;
	}
	</style>
	<!--暫定のスタイルシート△-->
	</head>
	<body>
		<div id="wrap">
			<div id="container">
	            <!--▽フリースペース投稿フォーム▽-->
	            <form id="freeBox1" style="" name="freeBox1" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;" onsubmit=""  enctype="multipart/form-data">
					<p>タグ1:<input type="text" name="tag1" value="<?php echo $tag1; ?>" />
					タグ2:<input type="text" name="tag2" value="<?php echo $tag2; ?>" /></p>
					<p>タグ3:<input type="text" name="tag3" value="<?php echo $tag3; ?>" />
					タグ4:<input type="text" name="tag4" value="<?php echo $tag4; ?>" /></p>
					<p>タグ5:<input type="text" name="tag5" value="<?php echo $tag5; ?>" />
					タグ6:<input type="text" name="tag6" value="<?php echo $tag6; ?>" /></p>
					<p>タグ7:<input type="text" name="tag7" value="<?php echo $tag7; ?>" />
					タグ8:<input type="text" name="tag8" value="<?php echo $tag8; ?>" /></p>
					<p>タグ9:<input type="text" name="tag9" value="<?php echo $tag9; ?>" />
					タグ10:<input type="text" name="tag10" value="<?php echo $tag10; ?>" /></p>
	            	<p class="tright" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="送信する"/></p>
	            </form>

				<div class="closeBtn">
					<input type="button" value="ウィンドウを閉じる" onclick="window.close();" />
				</div>
	            	
				<!--△フリースペース投稿フォーム△-->
			</div><!--container-->
		</div><!--wrap-->
	</body>
</html>