<?php

class TextConvert {
	
	
	function convertText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$upload_dir_path,$txt_res_dir,$txt_dir,$file_path,$af_id) {

		//txtの内容を取得して分析し、必要な情報（日付、金額、電話番号1,2,3、会社名、商品名、業種）
		//を取得して上書きする工程
//		for($m = 0;$m < count($img_file_arr);$m++) {
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name_arr = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$money_num_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();
		$data_arr = "";
		
		try {
	    	//var_dump($file);
	        //$filename = $img_file_arr[$m];
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$filename  = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$filename );
			$file_name_top = substr($filename , 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($filename , -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			//echo $txt_res_dir."/".$dlp.".txt";
			//echo file_exists($txt_res_dir."/".$dlp.".txt");
			//OCR済みのテキストファイルがあったらOCR処理しない
			if(file_exists($txt_res_dir."/".$dlp.".txt")) {
				//請求書が登録されていないか確認する。
				//$flag ="download_invoice_data_clm";
				//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
				//同じdownload_passwordで請求書が登録済みの場合
				if (substr($filename, -3, 3) == "txt") {
					//請求書登録
					//仮のデータで請求書登録をする。
					//仮の請求書・領収書データの登録
					//テキストを読んで上書き処理
					//echo "テキスト処理<br>";
					$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
					$text_file_path = $txt_dir."/".$dlp.".txt";

					//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
					if(file_exists($text_file_path_en)) {
						$data = file_get_contents($text_file_path_en);
						$data .= file_get_contents($text_file_path);
					} else {
						$data = file_get_contents($text_file_path);
					}
					
					$data_all = $data;
					//$data = mb_convert_encoding($data,"UTF-8","auto");
					$data = mb_convert_encoding( $data, "UTF-8", "SHIFT-JIS");

					//１データとして扱うために分けない。
					$data = explode( "\n", $data );
					//echo "<br>";
					$cnt = count( $data );
					//echo "<br>";
					//var_dump($data);
					//$data_arr = array();
					
					//業種、摘要の照合用リスト作成
					$ind_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/indtype.csv";
					$remarks_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/remarks.csv";
					
					$indtypes = file_get_contents($ind_csv);
					//var_dump($indtypes);
					$ind_arr = explode(",",$indtypes);
					//var_dump($ind_arr);
					
					$remarks = file_get_contents($remarks_csv);
					$remarks_arr = explode(",",$remarks);
					//var_dump($remarks_arr);
					/*
					foreach ($remarks_arr as $line) {
						//終端の空行を除く処理　空行の場合に取れる値は後述
						
						if($line) {
					   		$records[] = $line;
					 	}
						unset($line);
					}
					*/
					//var_dump($records);

					
					

					for ( $i = 0; $i < $cnt; $i++ ) {//echo e;
					
						//変数初期化
						$money_num_cnt = 0;
						$telkey_flag = 0;

						/*
						$outcode = "SHIFT-JIS";
						$incode = "UTF-8";
						$nl = "\r\n";
						$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
						*/
						//全角文字を半角に変換
						//$data = mb_convert_encoding( $data, "UTF-8", "UTF-16" );
						//$data = mb_convert_encoding( $data, "UTF-8", "SHIFT-JIS");
						$data2[$i] = $data[$i];//商品名と会社名の文字列を取得する時は数字を加工しない方が良いため分ける。
						$data[$i] = str_replace(array("！０１５","ｉ０１５"),"2015",$data[$i]);
						$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
						$data[$i] = strtr($data[$i], $kana);
						$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
						$data[$i] = str_replace(array("'","'"," ","内","総勘定元帳","万","・","゛","ｌ","ム","ヽ","○","△","|",",","，","・","．","　1　","　ｉ　","1口　","1ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","ｊ","－,","’","－","￣",", ",". ","‘","｀","丿","）","”","°","゛","卜","－","／","Ｌ","丶－","』","＼","ゞ"),"",$data[$i]);
						$data[$i] = str_replace(array("－","’","￣")," ",$data[$i]);
						$data[$i] = str_replace(array("龠"),"金",$data[$i]);
						$data[$i] = str_replace(array("禽","－","ー","‐"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n　"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n\r\n"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n\r\n\r\n\r\n\r\n"),"",$data[$i]);
						
						//$data[$i] = str_replace(array("　"),",",$data[$i]);
						//$data[$i] = str_replace(array("　　	"),",",$data[$i]);
						//$data[$i] = str_replace(array("　　　"),",",$data[$i]);
						$data[$i] = str_replace(array("　"),"○",$data[$i]);
						$data[$i] = str_replace(array("　　	"),"○",$data[$i]);
						$data[$i] = str_replace(array("　　　"),"○",$data[$i]);
						//$data[$i] = str_replace(array("　　"),"　",$data[$i]);
						$data[$i] = str_replace(array("○/○"),"",$data[$i]);
			
						
						$data[$i] = str_replace(array("○○○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("、","	"),"",$data[$i]);
						$data[$i] = str_replace(array(",,,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,"),",",$data[$i]);
						
						$data[$i] = str_replace(array(",2015","！015"),"2015",$data[$i]);
						$data[$i] = str_replace(array("叩","m","0c","0C","0(1"),"00",$data[$i]);
						$data[$i] = str_replace(array("ｊｌ","Ｉ","ｉｌ","ｊ","ｈ"),"/",$data[$i]);
						$data[$i] = str_replace(array("判"),"\4",$data[$i]);
						$data[$i] = str_replace(array("//","{{","[1"),"11",$data[$i]);
						$data[$i] = str_replace(array("。","。","〝","位","n","ロ","o","U","()","(1","ｏ","｛｝","［］","〔〕"),"0",$data[$i]);
						$data[$i] = str_replace(array("■","ー","]","L"),"1",$data[$i]);
						$data[$i] = str_replace(array("z"),"2",$data[$i]);
						$data[$i] = str_replace(array("會"),"3",$data[$i]);
						$data[$i] = str_replace(array("縄","繍"),"4",$data[$i]);
						$data[$i] = str_replace(array("轟","印","§"),"5",$data[$i]);
						$data[$i] = str_replace(array("s","野"),"6",$data[$i]);
						$data[$i] = str_replace(array("a","讐","B","日","自","ｓ","β"),"8",$data[$i]);
						$data[$i] = str_replace(array("g","q"),"9",$data[$i]);
						$data[$i] = str_replace(array("事","令","隼","藻","ff"),"年",$data[$i]);
						$data[$i] = str_replace(array("隷","A"),"月",$data[$i]);
						$data[$i] = str_replace(array("B","t1","曰"),"日",$data[$i]);
						$data[$i] = str_replace(array("年明","#明"),"年8月",$data[$i]);
						$data[$i] = str_replace(array("年9A","#9A"),"年9月",$data[$i]);
						$data[$i] = str_replace(array("年坍"),"年1月",$data[$i]);
						$data[$i] = str_replace(array("明旧"),"8月1日",$data[$i]);
						$data[$i] = str_replace(array("¨","－","一","・","~"),"-",$data[$i]);
						//$data[$i] = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data[$i]);
						$data[$i] = str_replace(array("普遍車","普遷車","普通車"),"普通車",$data[$i]);
						$data[$i] = str_replace(array(",普通車","普通車")," 普通車",$data[$i]);
						$data[$i] = str_replace(array("輕自動"),"軽自動",$data[$i]);
						$data[$i] = str_replace(array(",軽自動","軽自動")," 軽自動",$data[$i]);
						$data[$i] = str_replace(array("合","卦","會"),"合",$data[$i]);
						$data[$i] = str_replace(array("ＥＮＥｏｓ"),"ＥＮＥＯＳ",$data[$i]);
						$data[$i] = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data[$i]);
						$data[$i] = str_replace(array("オ寸","本寸"),"村",$data[$i]);
						$data[$i] = str_replace(array("言舌"),"話",$data[$i]);
						$data[$i] = str_replace("」R","ＪＲ",$data[$i]);
						$data[$i] = str_replace(array("20151","2051","20/5","2015?","2015#","2015：","20151","201ｓ","20！51"),"2015/",$data[$i]);
						$data[$i] = str_replace(array("20161","2061","20/6","2016?","2016#","2016：","20161","Zo/6"),"2016/",$data[$i]);
						$data[$i] = str_replace(array("20171","2071","20/7","2017?","2017#","2017：","20171"),"2017/",$data[$i]);
						//$data[$i] = str_replace(array("2015/121",),"2015/12/",$data[$i]);
						//$data[$i] = str_replace(array("2016/121",),"2016/12/",$data[$i]);
						//$data[$i] = str_replace(array("2017/121"),"2017/12/",$data[$i]);

						$patterns = array ('/次頁繰越/');
						$replace = array ('');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						
						$patterns = array ('/(20\d{2})\/[,.]*(\d{1,2})1[,.]*(\d{1,2})/');
						$replace = array ('\1/\2/\3,');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);

						$patterns = array ('/([0-9]+)○([0-9]+)/imu');
						$replace = array ('◆\1◆\2◆');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);

						$patterns = array ('/([0-9]+)/imu');
						$replace = array ('■\1□');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);

						$patterns = array ('/■[0-9]{1,2}□(.?+)/imu');
						$replace = array ('\1');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);


						$patterns = array ('/■[0-9]+□(.?+)[■,□]+([0-9]+)[■,□]+([0-9]+)/imu');
						$replace = array ('\1,\2,\3');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						
/*
						$patterns = array ('/([0-9]+)○(\W+)/imu');
						$replace = array ('■\1■\2');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
*/

/*						
						$patterns = array ('/○/imu');
						$replace = array (',');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
*/

						$patterns = array ('/,(.),(.),(.),(.)/imu');
						$replace = array ('\1\2\3\4');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);

						$patterns = array ('/,(.),(.),(.)/imu');
						$replace = array ('\1\2\3');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);


						$patterns = array ('/(\W{1,3}),(\W{1,3})/imu');
						$replace = array ('\1\2');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
/*
						$patterns = array ('/,(\W{1,4}),(\W{1,4})/im');
						$replace = array ('\1\2');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
*/
/*
						$patterns = array ('/[0-9]*,(.),(.),(.),(.),(.)/imu');
						$replace = array ('\1\2\3\4\5');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);						
*/												
/*
						$patterns = array ('/^○/im','/\n\n/im');
						$replace = array ('','\n');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);						
*/
						/*
						$patterns = array ('/○1\s\n/ism','/1\s/ism');
						$replace = array ('','');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);						
						*/
						
						
						//var_dump($data[$i]);
						/*
						$patterns = array ('/(\w{1})[,]{1}(\w{1})/');
						$replace = array ('\1\2/');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						*/
						$data[$i] = str_replace(array("■"),",",$data[$i]);
						$data[$i] = str_replace(array("○","■1■","1□","◆","□"),"",$data[$i]);
						$data[$i] = str_replace(array("ム■","△■"),"",$data[$i]);
						$data[$i] = str_replace(array(",1,"),",",$data[$i]);
						
						/*動かない。
						for ($k = 0;$k < count($ind_arr);$k++) {
							$patterns = '/'.$ind_arr[$k].'/imu';
							$replace = $ind_arr[$k].'A';
							$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						}
						*/
						$data[$i] = str_replace(array(",-")," ‐ ",$data[$i]);
						$data[$i] = str_replace(array(",,","ｌ,ｌ","ｌ,ｌ,ｌ"),",",$data[$i]);
						$data[$i] = str_replace(array("－"),"",$data[$i]);
						$data[$i] = str_replace(array("ＫＤＤ1"),"ＫＤＤＩ",$data[$i]);
						$data[$i] = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data[$i]);
						$data[$i] = str_replace(array("食年代"),"食事代",$data[$i]);
						$data[$i] = str_replace(array("商年"),"商事",$data[$i]);
						$data[$i] = str_replace(array("ＥＴＣ","ＥＴｃ","耳Ｔｃ"),"ＥＴＣ ",$data[$i]);
						$data[$i] = str_replace(array("ＥＴＣ ,"),"ＥＴＣ ",$data[$i]);
						$data[$i] = str_replace(array("¥","$","#","="),"￥",$data[$i]);
						$data[$i] = str_replace(array(":-"),".",$data[$i]);
						$data[$i] = str_replace(array("…","●","●,●","≒",",●","●,",",,","‥","ノ","/",",て","¶","］","ぺ","て","－\n","－","－","｜","1■","1□"),"",$data[$i]);
						$data[$i] = str_replace(array("消耗品費","濃耗品費"),"消耗品費,",$data[$i]);
						$data[$i] = str_replace("現金","現金,",$data[$i]);
						$data[$i] = str_replace("新聞図書費","新聞図書費,",$data[$i]);
						$data[$i] = str_replace("普通預金1","普通預金1,",$data[$i]);
						$data[$i] = str_replace("普通預金","普通預金1,",$data[$i]);
						$data[$i] = str_replace("雑費","雑費,",$data[$i]);
						$data[$i] = str_replace("水道光熱費","水道光熱費,",$data[$i]);
						$data[$i] = str_replace("車輌費","車輌費,",$data[$i]);
						$data[$i] = str_replace("預り金","預り金,",$data[$i]);
						$data[$i] = str_replace("修繕費","修繕費,",$data[$i]);
						$data[$i] = str_replace("福利厚生費","福利厚生費,",$data[$i]);
						$data[$i] = str_replace("雑給","雑給,",$data[$i]);
						$data[$i] = str_replace("旅費交通費","旅費交通費,",$data[$i]);
						$data[$i] = str_replace("広告宣伝費","広告宣伝費,",$data[$i]);
						$data[$i] = str_replace("複合","複合,",$data[$i]);
						$data[$i] = str_replace("仕入金額","仕入金額,",$data[$i]);
						$data[$i] = str_replace("会議費","会議費,",$data[$i]);
						$data[$i] = str_replace("事業主貸","事業主貸,",$data[$i]);
						$data[$i] = str_replace("地代家賃","地代家賃,",$data[$i]);
						$data[$i] = str_replace("交際接待費","交際接待費,",$data[$i]);
						$data[$i] = str_replace("保険料","保険料,",$data[$i]);
						$data[$i] = str_replace("通信費","通信費,",$data[$i]);
						$data[$i] = str_replace("租税公課","租税公課,",$data[$i]);
						$data[$i] = str_replace("未払消費税","未払消費税,",$data[$i]);
						$data[$i] = str_replace("法定福利費","法定福利費,",$data[$i]);
						$data[$i] = str_replace("諸会費","諸会費,",$data[$i]);
						$data[$i] = str_replace("支払手数料","支払手数料,",$data[$i]);
						$data[$i] = str_replace("期首棚卸高","期首棚卸高,",$data[$i]);
						$data[$i] = str_replace("棚卸資産","棚卸資産,",$data[$i]);
						$data[$i] = str_replace("買掛金","買掛金,",$data[$i]);
						$data[$i] = str_replace("未払金","未払金,",$data[$i]);
						$data[$i] = str_replace("給料賃金","給料賃金,",$data[$i]);
						$data[$i] = str_replace("減価償却費","減価償却費,",$data[$i]);
						$data[$i] = str_replace("仮受消費税","仮受消費税,",$data[$i]);
						$data[$i] = str_replace(array("売上高","売上商"),"売上高,",$data[$i]);
						$data[$i] = str_replace("仮払消費税","仮払消費税,",$data[$i]);
						$data[$i] = str_replace("現金,売上","現金売上",$data[$i]);
						
						
						
						for ( $i = 0; $i < $cnt; $i++ ) {
						
							//会社名を取得する
							$journalizing_arr = array(
								"消耗品費",
								"現金",
								"新聞図書費",
								"普通預金1",
								"雑費",
								"水道光熱費",
								"車輌費",
								"預り金",
								"修繕費",
								"福利厚生費",
								"雑給",
								"旅費交通費",
								"広告宣伝費",
								"複合",
								"仕入金額",
								"会議費",
								"事業主貸",
								"地代家賃",
								"交際接待費",
								"保険料",
								"通信費",
								"租税公課",
								"未払消費税",
								"法定福利費",
								"諸会費",
								"支払手数料",
								"期首棚卸高",
								"棚卸資産",
								"買掛金",
								"未払金",
								"給料賃金",
								"減価償却費",
								"仮受消費税",
								"売上高",
								"仮払消費税"
							);
							$p_cnt = 0;
							$match = "";
							
							for ($z = 0;$z < count($journalizing_arr);$z++) {
								echo "ここから　".$data[$i]."   →　　";
								echo $pattern_comp_end = "/".$journalizing_arr[$z]."/";echo "　ここまで<br>";
								echo $match_num = preg_match($pattern_comp_end, $data[$i], $match);
								echo "<br>";
								if ($match_num >= 1) {
									//$company_arr[$p_cnt] = $match[0];
									//echo "<br>仕訳候補<br/>\r\n";
									//var_dump($match);
									//echo $data[$i];
									//echo "<br/>";
									$data_arr .= $data[$i];
								}
							}
						}
						
					}//for文の終わり
					$data = $data_arr;
					// ファイルのパスを変数に格納
					$FileText = $txt_dir."/".$dlp."ci.txt";
					// ファイルに書き込む
					file_put_contents($FileText,$data,LOCK_EX);

					//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
					//入れる金額が無かった場合、一番上の金額を入れる。
				}//if ( ==png)

				//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
//				$AddWords = "\r\n".date('Y/m/d',strtotime($got_date)).",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$industry_type_s;
				//echo "<br>";
//				file_put_contents($FileText, $AddWords, FILE_APPEND);
				$outcode = "SHIFT-JIS";
				$incode = "UTF-8";
				$nl = "\n";
				$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
				//中間フォルダからリネームして結果フォルダに入れる。
//				rename( $file_path.$dlp.'ci.txt', $txt_res_dir."/".$dlp.'.txt');
				//処理とともに画像も結果フォルダにコピーする。
//				rename( $upload_dir_path."/".$dlp.'.jpg', $txt_res_dir."/".$dlp.'.jpg');
				
				//$outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

				// ファイルを出力する
				//readfile($FileText);
				//exit;
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
			//取得したTEXTファイルを削除する。
	//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//$counter++;
			return $FileText;
//		}//for文の終わり
	}//メソッドの終わり

	/**
	* テキストファイルの文字コードを変換し保存する
	* @param string $infname  入力ファイル名
	* @param string $incode   入力ファイルの文字コード
	* @param string $outfname 出力ファイル名
	* @param string $outcode  出力ファイルの文字コード
	* @param string $nl       出力ファイルの改行コード
	* @return string メッセージ
	*/
	function convertCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'wb');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}

	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}


	function recurse_chown_chgrp($mypath,$uid, $gid) {
		if (mkdir( $mypath, 0775, true ) ) {
			chmod( $mypath, 0775);
			chown( $mypath, $uid);
		    chgrp( $mypath, $gid);
		  //echo "ディレクトリ作成成功！！";
		} else {
		  //echo "ディレクトリ作成失敗！！";
		}
	} 

	function getFileList($dir) {
	    $files = glob(rtrim($dir, '/') . '/*');
	    $list = array();
	    foreach ($files as $file) {
	        if (is_file($file)) {
	            $list[] = $file;
	        }
	        if (is_dir($file)) {
	            $list = array_merge($list, getFileList($file));
	        }
	    }
	 
	    return $list;
	}

	function random($length = 12) {
	    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
	    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
	}
}//classの終わり

?>