<?php
session_start();
//メールリマインダー
//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__)."/../db_con/accountfirmsclientcontrol.class.php");
require_once(dirname(__FILE__)."/../db_con/accountfirmcontrol.class.php");

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$af_client_con = new account_firms_client_control();
$af_con = new account_firm_control();
$signin_type = $_REQUEST['signin_type'];

if ($signin_type == 1) {
	$page_type = "_af";
} else if ($signin_type == 2) {
	$page_type = "_acm";
}

//ここからメールアドレス照合
	if (isset($_REQUEST['email'])) {
		//echo "Emailアドレスの存在を確認する。";
		$email = $_SESSION['e_mail'] = htmlspecialchars($_REQUEST['email'],ENT_QUOTES);
		//$login_con = new user_control();
		//$login_arr = $login_con->user_select_email($pdo,$email);
		$af_arr = $af_con -> af_select_email($pdo,$email);
		//$_SESSION['user_id'] = $login_arr['user_id'];

		if ($af_arr != NULL) {
			//$passwordmaker = random(12);
			$af_id = $af_arr['id'];
			$passwordmaker = uniqid();
			$af_arr['password'] = md5($passwordmaker);

			$update_data = " `password` = '".$af_arr['password']."' ";
			$af_con -> af_update_data($pdo,$af_id ,$update_data);
			//var_dump($login_arr);
			
			mb_language("ja");
			mb_internal_encoding("UTF-8");
			$newpassword = $passwordmaker;
			$subject ="仮パスワード発行メール";
			$header = "From: ".mb_encode_mimeheader("Cloud Invoice 運営" ,'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>\r\n";
$message = $af_arr['name']."様

いつもご利用ありがとうございます。
仮パスワードの発行依頼がございました。
仮パスワードは、下記の通りです。

ID(E-mailアドレス):".$af_arr['e_mail']."
パスワード:".$newpassword."
サインインURL:http://storage.cloudinvoice.co.jp/invoice/signin".$page_type."

ご登録のE-mailアドレスと仮パスワードでログインしてから、パスワードの変更をお願いいたします。

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数ではございますが、
運営(info@storage.cloudinvoice.co.jp)までメールをいただけますでしょうか。

よろしくお願いいたします。

Cloud Invoice運営
";

	//echo $email;echo $subject;echo $message;echo $header;exit;
	mail($email,$subject,$message,$header);
        } else {
			//情報を取得できなかった場合はリマインダーへ
		    $_SESSION['emailcheck'] = "メールアドレスが登録されておりません。";
			header("Location:./forgotpassword".$page_type,FALSE);
    		exit();
        }
	}

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ユーザ詳細登録画面</title>
<script type="text/javascript" language="JavaScript">
<!--
function close_win(){
	var nvua = navigator.userAgent;
	if (nvua.indexOf('MSIE') >= 0){
		if (nvua.indexOf('MSIE 5.0') == -1) {
			top.opener = '';
		}
	}
    else if (nvua.indexOf('Gecko') >= 0) {
      top.name = 'CLOSE_WINDOW';
      wid = window.open('','CLOSE_WINDOW');
    }
    top.close();
}


-->
</script>
<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#wrap input {
	text-align:center;
	margin-left:auto;
	margin-right:auto;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}

</style>
<title>仮パスワード発行</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">仮パスワード発行</h1>
		<h2>仮パスワードを発行いたしました。</h2>
		<p>ご登録のメールアドレスに仮パスワードを送信いたしましたので、
		<br />仮パスワードでログインして任意のパスワードに変更してください。
		</p>
		<p><input type="button" value="閉じる" onclick="window.close();" />
		</p>
		<p onclick="window.open('about:blank','_self').close()" style="cursor:pointer;">（ブラウザがFirefoxの場合、ボタンでは閉じない事があります。）</p>
		<button onclick="location.href='//storage.cloudinvoice.co.jp'">サイトのトップに戻る</button>
	</div>
</body>
</html>