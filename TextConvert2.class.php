<?php

class TextConvert {
	
	function convertText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$upload_dir_path,$txt_res_dir,$txt_dir,$file_path,$af_id,$output,$fiscal_year,$financial_month,$com_name) {
		//txtの内容を取得して分析し、必要な情報（日付、金額、電話番号1,2,3、会社名、商品名、業種）
		//を取得して上書きする工程
//		for($m = 0;$m < count($img_file_arr);$m++) {
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name_arr = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$money_num_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();
		$first_cnt = 0;
		$partner_accounts = "";
		$account = "";
		$account_num = "";
		
		if ($fiscal_year == "") {
			$fiscal_year = 2014;//年度 ?y=2013で年度切り替え可能
		}
		if ($financial_month == "") {
			$financial_month = 4;//決算月 ?m=5で年度切り替え可能
		}
		if (isset($_REQUEST["y"])){
			$fiscal_year = $_REQUEST["y"];
		}
		if (isset($_REQUEST["m"])){
			$financial_month = $_REQUEST["m"];
		}

		$code_arr=array(
		"1101"=>"現金",
		"1107"=>"普通預金",
		"2109"=>"預り金",
		"3999"=>"複合",
		"1161"=>"事業主貸",
		"1171" => "商品",
		"2116"=>"未払消費税",
		"2119"=>"事業主借",
		"4201"=>"期首棚卸高",
		"4206"=>"期末棚卸高",
		"2103"=>"買掛金",
		"2106"=>"未払金",
		"2117"=>"未払事業税",
		"8000"=>"仮受消費税",
		"8001"=>"仮払消費税",
		"4423"=>"消耗品費",
		"4101"=>"売上高",
		"4102"=>"売上高1",
		"4434"=>"新聞図書費",
		"4435"=>"雑費",
		"4424"=>"水道光熱費",
		"4432"=>"車輌費",
		"4421"=>"修繕費",
		"4418"=>"福利厚生費",
		"4413"=>"雑給",
		"4425"=>"旅費交通費",
		"4404"=>"広告宣伝費",
		"4202"=>"仕入金額",
		"4436"=>"会議費",
		"4420"=>"地代家賃",
		"4428"=>"交際接待費",
		"4429"=>"保険料",
		"4430"=>"通信費",
		"4427"=>"租税公課",
		"4437"=>"法定福利費",
		"4431"=>"諸会費",
		"4433"=>"支払手数料",
		"1171"=>"棚卸資産",
		"1202"=>"建物付属設備",
		"1205"=>"工具器具備品",
		"4412"=>"給料賃金",
		"4419"=>"減価償却費",
		"5105"=>"雑収入",
		"5205"=>"雑捐失",
		"0000"=>"賄費"
		);

		require_once(dirname(__FILE__).'/Kana2Romaji.class.php');
		$kana2romaji = new Kana2Romaji();

		try {
	    	//var_dump($file);
	        //$filename = $img_file_arr[$m];
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$filename  = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$filename );
			$file_name_top = substr($filename , 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($filename , -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			//初期化
			$data_cnt = 0;
			//echo $txt_res_dir."/".$dlp.".txt";
			//echo file_exists($txt_res_dir."/".$dlp.".txt");
			//OCR済みのテキストファイルがあったらOCR処理しない
			if(file_exists($txt_res_dir."/".$dlp.".txt")) {
				//請求書が登録されていないか確認する。
				//$flag ="download_invoice_data_clm";
				//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
				//同じdownload_passwordで請求書が登録済みの場合
				if (substr($filename, -3, 3) == "txt") {
					//請求書登録
					//仮のデータで請求書登録をする。
					//仮の請求書・領収書データの登録
					//テキストを読んで上書き処理
					//echo "テキスト処理<br>";
					$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
					$text_file_path = $txt_dir."/".$dlp.".txt";

					//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
					if(file_exists($text_file_path_en)) {
						$data = file_get_contents($text_file_path_en);
						$data .= file_get_contents($text_file_path);
					} else {
						$data = file_get_contents($text_file_path);
					}
					
					$data_all = $data;
					//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

					//１データとして扱うために分けない。
					//$data = explode( "\n", $data );
					//echo "<br>";
					$data_cnt = count( $data );
					//echo "<br>";
					//var_dump($data);
					$fixed_data = "";
					$output_data = "";
					//業種、摘要の照合用リスト作成
					$ind_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/indtype.csv";
					$remarks_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/remarks.csv";
					
					$indtypes = file_get_contents($ind_csv);
					//var_dump($indtypes);
					$ind_arr = explode(",",$indtypes);
					//var_dump($ind_arr);
					
					$remarks = file_get_contents($remarks_csv);
					$remarks_arr = explode(",",$remarks);
					//var_dump($remarks_arr);
					/*
					foreach ($remarks_arr as $line) {
						//終端の空行を除く処理　空行の場合に取れる値は後述
						
						if($line) {
					   		$records[] = $line;
					 	}
						unset($line);
					}
					*/
					//var_dump($records);

					for ( $i = 0; $i < $data_cnt; $i++ ) {//echo e;
					
						//変数初期化
						$money_num_cnt = 0;
						$telkey_flag = 0;
						
						/*
						$outcode = "sjis-win";//"SHIFT-JIS";
						$incode = "UTF-8";
						$nl = "\r\n";
						$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
						*/
						
						//全角文字を半角に変換
						//$data = mb_convert_encoding( $data, "UTF-8", "UTF-16" );
						$data = mb_convert_encoding( mb_convert_encoding($data, "sjis-win", "SHIFT-JIS"),"UTF-8","sjis-win");
						
						$data = str_replace(array("！０１５","ｉ０１５"),"2015",$data);
						$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0","ｏ"=>"0");
						$data = strtr($data, $kana);
						$data = mb_convert_kana($data, 'aKvrn');
/*						$accounts_arr = array(
							"([0-9]{1}$)"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('\1■');
							$data = preg_replace($patterns, $replace, $data);
						}
*/						

						$data = $this -> convertNewline($data, $to = "\r\n");

	//echo $data;echo " <br><br>";
						
						$patterns = array ('/([0-9]{1,3}.{1}$)/mimu');
						$replace = array ('\1■');
						$data = preg_replace($patterns, $replace, $data);
						//$data = str_replace(array("課対仕入8％内税 ■"),"課対仕入8％内税",$data);
						//echo $data;echo "<br>";
						
						$data = str_replace(array("\r\n"),"",$data);
						$data = str_replace(array("■"),"\r\n",$data);
						//echo "<br>";
						
						$data = str_replace(array("現　　　　金","現　　　金","現　　金","現　金"),"現金,",$data);
						$data = str_replace(array("複　　　　合","複　　　合","複　　合","複　合","複　　　　ａ","複　　　ａ","複　　ａ","複　ａ"),"複合,",$data);
						$data = str_replace(array("。","。","〝","o","U","()","ｏ","｛｝","［］","〔〕"),"0",$data);
						$data = str_replace(array("會","輻"),"3",$data);
						$data = str_replace(array("繍","ｘ"),"4",$data);
						$data = str_replace(array("§"),"5",$data);
						$data = str_replace(array("讐","β"),"8",$data);
						$data = str_replace(array("g","q"),"9",$data);
						$data = str_replace(array("り叙","り全"),"り金",$data);
						$data = str_replace(array("SBI","ＳＢＩ","S日"),"SBI",$data);
						$data = str_replace(array("ホー△"),"ホーム",$data);
						$data = str_replace(array("が刈ン"),"ガソリン",$data);
						$data = str_replace(array("輛","涌","甬","稠人","贄","摩生","雜"),array("輌","通","通","預入","費","厚生","雑"),$data);
						
						
						//var_dump($data);
						$data = str_replace(array("Ｈ"),"11",$data);
						$data = str_replace(array("收"),"収",$data);
						$data = str_replace(array("肖"),"複",$data);
						$data = str_replace(array("龠","彖","奎","禽","叙","兮","昶","歛"),"金",$data);
						$data = str_replace(array("碵","陌","頑","煩","頂","須","汨","稠"),"預",$data);
						$data = str_replace(array("涌"),"通",$data);
						$data = str_replace(array("筏"),"複",$data);
						$data = str_replace(array("紿"),"給",$data);
						$data = str_replace(array("イ弋"),"代",$data);
						$data = str_replace(array("診涌","診通","普涌","普埴","倅通","普油"),"普通",$data);
						$data = str_replace(array("淤","渟","淬","鏗","矜"),"普",$data);
						$data = str_replace(array("肖否","複含","複ａ","複a","複き","複台","楔合","掏合","陶合","誨合","煦合","陶今"),"複合",$data);
						$data = str_replace(array("預氣","侑金","硯金","現奎","現彖","陌金","頂金","頂彖","碵金","甬金","附金","贖金","に金"),"預金",$data);
						$data = str_replace(array("現歛","現全"),"現金",$data);
						$data = str_replace(array("仕人"),"仕入",$data);
						$data = str_replace(array("脱","悦"),"税",$data);
						$data = str_replace(array("貲"),"貸",$data);
						$data = str_replace(array("起業額"),"起票額",$data);
						$data = str_replace(array("Ｍ"),"14",$data);
						$data = str_replace(array("]","L","I","！","!","ｊ","』","｜","】","ｌ"),"1",$data);
						$data = str_replace(array("繍","ｘ","・ｌ","・1","ｔ"),"4",$data);
						$data = str_replace(array("小1","・ｌｔ"),"44",$data);
						$data = str_replace(array("￣","_"),"ー",$data);
						$data = str_replace(array("-"),"一",$data);
								
						$data2 = $data;//後ろの数字を消して日付だけ取り出す処理をした方がいい。
						$data2 = str_replace(array(" ","−","｝","?","総勘定元帳","勘定元帳","∩","Ｃ廴","廴","（","ヽ","○","△","|","　ｉ　","　","口　","1ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","ｊ","Ｊ","＿","－","－,","－","‘","｀","丿","）","”","°","゛","卜","－","／","Ｌ","丶－","』","＼","ゞ","）","（","　I　","　Ｉ　","　1　"),"",$data2);
						$data2 = str_replace(array("・","’","。","."),"．",$data2);
						//$data2 = str_replace(array("．"),"/",$data2);
						//$data = str_replace(array("ｙ"),"1",$data);
						//$data2 = str_replace(array("ｙ"),"1",$data2);
						$data2 = str_replace(array("－","‐"),"",$data2);
						$data2 = str_replace(array("\r\n　"),"",$data2);
						$data2 = str_replace(array("\r\n"),"",$data2);
						$data2 = str_replace(array("\r\n\r\n"),"",$data2);
						$data2 = str_replace(array("\r\n\r\n\r\n\r\n\r\n"),"",$data2);
						//$data = str_replace(array("仕人"),"仕入",$data);
						
						$accounts_arr = array(
							"(普.?+.?+金)","(.?+通預金)","(普通.?+金)","(.?+涌預金)","(.?+預金)","(矜涌附金)"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('普通預金');
							$data = preg_replace($patterns, $replace, $data);
						}
						$accounts_arr = array(
							"(複.?+)","(.?+合)"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('複合');
							$data = preg_replace($patterns, $replace, $data);
						}

						
						
						$data4 = $data2;
						//$data2 = str_replace(array("　"),",",$data2);
						//$data2 = str_replace(array("　　	"),",",$data2);
						//$data2 = str_replace(array("　　　"),",",$data2);
						$data2 = str_replace(array("　","	"),"○",$data2);
						$data2 = str_replace(array("　　","		"),"○",$data2);
						$data2 = str_replace(array("　　　","			"),"○",$data2);
						//$data2 = str_replace(array("　　"),"　",$data2);
						$data2 = str_replace(array("○/○"),"",$data2);
			
						
						$data2 = str_replace(array("Ｏ"),"",$data2);
						$data2 = str_replace(array("○○○○○○"),"○",$data2);
						$data2 = str_replace(array("○○○○○"),"○",$data2);
						$data2 = str_replace(array("○○○○"),"○",$data2);
						$data2 = str_replace(array("○○○"),"○",$data2);
						$data2 = str_replace(array("○○"),"○",$data2);
						$data2 = str_replace(array("、","	"),"",$data2);
						$data2 = str_replace(array(",,,"),",",$data2);
						$data2 = str_replace(array(",,"),",",$data2);
						$data2 = str_replace(array(",,,"),",",$data2);
						$data2 = str_replace(array(",,"),",",$data2);
						//$data2 = str_replace(array("■","]","L"),"1",$data2);
						
						$data2 = str_replace(array("。","。","〝","位","o","U","()","ｏ","｛｝","［］","〔〕"),"0",$data2);
						$data2 = str_replace(array("]","L","I","！","!","I,","Ｉ,","Ｉ，"),"1",$data2);
						$data2 = str_replace(array("z"),"2",$data2);
						$data2 = str_replace(array("會","輻"),"3",$data2);
						$data2 = str_replace(array("繍","ｘ"),"4",$data2);
						$data2 = str_replace(array("§"),"5",$data2);
						$data2 = str_replace(array("s"),"6",$data2);
						$data2 = str_replace(array("讐","Ｓ","ｓ","β"),"8",$data2);
						$data2 = str_replace(array("g","q"),"9",$data2);
						$data2 = str_replace(array("り叙","り全"),"り金",$data2);
						

						
						//var_dump($data2);
						$data3 = $data2;				
						$data = str_replace(array("'","'"," ","−","｝","総勘定元帳","勘定元帳","∩","Ｃ廴","廴","（","ヽ","○","△","|",",","，","・","．","　1　","　ｉ　","口　","ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","ｊ","－,","’","－","￣",", ",". ","‘","｀","丿","）","”","°","゛","卜","－","／","Ｌ","丶－","』","＼","ゞ"),"",$data);
						$data = str_replace(array("－","’","￣")," ",$data);
						$data = str_replace(array("龠","彖","奎"),"金",$data);
						$data = str_replace(array("禽","－","‐"),"",$data);
						$data = str_replace(array("\r\n　"),"",$data);
						$data = str_replace(array("\r\n"),"",$data);
						$data = str_replace(array("\r\n\r\n"),"",$data);
						$data = str_replace(array("\r\n\r\n\r\n\r\n\r\n"),"",$data);
						$data = str_replace(array("診涌","診通","普涌","普埴","倅通","普油"),"普通",$data);
						$data = str_replace(array("淤","渟","淬","鏗","矜"),"普",$data);
						$data = str_replace(array("涌"),"通",$data);
						$data = str_replace(array("碵"),"預",$data);
						$data = str_replace(array("筏"),"複",$data);
						$data = str_replace(array("紿"),"給",$data);
						$data = str_replace(array("肖否","複含","複ａ","複a","複き","複台","楔合","掏合","陶合","誨合","煦合","陶今"),"複合",$data);
						$data = str_replace(array("侑金","陌金","頂金","頂彖","碵金","甬金","附金","贖金","に金"),"預金",$data);
						$data = str_replace(array("仕人"),"仕入",$data);
						$data = str_replace(array("俔","睨"),"税",$data);
						$data = str_replace(array("捐"),"損",$data);
						$data = str_replace(array("有費","陏費"),"賄費",$data);
					
						//$data = str_replace(array("仕人"),"仕入",$data);
						
						
						$patterns = array ('/　([1-5]{1}0)　/imu');
						$replace = array ('【【\1】】');
						$data = preg_replace($patterns, $replace, $data);
/*
						$patterns = array ('/他[0-9]{1,2}名/imu');
						$replace = array ('[[]]');
						$data = preg_replace($patterns, $replace, $data);
						
						mb_convert_kana($str_kana, 'kvrn') 
						$data = strtr($data, $kana);
						$data = mb_convert_kana($data, 'aKvrn');
*/
						$data = str_replace(array("【【10】】","【【20】】","【【30】】","【【40】】","【【50】】","【【60】】","【【70】】","【【80】】","【【90】】"),array("【【１０】】","【【２０】】","【【３０】】","【【４０】】","【【５０】】","【【６０】】","【【７０】】","【【８０】】","【【９０】】"),$data);
						
						$accounts_arr = array(
							"(普.?+.?+金)","(.?+通預金)","(普通.?+金)","(.?+涌預金)","(.?+預金)","(矜涌附金)"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('普通預金');
							$data = preg_replace($patterns, $replace, $data);
						}
						$accounts_arr = array(
							"(複.?+)","(.?+合)"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('複合');
							$data = preg_replace($patterns, $replace, $data);
						}
						
						$accounts_arr = array(
							"(.?+)合"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('複');
							$data = preg_replace($patterns, $replace, $data);
						}

						$accounts_arr = array(
							"売(.?+)高"
						);
						for ($s = 0;$s < count($accounts_arr);$s++) {
							$patterns = array ('/'.$accounts_arr.'/imu');
							$replace = array ('上');
							$data = preg_replace($patterns, $replace, $data);
						}
						
						//$data = str_replace(array("　"),",",$data);
						//$data = str_replace(array("　　	"),",",$data);
						//$data = str_replace(array("　　　"),",",$data);
						$data = str_replace(array("　","	"),"○",$data);
						$data = str_replace(array("　　","		"),"○",$data);
						$data = str_replace(array("　　　","			"),"○",$data);
						//$data = str_replace(array("　　"),"　",$data);
						$data = str_replace(array("○/○"),"",$data);
							
						$data = str_replace(array("Ｏ"),"",$data);
						$data = str_replace(array("○○○○○○"),"○",$data);
						$data = str_replace(array("○○○○○"),"○",$data);
						$data = str_replace(array("○○○○"),"○",$data);
						$data = str_replace(array("○○○"),"○",$data);
						$data = str_replace(array("○○"),"○",$data);
						$data = str_replace(array("、","	"),"",$data);
						$data = str_replace(array(",,,"),",",$data);
						$data = str_replace(array(",,"),",",$data);
						$data = str_replace(array(",,,"),",",$data);
						$data = str_replace(array(",,"),",",$data);
						
						$data = str_replace(array(",2015","！015"),"2015",$data);
						$data = str_replace(array("叩","m","0c","0C"),"00",$data);
						$data = str_replace(array("ｊｌ","ｉｌ","ｊ","ｈ"),"/",$data);
						//$data = str_replace(array("//","{{","[1"),"11",$data);
						$data = str_replace(array("。","。","〝","位","o","U","()","ｏ","｛｝","［］","〔〕","（ｊ"),"0",$data);
						$data = str_replace(array("]","L","I","！","!","ｊ","』","｜"),"1",$data);
						$data = str_replace(array("Ｉ","！"),"1",$data);
						$data = str_replace(array("z"),"2",$data);
						$data = str_replace(array("會","輻"),"3",$data);
						$data = str_replace(array("繍","ｘ"),"4",$data);
						$data = str_replace(array("轟","§"),"5",$data);
						$data = str_replace(array("s"),"6",$data);
						$data = str_replace(array("讐","Ｓ","ｓ","β"),"8",$data);
						$data = str_replace(array("g","q"),"9",$data);
						$data = str_replace(array("B","t1","曰"),"日",$data);
						$data = str_replace(array("¨","－","・","~"),"-",$data);
						//$data = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data);
						$data = str_replace(array("普遍車","普遷車","普通車"),"普通車",$data);
						$data = str_replace(array(",普通車","普通車")," 普通車",$data);
						$data = str_replace(array("輕自動"),"軽自動",$data);
						$data = str_replace(array(",軽自動","軽自動")," 軽自動",$data);
						$data = str_replace(array("合","卦","會"),"合",$data);
						$data = str_replace(array("ＥＮＥｏｓ"),"ＥＮＥＯＳ",$data);
						$data = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data);
						$data = str_replace(array("オ寸","本寸"),"村",$data);
						$data = str_replace(array("言舌"),"話",$data);
						$data = str_replace("」R","ＪＲ",$data);
						$data = str_replace("廿","サ",$data);
						$data = str_replace("巾","市",$data);
						$data = str_replace(array("り叙","り全"),"り金",$data);
						$data = str_replace(array("レ生費","摩生費"),"厚生費",$data);
						$data = str_replace(array("篤期"),"短期",$data);
						$data = str_replace(array("吐長","杜長"),"社長",$data);
						$data = str_replace(array("消費脱","消費説"),"消費税",$data);
						$data = str_replace(array("1名"),"１名",$data);
						//var_dump($data);

						$patterns = array ('/(20\d{2})\/[,.]*(\d{1,2})[,.]*(\d{1,2})/');
						$replace = array ('\1/\2/\3,');
						$data = preg_replace($patterns, $replace, $data);

						$patterns = array ('/([0-9]+)○([0-9]+)/imu');
						$replace = array ('◆\1◆\2◆');
						$data = preg_replace($patterns, $replace, $data);

						$patterns = array ('/([0-9]+)/imu');
						$replace = array ('■\1□');
						$data = preg_replace($patterns, $replace, $data);
						
						$data = str_replace("□月分","月分",$data);
												
						$patterns = array ('/■[0-9]{1,2}□(.?+)/imu');
						$replace = array ('\1');
						$data = preg_replace($patterns, $replace, $data);

						$patterns = array ('/■[0-9]+□(.?+)[■,□]+([0-9]+)[■,□]+([0-9]+)/imu');
						$replace = array ('\1,\2,\3');
						$data = preg_replace($patterns, $replace, $data);
						//var_dump($data);

						$patterns = array ('/,(.),(.),(.),(.)/imu');
						$replace = array ('\1\2\3\4');
						$data = preg_replace($patterns, $replace, $data);

						$patterns = array ('/,(.),(.),(.)/imu');
						$replace = array ('\1\2\3');
						$data = preg_replace($patterns, $replace, $data);


						$patterns = array ('/(\W{1,3}),(\W{1,3})/imu');
						$replace = array ('\1\2');
						$data = preg_replace($patterns, $replace, $data);
						
						$patterns = array ('/(20\d{2})\/[,.]*(\d{1,2})[,.]*(\d{1,2})/');
						$replace = array ('\1/\2/\3,');
						$data2 = preg_replace($patterns, $replace, $data2);

						$patterns = array ('/([0-9]+)○([0-9]+)/imu');
						$replace = array ('◆\1◆\2◆');
						$data2 = preg_replace($patterns, $replace, $data2);
						//var_dump($data);
						$patterns = array ('/([0-9]+)/imu');
						$replace = array ('■\1□');
						$data2 = preg_replace($patterns, $replace, $data2);

						$patterns = array ('/,■([0-9]{1,2})□(.?+)/imu');
						$replace = array ('\1\2');
						$data2 = preg_replace($patterns, $replace, $data2);


						$patterns = array ('/■([0-9]+)□(.?+)[■,□]+([0-9]+)[■,□]+([0-9]+)/imu');
						$replace = array ('\1,\2,\3,\4');
						$data2 = preg_replace($patterns, $replace, $data2);

						$data2 = str_replace(array("■"),",",$data2);
						$data2 = str_replace(array("○","◆","□"),"",$data2);
						$data2 = str_replace(array("△■"),"",$data2);
						$data2 = str_replace(array("…","●","●,●","≒",",●","●,",",,","‥","ノ","/",",て","¶","］","ぺ","て","－\n","－","－","｜","■","□"),"",$data2);
						$data2 = str_replace(array("。",".","．"),"",$data2);
						$data2 = str_replace(array("彳 ,"),"イ",$data2);
						$data = str_replace(array("■"),",",$data);
						$data = str_replace(array("○","■","□","◆","□"),"",$data);
						$data = str_replace(array("△■"),"",$data);						
						
						//▼文字列変換の場所▼
						//$patterns = array("/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/普.*金1{0,1}/imu","/.{1}通.*金1{0,1}/imu","/普.*.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売[\SA-Z\w]{0,3}高/imu","/売[\SA-Z\w]{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/[\SA-Z\w]{1}合/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/石油.*ン代/","/が.*ン代/","/ＳＳ.*リン代/","/カ.{1,2}ン代/","/.{1}録諸費.{1}/","/ＥＮＥ.{2}/","/コン.*タン/","/租.*公課/","/阻.*公課/","/法.{0,1}脱及び住民税/","/民脱/","/市県.{1}税/","/所.{0,1}税/","/所.{0,1}脱/","/所.{0,1}悦/","/所得礎/","/所得悦/","/所得脱/","/唯給/","/雑紿/","/外注加.*費/");
						//$replace = array('車輌費','新聞代','現金','複合','普通預金','普通預金','普通預金','普通預金','売上高','売上高','複合','複合','受取手形','未払金','未払金','石油ガソリン代','ガソリン代','ＳＳガソリン代','ガソリン代','登録諸費用',"ＥＮＥＯＳ","コンサルタン","租税公課","租税公課","法人税及び住民税","民税","市県民税","所得税","所得税","所得税","所得税","所得税","所得税","雑給","雑給","外注加工費");
						//$patterns = array("/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/普.*金1{0,1}/imu","/.{1}通.*金1{0,1}/imu","/普.*.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売.{0,3}高/imu","/売.{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/[\SA-Z\w]{1}合/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/石油.*ン代/","/が.*ン代/","/ＳＳ.*リン代/","/カ.{1,2}ン代/","/.{1}録諸費.{1}/","/ＥＮＥ.{2}/");
						//$replace = array('車輌費','新聞代','現金','複合','普通預金','普通預金','普通預金','普通預金','売上高','売上高','複合','複合','受取手形','未払金','未払金','石油ガソリン代','ガソリン代','ＳＳガソリン代','ガソリン代','登録諸費用',"ＥＮＥＯＳ");
						///$patterns = array("/売.{0,3}高/imu",);
						//$replace = array('売上高');
						//$patterns = array("/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/矜涌附金/");
						//$replace = array('車輌費','新聞代','現金','複合','普通預金');

						//$patterns = array("/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/石油.*ン代/","/が.*ン代/","/ＳＳ.*リン代/","/カ.{1,2}ン代/","/.{1}録諸費.{1}/","/ＥＮＥ.{2}/","/所得礎/","/所得悦/","/所得脱/","/唯給/","/雑紿/","/外注加.*費/");
						//$replace = array('車輌費','新聞代','現金','複合','受取手形','未払金','未払金','石油ガソリン代','ガソリン代','ＳＳガソリン代','ガソリン代','登録諸費用',"ＥＮＥＯＳ","所得税","所得税","所得税","雑給","雑給","外注加工費");

						//$data = preg_replace($patterns, $replace, $data);
						//$data2 = preg_replace($patterns, $replace, $data2);
						//var_dump($data);

						$data = str_replace(array(",-")," ‐ ",$data);
						$data = str_replace(array(",,","ｌ,ｌ","ｌ,ｌ,ｌ"),",",$data);
						$data = str_replace(array(",へ,"),"へ,",$data);
						$data = str_replace(array("－"),"",$data);
						$data = str_replace(array("NTT"),"ＮＴＴ",$data);
						$data = str_replace(array("ＫＤＤ1"),"ＫＤＤＩ",$data);
						$data = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data);
						$data = str_replace(array("食年代"),"食事代",$data);
						$data = str_replace(array("商年"),"商事",$data);
						$data = str_replace(array("ＥＴＣ","ＥＴｃ","耳Ｔｃ"),"ＥＴＣ ",$data);
						$data = str_replace(array("彳"),"イ",$data);
						$data = str_replace(array("¥","$","#","="),"￥",$data);
						$data = str_replace(array(":-"),".",$data);
						$data = str_replace(array("…","●","●,●","≒",",●","●,",",,","‥","ノ","/",",て","¶","］","ぺ","て","－\n","－","－","｜","■","□"),"",$data);
						$data = str_replace(array("現全","現歛"),"現金",$data);
						$data = str_replace("収人","収入",$data);
						$data = str_replace("仕人","仕入",$data);
						$data = str_replace(array("人金","イ昔","償去ｐ"),array("入金","借","償却"),$data);
						
						$data2 = str_replace(array("現全","現歛"),"現金",$data2);
						$data2 = str_replace("収人","収入",$data2);
						$data2 = str_replace("仕人","仕入",$data2);
						$data2 = str_replace(array("レ生費"),"厚生費",$data2);
						$data2 = str_replace(array("篤期"),"短期",$data2);
						$data2 = str_replace(array("吐長","杜長"),"社長",$data2);
						$data2 = str_replace(array("消費脱","消費説"),"消費税",$data2);
						$data2 = str_replace(array("有費","陏費"),"賄費",$data2);
						$data2 = str_replace(array("人金","イ昔","償去ｐ"),array("入金","借","償却"),$data2);
						
						$data = str_replace(array("【【１０】】","【【２０】】","【【３０】】","【【４０】】","【【５０】】","【【６０】】","【【７０】】","【【８０】】","【【９０】】"),array("【【10】】","【【20】】","【【30】】","【【40】】","【【50】】","【【60】】","【【70】】","【【80】】","【【90】】"),$data);
						//var_dump($data);var_dump($data2);exit;exit;
						//$data = mb_convert_encoding( $data, "UTF-8", "SHIFT-JIS");

						//１データとして扱うために分けない。
						$data = explode( "\r", $data );
						$data2 = explode( "\r", $data2 );
						$data3 = explode( "\r", $data3 );
						$data4 = explode( "\r", $data4 );
						//echo "<br>"
						$cnt = count($data);
						$cnt2 = count($data2);
						$cnt3 = count($data3);
						//var_dump($data);

						$pay_date = 0;
						$last_month = 0;
						$account_exist_flag = 0;
						
						for ( $i = 0; $i < $cnt; $i++ ) {
						//1行ごとの処理
							$tax_case = 0;
							$delete_line = 0;
							$account_flag = 0;
							//echo "<br>";
//							echo $data[$i];
//							echo "<br>";
							//echo $filename;
							//echo "<br>";
							

							//▼文字列変換の場所▼
							//$patterns = array("/\?{1}/","/�{1}/","/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/.{1}通.*金1{0,1}/imu","/普.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売[\SA-Z\w]{0,3}高/imu","/売[\SA-Z\w]{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/[\SA-Z\w]{1}合/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/石油.*ン代/","/が.*ン代/","/ＳＳ.*リン代/","/カ.{1,2}ン代/","/.{1}録諸費.{1}/","/ＥＮＥ.{2}/","/コン.*タン.{1}/","/租.*公課/","/阻.*公課/","/法.{0,1}脱及び住民税/","/民脱/","/唯給/","/外注加.*費/");
							//$replace = array('','','車輌費','新代','現金','複合','普通預金','普通預金','普通預金','売上高','売上高','複合','複合','受取手形','未払金','未払金','石油ガソリン代','ガソリン代','ＳＳガソリン代','ガソリン代','登録諸費用',"ＥＮＥＯＳ","コンサルタント","租税公課","租税公課","法人税及び住民税","民税","雑給","外注加工費");
							$patterns = array("/([0-9]{0,4})名/","/内税.*額/","/.{0,1}式会.{0,1}/","/法定福.*費/","/[ぶ]{1}\S{0.2}代/","/\S{0,1}[イト仆]{2}代/","/[ハ八]{0,2}[日目]{1}弋/","/り全/","/預.{1,2}金/","/中輌費/","/新闘代/","/呪金/","/陶.{1}/","/筏.{1}/","/通預企/","/.{0,2}座預金1{0,1}/imu","/普.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売[\SA-Z\w]{0,3}高/imu","/売[\SA-Z\w]{0,3}金/imu","/売[\SA-Z\w]{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/レッスン代/","/[かがカガヵ力]{1}[\S\W]{1,2}リン代/","/ＳＳ.*リン代/","/ＥＮＥ.{2}/","/コス.*油/","/石油.*ン代/","/ＥＮＥＯＳ.*ン代/","/出張費.*ン代/","/.{1}録諸費.{1}/","/コン.*タン/","/租.*公課/","/阻.*公課/","/法.{0,1}脱及び住民税/","/民脱/","/市県.{1}税/","/所.{0,1}税/","/所.{0,1}脱/","/所.{0,1}悦/","/所得礎/","/所得悦/","/所得脱/","/唯給/","/誰給/","/雑紿/","/外注加.*費/","/貸劉/","/繰人/","/貸.*繰入/","/貸.*戻入/","/減.*却費/","/仕.*勘定/","/他人.*勘定/","/什.*勘定/","/他入.*勘定/","/減価.*計額/");
							$replace = array("\1名","内税起票額","株式会社","法定福利費","バイト代","バイト代","バイト代",'り金','預り金','車輌費','新聞代','現金','複合','複合','通預金','当座預金','普通預金','普通預金','売上高','売上高','売掛金','複合','受取手形','未払金','未払金','レッスン代','ガソリン代','ＳＳガソリン代',"ＥＮＥＯＳ","コスモ石油",'石油ガソリン代','ＥＮＥＯＳガソリン代','出張費ガソリン代','登録諸費用',"コンサルタン","租税公課","租税公課","法人税及び住民税","民税","市県民税","所得税","所得税","所得税","所得税","所得税","所得税","雑給","雑給","雑給","外注加工費","貸倒","繰入","貸倒引当金繰入","貸倒引当金戻入","減価償却費","仕入調整勘定","仕入調整勘定","仕入調整勘定","仕入調整勘定","減価償却累計額");

							$data[$i] = preg_replace($patterns, $replace, $data[$i]);
							$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
							$data4[$i] = preg_replace($patterns, $replace, $data4[$i]);
							////echo "<br>";
							$match_tax_num = preg_match("/【【([0-9]{0,6})】】/imu", $data[$i], $match_tax);
							//var_dump($match_tax);
							$sales_tax = "";
							if ($match_tax_num > 0) {
								$sales_tax = $match_tax[1];
							}
							
							$data[$i] = str_replace(array("【【","】】","ガガソリン","S日"),array("   ","","ガソリン","ＳＢＩ"),$data[$i]);
							$data4[$i] = str_replace(array("]","L","！","!"),"1",$data4[$i]);
							$data4[$i] = str_replace(array("Ｉ","！"),"1",$data4[$i]);
							$data4[$i] = str_replace(array("z"),"2",$data4[$i]);
							$data4[$i] = str_replace(array("會","輻"),"3",$data4[$i]);
							$data4[$i] = str_replace(array("繍","ｘ"),"4",$data4[$i]);
							$data4[$i] = str_replace(array("轟","§"),"5",$data4[$i]);
							$data4[$i] = str_replace(array("s"),"6",$data4[$i]);
							$data4[$i] = str_replace(array("讐","Ｓ","B","ｓ","β"),"8",$data4[$i]);
							$data4[$i] = str_replace(array("g","q"),"9",$data4[$i]);
							$patterns = array("/(\D*)([1]{1}[0-2]{1})([1-3]{1}[0-9]{1})([\D ]*)/","/(\D*)([1]{1}[0-2]{1})([0-9]{1})([\D ]*)/","/(\D*)([1-9]{1})([1-3]{1}[0-9]{1})([\D ]*)/");
							$replace = array('\1,\2．\3,\4','\1,\2．\3,\4','\1,\2．\3,\4');
							$data4[$i] = preg_replace($patterns, $replace, $data4[$i]);
/*
							$patterns = array("/複[\SA-Z\w]{1}/","/[\SA-Z\w]{1}合/","/受取.*形/");
							$replace = array('複合','複合','受取手形');
							//$data[$i] = preg_replace("/複＆/imu", '複合', $data[$i]);
							$data[$i] = preg_replace($patterns, $replace, $data[$i]);
							$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
							//$data4[$i] = preg_replace($patterns, $replace, $data4[$i]);
							$accounts_arr = 
								$patterns = array("/売[\SA-Z\w]{0,3}高/","/売[\SA-Z\w]{0,3}鳥/");
								$replace = array('売上高');
								$data[$i] = preg_replace($patterns, $replace, $data[$i]);
								$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
								//$data4[$i] = preg_replace($patterns, $replace, $data4[$i]);
							//echo $data[$i] = preg_replace("/売[\S]{0,3}高/", "売上高", $data[$i]);
*/

						
							$data[$i] = str_replace(array("消耗品費",",濃耗品費",",消粍品費"),"消耗品費,",$data[$i]);
							$data[$i] = str_replace("現金",",現金,",$data[$i]);
							$data[$i] = str_replace("新聞図書費",",新聞図書費,",$data[$i]);
							//$data[$i] = str_replace(array("普通預金"),"普通預金,",$data[$i]);
							$data[$i] = str_replace(array("普通預金"),"普通預金,",$data[$i]);
							$data[$i] = str_replace(array("当座預金"),"当座預金1,",$data[$i]);
							$data[$i] = str_replace("雑費",",雑費,",$data[$i]);
							$data[$i] = str_replace("前受金",",前受金,",$data[$i]);
							$data[$i] = str_replace("水道光熱費",",水道光熱費,",$data[$i]);
							$data[$i] = str_replace("車輌費",",車輌費,",$data[$i]);
							$data[$i] = str_replace("車両費",",車両費,",$data[$i]);
							$data[$i] = str_replace("預り金",",預り金,",$data[$i]);
							$data[$i] = str_replace("預かり金",",預かり金,",$data[$i]);
							$data[$i] = str_replace("修繕費",",修繕費,",$data[$i]);
							$data[$i] = str_replace("福利厚生費",",福利厚生費,",$data[$i]);
							$data[$i] = str_replace("前払費用",",前払費用,",$data[$i]);
							$data[$i] = str_replace(array("雑給",",唯給"),"雑給,",$data[$i]);
							$data[$i] = str_replace("旅費交通費",",旅費交通費,",$data[$i]);
							$data[$i] = str_replace("広告宣伝費",",広告宣伝費,",$data[$i]);
							$data[$i] = str_replace(array("複合",",複ａ"),"複合,",$data[$i]);
							$data[$i] = str_replace("仕入金額",",仕入金額,",$data[$i]);
							$data[$i] = str_replace("会議費",",会議費,",$data[$i]);
							$data[$i] = str_replace("事業主借入金",",事業主借入金,",$data[$i]);
							$data[$i] = str_replace("事業主貸付金",",事業主貸付金,",$data[$i]);
							$data[$i] = str_replace("事業主貸",",事業主貸,",$data[$i]);
							$data[$i] = str_replace("事業主借",",事業主借,",$data[$i]);
							$data[$i] = str_replace("地代家賃",",地代家賃,",$data[$i]);
							$data[$i] = str_replace("交際接待費",",交際接待費,",$data[$i]);
							$data[$i] = str_replace("保険料",",保険料,",$data[$i]);
							$data[$i] = str_replace("通信費",",通信費,",$data[$i]);
							$data[$i] = str_replace(array("租税公課","徂税公課","阻税効果","柤税公課","阻脱公課","柤税公課"),",租税公課,",$data[$i]);
							$data[$i] = str_replace(array("未払消費税","末払消費税"),",未払消費税,",$data[$i]);
							$data[$i] = str_replace("法定福利費",",法定福利費,",$data[$i]);
							$data[$i] = str_replace(array("諸会費","睹会費"),",諸会費,",$data[$i]);
							$data[$i] = str_replace("支払手数料",",支払手数料,",$data[$i]);
							$data[$i] = str_replace("期首棚卸高",",期首棚卸高,",$data[$i]);
							$data[$i] = str_replace("棚卸資産",",棚卸資産,",$data[$i]);
							$data[$i] = str_replace("買掛金",",買掛金,",$data[$i]);
							$data[$i] = str_replace(array("未払金","末払金"),",未払金,",$data[$i]);
							$data[$i] = str_replace("給料賃金",",給料賃金,",$data[$i]);
							$data[$i] = str_replace("減価償却費",",減価償却費,",$data[$i]);
							$data[$i] = str_replace("減価償却累計額",",減価償却累計額,",$data[$i]);
							$data[$i] = str_replace("仮受消費税",",仮受消費税,",$data[$i]);
							$data[$i] = str_replace("受取手形",",受取手形,",$data[$i]);
							$data[$i] = str_replace(array("売上高","売上商"),",売上高,",$data[$i]);
							$data[$i] = str_replace("仮払消費税",",仮払消費税,",$data[$i]);
							$data[$i] = str_replace("売掛金",",売掛金,",$data[$i]);
							$data[$i] = str_replace("ブイシージー勘定",",ブイシージー勘定,",$data[$i]);
							$data[$i] = str_replace("仮受金",",仮受金,",$data[$i]);
							$data[$i] = str_replace("仮払金",",仮払金,",$data[$i]);
							$data[$i] = str_replace("定期預金",",定期預金,",$data[$i]);
							$data[$i] = str_replace("定期積金",",定期積金,",$data[$i]);
							$data[$i] = str_replace(array("定期積金,返済","定期積金,解約"),array(",定期積金返済",",定期積金解約"),$data[$i]);
							$data[$i] = str_replace("貸倒引当金","貸倒引当金,",$data[$i]);
							$data[$i] = str_replace(array("貸倒引当金戻入益","貸倒引当金,戻入益"),",貸倒引当金戻入益,",$data[$i]);
							$data[$i] = str_replace(array("貸倒引当金繰入額","貸倒引当金,繰入額"),",貸倒引当金繰入額,",$data[$i]);
							$data[$i] = str_replace("商品仕入高",",商品仕入高,",$data[$i]);
							$data[$i] = str_replace("本支店勘定",",本支店勘定,",$data[$i]);
							$data[$i] = str_replace("受取手数料",",受取手数料,",$data[$i]);
							$data[$i] = str_replace(array("未払費用","末払費用"),",未払費用,",$data[$i]);
							$data[$i] = str_replace("厚生費",",厚生費,",$data[$i]);
							$data[$i] = str_replace(array("未収入金","末収入金"),",未収入金,",$data[$i]);
							$data[$i] = str_replace("繰延資産償却","繰延資産償却,",$data[$i]);
							$data[$i] = str_replace("受取家賃",",受取家賃,",$data[$i]);
							$data[$i] = str_replace(array("立替金","彑替金"),",立替金,",$data[$i]);
							$data[$i] = str_replace("社内仕入",",社内仕入,",$data[$i]);
							$data[$i] = str_replace("棚卸高",",棚卸高,",$data[$i]);
							$data[$i] = str_replace("役員報酬",",役員報酬,",$data[$i]);
							$data[$i] = str_replace("給与手当",",給与手当,",$data[$i]);
							$data[$i] = str_replace("権利金",",権利金,",$data[$i]);
							$data[$i] = str_replace(array("法人税住民税及び事業税","法人税，住民税及び事業税"),",法人税住民税及び事業税,",$data[$i]);
							$data[$i] = str_replace("法人税等充当金",",法人税等充当金,",$data[$i]);
							$data[$i] = str_replace("未払法人税等",",未払法人税等,",$data[$i]);
							$data[$i] = str_replace("差入保証金",",差入保証金,",$data[$i]);
							$data[$i] = str_replace("保険積立金",",保険積立金,",$data[$i]);
							$data[$i] = str_replace("支払利息割引料",",支払利息・割引料,",$data[$i]);
							
							$data[$i] = str_replace("専従者給与",",専従者給与,",$data[$i]);
							$data[$i] = str_replace("支払利息",",支払利息,",$data[$i]);
							$data2[$i] = str_replace("支払利息,割引料",",支払利息・割引料,",$data2[$i]);
							$data[$i] = str_replace("賃借料",",賃借料,",$data[$i]);
							$data[$i] = str_replace("雑捐失",",雑捐失,",$data[$i]);
							$data[$i] = str_replace("雑収入",",雑収入,",$data[$i]);
							$data[$i] = str_replace("外注加工費",",外注加工費,",$data[$i]);
							$data[$i] = str_replace("固定資産除却損",",固定資産除却損,",$data[$i]);
							$data[$i] = str_replace("仕入調整勘定",",仕入調整勘定,",$data[$i]);
							$data[$i] = str_replace("仕掛工事",",仕掛工事,",$data[$i]);
							$data[$i] = str_replace("法人税及び住民税",",法人税及び住民税,",$data[$i]);
							$data[$i] = str_replace("工具器具備品",",工具器具備品,",$data[$i]);
							$data[$i] = str_replace("車両運搬具",",車両運搬具,",$data[$i]);
							$data[$i] = str_replace("車輌運搬具",",車輌運搬具,",$data[$i]);
							$data[$i] = str_replace("退職金共済掛金",",退職金共済掛金,",$data[$i]);
							$data[$i] = str_replace(array("事務用品費","事務用乱費"),",事務用品費,",$data[$i]);
							$data[$i] = str_replace(array("賄費"),",賄費,",$data[$i]);
							$data[$i] = str_replace(array("販売促進費"),",販売促進費,",$data[$i]);
							$data[$i] = str_replace(array("寄付金"),",寄付金,",$data[$i]);
							$data[$i] = str_replace(array("運賃"),",運賃,",$data[$i]);
							$data[$i] = str_replace(array("投資有価証券"),",投資有価証券,",$data[$i]);
							$data[$i] = str_replace(array("出資金"),",出資金,",$data[$i]);
							$data[$i] = str_replace(array("保険積立金"),",保険積立金,",$data[$i]);
							$data[$i] = str_replace(array("長期前払費用"),",長期前払費用,",$data[$i]);
							$data[$i] = str_replace(array("準備金"),",準備金,",$data[$i]);
							$data[$i] = str_replace(array("積立金"),",積立金,",$data[$i]);
							$data[$i] = str_replace(array("収入"),",収入,",$data[$i]);
							$data[$i] = str_replace(array("積立額"),",積立額,",$data[$i]);
							$data[$i] = str_replace(array("準備金,積立額"),",準備金積立額,",$data[$i]);
							$data[$i] = str_replace(array("交際費","交際費,"),",交際費,",$data[$i]);
							$data[$i] = str_replace(array("長期借入金","長期借人金"),",長期借入金,",$data[$i]);							$data[$i] = str_replace(array("借入金","佶入金","借人金","借大金"),",借入金,",$data[$i]);
							$data[$i] = str_replace(array("借入金,返済"),",借入金返済",$data[$i]);
							$data[$i] = str_replace(array("外注費"),",外注費,",$data[$i]);
							$data[$i] = str_replace(array("リース料"),",リース料,",$data[$i]);
							$data[$i] = str_replace(array("リース負債"),",リース負債,",$data[$i]);
							$data[$i] = str_replace(array("原材料仕入高"),",原材料仕入高,",$data[$i]);
							$data[$i] = str_replace(array("完成工事高"),",完成工事高,",$data[$i]);
							
							
							$data[$i] = str_replace("現金,過",",現金過",$data[$i]);
							$data[$i] = str_replace("現金,不足",",現金不足",$data[$i]);
							$data[$i] = str_replace("現金,売上",",現金売上",$data[$i]);
							$data[$i] = str_replace("現金,仕入",",現金仕入",$data[$i]);
							
							$data[$i] = str_replace("送金,料",",送金料",$data[$i]);

							$data2[$i] = str_replace(array("消耗品費","濃耗品費","消粍品費"),",消耗品費,",$data2[$i]);
							$data2[$i] = str_replace("現金",",現金,",$data2[$i]);
							$data2[$i] = str_replace("新聞図書費",",新聞図書費,",$data2[$i]);
							//$data2[$i] = str_replace("普通預金",",普通預金,",$data2[$i]);
							$data2[$i] = str_replace("普通預金",",普通預金,",$data2[$i]);
							$data2[$i] = str_replace(array("当座預金"),"当座預金1,",$data2[$i]);
							$data2[$i] = str_replace("雑費",",雑費,",$data2[$i]);
							$data2[$i] = str_replace("前受金",",前受金,",$data2[$i]);
							$data2[$i] = str_replace("水道光熱費",",水道光熱費,",$data2[$i]);
							$data2[$i] = str_replace("車輌費",",車輌費,",$data2[$i]);
							$data2[$i] = str_replace("車両費",",車両費,",$data2[$i]);
							$data2[$i] = str_replace("退職金共済掛金",",退職金共済掛金,",$data2[$i]);
							$data2[$i] = str_replace("預り金",",預り金,",$data2[$i]);
							$data2[$i] = str_replace("預かり金",",預かり金,",$data2[$i]);
							$data2[$i] = str_replace("修繕費",",修繕費,",$data2[$i]);
							$data2[$i] = str_replace("福利厚生費",",福利厚生費,",$data2[$i]);
							$data2[$i] = str_replace("前払費用",",前払費用,",$data2[$i]);
							$data2[$i] = str_replace(array("雑給","唯給"),",雑給,",$data2[$i]);
							$data2[$i] = str_replace("旅費交通費",",旅費交通費,",$data2[$i]);
							$data2[$i] = str_replace("広告宣伝費",",広告宣伝費,",$data2[$i]);
							$data2[$i] = str_replace(array("複合","複ａ"),",複合,",$data2[$i]);
							$data2[$i] = str_replace(array("仕入金額","仕人金額","仕大金額"),",仕入金額,",$data2[$i]);
							$data2[$i] = str_replace("会議費",",会議費,",$data2[$i]);
							$data2[$i] = str_replace("事業主借入金",",事業主借入金,",$data2[$i]);
							$data2[$i] = str_replace("事業主貸付金",",事業主貸付金,",$data2[$i]);
							$data2[$i] = str_replace("事業主貸",",事業主貸,",$data2[$i]);
							$data2[$i] = str_replace("事業主借",",事業主借,",$data2[$i]);
							$data2[$i] = str_replace("地代家賃",",地代家賃,",$data2[$i]);
							$data2[$i] = str_replace("交際接待費",",交際接待費,",$data2[$i]);
							$data2[$i] = str_replace("保険料",",保険料,",$data2[$i]);
							$data2[$i] = str_replace("通信費",",通信費,",$data2[$i]);
							$data2[$i] = str_replace(array("租税公課","徂税公課","阻税効果","柤税公課","阻脱公課","柤税公課"),",租税公課,",$data2[$i]);
							$data2[$i] = str_replace(array("未払消費税","末払消費税"),",未払消費税,",$data2[$i]);
							$data2[$i] = str_replace("法定福利費",",法定福利費,",$data2[$i]);
							$data2[$i] = str_replace(array("諸会費","睹会費"),",諸会費,",$data2[$i]);
							$data2[$i] = str_replace("支払手数料",",支払手数料,",$data2[$i]);
							$data2[$i] = str_replace("期首棚卸高",",期首棚卸高,",$data2[$i]);
							$data2[$i] = str_replace("棚卸資産",",棚卸資産,",$data2[$i]);
							$data2[$i] = str_replace("買掛金",",買掛金,",$data2[$i]);
							$data2[$i] = str_replace(array("未払金","末払金"),",未払金,",$data2[$i]);
							$data2[$i] = str_replace("給料賃金",",給料賃金,",$data2[$i]);
							$data2[$i] = str_replace("減価償却費",",減価償却費,",$data2[$i]);
							$data2[$i] = str_replace("減価償却累計額",",減価償却累計額,",$data2[$i]);
							$data2[$i] = str_replace("仮受消費税",",仮受消費税,",$data2[$i]);
							$data2[$i] = str_replace(array("売上高","売上商"),",売上高,",$data2[$i]);
							$data2[$i] = str_replace("仮払消費税",",仮払消費税,",$data2[$i]);						
							$data2[$i] = str_replace("売掛金",",売掛金",$data2[$i]);
							$data2[$i] = str_replace("ブイシージー勘定",",ブイシージー勘定,",$data2[$i]);
							$data2[$i] = str_replace("仮受金",",仮受金,",$data2[$i]);
							$data2[$i] = str_replace("仮払金",",仮払金,",$data2[$i]);
							$data2[$i] = str_replace("定期預金",",定期預金,",$data2[$i]);
							$data2[$i] = str_replace("定期積金",",定期積金,",$data2[$i]);
							$data2[$i] = str_replace("貸倒引当金",",貸倒引当金,",$data2[$i]);
							$data2[$i] = str_replace(array("貸倒引当金戻入益","貸倒引当金,戻入益"),",貸倒引当金戻入益,",$data2[$i]);
							$data2[$i] = str_replace(array("貸倒引当金繰入額","貸倒引当金,繰入額"),",貸倒引当金繰入額,",$data2[$i]);
							$data2[$i] = str_replace("商品仕入高",",商品仕入高,",$data2[$i]);
							$data2[$i] = str_replace("本支店勘定",",本支店勘定,",$data2[$i]);
							$data2[$i] = str_replace("受取手数料",",受取手数料,",$data2[$i]);
							$data2[$i] = str_replace(array("未払費用","末払費用"),",未払費用,",$data2[$i]);
							$data2[$i] = str_replace("厚生費",",厚生費,",$data2[$i]);
							$data2[$i] = str_replace(array("未収入金","末収入金"),",未収入金,",$data2[$i]);
							$data2[$i] = str_replace("繰延資産償却",",繰延資産償却,",$data2[$i]);
							$data2[$i] = str_replace("受取家賃",",受取家賃,",$data2[$i]);
							$data2[$i] = str_replace(array("立替金","彑替金"),",立替金,",$data2[$i]);
							$data2[$i] = str_replace("社内仕入",",社内仕入,",$data2[$i]);
							$data2[$i] = str_replace("棚卸高",",棚卸高,",$data2[$i]);
							$data2[$i] = str_replace("役員報酬",",役員報酬,",$data2[$i]);
							$data2[$i] = str_replace("給与手当",",給与手当,",$data2[$i]);
							$data2[$i] = str_replace("権利金",",権利金,",$data2[$i]);
							$data2[$i] = str_replace("法人税住民税及び事業税",",法人税住民税及び事業税,",$data2[$i]);
							$data2[$i] = str_replace("法人税等充当金",",法人税等充当金,",$data2[$i]);
							$data2[$i] = str_replace("未払法人税等",",未払法人税等,",$data2[$i]);
							$data2[$i] = str_replace("差入保証金",",差入保証金,",$data2[$i]);
							$data2[$i] = str_replace("保険積立金",",保険積立金,",$data2[$i]);
							$data2[$i] = str_replace("支払利息割引料",",支払利息・割引料,",$data2[$i]);
							
							$data[$i] = str_replace("専従者給与",",専従者給与,",$data[$i]);
							$data2[$i] = str_replace("支払利息",",支払利息,",$data2[$i]);
							$data2[$i] = str_replace("支払利息,割引料",",支払利息・割引料,",$data2[$i]);
							$data2[$i] = str_replace("賃借料",",賃借料,",$data2[$i]);
							$data2[$i] = str_replace("雑捐失",",雑捐失,",$data2[$i]);
							$data2[$i] = str_replace("外注加工費",",外注加工費,",$data2[$i]);
							$data2[$i] = str_replace("固定資産除却損",",固定資産除却損,",$data2[$i]);
							$data2[$i] = str_replace("仕入調整勘定",",仕入調整勘定,",$data2[$i]);
							$data2[$i] = str_replace("仕掛工事",",仕掛工事,",$data2[$i]);
							$data2[$i] = str_replace("雑収入",",雑収入,",$data2[$i]);
							$data2[$i] = str_replace("法人税及び住民税",",法人税及び住民税,",$data2[$i]);
							$data2[$i] = str_replace("工具器具備品",",工具器具備品,",$data2[$i]);
							$data2[$i] = str_replace("車両運搬具",",車両運搬具,",$data2[$i]);
							$data2[$i] = str_replace("車輌運搬具",",車輌運搬具,",$data2[$i]);
							$data2[$i] = str_replace("賄費",",賄費,",$data2[$i]);
							$data2[$i] = str_replace(array("販売促進費"),",販売促進費,",$data2[$i]);
							$data2[$i] = str_replace(array("寄付金"),",寄付金,",$data2[$i]);
							$data2[$i] = str_replace(array("運賃"),",運賃,",$data2[$i]);
							$data2[$i] = str_replace(array("投資有価証券"),",投資有価証券,",$data2[$i]);
							$data2[$i] = str_replace(array("出資金"),",出資金,",$data2[$i]);
							$data2[$i] = str_replace(array("積立金"),",積立金,",$data2[$i]);
							$data2[$i] = str_replace(array("長期前払費用"),",長期前払費用,",$data2[$i]);

							$data2[$i] = str_replace(array("交際費","交際費,"),",交際費,",$data2[$i]);
							$data2[$i] = str_replace(array("借入金","佶入金","借人金","借大金"),",借入金,",$data2[$i]);
							$data2[$i] = str_replace(array("借入金,返済"),",借入金返済",$data2[$i]);
							$data2[$i] = str_replace(array("外注費"),",外注費,",$data2[$i]);
							$data2[$i] = str_replace(array("リース料"),",リース料,",$data2[$i]);
							$data2[$i] = str_replace(array("リース負債"),",リース負債,",$data2[$i]);
							$data2[$i] = str_replace(array("原材料仕入高"),",原材料仕入高,",$data2[$i]);
							$data2[$i] = str_replace(array("完成工事高"),",完成工事高,",$data2[$i]);
							$data2[$i] = str_replace(array("準備金"),",準備金,",$data2[$i]);
							$data2[$i] = str_replace(array("積立金"),",積立金,",$data2[$i]);
							$data2[$i] = str_replace(array("収入"),",収入,",$data2[$i]);
							$data2[$i] = str_replace(array("積立額"),",積立額,",$data2[$i]);
							$data2[$i] = str_replace(array("準備金,積立額"),",準備金積立額,",$data2[$i]);
							
							$data2[$i] = str_replace("現金,過",",現金過",$data2[$i]);
							$data2[$i] = str_replace("現金,不足",",現金不足",$data2[$i]);
							$data2[$i] = str_replace("現金,売上",",現金売上",$data2[$i]);
							$data2[$i] = str_replace("現金,仕入",",現金仕入",$data2[$i]);
							$data2[$i] = str_replace("送金,料",",送金料",$data2[$i]);


							//echo "ここから<br>";
							//var_dump($data2[$i]);
							//var_dump($journalizing_matching_arr);
							//echo "ここまで<br>";
							//$data[$i] = $journalizing_matching_arr[0];
							//echo "<br>";
							//▲文字列変換の場所▲
							
							
							//echo $data[$i];echo " 1<br>";//摘要判定,金額
							
							//echo $data2[$i];echo " 2<br>";//科目判定
							//echo $data4[$i];echo " 4<br>";//日付判定
if ($i == 200) { //exit;
}
							$data[$i] = str_replace(",,",",",$data[$i]);
							//echo " aaa<br>";
							$data2[$i] = str_replace(",,",",",$data2[$i]);
							$data3[$i] = str_replace(",,",",",$data3[$i]);
							$data4[$i] = str_replace(",,",",",$data4[$i]);
							if ($i <= 15 && $first_cnt == 0 && $account_exist_flag == 0) {
								unset($account);
								$account_line = str_replace(",","",$data2[$i]);
								$this_account_name_arr = explode(",",$account_line);
								//echo "科目判定";var_dump($this_account_name_arr);
								//if ($this_account_name_arr[2]== "") {
								$ac_name = "";
								$account_name = "";
								//preg_match("/^,([0-9]{4})/",$data2[$i],$account);
								if ($this_account_name_arr[1] != NULL) {
								//echo "a";
									preg_match("/^([,]{0,1}[0-9]{3,4})/",$this_account_name_arr[1],$account);
									$ac_name = $this_account_name_arr[1];
								} else if ($this_account_name_arr[0] != "") {
								//echo "b";
									preg_match("/^([,]{0,1}[0-9]{3,4})/",$this_account_name_arr[0],$account);
									$ac_name = $this_account_name_arr[0];
								}
								//echo "<br>";
								//echo "科目array ";
								//echo "acnarr";
								//var_dump($account_name_arr);
								//echo "ac";
								//var_dump($account);
								if ($account[0] != ""){
									$account_name = preg_replace("/^([0-9]{3,4})/","\1,",$ac_name);
									//var_dump($account_name);echo "科目名";
									$com_match_flag = 0;
									$com_name_arr = array("株式会社","有限会社","株式","有限","事務所","会社","相手科目","貸方借方",$com_name);
									for ($m = 0;$m < count($com_name_arr);$m++) {
										//echo "/".$com_name_arr[$m]."/<br>";
										$com_match_num = preg_match("/".$com_name_arr[$m]."/",$account_name,$com_match);
										if ($com_match_num == 1) {
											//echo "com_match_flag";
											$com_match_flag = 1;
											break;
										}
									}
									//var_dump($com_match);
									if ($com_match_flag == 0) {
										//echo "this_account_name:";//var_dump($code_arr);
										//var_dump($account_name);
										$account_num = $code_arr[$account[1]];
										$account_name = str_replace(array(",",",","，","2","3","4","5","6","7","8","9"),"",$account_name);
										$account_name = str_replace(array("普通預金1","借?"),array("普通預金","借入金"),$account_name);
										//echo "<br>";
										$account_flag = 1;
										$account_exist_flag = 1;
									}
								}
								//}
								
							}

							//勘定科目を取得する
							$journalizing_arr = array(
								"複合",
								"消耗品費",
								"新聞図書費",
								"普通預金",
								"普通預金",
								"当座預金1",
								"当座預金",
								"雑費",
								"水道光熱費",
								"車輌費",
								"車両費",
								"諸費用預かり金",
								"諸費用預り金",
								"預り金",
								"修繕費",
								"福利厚生費",
								"雑給",
								"旅費交通費",
								"広告宣伝費",
								"仕入金額",
								"会議費",
								"事業主貸",
								"事業主借",
								"地代家賃",
								"交際接待費",
								"保険料",
								"通信費",
								"前受金",
								"租税公課",
								"寄付金",
								"未払消費税",
								"法定福利費",
								"諸会費",
								"支払手数料",
								"期首棚卸高",
								"棚卸資産",
								"買掛金",
								"未払金",
								"給料賃金",
								"減価償却費",
								"減価償却累計額",
								"仮受消費税",
								"売上高1",
								"売上高",
								"仮払消費税",
								"現金",
								"売掛金",
								"ブイシージー勘定",
								"仮受金",
								"仮払金",
								"定期預金",
								"定期積金",
								"賃貸料収入",
								"供託金",
								"投資有価証券",
								"出資金",
								"保険積立金",
								"長期前払費用",
								"貸倒引当金戻益",
								"貸倒引当金戻入益",
								"貸倒引当金繰入額",
								"貸倒引当金繰入",
								"貸倒引当金",
								"商品仕入高",
								"本支店勘定",
								"受取手数料",
								"未払費用",
								"受託収入",
								"利益準備金積立額",
								"福利厚生費",
								"厚生費",
								"退職金共済掛金",
								"支払利息・割引料",
								"支払利息",
								"未収入金",
								"現金仕入",
								"繰延資産償却",
								"受取家賃",
								"受取手形",
								"差入保証金",
								"商品",
								"立替金",
								"社内仕入",
								"期末棚卸高",
								"期首仕掛品棚卸高",
								"期末仕掛品棚卸高",
								"期首原材料棚卸高",
								"期末原材料棚卸高",
								"固定資産除却損",
								"役員報酬",
								"給与手当",
								"権利金",
								"法人税,住民税及び事業税",
								"法人税等充当金",
								"未払法人税等",
								"仕入調整勘定",
								"リース負債",
								"保険積立金",
								"雑損失",
								"賃借料",
								"外注加工費",
								"仕掛工事",
								"工具器具備品",
								"車両運搬具",
								"車輌運搬具",
								"工具器具備品",
								"リース資産",
								"事務用品費",
								"建物",
								"構築物",
								"機械装置",
								"完成工事高",
								"法人税及び住民税",
								"長期借入金",
								"役員借入金",
								"事業主借入金",
								"事業主貸付金",
								"事業主貸",
								"専従者給与",
								"荷造運賃",
								"運賃",
								"前払費用",
								"賄費",
								"交際費",
								"雑収入",
								"外注費",
								"運賃",
								"リース料",
								"原材料仕入高",
								"利益準備金",
								"別途積立金",
								"測量設計収入",
								"複ａ",
								"複",
								"信費",
								"合",
								//"勘定",
								//".*金",
								".*費"
							);
							$other_arr = array(
							"月計",
							"累計",
							"繰越",
							"相手科目",
							"計※※",
							"※",
							"内税起票額",
							"税起票"
							);
							$p_cnt = 0;
							$match = "";
							$account_data = "";
							$sum_month = 0;
							$match_num1 = 0;
							$match_num2 = 0;
							$match_num = 0;
							$other_flag = 0;
							$journalizing_match_flag = 0;
							for ($k = 0;$k < count($other_arr);$k++) {
								//dataから月計や累計や繰越を排除する。
								$pattern_comp_end = "/".$other_arr[$k]."/im";//echo "　ここまで<br>";
								$match_num2 = preg_match($pattern_comp_end, $data2[$i], $match2);
								//var_dump($match2);
								if ($match_num2 > 0) {
									$other_flag = 1;
								;}
								if ($match2[0] == "月計") {$sum_month = 1;}
							}//for ($k = 0;$k < count($other_arr);$k++)の終わり
							for ($z = 0;$z < count($journalizing_arr)-1;$z++) {
								//科目を取得できた行の処理
								//echo "ここから　".$data2[$i]."   →　　".$data4[$i];
								//data2から日付と科目のセットを取り出す。
								$pattern_comp_end = "/([0-9]{0,2}．[0-9]{0,2})(".$journalizing_arr[$z].")/im";//echo "　ここまで<br>";
								$match_num = preg_match($pattern_comp_end, $data4[$i], $match);
								//var_dump($match);echo "日付";
								
								$account_data = str_replace(array(", ","."," ","。","．","|","'","`"),"",$data2[$i]);
								
								//var_dump($account_data);
								//dataから科目を取り出す。
								$pattern_comp_2 = "/".$journalizing_arr[$z]."/im";//echo "　ここまで<br>";
								$match_num1 = preg_match($pattern_comp_2, $account_data, $match1);
								$match1[0] = str_replace(array("普通預金1",","),array("普通預金",""),$match1[0]);
								//$match_num1;echo ":m-num";
								//var_dump($match1);
								/*
								//dataから摘要と金額を取り出す。
								$pattern_comp_end = "/".$journalizing_arr[$z]."/im";//echo "　ここまで<br>";
								$match_num2 = preg_match($pattern_comp_end, $data[$i], $match1);
								*/
								//echo strlen($data[$i]);echo "strlen<br>m2-";echo $other_flag;
								//echo "<br>";
								
								if ($match_num1 >= 1 || (strlen($data[$i]) > 20 && $other_flag == 0 && $z == count($journalizing_arr) - 1)) {
								//if ($match_num1 >= 1) {
									$journalizing_match_flag = 1;

									//$company_arr[$p_cnt] = $match[0];
									//echo "<br>仕訳候補<br/>\r\n";
									//var_dump($match1);
									//echo $data[$i];
									//echo " abc ";
									//echo $data2[$i];
									//var_dump($match1);
									
									
									if ($sum_month == 1) {
									//echo "月";
										$last_month = $last_month + 1;
									}
									
									$date_arr =  explode(",",str_replace(array(".","．","。"),",",$match[1]));

									if ($date_arr[0] > 12) {
										$date_arr[0] = substr($date_arr[0],-1,1);
									}
									if ($date_arr[1] > 31) {
										$date_arr[1] = substr($date_arr[1],-2,1);
									}
									//var_dump($date_arr);
									
									//echo $inner_num = substr(str_replace(array(".","．","。"),"",$match[1]),-3,3);
									
									
									
									if (($date_arr[0] != "" && $date_arr[0] <=12)  && $date_arr[1] <= 31 && $date_arr[2] <= 31) {
										if (isset($date_arr[2])) {
											$inner_num = $date_arr[1]."/".$date_arr[2];
											$last_month = $date_arr[1];
											if ($date_arr[2] != "") {
												$new_date = $date_arr[2];
											}
										} else if ($date_arr[0] == 0){
											$inner_num = $last_month."/".$last_date;
											if (isset($date_arr[1])) {//echo "bbxxbb";
												$new_date = $date_arr[1];
											}
										} else {
											$inner_num = $date_arr[0]."/".$date_arr[1];
											$last_month = $date_arr[0];
											if ($date_arr[1] != "") {
												$new_date = $date_arr[1];
											}
										}

										//var_dump($date_arr);
										if ($new_date <= 31) {
											if ($new_date == "") {//echo "abcd<br>";
												$pay_date = $inner_num;
											} else {//echo "xyz<br>";
												$last_date = $new_date;//echo "a<br>";
												$pay_date = $inner_num;//echo "b<br>";
											}
										} else {
											$pay_date = $last_month."/".$last_date;
										}
									
									
									}
									
									//echo $last_date;echo "lastdate<br>";
									//echo $new_date;echo "newdate<br>";
									//if ($last_date < $new_date && $new_date <= 31) {

									$filename = str_replace("txt","jpg",$filename);
									$pattern = '/^,/i';
									$replacement = '';
									$data[$i] = preg_replace($pattern, $replacement, $data[$i]);
									$data_arr = explode(",",$data[$i]);
									$date_arr = explode("/",$pay_date);
									if ($_REQUEST["y"] == "") {
										$year = $fiscal_year;
									} else {
											$year = $_REQUEST["y"];
									}
									//var_dump($date_arr);
									
									$month = $date_arr[0];
									$day = $date_arr[1];
									
									if ($month >= 1 && $month <= $financial_month) {
										$year += 1;
									}
									
									//echo "expdataarr:";
									//var_dump($data_arr);echo $filename;
									//echo "個数:".count($data_arr)." ";
									$last_num = 0;
									
									$last_num = count($data_arr) - 1;
									//echo "abc";
									if ($data_arr[$last_num] == "") {//echo "xyz";
										$last_num = count($data_arr) - 2;
									}
									
									if ( $data_arr[1] == "内税起票額") {
										$delete_line = 1;
										$tax_case = 1;
										$remarks1 = $data_arr[1]." ".$data_arr[2];//echo "<br>";
										$amount_of_money = $data_arr[3] * 1;//echo "<br>";
										if ($last_num != 4) {
											$amount_of_money2 = $data_arr[4] * 1;//echo "<br>";
										}
											$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
										
									} else if ( $data_arr[1] == "雑損失") {
										$tax_case = 1;
										$remarks1 = $data_arr[1];//echo "<br>";
										$amount_of_money = $data_arr[$last_num - 1] * 1;//echo "<br>";
										$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
										
									} else {
										if (ctype_digit($data_arr[1])){
												$remarks1 = $data_arr[0];//echo "<br>";
												$amount_of_money = $data_arr[1] * 1;//echo "<br>";
												if ($last_num != 1) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
										} else {
											if (ctype_digit($data_arr[5])) {
												$remarks1 = $data_arr[1]." ".$data_arr[2];//echo "<br>";
												$amount_of_money = $data_arr[3] * 1;//echo "<br>";
												if ($last_num != 3) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											} else if (ctype_digit($data_arr[2])) {
												$remarks1 = $data_arr[1];//echo "<br>";
												$amount_of_money = $data_arr[2] * 1;//echo "<br>";
												if ($last_num != 2) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											} else {
												if ($data_arr[2] == "へ") {$data_arr[2] = $data_arr[1].$data_arr[2];}
													$remarks1 = $data_arr[2];//echo "<br>";
													$amount_of_money = $data_arr[$last_num - 1] * 1;//echo "<br>";
												if ($last_num != 3) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											}
										}
									}
									if ($match1[0] == "複" || $match1[0] == "合" || $match1[0] == "複ａ" ) {
										$match1[0] = "複合";
									}
									if ($match1[0] == "現") {
										$match1[0] = "現金";
									}
									if ($first_cnt == 0 ) {
										//if ($account_flag == 0) {
											//$fixed_data .= $match1[0].",".$pay_date.",".$data[$i].",".$filename.",".$account_num."\r";
									//		$fixed_data .= ",,".$amount_of_money.",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum."\r";
											
										//}
										$first_cnt = 1;
									}// else {
									
										$str = "";
/*
									    $options = array('-d', '/usr/local/lib/mecab/dic/mecab-ipadic-neologd');
									    $mecab = new MeCab_Tagger($options);
									    $nodes = $mecab->parseToNode($remarks1);

									    foreach ($nodes as $n) {
									        echo $str .=$n -> getSurface();
									    	$n -> getFeature()." ";
									    }
*/										//"UTF-8", "SHIFT-JIS"
										/*
										echo mb_detect_encoding($remarks1);
										//echo $remarks1 = mb_convert_encoding($remarks1, "EUC-JP", "UTF-8");
										0//echo $remarks1 = mb_convert_encoding($remarks1, "EUC-JP", "UTF-8");
										echo "<br><br><br>";
										$arg_yomi = array('-O' => 'yomi');
										$mecab = new MeCab_Tagger($arg_yomi);
										echo $str = $mecab -> parse($remarks1);
										mecab_destroy($mecab);
										// カタカナででてくるので、ひらがなへ変換。utf-8を指定しないとひらがなになってくれない
										echo  $str = mb_convert_kana($str, "c", "utf-8");
										echo "<br><br><br>";
										*/
										/*
										echo $ryakugo = $kana2romaji -> convert($str);
										echo "<br><br><br>";
										*/
										if ($output == 1) {
											if ($account_flag == 0 && $delete_line != 1 && $other_flag != 1 && ($remarks1 != "" || $match1[0] != "")) {
												//echo $account_name;echo " ->  ";echo $line_sum;
												$fixed_data .= ",,".$remarks1.",,,".$match1[0].",,,".($amount_of_money * 1).",".$day.",".$month.",".$year.",,".$account_name.",,,".$filename.",,".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												if ($tax_case == 1) {
													//$fixed_data .= ",,".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";
													//*$fixed_data .= ",".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";											
													$fixed_data .= ",,".$remarks1.",,,".$match1[0].",,,".($amount_of_money2 * 1).",".$day.",".$month.",".$year.",,".$account_name.",,,".$filename.",,".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												}
											}

										} else {
											if ($account_flag == 0 && $delete_line != 1 && $other_flag != 1 && ($remarks1 != "" || $match1[0] != "")) {
												//echo $account_name;echo " ->  ";echo $line_sum;
												//$fixed_data .= ",,".($amount_of_money * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";
												$fixed_data .= ",".$remarks1.",,".$match1[0].",".($amount_of_money * 1).",".$day.",".$month.",".$year.",".$account_name.",".$filename.",".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												if ($tax_case == 1) {
													//$fixed_data .= ",,".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";
													//*$fixed_data .= ",".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";											
													$fixed_data .= ",".$remarks1.",,".$match1[0].",".($amount_of_money2 * 1).",".$day.",".$month.",".$year.",".$account_name.",".$filename.",".$line_sum.",".$sales_tax.",".$ryakugo."\r";
												}
											}
										}
									//}
									//$fixed_data = str_replace(",,",",",$fixed_data);
									
									
									
									//$output_data .= ",,".$amount_of_money.",".$remarks1.",".$remarks2.",費用,".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",".'=IF(INDIRECT(ADDRESS(ROW(),MATCH("相手科目",$1:$1,0)))=INDIRECT(ADDRESS(2,MATCH("相手科目",$1:$1,0))),IF(P3=INDIRECT(ADDRESS(2,MATCH("右補助",$1:$1,0))),IF(INDIRECT(ADDRESS(ROW(),MATCH("売上費用",$1:$1,0)))="費用",INDIRECT(ADDRESS(ROW(),MATCH("金額",$1:$1,0)))*-1,IF(INDIRECT(ADDRESS(ROW(),MATCH("売上費用",$1:$1,0)))="売上",INDIRECT(ADDRESS(ROW(),MATCH("金額",$1:$1,0))),"\#N/A")),0),0)+INDIRECT(ADDRESS(ROW()-1,COLUMN()))'.","."".$account_num.",".$line_sum."\r";
									
/*									if ($first_cnt == 0) {
										$partner_accounts = $data2[$i]."\r";
										$fixed_data .= $match[0].",".$data2[$i]."\r";
										$first_cnt = 1;
									} else {
										$fixed_data .= $match[0].",".$data2[$i]."\r";
									}
*/
									break;
								}
							}//科目がマッチした行を取得する処理 for($z = 0;$z < count($journalizing_arr)-1;$z++)の終わり
							
							//科目がマッチしなかった行を書き出す処理
							if($journalizing_match_flag == 0 && ((strlen($data[$i]) > 10 && $other_flag == 0 && $z == count($journalizing_arr) - 1))) {
									
									
									
								$pattern_comp_end = "/([0-9]{0,2}．[0-9]{0,2})([\W\w]{1,4})/im";//echo "　ここまで<br>";
								$match_num = preg_match($pattern_comp_end, $data4[$i], $match);
								//var_dump($match);echo "日付a";
								
								$account_data = str_replace(array(", ","."," ","。","．","|","'","`"),"",$data[$i]);
								//var_dump($account_data);
								//dataから科目を取り出す。
								$pattern_comp_2 = "/[0-9]{0,5}([\W\w]{1,4})/im";//echo "　ここまで<br>";
								$match_num1 = preg_match($pattern_comp_2, $account_data, $match1);
									
									
									
									if ($sum_month == 1) {
									//echo "月";
										$last_month = $last_month + 1;
									}
									
									$date_arr =  explode(",",str_replace(array(".","．","。"),",",$match[1]));

									if ($date_arr[0] > 12) {
										$date_arr[0] = substr($date_arr[0],-1,1);
									}
									if ($date_arr[1] > 31) {
										$date_arr[1] = substr($date_arr[1],-2,1);
									}
									//var_dump($date_arr);
									
									//echo $inner_num = substr(str_replace(array(".","．","。"),"",$match[1]),-3,3);
									
									
									
									if (($date_arr[0] != "" && $date_arr[0] <=12)  && $date_arr[1] <= 31 && $date_arr[2] <= 31) {
										if (isset($date_arr[2])) {
											$inner_num = $date_arr[1]."/".$date_arr[2];
											$last_month = $date_arr[1];
											if ($date_arr[2] != "") {
												$new_date = $date_arr[2];
											}
										} else if ($date_arr[0] == 0){
											$inner_num = $last_month."/".$last_date;
											if (isset($date_arr[1])) {//echo "bbxxbb";
												$new_date = $date_arr[1];
											}
										} else {
											$inner_num = $date_arr[0]."/".$date_arr[1];
											$last_month = $date_arr[0];
											if ($date_arr[1] != "") {
												$new_date = $date_arr[1];
											}
										}

										//var_dump($date_arr);
										if ($new_date <= 31) {
											if ($new_date == "") {//echo "abcd<br>";
												$pay_date = $inner_num;
											} else {//echo "xyz<br>";
												$last_date = $new_date;//echo "a<br>";
												$pay_date = $inner_num;//echo "b<br>";
											}
										} else {
											$pay_date = $last_month."/".$last_date;
										}
									
									
									}
									
									//echo $last_date;echo "lastdate<br>";
									//echo $new_date;echo "newdate<br>";
									//if ($last_date < $new_date && $new_date <= 31) {

									$filename = str_replace("txt","jpg",$filename);
									$pattern = '/^,/i';
									$replacement = '';
									$data[$i] = preg_replace($pattern, $replacement, $data[$i]);
									$data_arr = explode(",",$data[$i]);
									$date_arr = explode("/",$pay_date);
									if ($_REQUEST["y"] == "") {
										$year = $fiscal_year;
									} else {
										$year = $_REQUEST["y"];
									}
									//var_dump($date_arr);
									
									$month = $date_arr[0];
									$day = $date_arr[1];
									
									if ($month >= 1 && $month <= $financial_month) {
										$year += 1;
									}
									
									//echo "expdataarr:";
									//var_dump($data_arr);echo $filename;
									//echo "個数:".count($data_arr)." ";
									$last_num = 0;
									
									$last_num = count($data_arr) - 1;
									//echo "abc";
									if ($data_arr[$last_num] == "") {//echo "xyz";
										$last_num = count($data_arr) - 2;
									}
									
									if ( $data_arr[1] == "内税起票額") {
										$delete_line = 1;
										$tax_case = 1;
										$remarks1 = $data_arr[1]." ".$data_arr[2];//echo "<br>";
										$amount_of_money = $data_arr[3] * 1;//echo "<br>";
										if ($last_num != 4) {
											$amount_of_money2 = $data_arr[4] * 1;//echo "<br>";
										}
											$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
										
									} else if ( $data_arr[1] == "雑損失") {
										$tax_case = 1;
										$remarks1 = $data_arr[1];//echo "<br>";
										$amount_of_money = $data_arr[$last_num - 1] * 1;//echo "<br>";
										$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
										
									} else {
										if (ctype_digit($data_arr[1])){
											$remarks1 = $data_arr[0];//echo "<br>";
											$amount_of_money = $data_arr[1] * 1;//echo "<br>";
											if ($last_num != 1) {
												$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
											}
										} else {
											if (ctype_digit($data_arr[5])) {
												$remarks1 = $data_arr[1]." ".$data_arr[2];//echo "<br>";
												$amount_of_money = $data_arr[3] * 1;//echo "<br>";
												if ($last_num != 3) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											} else if (ctype_digit($data_arr[2])) {
												$remarks1 = $data_arr[1];//echo "<br>";
												$amount_of_money = $data_arr[2] * 1;//echo "<br>";
												if ($last_num != 2) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											} else {
												$remarks1 = $data_arr[2];//echo "<br>";
												$amount_of_money = $data_arr[$last_num - 1] * 1;//echo "<br>";
												if ($last_num != 3) {
													$line_sum = $data_arr[$last_num] * 1;//echo "<br>";
												}
											}
										}
									}
									if ($match1[0] == "複" || $match1[0] == "合" || $match1[0] == "複ａ" ) {
										$match1[0] = "複合";
									}
									if ($match1[0] == "現") {
										$match1[0] = "現金";
									}
									if ($first_cnt == 0 ) {
										//if ($account_flag == 0) {
											//$fixed_data .= $match1[0].",".$pay_date.",".$data[$i].",".$filename.",".$account_num."\r";
									//		$fixed_data .= ",,".$amount_of_money.",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum."\r";
											
										//}
										$first_cnt = 0;
									}// else {
									
										$str = "";
/*
									    $options = array('-d', '/usr/local/lib/mecab/dic/mecab-ipadic-neologd');
									    $mecab = new MeCab_Tagger($options);
									    $nodes = $mecab->parseToNode($remarks1);

									    foreach ($nodes as $n) {
									        echo $str .=$n -> getSurface();
									    	$n -> getFeature()." ";
									    }
*/										//"UTF-8", "SHIFT-JIS"
										/*
										echo mb_detect_encoding($remarks1);
										//echo $remarks1 = mb_convert_encoding($remarks1, "EUC-JP", "UTF-8");
										0//echo $remarks1 = mb_convert_encoding($remarks1, "EUC-JP", "UTF-8");
										echo "<br><br><br>";
										$arg_yomi = array('-O' => 'yomi');
										$mecab = new MeCab_Tagger($arg_yomi);
										echo $str = $mecab -> parse($remarks1);
										mecab_destroy($mecab);
										// カタカナででてくるので、ひらがなへ変換。utf-8を指定しないとひらがなになってくれない
										echo  $str = mb_convert_kana($str, "c", "utf-8");
										echo "<br><br><br>";
										*/
										/*
										echo $ryakugo = $kana2romaji -> convert($str);
										echo "<br><br><br>";
										*/
										
										if ($output == 1) {
											if ($account_flag == 0 && $delete_line != 1 && $other_flag != 1 && ($remarks1 != "" || $match1[0] != "")) {
												//echo $account_name;echo " ->  ";echo $line_sum;
												$fixed_data .= ",,".$remarks1.",,,".$match1[0].",,,".($amount_of_money * 1).",".$day.",".$month.",".$year.",,".$account_name.",,,".$filename.",,".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												if ($tax_case == 1) {
													//$fixed_data .= ",,".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";
													//*$fixed_data .= ",".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";											
													$fixed_data .= ",,".$remarks1.",,,".$match1[0].",,,".($amount_of_money2 * 1).",".$day.",".$month.",".$year.",,".$account_name.",,,".$filename.",,".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												}
											}

										} else {
											if ($account_flag == 0 && $delete_line != 1 && $other_flag != 1 && ($remarks1 != "" || $match1[0] != "")) {
												//echo $account_name;echo " ->  ";echo $line_sum;
												$fixed_data .= ",".$remarks1.",,".$match1[0].",".($amount_of_money * 1).",".$day.",".$month.",".$year.",".$account_name.",".$filename.",".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												if ($tax_case == 1) {
													//$fixed_data .= ",,".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";
													//$fixed_data .= ",".($amount_of_money2 * 1).",".$remarks1.",".$remarks2.",費用,".$match1[0].",".$left_sub.",".$left_sect.",".$day.",".$month.",".$year.",,".$filename.",".$account_name.",".$line_sum.",".$ryakugo."\r";											
													$fixed_data .= ",".$remarks1.",,".$match1[0].",".($amount_of_money2 * 1).",".$day.",".$month.",".$year.",".$account_name.",".$filename.",".$line_sum.",".$sales_tax.",".$ryakugo."\r";											
												}
											}
										}
							}
						}//1行ごとの処理の終わり for ( $i = 0; $i < $cnt; $i++ )
//exit;
						// ファイルのパスを変数に格納
						//開発用
						//$FileText = $txt_dir."/".$dlp."ci.txt";
						$FileText = $txt_dir."/TextConvertResult/".$dlp.".csv";
						$fixed_data = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n"),"\n",$fixed_data);
						// ファイルに書き込む
						file_put_contents($FileText,$fixed_data,LOCK_EX);
						//file_put_contents($FileText,$output_data,LOCK_EX);
						
					}
					
					//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
					//入れる金額が無かった場合、一番上の金額を入れる。
				}//if ( ==png)

				//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
//				$AddWords = "\r\n".date('Y/m/d',strtotime($got_date)).",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$industry_type_s;
				//echo "<br>";
//				file_put_contents($FileText, $AddWords, FILE_APPEND);
				//$outcode = "SHIFT-JIS";

				//全ファイル記録用ファイルに追記
				$FileText2 = $txt_dir."/TextConvertResult/convert_data_all.csv";
				//file_put_contents($FileText2,$fixed_data, FILE_APPEND);				
				//$text_all = file_get_contents($FileText2);
				//$text_all = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n"),"\n",$text_all);exit;
				$outcode = "sjis-win";
				$incode = "UTF-8";
				$nl = "\n";
				$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
				$this ->convertTextCode($FileText , $outcode, $FileText2 , $outcode, $nl);

				//中間フォルダからリネームして結果フォルダに入れる。
//				rename( $file_path.$dlp.'ci.txt', $txt_res_dir."/".$dlp.'.txt');
				//処理とともに画像も結果フォルダにコピーする。
//				rename( $upload_dir_path."/".$dlp.'.jpg', $txt_res_dir."/".$dlp.'.jpg');
				
				//$outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

				// ファイルを出力する
				//readfile($FileText);
				//exit;
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
			//取得したTEXTファイルを削除する。
	//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//$counter++;
			return $FileText;
//		}//for文の終わり
	}//メソッドの終わり

	/**
	* テキストファイルの文字コードを変換し保存する
	* @param string $infname  入力ファイル名
	* @param string $incode   入力ファイルの文字コード
	* @param string $outfname 出力ファイル名
	* @param string $outcode  出力ファイルの文字コード
	* @param string $nl       出力ファイルの改行コード
	* @return string メッセージ
	*/
	function convertCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'wb');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}
	
	function convertTextCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
		$instr = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n","\n\n"),"\n",$instr);
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'a');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}
	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}


	function recurse_chown_chgrp($mypath,$uid, $gid) {
		if (mkdir( $mypath, 0775, true ) ) {
			chmod( $mypath, 0775);
			chown( $mypath, $uid);
		    chgrp( $mypath, $gid);
		  //echo "ディレクトリ作成成功！！";
		} else {
		  //echo "ディレクトリ作成失敗！！";
		}
	}

	function getFileList($dir) {
	    $files = glob(rtrim($dir, '/') . '/*');
	    $list = array();
	    foreach ($files as $file) {
	        if (is_file($file)) {
	            $list[] = $file;
	        }
	        if (is_dir($file)) {
	            $list = array_merge($list, getFileList($file));
	        }
	    }
	 
	    return $list;
	}

	function random($length = 12) {
	    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
	    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
	}
	
	function convertNewline($string, $to="\n") {
	    return preg_replace("/\r\n|\r|\n/", $to, $string);
	}
	
}//classの終わり

?>