<?php require("header.php");?>


<title>非正規ユーザーのアップロード - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}




?>

<article>

	<section id="m-1-box">
		<h2>
			非正規ユーザーのアップロード
		</h2>
		<p>
		非正規ユーザーをCSVでアップロードします。(画像も仕訳csvファイルも全部同時にアップロードする。)
		
		</p>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./upload_nruser" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo dirname(__FILE__).'/../pdf/nru';?>" />
				<input type="hidden" name="return_url" value="./nruser_upload" />
				<input type="hidden" name="ahead_url" value="./makeuser" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="アップロード" />
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>
			
	</section>

</article>
<?php require("footer.php");?>