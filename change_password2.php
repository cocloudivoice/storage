<?php session_start(); ?>
<?php
//パンクズリスト用設定（headerより上に置く）
$bread_num = 1;//基本的に1
$bread_arr = array("パスワード変更");//現在のページの名前	
?>
<!DOCTYPE html>

<html Lang="ja">

<head>
<title>クラウドインボイス</title>
	
<script src="//syachicloud.black/js/jquery-2.1.1.min.js" type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript" src="//syachicloud.black/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="//syachicloud.black/js/jquery-ui-i18n.js"></script>
<link rel="stylesheet" href="//syachicloud.black/css/jquery-ui.min.css" />
<!--<link rel="stylesheet" href="./css/header.css" type="text/css" />-->
<script type="text/javascript">


<?php
if (isset($af_id)) {
	echo "function chClient(client) {";
	echo 'document.getElementById("ch_client_form").submit();';
	echo "}";
}
?>

</script>
<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../pdf/PdfToImg.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');

//変数の宣言
$words = "";
$flag = "";
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_cd = $db_con -> select_db_connect(2);

if(isset($_SESSION['user_id'])){
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout_af");
	exit();
}
if ($_REQUEST['change_pass'] == 1) {

	//ここからメールアドレス照合
	if (isset($_REQUEST['temp_password'])) {
		//Emailアドレスの存在を確認する。
		$temp_password = md5(htmlspecialchars($_REQUEST['temp_password'],ENT_QUOTES));
		if ($temp_password == $af_arr["password"]) {
			if ($_REQUEST['password'] == $_REQUEST['password2']) {
				mb_language("ja");
				mb_internal_encoding("UTF-8");
				$edit_email = $af_arr["e_mail"];
				$edit_val = $af_arr['id'];
				$pass = $_REQUEST['password'];
				$update_data = "`password` = '".md5($pass)."'";
				$message = "
------------------------------------------------------------
本メールは、システムより自動送信しています。
心当たりがない場合は、下記のお問い合わせ先までご連絡ください。
なお、このメールに返信されても応答できません。ご了承ください。
------------------------------------------------------------

".$user_company_name[0]['company_name']."
".$af_arr['name']."　様

クラウドインボイスをご利用いただきありがとうございます。

次の通りパスワードが変更されましたのでお知らせします。

------------------------------------------------------------

▼ログインURL
https://storage.cloudinvoice.co.jp/invoice/signin_af
		
▼ユーザーID
".$af_arr['e_mail']."
		
▼パスワード
".$pass."

------------------------------------------------------------

ご案内は以上です。
どうぞ、よろしくお願いいたします。


■■■■■■■■■■■■■■■■■■■■■■
	
クラウドインボイス株式会社
東京都新宿区西新宿7-18-2 TAPビル3階
TEL： 03-6328-0945
FAX： 03-6328-2708
E-mail： info@cloudinvoice.co.jp
受付時間：10:00～18:00
※土日祝日、弊社指定休日を除く

■■■■■■■■■■■■■■■■■■■■■■";
				$headers = 'From: クラウドインボイス<info@cloudinvoice.co.jp>';
				$af_con -> af_update_data($pdo,$edit_val,$update_data);
				mail($edit_email, 'パスワード変更通知',$message,$headers);
				echo "<script type='text/javascript'>alert('".$edit_email."に新パスワードを送信しました。');</script>";
				echo "<script type='text/javascript'>alert('パスワードを更新しました。');</script>";
				echo "<script type='text/javascript'>parent.location.href='';	</script>";
			}
		} else {
			$_SESSION['err_msg'] ="現在のパスワードが間違っています。";
		}
		
	}

}
?>

<script type="text/javascript">
	function checkPass() {
		var p1 = register.password.value;
		var p2 = register.password2.value;
		if (p1 != "") {
			if (p1 == p2) {
				register.submit();
				return true;
				
			}else{
				alert('パスワードが一致していません');
				return false;
			}
		} else {
			alert("パスワード欄が空欄です。");
			return false;
		}
	}
</script>
<style type="text/css">
	.red { color:red;}
	table#long {width:500px;}
	#button {margin-top:10px;}
</style>


<title>パスワード変更</title>
</head>
<body>
<article>
	<section id="m-2-box">
	<section  id="tag">
		<h2>
		パスワードの変更
		</h2>
	
		<form name="register" action="" method="post">
			<div id="passbox" class="box tagbox">
				<div class="tagbottom">
					<table id="long">
						<tr id="first">
							<th>現在のパスワード</th>
							<td><input type="password" name="temp_password" /></td>
						</tr>
						<tr>
							<th>新しいパスワード</th>
							<td><input type="password" name="password" /></td>
						</tr>
						<tr id="last">
							<th id="last">新しいパスワード（確認）</th>
							<td id="last"><input type="password" name="password2" /></td>
						</tr>
					</table>
					<?php
						echo '<p class="red">'.$_SESSION['err_msg'].'</p>';
						$_SESSION['err_msg'] = "";
					?>
				</div>
			</div>
			<input type="hidden" name="change_pass" value="1" /><br>
			<div id="clear"></div>
			<div id="button">
				<input type="button" onclick="return checkPass();false" class="RegisterButton" value="変更する">
			</div>
				<?php
				if ($MessageBox) {
					echo '<p style="color:red;">'.$MessageBox.'</p>';
				}
				?>
			</form>
	</section>
</article>
</body>
