<?php
ini_set('session.use_cookies', 1);//お客様がクッキーを利用しているときは、セッションをクッキーに保存する
ini_set('session.cookie_lifetime', 0);//セッションをクッキーに保存したときは、ブラウザを閉じるまでセッションを切断しない
ini_set('session.gc_maxlifetime', 7200);//クッキーを利用しないときは、セッション有効時間は２時間

session_start();
//$_SESSION['user_id']="";
//session_id();
?>
		
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ログイン画面</title>
<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:400px;
	height:auto;
	margin-left:auto;
	margin-right:auto;
}
#frame{
	width:300px;
	height:150px;
	text-align:center;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
</style>
</head>
<body>
	
	
	<div id="wrap">
		<h1>ログイン画面</h1>
		<div id="frame">
			<form name="login_form1" method="post" action="https://co.cloudinvoice.co.jp/invoice/login_check.php" >
			<p>ログインID  :　<input type="text" name="id" style="ime-mode: disabled;" ></p>
			<p>パスワード   :　<input type="password" name="password" ></p>
			<p id="submit_b"><input type="submit" name="submit" value="ログイン" /></p></form>
        <?php echo $_SESSION['check']."\n"; ?><!--ログイン失敗を通知-->
		</div>
			<form name="login_form2" method="post" action="https://co.cloudinvoice.co.jp/invoice/signup_1.php" >
			<p id="touroku_b"><input type="submit" name="toroku" value="ユーザ登録" ></p></form>
        <!--<h2><a href="https://co.cloudinvoice.co.jp/invoice/insertsite.php"></h2>-->
	</div>
</body>
</html>