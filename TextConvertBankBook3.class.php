<?php

class TextConvertBankBook {
	
	function convertText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$upload_dir_path,$txt_res_dir,$txt_dir,$file_path,$af_id,$output,$fiscal_year,$financial_month,$com_name) {
		//txtの内容を取得して分析し、必要な情報（日付、金額、電話番号1,2,3、会社名、商品名、業種）
		//を取得して上書きする工程
//		for($m = 0;$m < count($img_file_arr);$m++) {
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name_arr = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$money_num_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();
		$first_cnt = 0;
		$partner_accounts = "";
		$account = "";
		$account_num = "";
		$fixed_data_b1 = "";
		
		if ($fiscal_year == "") {
			$fiscal_year = 2017;//年度 ?y=2013で年度切り替え可能
		}
		if ($financial_month == "") {
			$financial_month = 12;//決算月 ?m=5で年度切り替え可能
		}
		if (isset($_REQUEST["y"])){
			$fiscal_year = $_REQUEST["y"];
		}

		require_once(dirname(__FILE__).'/Kana2Romaji.class.php');
		$kana2romaji = new Kana2Romaji();

		try {
	    	//var_dump($file);
	        //$filename = $img_file_arr[$m];
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$filename  = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$filename );
			$file_name_top = substr($filename , 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($filename , -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			//初期化
			$data_cnt = 0;
			//echo $txt_res_dir."/".$dlp.".txt";
			//echo file_exists($txt_res_dir."/".$dlp.".txt");
			//OCR済みのテキストファイルがあったらOCR処理しない
			if(file_exists($txt_res_dir."/".$dlp.".txt")) {
				//請求書が登録されていないか確認する。
				//$flag ="download_invoice_data_clm";
				//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
				//同じdownload_passwordで請求書が登録済みの場合
				if (substr($filename, -3, 3) == "txt") {
					//請求書登録
					//仮のデータで請求書登録をする。
					//仮の請求書・領収書データの登録
					//テキストを読んで上書き処理
					//echo "テキスト処理<br>";
					$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
					$text_file_path = $txt_dir."/".$dlp.".txt";

					//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
					if(file_exists($text_file_path_en)) {
						$data = file_get_contents($text_file_path_en);
						$data .= file_get_contents($text_file_path);
					} else {
						$data = file_get_contents($text_file_path);
					}
					
					$data_all = $data;
					//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

					//１データとして扱うために分けない。
					$data = str_replace("\r","\n",$data);
					$data = explode( "\n", $data );
					
					//echo "<br>";
					$data_cnt = count( $data );
					//echo "<br>";
					//var_dump($data);
					$fixed_data = "";
					$output_data = "";
					//業種、摘要の照合用リスト作成
					$ind_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/indtype.csv";
					$remarks_csv = "/var/www/storage.cloudinvoice.co.jp/html/files/mjsocr/remarks.csv";
					
					$indtypes = file_get_contents($ind_csv);
					//var_dump($indtypes);
					$ind_arr = explode(",",$indtypes);
					//var_dump($ind_arr);
					
					$remarks = file_get_contents($remarks_csv);
					$remarks_arr = explode(",",$remarks);
					//var_dump($remarks_arr);
					/*
					foreach ($remarks_arr as $line) {
						//終端の空行を除く処理　空行の場合に取れる値は後述
						
						if($line) {
					   		$records[] = $line;
					 	}
						unset($line);
					}
					*/
					//var_dump($records);
					//echo $data_cnt; 
					for ( $i = 0; $i < $data_cnt; $i++ ) {//echo e;
					
						//変数初期化
						$money_num_cnt = 0;
						$telkey_flag = 0;
						
						/*
						$outcode = "sjis-win";//"SHIFT-JIS";
						$incode = "UTF-8";
						$nl = "\r\n";
						$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
						*/
						
						//全角文字を半角に変換
						//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
						$data[$i] = mb_convert_encoding( mb_convert_encoding($data[$i], "sjis-win", "SHIFT-JIS"),"UTF-8","sjis-win");
						$data[$i] = str_replace(array("Ｄ","ｊ","，","、","。","・","冶","冰"),"",$data[$i]);
						$data[$i] = str_replace(array("　１　"),"",$data[$i]);
						
						$data[$i] = str_replace(array("！０１５","ｉ０１５"),"2015",$data[$i]);
						$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0","ｏ"=>"0");
						$data[$i] = strtr($data[$i], $kana);
						$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
						//var_dump($data[$i]);
						$data[$i] = str_replace(array("。","。","o","U","()","ｏ","｛｝","［］","〔〕","【】","（）","Ｑ","叭）","1ａ"),"0",$data[$i]);
						$data[$i] = str_replace(array("￥","＊"),"￥￥",$data[$i]);
						$data[$i] = str_replace(array("ｂ"),"1",$data[$i]);
						$data[$i] = str_replace(array("Ｚ"),"2",$data[$i]);
						$data[$i] = str_replace(array("會","輻"),"3",$data[$i]);
						$data[$i] = str_replace(array("繍","ｘ","刈"),"4",$data[$i]);
						$data[$i] = str_replace(array("§"),"5",$data[$i]);
						$data[$i] = str_replace(array("ら"),"6",$data[$i]);
						$data[$i] = str_replace(array("讐","β"),"8",$data[$i]);
						echo $data[$i] = str_replace(array("g","ｇ","Ｊ","q"),"9",$data[$i]);
						
						$patterns = array ("/([0-9]{1,2})[。，．　．－]{1,2}([0-9]{1,2})[。，．　．－]{1,2}([0-9]{1,2})[。．　．]{1,2}/imu","/([0-9]{1,2})月([0-9]{1,2})日/imu","/Ｈ([0-9]{1,2})[。，．　．]{1,2}([0-9]{1,2})[。，．　．]{1,2}([0-9]{1,2})[。．　．]{1,2}/imu","/Ｈ([0-9]{1,2})[。．　．]{1,2}([0-9]{1})([0-9]{1,2})/imu");
						$replace = array ('\1年\2月\3日日','\1月\2日日','\1年\2月\3日日','\1年\2月\3日日');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						//echo "<br>";
						//var_dump($data[$i]);
						
						$data[$i] = str_replace(array("叩"),"00",$data[$i]);
						$data[$i] = str_replace(array("ＪＩ","員","ｎ"),"11",$data[$i]);
						$data[$i] = str_replace(array("珮"),"33",$data[$i]);
							
						$data[$i] = str_replace(array("收"),"収",$data[$i]);
						$data[$i] = str_replace(array("“","‘","’","”","｀","・","ｒ","一","－－"),"－",$data[$i]);
						$data[$i] = str_replace(array("龠","彖","奎","禽","叙","兮","昶","歛"),"金",$data[$i]);
						$data[$i] = str_replace(array("　"),"○",$data[$i]);
						$data[$i] = str_replace(array("－○"),"－0",$data[$i]);
						$data[$i] = str_replace(array("，　","畠","，","″","？","亠","冖","。","．"," ○","〕","\s1\s","　1　","\s1\s","[\s　 ]*1[\s　 ]*","○－1"),"",$data[$i]);
						$data[$i] = str_replace(array("○1○","○1 ","ｆ","ｚ"),"",$data[$i]);
						$data[$i] = str_replace(array("1ヽ゛そ"),"ドコモ",$data[$i]);
						$data[$i] = str_replace(array("りイタイ"),"ケイタイ",$data[$i]);
						$data[$i] = str_replace(array("ヵ","ヶ","ッ","］","ィ","仆","ト1－","ｐ","Ξｌ","杓","捫","：ｌ","：ﾞ","洲゜","1こ","う","こ","埓","コり","ｔ","リ゛","ヌ","ウリイ","＾","冫"),array("カ","ケ","ツ","コ","イ","イト","ド","ヤ","ヨ","キヨ","チベ","ユ","ー","ンパ","ヒ","ラ","ン","キ","コウ","セ","ゾ","ス","ウサイ","ヘ","ン"),$data[$i]);
						$data[$i] = str_replace(array("Ｉ","￡","テヽヽン4","錯","｜回コ","リニツク","プツ","池","2ね","岶動","チ1","チｌ","衵","倩","小｜冫","Ｐ","ｙヽ","吋","－ン4"),array("エ","ム","デンキ","イル","ルカ","ソニツク","プラ","スセ","スセン","自動","チユ","チユ","キヨ","ウサ","小口","ア","ジ","シヤ","ンキ"),$data[$i]);
						$data[$i] = str_replace(array("イ弋"),"代",$data[$i]);
						$data[$i] = str_replace(array("迪ｉＪＩへ"),"当座へ",$data[$i]);
						//echo $data[$i];echo "<br>";
						$data[$i] = str_replace(array("キヨウリイ","ヨｌウ","ＤｓＫ","抖","ニ1り","ｔ！リり","凹り","竚ヽン","自蚰","リー2","アーｌ","アーｌ","哘","＝イ","：－コ゛ヤ","すコ－ヤ","す","コヽヽ","貸伺","囗"),array("キヨウサイ","シヨウ","ＤＳＫ","キキ","ユウ","セゾン","セゾン","セゾン","自動","リース","アール","アール","ルテ","テイ","ナゴヤ","ナゴヤ","ナ","ゴ","貸付","ロ"),$data[$i]);
						$data[$i] = str_replace(array("次","迪","戔","聶","寧","拿","寧","寒","搴","韋","暈","ｘ","ｓ","水","米"),"★",$data[$i]);
						$data[$i] = str_replace(array("保腕料","保泱料"),"保険料",$data[$i]);
						$data[$i] = str_replace(array("肖否","複含","複ａ","複a","複き","複台","楔合","掏合","陶合","誨合","煦合","陶今"),"複合",$data[$i]);
						$data[$i] = str_replace(array("預氣","侑金","硯金","現奎","現彖","陌金","積金","頂金","頂彖","碵金","甬金","附金","贖金","に金"),"預金",$data[$i]);
						$data[$i] = str_replace(array("ダイしクト"),"ダイレクト",$data[$i]);
						$data[$i] = str_replace(array("フ11コ"),"フリコ",$data[$i]);
						$data[$i] = str_replace(array("をリコ","4リリシホ吻","セリリシホウｉ9","リリシ","ｉ9"),array("オリコ","ゼイリシホウシ","ゼイリシホウシ","イリシ","シ"),$data[$i]);
						$data[$i] = str_replace(array("リフト－ニノク","リフトハヽじ"),"ソフトバンク",$data[$i]);
						$data[$i] = str_replace(array("○○"),"★",$data[$i]);
						//echo $data[$i];echo "<br>";
						$data[$i] = str_replace(array("脱","悦"),"税",$data[$i]);
						$data[$i] = str_replace(array("振ら","Ｉ辰Ｅ１","Ｉ辰Ｅ","匆Ｅ込","孫1込","1ミ1","振Ｅ5き","振Ｅ","振＝込"),"振込",$data[$i]);
						$data[$i] = str_replace(array("シャ0ウシ","シャ囗ウシ"),"シャロウシ",$data[$i]);
						$data[$i] = str_replace(array("ｔ：゛","t゛"),"ビ",$data[$i]);
						$data[$i] = str_replace(array("酊Ｍ","ｇＴＭ","「Ｍ","内ＴＭ"),"ＡＴＭ",$data[$i]);
						$data[$i] = str_replace(array("]","L","I","！","!","』","｜","】","ｌ"),"1",$data[$i]);
						$data[$i] = str_replace(array("繍","ｘ","・ｌ","・1","ｔ"),"4",$data[$i]);
						$data[$i] = str_replace(array("小1","・ｌｔ"),"44",$data[$i]);
						$data[$i] = str_replace(array("￣","_"),"ー",$data[$i]);
						
						$data2[$i] = $data[$i];//後ろの数字を消して日付だけ取り出す処理をした方がいい。

						
						$data2[$i] = str_replace(array("○○"),"★",$data2[$i]);
						$data2[$i] = str_replace(array(" ","万","｝","?","，　","？","亠","∩","Ｃ廴","廴","（","ヽ","○","△","|","　ｉ　","○","口　","1ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","ｊ","Ｊ","＿","‘","｀","丿","）","”","°","゛","卜","／","Ｌ","』","＼","ゞ","）","（","　I　","　Ｉ　","　1　"),"",$data2[$i]);
						$data2[$i] = str_replace(array("・","’","。","."),"．",$data2[$i]);
						//$data2[$i] = str_replace(array("．"),"/",$data2[$i]);
						//$data[$i] = str_replace(array("ｙ"),"1",$data[$i]);
						//$data2[$i] = str_replace(array("ｙ"),"1",$data2[$i]);
						$data2[$i] = str_replace(array("－○"),"－0",$data2[$i]);
						$data2[$i] = str_replace(array("○1 "),"",$data2[$i]);
						//echo "aaa<br>";
						$data2[$i] = str_replace(array("\r\n　"),"",$data2[$i]);
						$data2[$i] = str_replace(array("\r\n"),"",$data2[$i]);
						$data2[$i] = str_replace(array("\r\n\r\n"),"",$data2[$i]);
						$data2[$i] = str_replace(array("\r\n\r\n\r\n\r\n\r\n"),"",$data2[$i]);
						//$data[$i] = str_replace(array("仕人"),"仕入",$data[$i]);
						var_dump($data[$i]);
						$patterns = array ('/○1\r/imu','/^1/imu',"/○1○/imu","/★1\r/imu","/([0-9]{1,5})★([0-9]{3})\r/imu");
						$replace = array ('','','','','\1\2','★\1\2★');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
						$patterns = array ('/([0-3]{1}[0-9]{1})－([0-1]{0,1}[0-9]{1})－([0-3]{0,1}[0-9]{1})/imu','/([0-3]{1}[0-9]{1})([0-1]{1}[0-9]{1})－([0-3]{1}[0-9]{1})/imu');
						$replace = array ('○■\1-\2-\3□','○■\1-\2-\3□');
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
						//var_dump($data[$i]);
						//echo "あああ<br>";
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						//echo "<br>";
						//var_dump($data[$i]);
						//var_dump($data2[$i]);
						$data[$i] = str_replace(array("○○"),"★",$data[$i]);
						$data2[$i] = str_replace(array("○○"),"★",$data2[$i]);
						//echo "<br>";
						if ($i == 23) {
						//exit;
						}
						$data4[$i] = $data2[$i];
						//$data2[$i] = str_replace(array("　"),",",$data2[$i]);
						//$data2[$i] = str_replace(array("　　	"),",",$data2[$i]);
						//$data2[$i] = str_replace(array("　　　"),",",$data2[$i]);
						$data2[$i] = str_replace(array("　","	"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("　　","		"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("　　　","			"),"○",$data2[$i]);
						//$data2[$i] = str_replace(array("　　"),"　",$data2[$i]);
						$data2[$i] = str_replace(array("○/○"),"",$data2[$i]);
						
						$data2[$i] = str_replace(array("－○"),"－",$data2[$i]);
						$data2[$i] = str_replace(array("○○○○○○"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("○○○○○"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("○○○○"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("○○○"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("○○"),"○",$data2[$i]);
						$data2[$i] = str_replace(array("、","	"),"",$data2[$i]);
						$data2[$i] = str_replace(array(",,,"),",",$data2[$i]);
						$data2[$i] = str_replace(array(",,"),",",$data2[$i]);
						$data2[$i] = str_replace(array(",,,"),",",$data2[$i]);
						$data2[$i] = str_replace(array(",,"),",",$data2[$i]);
						//$data2[$i] = str_replace(array("■","]","L"),"1",$data2[$i]);
						
						$data2[$i] = str_replace(array("。","。","〝","位","o","U","()","ｏ","｛｝","［］","〔〕"),"0",$data2[$i]);
						$data2[$i] = str_replace(array("]","L","I","！","!","I,","Ｉ,","Ｉ，","ｂ"),"1",$data2[$i]);
						$data2[$i] = str_replace(array("z"),"2",$data2[$i]);
						$data2[$i] = str_replace(array("會","輻"),"3",$data2[$i]);
						$data2[$i] = str_replace(array("繍","ｘ"),"4",$data2[$i]);
						$data2[$i] = str_replace(array("§"),"5",$data2[$i]);
						$data2[$i] = str_replace(array("s"),"6",$data2[$i]);
						$data2[$i] = str_replace(array("讐","β","畠"),"8",$data2[$i]);
						$data2[$i] = str_replace(array("g","q"),"9",$data2[$i]);
						$data2[$i] = str_replace(array("り叙","り全"),"り金",$data2[$i]);

						//var_dump(	$data2[$i]);
						$data3[$i] = $data2[$i];				
						$data[$i] = str_replace(array("'","'"," ","−","｝","ｆ","ｚ","拿","東","＊","","総勘定元帳","勘定元帳","万","∩","Ｃ廴","廴","（","ヽ","○","△","|",",","，","・","．","　1　","　ｉ　","口　","ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","ｊ","’","￣",", ",". ","‘","｀","丿","）","”","°","゛","卜","／","Ｌ","』","＼","ゞ"),"",$data[$i]);
						$data[$i] = str_replace(array("’","￣")," ",$data[$i]);
						$data[$i] = str_replace(array("龠","彖","奎"),"金",$data[$i]);
						$data[$i] = str_replace(array("Ｘｌ　"),"円",$data[$i]);
						$data[$i] = str_replace(array("\r\n　"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n\r\n"),"",$data[$i]);
						$data[$i] = str_replace(array("\r\n\r\n\r\n\r\n\r\n"),"",$data[$i]);
						$data[$i] = str_replace(array("診涌","診通","普涌","普埴","倅通","普油"),"普通",$data[$i]);
						$data[$i] = str_replace(array("淤","渟","淬","鏗","矜"),"普",$data[$i]);
						$data[$i] = str_replace(array("涌"),"通",$data[$i]);
						$data[$i] = str_replace(array("碵"),"預",$data[$i]);
						$data[$i] = str_replace(array("筏"),"複",$data[$i]);
						$data[$i] = str_replace(array("紿"),"給",$data[$i]);
						$data[$i] = str_replace(array("肖否","複含","複ａ","複a","複き","複台","楔合","掏合","陶合","誨合","煦合","陶今"),"複合",$data[$i]);
						$data[$i] = str_replace(array("侑金","陌金","積金","頂金","頂彖","碵金","甬金","附金","贖金","に金"),"預金",$data[$i]);
						$data[$i] = str_replace(array("仕人"),"仕入",$data[$i]);
						$data[$i] = str_replace(array("俔","睨"),"税",$data[$i]);
						$data[$i] = str_replace(array("捐"),"損",$data[$i]);
						$data[$i] = str_replace(array("有費","陏費"),"賄費",$data[$i]);
						
						//$data[$i] = str_replace(array("仕人"),"仕入",$data[$i]);
						
						$data[$i] = str_replace(array("　","	"),"○",$data[$i]);
						$data[$i] = str_replace(array("　　","		"),"○",$data[$i]);
						$data[$i] = str_replace(array("　　　","			"),"○",$data[$i]);
						//$data[$i] = str_replace(array("　　"),"　",$data[$i]);
						$data[$i] = str_replace(array("○/○"),"",$data[$i]);
							
						$data[$i] = str_replace(array("Ｏ"),"",$data[$i]);
						$data[$i] = str_replace(array("○○○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("○○"),"○",$data[$i]);
						$data[$i] = str_replace(array("、","	"),"",$data[$i]);
						$data[$i] = str_replace(array(",,,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,,"),",",$data[$i]);
						$data[$i] = str_replace(array(",,"),",",$data[$i]);
						
						$data[$i] = str_replace(array(",2015","！015"),"2015",$data[$i]);
						$data[$i] = str_replace(array("叩","m","0c","0C"),"00",$data[$i]);
						$data[$i] = str_replace(array("ｊｌ","ｉｌ","ｊ","ｈ"),"/",$data[$i]);
						//$data[$i] = str_replace(array("//","{{","[1"),"11",$data[$i]);
						$data[$i] = str_replace(array("。","。","〝","位","o","U","()","ｏ","｛｝","［］","〔〕","（ｊ"),"0",$data[$i]);
						$data[$i] = str_replace(array("]","L","I","！","!","ｂ","』","｜"),"1",$data[$i]);
						$data[$i] = str_replace(array("Ｉ","！"),"1",$data[$i]);
						$data[$i] = str_replace(array("z"),"2",$data[$i]);
						$data[$i] = str_replace(array("會","輻"),"3",$data[$i]);
						$data[$i] = str_replace(array("繍","ｘ"),"4",$data[$i]);
						$data[$i] = str_replace(array("轟","§"),"5",$data[$i]);
						$data[$i] = str_replace(array("s"),"6",$data[$i]);
						$data[$i] = str_replace(array("讐","β"),"8",$data[$i]);
						$data[$i] = str_replace(array("g","q"),"9",$data[$i]);
						$data[$i] = str_replace(array("B","t1","曰"),"日",$data[$i]);
						$data[$i] = str_replace(array("¨","－","・","~"),"-",$data[$i]);
						//$data[$i] = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data[$i]);
						$data[$i] = str_replace(array("普遍車","普遷車","普通車"),"普通車",$data[$i]);
						$data[$i] = str_replace(array(",普通車","普通車")," 普通車",$data[$i]);
						$data[$i] = str_replace(array("輕自動"),"軽自動",$data[$i]);
						$data[$i] = str_replace(array(",軽自動","軽自動")," 軽自動",$data[$i]);
						$data[$i] = str_replace(array("合","卦","會"),"合",$data[$i]);
						$data[$i] = str_replace(array("ＥＮＥｏｓ"),"ＥＮＥＯＳ",$data[$i]);
						$data[$i] = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data[$i]);
						$data[$i] = str_replace(array("オ寸","本寸"),"村",$data[$i]);
						$data[$i] = str_replace(array("言舌"),"話",$data[$i]);
						$data[$i] = str_replace("」R","ＪＲ",$data[$i]);
						$data[$i] = str_replace("廿","サ",$data[$i]);
						$data[$i] = str_replace("巾","市",$data[$i]);
						$data[$i] = str_replace(array("り叙","り全"),"り金",$data[$i]);
						$data[$i] = str_replace(array("レ生費"),"厚生費",$data[$i]);
						$data[$i] = str_replace(array("篤期"),"短期",$data[$i]);
						$data[$i] = str_replace(array("□","■"),",",$data[$i]);
						$data[$i] = str_replace(array("消費脱","消費説"),"消費税",$data[$i]);
						$data[$i] = str_replace(array("1名"),"１名",$data[$i]);
						$data[$i] = str_replace(array("Ｈ★"),"Ｈ",$data[$i]);
						$patterns = array ('/([0-3]{1}[0-9]{1})－([0-1]{0,1}[0-9]{1})－([0-3]{0,1}[0-9]{1})/imu');
						$replace = array ('○■\1年\2月\3日□,');
						$data[$i] = preg_replace($patterns, $replace, $data[$i]);
						$patterns = array ('/([0-9]+)○([0-9]+)/imu');
						$replace = array ('◆\1◆\2◆');
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
						//var_dump($data[$i]);
						$patterns = array ('/([0-9]+)/imu');
						$replace = array ('■\1□');
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);

						$patterns = array ('/,■([0-9]{1,2})□(.?+)/imu');
						$replace = array ('\1\2');
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);


						$patterns = array ('/■([0-9]+)□(.?+)[■,□]+([0-9]+)[■,□]+([0-9]+)/imu');
						$replace = array ('\1,\2,\3,\4');
						$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);

						$data2[$i] = str_replace(array("■","□"),"★",$data2[$i]);
						$data2[$i] = str_replace(array("◆","□"),"",$data2[$i]);
						$data2[$i] = str_replace(array("■","△■"),"",$data2[$i]);
						$data2[$i] = str_replace(array("…","●","●,●","≒",",●","●,",",,","‥","ノ","/",",て","¶","］","ぺ","て","－\n","｜","■","□"),"",$data2[$i]);
						$data2[$i] = str_replace(array("。",".","．"),"",$data2[$i]);
						$data2[$i] = str_replace(array("彳 ,"),"イ",$data2[$i]);
						//var_dump($data[$i]);
						$data[$i] = str_replace(array("■"),",",$data[$i]);
						$data[$i] = str_replace(array("■","□","◆","□"),"",$data[$i]);
						$data[$i] = str_replace(array("■","△■"),"",$data[$i]);
						//$data[$i] = str_replace(array(",1,"),",",$data[$i]);

						//▼文字列変換の場所▼
						$data[$i] = str_replace(array(",-")," ‐ ",$data[$i]);
						$data[$i] = str_replace(array(",,","ｌ,ｌ","ｌ,ｌ,ｌ"),",",$data[$i]);
						$data[$i] = str_replace(array(",へ,"),"へ,",$data[$i]);
						$data[$i] = str_replace(array("ＫＤＤ1"),"ＫＤＤＩ",$data[$i]);
						$data[$i] = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data[$i]);
						$data[$i] = str_replace(array("食年代"),"食事代",$data[$i]);
						$data[$i] = str_replace(array("商年"),"商事",$data[$i]);
						$data[$i] = str_replace(array("ＥＴＣ","ＥＴｃ","耳Ｔｃ"),"ＥＴＣ ",$data[$i]);
						$data[$i] = str_replace(array("彳"),"イ",$data[$i]);
						$data[$i] = str_replace(array("¥","$","#","="),"￥",$data[$i]);
						$data[$i] = str_replace(array(":-"),".",$data[$i]);
						$data[$i] = str_replace(array("…","●","●,●","≒",",●","●,",",,","‥","ノ","/",",て","¶","］","ぺ","て","－\n","－","－","｜","■","□"),"",$data[$i]);
						$data[$i] = str_replace(array("現全","現歛"),"現金",$data[$i]);
						$data[$i] = str_replace("収人","収入",$data[$i]);
						$data[$i] = str_replace("仕人","仕入",$data[$i]);

						$data2[$i] = str_replace(array("現全","現歛"),"現金",$data2[$i]);
						$data2[$i] = str_replace("収人","収入",$data2[$i]);
						$data2[$i] = str_replace("仕人","仕入",$data2[$i]);
						$data2[$i] = str_replace(array("レ生費"),"厚生費",$data2[$i]);
						$data2[$i] = str_replace(array("篤期"),"短期",$data2[$i]);
						$data2[$i] = str_replace(array("吐長","杜長"),"社長",$data2[$i]);
						$data2[$i] = str_replace(array("消費脱","消費説"),"消費税",$data2[$i]);
						$data2[$i] = str_replace(array("□","■"),"★",$data2[$i]);
						$data2[$i] = str_replace(array("★★"),"A",$data2[$i]);

						$cnt = count($data[$i]);
						$cnt2 = count($data2[$i]);
						$cnt3 = count($data3[$i]);
						$cnt4 = count($data4[$i]);

						$pay_date = 0;
						$last_month = 0;
						$account_exist_flag = 0;
						
//						for ( $i = 0; $i < $cnt; $i++ ) {
						//1行ごとの処理
							$tax_case = 0;
							$delete_line = 0;
							$account_flag = 0;
							//echo "<br>";
							//echo $data[$i];
							//echo "<br>";
							//echo $filename;
							//echo "<br>";
							

							//▼文字列変換の場所▼
							//$patterns = array("/\?{1}/","/�{1}/","/.{1}輌費/","/新闘代/","/呪金/","/陶.{1}/","/.{1}通.*金1{0,1}/imu","/普.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売[\SA-Z\w]{0,3}高/imu","/売[\SA-Z\w]{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/[\SA-Z\w]{1}合/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/石油.*ン代/","/が.*ン代/","/ＳＳ.*リン代/","/カ.{1,2}ン代/","/.{1}録諸費.{1}/","/ＥＮＥ.{2}/","/コン.*タン.{1}/","/租.*公課/","/阻.*公課/","/法.{0,1}脱及び住民税/","/民脱/","/唯給/","/外注加.*費/");
							//$replace = array('','','車輌費','新代','現金','複合','普通預金1','普通預金1','普通預金1','売上高','売上高','複合','複合','受取手形','未払金','未払金','石油ガソリン代','ガソリン代','ＳＳガソリン代','ガソリン代','登録諸費用',"ＥＮＥＯＳ","コンサルタント","租税公課","租税公課","法人税及び住民税","民税","雑給","外注加工費");
							$patterns = array("/([0-9]{0,4})名/","/内税.*額/","/.{0,1}式会.{0,1}/","/法定福.*費/","/[ぶ]{1}\S{0.2}代/","/\S{0,1}[イト仆]{2}代/","/[ハ八]{0,2}[日目]{1}弋/","/り全/","/預.{0,2}金/","/中輌費/","/新闘代/","/呪金/","/陶.{1}/","/筏.{1}/","/通預企/","/.{1}通.*金1{0,1}/imu","/普.*金1{0,1}/imu","/矜涌附金1{0,1}/imu","/売[\SA-Z\w]{0,3}高/imu","/売[\SA-Z\w]{0,3}鳥/imu","/複[\SA-Z\w]{1}/imu","/[\SA-Z\w]{1}合/imu","/受取.*形/imu","/末.{1}金/","/未.{1}金/","/レッスン代/","/[かがカガヵ力]{1}[\S\W]{1,2}リン代/","/ＳＳ.*リン代/","/ＥＮＥ.{2}/","/コス.*油/","/石油.*ン代/","/ＥＮＥＯＳ.*ン代/","/出張費.*ン代/","/.{1}録諸費.{1}/","/コン.*タン/","/租.*公課/","/阻.*公課/","/法.{0,1}脱及び住民税/","/民脱/","/市県.{1}税/","/所.{0,1}税/","/所.{0,1}脱/","/所.{0,1}悦/","/所得礎/","/所得悦/","/所得脱/","/唯給/","/誰給/","/雑紿/","/外注加.*費/","/貸劉/","/繰人/","/貸.*繰入/","/貸.*戻入/","/減.*却費/","/仕.*勘定/","/他人.*勘定/","/什.*勘定/","/他入.*勘定/","/減価.*計額/");
							$replace = array("$1名","内税起票額","株式会社","法定福利費","バイト代","バイト代","バイト代",'り金','預り金','車輌費','新聞代','現金','複合','複合','通預金','普通預金1','普通預金1','普通預金1','売上高','売上高','複合','複合','受取手形','未払金','未払金','レッスン代','ガソリン代','ＳＳガソリン代',"ＥＮＥＯＳ","コスモ石油",'石油ガソリン代','ＥＮＥＯＳガソリン代','出張費ガソリン代','登録諸費用',"コンサルタン","租税公課","租税公課","法人税及び住民税","民税","市県民税","所得税","所得税","所得税","所得税","所得税","所得税","雑給","雑給","雑給","外注加工費","貸倒","繰入","貸倒引当金繰入","貸倒引当金戻入","減価償却費","仕入調整勘定","仕入調整勘定","仕入調整勘定","仕入調整勘定","減価償却累計額");

							$data[$i] = preg_replace($patterns, $replace, $data[$i]);
							$data2[$i] = preg_replace($patterns, $replace, $data2[$i]);
							$data4[$i] = preg_replace($patterns, $replace, $data4[$i]);
							////echo "<br>";
							
							$data4[$i] = str_replace(array("]","L","I","！","!"),"1",$data4[$i]);
							$data4[$i] = str_replace(array("Ｉ","！"),"1",$data4[$i]);
							$data4[$i] = str_replace(array("z"),"2",$data4[$i]);
							$data4[$i] = str_replace(array("會","輻"),"3",$data4[$i]);
							$data4[$i] = str_replace(array("繍","ｘ"),"4",$data4[$i]);
							$data4[$i] = str_replace(array("轟","§"),"5",$data4[$i]);
							$data4[$i] = str_replace(array("s"),"6",$data4[$i]);
							$data4[$i] = str_replace(array("讐","Ｓ","B","ｓ","β"),"8",$data4[$i]);
							$data4[$i] = str_replace(array("g","q"),"9",$data4[$i]);

							//▲文字列変換の場所▲
							
							//echo $data[$i];echo " 1<br>";//摘要判定
							//echo $data2[$i];echo " 2<br>";//科目判定
							//echo $data4[$i];echo " 4<br>";//日付判定
							//echo "<br>data";
							$data4[$i] = str_replace(array(",","○"),array("","★"),$data4[$i]);
							echo "<br>data3";
							echo $data3[$i] = str_replace(",","",$data4[$i]);
							echo "<br>";
							
							if ($_REQUEST["m"] == ""){
								$financial_month = $_REQUEST["m"];
							} else {
								$financial_month = $financial_month;
							}
							if ($_REQUEST["y"] == "") {
								$year = $fiscal_year;
							} else {
								$year = $_REQUEST["y"];
							}

							
							//日付の取得
							//$pattern_comp_end = "/([1-3]{0,1}[0-9]{1})[-－\s\t 　]{0,2}([0-1]{0,1}[0-9]{1})[-－\s\t 　]{0,2}([0-3]{0,1}[0-9]{1}).*\W([0-9]{3,10}).*\W([0-9]{3,10})/is";//echo "　ここまで<br>";
							$pattern_comp_end = "/([1-9]{0,3}[0-9]{0,1})年([0-1]{0,1}[0-9]{1})[月]{0,3}([0-3]{0,1}[0-9]{1})[日]{0,3}\W*([0-9]{3,10})円(.*★{0,3}\W*)([0-9]{3,10})円/is";//echo "　ここまで<br>";
							$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							if ($match_num == 0) {
								$pattern_comp_end = "/()([0-1]{0,1}[0-9]{1})月([0-3]{0,1}[0-9]{1})日/is";//echo "　ここまで<br>";
								$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							}
							if ($match_num == 0) {
								$pattern_comp_end = "/([1-9]{0,3}[0-9]{0,1})[年\-]{0,3}([0-1]{0,1}[0-9]{1})[月\-]{0,3}([0-3]{0,1}[0-9]{1})[日]{0,3}/is";//echo "　ここまで<br>";
								$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							}
	
							if ($match_num > 0) {
								//echo "data3";
								$data3[$i];//echo "<br>";
								//var_dump($match);
								//echo "<br>match";
								
								$month = intval($match[2][0]);
								$day = intval($match[3][0]);
								$remarks1 = "";
								
								if ($month >= 1 && $month <= $financial_month) {
									$year += 1;
								}
								echo "<br>日付<br>";
								if ($match[1][0]-12 >= 10 && $match[1][0]-12 <= 30 ) {
								echo	$ydate = "20".($match[1][0]-12)."-".$match[2][0]."-".$match[3][0];
								echo "<br>";
								} else {
								echo 	$ydate = $year."-".$match[2][0]."-".$match[3][0];
								echo "<br>";
								}
							}
							
							//日付以外の情報の取得
							$pattern_comp_end = "/()()()([0-9]{2,10})円(.*★{0,3}\W*)([0-9]{2,10})円/is";//echo "　ここまで<br>";
							$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							if ($match_num == 0) {
								$pattern_comp_end = "/()()()\W*￥￥([0-9]{3,10})(.*★{0,3}\W*)￥￥([0-9]{3,10})/is";//echo "　ここまで<br>";
								$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							}
							if ($match_num == 0) {
								$pattern_comp_end = "/()()()\W*￥￥([0-9]{3,10})(.*★{0,3}\W*)￥￥([0-9]{3,10})/is";//echo "　ここまで<br>";
								$match_num = preg_match_all($pattern_comp_end, $data3[$i], $match);
							}

							
							if ($match_num > 0) {
								echo "<br>金額<br>";
								
								echo $money = $match[4][0];
								echo "<br>";
								echo $remarks1 = str_replace("★","",$match[5][0]);
								echo "<br>";
								echo $sum = $match[6][0];
								echo "<br>";
								echo $money2 = ABS($sum - $pre);
								echo "<br>";
								$pre = $sum;
								
							}
							//勘定科目を取得する
							$p_cnt = 0;
							$match = "";
							$account_data = "";
							$sum_month = 0;
							$match_num1 = 0;
							$match_num2 = 0;
							$match_num = 0;
							$other_flag = 0;
							$journalizing_match_flag = 0;
							
							if ((strlen($data[$i]) > 10 && $other_flag == 0)) {
								$journalizing_match_flag = 1;
								
								if ($sum_month == 1) {
								//echo "月";
									$last_month = $last_month + 1;
								}
								
								$date_arr =  explode(",",str_replace(array(".","．","。"),",",$match[1]));

								if ($date_arr[0] > 12) {
									$date_arr[0] = substr($date_arr[0],-1,1);
								}
								if ($date_arr[1] > 31) {
									$date_arr[1] = substr($date_arr[1],-2,1);
								}
								
								$filename = str_replace("txt","jpg",$filename);
								$pattern = '/^,/i';
								$replacement = '';
								$data[$i] = preg_replace($pattern, $replacement, $data[$i]);
								$data_arr = explode(",",$data[$i]);
								$date_arr = explode("/",$pay_date);
								
								if ($month >= 1 && $month <= $financial_month) {
									$year += 1;
								}
								
								$str = "";
								$remarks1 = str_replace("\r","",$remarks1);

								if ($fixed_data_b1 != $ydate.",".$money.",".$sum.",".$money2.",".$pl.",".$remarks1.",".$filename."\r") {
									echo	$fixed_data .= $ydate.",".$money.",".$sum.",".$money2.",".$pl.",".$remarks1.",".$filename."\r";
									$fixed_data_b1 = $ydate.",".$money.",".$sum.",".$money2.",".$pl.",".$remarks1.",".$filename."\r";
									echo "<br>";
								}
							}//if()

						// ファイルのパスを変数に格納
						//開発用
						//$FileText = $txt_dir."/".$dlp."ci.txt";
						$FileText = $txt_dir."/TextConvertBankResult/".$dlp.".csv";
						$fixed_data = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n"),"\n",$fixed_data);
						// ファイルに書き込む
						file_put_contents($FileText,$fixed_data,LOCK_EX);
						//file_put_contents($FileText,$output_data,LOCK_EX);
						
						
					}//1行ごとの処理の終わり for ( $i = 0; $i < $cnt; $i++ )
					
					//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
					//入れる金額が無かった場合、一番上の金額を入れる。
				}//if ( ==png)

				//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
				//$AddWords = "\r\n".date('Y/m/d',strtotime($got_date)).",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$industry_type_s;
				//file_put_contents($FileText, $AddWords, FILE_APPEND);

				//全ファイル記録用ファイルに追記
				$FileText2 = $txt_dir."/TextConvertBankResult/convert_data_all.csv";
				$outcode = "sjis-win";
				$incode = "UTF-8";
				$nl = "\n";
				$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
				$this ->convertTextCode($FileText , $outcode, $FileText2 , $outcode, $nl);
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
			//取得したTEXTファイルを削除する。
	//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//$counter++;
			return $FileText;
//		}//for文の終わり
	}//メソッドの終わり

	/**
	* テキストファイルの文字コードを変換し保存する
	* @param string $infname  入力ファイル名
	* @param string $incode   入力ファイルの文字コード
	* @param string $outfname 出力ファイル名
	* @param string $outcode  出力ファイルの文字コード
	* @param string $nl       出力ファイルの改行コード
	* @return string メッセージ
	*/
	function convertCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'wb');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}
	
	function convertTextCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
		$instr = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n","\n\n"),"\n",$instr);
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'a');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}
	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}


	function recurse_chown_chgrp($mypath,$uid, $gid) {
		if (mkdir( $mypath, 0775, true ) ) {
			chmod( $mypath, 0775);
			chown( $mypath, $uid);
		    chgrp( $mypath, $gid);
		  //echo "ディレクトリ作成成功！！";
		} else {
		  //echo "ディレクトリ作成失敗！！";
		}
	}

	function getFileList($dir) {
	    $files = glob(rtrim($dir, '/') . '/*');
	    $list = array();
	    foreach ($files as $file) {
	        if (is_file($file)) {
	            $list[] = $file;
	        }
	        if (is_dir($file)) {
	            $list = array_merge($list, getFileList($file));
	        }
	    }
	 
	    return $list;
	}

	function random($length = 12) {
	    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
	    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
	}
}//classの終わり

?>