<?php require("header.php");?>

<title>楽たす請求書画像のテキスト化 - Cloud Invoice</title>

<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words = "";
$flag = "";
$sql = "";
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
if ($_SESSION['ret_msg']){
	$up_msg = $_SESSION['ret_msg'];
	unset($_SESSION['ret_msg']);
}
if ($_SESSION['raku_csv_msg']){
	$raku_csv_msg = $_SESSION['raku_csv_msg'];
	unset($_SESSION['raku_csv_msg']);
}

//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}
$dir_path = dirname(__FILE__)."/../pdf/png/".$af_id;
$dir_path4 = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/RakutasuOCRResult";

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}
?>
<script type="text/javascript">
$(function() {
  $('#SendMailBtn').prop('disabled', true);

  $('#mail_flag').on('click', function() {
    if ( $(this).prop('checked') == false ) {
      $('#SendMailBtn').prop('disabled', true);
    } else {
      $('#SendMailBtn').prop('disabled', false);
    }
  });
});
</script>
<article>
	<section id="m-1-box">
		<h2>
			楽たす請求書
		</h2>
		<p>
		請求書をテキスト化します。<br>
		</p>

		<div style="margin-top:40px;">
		<p>■楽たす請求書電子化サービスログイン</p>
		<p><input type="button" name="journalize" onclick="window.open('https://mng-furikomi.bizsky.jp/Bpo/Login?returnUrl=%2fbpo','')" value="楽たすログイン"/></p>
		</div>
		
		<p>■到着メール送信（MJSへ）</p>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form id="sendMailForm" name="form_sansho" action="./rakutasu_send_mail" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./rakutasu_imgs_to_text_upload_all" />
			</div>
			<input type="text" name="cs_no" value="">:顧客番号<br>
			<input type="text" name="cs_name" value="">:顧客名<br>
			<input type="text" name="ind_num" value="">:請求書実数<br>
			<input type="text" name="received_date" value="<?php echo date('Y/m/d');?>">:到着日<br>
			<label><input type="checkbox" id="mail_flag" name="mail_flag" value="1">:メールを送る</label><br>
			<input type="button" id="SendMailBtn" onclick="submit();" value="到着メール送信" /><br>
		</form>
		<?php echo "<span style='color:blue;'>".$_SESSION['mail_comment']."</span>";?>

		
		<p>■画像アップロード</p>
		<form name="form_sansho" action="./upload_rakutasu_imgs_for_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="unzip_flag" value="1" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="画像アップロード" />
     		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		</form>

		<p>■テキスト化</p>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./upload_rakutasu_imgs_to_text_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./rakutasu_imgs_to_text_upload_all" />
				<?php $_SESSION['comment2'] = "";?>
			    
			</div>
			<input type="text" name="cs_no" value="">:顧客番号※必須<br>
			<input type="text" name="cs_name2" value="">:請求者から省く顧客名の一部を書く<br>
			<input type="button" onclick="submit();" value="テキスト化開始" /><br>
		</form>
		<p>■請求書支払依頼書テキスト化</p>
		<form name="form_sansho" action="./upload_rakutasu_format_imgs_to_text_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./rakutasu_imgs_to_text_upload_all" />
				<?php $_SESSION['comment2'] = "";?>
			    
			</div>
			<input type="text" name="cs_no" value="">:顧客番号※必須<br>
			<input type="button" onclick="submit();" value="請求書支払依頼書テキスト化開始" /><br>
		</form>

		<?php echo "<span style='color:blue;'>".$_SESSION['comment2']."</span>";?>

		<p>■アップロード用CSV加工</p>
		<form name="form_sansho" action="./upload_rakutasu_imgs_for_csv" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./rakutasu_imgs_to_text_upload_all" />
				<input type="hidden" name="csv_flag" value="1" />
			</div>
			<input type="button" onclick="submit();" value="CSVアップロード" />
     		<?php echo "<span style='color:red;'>".$raku_csv_msg."</span>";?>
		</form>
		
		<div style="margin-top:40px;">
		<p>■テキスト化したデータを手動で取り出す</p>
		<input type="button" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path;?>&type=*&opt=r'" value="テキスト化したファイルをダウンロード"/>
		</div>
	</section>
	<p>　</p>
</article>
<?php 
	$_SESSION['comment'] = "";
	$_SESSION['comment2'] = "";
	$_SESSION['mail_comment'] = "";
?>
<?php require("footer.php");?>