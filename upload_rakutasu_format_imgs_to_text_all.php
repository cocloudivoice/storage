<?php
session_start();

//処理制限時間を外す
set_time_limit(0);

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
include_once(dirname(__FILE__)."/../pdf/CloudVisionTextDetection.class.php");
//自動仕分け用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = $_REQUEST['path'];
$file_name = $_FILES['upfile']['name'];
//$ac_no = $_REQUEST['ac_no'];//受付番号
//$cs_name = $_REQUEST['cs_name'];//顧客名
$cs_name2 = $_REQUEST['cs_name2'];//請求者から省く顧客名の一部
$cs_no = $_REQUEST['cs_no'];//顧客番号
//$p_num = $_REQUEST['p_num'];//申告通数
//$ind_num = $_REQUEST['ind_num'];//実通数
//$mail_send = $_REQUEST['mail_flag'];//メールフラグ


$upload_dir_path = $path;//dirname(__FILE__)."/../pdf/png/".$af_id;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;
$AllData = "請求書番号,支払期日,請求元会社名※,銀行名※,支店名※,口座種別※,口座番号※,口座名義人※,請求金額※,画像名※";
//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
//$txt_dir = dirname(__FILE__)."/../pdf/png/".$af_id;
if ($upload_dir_path == "" && $upload_dir_path == NULL) {
	$upload_dir_path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/".$_SESSION["af_id"];
}

$txt_dir = $upload_dir_path;

//画像を最後に入れるディレクトリを作成
mkdir($txt_dir."/"."Data", 0777);
mkdir($txt_dir."/Data/"."Images", 0777);

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//$upload_dir_pathのフォルダが存在しない場合に作る。
recurse_chown_chgrp($upload_dir_path, $uid, $gid);

setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');

mb_language("ja");
mb_internal_encoding("UTF-8");

/*
//到着メール送信機能を別にしたためコメントアウト
if ($mail_send == 1 && $cs_name != "") {
	
    //$email= "arisa_nagano@mjs.co.jp,ayaka_tamaki@mjs.co.jp";
	$email= "bizsky_office@mjs.co.jp,cc@cloudinvoice.co.jp";
	$headers = "From:".mb_encode_mimeheader("クラウドインボイス 楽たす事務係","ISO-2022-JP-MS")."<info@cloudinvoice.co.jp>";
	$subject = "請求書が到着しました。【クラウドインボイス 楽たす事務係】" ;
	$message ="
ミロク情報サービス
長野様
玉置様

お世話になっております。
クラウドインボイスの楽たす事務係です。

請求書が到着致しましたのでご連絡いたします。
到着日：".date('Y/m/d')."
顧客名：".$cs_name."
顧客番号：".$cs_no."

以上、よろしくお願い致します。

";
	
	$message = str_replace(array("\r\n","\r"),"\n",$message);
	//$message = mb_convert_encoding($message,"ISO-2022-JP-MS");
	//mail($email,$subject,$message,$headers);
	$message = str_replace(array("\r\n", "\r"), "\n", $message);
	mb_send_mail($email,$subject,$message,$headers);
	//mail($email,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'SJIS'),$headers);

}
*/

$_FILES['upfile']['name'] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$_FILES['upfile']['name']);
$_FILES['upfile']['name'] = str_replace(array(".PNG"),".png",$_FILES['upfile']['name']);
$_FILES['upfile']['name'] = str_replace(array(".tiff"),".tif",$_FILES['upfile']['name']);

$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}

$file_list_arr = getFileList($upload_dir_path);
$k = 0;
//var_dump($file_list_arr);
for($i = 0;$i < count($file_list_arr); $i++) {
	//echo $file_list_arr[$i];
	//echo "<br>";
	//if (substr($file_list_arr[$i], -3, 3) == "png" || substr($file_list_arr[$i], -3, 3) == "jpg") {
	//echo "a<br>";
	if ( substr($file_list_arr[$i], -3, 3) == "pdf" || substr($file_list_arr[$i], -3, 3) == "png" || substr($file_list_arr[$i], -3, 3) == "PDF" || substr($file_list_arr[$i], -3, 3) == "PNG" || substr($file_list_arr[$i], -3, 3) == "jpg"|| substr($file_list_arr[$i], -3, 3) == "JPG" || substr($file_list_arr[$i], -3, 3) == "jpeg"|| substr($file_list_arr[$i], -3, 3) == "JPEG" || substr($file_list_arr[$i], -3, 3) == "tif"|| substr($file_list_arr[$i], -3, 3) == "bmp"|| substr($file_list_arr[$i], -3, 3) == "BMP") {
		$img_file_arr[$k] =  basename($file_list_arr[$i]);
		//echo "aaa<br>";
		$k++;
	}
}
/*
//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}

//▲企業データの取得▲
*/

if ($img_file_arr) {
	
	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	$counter = 0;
	$error_num = 0;
    //$file_ary = reArrayFiles($img_file_arr);
	//$file_num = count($file_ary);
	//txtだけを先に読み込む。ブサイクだけどとりあえず。
	for($m = 0;$m < count ($img_file_arr) ;$m++) {
		if(!file_exists($txt_dir."/".substr($img_file_arr[$m], 0, -4).".txt")) {
			if ( substr($img_file_arr[$m], -3, 3) == "pdf" || substr($img_file_arr[$m], -3, 3) == "png" || substr($img_file_arr[$m], -3, 3) == "PDF" || substr($img_file_arr[$m], -3, 3) == "PNG" || substr($img_file_arr[$m], -3, 3) == "jpg"|| substr($img_file_arr[$m], -3, 3) == "JPG" || substr($img_file_arr[$m], -3, 3) == "jpeg"|| substr($img_file_arr[$m], -3, 3) == "JPEG"   || substr($img_file_arr[$m], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp") {
				//ファイル名変換処理
//				$img_file_arr[$m] = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);
				$file_name_top = substr($img_file_arr[$m], 0, -4);//echo ":top<br>\r\n";
				$file_extension = substr($img_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
				$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
				$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";

				$pdf_name = substr($filename, 0, -4);
				
				$im = new imagick();
				
				$file_path = $upload_dir_path."/";
				$pdf_file = $file_path.$file_name_top.".pdf";
				//for ($m = 1;$m <= 120;$m++) {
				if ($file_extension == ".png" || $file_extension == ".jpg" || $file_extension == ".bmp" ) {
					//echo "bmp2";
					shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
/*
					//ファイル名が出力ファイル名と違う時。
					if ($file_name_top != $dlp) {
					
						//pngファイルの時、出力ファイル名(dlp)と同名でpngを作り直し、元ファイルを消す。 
						if ($file_extension == ".png") {
							shell_exec("\mv -f ".$file_path.$file_name_top.".png"." ".$file_path.$dlp.".png 2>&1");
							shell_exec("rm -f ".$file_path.$file_name_top.".png 2>&1");
						}
					}

*/
					//jpgファイルの時、出力ファイル名(dlp)と同名でpngを作り直し、元ファイルを消す。
					if ($file_extension == ".jpg") {
						//(mogrify -resize 320x -unsharp 2x1.4+0.5+0 -colors 65 -quality 100 -verbose *.png);
						//shell_exec("mogrify -resize 768x -unsharp 2x1.4+0.5+0  ".$file_path.$file_name_top.$file_extension." 2>&1");
						shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".jpg 2>&1");
						//shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".png 2>&1");	
						//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.$file_extension." 2>&1");
						//shell_exec("convert ".$file_path.$dlp.$file_extension."  -colorspace gray  ".$file_path.$dlp.$file_extension." 2>&1");
						
						//shell_exec("convert ".$file_path.$dlp.$file_extension." -negate ".$file_path.$dlp.$file_extension." 2>&1");

					}

					//echo $dlp."png";
					//shell_exec("convert ".$file_path.$dlp.$file_extension." -negate ".$file_path.$dlp.$file_extension." 2>&1");
					//shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//$ImgText = $cloudvision_con -> ImagetoText($file_path.$dlp.$file_extension,$feature);

					$ImgText = $cloudvision_con -> ImagetoText($file_path.$dlp.$file_extension,$feature);
					if ($ImgText == "") {
						//echo "2nd<br>";
						$ImgText = $cloudvision_con -> ImagetoText($file_path.$file_name_top.$file_extension,$feature);
						//var_dump($ImgText);exit;
					}

	//				$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
	//				$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1");
					//var_dump($res);
					//echo "/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1";
					//echo "<br>";
					
					// ファイルのパスを変数に格納
					$FileText = $file_path.$dlp.'.txt';
					// ファイルに書き込む
					file_put_contents($FileText,$ImgText,LOCK_EX);
					// ファイルを出力する
					//readfile($FileText);
					//exit;
					
				}
				if ($file_extension == ".tif" ||$file_extension == ".jpg" ||$file_extension == ".png") {//echo "tifです。";exit;
					//shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
				//	shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
					$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."2 -l jpn 2>&1");
					//var_dump($res);
				}
				if ($file_extension == ".bmp") {
					//shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
			//		shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1");
					//var_dump($res);
				}

					//PDFファイルをテキスト化する。
					//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png 2>&1");
					//echo "convert ".$file_path.".pdf -geometry 100% ".$file_path.".png";
				if ($file_extension == ".pdf") {
					//pdftotextでpdfから
					//shell_exec("pdftotext -raw ".$file_path."6988b98f05ca4df774733315f7725519.pdf ".$file_path."6988b98f05ca4df774733315f7725519.txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					shell_exec("pdftotext -raw ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					//shell_exec("pdftotext ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png ;");
					shell_exec("convert -density 600 ".$file_path.$dlp.".pdf ".$file_path.$dlp.".png > /logs/cloudinvoice/makeimg/moveImage/errlog".date('Ymdhis').".log 2>&1");
					//print_r ($output);
				}
				
				if ($file_extension == ".jpg") {
					//(mogrify -resize 320x -unsharp 2x1.4+0.5+0 -colors 65 -quality 100 -verbose *.png);
					shell_exec("mogrify  -density 200 ".$file_path.$file_name_top.$file_extension." 2>&1");
					shell_exec("mogrify -resize 768x -unsharp 2x1.4+0.5+0  ".$file_path.$file_name_top.$file_extension." 2>&1");
					//shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".png 2>&1");	
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.$file_extension." 2>&1");
					//shell_exec("convert ".$file_path.$dlp.$file_extension."  -colorspace gray  ".$file_path.$dlp.$file_extension." 2>&1");
					
					//shell_exec("convert ".$file_path.$dlp.$file_extension." -negate ".$file_path.$dlp.$file_extension." 2>&1");

				}
				
					//var_dump($retval);
					//var_dump($last_line);
				$path = $file_path;
			}
		}

		try {		
//			shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
//			shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
			shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			shell_exec("\mv -f ".$file_path.$dlp.".jpg ".$file_path."Data/Images/".$dlp.".jpg 2>&1");
			//shell_exec("rm -f ".$file_path.$dlp.".jpg 2>&1");
		} catch (Exception $e) {
			echo $e -> getMessage;
		}
	//	$DataNum++;
	}
	$_SESSION["comment2"] = "画像をOCRでデータ化しました。";

	//画像を読み込みながら請求書データを作成し、txtの内容を取得して上書きする。
	for($m = 0;$m < count ($img_file_arr) ;$m++) {//echo d;
		
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name = "";
		$product_name_arr = "";
		$account = "";
		$acnt_name_arr = "";
		$account_name = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		$account_num = "";
		$account_holder = "";
		$blanch_name = "";
		
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$acnt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();
		$bank_flag = 0;//最初の銀行取得フラグ
		$company_arr = array();
		$company_cnt = 0;
		$company_num_cnt = 0;
		$account_holder_arr = array();
		$account_holder_cnt = 0;
		$account_holder_num_cnt = 0;
		$blanch_arr = array();
		$blanch_cnt = 0;
		$blanch_num_cnt = 0;
		$acnt_hld_cnt = 0;
		$invoice_num = "";
		$ac_off_flag = 0;
		$account_holder_off_cnt = 0;
		$send_company_arr = array();
		
		try {
	    	//var_dump($file);
	        $filename = $img_file_arr[$m];
	        if (substr($filename, -3, 3) == "csv"){
	        	$csv_file_name = $filename;
	        }
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$img_file_arr[$m] = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);
			$file_name_top = substr($img_file_arr[$m], 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($img_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			
			//請求書が登録されていないか確認する。
			//$flag ="download_invoice_data_clm";
			//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
			//同じdownload_passwordで請求書が登録済みの場合
			if (substr($filename, -3, 3) == "png" || substr($filename, -3, 3) == "jpg" || substr($filename, -3, 3) == "tif" || substr($filename, -3, 3) == "pdf"|| substr($img_file_arr[$m], -3, 3) == "bmp" ) {
				//請求書登録
				//仮のデータで請求書登録をする。
				//仮の請求書・領収書データの登録
				
				//テキストを読んで上書き処理
				//echo "テキスト処理<br>";
				$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
				$text_file_path = $txt_dir."/".$dlp.".txt";
				//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
				if(file_exists($text_file_path_en)) {
					$data = file_get_contents($text_file_path_en);
					$data .= file_get_contents($text_file_path);
				} else {
					$data = file_get_contents($text_file_path);
				}
				
				$data_all = $data;
				//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

				//var_dump($data);
				$data = str_replace(array("/"," "),array(""),$data);
				$data = str_replace(array("株式会社\n","有限会社\n","法人\n","株式会社\r\n","有限会社\r\n","法人\r\n","\n御中","\r\n御中","\n様","\r\n様"),array("株式会社","有限会社","法人","株式会社","有限会社","法人","御中","御中","様","様"),$data);
				$data = str_replace(array("処理\n","振\n","込\n","手\n","数\n","料\n","口\n","座\n","口座名義\n","口座名職","機関\n","本\n","支\n","摘要\n","項目\n","種類1","番号1"),array("処理","振","込","手","数","料","口","座","口座名義","口座名義","機関","支","摘要","項目","種類","番号"),$data);
				$data = str_replace(array("依頼日\n","依賴日\n","承認\n","担当者\n","翌々月5日払いです。\n","振込希望\n","払先\n","名\n","金額(消費税込〕\n","金額(消費税込)\n","金融\n","金額\n"),array("依頼日","依頼日","承認","担当者","振込希望日","振込希望","払先","名","金額(消費税込〕","金額(消費税込)","金融","金額"),$data);
				$data = str_replace(array("(カ","カ)","(ユ","ユ)"),array("（カ","カ）","（ユ","ユ）"),$data);
				$data = str_replace(array("(",")","[","]","〕","〔"),array(""),$data);
				$data = str_replace(array("ﾌﾘｶﾞﾅ","フリガナ","金融機関名","機関名","機関","支座座","金融","本支店名","店名|","名|","支払先名","払先名","|"),array("","","","","","","","","","","","",""),$data);
				$data = str_replace(array("·"),array("・"),$data);
				var_dump($data);
				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				
				for ( $i = 0; $i < $cnt; $i++ ) {//echo e;
					
					//変数初期化
					$money_num_cnt = 0;
					
					//全角文字を半角に変換
					//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
					$data2[$i] = $data[$i];//商品名と会社名の文字列を取得する時は数字を加工しない方が良いため分ける。
					$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
					$data[$i] = strtr($data[$i], $kana);
					$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
					$data[$i] = str_replace(array("名|","'","'"," ","|",":","：","|","<",">","■","★","【","】"),"",$data[$i]);
					$data[$i] = str_replace(array("、"),",",$data[$i]);
					$data[$i] = str_replace(array("叩","m","0c","0C","0(1"),"00",$data[$i]);
					$data[$i] = str_replace(array("//","{{","[1"),"11",$data[$i]);
					$data[$i] = str_replace(array("株式会社\n","有限会社\n","法人\n","株式会社\r\n","有限会社\r\n","法人\r\n","\n御中","\r\n御中","\n様","\r\n様"),array("株式会社","有限会社","法人","株式会社","有限会社","法人","御中","御中","様","様"),$data[$i]);
					
					$data[$i] = str_replace(array("]"),"1",$data[$i]);
					$data[$i] = str_replace(array("繍"),"4",$data[$i]);
					$data[$i] = str_replace(array("§"),"5",$data[$i]);
					$data[$i] = str_replace(array("a","讐","B"),"8",$data[$i]);
					$data[$i] = str_replace(array("g","q"),"9",$data[$i]);
					$data[$i] = str_replace(array("･","·"),"・",$data[$i]);
					$data[$i] = str_replace(array("ff"),"年",$data[$i]);
					$data[$i] = str_replace(array("隷","A"),"月",$data[$i]);
					$data[$i] = str_replace(array("B","t1","曰"),"日",$data[$i]);
					$data[$i] = str_replace(array("年明","#明"),"年8月",$data[$i]);
					$data[$i] = str_replace(array("年9A","#9A"),"年9月",$data[$i]);
					$data[$i] = str_replace(array("口座名義株式会社","座名義株式会社","名義株式会社"),"株式会社",$data[$i]);
					$data[$i] = str_replace(array("振込銀行","取引銀行","下記銀行","下記の銀行","以下の銀行","支店名","お支払いは","お振込みは","お振込は","お振込の","御振込先","お振込先","振込先","振込先銀行"),"",$data[$i]);
					$data[$i] = str_replace(array("明旧"),"8月1日",$data[$i]);
					$data[$i] = str_replace(array("¨","－","一","・","~"),"-",$data[$i]);
					$data[$i] = str_replace(array("合言 "),"合計",$data[$i]);
					$data[$i] = str_replace(array("卦","會"),"合",$data[$i]);
					$data[$i] = str_replace(array("小","′」ヽ"),"小",$data[$i]);
					$data[$i] = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data[$i]);
					$data[$i] = str_replace(array("オ寸","本寸"),"村",$data[$i]);
					$data[$i] = str_replace(array("三菱東京0FJ銀行","三菱東京UFJ銀行"),"三菱東京ＵＦＪ銀行",$data[$i]);
					$data[$i] = str_replace(array("義カ","義か"),array("義カ)","義カ)"),$data[$i]);
					$data[$i] = str_replace("F]","円",$data[$i]);
					$data[$i] = str_replace(array("1EL","1E1","Te1","TE1","E1","e1"),"TEL",$data[$i]);
					$data[$i] = str_replace(array("ﾛ座","0座"),"口座",$data[$i]);
					$data2[$i] = str_replace(array("振込銀行","取引銀行","下記銀行","下記の銀行","以下の銀行","支店名","お支払いは","お振込みは","お振込は","お振込の","御振込先","お振込先","振込先","振込先銀行"),"",$data2[$i]);
					$data2[$i] = str_replace(array("'","'"," ","|",":","：","|","<",">","■","★","【","】"),"",$data2[$i]);
					$data2[$i] = str_replace(array("ﾛ座","0座"),"口座",$data2[$i]);
					$data2[$i] = str_replace(array("7ット"),"フット",$data2[$i]);
					$data2[$i] = str_replace(array("ライ7"),"ライフ",$data2[$i]);
					$data2[$i] = str_replace(array("JT8"),"JTB",$data2[$i]);
					$data2[$i] = str_replace(array("NewDays"),"ニューデイズ",$data2[$i]);
					$data2[$i] = str_replace(array("才リ才ン"),"オリオン",$data2[$i]);
					$data2[$i] = str_replace(array("プ0"),"プロ",$data2[$i]);
					$data[$i] = str_replace(array("¥","$","#","="),"￥",$data[$i]);
					$data[$i] = str_replace(array(":-"),".",$data[$i]);
					
					echo $data[$i];
					echo "<br>";
					//echo $data2[$i];
					//echo "<br>";
					
					//echo "<br>";
					//echo $data[$i];
					//echo "<br>";
					//-を用いた日付で時間がスペースの後に時間がついていた場合取得できなくなるため、スペースを消す前に移動した。
					
					$pattern_comp_end = "/^[*]{1}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data[$i] = str_replace(array("*"),"￥",$data[$i]);
					}
					
					//初期加工のみで利用したい場合の変数
					//echo "<br>";
					$ori_data = $data[$i];

					if ($company_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/(\W{3,45}株式会社\W{3,45})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//支払い側企業名を省く処理
							$match_num2 = 0;
							//echo $match[0];
							//echo "<br>";
							$match2 = "";
							$match3 = "";
							$match4 = "";
							$match_num2 = 0;
							$match_num3 = 0;
							$match_num4 = 0;
							$no_check_flag = 0;
							$match_num2 = preg_match("/御中/", $match[0], $match2);
							$match_num3 = preg_match("/様/", $match[0], $match3);
							$match_num4 = preg_match("/殿/", $match[0], $match4);
							
							if ($data[($i + 2)] == "御中"|| $data[($i + 1)] == "御中"||$data[($i + 2)] == "様"|| $data[($i + 1)] == "様"||$data[($i + 2)] == "殿"|| $data[($i + 1)] == "殿") {
								$no_check_flag = 1;
							}
							//var_dump($match2);	var_dump($match3);
							//echo $no_check_flag."<br>";
							if ($match_num2 == 0 && $match_num3 == 0  && $match_num4 == 0 && $no_check_flag == 0) {
								echo "会社候補<br/>\r\n";
								echo "<br>会社候補";echo $data[$i];var_dump($match);
								$company_arr[$company_cnt] = $data_arr[$i] = str_replace(array("\\","￥","",",","，",".","．","_","＿","‐","","-",""),"",$match[0]);
								//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$company_cnt++;
								$company_num_cnt++;
								$company_data_end_line = $i;
							}
						}
						
					}
					if ($company_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/(.*有限会社.*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//支払い側企業名を省く処理
							$match_num2 = 0;
							//echo $match[0];
							//echo "<br>";
							$match2 = "";
							$match3 = "";
							$match4 = "";
							$match_num2 = 0;
							$match_num3 = 0;
							$match_num4 = 0;
							$no_check_flag = 0;
							$match_num2 = preg_match("/御中/", $match[0], $match2);
							$match_num3 = preg_match("/様/", $match[0], $match3);
							$match_num4 = preg_match("/殿/", $match[0], $match4);
							if ($data[($i + 2)] == "御中"|| $data[($i + 1)] == "御中"||$data[($i + 2)] == "様"|| $data[($i + 1)] == "様"||$data[($i + 2)] == "殿"|| $data[($i + 1)] == "殿") {
								$no_check_flag = 1;
							}
							//var_dump($match2);	var_dump($match3);
							//echo $no_check_flag."<br>";
							if ($match_num2 == 0 && $match_num3 == 0  && $match_num4 == 0 && $no_check_flag == 0) {
								//echo "会社候補<br/>\r\n";
								//echo "<br>会社候補";echo $data[$i];var_dump($match);
								$company_arr[$company_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
								//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$company_cnt++;
								$company_num_cnt++;
								$company_data_end_line = $i;
							}
						}
						
					}

					if ($account_holder_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/(.*名義.*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$match[0] = str_replace(array("様","御中","殿","人","口座","名義"),"",$match[0]);
							//echo "会社候補<br/>\r\n";
							//echo "<br>会社候補";echo $data[$i];var_dump($match);
							$account_holder = $account_holder_arr[$account_holder_cnt] = $data_arr[$i] = str_replace(array("\\","￥",",","，",".","．","_","＿","‐","-"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$account_holder_cnt++;
							$account_holder_num_cnt++;
						}
					}

					//口座名義が口座名義の文言と同じ行になかった場合に取得するため
					if ( $ac_off_flag = 1 && $account_holder == "") {
						//echo "achin";
						$account_holder = $data2[$i];
						$ac_off_flag = 2;
						$account_holder_off_cnt++;
					}
					
					if ($account_holder_num_cnt < 2) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_arr = array("口座名義","口座名","口座","名義","カ）","（カ","（ユ","ユ）","ｶ\)","\(ｶ","\(ﾕ","ﾕ\)");
						
						for ($ach = 0;$ach < count($pattern_arr);$ach++ ) {
							$pattern_comp_end = "/(.*".$pattern_arr[$ach].".*)/";
							$match_num = preg_match($pattern_comp_end, $data[$i], $match);
							if ($match_num >= 1) {
								$match[0] = str_replace(array("口座名義","口座名","口座","名義","人"),"",$match[0]);
								//echo "会社候補<br/>\r\n";
								//echo "<br>会社候補";echo $data[$i];var_dump($match);
								
								$account_holder = $account_holder_arr[$account_holder_cnt] = $data_arr[$i] = str_replace(array("\\","￥",",","，",".","．","_","＿","‐","-"),"",$match[0]);
								//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$ac_off_flag = 1;
								$account_holder_cnt++;
								$account_holder_num_cnt++;
								$account_holder_off_cnt++;
							}
						}
					}

					if ($blanch_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_arr = array("支店");
						for ($ach = 0;$ach < count($pattern_arr);$ach++ ) {
							$pattern_comp_end = "/(.*".$pattern_arr[$ach].".*)/";
							$match_num = preg_match($pattern_comp_end, $data[$i], $match);
							if ($match_num >= 1) {
								
								//echo "支店候補<br/>\r\n";
								//echo "<br>支店候補";echo $data[$i];var_dump($match);
								$blanch_name = $data_arr[$i] = str_replace(array("\\","￥",",","，",".","．","_","＿","‐","-","名1"),"",$match[0]);
								//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$blanch_cnt++;
								$blanch_num_cnt++;
							}
						}
					}

					if  ($acnt_cnt == 0) {
						//振込先口座番号を取得する
						//1をーに変換し直す。
						$product_check_arr = array(".*普通.*",".*当座.*",".*(普).*",".*（普）.*");
						$acnt_cnt = 0;
						$last_product = "";
						$match = "";
						$acnt_arr = array();
						for ($z = 0;$z < count($product_check_arr);$z++) {
							$pattern_comp_end = "/".$product_check_arr[$z]."/";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							if ($match_num >= 1) {
								$acnt_arr[$acnt_cnt] = $match[0];
								if ($acnt_arr[$acnt_cnt] != $last_product) {
									if ($acnt_cnt != 0) {$space = " ";}
									$last_product = $acnt_arr[$acnt_cnt];
									$acnt_name_arr[$acnt_cnt] = $space.$acnt_arr[$acnt_cnt];
									//金額が入っていない時の候補
									$pattern_comp_end = "/([0-9]{6,20})/";
										$match_num2 = preg_match($pattern_comp_end, $data2[$i], $match2);
									if ($match_num2 >= 1) {
										$account_num = $data_arr[$i] = $match2[1];
										$acnt_cnt++;
									}
									$acnt_cnt++;
									break;
								}
							}
						}
					}
					//echo $data2[$i];echo "<br>";
					//口座番号取得
					if  ($acnt_cnt == 1) {
						//口座番号が入っていない時の候補
						$pattern_comp_end = "/([0-9]{6,20})/";
						$match_num2 = preg_match($pattern_comp_end, $data2[$i], $match2);
						if ($match_num2 >= 1) {
							$account_num = $data_arr[$i] = $match2[1];
							$acnt_cnt++;
						}
					}



					//お支払い期限: 2016年05月25日
					$pattern_comp_end = "/期限.*([0-9]{2,4})年([0-9]{1,2})月([0-9]{1,2})日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補a<br/>\r\n";
						//var_dump($match);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
							//echo "登録";
						}
						//exit;
					}
					
					$pattern_comp_end = "/翌々月5日払いです.*([0-9]{2,4})[.,-\/ ]{1,2}([0-9]{1,2})[.,-\/ ]{1,2}([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補a<br/>\r\n";
						//var_dump($match);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}

					$data[$i] = str_replace(array(".","．"),"",$data[$i]);
					$pattern_comp_end = "/振込希望.*([0-9]{4})([0-9]{2})([0-9]{2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補b<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;

							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					
					$pattern_comp_end = "/支払.*([0-9]{2,4}),([0-9]{1,2}),([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補c<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}

					//電話番号用処理
					
					//$tel_test = preg_replace('/[^0-9]/', '', $data[$i]);
					//数字だけ抜き出す処理だと合計金額などと区別しづらいかも。
					$tel_test = str_replace(array("_"),"",$data[$i]);
					if (intval($tel_test) != 0 ) {
						$data[$i] = str_replace(array("(","（","_"),"-",$data[$i]);
					}
					$data[$i] = str_replace(array(" ","　"),"",$data[$i]);
					$data[$i] = str_replace(array("(","（"),"-",$data[$i]);
					$data[$i] = str_replace(array(")","）"),"-",$data[$i]);
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "aaatel<br>\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}[- ]+[0-9]{2,4}[- ]+[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}

					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}\([0-9]{2,4}\)[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							//echo $match[0];
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					if ($tel_cnt == 0) {
						//電話番号取得（番号を取得）
						//echo $data[$i];echo "\r\n";
						$pattern_comp_end = "/[1-9]{1}[0-9]{1,3}-[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{6,8}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
						}
					}
					
					if ($money_num_cnt == 0) {
						//echo $data[$i];
						//$pattern_comp_end = "/合計(\\\w*)/";
						$pattern_comp_end = "/合計.*[額]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "合計金額候補B<br/>\r\n";
							$goukei_top = $goukei = $data_arr[$i] = str_replace(array("\\","￥","円","計","合",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//	$goukei = substr($goukei,2);
							$data_arr[$i] = $data[$i];
							$money_cnt++;
							$money_num_cnt++;
						}
					}
					/*
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/計.*￥(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					*/

					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/消費税[等]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補 計<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","消費税","等",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/請求金額.*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補a<br/>\r\n";
							$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","請求金額","金額","金","御支払",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

				
					if ($money_num_cnt == 0) {
						$data[$i] = str_replace(array("Y","F","軍"),"\¥",$data[$i]);
						$data[$i] = str_replace(array("D","O","〕"),"0",$data[$i]);
						$data[$i] = str_replace(array("∞"),"00",$data[$i]);
						$data[$i] = str_replace(array("ヮ","i","ぅ","ゥ","′","り",",","#"),"",$data[$i]);
						$data[$i] = str_replace(array("\\"),"￥",$data[$i]);
						$pattern_comp_end = "/￥(\d+)/";
						//$pattern_comp_end = "/(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//echo"<br>金額:";echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
							//echo "金額候補\<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥",",","，","。",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";var_dump($money_arr[$money_cnt]);
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}

					if ($money_num_cnt == 0) {
						$data[$i] = str_replace(",","",$data[$i]);
						$pattern_comp_end = "/小計[額]*.*[￥]*(\d*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補 小計<br/>\r\n";
							//echo $data[$i];
							//echo "<br>";
							$syoukei = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","小","額",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/消費税[等]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補 計<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","消費税","等",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
					if ($money_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/請求金額.*(\d+)[円]{0,1}/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
						//	echo "金額候補b<br/>\r\n";
						//echo "<br>金額候補 円";echo $data[$i];var_dump($match);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","金","請求金額","金額","御支払金額","御支払",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
/*
					if ($money_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/[金]*(\d+)円/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
						//echo "<br>金額候補 円";echo $data[$i];var_dump($match);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
*/
					//echo $data[($i - 1)];echo ":前<br>";
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/(\d{3,7})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						
						if ($data[($i - 2)] == "合計"|| $data[($i - 1)] == "合計"||$data[($i - 1)] == "ご請求金額"||$data[($i - 1)] == "請求金額"||$data[($i - 1)] == "請求額"||$data[($i - 1)] == "請求合計金額"||$data[($i - 1)] == "御請求金額") {
						
							//echo "金額候補A<br/>\r\n";
							$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//exit;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
						if ($money_num_cnt == 0) {
							//金額が入っていない時の候補
							$pattern_comp_end = "/([1-9]{1}[0-9]{0,2},[0-9]{3})/";
							$match_num = preg_match($pattern_comp_end, $ori_data, $match);
							if ($match_num >= 1) {
								//echo "金額候補<br/>\r\n";
								$money_arr[$money_cnt] = $ori_data = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
								//echo "<br>金額候補";
								//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr[$i] = $ori_data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
								if (intval($money_arr[$money_cnt]) > 0) {
									$sum_flag += 1;
									$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
								}
							}
						}
					
						//echo $ori_data;echo $money_num_cnt;
						//echo "<br>";
//					if ($money_num_cnt == 0) {

						//金額が入っていない時の候補
						$pattern_comp_end = "/請求番号|No|NO.*([0-9\W]{1,20})/";
						$match_num = preg_match($pattern_comp_end, $ori_data, $match);
						if ($match_num >= 1) {
							//echo "請求番号";
							$invoice_num = $match[1];
							//echo "金額候補<br/>\r\n";
							//$invoice_num = $ori_data = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $ori_data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							
							$company_data_end_line = $i;
						}
//					}
//echo $data[$i];echo "mncnt:".$money_num_cnt;echo "<br>";
					
					
						$pattern_comp_end = "/書番号|No|NO.*([0-9\W]{1,20})/";
						$match_num = preg_match($pattern_comp_end, $ori_data, $match);
						if ($match_num >= 1) {
							//echo "請求番号";
							$invoice_num = $match[1];
							//echo "金額候補<br/>\r\n";
							//$invoice_num = $ori_data = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $ori_data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							
							$company_data_end_line = $i;
						}

					
					
					$data[$i] = str_replace(array("-","_","－","＿","・","･",".","．","`",":"),"",$data[$i]);
					$data[$i] = str_replace(array("午","撃","#"),"年",$data[$i]);
					$data[$i] = str_replace("l","1",$data[$i]);
					//echo "<br/>";
					$pattern_comp_end = "/支払.*([0-9]{2,4})年([0-9]{1,2})月([0-9]{1,2})日/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}
					
					$pattern_comp_end = "/支払.*([0-9]{2,4})[年A#]*([0-9]{1,2})月([0-9]{1,2})/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data[$i] = str_replace(array("-","_","－",),"",$data[$i]);
						//echo "日付候補d<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$company_data_end_line = $i;
					}

					$data[$i] = str_replace(array("′","ノ",",-",":-"),"/",$data[$i]);//日付用変換
					$pattern_comp_end = "/支払.*([0-9]+)\/([0-9]{1,2})\/([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;

						//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}

					$data[$i] = str_replace("/","1",$data[$i]);
					$data[$i] = str_replace("-","",$data[$i]);
					//echo "<br/>";
					$pattern_comp_end = "/.*([0-9]{2,4})年([0-9]{1,2})月([0-9]{1,2}).*振込/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}
					
					$pattern_comp_end = "/支払.*([0-9]{2,4}).([0-9]{1,2}).([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//var_dump($match);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					if  ($pdt_cnt == 0) {
						//振込先口座番号を取得する
						//1をーに変換し直す。
						$product_check_arr = array(".*銀行.*",".*信用金庫.*",".*信用組合.*",".*金庫.*",".*農業協同組合",".*支店.*",".*新銀行東京.*");
						$p_cnt = 0;
						$last_product = "";
						$match = "";
						$product_arr = array();
						for ($z = 0;$z < count($product_check_arr);$z++) {
							$pattern_comp_end = "/".$product_check_arr[$z]."/";
							$pattern_comp_end;//echo "<br>";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							//var_dump($match);
							////echo "<br>";
							//echo $match_num." ".$bank_flag;
							//echo "<br>";
							if ($match_num >= 1 && $bank_flag == 0) {echo "aaa";
								preg_match("/(.*)銀行|(.*)信用金庫|(\W{3,24})中央金庫|(v)信用組合|(.*)農業協同組合|(.*)支店/im", $match[0], $bank_match);
								//var_dump($bank_match);
								$match[0] = str_replace(array(" ","　"),"",$match[0]);
								if ($match[0] == "銀行" || $match[0] == "下記銀行" || $match[0] == "取引銀行"||$match[0] == "<取引銀行") {
									//誤取得
								} else if ($bank_match[1] != ""||$bank_match[2] != ""||$bank_match[3] != ""||$bank_match[4] != ""||$bank_match[5] != "") {
									//echo "<br>銀行候補<br/>\r\n";
									$product_arr[$p_cnt] = $match[0];
									$bank_flag = 1;
				
									//echo $data2[$i];
									//echo "<br/>";
									//exit;
									if ($product_arr[$p_cnt] != $last_product) {
										if ($p_cnt != 0) {$space = " ";}
										$last_product = $product_arr[$p_cnt];
										$product_name_arr[$p_cnt] = $space.$product_arr[$p_cnt];
										$p_cnt++;
										break;
									}
								}
							}
						}
					}
					

					
					//echo $data2[$i];
					if  ($comp_cnt == 0) {
						//会社名を取得する
						$company_check_arr = array(".*株式会社.*",".*有限会社.*",".*（株）.*",".*（有）.*",".*公社",".*法人.*",".*税理士",".*行政書士",".*司法書士");
						$p_cnt = 0;
						$last_company = "";
						$match = "";
						$company_arr = array();
						for ($z = 0;$z < count($company_check_arr);$z++) {
							$pattern_comp_end = "/".$company_check_arr[$z]."/";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							if ($match_num >= 1) {
								//支払い側企業名を省く処理
								$match_num2 = 0;
								$match2 = "";
								$match3 = "";
								$match_num2 = 0;
								$match_num3 = 0;
								$no_check_flag = 0;
								$match_num2 = preg_match("/御中/", $match[0], $match2);
								$match_num3 = preg_match("/様/", $match[0], $match3);
								if ($data[($i + 2)] == "御中"|| $data[($i + 1)] == "御中"||$data[($i + 2)] == "様"|| $data[($i + 1)] == "様") {
									$no_check_flag = 1;
								}
								if ($match_num2 == 0 && $match_num3 == 0 && $no_check_flag == 0) {
									$company_arr[$p_cnt] = $match[0];
									//echo "<br>会社名候補<br/>\r\n";
									//var_dump($match);
									//echo $data2[$i];
									//echo "<br/>";
									//exit;
									if ($company_arr[$p_cnt] != $last_company) {
										if ($p_cnt != 0) {$space = " ";}
										$last_company = $company_arr[$p_cnt];
										$company_name_arr[$p_cnt] = $space.$company_arr[$p_cnt];
										$p_cnt++;
										$comp_cnt++;
										break;
									}
								}
							}
						}
					}
					//echo $data[$i];echo " ".$i."<br/>";
				}
				
				//情報の整理
				$got_amount_of_money = 0;
				$max_num_times = 0;
				$fixed_flag = 0;
				$max_box = 0;
				//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
				if ($goukei_top == 0) {
					//echo "金額候補を絞る";
					//計を取得できた場合、その最大値を合計と見做す。
					if ($sum_flag >= 1) {
						for ($j = 0;$j < $sum_flag;$j++) {
							if ($max_box <= $sum_box[$j]) {
								$max_box = $sum_box[$j];
							}
						}
	//					echo "z<br>";
						$got_amount_of_money = $max_box;
						$fixed_flag = 1;
					}
					
					//echo $goukei;echo "合計<br/>";
					if ($goukei == 0) {
						
						if ($syoukei > 0) {
							//echo "小計";echo "<br>";
							$got_amount_of_money = round($syoukei * 108/100,0);
							$fixed_flag = 1;
							//echo "<br>";
						}
						
						for ($n = 0;$n < count($money_arr);$n++){
							$temp_money = intval($money_arr[$n]);
							if($fixed_flag == 0) {
								if ($temp_money != 0) {
									//echo "<br/>".$n."tmpmoney".$temp_money."<br>";
									//直後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
									if ($n >= 2) {
										$after2 = abs($temp_money - intval($money_arr[$n-1]));
										if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
											//echo "a<br>";
											$got_amount_of_money = intval($money_arr[$n-2]);
											$fixed_flag = 1;
										}
									}
									
									

									//直前の2つの金額を足したものと同額であれば合計と見做す。(小計と外税と合計)
									if ($fixed_flag == 0) {
										if ($n >= 2) {
											$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
											if ($before2 == $temp_money && $before2 != 0) {
												//echo "c<br>";
												$got_amount_of_money = $temp_money;
												$fixed_flag = 1;
											}
										}
									}
									
									//直後の1つを飛ばしたその後2つの金額の差の絶対値と同額であれば合計と見做す。
									if ($fixed_flag == 0) {
										if ($n >= 3) {
											$after2 = abs($temp_money - intval($money_arr[$n-1]));
											if ($after2 == intval($money_arr[$n-3]) && $after2 != 0) {
												//echo "d<br>";
												$got_amount_of_money = intval($money_arr[$n-3]);
												$fixed_flag = 1;
											}
										}
									}
									
									//直後の金額を引いたものに8%をかけて四捨五入したものと直後の金額が一致していたら合計と見做す（上に合計で内税の場合）
									if ($fixed_flag == 0) {
										if ($n >= 0) {
											$skei = $money_arr[$n] - $money_arr[$n+1];
											$stax = round(($skei * 8/100),0);
											$stax2 = floor($skei * 8/100);
											$stax3 = ceil($skei * 8/100);
											if ($stax == $money_arr[$n+1]||$stax2 == $money_arr[$n+1]||$stax3 == $money_arr[$n+1]) {
												//echo "内税のとこ<br>";
												$got_amount_of_money = $temp_money;
												$fixed_flag = 1;
											}
										}
									}

									//直前の金額を引いたものに8%をかけて四捨五入したものと直後の金額が一致していたら合計と見做す（下に合計で内税の場合）
									if ($fixed_flag == 0) {
										if ($n >= 1) {
											$skei = intval($money_arr[$n]) - intval($money_arr[$n-1]);
											$stax = round(($skei * 8/100),0);
											$stax2 = floor($skei * 8/100);
											$stax3 = ceil($skei * 8/100);
											if ($stax == $money_arr[$n-1]||$stax2 == $money_arr[$n-1]||$stax3 == $money_arr[$n-1]) {
												//echo "内税のとこ２<br>";
												$got_amount_of_money = $temp_money;
												$fixed_flag = 1;
											}
										}
									}
									
									/*//無い方が金額の精度が上がる可能性があるので一旦コメントアウトする。
									if($fixed_flag == 0) {
										$num_of_same = 0;
										$candidate_box = 0;
										//var_dump($money_arr);
										//echo			count($money_arr);
										//同じ数字が2回以上あるものを取得するロジック
										for ($h = 0;$h < count($money_arr);$h++){
											//echo "<br/>tmp2 temp:";
											$temp_money2 = intval($money_arr[$h]);
											//echo " 2回";echo $temp_money;
											//if ($temp_money == $temp_money2){echo "同じ？？<br>";}
											if ($temp_money == $temp_money2) {
												$num_of_same++;
											}
										}
										//echo "<br>回数：".$num_of_same;
										//2回以上のものの中から回数が最大のもの
										if ($num_of_same >= 2) {

											//2回以上のものの中で最大の金額
											if($got_amount_of_money < $temp_money) {
												//echo "e<br/>";
												$got_amount_of_money = $temp_money;
											}
										}
									}
									*/
								}
							}
							
							//▼お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▼
							//直後の2つの金額の上の金額から下の金額を引いた額と同額であれば合計と見做す。（合計と預かりとお釣り）
							if ($n >= 2) {
								//echo $n;echo " b－2のとこ ";
								//var_dump($money_arr);
								//echo intval($money_arr[$n-1])." ".$temp_money." ".$money_arr[$n]."<br/>";
								$after2 = intval($money_arr[$n-1]) - $temp_money;
								if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
									//echo "b-2<br>";
									$got_amount_of_money = intval($money_arr[$n-2]);
									$fixed_flag = 1;
								}
							}
							
							//一つ飛ばした後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
							if ($n >= 3) {
								//echo $n;echo " bのとこ ";echo $temp_money."<br/>";
								//echo intval($money_arr[$n-1])."<br/>";
								$after3 = abs($temp_money - intval($money_arr[$n-1]));
								if ($after3 == intval($money_arr[$n-3]) && $after3 != 0) {
									//echo "b<br>";
									$got_amount_of_money = intval($money_arr[$n-3]);
									$fixed_flag = 1;
								}
							}
							
							if ($n >= 2) {
								//echo "小計と外税と合計（外側）";
								//echo $temp_money;
								$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
								if ($before2 == $temp_money && $before2 != 0) {
									$got_amount_of_money = $temp_money;
									$fixed_flag = 1;
									//echo "d<br>";
								}
							}

							
							//▲お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▲

							
						}
					} else {
						$got_amount_of_money = $goukei;
					}
				} else {
					//echo "top";
					$got_amount_of_money = $goukei_top;
				}
				//入れる金額が無かった場合と一番上の金額の方が候補金額より大きい場合、一番上の金額を入れる。
				//if ($got_amount_of_money == 0) {
				if ($got_amount_of_money == 0 || ($got_amount_of_money <= intval($money_arr[0]))) {
					//echo "in";
					//echo "f<br>";echo $money_arr[0];
					$got_amount_of_money = intval($money_arr[0]);
				}
				//exit;
				//echo "<br/>";echo $got_amount_of_money." 合計<br/><br/>";//exit;
				//echo "<br> got_date_top ";echo $got_date_top[0];var_dump($date_arr);echo "<br>";echo strtotime($date_arr[0]);echo "<br>";echo date('Y/m/d',strtotime($date_arr[0]));exit;
				//日付
				if ($got_date_top[0] == "") {
					for ($d = 0;$d < count($date_arr);$d++){
						//候補の中の一番最初のものを日付にする。（暫定）
						
						//echo "<br/>日付：";
						//echo strtotime($date_arr[$d]);
						//echo "<br>";
						//echo strtotime(date('Ymd'));
						//echo "<br>";
						if (strtotime($date_arr[0]) <= strtotime(date('Ymd'))) {
							//echo $date_arr[$d];
							//echo substr($date_arr[0],0,4);
							//echo "<br>";
							//echo date('Y',strtotime("+1 year"));
							//echo substr($date_arr[$d],0,4);echo "<br>";
							//echo date('Y',strtotime("+3 year"));echo "<br>";
							While (substr($date_arr[$d],0,4) < date('Y',strtotime("+2 year")) && substr($date_arr[$d],0,4) >= date('Y',strtotime("-2 year"))) {
								$got_date = $date_arr[$d];
								//echo "<br>";
								break;
								if ($d = count($date_arr) - 1) {
									break;
								}
							}
						}
						//echo "<br/>";
						//echo "\r\n";
					}
				} else {
					
					//for ($dtcnt = 0;$dtcnt < count($got_date_top);$dtcnt++) {
					//	$got_date = $got_date_top[$dtcnt];
					//}
					$got_date = $got_date_top[0];
					
				}
				//var_dump($tel_arr);
				//電話番号
				$exist_flag = 0;
				$comp_name = "";
				if (count($tel_arr) > 0) {
					$tel_arr_num = count($tel_arr);
				} else {
					$tel_arr_num = 1;
				}
				//var_dump($tel_arr_num);
				for ($t = 0;$t < $tel_arr_num;$t++){
					//echo "tel選択：";
					if (substr($tel_arr[$t],0,1) == 0) {
					//echo "<br>電話番号：";
						$tel_num = $tel_arr[$t];
					//echo "<br>";
					}
				}
			}

			// ファイルのパスを変数に格納
			//echo "<br>";
			$FileText = $file_path.$dlp.'.txt';
			//echo "<br>".$FileText."<br>";
			//振込先情報を取り出す
			if (count($product_name_arr) > 0) {
				for ($n = 0;$n < count($product_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$product .= $sp.$product_name_arr[$n];
				}
				//$product_name = $searched_company_name." ".$product;
				$product_name = $product;
			} else {
					$product_name = $searched_company_name;
			}

			//口座情報を取り出す
			if (count($acnt_name_arr) > 0) {
				for ($n = 0;$n < count($acnt_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$account .= $sp.$acnt_name_arr[$n];
				}
				//$account_name = $searched_company_name." ".$product;
				$account_name = $account;
			}
			
			//会社名を取り出す
			if (count($company_name_arr) > 0) {
				for ($n = 0;$n < count($company_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$company_name_one .= $sp.$company_name_arr[$n];
				}
				//$company_names = $searched_company_name." ".$product;
				$company_names = $company_name_one;
			} else {
					$company_names = $searched_company_name;
			}
			//日付が未取得の場合の処理
			if ($got_date == "" || date('Ymd',strtotime($got_date)) == "20170101") {
				if ($last_receipt_date != "") {
					$got_date = date('Ym',strtotime($last_receipt_date))."01";
				} else {
					$got_date = date("Y",time())."0101";
				}
			}
			
			if ($product_name == $account_name) {
				$account_name = "";
			}
			$name = "";
			$acNo = "";
			$bank = "";
			$blanch = "";
			$type = "";
			
			$pattern_arr = array("銀行名","\(","\)","（","）","振込口座","口座名義人","口座名義","口座");

			for ($pat_cnt = 0;$pat_cnt < count($pattern_arr); $pat_cnt++) {
				$pattern = array("/(.*)".$pattern_arr[$pat_cnt]."(.*)/i");
				$replecement = "$1$2";
				if(preg_replace($pattern,$replecement,$product_name)) {
				  $product_name = preg_replace($pattern,$replecement,$product_name);
				}
				if(preg_replace($pattern,$replecement,$account_name)) {
				  $account_name = preg_replace($pattern,$replecement,$account_name);
				}
			}
			$org = array("NO","No"," ",":",".","(",")","/","０");
			$rep = array("","","","","","","","1","0");
			$product_name = str_replace($org,$rep,$product_name);
			$account_name = str_replace($org,$rep,$account_name);
			
			if ($product_name == $account_name) {
				$bank_account_str = $product_name;
			} else {
				$bank_account_str = $product_name.$account_name;
			}
			
			//var_dump($bank);echo"<br>";echo $bank_account_str;echo"<br>";
			//銀行名リストと照合してbank_account_strから銀行名を抽出する。
			$banks_data = file_get_contents(dirname(__FILE__).'/../invoice/files/bank.txt');
			$banks = explode("\n",$banks_data);
			for ($n = 0;$n < count($banks);$n++) {
				$bank_arr = explode(",",$banks[$n]);
				
				$pattern_comp_end = "/".$bank_arr[1]."/im";
				$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
				if ($match_num >= 1) {
					//var_dump($match);
					$bank = $match[0];
					break;
				}
			}
			if ($bank == "") {
				//銀行名、支店名、口座種別、口座名義人などを$product_name,$account_nameから抽出する。
				$pattern_comp_end = "/(新銀行東京|\W{3,24}銀行|\W{3,24}信用金庫|\W{3,24}信用組合)/im";
				$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
				if ($match_num >= 1) {
					//var_dump($match);
					$bank = $match[1];
				}
			}
			
			$pattern_comp_end = "/銀行(\W{3,24}支店|\W{3,24}出張所|本店営業部)/im";
			$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
			if ($match_num >= 1) {
				//var_dump($match);
				$blanch = $match[1];
			} else {
				$pattern_comp_end = "/信用金庫(\W{3,24}支店|\W{3,24}出張所|本店営業部)/im";
				$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
				if ($match_num >= 1) {
					//var_dump($match);
					$blanch = $match[1];
				} else {
					$pattern_comp_end = "/信用組合(\W{3,24}支店|\W{3,24}出張所|本店営業部)/im";
					$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
					if ($match_num >= 1) {
						//var_dump($match);
						$blanch = $match[1];
					} else {
						$pattern_comp_end = "/(\W{3,24}支店|\W{3,24}出張所|本店営業部)/im";
						$match_num = preg_match($pattern_comp_end, $bank_account_str, $match);
						if ($match_num >= 1) {
							//var_dump($match);
							$blanch = $match[1];
						}
					}
				}
			}

			$pattern_comp_end = "/(普通|当座|\(普\))/im";
			$product_name.$account_name.$account_num;
			$match_num = preg_match($pattern_comp_end, $bank_account_str.$account_num, $match);
			if ($match_num >= 1) {
				//var_dump($match);
				$type = $match[1];
			}
			//echo "C".$bank_account_str." ".$account_num;
			$product_name.$account_name.$account_num.$blanch_name;
			$pattern_comp_end = "/([0-9]{4,7})(.*)$/im";
			$match_num = preg_match($pattern_comp_end, $bank_account_str.$account_num, $match);
			if ($match_num >= 1) {
				//var_dump($match);
				$acNo = $match[1];
				$kana2 = array("ャ"=>"ヤ","ュ"=>"ユ","ョ"=>"ヨ");
				$match[2] = strtr($match[2], $kana2);
				$match[2] = str_replace(array("普通","当座","(普)"),"",$match[2]);
				$name = mb_convert_kana($match[2],"k","UTF-8");
				//echo "<br>";
				//var_dump($match);
				//exit;
			}

			$pattern_comp_end = "/口座名(.*)$/im";
			$match_num = preg_match($pattern_comp_end, $bank_account_str.$account_num, $match);
			if ($match_num >= 1) {
				$kana2 = array("ャ"=>"ヤ","ュ"=>"ユ","ョ"=>"ヨ");
				$match[1] = strtr($match[1], $kana2);
				$match[1] = str_replace(array("普通","当座","(普)"),"",$match[1]);
				$name = mb_convert_kana($match[2],"k","UTF-8");
				//echo "<br>";
				//var_dump($match);
				//exit;
			}
			echo $dlp.'.jpg';echo " image<br>";
			echo $got_date;echo " date<br>";
			echo $paydate = date('Y/m/d',strtotime($got_date));
			if ($paydate == '2017/01/01') {
				$paydate ="";
			}
			//echo $account_holder;echo "AAA<br>";
			if ($account_holder != "") {
				if (intval($account_holder) == 0) {
					$pattern_comp_end = "/\((\W{3,45})\)(\W{3,45})$/im";
					$match_num = preg_match($pattern_comp_end, $account_holder, $match);
					if ($match_num >= 1) {
						$name = mb_convert_kana($match[1],"k","UTF-8");
					} else {
						$name = mb_convert_kana($account_holder,"k","UTF-8");
					}
				}
			}

			if ($blanch_name != "") {
				if ($blanch == $bank || $blanch == "") {
					$blanch = mb_convert_kana($blanch_name,"k","UTF-8");
				}
			}
			$company_name_fixed = "";
			if ($company_arr[1] != NULL) {
				$company_name_fixed = $company_arr[1];
			} else {
				$company_name_fixed = $company_arr[0];
			}
			$acNo = str_replace("0000000","",$acNo);
			$name = str_replace(array("株式会社","カ）","（カ","有限会社","ユ）","（ユ"),array("(ｶ)","ｶ)","(ｶ","(ﾕ)","ﾕ)","(ﾕ"),$name);
//			$AddWords = "\r\n".$paydate.",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$account_name.",".$industry_type_s;
			//支払日を取得するルールがないので一旦消しておく。20170424hmsk
			//$AllData .= "\r\n".'"","'.date('Y/m/d',strtotime($got_date)).'","","'.$product_name.'","'.$account_name.'",'.'""'.','.$got_amount_of_money.',"'.$dlp.'.jpg"';
			//$AllData .= "\r\n".'"","","","'.$product_name.'","'.$account_name.'",'.'""'.','.$got_amount_of_money.',"'.$dlp.'.jpg"';
echo			$AllData .= "\n".'"'.$invoice_num.'","'.$paydate.'","'.$company_name_fixed.'","'.$bank.'","'.$blanch.'","'.$type.'","'.sprintf("%07d",$acNo).'","'.$name.'",'.$got_amount_of_money.',"'.$dlp.'.jpg"'.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3);
			//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
//			echo "<br>";
//			exit;
/*
			file_put_contents($FileText, $AddWords, FILE_APPEND);
			$outcode = "sjis-win";
			$incode = "UTF-8";
			$nl = "\r\n";
			convertCode($FileText , $incode, $FileText , $outcode, $nl);
*/
			
			// ファイルを出力する
			//readfile($FileText);
			//exit;
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
		//取得したTEXTファイルを削除する。
//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
		$counter++;
	}//foreach文の終わり//for文に変更

	//全ファイル記録用ファイルに追記
	//echo $AllData;
	$FileText2 = $txt_dir."/Data/".$cs_no.date("Ymd").".csv";
	file_put_contents($FileText2, $AllData, FILE_APPEND | LOCK_EX);
	$outcode = "sjis-win";
	$incode = "UTF-8";
	$nl = "\n";
	convertCode($FileText2 , $incode, $FileText2 , $outcode, $nl);
}

if ($counter > 0) {
	$counter = $counter;
	$_SESSION['ret_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをテキスト化しました。";
}
if($error_num > 0) {
	$_SESSION['ret_msg'] = $error_num."件のファイルをテキスト化できませんでした。";
}
//echo $counter;
$file_list_arr = getFileList($path);
//var_dump($file_list_arr);

//テスト用。仮にここで止める。
exit;

if(count($file_list_arr) > 0) {
	header("Location:./download_imgs_to_text_all?path=".$upload_dir_path."&type=*&opt=r",true);
	echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./rakutasu_imgs_to_text_upload_all\"'>";
	exit;
} else {
	//echo "no DL<br>";
}

header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://storage.cloudinvoice.co.jp/invoice/rakutasu_imgs_to_text_upload_all'</script>";
echo "</html>";

echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./rakutasu_imgs_to_text_upload_all\"'>";


//header("Location:https://storage.cloudinvoice.co.jp/invoice/rakutasu_imgs_to_text_upload_all",true);
exit;


/**
* テキストファイルの文字コードを変換し保存する
* @param string $infname  入力ファイル名
* @param string $incode   入力ファイルの文字コード
* @param string $outfname 出力ファイル名
* @param string $outcode  出力ファイルの文字コード
* @param string $nl       出力ファイルの改行コード
* @return string メッセージ
*/
function convertCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'wb');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}

function convertTextCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'a');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}


function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>