<?php
session_start();

//必要なクラスを読み込む
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../cfiles/ReadCsvFile.class.php");
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//echo "a<br/>";
//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$auto_journalize_con = new auto_journalize_control();
$read_csv_con = new ReadCsvFile();

//変数の宣言
$number_of_times = 0;
$success_num = 0;
$all_num = 0;
$filepath = dirname(__FILE__).'/../pdf/nru';
$file_name = htmlspecialchars($_GET['fn'],ENT_QUOTES);
//var_dump($file_name);
if ($file_name == "") {
	$file_name = "username.csv";
}
//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$all_num = count($csv) - 1;

//var_dump($csv);
//ob_start();
//CSVデータを一行ずつ取り出してDBに登録する。
foreach ($csv as $value) {
	if ($value[0] != "仕訳No順") {
		//存在フラグの初期化
		$exist_flag = 0;
		
		$journalized_code = $value[0];
		$phone_number = $value[1];
		$user_name = $value[2];
		$paid_date = date('Y-m-d',strtotime($value[3]));
		$pay_date = date('Ymd',strtotime($value[3]));
		$total_price = $value[4];
		$sales_tax = $value[5];
		if ($value[6] == ""){
			$withholding_tax = 0;
		} else {
			$withholding_tax = $value[6];
		}
		$product_name = $value[8];
		$dlp = $download_password = $value[10];
		$paper_type = $value[11];
				if (is_int($value[12])){
			echo "id照合<br/>";
			$destination_company_id = $value[12];
			//企業データ存在確認（元側）▼
			$flag = "company_data";
			$words = "";
			$destination_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_company_id,$words);
			$destination_company_id = $destination_company_data_arr[0]['company_id'];
		} else {
			echo "名前照合<br/>";
			$destination_company_name = $value[12];
			//企業データ存在確認（元側）▼
			$flag = "company_data_from_name";
			$words = "";
			$destination_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_company_name,$words);
			$destination_company_id = $destination_company_data_arr[0]['company_id'];
		}
		if ($destination_company_id == "" || $destination_company_id == NULL) {
			echo "請求先の企業".$value[12]."のデータが存在しません";
			exit();
		}
		$non_regular_flag = $value[13];
		$invoice_code = "90".$pay_date."-".$journalized_code;
		//echo "<br/>\r\n";


		//企業データ存在確認▼
		if ($phone_number != "") {

			$company_id = "".$user_arr['user_id'];
			$flag = "company_data_from_tel";
			$words = "";
			$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$phone_number,$words);
			//var_dump($company_data_arr);
			if(isset($company_data_arr[0]['tel_num'])) {
				//echo "TELあり<br/>\r\n";
				$exist_flag = 1;
			}// else{echo "TELなし<br/>\r\n";}
		} else {
			$flag = "company_data_from_name";
			$words = "";
			$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$user_name,$words);
			//var_dump($company_data_arr);
			if(isset($company_data_arr[0]['company_name'])){
				//echo "名前あり<br/>\r\n";
				$exist_flag = 1;
			}// else {echo "名前なし<br/>\r\n";}
		}
		//企業データ存在確認▲




		//企業アカウントが存在する時
		$company_id = $company_data_arr[0]['company_id'];
		//echo "<br/>\r\n";

		//請求書・領収書データの登録
		$path = dirname(__FILE__).'/../files/'.sprintf("%012d", $company_id)."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
	
		$pdf_url = dirname(__FILE__).'/../files/'.sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".pdf";
		//echo "<br/>\r\n";
	echo	$png_url = dirname(__FILE__).'/../files/'.sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".png";
		echo "<br/>\r\n";
		//echo "<br/>\r\n";
		//共通コード+請求書番号で請求書データの存在を確認する
		$sql = "SELECT * FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$company_id." AND `invoice_code` = '".$invoice_code."' ";
		$update_flag_arr = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
		//var_dump($update_flag_arr);
		//echo $update_flag_arr[0]['invoice_code'];
		//同一請求書番号のデータが存在する時は処理をしない

		//pngが存在した場合、上書きする。
		if (file_exists($png_url)) {
		//pngが存在する場合は処理しない
		//if (!file_exists($png_url)) {
		//pngが存在した場合上書きを10回分まで行なう	
		//if (file_exists($png_url) && $number_of_times < 10) {
			try {
				$number_of_times++;
				echo "PDF、PNG移動";
				echo "<br/>\r\n";
				// ファイルの存在確認
				//PNGファイルを保存
				//削除用
				
				//	echo $path2 = "/var/www/storage.cloudinvoice.co.jp/html/files/".$rm_data;
				//	echo "<br/>\r\n";
				//	shell_exec("rm -Rf ".$path2);
				//	shell_exec("rm -Rf ".$path2);
				$pdftoimg_con = new PdfToImg();
				
				//各ディレクトリに割り振る場合（配置用：通常）
				$pdftoimg_con -> moveImage($path,$dlp);
				
				//ディレクトリから収集して戻す場合（回収用）
				//$pdftoimg_con -> moveImageReverse($path,$dlp);				
				
				//変換して移動
				//$pdftoimg_con -> changeImage($path,$dlp);
				
			} catch (Exception $e) {
				echo $e -> getMessage;
			}
		}

	}
}
echo "<br/>ファイル移動回数：".$number_of_times;
fclose($temp);
unlink($file);

//ob_end_flush();

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>非正規ユーザーアップロード完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">非正規ユーザーアップロード完了</h1>
		<h2>非正規ユーザーの登録が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./nruser_upload'">非正規ユーザーのアップロード画面に戻る</button>
	</div>
</body>
</html>