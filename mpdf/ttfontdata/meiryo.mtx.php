<?php
$name='Meiryo';
$type='TTF';
$desc=array (
  'Ascent' => 1060,
  'Descent' => -440,
  'CapHeight' => 736,
  'Flags' => 4,
  'FontBBox' => '[-387 -440 1822 1060]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 1000,
);
$up=-100;
$ut=50;
$ttffile='/var/www/storage.cloudinvoice.co.jp/html/invoice/mpdf/ttfonts/meiryo.ttf';
$TTCfontID='0';
$originalsize=8594960;
$sip=false;
$smp=false;
$BMPselected=true;
$fontkey='meiryo';
$panose=' 8 0 2 b 6 4 3 5 4 4 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>