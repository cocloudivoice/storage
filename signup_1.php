<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" /><!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
</script><script src="selectivizr.js">
</script><![endif]-->

<meta http-equiv="Content-Script-Type" content="text/javascript">
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("jquery", "1.2");</script>
<title>ユーザ登録画面</title>
<script type="text/javascript">
function form_submit(){
	for (var i=0 ; i<4 ; i++){
	 var textvalue=[];
	 textvalue = document.getElementsByTagName('input')[i].value;
		if ( textvalue == null || textvalue == "" ){
	      alert("項目データが無い行があります。\n項目名を入力してください。");
          return;
		}
	}
	textvalue2 = document.getElementsByTagName('input')[2].value;
	textvalue3 = document.getElementsByTagName('input')[3].value;
	if ( textvalue2 != textvalue3 ){
      alert("パスワードの入力が一致していません");
      return;
    }

    document.form1.submit();
}
</script>
<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:400px;
	height:400px;
	margin-left:auto;
	margin-right:auto;
}
#frame{
	width:400px;
	height:400px;
	text-align:center;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
</style>
</head>
<body>
	
	
	<div id="wrap">
		<h1>ユーザ登録画面</h1>
		<div id="frame">
			<form name="form1" method="post" action="./signup_2.php">
			<p>あなたのお名前を入力してください（ニックネーム）  :　<input type="text" name="yourname" size="40" maxlength="40" /></p>
			<p>あなたのメールアドレスを入力してください   :　<input type="text" name="youraddress" size="40" maxlength="40" /></p>
            <p>あなたのパスワードを入力してください       :　<input name="yourpass1" size="42" maxlength="40" oncopy="return false" onpaste="return false" oncontextmenu="return false" type="password" /></p>
            <p>あなたのパスワードを入力してください（再） :　<input name="yourpass2" size="42" maxlength="40" oncopy="return false" onpaste="return false" oncontextmenu="return false" type="password" /></p>
			<p id="touroku_b"><input type="button" onclick="form_submit()" name="toroku" value="登録" /></p></form>
		</div>
	</div>
</body>
</html>