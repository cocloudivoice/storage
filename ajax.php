<?php session_start(); ?>
<?php require("header.php");?>


<title>送付先管理 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
}

$del_flag = $_REQUEST['del_flag'];


//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

if ($del_flag == 1) {
		$sql = "DELETE FROM `ADDRESSEE_TABLE` WHERE id = ".$_REQUEST['id']."";

		$company_con -> company_sql($pdo,$company_id,$sql);

}

if (isset($_REQUEST['email'])) {
echo		$sql = "UPDATE `ADDRESSEE_TABLE` SET 
		`client_id`='".$_REQUEST['client_id']."',`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`addressee`='".$_REQUEST['addressee']."',`department`='".$_REQUEST['department']."',`position`='".$_REQUEST['position']."', 
		`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',`fax_num`='".$_REQUEST['fax_num']."',
		`email`='".$_REQUEST['email']."',`client_company_id`='".$_REQUEST['client_company_id']."' 
		 WHERE id = ".$_REQUEST['id']." ";
	$company_con -> company_sql($pdo,$company_id,$sql);

}

//設定変更の処理▲

//設定内容の表示データ取得
$flag = "addressee_data_all";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($company_arr);
?>

<script>

function OpenSouhuEditFrame(cid){
	document.getElementById("souhuEditFrame").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	document.getElementById("souhuEditFrame").setAttribute('src', './frame/souhuEditFrame?cid='+cid+'');
}

function OpenSouhuEditFrameIns(){
	document.getElementById("souhuEditFrame").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	document.getElementById("souhuEditFrame").setAttribute('src', './frame/souhuEditFrame?ins=1');
}

</script>
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {

        /**
         * 送信ボタンクリック
         */
        $('#send').click(function()
        {
            //POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
            var data = {request : $('#request').val()};

            /**
             * Ajax通信メソッド
             * @param type  : HTTP通信の種類
             * @param url   : リクエスト送信先のURL
             * @param data  : サーバに送信する値
             */
            $.ajax({
                type: "POST",
                url: "./send.php",
                data: data,
                /**
                 * Ajax通信が成功した場合に呼び出されるメソッド
                 */
                success: function(data, dataType)
                {
                    //successのブロック内は、Ajax通信が成功した場合に呼び出される

                    //PHPから返ってきたデータの表示
                    document.getElementById('result').innerHTML = data;
                    //document.write(data);
                },
                /**
                 * Ajax通信が失敗した場合に呼び出されるメソッド
                 */
                error: function(XMLHttpRequest, textStatus, errorThrown)
                {
                    //通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

                    //this;
                    //thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

                    //エラーメッセージの表示
                    alert('Error : ' + errorThrown);
                }
            });
            
            //サブミット後、ページをリロードしないようにする
            return false;
        });
    });
    </script>
<iframe id="souhuEditFrame" src="./frame/souhuEditFrame"></iframe>


<article>

	<section id="m-3-box">
		<h2>
			送付先管理
		</h2>
		<button onclick="OpenSouhuEditFrameIns('')">
		新しい送付先を登録
		</button>
			<table>
			<tr id="first">
				<th>送付先コード</th>
				<th>送付先コード宛先</th>
				<th>共通コード</th>
				<th>共通コード宛先</th>
				<th>担当者氏名</th>
				<th>担当者部署</th>
				<th>電話番号</th>
				<th>メールアドレス</th>
				<th>編集</th>
				<th>削除</th>
			</tr>
	<?php for ($i = 0; $i < count($company_arr); $i++) { ;
		$client_comp_id = $company_arr[$i]["client_company_id"];
	?>
			<tr>
				<td id="souhuNo"><?php echo $company_arr[$i]["client_id"];?></td>
				<td id="souhusaki"><?php echo $company_arr[$i]["company_name"];?></td>
				<td id="yourBango"><?php if ($client_comp_id == 0) {echo "";} else { echo $client_comp_id;}?></td>
				<?php 
					$flag = "company_data";
					$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$client_comp_id,$words);

				?>
				<td id="souhusaki" style="background-color:<?php if ($client_comp_id != 0 && !isset($company_one_arr[0]['company_name'])) { echo 'red';}?>;"><?php echo $company_one_arr[0]["company_name"];?></td>
				<td id="yourTanto"><?php echo $company_arr[$i]["addressee"];?></td>
				<td id="yourBusho"><?php echo $company_arr[$i]["department"];?></td>
				<td id="yourBango"><?php echo $company_arr[$i]["tel_num"];?></td>
				<td id="yourAddress"><?php echo $company_arr[$i]["email"];?></td>
				<td id="souhuEdit"><button id="souhuEditButton" onclick="OpenSouhuEditFrame('<?php echo $company_arr[$i]['client_id'];?>')"></button></td>
				<td id="souhuDelete"><form action="" method="post" ><input type="hidden" name="id" value="<?php echo $company_arr[$i]['id'];?>" /><input type="hidden" name="del_flag" value="1" /><button id="souhuDeleteButton" onclick="submit();"></button></form></td>
			</tr>
	<?php };?>
		
		
		</table>

	    <h3>共通コード確認</h3>
	    <form method="post">
	        <p>共通コード<input type="text" id="request" /></p>
	        <p><input id="send" value="確認" type="submit" /></p>
	    </form>
	    <div id="result"></div>
<!--
		<form action="http://co.cloudinvoice.co.jp/invoice/test_ajax.html" target="_blank">
			<input type="button" value="共通コード確認" onclick="location.href='http://co.cloudinvoice.co.jp/invoice/test_ajax.html'" target="_blank">
			<input type="submit" value="確認">
		</form>
	    <div id="result"></div>
-->
		</section>
</article>

<div id="fadeLayer"></div>


<?php require("footer.php");?>