<?php require("header.php");?>


<title>請求CSV確認画面 - Cloud Invoice</title>


<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../cfiles/changekoyomi.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$db_con = new db_control();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$koyomi_con = new ChangeKoyomi();

//変数の宣言
$company_id = $_SESSION['user_id'];

//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];
$top4 = substr($company_id,0,4);
$mid4 = substr($company_id,4,4);
$file = '../files/'.$top4."/".$mid4."/".$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	// 読み込むファイル名の指定
	// ファイルポインタを開く
	$fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
	fclose($file);
}


//setlocale(LC_ALL, 'ja_JP');<br />
//mb_detect_order("SJIS-win");<br />
//$list=array();<br />
	
//PHP5用回避$fileData=file_get_contents($file['tmp_name']);$fileBuf = mb_convert_encoding($fileData, "utf-8", "sjis-win");$tmp = tmpfile();fwrite($tmp, $fileBuf);rewind($tmp);while(($array = fgetcsv($tmp, 2500, ',')) !== false) {  $list[] = $array;}

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}

$columns_num = sizeof(fgetcsv(fopen($file, "r"), 1000, ","));
fclose($file);

?>

<style>
.bk_red {
	background-color:red;
}
</style>
<article>

	<section id="m-3-box">
		<h2>
			請求CSV－確認画面
		</h2>
		
		<table>
			<tr id="first">
<?php
//			if ($_SESSION['format'] == 1 || $_SESSION['format'] == NULL) {
?>
				<th>共通CD</th>
				<th>共通CD宛先</th>
				<th>送付先CD</th>
				<th>送付先CD宛先</th>
				<th>請求書日付</th>
				<th>請求書番号</th>
				<th>請求書名称</th>
				<th>当社担当者</th>
				<th>支払日</th>
				<th>振込口座</th>
				<th>商品名</th>
				<th>請求金額</th>
				<!--
				<th id="meisai">明細数</th>
				-->
<!--
<?php
//			} else if ($_SESSION['format'] == 2) {
?>
				<th>固定</th>
				<th></th>
				<th></th>
				<th>請求書日付</th>
				<th>固定</th>
				<th></th>
				<th></th>
				<th>固定</th>
				<th>合計金額</th>
				<th>固定</th>
				<th>固定</th>
				<th></th>
				<th></th>
				<th>固定</th>
				<th>合計金額</th>
				<th>消費税額</th>
				<th>送付先コード</th>
				<th></th>
				<th></th>
				<th>固定</th>
				<th></th>
				<th>固定</th>
				<th></th>
				<th></th>
				<th>固定</th>
<?php
//			}
?>
-->
			</tr>

<?php
foreach ($csv as $value) {
	//var_dump($value);echo "<br />\n";
	if ( $value[0] == "共通コード" || $value[0] == "固定" || $value[0] == "済") {
		if ($value[0] == "済") {
			for ($i = 0 ;$i < $columns_num;$i++) {
				switch ($value[$i]) {
					case "済":
						$col1 = $i;
			        	break;
					case "日付":
						$col2 = $i;
			        	break;
					case "コード":
						$col3 = $i;
			        	break;
					case "会社名":
						$col4 = $i;
			        	break;
					case "販売区分":
						$col5 = $i;
			        	break;
					case "商品":
						$col6 = $i;
			        	break;
					case "数量":
						$col7 = $i;
			        	break;
					case "単価":
						$col8 = $i;
			        	break;
					case "金額":
						$col9 = $i;
			        	break;
			        default:
			        	$col10 = $i;
		        }
		    }
		}/* else if ($value[0] == "共通コード") {
			for ($i = 0 ;$i < $columns_num;$i++) {
				switch ($value[$i]) {
					case "共通コード":
						$col1 = $i;
			        	break;
					case "送付先コード":
						$col2 = $i;
			        	break;
					case "請求書日付":
						$col3 = $i;
			        	break;
					case "請求書番号":
						$col4 = $i;
			        	break;
					case "請求書名":
						$col5 = $i;
			        	break;
					case "当社担当者":
						$col6 = $i;
			        	break;
					case "支払日付":
						$col7 = $i;
			        	break;
					case "振込口座":
						$col8 = $i;
			        	break;
					case "納品日付":
						$col9 = $i;
			        	break;
					case "品番":
						$col10 = $i;
			        	break;
					case "品名":
						$col11 = $i;
			        	break;
					case "単価":
						$col12 = $i;
			        	break;
					case "数量":
						$col13 = $i;
			        	break;
					case "単位":
						$col14 = $i;
			        	break;
					case "税抜き合計金額":
						$col15 = $i;
			        	break;
					case "消費税額":
						$col16 = $i;
			        	break;
					case "源泉徴収税":
						$col17 = $i;
			        	break;
					case "合計金額":
						$col18 = $i;
			        	break;
					case "備考1":
						$col19 = $i;
			        	break;
					case "備考2":
						$col20 = $i;
			        	break;
					case "備考3":
						$col21 = $i;
			        	break;
			        default:
			        	$col22 = $i;
		        }
		    }
		}*/
		
	} else {
		if ($_SESSION['format'] == 1) {
			if ($value[0] == "") {
				$value[0] = 0;
			}
?>
			<tr>
				<td id="kyoutuCD"><?php echo $value[0] = str_replace(array("　"," ","-"),"",$value[0]);?></td>
				<?php 
				//var_dump($value);//."<br/><br/>";
				unset($company_name);
				//echo "val0".$value[0];echo "val1".$value[1];echo "val2".$value[2];echo "val3".$value[3];
				if ($value[0] != "") {
					$flag = "company_data";
					$client_id = $value[0];
					$company_name = $company_con -> company_sql_flag($pdo,$flag,$client_id,$words);
				}				
				//var_dump($company_name);
				?>
				<td id="atesaki" <?php if ($value[0] != "" && $company_name[0]['company_name'] == "") { echo 'class="bk_red"';}?>><?php echo $company_name[0]['company_name'];?></td>
				<td id="kanriCD"><?php echo $value[1];?></td>
				<?php
				unset($company_name2);
				if ( $value[1] != "" ) {
					$flag = "addressee_data_csv";
					$words = str_replace(array(" ","　"),"",$value[1]);
					$client_id = $value[0];
					$company_name2 = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					
					
				}
				?>
				<td id="atesaki" <?php if ($value[1] != "" && $company_name2[0]['company_name'] == "") { echo 'class="bk_red"';}?>><?php echo $company_name2[0]['company_name'];?></td>
				<td id="seikyuBi"><?php echo $value[2];?></td>
				<td id="seikyuNo"><?php echo $value[3];?></td>
				<td id="seikyuName"><?php echo $value[4];?></td>
				<td id="thisTanto"><?php echo $value[5];?></td>
				<td id="siharaibi"><?php echo $value[6];?></td>
				<td id="thisKoza"><?php echo $value[7];?></td>
				<td id="siharaibi"><?php echo $value[10];?></td>
				<td id="sumMoney"><?php echo number_format($value[17]);?></td>
				<!--
				<td id="meisai"></td>
				-->
			</tr>
<?php
		} else if ($_SESSION['format'] == 2) {

				unset($company_name2);
				if ( $value[$col3] != "" ) {
					$flag = "addressee_data_csv";
					$words = str_replace(array(" ","　"),"",$value[$col3]);
					$client_id = "";
					$company_name2 = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					//var_dump($company_name2);
					$client_company_id = $company_name2[0]["client_company_id"];
					
					if (count($company_name2) == 0) {
						$sql = "INSERT INTO `ADDRESSEE_TABLE` (`client_id`, `company_name`, `company_id`) VALUES ('".$value[$col3]."','".$value[$col4]."',".$company_id.");";
						$company_con -> company_sql($pdo,$company_id,$sql);
					
						//再度行なう
						$flag = "addressee_data_csv";
						$words = str_replace(array(" ","　"),"",$value[$col3]);
						$client_id = "";
						$company_name2 = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
						//var_dump($company_name2);
						$client_company_id = $company_name2[0]["client_company_id"];

					}

					
					
				} 
				
				//var_dump($value);//."<br/><br/>";
				unset($company_name);
				if ($client_company_id != "") {
					$flag = "company_data";
					$company_name = $company_con -> company_sql_flag($pdo,$flag,$client_company_id,$words);
				}				
				if (isset($company_name[0]["company_name"])) {
					$company_name = $company_name[0]["company_name"];
				} else {
					$company_name = "";
				}
				
				//支払日設定
				//if (isset($value[$col10])) {
				//	$pay_date = $value[$col10];//支払日を取得していれる必要がある。
				//} else {
					$pay_date = date('Ymd', strtotime('last day of ' . date("Ymd", strtotime("+1 month"))));
				//}

				
				
?>

			<tr>
				<td id="kyoutuCD"><?php if ($client_company_id != 0) { echo $client_company_id;}?></td>
				<td id="atesaki"><?php echo $company_name;?></td>
				<td id="kanriCD">
				<?php echo $value[$col3];?></td>
				
				<td id="atesaki" <?php if ($value[6] != "" && $company_name2[0]['company_name'] == "") { echo 'class="bk_red"';}?>><?php echo $company_name2[0]['company_name'];?></td>
				<td id="seikyuBi">
				<?php
					 $sale_date = $koyomi_con -> convJtGDate($value[$col2]);
					 echo $sale_date;
				?>
				</td>
				<td id="seikyuNo">
				<?php
					$sale_date = $koyomi_con -> convJtGDate($value[$col2]);
					echo $value[$col3].$sale_date;
				?>
				</td>
				<td id="seikyuName"><?php echo $value[$col4].$sale_date;?></td>
				<td id="thisTanto"><?php?></td>
				<td id="siharaibi"><?php echo $pay_date;?></td>
				<td id="thisKoza"><?php?></td>
				<td id="siharaibi"><?php echo $value[$col6];?></td>
				<td id="sumMoney"><?php echo number_format($value[$col9]);?></td>
				
				<!--
				<td id="sumMoney"><?php echo number_format($value[17]);?></td>
				-->
				<!--
				<td id="meisai"></td>
				-->
			</tr>

<?php
		}
	}
}

fclose($temp);

?>
		
		
		</table>

		<form action="./read_csv" method="post" name="read_csv" />
			<input type="hidden" name="company_id" value="<?php echo $company_id;?>" />
			<input type="hidden" name="colms" value="20" />
			<input type="hidden" name="format_type" value="<?php echo $_SESSION['format'];?>" />
			<!--
			<input type="button" name="kakunin" onclick="submit()" value="CSV取り込み" />
			-->
			<button name="kakunin" onclick="submit()">CSV取り込み</button>
			<button type="button" onclick="location.href='./seikyucsv'">キャンセル</button>
		</form>
					
	</section>
</article>


<?php require("footer.php");?>