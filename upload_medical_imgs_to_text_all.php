<?php
session_start();

//処理制限時間を外す
set_time_limit(0);

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
include_once(dirname(__FILE__)."/../pdf/CloudVisionTextDetection.class.php");
//自動仕分け用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
//$return_url =  = dirname(__FILE__).'/imgs_to_text_upload_all';
if (isset($_REQUEST['ahead_url'])) {
	$ahead_url = $_REQUEST['ahead_url'];
	$return_url = $_REQUEST['return_url'];
}
//echo $_REQUEST['path'];echo "req<br>";
if ($_REQUEST['path'] == NULL ||$_REQUEST['path'] == "") {
	$path = dirname(__FILE__)."/../pdf/png/5129391867";
} else {
	$path = $_REQUEST['path'];
}
$med = 0;
if ($_REQUEST["med"] == 1) {
	//医療費の時
	$med = $_REQUEST["med"];
}

$file_name = $_FILES['upfile']['name'];
$upload_dir_path = $path;//dirname(__FILE__)."/../pdf/png/".$af_id;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;
//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
//$txt_dir = dirname(__FILE__)."/../pdf/png/".$af_id;
$txt_dir = $upload_dir_path;

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//$upload_dir_pathのフォルダが存在しない場合に作る。
recurse_chown_chgrp($upload_dir_path, $uid, $gid);

setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');


$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];
$fname = $_REQUEST['fname'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}

$file_list_arr = getFileList($upload_dir_path);
$k = 0;
//var_dump($file_list_arr);
for($i = 0;$i < count($file_list_arr); $i++) {
	//echo $file_list_arr[$i];
	//echo "<br>";
	//if (substr($file_list_arr[$i], -3, 3) == "png" || substr($file_list_arr[$i], -3, 3) == "jpg") {
	//echo "a<br>";
	if (substr($file_list_arr[$i], -3, 3) == "png" || substr($file_list_arr[$i], -3, 3) == "jpg" || substr($file_list_arr[$i], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp") {
		$img_file_arr[$k] =  basename($file_list_arr[$i]);
		//echo "<br>";
		$k++;
	}
}

//var_dump($img_file_arr);
/*
//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}

//▲企業データの取得▲
*/

if ($img_file_arr) {

	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	$counter = 0;
	$error_num = 0;
    //$file_ary = reArrayFiles($img_file_arr);
	//$file_num = count($file_ary);
	
	//txtだけを先に読み込む。ブサイクだけどとりあえず。
	for($m = 0;$m < count ($img_file_arr) ;$m++) {//echo "c";
		if(!file_exists($txt_dir."/".substr($img_file_arr[$m], 0, -4).".txt")) {//echo "d";
			if ( substr($img_file_arr[$m], -3, 3) == "pdf" || substr($img_file_arr[$m], -3, 3) == "png" || substr($img_file_arr[$m], -3, 3) == "jpg" || substr($img_file_arr[$m], -3, 3) == "JPG" || substr($img_file_arr[$m], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp") {
				//echo "bmp1";
				//ファイル名変換処理
//				$img_file_arr[$m] = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);
				$file_name_top = substr($img_file_arr[$m], 0, -4);//echo ":top<br>\r\n";
				$file_extension = substr($img_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
				$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
				$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";

				$pdf_name = substr($filename, 0, -4);

				try {
					$im = new imagick();
				}catch (Exception $e) {
					echo $e->getMessage();
				}	
				$file_path = $upload_dir_path."/";
				$pdf_file = $file_path.$file_name_top.".pdf";
				//for ($m = 1;$m <= 120;$m++) {
 				if ($file_extension == ".png" || $file_extension == ".jpg" || $file_extension == ".bmp" ) {

					//shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
/*
					//ファイル名が出力ファイル名と違う時。
					if ($file_name_top != $dlp) {
					
						//pngファイルの時、出力ファイル名(dlp)と同名でpngを作り直し、元ファイルを消す。 
						if ($file_extension == ".png") {
							shell_exec("\mv -f ".$file_path.$file_name_top.".png"." ".$file_path.$dlp.".png 2>&1");
							shell_exec("rm -f ".$file_path.$file_name_top.".png 2>&1");
						}
					}
*/
/*
					//jpgファイルの時、出力ファイル名(dlp)と同名でpngを作り直し、元ファイルを消す。
					if ($file_extension == ".jpg") {
						shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".png 2>&1");	
					}
*/
					//echo $dlp."png";
					//shell_exec("convert -geometry 100% ".$file_path.$file_name_top.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//$ImgText = $cloudvision_con -> ImagetoText($file_path.$dlp.$file_extension,$feature);
					$ImgText = $cloudvision_con -> ImagetoText($file_path.$dlp.$file_extension,$feature);
					if ($ImgText == "") {
						//echo "2nd<br>";
						$ImgText = $cloudvision_con -> ImagetoText($file_path.$file_name_top.$file_extension,$feature);
					}
					
					// ファイルのパスを変数に格納
					$FileText = $file_path.$dlp.'.txt';
					// ファイルに書き込む
					file_put_contents($FileText,$ImgText,LOCK_EX);
					// ファイルを出力する
					//readfile($FileText);
					//exit;
					
	//				$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
	//				$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1");
					//var_dump($res);
				}
				if ($file_extension == ".tif") {
					//shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1");
					//var_dump($res);
				}
				if ($file_extension == ".bmp") {
					//shell_exec("convert -geometry 100% -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					//shell_exec("convert -monochrome ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".tif 2>&1");
					shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp."_1 2>&1");
					//$res = shell_exec("/usr/local/bin/tesseract ".$file_path.$dlp.".tif ".$file_path.$dlp." -l jpn 2>&1");
					//var_dump($res);
				}

					//PDFファイルをテキスト化する。
					//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png 2>&1");
					//echo "convert ".$file_path.".pdf -geometry 100% ".$file_path.".png";
				if ($file_extension == ".pdf") {
					//pdftotextでpdfから
					//shell_exec("pdftotext -raw ".$file_path."6988b98f05ca4df774733315f7725519.pdf ".$file_path."6988b98f05ca4df774733315f7725519.txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					shell_exec("pdftotext -raw ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					//shell_exec("pdftotext ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png ;");
					shell_exec("convert -density 600 ".$file_path.$dlp.".pdf ".$file_path.$dlp.".png > /logs/cloudinvoice/makeimg/moveImage/errlog".date('Ymdhis').".log 2>&1");
					//print_r ($output);
				}
					//var_dump($retval);
					//var_dump($last_line);
				$path = $file_path;
			}
		}
		try {		
//			shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
//			shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
			shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//shell_exec("rm -f ".$file_path.$dlp.".jpg 2>&1");
		} catch (Exception $e) {
			echo $e -> getMessage;
		}
	//	$DataNum++;
	}
	$_SESSION["comment"] = "データを登録しました。";
	//var_dump($_REQUEST);echo "<br>";
	//受信者データの登録処理
	$family_arr = array();
	for ($l0 = 1;$l0 <= 4;$l0++ ) {
		if ($_REQUEST["fname".$l0] != "") {
			$family_arr[$l0][0] = $_REQUEST["fname".$l0].$_REQUEST["lname".$l0];
			$family_arr[$l0][1] = $_REQUEST["relation".$l0];
			$family_arr[$l0][2] = $_REQUEST["fname".$l0];
			$family_arr[$l0][3] = $_REQUEST["lname".$l0];
			$family_arr[$l0][4] = $_REQUEST["lnamekn".$l0];
		}
	}
	//var_dump($family_arr);
	//exit;
	//画像を読み込みながら請求書データを作成し、txtの内容を取得して上書きする。
	for($m = 0;$m < count ($img_file_arr) ;$m++) {
		//echo "x";
		
		//初期化
		$exist_flag = 0;
		$checked_tel_num = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name = "";
		$product_name_arr = array();
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = array();
		$pay_method_arr = array();
		$industry_type_s = "";
		$pay_method = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$pay_method_cnt = 0;
		$pay_cnt = 0;
		$nodata_flag = 0;
		$telkey_flag = 0;
		$data2 = array();

		
		$zip_flag == 0;
		$address_flag = 1;
		$company_data_end_line = 0;
		$address_cnt = 0;
		$address2_cnt = 0;
		$address2_arr = array();
		$address_arr = array();

		$name_fix_flag = 0;
		$fixed_name = "";
		$name_arr = array();
		$relation_arr = array();
		$name_cnt = 0;
		
		try {
			
	        $filename = $img_file_arr[$m];
	        if (substr($filename, -3, 3) == "csv"){
	        	$csv_file_name = $filename;
	        }
				
			//ファイル名変換処理
//			$img_file_arr[$m] = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
			$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);
			$file_name_top = substr($img_file_arr[$m], 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($img_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			
			//請求書が登録されていないか確認する。
			//$flag ="download_invoice_data_clm";
			//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
			//同じdownload_passwordで請求書が登録済みの場合
			if (substr($filename, -3, 3) == "png" || substr($filename, -3, 3) == "jpg" || substr($filename, -3, 3) == "tif" || substr($filename, -3, 3) == "pdf"|| substr($img_file_arr[$m], -3, 3) == "bmp" ) {
				//請求書登録
				//仮のデータで請求書登録をする。
				//仮の請求書・領収書データの登録
				
				//テキストを読んで上書き処理
				//echo "テキスト処理<br>";
				$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
				$text_file_path = $txt_dir."/".$dlp.".txt";
				$FileText = $text_file_path;
				//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
				if(file_exists($text_file_path_en)) {
					$data = file_get_contents($text_file_path_en);
					$data .= file_get_contents($text_file_path);
				} else {
					$data = file_get_contents($text_file_path);
				}
				
				$data_all = $data;
				//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

				//var_dump($data);
				$data = str_replace(array("\n\n","\n\n"),array("\n"),$data);
				$data = str_replace(array("年\n\n","月\n\n"),array("年","月"),$data);
				$data = str_replace(array("年\n","月\n"),array("年","月"),$data);
				$data = str_replace(array("都\n","道\n","府\n","県\n","市\n","区\n","町\n","村\n","番地\n"),array("都","道","府","県","市","区","町","村","番地"),$data);
				$data = str_replace(array("年\n","月\n"),array("年","月"),$data);
				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				

				for ( $i = 0; $i < $cnt; $i++ ) {//echo e;
					
					//変数初期化
					$money_num_cnt = 0;
					$company_data_end_line = 0;
					$fixed_flag = 0;
					$zip_flag= 0;
					$section_flag = 0;
					$address_flag = 0;
					
					//全角文字を半角に変換
					//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
					$data2[$i] = $data[$i];//商品名と会社名の文字列を取得する時は数字を加工しない方が良いため分ける。
					$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
					$data[$i] = strtr($data[$i], $kana);
					$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
					$data[$i] = str_replace(array("'","'"," ","|","、","，","．"),"",$data[$i]);
					$data[$i] = str_replace(array("、"),",",$data[$i]);
					$data[$i] = str_replace(array("叩","m","0c","0C","0(1"),"00",$data[$i]);
					$data[$i] = str_replace(array("判"),"\4",$data[$i]);
					$data[$i] = str_replace(array("//","{{","[1"),"11",$data[$i]);
					$data[$i] = str_replace(array("。","。","〝","n","o","U","()"),"0",$data[$i]);
					$data[$i] = str_replace(array("■","]","L"),"1",$data[$i]);
					$data[$i] = str_replace(array("z"),"2",$data[$i]);
					$data[$i] = str_replace(array("會"),"3",$data[$i]);
					$data[$i] = str_replace(array("繍"),"4",$data[$i]);
					$data[$i] = str_replace(array("§"),"5",$data[$i]);
					$data[$i] = str_replace(array("s"),"6",$data[$i]);
					$data[$i] = str_replace(array("a","讐","B"),"8",$data[$i]);
					$data[$i] = str_replace(array("g","q"),"9",$data[$i]);
					$data[$i] = str_replace(array("藻","ff"),"年",$data[$i]);
					$data[$i] = str_replace(array("隷","A"),"月",$data[$i]);
					$data[$i] = str_replace(array("B","t1","曰",""),"日",$data[$i]);
					$data[$i] = str_replace(array("#明"),"年8月",$data[$i]);
					$data[$i] = str_replace(array("年9A","#9A"),"年9月",$data[$i]);
					$data[$i] = str_replace(array("年坍"),"年1月",$data[$i]);
					$data[$i] = str_replace(array("明旧"),"8月1日",$data[$i]);
					$data[$i] = str_replace(array("¨","－","一","・","~"),"-",$data[$i]);
					//$data[$i] = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data[$i]);
					$data[$i] = str_replace(array("合言 "),"合計",$data[$i]);
					$data[$i] = str_replace(array("合","卦","會"),"合",$data[$i]);
					$data[$i] = str_replace(array("小","′」ヽ"),"小",$data[$i]);
					$data[$i] = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data[$i]);
					$data[$i] = str_replace(array("オ寸","本寸"),"村",$data[$i]);
					$data[$i] = str_replace(array("言舌"),"話",$data[$i]);
					$data[$i] = str_replace("」R","JR",$data[$i]);
					$data[$i] = str_replace("西新宿NSGi会","西新宿NSG会",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/5","2015?","2015#"),"2015年",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/6","2016?","2016#","Zo/6年"),"2016年",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/6","2017?","2017#"),"2017年",$data[$i]);
					$data[$i] = str_replace("F]","円",$data[$i]);
					$data[$i] = str_replace(array("1EL","1E1","Te1","TE1","E1","e1"),"TEL",$data[$i]);
					$data2[$i] = str_replace(array("馬主"),"駐",$data2[$i]);
					$data2[$i] = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data2[$i]);
					$data2[$i] = str_replace(array("食年代"),"食事代",$data2[$i]);
					$data2[$i] = str_replace(array("商年"),"商事",$data2[$i]);
					$data2[$i] = str_replace(array("ゆつメル"),"ゆうメール",$data2[$i]);
					$data2[$i] = str_replace(array("収入5紙"),"収入印紙",$data2[$i]);
					$data2[$i] = str_replace(array("7ット"),"フット",$data2[$i]);
					$data2[$i] = str_replace(array("ライ7"),"ライフ",$data2[$i]);
					$data2[$i] = str_replace(array("SEKthSHO"),"セキショー",$data2[$i]);
					$data2[$i] = str_replace(array("JT8"),"JTB",$data2[$i]);
					$data2[$i] = str_replace(array("ENE0S","ENEOS","ENEoS"),"エネオス",$data2[$i]);
					$data2[$i] = str_replace(array("サ0ン"),"サロン",$data2[$i]);
					$data2[$i] = str_replace(array("DAIS0","DAISO"),"ダイソー",$data2[$i]);
					$data2[$i] = str_replace(array("MINIST0P","MINISTOP"),"ミニストップ",$data2[$i]);
					$data2[$i] = str_replace(array("LAWS0N","LAWSON"),"ローソン",$data2[$i]);
					$data2[$i] = str_replace(array("NewDays"),"ニューデイズ",$data2[$i]);
					$data2[$i] = str_replace(array("70ジャポン"),"フロジャポン",$data2[$i]);
					$data2[$i] = str_replace(array("プ0"),"プロ",$data2[$i]);
					$data2[$i] = str_replace(array("武蔵松山"),"武蔵松山カントリークラブ",$data2[$i]);
					$data[$i] = str_replace(array("¥","$","#","="),"￥",$data[$i]);
					$data[$i] = str_replace(array(":-"),".",$data[$i]);
					$data[$i] = str_replace(array("\n様"),"様",$data[$i]);
					$data[$i] = str_replace(array($fname."\n"),$fname,$data[$i]);
					//echo "<br>";
					//echo $data[$i];
					//echo "<br>";
					//-を用いた日付で時間がスペースの後に時間がついていた場合取得できなくなるため、スペースを消す前に移動した。
					
					$pattern_comp_end = "/^[*]{1}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data[$i] = str_replace(array("*"),"￥",$data[$i]);
					}
					
					//初期加工のみで利用したい場合の変数
					$ori_data = $data[$i];
					
					if ($fixed_flag == 0) {
						$pattern_comp_end = "/(東京都|北海道|(?:京都|大阪)府|[^0-9a-zA-Z]{6,9}県)((?:四日市|廿日市|野々市|かすみがうら|つくばみらい|いちき串木野)市|(?:杵島郡大町|余市郡余市|高市郡高取)町|.{3,12}市.{3,12}区|.{3,9}区|.{3,15}市(?=.*市)|.{3,15}市|.{6,27}町(?=.*町)|.{6,27}町|.{9,24}村(?=.*村)|.{9,24}村)(.*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						$match[0] = str_replace(array("〒","�"),"",$match[0]);
						//var_dump($match);
						//echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
						//	echo "住所候補<br/>\r\n";
							$address_arr[$address_cnt] = $data[$i] = $match[0];//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$address_cnt++;
							$company_data_end_line = $i;
							$fixed_flag = 1;
							$address_flag = 1;
						}
					}
					
					//住所
					if ($address_flag == 0 && ($fixed_flag == 0)) {
						$pattern_comp_end = "/((?:四日市|廿日市|野々市|かすみがうら|つくばみらい|いちき串木野)市|(?:杵島郡大町|余市郡余市|高市郡高取)町|[^0-9a-zA-Z]{3,12}市[^0-9a-zA-Z]{3,12}区|[^0-9a-zA-Z]{3,9}区|[^0-9a-zA-Z]{3,15}市(?=.*市)|[^0-9a-zA-Z]{3,15}市|[^0-9a-zA-Z]{6,27}町(?=[^0-9a-zA-Z]*町)|[^0-9a-zA-Z]{6,27}町|[^0-9a-zA-Z]{9,24}村(?=.*村)|[^0-9a-zA-Z]{9,24}村)(.*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						$match[0] = str_replace(array("〒","010"),"",$match[0]);
						//var_dump($match);
						//echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
							//echo "<br/>\r\n住所候補2<br/>\r\n";
							$address_arr[$address_cnt] = $data[$i] = $match[0];//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$address_cnt++;
							$company_data_end_line = $i;
							$fixed_flag = 1;
							$address_flag = 1;
						}
					}
					
					//住所2
					if ($fixed_flag == 0) {
						$pattern_comp_end = "/(\W*\w*(?:号室|ビル|棟|ハイツ|パレス|ヒルズ|プラザ|館).*$)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						
						//echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
						//	echo "<br/>\r\n住所2候補<br/>\r\n";var_dump($match);
							$address2_arr[$address2_cnt] = $data[$i] = str_replace(array("\\","￥"),"",$match[1]);//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$address2_cnt++;
							$company_data_end_line = $i;
							$fixed_flag = 1;
							$address_flag = 1;
						}
					}
					//echo "abc";
					//echo $data[$i];
					//echo "<br>";
					//echo $name_fix_flag;
					//echo "<br>";
					
					//名前
					if ($fixed_flag == 0 && $name_fix_flag == 0) {
						for ($l1 = 1;$l1 <= 4;$l1++) {
							for ($l2 = 2;$l2 <= 4;$l2++) {
								if ($family_arr[$l1][$l2] != "") {
									$pattern_comp_end = "/".$family_arr[$l1][$l2]."/";
									
									$data[$i] = str_replace(array("ヽ一ヽ","σ","、","＼、","＼","﹁","」三;","ヽ，コ"),"",$data[$i]);
									$data[$i] = str_replace(array("丁"),"T",$data[$i]);
									$data[$i] = str_replace("-","一",$data[$i]);
									$match_num = preg_match($pattern_comp_end, $data[$i], $match);
									//var_dump($match);
									if ($match_num > 0) {
										$str_length = mb_strlen($match[0],"utf-8");
										//var_dump($family_arr[$l]);
										$name_arr[$name_cnt] = $family_arr[$l1][0];//echo "\r\n";
										$relation_arr[$name_cnt] = $family_arr[$l1][1];//echo "\r\n";
										$data_arr[$i] = $data[$i];
										//echo "<br/>\r\n";
										$company_data_end_line = $i;
										$fixed_flag = 1;
										$name_cnt++;
										break;

									}
								}
							}
						}
					}
					
					//名前
					if ($fixed_flag == 0) {
						$pattern_comp_end = "/氏名\W{6,36}/";
						
						$data[$i] = str_replace(array("ヽ一ヽ","σ","、","＼、","＼","﹁","」三;","ヽ，コ"),"",$data[$i]);
						$data[$i] = str_replace(array("丁"),"T",$data[$i]);
						$data[$i] = str_replace("-","一",$data[$i]);
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//var_dump($match);
						
						$pattern = '/\W+/u';
						//echo "<br/>name:";
						$match_text_num = preg_match($pattern, $data[$i],$text_match);
						//var_dump($text_match);
						if ($match_text_num == 0) {
							$match[0] = str_replace(array("丼","圍","様","氏名","患者名"),array("井","園","","",""),$match[0]);
							$str_length = mb_strlen($match[0],"utf-8");
							if ($str_length >= 3) {
								$fixed_name = $name_arr[$name_cnt] = $data[$i] = str_replace(array("\\","￥"),"",$match[0]);//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//echo "<br/>\r\n";
								$company_data_end_line = $i;
								$fixed_flag = 1;
								$name_fix_flag = 1;
								$name_cnt++;

							}
						}
					}

					if ($fixed_flag == 0) {
						$pattern_comp_end = "/\W{6,36}様/";
						
						$data[$i] = str_replace(array("ヽ一ヽ","σ","、","＼、","＼","﹁","」三;","ヽ，コ"),"",$data[$i]);
						$data[$i] = str_replace(array("丁"),"T",$data[$i]);
						$data[$i] = str_replace("-","一",$data[$i]);
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//var_dump($match);
					
						
						$pattern = '/\W+/u';
						//echo "<br/>name:";
						$match_text_num = preg_match($pattern, $data[$i],$text_match);
						//var_dump($text_match);
						if ($match_text_num == 0) {
							$match[0] = str_replace(array("丼","圍","様","氏名","患者名"),array("井","園","","",""),$match[0]);
							$str_length = mb_strlen($match[0],"utf-8");
							if ($str_length >= 3) {
								$fixed_name = $name_arr[$name_cnt] = $data[$i] = str_replace(array("\\","￥"),"",$match[0]);//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//echo "<br/>\r\n";
								$company_data_end_line = $i;
								$fixed_flag = 1;
								$name_fix_flag = 1;
								$name_cnt++;

							}
						}
					}

					//名前
					if ($fixed_flag == 0 && $name_fix_flag == 0) {
						$pattern_comp_end = "/\W{6,30}/";
						
						$data[$i] = str_replace(array("ヽ一ヽ","σ","、","＼、","＼","﹁","」三;","ヽ，コ"),"",$data[$i]);
						$data[$i] = str_replace(array("丁"),"T",$data[$i]);
						$data[$i] = str_replace("-","一",$data[$i]);
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//var_dump($match);
						
						$pattern = '/\W+/u';
						//echo "<br/>name:";
						$match_text_num = preg_match($pattern, $data[$i],$text_match);
						//var_dump($text_match);
						if ($match_text_num == 0) {
							$match[0] = str_replace(array("丼","圍","様","氏名","患者名"),array("井","園","","",""),$match[0]);
							$str_length = mb_strlen($match[0],"utf-8");
							if ($str_length >= 3) {
								$name_arr[$name_cnt] = $data[$i] = str_replace(array("\\","￥"),"",$match[0]);//echo "\r\n";
								$data_arr[$i] = $data[$i];
								//echo "<br/>\r\n";
								$company_data_end_line = $i;
								$fixed_flag = 1;
								$name_cnt++;
							}
						}
					}
					
					if ($fixed_flag == 0) {
						$data[$i] = str_replace("一","-",$data[$i]);
						$data[$i] = str_replace(array("T"),"丁",$data[$i]);
						$pattern_comp_end = "/([0-9]{2,4})[\/\.\-\, －年]{1,2}([0-9]{1,2})[\/\.\-\, 一－月]{1,2}([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補a<br/>\r\n";
							//var_dump($match);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;
								$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
								//echo $data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$fixed_flag = 1;
								$company_data_end_line = $i;
							}
						}
					}
					
					if ($fixed_flag == 0) {
						$data[$i] = str_replace(array(".","．"),"",$data[$i]);
						$pattern_comp_end = "/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補b<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;

								$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
								//echo $data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$fixed_flag = 1;
								$company_data_end_line = $i;
							}
						}
					}
					
					if ($fixed_flag == 0) {
						$pattern_comp_end = "/([0-9]{2,4}),([0-9]{1,2}),([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補c<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;

								$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
								//echo $data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$fixed_flag = 1;
								$company_data_end_line = $i;
							}
						}
					}
					//電話番号用処理
					
					//$tel_test = preg_replace('/[^0-9]/', '', $data[$i]);
					//数字だけ抜き出す処理だと合計金額などと区別しづらいかも。
					$tel_test = str_replace(array("_"),"",$data[$i]);
					if (intval($tel_test) != 0 ) {
						$data[$i] = str_replace(array("(","（","_"),"-",$data[$i]);
					}
					$data[$i] = str_replace(array(" ","　"),"",$data[$i]);
					$data[$i] = str_replace(array("(","（"),"-",$data[$i]);
					$data[$i] = str_replace(array(")","）"),"-",$data[$i]);
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "aaatel<br>\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}[- ]+[0-9]{2,4}[- ]+[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}

					//電話番号取得（番号を取得）
					//echo $data[$i];echo "bbbtel<br>\r\n";
					$pattern_comp_end = "/0{1}[0-9]{9}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}\([0-9]{2,4}\)[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							//echo $match[0];
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					if ($tel_cnt == 0) {
						//電話番号取得（番号を取得）
						//echo $data[$i];echo "\r\n";
						$pattern_comp_end = "/[1-9]{1}[0-9]{1,3}-[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{6,8}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
						}
					}
					
					$data[$i] = str_replace(array(","),"",$data[$i]);
					if ($money_num_cnt == 0) {
						//echo $data[$i];
						//$pattern_comp_end = "/合計(\\\w*)/";
						$pattern_comp_end = "/合計.*[額]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "合計金額候補B<br/>\r\n";
							//$goukei_top = $goukei = $data_arr[$i] = str_replace(array("\\","￥","円","計","合","額","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","合","額","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//	$goukei = substr($goukei,2);
							$data_arr[$i] = $data[$i];
							$money_cnt++;
							$money_num_cnt++;
						}
					}

					if ($money_num_cnt == 0) {
						$data[$i] = str_replace(",","",$data[$i]);
						$pattern_comp_end = "/小計[額]*.*[￥]*(\d*)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補 小計<br/>\r\n";
							//echo $data[$i];
							//echo "<br>";
							$syoukei = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","小","額","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/計.*[額]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補C<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","額","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/消費税[等]*[￥]*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補 計<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","月","消費税","等",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/請求金額.*(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補E<br/>\r\n";
							$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
					if ($money_num_cnt == 0) {
						//点数を除外する。
						$pattern_comp_end = "/(\d*)点/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額除外候補<br/>\r\n";
						//echo "<br>金額除外候補 点";echo $data[$i];var_dump($match);
							//$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","点",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							//$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					if ($money_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/(\d+)円/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補F<br/>";
						//echo "<br>金額候補 円";echo $data[$i];var_dump($match);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","月",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}

					if ($money_num_cnt == 0) {
						$data[$i] = str_replace(array("Y","F","軍"),"\¥",$data[$i]);
						$data[$i] = str_replace(array("D","O","〕"),"0",$data[$i]);
						$data[$i] = str_replace(array("∞"),"00",$data[$i]);
						$data[$i] = str_replace(array("ヮ","i","ぅ","ゥ","′","り",",","#"),"",$data[$i]);
						$data[$i] = str_replace(array("\\"),"￥",$data[$i]);
						$pattern_comp_end = "/￥(\d+)/";
						//$pattern_comp_end = "/(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//echo"<br>金額:";echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
							//echo "金額候補\<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","月",",","，","。",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";var_dump($money_arr[$money_cnt]);
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					//echo $data[($i - 1)];echo ":前<br>";
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/(\d{3,7})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						
						if ($data[($i - 2)] == "合計"|| $data[($i - 1)] == "合計") {
						
							//echo "金額候補A<br/>\r\n";
							//$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
						//echo $ori_data;echo $money_num_cnt;
						//echo "<br>";
					if ($money_num_cnt == 0) {
						
						//金額が入っていない時の候補
						$pattern_comp_end = "/([1-9]{1}[0-9]{0,2},[0-9]{3})/";
						$match_num = preg_match($pattern_comp_end, $ori_data, $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
							$money_arr[$money_cnt] = $ori_data = str_replace(array("\\","￥","円","計","月",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $ori_data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					//echo $data[$i];//echo "mncnt:".$money_num_cnt;echo "<br>";

					if ($fixed_flag == 0) {
						$data[$i] = str_replace(array("-","_","－","＿","・","･",".","．","`",":"),"",$data[$i]);
						$data[$i] = str_replace(array("午","撃","#"),"年",$data[$i]);
						$data[$i] = str_replace("l","1",$data[$i]);
						//echo "<br/>";
						
						$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})日/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補aaa<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}

					if ($fixed_flag == 0) {
						$pattern_comp_end = "/([0-9]{1,4})[年A#]*([0-9]{1,2})月.*([0-9]{1,2})日/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$data[$i] = str_replace(array("-","_","－",),"",$data[$i]);
							//echo "日付候補bbb<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo "ccc<br>";
							//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}

					if ($fixed_flag == 0) {
						$pattern_comp_end = "/(\d*)年(\d*)月(\d*)/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$data[$i] = str_replace(array("-","_","－",),"",$data[$i]);
							//echo "日付候補d<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}

					if ($fixed_flag == 0) {
						$pattern_comp_end = "/(\d*)年(\d*).*(\d*))/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$data[$i] = str_replace(array("-","_","－",),"",$data[$i]);
							//echo "日付候補d<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}

					if ($fixed_flag == 0) {
						$data[$i] = str_replace(array("′","ノ",",-",":-"),"/",$data[$i]);//日付用変換
						$pattern_comp_end = "/([0-9]+)[\/\.- ]{1}([0-9]{1,2})[\/\.- ]{1}([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;

							//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}
					if ($fixed_flag == 0) {
						$data[$i] = str_replace("/","1",$data[$i]);
						$data[$i] = str_replace("-","",$data[$i]);
						//echo "<br/>";
						$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$fixed_flag = 1;
							$company_data_end_line = $i;
						}
					}
					
					if ($fixed_flag == 0) {
						$pattern_comp_end = "/([0-9]{2,4}).([0-9]{1,2}).([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							//var_dump($match);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;
								//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
								//echo $data_arr[$i] = $data[$i];
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$fixed_flag = 1;
								$company_data_end_line = $i;
							}
						}
					}
					
					if  ($pay_method_cnt == 0) {
						//会社名を取得する
						$pay_check_arr = array("\W{3,24}支払","クレジット利用","クレジット扱","ポイント利用","クレジット決済");
						$last_company = "";
						$match = "";
						$pay_method_arr = array();

						for ($z = 0;$z < count($pay_check_arr);$z++) {
							$pattern_pay_end = "/".$pay_check_arr[$z]."/";
							//echo "<br>";
							$match_num = preg_match($pattern_pay_end, $data2[$i], $match);
							//echo var_dump($match);
							
							//echo $data2[$i];
						//	echo "支払関連";
							//echo "<br>";
							if ($match_num >= 1) {
								$pay_method = $pay_method_arr[$pay_cnt] = str_replace(array(" ","　"),"",$match[0]);
								//echo "<br>支払い方法候補<br/>\r\n";
								//var_dump($match);
								//echo $data2[$i];
								//echo "<br/>";
								$pay_cnt++;
								$pay_method_cnt++;
								break;

							}
						}
					}

					if  ($pdt_cnt == 0) {
						//商品名を取得する
						//商品名の為に1をーに変換し直す。
						//$data2[$i] = str_replace(array("1"),"ー",$data2[$i]);
						//$product_check_arr = array("食事代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENE0Sヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","定期券","乗車券類","特急券","普通券","飲食代","レストラン","宿泊代","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","回数券","乗車券","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",",*航空便.*","簡易書留",".*切手.*","水道料金","サントリー","チケット代","ゴルフプレー.*代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車","収入印紙","印紙");
						$product_check_arr = array("食事代","飲食代","切手代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","電話料金等","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENEOSヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","飲食代","レストラン","宿泊代",".*代として",".*料として","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",".*書留.*",",*航空便.*","簡易書留",".*切手.*","水道料金","ガス料金","電気料金","サントリー","チケット代","ゴルフプレー.*代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車","印紙代");
						$p_cnt = 0;
						$last_product = "";
						$match = "";
						$product_arr = array();
						for ($z = 0;$z < count($product_check_arr);$z++) {
							$pattern_comp_end = "/".$product_check_arr[$z]."/";
							$pattern_comp_end;//echo "<br>";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							//echo "<br>";
							if ($match_num >= 1) {
								$product_arr[$p_cnt] = $match[0];
								//echo "<br>商品候補<br/>\r\n";
								//var_dump($match);
								//echo $data2[$i];
								//echo "<br/>";
								//exit;
								if ($match[0] == "収入印紙" ||$match[0] == "印紙" ||$match[0] == "小切手" ) {
								} else {
									if ($product_arr[$p_cnt] != $last_product) {
										if ($p_cnt != 0) {$space = " ";}
										$last_product = $product_arr[$p_cnt];
										$product_name_arr[$p_cnt] = $space.$product_arr[$p_cnt];
										$p_cnt++;
										if ($match[0] != "収入印紙" ||$match[0] != "印紙" ||$match[0] != "小切手" ) {
											$pdt_cnt++;
											break;
										}
									}
								}
							}
						}
					}
					//echo $data2[$i];
					
					if  ($comp_cnt == 0) {
						//会社名を取得する
						$company_check_arr = array("東海旅客","JR東日本","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","セキショー","宇佐美","小田急.*","東京地下鉄","東日本旅客","ケーズデンキ","ベスト電器","ビックカメラ","Express","エネオス","シェル石油","コスモ石油","Unidy","ユニディ","ケーヨーデイツー","オリンピック","loft",".*JTB.*","ロフト","サブウェイ","CAFE de CRIE","ABC-MART",".*駐車場.*",".*パーク.*",".*パーキング.*",".*駐輪場","bookshelf","くすりの福太郎","リンガーハット","STARBUCKS","スターバックス","コメダ珈琲店","タリーズコーヒー","ドトールコーヒー","エクセルシオール","カフェ・ド・クリエ","ベローチェ","ジェーソン","イトーヨーカドー","ガスト","ロイヤルホスト","デニーズ","ココス","夢庵","サイゼリヤ","ジョイフル","ジョナサン","バーミヤン","新鮮市場","カインズ",".*株式会社.*",".*有限会社.*",".*（株）.*",".*（有）.*",".*公社",".*水道局","マクドナルド","モスバーガー","ファッションセンターしまむら","ジーユー","ユニクロ","UT","アーバンリサーチ","SHIPS","シップス","ヴィクトリアゴルフ","マルエイ","ダイソー","DAISO","(.*市)会計管理者","武蔵松山カントリークラブ","アップルパーク勝どき第1","タイムズ","ユアーズパーク","コインパーク","タイムパーク","パーク２４","八重洲駐車場","マンゴツリー","東海パッセン","未来堂","ステーキてっぺい",".*パーキング","東京急行電鉄","阪急鉄道","南海電鉄","阪神電鉄","東急電鉄","東武電鉄","西武鉄道","東日本旅客鉄道","東京地下鉄","東京急行電鉄","京阪電気鉄道","小田急電鉄","西武鉄道","京成電鉄","京浜急行電鉄","東武鉄道","西日本旅客鉄道","京王電鉄","近畿日本鉄道","阪急電鉄","阪神電気鉄道","西日本鉄道","南海電気鉄道","東海旅客鉄道","名古屋鉄道","九州旅客鉄道","四国旅客鉄道","北海道旅客鉄道","相模鉄道","日本貨物鉄道","IGRいわて銀河鉄道","IRいしかわ鉄道","WILLER TRAINS","あいの風とやま鉄道","アルピコ交通","いすみ鉄道","えちごトキめき鉄道","えちぜん鉄道","くま川鉄道","しなの鉄道","スカイレールサービス","とさでん交通","のと鉄道","ひたちなか海浜鉄道","ゆりかもめ","わたらせ渓谷鐵道","阿佐海岸鉄道","阿武隈急行","愛知環状鉄道","愛知高速交通","伊賀鉄道","伊勢鉄道","伊豆急行","伊豆箱根鉄道","伊予鉄道","衣浦臨海鉄道","井原鉄道","一畑電車","叡山電鉄","遠州鉄道","横浜シーサイドライン","横浜高速鉄道","横浜市交通局","岡山電気軌道","岡本製作所","沖縄都市モノレール","会津鉄道","岳南電車","甘木鉄道","関西高速鉄道","関西電力 - 無軌条式・本社は大阪府","関東鉄道","紀州鉄道","京都市交通局","京福電気鉄道","京葉臨海鉄道","錦川鉄道","近江鉄道","熊本市交通局","熊本電気鉄道","御岳登山鉄道","広島高速交通","広島電鉄","弘南鉄道","江ノ島電鉄","高松琴平電気鉄道","高尾登山電鉄","黒部峡谷鉄道","嵯峨野観光鉄道","阪堺電気軌道","埼玉高速鉄道","埼玉新都市交通","皿倉登山鉄道","三岐鉄道","三陸鉄道","山形鉄道","山万・本社は東京都","山陽電気鉄道","四国ケーブル","四日市あすなろう鉄道","鹿児島市交通局","鹿島臨海鉄道","芝山鉄道","若桜町","若桜鉄道","首都圏新都市鉄道","秋田内陸縦貫鉄道","秋田臨海鉄道","小湊鐵道","松浦鉄道","湘南モノレール","上信電鉄","上田電鉄","上飯田連絡線","上毛電気鉄道","信楽高原鐵道","新関西国際空港","新京成電鉄","真岡鐵道","神戸すまいまちづくり公社","神戸高速鉄道","神戸市交通局","神戸新交通","神戸電鉄","神奈川臨海鉄道","水間鉄道","水島臨海鉄道","成田空港高速鉄道","成田高速鉄道アクセス","西大阪高速鉄道","西濃鉄道","静岡鉄道","仙台空港鉄道","仙台市交通局","千葉ニュータウン鉄道","千葉都市モノレール","泉北高速鉄道","多摩都市モノレール","大井川鐵道","大阪外環状鉄道","大阪港トランスポートシステム","大阪高速鉄道","大阪市交通局","大山観光電鉄","樽見鉄道","丹後海陸交通","智頭急行","筑波観光鉄道","筑豊電気鉄道","秩父鉄道","中之島高速鉄道","中部国際空港連絡鉄道","銚子電気鉄道","長崎電気軌道","長野電鉄","長良川鉄道","津軽鉄道","天竜浜名湖鉄道","土佐くろしお鉄道","島原鉄道","東海交通事業","東京モノレール","東京都交通局","東京臨海高速鉄道","東葉高速鉄道","道南いさりび鉄道","奈良生駒高速鉄道","南阿蘇鉄道","能勢電鉄","箱根登山鉄道","八頭町","比叡山鉄道","肥薩おれんじ鉄道","富山ライトレール","富士急行","舞浜リゾートライン","福井鉄道","福岡市交通局","福島交通","福島臨海鉄道","平成筑豊鉄道","豊橋鉄道","北越急行","北近畿タンゴ鉄道","北九州高速鉄道","北九州市","北条鉄道","北神急行電鉄","北総鉄道","北大阪急行電鉄","北陸鉄道","万葉線","名古屋ガイドウェイバス","名古屋市交通局","名古屋臨海高速鉄道","名古屋臨海鉄道","明知鉄道","野岩鉄道","由利高原鉄道","養老鉄道","立山黒部貫光","流鉄","六甲山観光","和歌山電鐵",".*地下鉄.*",".*鉄道.*",".*交通局.*","日本交通","平和交通","西新宿NSG会",".*税理士",".*行政書士",".*司法書士",".*行政書士協同組合",".*税理士会",".*医師会",".*司法書士会",".*商工会議所","東京電力","東京ガス","法務局",".*道路株式会社","NEXCO.*日本","NTTファイナンス","NTTファイナンス","パーキング","KDDI","ソフトバンク","softbank","SoftBank","国税局",".*税事務所","スターバックス","ファミリーマート","NewDays","ローソン","LAWSON","セブンイレブン","セブン-イレブン","セブン‐イレブン","サンクス","ココストア","ミニストップ","ポプラ","スリーエフ",".*ホテル",".*カントリークラブ",".*ゴルフ場",".*ゴルフ倶楽部",".警察","郵便局","関西ガス","関西電力","九州電力","東北電力",".*役場",".*市役所",".*区役所",".*金銭出納員",".*出納員",".*会計管理者",".*法律事務所",".*病院",".*医院",".*歯科医院",".*歯科",".*クリニック",".*クリニ.*",".*外科",".*内科",".*皮膚科",".*耳鼻咽喉科",".*整骨院",".*接骨院","医療法人.*","ヤマト運輸","クロネコヤマト","佐川急便","ペリカン便","西濃運輸","赤帽","売捌所","売さばき",".*公証人.*",".*新聞","ヤマトフィナンシャル");
						$p_cnt = 0;
						$last_company = "";
						$match = "";
						$company_arr = array();
						for ($z = 0;$z < count($company_check_arr);$z++) {
							$pattern_comp_end = "/".$company_check_arr[$z]."/";
							//echo "<br>";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							//echo "<br>";
							if ($match_num >= 1) {
								
								//echo $match[0];
								//echo "<br>";
								$match2 = "";
								$match3 = "";
								$match_num2 = 0;
								$match_num3 = 0;
								$no_check_flag = 0;
								$match_num2 = preg_match("/御中/", $match[0], $match2);
								$match_num3 = preg_match("/様/", $match[0], $match3);
								
								if ($data[($i + 2)] == "御中"|| $data[($i + 1)] == "御中"||$data[($i + 2)] == "様"|| $data[($i + 1)] == "様") {
									$no_check_flag = 1;
								}
								//var_dump($match2);	var_dump($match3);
								//echo $no_check_flag."<br>";
								if ($match_num2 == 0 && $match_num3 == 0 && $no_check_flag == 0) {
									$company_arr[$p_cnt] = $match[0];
									//echo "<br>会社名候補<br/>\r\n";
									//var_dump($match);
									//echo $data2[$i];
									//echo "<br/>";
									if ($company_arr[$p_cnt] != $last_company) {
										if ($p_cnt != 0) {$space = " ";}
										$last_company = $company_arr[$p_cnt];
										$company_name_arr[$p_cnt] = $space.$company_arr[$p_cnt];
										$p_cnt++;
										$comp_cnt++;
										break;
									}
								}
							}
						}
					}
					//echo $data[$i];echo " ".$i."<br/>";
				}//for($iの終わり)
				//echo $i;echo "iの終わり<br>";
				//情報の整理
				$got_amount_of_money = 0;
				$max_num_times = 0;
				$fixed_flag = 0;
				$max_box = 0;
				//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
				if ($goukei_top == 0) {
					//echo "金額候補を絞る";
					//計を取得できた場合、その最大値を合計と見做す。
					if ($sum_flag >= 1) {
						for ($j = 0;$j < $sum_flag;$j++) {
							if ($max_box <= $sum_box[$j]) {
								$max_box = $sum_box[$j];
							}
						}
						//echo "z<br>";
						$got_amount_of_money = $max_box;
						$fixed_flag = 1;
					}
					$mcnt = count($money_arr);
					rsort($money_arr);
					if (max($money_arr) > 0) {
						$max1 = $money_arr[0];
							if ($mcnt >= 1 ) {
								$max2 = $money_arr[1];
							}
							if ($mcnt >= 2 ) {
								$max3 = $money_arr[2];
							}
							$max1_30 = round($max1 * 30/100,-1);
							$max1_30_2 = floor($max1 * 30/100);
							$max1_10 = round($max1 * 10/100,-1);
							$max1_10_2 = floor($max1 * 10/100);
							
						if ($mcnt >= 1 && ($max2 == $max1_30||$max2 == $max1_30_2||$max2 == $max1_10||$max2 == $max1_10_2)) {
							//大きい方から2番目の数が一番大きい数の30%もしくは10%と一致する時、2番目の数を支払額とする。
							$got_amount_of_money = $max2;
						} else if ($mcnt >= 2 && ($max3 == $max1_30||$max3 == $max1_30_2||$max3 == $max1_10||$max3 == $max1_10_2)) {
							//大きい方から3番目の数が一番大きい数の30%もしくは10%と一致する時、3番目の数を支払額とする。（2番目の数を誤取得した時のため）
							$got_amount_of_money = $max3;
						} else {
							//保険診療ではない支払と見做して一番大きい金額を支払額とする。
							$got_amount_of_money = max($money_arr);
						}
						$fixed_flag = 1;
					} else {
						if ($goukei == 0) {
							if ($syoukei > 0) {
								//echo "小計";echo "<br>";
								$got_amount_of_money = round($syoukei * 108/100,0);
								$fixed_flag = 1;
								//echo "<br>";
							}

							for ($n = 0;$n < count($money_arr);$n++){
								$temp_money = intval($money_arr[$n]);
								
								if($fixed_flag == 0) {
									if ($temp_money != 0 ||$temp_money != 1) {
										//echo "<br/>".$n."tmpmoney".$temp_money."<br>";
										//直後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
										if ($n >= 2) {
											$after2 = abs($temp_money - intval($money_arr[$n-1]));
											if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
												//echo "a<br>";
												$got_amount_of_money = intval($money_arr[$n-2]);
												$fixed_flag = 1;
											}
										}
										/*
										//直前の2つの金額を足したものと同額であれば合計と見做す。(小計と外税と合計)
										if ($fixed_flag == 0) {
											if ($n >= 2) {
												echo "小計と外税と合計";
												//echo $temp_money;
												$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
												if ($before2 == $temp_money && $before2 != 0) {
													$got_amount_of_money = $temp_money;
													$fixed_flag = 1;
													echo "d<br>";
												}
											}
										}
										*/
										if ($fixed_flag == 0) {
											//直後の1つを飛ばしたその後2つの金額の差の絶対値と同額であれば合計と見做す。
											if ($n >= 3) {
												$after2 = abs($temp_money - intval($money_arr[$n-1]));
												if ($after2 == intval($money_arr[$n-3]) && $after2 != 0) {
													//echo "d<br>";
													$got_amount_of_money = intval($money_arr[$n-3]);
													$fixed_flag = 1;
												}
											}
										}
										/*
										//直後の金額を引いたものに8%をかけて四捨五入したものと直後の金額が一致していたら合計と見做す（上に合計で内税の場合）
										if ($fixed_flag == 0) {
											if ($n >= 0) {
												echo $money_arr[$n+1];echo "<br>";
												echo $skei = $money_arr[$n] - $money_arr[$n+1];echo "<br>";
												echo $stax = round(($skei * 8/100),0);
												$stax2 = floor($skei * 8/100);
												$stax3 = ceil($skei * 8/100);
												if ($stax == $money_arr[$n+1]||$stax2 == $money_arr[$n+1]||$stax3 == $money_arr[$n+1]) {
													echo "内税のとこ<br>";
													$got_amount_of_money = $temp_money;
													$fixed_flag = 1;
												}
											}
										}
										*/

										//直前の金額を引いたものに8%をかけて四捨五入したものと直後の金額が一致していたら合計と見做す（下に合計で内税の場合）
										if ($fixed_flag == 0) {
											if ($n >= 1) {
												$skei = intval($money_arr[$n]) - intval($money_arr[$n-1]);
												$stax = round(($skei * 8/100),0);
												$stax2 = floor($skei * 8/100);
												$stax3 = ceil($skei * 8/100);
												if ($stax == $money_arr[$n-1]||$stax2 == $money_arr[$n-1]||$stax3 == $money_arr[$n-1]) {
													//echo "内税のとこ２<br>";
													$got_amount_of_money = $temp_money;
													$fixed_flag = 1;
												}
											}
										}
										
										//無い方が金額の精度が上がる可能性があるので一旦コメントアウトする。20170728hmsk
										if($fixed_flag == 0) {
											$num_of_same = 0;
											$candidate_box = 0;
											//var_dump($money_arr);
											//echo			count($money_arr);
											//同じ数字が2回以上あるものを取得するロジック
											for ($h = 0;$h < count($money_arr);$h++){
												//echo "<br/>tmp2 temp:";
												$temp_money2 = intval($money_arr[$h]);
												//echo " 2回";echo $temp_money;
												//if ($temp_money == $temp_money2){echo "同じ？？<br>";}
												if ($temp_money == $temp_money2) {
													$num_of_same++;
												}
											}
											//echo "<br>回数：".$num_of_same;
											//2回以上のものの中から回数が最大のもの
											if ($num_of_same >= 2) {

												//2回以上のものの中で最大の金額
												if($got_amount_of_money < $temp_money) {
													//echo "e<br/>";
													$got_amount_of_money = $temp_money;
												}
											}
										}
										
									}
								}
								
								//▼お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▼
								//直後の2つの金額の上の金額から下の金額を引いた額と同額であれば合計と見做す。（合計と預かりとお釣り）
								if ($n >= 2) {
									//echo $n;echo " b－2のとこ ";
									//var_dump($money_arr);
									//echo intval($money_arr[$n-1])." ".$temp_money." ".$money_arr[$n]."<br/>";
									$after2 = intval($money_arr[$n-1]) - $temp_money;
									if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
										//echo "b-2<br>";
										$got_amount_of_money = intval($money_arr[$n-2]);
										$fixed_flag = 1;
									}
								}

								//一つ飛ばした後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
								if ($n >= 3) {
									//echo $n;echo " bのとこ ";echo $temp_money."<br/>";
									//echo intval($money_arr[$n-1])."<br/>";
									$after3 = abs($temp_money - intval($money_arr[$n-1]));
									if ($after3 == intval($money_arr[$n-3]) && $after3 != 0) {
										//echo "b<br>";
										$got_amount_of_money = intval($money_arr[$n-3]);
										$fixed_flag = 1;
									}
								}

							/*
								if ($n >= 2) {
									echo "小計と外税と合計（外側）";
									//echo $temp_money;
									$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
									if ($before2 == $temp_money && $before2 != 0) {
										$got_amount_of_money = $temp_money;
										$fixed_flag = 1;
										echo "d<br>";
									}
								}
								//▲お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▲
							*/
							}
						} else {
							//echo "else";
							$got_amount_of_money = $goukei;
						}
					}
				} else {
					//echo "top";
					$got_amount_of_money = $goukei_top;
				}
				
				//入れる金額が無かった場合、一番上の金額を入れる。
				if ($got_amount_of_money == 0 ) {
					//一番上が0円の場合、一番最後の数字を入れる。
					if ($money_arr[0] == 0) {
						//echo "f<br>";echo $money_arr[0];
						$marr = count($money_arr) - 1;
						$got_amount_of_money = intval($money_arr[$marr]);
					} else {
						//echo "f2<br>";echo $money_arr[0];
						$got_amount_of_money = intval($money_arr[0]);						
					}
				}
				
				//exit;
				//echo "<br/>";echo $got_amount_of_money." 合計<br/><br/>";//exit;
				//var_dump($date_arr);
				//日付
				if ($got_date_top[0] == "") {
					for ($d = 0;$d < count($date_arr);$d++) {
						//候補の中の一番最初のものを日付にする。（暫定）
						
						//echo "<br/>日付：";
						//echo strtotime($date_arr[$d]);
						//echo "<br>";
						//echo strtotime(date('Ymd'));
						//echo "<br>";
						if (strtotime($date_arr[$d]) <= strtotime(date('Ymd'))) {
							//echo $date_arr[$d];
							//echo substr($date_arr[0],0,4);
							//echo "<br>";
							//echo date('Y',strtotime("+1 year"));
							//echo substr($date_arr[$d],0,4);echo "<br>";
							//echo date('Y',strtotime("+3 year"));echo "<br>";
							While (substr($date_arr[$d],0,4) < date('Y',strtotime("+2 year")) && substr($date_arr[$d],0,4) >= date('Y',strtotime("-2 year"))) {
								$got_date = $date_arr[$d];
								//echo "<br>";
								break;
								if ($d = count($date_arr) - 1) {
									break;
								}
							}
						}
						//echo "<br/>";
						//echo "\r\n";
					}
				} else {
					
					//for ($dtcnt = 0;$dtcnt < count($got_date_top);$dtcnt++) {
					//	$got_date = $got_date_top[$dtcnt];
					//}
					$got_date = $got_date_top[0];
					
				}
				
				//電話番号
				$checked_tel_num = 0;
				$comp_name = $company_name_arr[0];
				//配列内の重複要素を削除する。
				$tel_arr = array_unique($tel_arr);
				if (count($tel_arr) > 0) {
					$tel_arr_num = count($tel_arr);
				} else {
					$tel_arr_num = 1;
				}
				
				//var_dump($tel_arr_num);
				for ($t = 0;$t < $tel_arr_num;$t++){
					//echo "tel選択：";
					if (substr($tel_arr[$t],0,1) == 0) {
					//echo "<br>電話番号：";
						$tel_num = $tel_arr[$t];
					//echo "<br>";
					}
//exit;
////////電話番号会社名検索▼なくてもいい。ない方が速い。
					if ($exist_flag == 0) {
						//企業データ取得▼
						//電話番号が無かった場合、検索キーワードテーブルから電話番号で存在確認
						$tel_num = str_replace("-","",$tel_num);
						if ($tel_num == "0000000000") {$tel_num="";$tel_arr[$t] = "";}
						$tel_check = strlen($tel_num);
						//電話番号が10桁か11桁の場合はキーワード検索テーブルで電話番号検索、名前検索の順で検索し、
						//なければ全国企業データベースで電話番号検索し、それでも無ければネット経由で検索する。
						if (($tel_check == 10 || $tel_check == 11) && substr($tel_num,0,2) != "00") {
							//echo "電話キーワード検索<br/>";
							//検索キーワードテーブルからキーワードで検索
							$flag = "search_keywords4";
							$keywords = $tel_num;
							//echo "<br/>";
							$search_result_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$keywords,$aj_words2);
							//var_dump($search_result_arr);
							if (count($search_result_arr) == 0) {
								//電話番号検索でキーワード検索テーブルから企業データが見つからなかった場合
								//echo "名前で検索<br/>\r\n";
								if (isset($comp_name) && $comp_name != "") {
									$flag = "search_keywords2";
									$keywords = $comp_name;
									$search_result_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$keywords,$aj_words2);
									//var_dump($searched_company_data_arr);
									if (isset($search_result_arr[0]['company_name'])) {
										//echo "名前あり<br/>\r\n";
										$exist_flag = 1;
										$checked_tel_num = $tel_num;
									}
								}

								if (count($search_result_arr) == 0) {
									//echo "名前確認<br/>\r\n";
									//全国企業データベースで電話番号検索　重いのでコメントアウト20170610hmsk
									
									$flag = "company_search_data_table_from_tel";
									$words = "";
									if (isset($tel_num) && $tel_num != "") {
										//echo "電話番号検索　全国版";
										$tel_top4 = substr($tel_num,0,4);
										if ($tel_top4 == "0120"||$tel_top4 == "0800"||$tel_top4 == "0570") {
											
										} else {
											$area = substr($tel_num,1,1);
											$words = $tel_num;
											$conditions = "";
											SWITCH ($area) {
												CASE 1:$conditions = "`COMPANY_SEARCH_TABLE1`";break;
												CASE 2:$conditions = "`COMPANY_SEARCH_TABLE2`";break;
												CASE 3:$conditions = "`COMPANY_SEARCH_TABLE3`";break;
												CASE 4:$conditions = "`COMPANY_SEARCH_TABLE4`";break;
												CASE 5:$conditions = "`COMPANY_SEARCH_TABLE5`";break;
												CASE 6:$conditions = "`COMPANY_SEARCH_TABLE6`";break;
												CASE 7:$conditions = "`COMPANY_SEARCH_TABLE7`";break;
												CASE 8:$conditions = "`COMPANY_SEARCH_TABLE8`";break;
												CASE 9:$conditions = "`COMPANY_SEARCH_TABLE9`";break;
												DEFAULT:$conditions = "`COMPANY_SEARCH_TABLE`";break;
											}
											$searched_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$comp_name,$words,$conditions);
											//var_dump($searched_company_data_arr[0]["company_name"]);
											if (count($searched_company_data_arr) > 0) {
												//企業が存在したのでフラグを立てる
												$exist_flag = 1;
												$checked_tel_num = $tel_num;
												//	var_dump($searched_company_data_arr);
												//exit;
											}
											//var_dump($searched_company_data_arr[0]["company_name"]);
										}
									}
								}
							}
						
						
							if (isset($search_result_arr[0]['company_name']) && $search_result_arr[0]["company_name"] != "") {
								//電話番号検索でキーワード検索テーブルから企業データが見つかった場合
								//echo "TELキーあり<br/>\r\n";
								$searched_company_name = $got_company_name = $comp_name = $search_result_arr[0]["company_name"];
								$product_name = $search_result_arr[0]["remarks1"];
								$industry_type_s = $search_result_arr[0]["remarks2"];
								$telkey_flag = 1;
								//echo "<br>";
							} else if (isset($searched_company_data_arr[0]["company_name"]) && $searched_company_data_arr[0]["company_name"] != "") {
								//全国企業データベース検索で企業データ取得できた場合
								//echo "名前あり下<br/>\r\n";
								$got_company_name = $searched_company_name = $searched_company_data_arr[0]['company_name'];
								//	echo "<br/>\r\n";
								$claimant_id = sprintf('%012d',$searched_company_data_arr[0]["company_id"]);
								//echo "<br/>\r\n";
								$industry_type_s = $searched_company_data_arr[0]["industry_type_s"];
								//echo "<br/>\r\n";
								$searched_company_address1 = $searched_company_data_arr[0]['address1'];
								//echo "<br/>\r\n";
								$searched_company_address2 = $searched_company_data_arr[0]['address2'];
								//echo "<br/>\r\n";
								$searched_company_tel_num = $searched_company_data_arr[0]['tel_num'];
								
								
							} else {
								$tel_check = strlen($tel_num);
								if ($tel_check == 10 || $tel_check == 11) {
									$options = array(
									  'http' => array(
									    'method' => 'GET',
									    'header' => 'User-Agent: DoCoMo/2.0 P903i(c100;TB;W24H12)',
									  ),
									);
									$context = stream_context_create($options);

									//echo "名前なし <br/>WEB検索<br/>\r\n";
									$match_nodata = array();
									$match_tel= array();
									$match_ind = array();
									
									//電話番号でも取得できなかった場合、マピオンで電話番号検索をして、情報があれば会社を作って渡す。
									/*
									//マピオン
									$tel_test = preg_replace('/[^0-9]/', '', $tel_num);
									$tel_get_file = file_get_contents("http://www.mapion.co.jp/phonebook/M01010/13116/G".$tel_test."-001/");
									//var_dump($tel_get_file);
									$pattern_tel_search = '/ content="(.+?),地図/imu';
									
									preg_match($pattern_tel_search, $tel_get_file, $match_tel3);
									//var_dump( $match_tel3);
									$com_res_arr = explode(",",$match_tel3[1]);
									echo  $com_name_data = $com_res_arr[0];//社名
									//echo "<br/>";
									echo $match_ind[1] = $com_res_arr[2];//業種

									echo "<br>電話帳ナビ";
									$tel_get_file_nav = file_get_contents("https://www.telnavi.jp/phone/".str_replace("-","",$tel_num));
									//var_dump($tel_get_file_nav);
									$pattern_tel_search = '/data-hashtags="電話帳ナビ" data-text=\"(.+?)\">/imu';
									$pattern_ind_type = '/<td class="longtext"><a href=".+?">(.+?)<\/a>/imu';
									preg_match($pattern_tel_search, $tel_get_file_nav, $match_tel2);
									preg_match($pattern_ind_type, $tel_get_file_nav, $match_ind2);
									
									$com_name_data = explode(" ",$match_tel2[1]);//社名
									echo $com_name_data = $match_tel2[1];
									//echo "<br/>";
									echo $match_ind[1] = $match_ind2[1];//業種
									*/
									
							/*
									$options = array(
									  'http' => array(
									    'method' => 'GET',
									    'header' => 'User-Agent: DoCoMo/2.0 P903i(c100;TB;W24H12)',
									  ),
									);
									$context = stream_context_create($options);
							*/
		//							if ($new_company_name == "") {
										
										//echo "<br>ヤフーロコ";echo "http://search.loco.yahoo.co.jp/search?p=".$tel_num;
										$tel_get_file = file_get_contents("http://search.loco.yahoo.co.jp/search?p=".$tel_num, false, $context);
										//var_dump($tel_get_file);
										$pattern_nodata = '/施設は見つかりませんでした。/';
										preg_match($pattern_nodata, $tel_get_file, $match_nodata);
										//echo "test";
										if (count($match_nodata) == 0) {

											$pattern_tel_search = '/class="ic"><\/span>(.+?)<\/a>/imu';
											$pattern_ind_type = '/p class="genre">(.*)<\/p>/imu';
											preg_match($pattern_tel_search, $tel_get_file, $match_tel);
											preg_match($pattern_ind_type, $tel_get_file, $match_ind);
											//echo "<br>tel_test▼<br>";

											//echo $tel_num;
											//echo "<br/>";
											//echo $match_ind[1];//業種
											//echo "<br>tel_test▲<br>";
											
											if ($match_ind[1] != "") {
												$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_ind[1]);
											}
											
											if (count($match_tel[1]) > 0) {
												$com_name_data_arr = explode(" ",$match_tel[1]);//社名
												$new_company_name = str_replace(array("&nbsp;","&amp;"),array("","＆"),$com_name_data_arr[0]);
												$industry_type_s = $match_ind[1];//業種
												//var_dump($new_company_name);
												//var_dump($industry_type_s);
												//var_dump($com_name_data_arr);
												//exit;
											}
											if($new_company_name != "") {
												$searched_company_name = $got_company_name = $new_company_name;
											}
										} else {
											$nodata_flag = 1;
											//exit;
										}
		//							}
/*
									if ($nodata_flag == 1) {
										if ($new_company_name == "") {
											$telephone_num = str_replace("-","_",$tel_num);
											//echo "<br>jpnumber";echo "http://www.jpnumber.com/numberinfo_".$telephone_num.".html";
											$tel_get_file = file_get_contents("http://www.jpnumber.com/numberinfo_".$telephone_num.".html", false, $context);
											//var_dump($tel_get_file);
											$pattern_tel_search = '/td class="autonewline">(.+?)<\/td>/imu';
											//$pattern_ind_type = '/td class="autonewline">(.+?)<\/td>/imu';
											preg_match_all($pattern_tel_search, $tel_get_file, $match_tel);
											//preg_match_all($pattern_ind_type, $tel_get_file, $match_ind);
											//echo "<br>tel_test▼<br>";
											//echo $match_tel[1][0];
											//echo $match_tel[1][1];
											if ($match_ind[1] != "") {
												$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_tel[1][1]);
											}
											if (count($match_tel[1][0]) > 0) {
												//var_dump($com_name_data_arr);
												$new_company_name = str_replace(array("&nbsp;","&amp;"),array("","＆"),$match_tel[1][0]);//社名
												$industry_type_s = $match_tel[1][1];//業種
											}
											if($new_company_name != "") {
												$searched_company_name = $got_company_name = $new_company_name;
											}
										}
									}
*/
/*

									//電話帳ナビテスト
									if ($new_company_name == "") {
										//echo "<br>電話帳ナビ";echo "https://www.telnavi.jp/phone/".str_replace("-","",$tel_num);
										$tel_get_file_nav = file_get_contents("https://www.telnavi.jp/phone/".str_replace("-","",$tel_num), false, $context);
										//var_dump($tel_get_file_nav);
										$pattern_tel_search = '/data-hashtags="電話帳ナビ" data-text=\"(.+?)\">/imu';
										$pattern_ind_type = '/<td class="longtext"><a href=".+?">(.+?)<\/a>/imu';
										preg_match($pattern_tel_search, $tel_get_file_nav, $match_tel2);
										preg_match($pattern_ind_type, $tel_get_file_nav, $match_ind2);
										//echo "<br>tel_test▼<br>";
										//var_dump($match_tel);
										//echo "<br/>";
										//echo $match_ind[1];//業種
										//echo "<br>tel_test▲<br>";
										if ($match_ind[1] != "") {
											$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_ind[1]);
										}
										$com_name_data_arr = explode(" ",$match_tel[1]);//社名
										$new_company_name = $com_name_data_arr[0];
										//$industry_type_s = $match_ind[1];//業種
										if($new_company_name != "") {
											$searched_company_name = $got_company_name = $new_company_name;
										}
										//電話帳ナビテスト
									}

									if ($new_company_name == "") {
										$telephone_num = str_replace("-","",$tel_num);
										//echo "<br>japhones";echo "http://www.japhones.com/phones/".$telephone_num.".html";
										$tel_get_file = file_get_contents("http://www.japhones.com/phones/".$telephone_num.".html", false, $context);
										//var_dump($tel_get_file);
										$pattern_tel_search = '/<td>(.+?)<a href="#phonedetailsubmit"/imu';
										//$pattern_ind_type = '/td class="tit">業&nbsp;&nbsp;種<\/td>[\n\s\t]{0,10}<td>(.+?)<\/td>"/imu';
										//$pattern_ind_type = '/td class="tit">(.+?)<\/td>"/imu';
										preg_match_all($pattern_tel_search, $tel_get_file, $match_tel);
										//preg_match_all($pattern_ind_type, $tel_get_file, $match_ind);
										//echo "<br>tel_test▼<br>";
										//var_dump($match_tel);
										//var_dump($match_ind);
										//echo $match_tel[0][0];
										//echo $match_tel[0][1];
										if ($match_ind[1] != "") {
											$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_tel[0][1]);
										}
										//var_dump($com_name_data_arr);
										$new_company_name = $match_tel[1][0];//社名
										//$industry_type_s = $match_tel[1][1];//業種
										if($new_company_name != "") {
											$searched_company_name = $got_company_name = $new_company_name;
										}
									}
	*/

									//会社の情報が存在したら新しい会社のユーザーを作る。
									if ($new_company_name != NULL || $new_company_name != "") {
										//echo "請求側の企業が存在しない時の処理<br/>\r\n";
										//仮メールアドレス生成
										$mail_address = md5(random(12)).rand(4)."@user.cloudinvoice.co.jp";
										$user_con = new user_control();
										//Emailアドレスの存在を確認する。		
										//$login_check_arr = $user_con -> user_select_email($pdo,$mail_address);

										if ($login_check_arr != NULL) {
											//$login_arr = $user_con -> user_select($pdo,$mail_address);

											//$passwordmaker = random(12);
											//$passwordmaker = uniqid();
											$passwordmaker = "1234abcd";
											$password = $login_arr['password'] = md5($passwordmaker);
											//$login_arr['nick_name'] = $user_name;
											//$user_con -> user_update($pdo,$login_arr);
											$update_data = " `password` = '".$password."' ";
											//$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

										}

										//$user_arr = $user_con -> user_select($pdo,$mail_address);

										//ユーザー登録
										$user_arr['nick_name']   = $new_company_name;
										$user_arr['mail_address'] = $mail_address;
										$user_arr['tel_num'] = $tel_num;
										$user_arr['non_regular_flag'] = 2;//$non_regular_flag;
										//共通コードの生成と取得
										if (!$user_arr['user_id']){
											$user_num = $user_con -> user_get_new_num($pdo);
											$user_arr['user_id'] = $user_num;
										}
										//$result = $user_con -> user_insert($pdo,$user_arr);

										$area = substr($tel_num,1,1);
										$words = $tel_num;
										$conditions = "";
										SWITCH ($area) {
											CASE 1:$conditions = "`COMPANY_SEARCH_TABLE1`";break;
											CASE 2:$conditions = "`COMPANY_SEARCH_TABLE2`";break;
											CASE 3:$conditions = "`COMPANY_SEARCH_TABLE3`";break;
											CASE 4:$conditions = "`COMPANY_SEARCH_TABLE4`";break;
											CASE 5:$conditions = "`COMPANY_SEARCH_TABLE5`";break;
											CASE 6:$conditions = "`COMPANY_SEARCH_TABLE6`";break;
											CASE 7:$conditions = "`COMPANY_SEARCH_TABLE7`";break;
											CASE 8:$conditions = "`COMPANY_SEARCH_TABLE8`";break;
											CASE 9:$conditions = "`COMPANY_SEARCH_TABLE9`";break;
											DEFAULT:$conditions = "`COMPANY_SEARCH_TABLE`";break;
										}
										
										//企業データ登録▼
										$claimant_id = "".sprintf('%012d',$user_arr['user_id']);
										$sql = 'SELECT * FROM `'.$conditions.'` WHERE `company_id` = '.$user_arr['user_id'].' AND `company_name` = "'.$user_arr['nick_name'].'" ;';
										$company_arr = $company_con -> company_sql($pdo,$user_arr['user_id'],$sql);
										$com_exists_flag = $company_arr['chk']['select_count'];
										if ($com_exists_flag == 0) {
										$sql = '
											INSERT INTO `'.$conditions.'` ( 
												`company_id`, `company_name`, `company_name_reading`, `zip_code`, `prefecture`, `address1`, `address_full`,`email`, `tel_num`, `non_regular_flag`, `industry_type_s`
											) VALUES ( 
												'.$user_arr['user_id'].', "'.$user_arr['nick_name'].'","'.$company_name_reading.'", "'.$zip_code.'", "'.$prefecture.'", "'.$address1.'", "'.$address_full.'", "'.$user_arr['mail_address'].'","'.$user_arr['tel_num'].'","'.$user_arr['non_regular_flag'].'","'.$industry_type_s.'"
											)';
										}
										$company_arr = $company_con -> company_sql($pdo,$claimant_id,$sql);
										//企業データ登録▲

										//▼自動仕訳用企業DB登録▼
										//企業品目テーブルと企業用レコードテーブルを登録
										/*
										//$table_name = "".sprintf("%012d", $claimant_company_id);
										//$table_name = "CUSTOMER_JOURNALIZE_TABLE";
										try {
											$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
											$company_num++;
										} catch (PDOException $e) {
										    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
										    $_SESSION['error_msg'] = $e -> getMessage();
										}
										*/
										//▲自動仕訳用企業DB登録▲
										$exist_flag = 1;
										$checked_tel_num = $tel_num;
									}
									//echo "名前照合\r\n";
									//企業データ取得▲
								}
							}
						} else {
							$tel_arr[$t] = "";
						}//電話番号10桁か11桁
					}
					//企業データ取得▲
////////電話番号会社名検索/////▲なくてもいい
				}//for ($t)の終わり
			}//if($img_file_arr[$m])文の終わり　//for文の終わり　間違い

			//商品名を取り出す
			if (count($product_name_arr) > 0) {
				for ($n = 0;$n < count($product_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$product .= $sp.$product_name_arr[$n];
				}
				//$product_name = $searched_company_name." ".$product;
				$product_name = $product;
			} else if ($product_name != "") {
				$product_name = $product_name;
			} else {
				$product_name = $searched_company_name;
			}

			//会社名を取り出す
			if ($searched_company_name != "") {
				$company_names = $searched_company_name;
			} else if (count($company_name_arr) > 0) {
				for ($n = 0;$n < count($company_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$company_name_one .= $sp.$company_name_arr[$n];
				}
				//$company_names = $searched_company_name." ".$product;
				$company_names = $company_name_one;
			} else {
					$company_names = $comp_name;
			}

			//var_dump($name_arr);
			if ($fixed_name != "") {
				$name = $fixed_name;
			} else {
				
				//▼リストで名前を照合▼
				$fp	=	fopen(dirname(__FILE__).'/../files/common/name.csv','r');
				$name_check = "";
				
				if	($fp){
					if	(flock($fp,	LOCK_SH)){
						while	(!feof($fp))	{
							$buffer = fgets($fp);
							$buffer	= str_replace(array(" ","\r","\n","\t","\r\n"),"",$buffer);
							$name_pattern = "/".$buffer."/";
							
							for ($nan = 0; $nan < 5; $nan++) {
								$nan_num = preg_match($name_pattern,$name_arr[$nan],$nanatch_name);
								
								if ($nan_num >0) {
									$name_check = $name_arr[$nan];
									$relationship = $relation_arr[$nan];
								}
							}
						}
						flock($fp,LOCK_UN);
					}else{
						print('ファイルロックに失敗しました');
					}
				}

				$flag	=	fclose($fp);
				//▲リストで名前を照合▲
				if ($name_check != "") {
					$name = $name_check; 
				} else {
					$name = $name_arr[0];
					$relationship = $relation_arr[0];
				}
			}
			if ($searched_company_address1 != "" || $searched_company_address1 != NULL) {
				$address1 = $searched_company_address1;
			} else {
				$address1 = $address_arr[0];
			}
			if ($searched_company_address2 != "" || $searched_company_address2 != NULL) {
				$address2 = $searched_company_address2;
			} else {
				$address2 = $address2_arr[0];
			}

			
//echo $company_names;
//echo "<br>";
//echo $product_name;
//exit;
			//日付が未取得の場合の処理
			if ($got_date == "" || date('Ymd',strtotime($got_date)) == "19700101") {
				if ($last_receipt_date != "") {
					$got_date = date('Ym',strtotime($last_receipt_date))."01";
				} else {
					$got_date = date("Y",time())."0101";
				}
			} else {
				//日付が取得できていた場合に前回のレシートの日付として保存する。
				$last_receipt_date = $got_date;
			}
			//電話番号が複数取得できた場合に候補1,2,3がかぶらないようにする処理
			for ($t_cnt = 0;$t_cnt < 3;$t_cnt++) {
				$tel_check = strlen($tel_arr[$t_cnt]);
				if (($tel_check != 10 && $tel_check != 11) || substr($tel_num,0,2) == "00") {
					$tel_arr[$t_cnt] = "";
				}
			}
			if ($tel_num == str_replace("-","",$tel_arr[0])) { $tel_num2 = str_replace("-","",$tel_arr[1]);$tel_num3 = str_replace("-","",$tel_arr[2]);} else if ($tel_num == str_replace("-","",$tel_arr[1])) { $tel_num2 = str_replace("-","",$tel_arr[0]);$tel_num3 = str_replace("-","",$tel_arr[2]);} else { $tel_num2 = str_replace("-","",$tel_arr[0]);$tel_num3 = str_replace("-","",$tel_arr[1]);}
			//if ($searched_company_name == "" || $searched_company_name == "郵便局" ) {$searched_company_name = $company_names;}
			if ($company_names == "") {$company_names = $searched_company_name;}
			if ($industry_type_s == "預入") {$industry_type_s = "";}
			
			//業種に下記の配列内のものがあった場合、商品名を変換する。
			$ind_type_check_arr = array("ケーキ","カフェ","ファミレス","ファストフード","居酒屋",".*食堂.*","宴会場","ダイニングバー","レストラン","寿司","持ち帰り専門",".*料理","ラーメン","うどん","丼もの","弁当","ステーキ","ピザ","ドリア","ハンバーグ","ホテル","水族館","映画館","美術館","エンタメ");
			for ($z = 0;$z < count($ind_type_check_arr);$z++) {
				$pattern_comp_end = "/".$ind_type_check_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $industry_type_s, $match);
				if ($match_num > 0) {
					if ($match[0] == "ホテル"||$match[0] == "旅館") {
						$product_name = "宿泊代";
					}else if ($match[0] == "居酒屋"||$match[0] == "ダイニングバー") {
						$product_name = "居酒屋";
					} else if($match[0] =="持ち帰り専門") {
						$product_name = "持ち帰り食品";
					} else if ($ind_type_check_arr[$z] == "弁当"){
							$product_name = "弁当代";
					} else if ($ind_type_check_arr[$z] == "水族館"||$ind_type_check_arr[$z] == "映画館"||$ind_type_check_arr[$z] == "美術館"||$ind_type_check_arr[$z] == "エンタメ"){
							$product_name = "チケット代";
					} else {
						$product_name = "飲食代";
					}
					break;
				}
			}

			//鉄道会社
			$cname_conv_arr = array("東海旅客","JR東日本","jr東日本","京成電鉄","東急電鉄","東武鉄道","西武鉄道","東京地下鉄","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","東京地下鉄","東日本旅客","近畿日本鉄道","地下鉄","鉄道");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $company_names, $match);
				if ($match_num > 0) {
					//$company_names = "鉄道会社";
					$industry_type_s = "交通機関";
					break;
				}
			}

			//コンビニ等店舗ぬき
			$cname_conv_arr = array("セブンイレブン","ファミリーマート","ニューデイズ","スリーエフ","ローソン","NewDays","ローソン","LAWSON","サンクス","ポプラ","ジェーソン","マルエイ","マルエツ","ミニストップ","くすりの福太郎","ダイソー","DAISO","ファッションセンターしまむら","UT","アーバンリサーチ","SHIPS","シップス","ヴィクトリアゴルフ","ジーユー","ユニクロ","ドン・キホーテ","エネオス","ロフト","日本郵便株式会社","カメラのキタムラ");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $company_names, $match);
				if ($match_num > 0) {
					$company_names = $cname_conv_arr[$z];
					if ($product_name == "コンビニ") {$product_name = "";$industry_type_s = "";break;}
				}
			}
			
			//タクシー
			$cname_conv_arr = array("基本運賃","乗車料金","タクシー");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $product_name.$industry_type_s, $match);
				if ($match_num > 0) {
				//$company_names = "タクシー";
				$product_name = "タクシー代";
				$industry_type_s = "交通機関";
				break;
				}
			}
			
			//駐車場
			$cname_conv_arr = array("駐車料金","パーキング");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $product_name, $match);
				if ($match_num > 0) {
					//$company_names = "駐車場";
					$product_name = "駐車料金";
					$industry_type_s = "駐車場";
					break;
				}
			}

			//書店
			$pattern_comp_end = "/書店/";
			$match_num = preg_match($pattern_comp_end, $industry_type_s.$company_names, $match);
			if ($match_num > 0) {$product_name = "書籍";}

			//電力
			$pattern_comp_end = "/電力/";
			$match_num = preg_match($pattern_comp_end, $company_names, $match);
			if ($match_num > 0) {$product_name = "電気料金";}

			//新聞
			$pattern_comp_end = "/新聞/";
			$match_num = preg_match($pattern_comp_end, $company_names, $match);
			if ($match_num > 0) {$product_name = "購読料";}
//echo $counter;
//if ($counter == 4) {exit;}

			$company_names = str_replace(array("有限会社","株式会社","（株）","（有）","未登録 &nbsp;","&nbsp;","金銭出納員","出納員"),"",$company_names);
			
			//$product_name = $industry_type_s;

			//商品名検索の結果が無かった場合、業種を商品名に入れる
			//if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "第一種定型" || $product_name == "簡易書留") {$product_name = str_replace(array("第一種定型外","簡易書留"),"郵便",$product_name);} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
			//if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
			
			//代金引換用の処理//間違えやすいものにアラートを出すため
			if ($company_names == "ヤマトフィナンシャル") {$company_names = "代金引換";}
			if ($company_names == "売捌所" ||$company_names == "売さばき") {$company_names = "法務局";}
			if ($product_name == "プリカ") {$product_name = "プリペイドカード";}
			//if ($product_name == "代金引換") {$company_names = "代金引換";}
			
			//外部から取得した時、たまに&nbsp;が入ってしまうのを消す処理
			$industry_type_s = str_replace(array("&nbsp;"),"",$industry_type_s);
			$product_name = str_replace(array("&nbsp;","として"),"",$product_name);

			if ($got_date != "") {
				$receipt_date = date('Y/m/d',strtotime($got_date));
			} else {
				$receipt_date = "";
			}
			//テキスト化ファイルの末尾に名前,続柄,病院名,病院の住所,支払医療費,受取保険金等をカンマ区切りで追加する。
//echo			$AddWords = "\r\n".$name.",".$relationship.",".$company_names.",".$address1.$address2.",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$product_name.",".$industry_type_s;
			$AddWords = $name.",".$relationship.",".$company_names.",".$address1.$address2.",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3)."\n";
			$AllWords .= $name.",".$relationship.",".$company_names.",".$address1.$address2.",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3)."\n";
			//echo "<br>";
			file_put_contents($FileText, $AddWords, FILE_APPEND);
			$outcode = "SHIFT-JIS";
			$incode = "UTF-8";
			$nl = "\n";
			convertCode($FileText , $incode, $FileText , $outcode, $nl);

			//$outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);
			if ($tel_num != "" && $company_names != "" && $telkey_flag == 0) {
				if ($checked_tel_num != 0) {$tel_num = $checked_tel_num;}
				//電話番号が存在する場合、桁数を調べて10桁か11桁なら登録する。
				$tel_num = str_replace(array("-"),"",$tel_num);
				$tel_check = strlen($tel_num);
				if (($tel_check == 10 || $tel_check == 11) && substr($tel_num,0,2) != "00") {
					$flag = "search_keywords4";
					$res_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$tel_num,$company_names,$remarks1,$remarks2);
					//var_dump($res_arr);
					//電話番号が存在したらアップデートする。
					if (count($res_arr) > 0) {
						$flag = "update_keyword";
						$keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$tel_num,$company_names,$product_name,$industry_type_s);
					} else {
						//同じ電話番号が存在しない時、キーワード登録
						$flag = "insert_keyword";
						$keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$tel_num,$company_names,$product_name,$industry_type_s);
					}
					//echo "<br>";
				}
			}//if ($tel_num != "" && $company_names != "")文の終わり
			
			// ファイルを出力する
			//readfile($FileText);
			//exit;
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
		//取得したTEXTファイルを削除する。
//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
		$counter++;
	}//foreach文の終わり//for文に変更
}
//全ファイル記録用ファイルに追記
$FileText2 = $txt_dir."/convert_data_all.csv";
$outcode = "SHIFT-JIS";
$incode = "UTF-8";
$nl = "\n";
file_put_contents($FileText2, $AllWords, FILE_APPEND);
convertCode($FileText2 , $incode, $FileText2 , $outcode, $nl);

//echo "カウント";
//echo $counter;
if ($counter > 0) {
	$counter = $counter;
	$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをアップロードしました。";
}
if($error_num > 0) {
	$_SESSION['up_info_msg'] = $error_num."件のファイルをアップロードできませんでした。";
}
//echo $counter;
$file_list_arr = getFileList($path);
//var_dump($file_list_arr);

//テスト用。仮にここで止める。
//exit;

if(count($file_list_arr) > 0) {
	header("Location:./download_imgs_to_text_all?path=".$upload_dir_path."&type=all",true);
	echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./imgs_to_text_upload_all\"'>";
	exit;
} else {
	//echo "no DL<br>";
}

header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all'</script>";
echo "</html>";

echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./imgs_to_text_upload_all\"'>";


//header("Location:https://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all",true);
//exit;


/**
* テキストファイルの文字コードを変換し保存する
* @param string $infname  入力ファイル名
* @param string $incode   入力ファイルの文字コード
* @param string $outfname 出力ファイル名
* @param string $outcode  出力ファイルの文字コード
* @param string $nl       出力ファイルの改行コード
* @return string メッセージ
*/
function convertCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'wb');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>