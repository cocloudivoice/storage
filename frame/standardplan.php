<?php session_start(); ?>
<?php require("fheader.php");?>

<body>


	<section id="m-2-box">
		<h2>
		スタンダードプランに変更
		</h2>
		
		
		<div id="close">
			<a  onclick="CloseFrame(13)">
				<img src="/images/cross.png">
			</a>
		</div>

		<p>
			スタンダードプランは月額4,320円（税込）となります。<br>
			郵送代行が1通180円、データ保管期間が無制限、お客様専用カスタマーのバックアップサービスが受けられます。<br>
			丸投げオプションで200仕訳まで無料、超過分は1仕訳30円になります。<br>
			スタンダードプランに加入されますか？
			
		</p>
		

		
		<div id="button">
			<button onclick="submit(13);">申込む</button>
		
			<button onclick="CloseFrame(13)">閉じる</button>
		</div>
		
	</section>


</body>


