<?php
session_start(); 

if (isset($_SESSION['user_id'])){
	$company_id = $_SESSION['user_id'];
} else {
//	header("Location:../logout.php");
//	exit();
}

?>

<?php require("fheader.php");?>
<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__).'/../../db_con/usercontrol.class.php');
require_once(dirname(__FILE__).'/../../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();
if (isset($_REQUEST['company_name'])) {

//ランダムでhistorical_codeを作成
$historical_code = md5('ci'.rand(10).date('Y-m-d h:s:i'));
	$sql = "SELECT max(id) as `id_num` FROM `COMPANY_TABLE`;";
	$id_num_arr = $company_con -> company_sql($pdo,$company_id,$sql);
	$new_id = $id_num_arr[0]["id_num"] + 1;
	$sql = "INSERT INTO `COMPANY_TABLE` SELECT '".$new_id."' AS `id`, '1' AS `del_flag`, `historical`, `historical_code`, `company_id`, `company_name`, `company_name_reading`, `non_regular_flag`, `plan_status`, `segment`, `industry_type`, `industry_type_s`, `zip_code`, `prefecture`, `address1`, `address2`, `address_full`, `tel_num`, `fax_num`, `url`, `email`, `section`, `clerk`, `account_name`, `bank_id`, `blanch_id`, `account_devision`, `account_num`, `bank_name1`, `blanch_name1`, `account_devision1`, `account_num1`, `bank_name2`, `blanch_name2`, `account_devision2`, `account_num2`, `bank_name3`, `blanch_name3`, `account_devision3`, `account_num3`, `bank_name4`, `blanch_name4`, `account_devision4`, `account_num4`, `bank_name5`, `blanch_name5`, `account_devision5`, `account_num5`, `account_info`, `stamp_image`, `logotype`, `remarks1`, `remarks2`, `payment_id`, `recursion_id`, `template_type`, `original_template1`, `original_template2`, `original_template3`, `original_template4`, `original_template5`, `update_date`
	FROM `COMPANY_TABLE` WHERE `id` = '".$_REQUEST['id']."';";
	$company_con -> company_sql($pdo,$company_id,$sql);

	if ($_REQUEST['change_place'] == 1) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`segment`=".$_REQUEST['segment'].",`industry_type`='".$_REQUEST['LTitle1'] ."',`industry_type_s`='".$_REQUEST['LTitle2'] ."',`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`address_full`='".$_REQUEST['prefecture'].$_REQUEST['address1'].$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',
		`fax_num`='".$_REQUEST['fax_num']."',`url`='".$_REQUEST['url']."',`historical` = '".($_REQUEST['historical'] + 1)."' ,`historical_code` = '".$historical_code."'
		 WHERE `id` = '".$_REQUEST['id']."'";
	} else if ($_REQUEST['change_place'] == 2) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`email`='".$_REQUEST['email']."',`section`='".$_REQUEST['section']."',
		`clerk`='".$_REQUEST['clerk']."',`bank_id`=".$_REQUEST['bank_id'].",
		`blanch_id`=".$_REQUEST['blanch_id'].",`account_devision`=".$_REQUEST['account_devision'].",
		`account_num`=".$_REQUEST['account_num'].",`account_info`='".$_REQUEST['account_info']."',
		`remarks1`='".$_REQUEST['remarks1']."',`remarks2`='".$_REQUEST['remarks2']."',`historical` = '".($_REQUEST['historical'] + 1)."' ,`historical_code` = '".$historical_code."'
		 WHERE `id` = '".$_REQUEST['id']."'";
	}
	$company_con -> company_sql($pdo,$company_id,$sql);
	//元データを過去のデータに変更する（del_flag = 1にする）
	//$sql = "INSERT INTO `ADDRESSEE_TABLE`  `del_flag` = 1 WHERE company_id = ".$company_id." AND `historical` = '".$_REQUEST['historical']."'";
	
	echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
}

//設定変更の処理▲

//設定内容の表示データ取得
$flag = "company_data";
$company_arr  = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);

if (isset($company_arr[0]['industry_type'])) {
	$L1 = $company_arr[0]['industry_type'];
	$L2 = $company_arr[0]['industry_type_s'];
}
?>

<script>


var urlList1 = new Array();
var MaxList2 = new Array();
var urlList2 = new Array();
	urlList2[0] = new Array();
	urlList2[1] = new Array();
	urlList2[2] = new Array();
	urlList2[3] = new Array();
	urlList2[4] = new Array();
	urlList2[5] = new Array();
	urlList2[6] = new Array();
	urlList2[7] = new Array();
	urlList2[8] = new Array();
	urlList2[9] = new Array();
	urlList2[10] = new Array();
	urlList2[11] = new Array();
	urlList2[12] = new Array();
	urlList2[13] = new Array();
	urlList2[14] = new Array();
	urlList2[15] = new Array();
	urlList2[16] = new Array();
	urlList2[17] = new Array();
	urlList2[18] = new Array();
	urlList2[19] = new Array();
	urlList2[20] = new Array();
	urlList2[21] = new Array();
	urlList2[22] = new Array();

var MaxList1 = 22;


urlList1[0] = "IT/通信";
MaxList2[0] = 5;
urlList2[0][0] = "システムインテグレータ";
urlList2[0][1] = "ベンダ/ASP";
urlList2[0][2] = "コンサルティングファーム";
urlList2[0][3] = "ITアウトソーシング";
urlList2[0][4] = "通信キャリア/ISP";

urlList1[1] = "メーカー(機械/電気)";
MaxList2[1] = 12;
urlList2[1][0] = "総合電機メーカー";
urlList2[1][1] = "産業用装置";
urlList2[1][2] = "重工業/造船";
urlList2[1][3] = "機械部品/金型";
urlList2[1][4] = "家電/モバイル/複写機";
urlList2[1][5] = "電子部品";
urlList2[1][6] = "半導体";
urlList2[1][7] = "光学機器/精密機器";
urlList2[1][8] = "自動車/バイク";
urlList2[1][9] = "建設機械/輸送機器";
urlList2[1][10] = "受託加工業";
urlList2[1][11] = "ゲーム/アミューズメント";

urlList1[2] = "メーカー(化学/食品/その他)";
MaxList2[2] = 13;
urlList2[2][0] = "総合化学";
urlList2[2][1] = "電子材料";
urlList2[2][2] = "非鉄金属/金属製品";
urlList2[2][3] = "ゴム/プラスチック製品";
urlList2[2][4] = "紙/パルプ/木材";
urlList2[2][5] = "ガラス/セラミック/セメント";
urlList2[2][6] = "ナノテク/肥料/農薬";
urlList2[2][7] = "繊維";
urlList2[2][8] = "服飾/皮革/スポーツ用品";
urlList2[2][9] = "日用品/文具/オフィス用品";
urlList2[2][10] = "食品/飲料";
urlList2[2][11] = "化粧品";
urlList2[2][12] = "その他メーカー";

urlList1[3] = "商社";
MaxList2[3] = 20;
urlList2[3][0] = "総合商社";
urlList2[3][1] = "産業用装置";
urlList2[3][2] = "機械部品/金型";
urlList2[3][3] = "家電/電気機器/ネットワーク機器";
urlList2[3][4] = "電子部品/半導体";
urlList2[3][5] = "ゲーム/アミューズメント";
urlList2[3][6] = "光学機器/精密機器";
urlList2[3][7] = "自動車部品/輸送機器";
urlList2[3][8] = "電子材料/ナノテク/バイオ";
urlList2[3][9] = "非鉄金属/金属製品";
urlList2[3][10] = "ゴム/パルプ/ガラス";
urlList2[3][11] = "飼料/肥料/農薬";
urlList2[3][12] = "繊維";
urlList2[3][13] = "服飾/皮革/スポーツ用品";
urlList2[3][14] = "日用品";
urlList2[3][15] = "書籍/雑誌";
urlList2[3][16] = "オフィス用品";
urlList2[3][17] = "建材";
urlList2[3][17] = "エネルギー";
urlList2[3][18] = "食品/飲料/たばこ";
urlList2[3][19] = "その他商社";

urlList1[4] = "医療";
MaxList2[4] = 17;
urlList2[4][0] = "医薬品メーカー";
urlList2[4][1] = "理化学機器/試薬メーカー";
urlList2[4][2] = "医療機器メーカー";
urlList2[4][3] = "医薬品卸";
urlList2[4][4] = "医療機器卸";
urlList2[4][5] = "CRO";
urlList2[4][6] = "SMO";
urlList2[4][7] = "CSO";
urlList2[4][8] = "CMO";
urlList2[4][9] = "病院/クリニック";
urlList2[4][10] = "調剤薬局/ドラッグストア";
urlList2[4][11] = "バイオベンチャー";
urlList2[4][12] = "大学/研究施設";
urlList2[4][13] = "福祉介護関連サービス";
urlList2[4][14] = "医療コンサルティング";
urlList2[4][15] = "医療広告代理店/マーケティング";
urlList2[4][16] = "その他医療関連";

urlList1[5] = "金融";
MaxList2[5] = 23;
urlList2[5][0] = "都市銀行";
urlList2[5][1] = "地方銀行";
urlList2[5][2] = "信託銀行";
urlList2[5][3] = "信用金庫組合";
urlList2[5][4] = "証券会社";
urlList2[5][5] = "生命保険";
urlList2[5][6] = "損害保険";
urlList2[5][7] = "保険代理店";
urlList2[5][8] = "クレジット/信販";
urlList2[5][9] = "消費者金融";
urlList2[5][10] = "商工ローン";
urlList2[5][11] = "住宅ローン会社 ";
urlList2[5][12] = "証券取引所";
urlList2[5][13] = "投信/投資顧問";
urlList2[5][14] = "ベンチャーキャピタル";
urlList2[5][15] = "債権回収会社";
urlList2[5][16] = "商品先物取引";
urlList2[5][17] = "外国為替";
urlList2[5][18] = "リース";
urlList2[5][19] = "短資";
urlList2[5][20] = "格付会社";
urlList2[5][21] = "金融情報ベンダー";
urlList2[5][22] = "その他金融";


urlList1[6] = "建設/不動産";
MaxList2[6] = 14;
urlList2[6][0] = "設計事務所";
urlList2[6][1] = "建設コンサルティング";
urlList2[6][2] = "プラントメーカー";
urlList2[6][3] = "ゼネコン";
urlList2[6][4] = "サブコン";
urlList2[6][5] = "住宅（ハウスメーカー）";
urlList2[6][6] = "内装インテリア";
urlList2[6][7] = "ディベロッパー";
urlList2[6][8] = "不動産仲介";
urlList2[6][9] = "不動産管理";
urlList2[6][10] = "設備管理メンテナンス";
urlList2[6][11] = "土地活用";
urlList2[6][12] = "不動産金融";
urlList2[6][13] = "住宅設備建材";

urlList1[7] = "コンサルティング/専門事務所";
MaxList2[7] = 15;
urlList2[7][0] = "経営/戦略コンサルティング";
urlList2[7][1] = "組織人事コンサルティング";
urlList2[7][2] = "財務/会計アドバイザリー";
urlList2[7][3] = "リスクコンサルティング";
urlList2[7][4] = "ITコンサルティング";
urlList2[7][5] = "その他専門コンサルティング";
urlList2[7][6] = "シンクタンク";
urlList2[7][7] = "マーケティング/リサーチ";
urlList2[7][8] = "監査法人";
urlList2[7][9] = "税理士法人";
urlList2[7][10] = "法律事務所";
urlList2[7][11] = "会計事務所";
urlList2[7][12] = "特許事務所弁理士事務所";
urlList2[7][13] = "司法書士事務所/行政書士事務所";
urlList2[7][14] = "社会保険労務士事務所";


urlList1[8] = "人材サービス/アウトソーシング";
MaxList2[8] = 8;
urlList2[8][0] = "人材紹介/職業紹介";
urlList2[8][1] = "人材派遣";
urlList2[8][2] = "人材広告";
urlList2[8][3] = "組織人事コンサルティング ";
urlList2[8][4] = "研修サービス";
urlList2[8][5] = "アウトソーシング";
urlList2[8][6] = "技術系アウトソーシング";
urlList2[8][7] = "コールセンター";

urlList1[9] = "メディア/広告";
MaxList2[9] = 10;
urlList2[9][0] = "EC/ポータル/ソーシャル";
urlList2[9][1] = "Webインテグレータ/Web制作";
urlList2[9][2] = "総合広告代理店";
urlList2[9][3] = "専門広告代理店";
urlList2[9][4] = "広告制作";
urlList2[9][5] = "PR代理店";
urlList2[9][6] = "SP代理店";
urlList2[9][7] = "放送/新聞/出版";
urlList2[9][8] = "ネット広告/Webマーケティング";
urlList2[9][9] = "印刷";

urlList1[10] = "小売";
MaxList2[10] = 10;
urlList2[10][0] = "百貨店";
urlList2[10][1] = "食品スーパー/GMS";
urlList2[10][2] = "ディスカウントストア/量販店";
urlList2[10][3] = "コンビニエンスストア";
urlList2[10][4] = "ホームセンター/DIY";
urlList2[10][5] = "自動車ディーラー";
urlList2[10][6] = "アパレル販売";
urlList2[10][7] = "ラグジュアリ";
urlList2[10][8] = "その他専門店小売業";
urlList2[10][9] = "通信販売";

urlList1[11] = "外食";
MaxList2[11] = 4;
urlList2[11][0] = "レストラン";
urlList2[11][1] = "カフェ/ファーストフード";
urlList2[11][2] = "居酒屋/パブ";
urlList2[11][3] = "宅食/給食/中食";

urlList1[12] = "運輸/物流";
MaxList2[12] = 6;
urlList2[12][0] = "鉄道業";
urlList2[12][1] = "道路貨物運送業";
urlList2[12][2] = "道路旅客運送業";
urlList2[12][3] = "海運業";
urlList2[12][4] = "航空運輸業";
urlList2[12][5] = "倉庫業梱包業";

urlList1[13] = "エネルギー";
MaxList2[13] = 2;
urlList2[13][0] = "電力/ガス/石油";
urlList2[13][1] = "新エネルギー";

urlList1[14] = "旅行/宿泊/レジャー";
MaxList2[14] = 4;
urlList2[14][0] = "旅行旅行代理業";
urlList2[14][1] = "ホテル旅館宿泊施設";
urlList2[14][2] = "レジャー/アミューズメント";
urlList2[14][3] = "スポーツヘルス関連施設";

urlList1[15] = "警備/清掃";
MaxList2[15] = 2;
urlList2[15][0] = "警備業";
urlList2[15][1] = "清掃業";

urlList1[16] = "理容/美容/エステ";
MaxList2[16] = 1;
urlList2[16][0] = "理容/美容/エステ";

urlList1[17] = "教育";
MaxList2[17] = 3;
urlList2[17][0] = "学習塾/予備校/専門学校";
urlList2[17][1] = "その他各種スクール";
urlList2[17][2] = "学校";

urlList1[18] = "冠婚葬祭";
MaxList2[18] = 2;
urlList2[18][0] = "葬儀";
urlList2[18][1] = "ウェディング";

urlList1[19] = "農林/水産/鉱業";
MaxList2[19] = 3;
urlList2[19][0] = "農林";
urlList2[19][1] = "水産";
urlList2[19][2] = "鉱業";

urlList1[20] = "公社/官公庁/学校";
MaxList2[20] = 3;
urlList2[20][0] = "公社";
urlList2[20][1] = "官公庁";
urlList2[20][2] = "学校";

urlList1[21] = "その他";
MaxList2[21] = 2;
urlList2[21][0] = "特殊法人/財団法人/その他団体連合会";
urlList2[21][1] = "その他";


function List1Out(Ltitle1,Ltitle2){
	var i,chk_num = 0;
	document.getElementById("LTitle1").options.length = MaxList1;
	for(i = 0; i <= MaxList1 - 1; i++){
		document.getElementById("LTitle1").options[i].text = urlList1[i];
		document.getElementById("LTitle1").options[i].value = urlList1[i];
		if (Ltitle1 == urlList1[i]){ chk_num = i;}
	}
	document.getElementById("LTitle1").options[chk_num].selected = true;
	List2Out(Ltitle2);
}

function List2Out(Ltitle2){
	var i,n,chk_num2 = 0;
	n = document.getElementById("LTitle1").selectedIndex;
	document.getElementById("LTitle2").options.length = MaxList2[n];
	for(i = 0; i <= MaxList2[n] - 1; i++){
		document.getElementById("LTitle2").options[i].text = urlList2[n][i];
		document.getElementById("LTitle2").options[i].value = urlList2[n][i];
		if (Ltitle2 == urlList2[n][i]){ chk_num2 = i;}
	}
	document.getElementById("LTitle2").selectedIndex = chk_num2;
}


</script>

<script src="../js/jquery.autoKana.js" language="javascript" type="text/javascript"></script></script>
<script type="text/javascript">
    $(document).ready(
        function() {               
            $.fn.autoKana('#company_name', '#company_name_reading', {
                katakana : true  //true：カタカナ、false：ひらがな（デフォルト）
        });
    });
</script>

<body onload="List1Out('<?php echo $L1.'\',\''.$L2;?>')">

	<section id="m-2-box">
		<h2>
		基本設定
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(1)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form name="default_form" action="" method="post" >
		<table>
			<tr id="first">
				<th>事業所名</th>
				<td><input type="text" id="company_name"  name="company_name" value="<?php echo $company_arr[0]['company_name']; ?>" /></td>
			</tr>
			<tr>
				<th>フリガナ</th>
				<td><input type="text" id="company_name_reading" name="company_name_reading" value="<?php echo $company_arr[0]['company_name_reading']; ?>" /></td>
			</tr>
			<tr>
				<th>事業所区分</th>
				<td><?php if ($company_arr[0]['segment'] == 0 ) { echo '<input type="radio" name="segment" value="0" checked /> 個人事業 <input type="radio" name="segment" value="1" /> 法人 ';} else if ($company_arr[0]['segment'] == 1 ) { echo '<input type="radio" name="segment" value="0" />個人事業主 <input type="radio" name="segment" value="1" checked />法人 ';} ?> </td>
			</tr>
			<tr>
				<th>事業所種別</th>
				<td>
					<select name="LTitle1" id="LTitle1" size="1" onchange="List2Out()">
					</select>
					<select name="LTitle2" id="LTitle2" size="1">
					</select>
				</td>
			</tr>
			<tr>
				<th>郵便番号</th>
				<td><input type="text" name="zip_code" value="<?php echo $company_arr[0]['zip_code']; ?>" /></td>
			</tr>
			<tr>
				<th>都道府県</th>
				<td><input type="text" name="prefecture" value="<?php echo $company_arr[0]['prefecture']; ?>" /></td>
			</tr>
			<tr>
				<th>住所１</th>
				<td><input type="text" name="address1" value="<?php echo $company_arr[0]['address1']; ?>" /></td>
			</tr>
			<tr>
				<th>住所２</th>
				<td><input type="text" name="address2" value="<?php echo $company_arr[0]['address2']; ?>" /></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><input type="text" name="tel_num" value="<?php echo $company_arr[0]['tel_num']; ?>" /></td>
			</tr>
			<tr>
				<th>FAX番号</th>
				<td><input type="text" name="fax_num" value="<?php echo $company_arr[0]['fax_num']; ?>" /></td>
			</tr>
			<tr id="last">
				<th id="last">URL</th>
				<td id="last"><input type="text" name="url" value="<?php echo $company_arr[0]['url']; ?>" /></td>
				<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
				<input type="hidden" name="change_place" value="1" />
				<input type="hidden" name="historical" value="<?php echo $company_arr[0]['historical']; ?>" />
				<input type="hidden" name="id" value="<?php echo $company_arr[0]['id']; ?>" />
			</tr>
		
		
		</table>
		
		<div id="button">
			<button onclick="submit();">更新する</button>
		
			<button onclick="CloseFrame(1)">閉じる</button>
		</div>
		</form>
	</section>


</body>


