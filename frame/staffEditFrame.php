<?php session_start(); ?>
<?php require("fheader.php");?>
<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if (isset($_REQUEST['cid'])) {
	$client_id = $cid = $_REQUEST['cid'];
}
if (isset($_REQUEST['ins'])) {
	$ins_id = $_REQUEST['ins'];
}
if (isset($_REQUEST['client_id'])) {
	if ($_REQUEST['client_id'] == "") {
		$client_id = $_REQUEST['client_id'] = random(11);
	} else {
		$client_id = $_REQUEST['client_id'];
	}
}
if (isset($_REQUEST['id'])) {
	$staff_table_id = $_REQUEST['id'];
}

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
}

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//client_idの重複チェック用 履歴データを残す仕組みに変更した為、重複チェックはしない。20150522濱崎
//設定内容の表示データ取得
$flag = "staff_data_one";
$words = "AND company_id = ".$company_id." " ;
$search_duplication = $company_con -> company_sql_flag($pdo,$flag,$client_id,$words);
$registered_id = $search_duplication[0]['id'];
$duplication_flag = count($search_duplication);
*/
	
if ($ins_id == 1 && $_REQUEST['client_id'] != "" && $duplication_flag == 0) {
	//新しい送付先を作成
	$sql = "INSERT INTO `STAFF_TABLE`(
	`client_id`,`customer_code`,`company_name`,`company_name_reading`,`staff`,`department`,`position`, 
	`zip_code`,`prefecture`,`address1`,`address2`,`tel_num`, `fax_num`, 
	`email`,`payer`,`bank_number`,`branch_number`,`bank_account_number`,`client_company_id`,`company_id`
	) VALUES ( 
	'".$_REQUEST['client_id']."','".$_REQUEST['customer_code']."','".$_REQUEST['company_name']."','".$_REQUEST['company_name_reading']."','".$_REQUEST['staff']."','".$_REQUEST['department']."','".$_REQUEST['position']."', 
	'".$_REQUEST['zip_code']."','".$_REQUEST['prefecture']."','".$_REQUEST['address1']."','".$_REQUEST['address2']."','".$_REQUEST['tel_num']."', '".$_REQUEST['fax_num']."', 
	'".$_REQUEST['email']."','".$_REQUEST['payer']."','".$_REQUEST['bank_number']."','".$_REQUEST['branch_number']."','".$_REQUEST['bank_account_number']."','".$_REQUEST['client_company_id']."',".$user_id.")";

	echo $company_con -> company_sql($pdo,$company_id,$sql);
	echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
} else if (isset($cid) && $_REQUEST['client_id'] != "" && (($staff_table_id != $registered_id && $duplication_flag == 0)||($staff_table_id == $registered_id && $duplication_flag == 1))) {
	
	//元データを変更したデータをもった送付先を新規作成（historicalに+1する）
	$sql = "INSERT INTO `STAFF_TABLE`(
	`historical`,`client_id`,`customer_code`,`company_name`,`company_name_reading`,`staff`,`department`,`position`, 
	`zip_code`,`prefecture`,`address1`,`address2`,`tel_num`, `fax_num`, 
	`email`,`payer`,`bank_number`,`branch_number`,`bank_account_number`,`client_company_id`,`company_id`
	) VALUES ( 
	`historical` + 1,'".$_REQUEST['client_id']."','".$_REQUEST['customer_code']."','".$_REQUEST['company_name']."','".$_REQUEST['company_name_reading']."','".$_REQUEST['staff']."','".$_REQUEST['department']."','".$_REQUEST['position']."', 
	'".$_REQUEST['zip_code']."','".$_REQUEST['prefecture']."','".$_REQUEST['address1']."','".$_REQUEST['address2']."','".$_REQUEST['tel_num']."', '".$_REQUEST['fax_num']."', 
	'".$_REQUEST['email']."','".$_REQUEST['payer']."','".$_REQUEST['bank_number']."','".$_REQUEST['branch_number']."','".$_REQUEST['bank_account_number']."','".$_REQUEST['client_company_id']."',".$user_id.")";

	$company_con -> company_sql($pdo,$company_id,$sql);
	
	//元データを過去のデータに変更する（del_flag = 1にする）
	$sql = "UPDATE `STAFF_TABLE` SET `del_flag` = 1 WHERE id = ".$_REQUEST['id'];
	
	/*以前のそのまま変更していたバージョン
	$sql = "UPDATE `STAFF_TABLE` SET 
	`client_id`='".$_REQUEST['client_id']."',`customer_code`='".$_REQUEST['customer_code']."',`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
	`staff`='".$_REQUEST['staff']."',`department`='".$_REQUEST['department']."',`position`='".$_REQUEST['position']."', 
	`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
	`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',`fax_num`='".$_REQUEST['fax_num']."',
	`email`='".$_REQUEST['email']."',`payer`='".$_REQUEST['payer']."',`bank_number`='".$_REQUEST['bank_number']."',`branch_number`='".$_REQUEST['branch_number']."',`bank_account_number`='".$_REQUEST['bank_account_number']."',`client_company_id`='".$_REQUEST['client_company_id']."' 
	 WHERE id = ".$_REQUEST['id']." ";
	*/
	$company_con -> company_sql($pdo,$company_id,$sql);
	echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";

} else if($_REQUEST['client_id'] != "") {
	//echo '<p style="color:red;">管理コードが重複しています。</p>';
	echo "<script type='text/javascript'>alert('管理コードが重複しています。');</script>";
	echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
}

//設定変更の処理▲

//設定内容の表示データ取得
$flag = "staff_data_one";
$words = "AND company_id = ".$company_id." AND `del_flag` <> 1";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$client_id,$words);
$client_company_id = $company_arr[0]['client_company_id'];

function random($length) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>

<script>

function OpenkCommonSearchFrame(){
	document.getElementById("k2CommonSearch").style.display="block";
	document.getElementById("none2Layer").style.visibility = "visible";
	window.scrollTo(0,0);
}

function FrameHeight(){
	window.setTimeout("window.frames['k2CommonSearch'].test()","600");
}

function CloseFrame2(){
	document.getElementById("none2Layer").style.display = "none";
	document.getElementById("k2CommonSearch").style.display = "none";
}
</script>

<body>

	<iframe id="k2CommonSearch" name="k2CommonSearch" src="./k2CommonSearch" scrolling="no"></iframe>

	<section id="m-2-box">
		<h2>
		送付先設定
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(20)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form name="default_form" action="" method="post" >
		<table>
			<!--<tr id="first">-->
				<!--<th>送付先コード</th>-->
			<!--	<td><input type="hidden" id="client_id" name="client_id" value="<?php echo $company_arr[0]['client_id']; ?>" /></td>-->
			<!--</tr>-->
			<!--<tr>-->
			<!--<th>共通コード<br>(基本未記入)</th>
				<td>
					<input type="hidden" id="Kyoutu_No" name="client_company_id" class="kCommonSearch" name="destination_id_visible" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame()" value="<?php if ($client_company_id != "") {echo $client_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
					<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="id" value="<?php echo $company_arr[0]['id']; ?>" />
				</td>-->
			<!--</tr>-->
			<tr id="first">
				<th>顧客コード</th>
				<td><input type="text" id="customer_code" name="customer_code" value="<?php echo $company_arr[0]['customer_code']; ?>" onkeyup="makeClientId();" /></td>
			</tr>

			<tr>
				<th>事業所名</th>
				<td><input type="text" name="company_name" value="<?php echo $company_arr[0]['company_name']; ?>" /></td>
			</tr>
			<tr>
				<th>所属部署</th>
				<td><input type="text" name="department" value="<?php echo $company_arr[0]['department']; ?>" /></td>
			</tr>
			<!--
			<tr>
				<th>担当者役職</th>
				<td><input type="text" name="position" value="<?php echo $company_arr[0]['position']; ?>" /></td>
			</tr>
			-->
			<tr>
				<th>担当者</th>
				<td><input type="text" name="staff" value="<?php echo $company_arr[0]['staff']; ?>" /></td>
			</tr>
			<tr>
				<th>郵便番号</th>
				<td><input type="text" name="zip_code" value="<?php echo $company_arr[0]['zip_code']; ?>" /></td>
			</tr>
			<tr>
				<th>都道府県</th>
				<td><input type="text" name="prefecture" value="<?php echo $company_arr[0]['prefecture']; ?>" /></td>
			</tr>
			<tr>
				<th>住所１</th>
				<td><input type="text" name="address1" value="<?php echo $company_arr[0]['address1']; ?>" /></td>
			</tr>
			<tr>
				<th>住所２</th>
				<td><input type="text" name="address2" value="<?php echo $company_arr[0]['address2']; ?>" /></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><input type="text" name="tel_num" value="<?php echo $company_arr[0]['tel_num']; ?>" /></td>
			</tr>
			<tr>
				<th>FAX番号</th>
				<td><input type="text" name="fax_num" value="<?php echo $company_arr[0]['fax_num']; ?>" /></td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td><input type="text" name="email" value="<?php echo $company_arr[0]['email']; ?>" />
					<input type="hidden" id="client_id" name="client_id" value="<?php echo $company_arr[0]['client_id']; ?>" />
					<input type="hidden" id="Kyoutu_No" name="client_company_id" class="kCommonSearch" name="destination_id_visible" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame()" value="<?php if ($client_company_id != "") {echo $client_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
					<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="id" value="<?php echo $company_arr[0]['id']; ?>" />
				</td>
			</tr>
			<tr>
				<th>銀行口座名義<br/>(カタカナ)</th>
				<td><input type="text" name="payer" value="<?php echo $company_arr[0]['payer']; ?>" /></td>
			</tr>
			<tr>
				<th>銀行番号</th>
				<td><input type="text" name="bank_number" value="<?php echo $company_arr[0]['bank_number']; ?>" /></td>
			</tr>
			<tr>
				<th>支店番号</th>
				<td><input type="text" name="branch_number" value="<?php echo $company_arr[0]['branch_number']; ?>" /></td>
			</tr>
			<tr id="last">
				<th id="last">口座番号</th>
				<td id="last"><input type="text" name="bank_account_number" value="<?php echo $company_arr[0]['bank_account_number']; ?>" /></td>
			</tr>

		</table>
		
		<div id="button">
			<button onclick="submit();">更新する</button>
		
			<button onclick="CloseFrame(20)">閉じる</button>
		</div>
		</form>

	</section>
<div id="none2Layer" onclick="CloseFrame2()"></div>
<div id

</body>


