<?php session_start(); ?>
<?php require("fheader.php");?>
<?php

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
}

?>

<script type="text/javascript">


$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});

function calk(){
	var target = document.forms.default_form.box1.value;
	var idate = document.forms.default_form.MakeDate.value;
	var imonth = document.forms.default_form.MakeMonth.value;
	var ldate = document.forms.default_form.box2.value;
	var pdate = document.forms.default_form.PayDay.value;
	var pmonth = document.forms.default_form.PayMonth.value;
	
	var yy = ldate.substring(0,4) * 1;
	var mm = ldate.substring(4,6) -1;
	var dd = ldate.substring(6,8) * 1;
	var ldate = new Date(yy,mm,dd);

	var y = target.substring(0,4) * 1;
	var d = target.substring(6,8) * 1;
	if (d < idate){
		var m = target.substring(4,6)-1;
	}
	else{
		var m = target.substring(4,6);
	}
	var dt1 = new Date(y,m,idate);
	
	if (pdate == 0){
		var pm = m*1 + pmonth*1;
	}else{
		var pm = m*1 + pmonth*1;
	}
	var pt1 = new Date(y,pm,pdate);
	
	if (dt1 <= ldate){
	document.forms.default_form.make1.value = dt1.toLocaleDateString("ja-JP");
	document.forms.default_form.pay1.value = pt1.toLocaleDateString("ja-JP");
	}
	else{
	document.forms.default_form.make1.value ="";
	document.forms.default_form.pay1.value = "";
	}
	
	m = m*1 + imonth*1;
	var dt2 = new Date(y,m,idate);
	pm = pm*1 + imonth*1;
	var pt2 = new Date(y,pm,pdate);
	if (dt2 <= ldate){
	document.forms.default_form.make2.value = dt2.toLocaleDateString("ja-JP");
	document.forms.default_form.pay2.value = pt2.toLocaleDateString("ja-JP");
	}
	else{
	document.forms.default_form.make2.value = "";
	document.forms.default_form.pay2.value = "";
	}
	
	m = m*1 + imonth*1;
	var dt3 = new Date(y,m,idate);
	pm = pm*1 + imonth*1;
	var pt3 = new Date(y,pm,pdate);
	if (dt3 <= ldate){
	document.forms.default_form.make3.value = dt3.toLocaleDateString("ja-JP");
	document.forms.default_form.pay3.value = pt3.toLocaleDateString("ja-JP");
	}
	else{
	document.forms.default_form.make3.value = "";
	document.forms.default_form.pay3.value = "";
	}

}

</script>
<script type="text/javascript">
// 全ての文字列 s1 を s2 に置き換える  
function replaceAll(expression, org, dest){  
    return expression.split(org).join(dest);  
} 

function setParentInputValues() {
	try {
		parent.document.getElementById('save_template').value = 1;
		parent.document.getElementById('template_cycle').value = document.getElementById('MakeMonth').value;
		parent.document.getElementById('reservation_date').value = document.getElementById('MakeDate').value;
		parent.document.getElementById('template_maturity_cycle').value = document.getElementById('PayMonth').value;
		parent.document.getElementById('template_maturity_date').value = document.getElementById('PayDay').value;
		parent.document.getElementById('start_date').value = document.getElementById('box1').value;
		parent.document.getElementById('end_date').value = document.getElementById('box2').value;
//スラッシュを削除すると、月付の桁数が1桁の時にDBで日付と認識されなくなるため、/の置き換えをなしにする。
//		parent.document.getElementById('next_creation_date').value = replaceAll(document.getElementById('make1').value, "/", "");
//		parent.document.getElementById('next_payment_date').value = replaceAll(document.getElementById('pay1').value, "/", "");
		parent.document.getElementById('next_creation_date').value = document.getElementById('make1').value;
		parent.document.getElementById('next_payment_date').value = document.getElementById('pay1').value;
		parent.document.getElementById('template_name').value = document.getElementById('template_name').value;
		parent.saveRegularInvoice();
	} catch (e) {
		alert("送信スクリプトエラーです。");
	}
}

</script>


<body onLoad="calk()">


	<section id="m-2-box">
		<h2>
			定期請求書設定
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(17)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form name="default_form" action="" method="post" >
		<table>
			<tr id="first">
				<th>定期請求名</th>
				<td id=""><input type="text" id="template_name" name="template_name" /></td>
			</tr>
			<tr id="">
				<th>周期</th>
				<td>
					<select id="MakeMonth" name="MakeMonth" onchange="calk()">
						<option value="1">毎月</option>
						<option value="2">隔月</option>
						<option value="3">3ヶ月毎</option>
						<option value="6">半年毎</option>
						<option value="12">1年毎</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>日付</th>
				<td>
					<select id="MakeDate" name="MakeDate" onchange="calk()">
						<option value="0">月末</option>
					<script type="text/javascript">
						for (var i = 30; i >= 1; i--) {
							document.write('<option value="'+i+'">'+i+'日</option>');
						}
					</script>
					</select>
				</td>
			</tr>
			<tr>
				<th>開始日</th>
				<td>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 1;
						var d=now.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input id="box1" onblur="calk()" onclick="calk()" onchange="calk()" type="text" value="' + hiduke + '" />');
					</script>
				</td>
			</tr>
			<tr>
				<th>終了日</th>
				<td>
					<script>
						var now=new Date();
						var y=now.getFullYear() + 1;
						var m=now.getMonth() + 1;
						var d=now.getDate() -1 ;
						var hiduke=y*10000+m*100+d;
						document.write('<input id="box2" onblur="calk()" onclick="calk()" onchange="calk()" type="text" value="' + hiduke + '" />');
					</script>
				</td>
			</tr>
			<tr>
				<th style="height:100px;">直近作成日</th>
				<td>
					<input type="text" id="make1" readonly="readonly" style="border:0; box-shadow:none;" />
					<input type="text" id="make2" readonly="readonly" style="border:0; box-shadow:none;" />
					<input type="text" id="make3" readonly="readonly" style="border:0; box-shadow:none;" />
				</td>
			</tr>
			<tr>
				<th>支払い期日</th>
				<td>
					<select id="PayMonth" name="PayMonth" onchange="calk()">
						<option value="1">翌月</option>
						<option value="0">当月</option>
						<option value="2">2ヶ月後</option>
						<option value="3">3ヶ月後</option>
						<option value="4">4ヶ月後</option>
						<option value="5">5ヶ月後</option>
						<option value="6">6ヶ月後</option>
					</select>
					の
					<select id="PayDay" name="PayDay" onchange="calk()">
						<option value="0">月末</option>
					<script type="text/javascript">
						for (var i = 30; i >= 1; i--) {
							document.write('<option value="'+i+'">'+i+'日</option>');
						}
					</script>
					</select>
					
				</td>
			</tr>
			<tr id="last">
				<th id="last" style="height:100px;">直近支払期日</th>
				<td id="last">
					<input type="text" id="pay1" readonly="readonly" style="border:0; box-shadow:none;" />
					<input type="text" id="pay2" readonly="readonly" style="border:0; box-shadow:none;" />
					<input type="text" id="pay3" readonly="readonly" style="border:0; box-shadow:none;" />
				</td>
			</tr>

		
		
		</table>
		
		<div id="button">
			<button onclick="setParentInputValues();">保存する</button>
			<button onclick="CloseFrame(17)">閉じる</button>
		</div>
		</form>

	</section>


</body>
