<?php require("fheader.php");?>

<body>

	<section id="m-2-box">
		<h2>
		請求書発行
		</h2>
		
		<div id="close">
			<a  onclick="CloseFrame(6)">
				<img src="/images/cross.png">
			</a>
		</div>

		<p>
			選択した請求書のPDFを作成致します。<br>
			請求データの状態が発行済みに変更され、請求データの削除が出来なくなります。<br>
			選択した請求書のPDFを作成してよろしいですか？
		</p>
		
		<div id="button">
			<button onclick="submit(6)">PDFで発行する</button>
			<button onclick="CloseFrame(6)">閉じる</button>
		</div>
		
	</section>


</body>
