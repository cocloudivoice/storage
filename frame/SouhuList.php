<?php require("../main_read.php");?>

<?php

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();


//送付先管理のデータ取得
$flag = "addressee_data_all_with_conditions";
$condition_words = "ORDER BY `company_name` ASC ";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$condition_words);
$flag="";
$words="";
//var_dump($company_arr);


if ($destination_data_arr[0]["company_id"] != 0 && isset($destination_data_arr[0]["company_id"])) {
	if (isset($destination_data_arr[0]["company_id"])) { $destination_company_id = $destination_data_arr[0]["company_id"];}
	if (isset($destination_data_arr[0]["company_name"])) { $destination_name = $destination_data_arr[0]["company_name"];}
	if (isset($destination_data_arr[0]["zip_code"])) { $destination_zip_code = $destination_data_arr[0]["zip_code"];}
	if (isset($destination_data_arr[0]["address1"])) { $destination_address = $destination_data_arr[0]["prefecture"].$destination_data_arr[0]["address1"];}
	if (isset($destination_data_arr[0]["address2"])) { $destination_address2 = $destination_data_arr[0]["address2"];}
	if (isset($destination_data_arr[0]["section"])) { $destination_clerk = $destination_data_arr[0]["section"]; }
	if (isset($destination_data_arr[0]["clerk"])) { $destination_clerk2 = $destination_data_arr[0]["clerk"]; }
	if (isset($destination_data_arr[0]["email"])) { $destination_email = $destination_data_arr[0]["email"]; }
}


?>


<script>

function getText(obj,code){
	var souhu = obj.innerHTML;
	//alert(code);
	//var code = souhu.match(/\(.+?\)/);
	//send_code = String(code).slice(1,-1);
	send_code = String(code);
	parent.document.getElementById("souhuname").value = souhu;
	parent.document.getElementById("select_dname").value = send_code;
	parent.document.getElementById("send_code").value = send_code;
	parent.TransClientId();
	parent.document.getElementById("SouhuList").style.display = "none";
	parent.document.getElementById("CommonSearch").style.display = "none";
	parent.document.getElementById("ChangeInvoice").style.display = "none";
	parent.document.getElementById("company_addressee_flag").value = 2;
	//submitせずにAjaxで処理する。
	parent.sendByAjax(2);
	//parent.document.client_code_sender.submit();
}


function grep (pattern) {
	try {
		var regex = new RegExp(pattern, "i");
		var spans = document.getElementsByTagName('p');
		var length = spans.length;
		for (var i = 0; i < length; i++) {
			var e = spans[i];
			if (e.className == "line") {
				if (e.innerHTML.match(regex)) {
					e.style.display = "block";
				} else {
					e.style.display = "none";
				}
			}
		}
	} catch (e) {
	}
}

function sizingFrame(){
	var x = document.getElementById("result").clientHeight;
	if ( x == 0 ){
		parent.document.getElementById("SouhuList").style.display = "none";
	}
	else{
		parent.document.getElementById("SouhuList").style.height = x + "px" ;
	}
}

</script>


<style type="text/css">

html *{margin:0px; padding:0px; outline:none; overflow:hidden; max-height:235px;}
body {background-color:rgb(250,250,250); font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
.line:hover{background-color:rgb(55,120,215);}

</style>


<body>
<form onsubmit="return false;" name="myForm">
	<div id="result">
	<input type="hidden" id="sList" name="pattern" onChange="grep(this.value)" onkeyup="grep(this.value)"></input>
	<div id="lists" style="width:365px; overflow:hidden; background-color:white;">
		<?php
		for($k = 0; $k < count($company_arr); $k++ ) {
			echo '<p class="line" onclick="getText(this,\''.(string)$company_arr[$k]["client_id"].'\')" style="width:300px; padding:2px 0px 2px 4px; height:35px; margin:0; font-size:12px; font-weight:bold;">'.$company_arr[$k]["company_name"].' ('.$company_arr[$k]["customer_code"].')<br> <span style="font-weight:normal;">'.$company_arr[$k]["prefecture"].$company_arr[$k]["address1"].' '.$company_arr[$k]["address2"].'</span></p>';
		}
		?>
	</div>
	</div>
</form>

</body>
