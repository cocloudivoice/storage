<?php require("fheader.php");?>

<body>

	<section id="m-2-box">
		<h2>
		請求データの削除
		</h2>
		

		<div id="close">
			<a  onclick="CloseFrame(7)">
				<img src="/images/cross.png">
			</a>
		</div>

		<p>
			選択した請求データを削除致します。<br>
			よろしいですか？
		</p>
		
		<div id="button">
			<button onclick="submit(7)">削除する</button>
			<button onclick="CloseFrame(7)">閉じる</button>
		</div>
		
	</section>


</body>
