<?php 
session_start();
require("fheader.php");

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');

//変数の宣言
if(isset($_SESSION["user_id"])) {
	$login_user = $_SESSION["user_id"];
}


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//ユーザーテーブルのコントローラーを呼び出す
$user_con = new user_control();
$user_arr  = $user_con->user_select_id($pdo,$login_user);

//ここからメールアドレス照合
if (isset($_REQUEST['temp_password'])) {
	//Emailアドレスの存在を確認する。
	$temp_password = md5(htmlspecialchars($_REQUEST['temp_password'],ENT_QUOTES));
	if ($temp_password == $user_arr['password']) {
		if ($_REQUEST['password'] == $_REQUEST['password2']) {
			$user_arr['password'] = md5($_REQUEST['password']);
			$user_con -> user_update($pdo,$user_arr);
			echo "<script type='text/javascript'>alert('パスワードを更新しました。');</script>";
			echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
		}
	} else {
		$_SESSION['err_msg'] ="現在のパスワードが間違っています。";
	}
	
}

?>

<script type="text/javascript">
	function checkPass() {
		var p1 = register.password.value;
		var p2 = register.password2.value;
		
		if (p1 == p2) {
			register.submit();
			return true;
			
		}else{
			alert('パスワードが一致していません');
			return false;
		}
		
	}
</script>
<style type="text/css">
	.red { color:red;}
</style>

<body>

	<section id="m-2-box">
		<h2>
		メールアドレスの変更
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(8)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form  name="register" action="" method="post">
		<table id="long">
			<tr id="first">
				<th>現在のパスワード</th>
				<td><input type="password" name="temp_password" /></td>
			</tr>
			<tr>
				<th>新しいパスワード</th>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr id="last">
				<th id="last">新しいパスワード（確認）</th>
				<td id="last"><input type="password" name="password2" /></td>
			</tr>
		</table>
		<?php
			echo '<p class="red">'.$_SESSION['err_msg'].'</p>';
			$_SESSION['err_msg'] = "";
		?>
		
		
		<div id="button">
			<button onclick="return checkPass();false">送信する</button>
			<button onclick="CloseFrame(8)">閉じる</button>
		</div>
		</form>

	</section>


</body>
