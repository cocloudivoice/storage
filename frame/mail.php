<?php require("fheader.php");?>

<body>

	<section id="m-2-box">
		<h2>
		請求書メール送信
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(4)">
				<img src="/images/cross.png">
			</a>
		</div>
		
		<div id="clear"></div>
		
		<p>
			選択した請求書を取引先にメール送信致します。<br>
			また、請求データの状態が発行済みに変更され、請求データの削除が出来なくなります。<br>
			請求書をメール送信してよろしいですか？
		</p>
		
		<div id="button">
			<button onclick="submit(4)">送信する</button>
			<button onclick="CloseFrame(4)">閉じる</button>
		</div>
		
	</section>


</body>
