<?php session_start(); ?>
<?php require("fheader.php");?>

<body>


	<section id="m-2-box">
		<h2>
		ビギナープランに変更
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(12)">
				<img src="/images/cross.png">
			</a>
		</div>

		<p>
			ビギナープランは月額無料です。<br>
			郵送代が200円、データ保管期間が3ヶ月間、サポートがメール対応となります。<br>
			また、今月分まで現在のプランの費用が発生致します。<br>
			丸投げオプションで30仕訳まで無料、超過分は1仕訳35円になります。<br>
			ビギナープランに変更しますか？
		</p>
		
		<div id="button">
			<button onclick="submit(12);">申込む</button>
			
			<button onclick="CloseFrame(12)">閉じる</button>

		</div>
		
	</section>


</body>


