<?php require("../main_read.php");?>

<?php

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();


//送付先管理のデータ取得
$flag = "addressee_data_all_with_conditions";
$condition_words = "ORDER BY `company_name` ASC ";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$condition_words);
$flag="";
$words="";
//var_dump($company_arr);


if ($destination_data_arr[0]["company_id"] != 0 && isset($destination_data_arr[0]["company_id"])) {
	if (isset($destination_data_arr[0]["company_id"])) { $destination_company_id = $destination_data_arr[0]["company_id"];}
	if (isset($destination_data_arr[0]["company_name"])) { $destination_name = $destination_data_arr[0]["company_name"];}
	if (isset($destination_data_arr[0]["zip_code"])) { $destination_zip_code = $destination_data_arr[0]["zip_code"];}
	if (isset($destination_data_arr[0]["address1"])) { $destination_address = $destination_data_arr[0]["prefecture"].$destination_data_arr[0]["address1"];}
	if (isset($destination_data_arr[0]["address2"])) { $destination_address2 = $destination_data_arr[0]["address2"];}
	if (isset($destination_data_arr[0]["section"])) { $destination_clerk = $destination_data_arr[0]["section"]; }
	if (isset($destination_data_arr[0]["clerk"])) { $destination_clerk2 = $destination_data_arr[0]["clerk"]; }
}


?>

<script>

function getText(obj,code){
	var souhu = obj.innerHTML;
	//alert(code);
	//var code = souhu.match(/\(.+?\)/);
	//send_code = String(code).slice(1,-1);
	send_code = String(code);
	parent.document.getElementById("souhuname").value=souhu;
	parent.document.getElementById("select_dname").value=send_code;
	parent.document.getElementById("noneLayer").style.visibility = "hidden";
	parent.document.getElementById("kSouhuList").style.display = "none";
}

function grep (pattern) {
	try {
		var regex = new RegExp(pattern, "i");
		var spans = document.getElementsByTagName('p');
		var length = spans.length;
		for (var i = 0; i < length; i++) {
			var e = spans[i];
			if (e.className == "line") {
				if (e.innerHTML.match(regex)) {
					e.style.display = "block";
				} else {
					e.style.display = "none";
				}
			}
		}
	} catch (e) {
	}
}

function test(){
	var x = document.getElementById("result").clientHeight;
	if ( x == 0 ){
		parent.document.getElementById("kSouhuList").style.display = "none";
	}
	else{
		parent.document.getElementById("kSouhuList").style.height = x + "px" ;
	}
}


</script>


<style type="text/css">

html *{margin:0px; padding:0px; outline:none; overflow-x:hidden;}
body {background-color:rgb(250,250,250); font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
.line:hover{background-color:rgb(55,120,215);}

</style>


<body>
<form onsubmit="return false;">
	<div id="result">
	<input type="text" id="sList" name="pattern" onkeyup="grep(this.value)" style="width:367px; margin:0 0 2px 0; height:30px; background-image:url('../../images/search.png'); background-repeat:no-repeat; background-position:325px 2px; background-size:20px;">
	</input>
	<div style="width:365px; border-width:1px; padding:5px 0 0 0; border-style:solid; border-color:rgb(200,200,200); overflow-x:hidden; background-color:white;">
	<?php
	for($k = 0; $k < count($company_arr); $k++ ) {
		echo '<p class="line" onclick="getText(this,\''.(string)$company_arr[$k]["client_id"].'\')" style="width:300px; padding:0px 0	 0px 10px; height:25px; margin:0;">'.$company_arr[$k]["company_name"].' ('.$company_arr[$k]["customer_code"].')'.'</p>';
	}
	?>
	</div>
	</div>
</form>

</body>
