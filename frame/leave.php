<?php 
session_start();
require("fheader.php");

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');

//変数の宣言
if(isset($_SESSION["user_id"])) {
	$login_user = $_SESSION["user_id"];
}


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//ユーザーテーブルのコントローラーを呼び出す
$user_con = new user_control();
$user_arr  = $user_con->user_select_id($pdo,$login_user);

//ここからメールアドレス照合
if (isset($_REQUEST['password'])) {
	//Emailアドレスの存在を確認する。
	$password1 = md5(htmlspecialchars($_REQUEST['password'],ENT_QUOTES));
	if ($password1 == $user_arr['password']) {
		
		$user_con -> user_delete_data($pdo,$login_user,$user_arr['mail_address']);
		$_SESSION['user_id'] = NULL;
		echo "<script type='text/javascript'>alert('退会処理が完了しました。またのご利用をお待ちしております。');</script>";
		echo "<script type='text/javascript'>parent.location.href = './../logout';</script>";
		exit();
	} else {
		$_SESSION['err_msg'] ="パスワードが間違っています。";
	}
	
}

?>


<body>

	<section id="m-2-box">
		<h2>
		退会
		</h2>
		
		
		<div id="close">
			<a  onclick="CloseFrame(10)">
				<img src="/images/cross.png">
			</a>
		</div>
		<?php
			echo "<p style='color:red;'>".$_SESSION['err_msg']."</p>";
			$_SESSION['err_msg'] = "";
		?>

		<p>
			退会致しますと、これまでに入力していただいたデータはすべて削除されます。<br>
			また、削除したデータの復旧もいたしかねますので、ご理解ください。
		</p>
		<form  name="register" action="" method="post">
			<table id="long">
				<tr>
					<th id="last">パスワード</th>
					<td id="last"><input type="password" name="password" /></td>
				</tr>
			</table>
		
			<div id="button">
				<button onclick="submit();">退会する</button>
				<button onclick="CloseFrame(10)">閉じる</button>
			</div>
		</form>
	</section>


</body>
