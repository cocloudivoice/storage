<?php session_start(); ?>
<?php require("fheader.php");?>
<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
$path;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
}

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

//ロゴと印鑑の登録内容を消す処理
if (isset($_REQUEST['delete_img'])) {
	$del_img = htmlspecialchars($_REQUEST['delete_img'],ENT_QUOTES);
	if ($del_img == 1) {
		//印鑑
		$sql = "UPDATE `COMPANY_TABLE` SET 
		stamp_image = '' 
		WHERE company_id = ".$company_id."";

	}else if ($del_img == 2) {
		//ロゴ
		$sql = "UPDATE `COMPANY_TABLE` SET 
		logotype = '' 
		WHERE company_id = ".$company_id."";
	}
	$company_con -> company_sql($pdo,$company_id,$sql);
}


if (isset($_REQUEST['email'])) {

	if ($_REQUEST['change_place'] == 1) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`segment`=".$_REQUEST['segment'].",`zip_code`=".$_REQUEST['zip_code'].",`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`tel_num`=".$_REQUEST['tel_num'].",
		`fax_num`=".$_REQUEST['fax_num'].",`url`='".$_REQUEST['url']."' 
		 WHERE company_id = ".$company_id."";
	} else if ($_REQUEST['change_place'] == 2) {
		$account_info = "";
		if ($_REQUEST['ginkou1'] != "" ) { $account_info .= $_REQUEST['ginkou1']." ".$_REQUEST['siten1']." ".$_REQUEST['shubetu1']." ".$_REQUEST['kouza1']."<br/>";}
		if ($_REQUEST['ginkou2'] != "" ) { $account_info .= $_REQUEST['ginkou2']." ".$_REQUEST['siten2']." ".$_REQUEST['shubetu2']." ".$_REQUEST['kouza2']."<br/>";}
		if ($_REQUEST['ginkou3'] != "" ) { $account_info .= $_REQUEST['ginkou3']." ".$_REQUEST['siten3']." ".$_REQUEST['shubetu3']." ".$_REQUEST['kouza3']."<br/>";}
		if ($_REQUEST['ginkou4'] != "" ) { $account_info .= $_REQUEST['ginkou4']." ".$_REQUEST['siten4']." ".$_REQUEST['shubetu4']." ".$_REQUEST['kouza4']."<br/>";}
		if ($_REQUEST['ginkou5'] != "" ) { $account_info .= $_REQUEST['ginkou5']." ".$_REQUEST['siten5']." ".$_REQUEST['shubetu5']." ".$_REQUEST['kouza5']."<br/>";}
		//echo "<br/><br/>";
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`email`='".$_REQUEST['email']."',`section`='".$_REQUEST['section']."',
		`clerk`='".$_REQUEST['clerk']."',`account_name` = '".$_REQUEST['account_name']."',
		`bank_name1` = '".$_REQUEST['ginkou1']."',`blanch_name1` = '".$_REQUEST['siten1']."',`account_devision1` = '".$_REQUEST['shubetu1']."',`account_num1` ='".$_REQUEST['kouza1']."',
		`bank_name2` = '".$_REQUEST['ginkou2']."',`blanch_name2` = '".$_REQUEST['siten2']."',`account_devision2` = '".$_REQUEST['shubetu2']."',`account_num2` ='".$_REQUEST['kouza2']."',
		`bank_name3` = '".$_REQUEST['ginkou3']."',`blanch_name3` = '".$_REQUEST['siten3']."',`account_devision3` = '".$_REQUEST['shubetu3']."',`account_num3` ='".$_REQUEST['kouza3']."',
		`bank_name4` = '".$_REQUEST['ginkou4']."',`blanch_name4` = '".$_REQUEST['siten4']."',`account_devision4` = '".$_REQUEST['shubetu4']."',`account_num4` ='".$_REQUEST['kouza4']."',
		`bank_name5` = '".$_REQUEST['ginkou5']."',`blanch_name5` = '".$_REQUEST['siten5']."',`account_devision5` = '".$_REQUEST['shubetu5']."',`account_num5` ='".$_REQUEST['kouza5']."',
		`account_info`='".$account_info."',
		`remarks1`='".$_REQUEST['remarks1']."',`remarks2`='".$_REQUEST['remarks2']."' 
		 WHERE company_id = ".$company_id."";
	}
	//echo $sql;
	$company_con -> company_sql($pdo,$company_id,$sql);
}

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);

$path = "../../files/" .$top4."/". $mid4."/".$user_id. "/". "images";
//$path = "../../files/" . $user_id. "/". "images";
//画像のディレクトリ作成処理
if (isset($_FILES['stamp']["tmp_name"])|| isset($_FILES['logo']["tmp_name"])) {
	if (file_exists($path)) {
		echo "ディレクトリが存在します！！";
	} else {
		if ( mkdir( $path, 0775, true ) ) {
		  echo "ディレクトリ作成成功！！";
		} else {
		  echo "ディレクトリ作成失敗！！";
		}
	}
}
//var_dump($_FILES);
//画像のDB登録処理
if (is_uploaded_file($_FILES['stamp']["tmp_name"]) || is_uploaded_file($_FILES['logo']["tmp_name"])) {

	if ( isset($_SESSION['stamp_image']) ) {
		$stamp_image = $_SESSION['stamp_image'];
		$_SESSION['stamp_image'] = "";
	}
	
	if ( isset($_SESSION['logotype']) ) {
		$logotype = $_SESSION['logotype'];
		$_SESSION['logotype'] = "";
	}
	
	if (move_uploaded_file($_FILES['stamp']["tmp_name"], $path. "/" . $_FILES['stamp']["name"])) {
		
		//echo "stamp";
		chmod($path. "/" . $_FILES['stamp']["name"], 0664);
		$_SESSION['file_comment'] .= $path. "/" . $_FILES['stamp']["name"] . "をアップロードしました。<br/>";
		$stamp_image = $path. "/" . $_FILES['stamp']["name"];
	} else {
		$_SESSION['file_comment'] .= "印影のイメージをアップロードできませんでした。<br/>";
	}

	if (move_uploaded_file($_FILES["logo"]["tmp_name"], $path. "/" . $_FILES["logo"]["name"])) {
		//echo "logo";
		chmod($path. "/" . $_FILES["logo"]["name"], 0664);
		$_SESSION['file_comment'] .= $path. "/" . $_FILES["logo"]["name"] . "をアップロードしました。<br/>";
		$logotype = $path. "/" . $_FILES['logo']["name"];
	} else {
		$_SESSION['file_comment'] .= "ロゴのイメージをアップロードできませんでした。";
	}
	
	$flag = "image_upload";
	$words .= " `logotype` = '".$logotype."', `stamp_image` = '".$stamp_image."' " ;

	//var_dump($user_arr);
	$result = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$words = NULL;
	//var_dump($result);

} 


//設定変更の処理▲

//設定内容の表示データ取得
$flag = "company_data";
$company_arr  = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);

?>

<script>


function addRow(){
	var tblObj=document.getElementById("reTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==12){alert("これ以上登録する事はできません。");return;}
	var insObj = tblObj.insertRow(rowCnt - 6);
	
	var thObj = document.createElement( 'th' ); 
	thObj.innerHTML = '<h3>振込口座' + (rowCnt - 6) + '</h3>';
	insObj.appendChild(thObj);
	
	var tdObj = document.createElement( 'td' ); 
	tdObj.innerHTML ='<div id="ginkou"><p>銀行</p><input type="text" name="ginkou' + (rowCnt - 6) + '"></input></div><div id="siten"><p>支店</p><input type="text" name="siten' + (rowCnt - 6) + '"></input></div><div id="clear"></div><div id="shubetu"><input type="radio" name="shubetu' + (rowCnt - 6) + '" id="add" checked="checked"></input>普通<input type="radio" name="shubetu' + (rowCnt - 6) + '" id="add2"></input>当座</div><div id="kouza">口座番号<input type="text" id="add" name="kouza' + (rowCnt - 6) + '"></input></div><div id="clear"></div>';
	tdObj.id = "hurikomikouza";
	insObj.appendChild(tdObj);
	
}


function delRow(){
	var tblObj = document.getElementById("reTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==8){alert("削除できません。");return;}
	tblObj.deleteRow(rowCnt-7);
}

function delImg(n) {
	document.getElementById('delete_img').value = n;
	document.del_img_form.submit();
}


</script>





<body>
<form action="" method="post" name="del_img_form">
	<input type="hidden" id="delete_img" name="delete_img" value="" />
</form>
	<section id="m-2-box">
		<h2>
			帳票設定
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(2)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form name="report_form" action="./report.php" method="post" enctype="multipart/form-data" >
		<table id="reTBL">
			<tr id="first">
				<th>口座名義</th>
				<td><input type="text" name="account_name" value="<?php echo $company_arr[0]['account_name'];?>" /></td>
			</tr>
			<?php
			//var_dump($company_arr);
			$count_account = 1;
			if($company_arr[0]["bank_name5"] != "") {
				$count_account = 5;
			} else if ($company_arr[0]["bank_name4"] != "") {
				$count_account = 4;
			} else if ($company_arr[0]["bank_name3"] != "") {
				$count_account = 3;
			} else if ($company_arr[0]["bank_name2"] != "") {
				$count_account = 2;
			} else {
				$count_account = 1;
			}
			for ( $i = 1 ;$i <= $count_account ;$i++ ) {
			?>
			<tr>
				<th id="hurikomikouza">
					<h3>振込口座</h3>
					<?php 
					if ($i == 1) {
					?>
					<input type="button" value="口座追加" onclick="addRow()" id="button1"></input>
					<input type="button" value="口座削除" onclick="delRow()" id="button2"></input>
					<?php
					}
					?>
					</th>
				<!--<td><input type="text" name="account_info" value="<?php echo $company_arr[0]['account_info']; ?>" /></td>-->
				<td id="hurikomikouza">
					<div id="ginkou">
						<p>銀行</p>
						<?php $col_name = "bank_name".$i;?>
						<input type="text" name="ginkou<?php echo $i;?>" value="<?php echo $company_arr[0][$col_name];?>"></input>
					</div>
					<div id="siten">
						<p>支店</p>
						<?php $col_name = "blanch_name".$i;?>
						<input type="text" name="siten<?php echo $i;?>" value="<?php echo $company_arr[0][$col_name];?>"></input>
					</div>
					<div id="clear"></div>
					<div id="shubetu">
						<?php
						$col_name = "account_devision".$i;
						if ($company_arr[0][$col_name] == "普通") {
							$devision1 = "checked";
						} else if ($company_arr[0][$col_name] == "当座") {
							$devision2 = "checked";
						}
						?>
						<label><input type="radio" name="shubetu<?php echo $i;?>" value="普通" checked="checked" <?php echo $devision1;?> />
						普通</label>
						<label><input type="radio" name="shubetu<?php echo $i;?>" value="当座" <?php echo $devision2;?> />
						当座</label>
					</div>
					<div id="kouza">
						口座番号
						<?php $col_name = "account_num".$i;?>
						<input type="text" name="kouza<?php echo $i;?>" value="<?php echo $company_arr[0][$col_name];?>"></input>
					</div>
					<div id="clear"></div>
				</td>
			</tr>
			<?php
			}
			?>
			<tr>
				<th>ロゴ</th>
				<td>
					<input type="file" name="logo" />
					<?php
						if (file_exists($company_arr[0]["logotype"])) { 
							echo '<img src="'.$company_arr[0]["logotype"].'" alt="ロゴ画像" style="max-width:60px;max-height:60px;" />';
							$_SESSION['logotype'] = $company_arr[0]["logotype"];
							echo '　<input type="button" name="del_logo" onclick="delImg(2);" value="ロゴを消去" />';
						}
					?>
					
				</td>
			</tr>
			<tr>
				<th>印影</th>
				<td>
					<input type="file" name="stamp" />
					<?php
						if (file_exists($company_arr[0]["stamp_image"])) { 
							echo '<img src="'.$company_arr[0]["stamp_image"].'" alt="印鑑画像" style="max-width:60px;max-height:60px;" />';
							$_SESSION['stamp_image'] = $company_arr[0]["stamp_image"];
							echo '　<input type="button" name="del_stamp" onclick="delImg(1);" value="印影を消去" />';
						}
					?>
				</td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td><input type="text" name="email" value="<?php echo $company_arr[0]['email']; ?>" /></td>
			</tr>
			<tr>
				<th>部署</th>
				<td><input type="text" name="section" value="<?php echo $company_arr[0]['section']; ?>" /></td>
			</tr>
			<tr>
				<th>担当者</th>
				<td><input type="text" name="clerk" value="<?php echo $company_arr[0]['clerk']; ?>" /></td>
			</tr>
			<tr id="last">
				<th id="last">備考</th>
				<td id="last">
					<input type="text" name="remarks1" value="<?php echo $company_arr[0]['remarks1']; ?>" />
					<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="change_place" value="2" />
				</td>
		
			</tr>
			<!--
			<tr id="last">
				<th id="last">備考２</th>
				<td id="last">
					<input type="text" name="remarks2" value="<?php echo $company_arr[0]['remarks2']; ?>" />
					<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="change_place" value="2" />
				</td>
			</tr>
			-->
		
		</table>

		<div id="button">
			<button onclick="submit();">更新する</button>
			<button onclick="CloseFrame(2)">閉じる</button>
		</div>
		</form>
		
	</section>


</body>
	
<?php
if (isset($_REQUEST['email'])) {
	echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
}
?>