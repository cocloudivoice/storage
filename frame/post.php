<?php require("fheader.php");?>

<body>
<script type="text/javascript">
	function sendReserveDate() {
		parent.send_reserve_date.value = document.getElementById('send_date').value;
	}
</script>
	<section id="m-2-box">
		<h2>
		請求書郵送依頼
		</h2>
		
		<div id="close">
			<a  onclick="CloseFrame(5)">
				<img src="/images/cross.png">
			</a>
		</div>


		<p>
			選択した請求書を取引先に郵送致します。<br>
			請求書の郵送は有料となります。ご注意ください。また、請求データの状態が発行済みに変更され、請求データの削除が出来なくなります。<br>
			請求書の郵送依頼を行ってよろしいですか？
		</p>
		<p>
			<input type="text" id="send_date" name="send_date" value="" onkeyup="sendReserveDate()" placeholder="発送希望日があれば日付を入力（例）2014/11/01"/>
		</p>
		<div id="button">
			<button onclick="submit(5)">郵送依頼をする</button>
			<!--<button onclick="submit(51)">FAX依頼をする</button>-->
			<button onclick="CloseFrame(5)">閉じる</button>
		</div>
		
	</section>


</body>
