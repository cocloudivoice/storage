<?php session_start(); ?>
<?php require("fheader.php");?>

<body>


	<section id="m-2-box">
		<h2>
		プレミアムプランに変更
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(14)">
				<img src="/images/cross.png">
			</a>
		</div>

		<p>
			プレミアムプランは月額8,640円（税込）となります。<br>
			郵送代行が1通160円、データ保管期間が無制限、お客様専用カスタマーのバックアップサービスに加えて、専用請求書の作成、会計ソフト変換ツールの利用が可能です。<br>
			丸投げオプションで400仕訳まで無料、超過分は1仕訳25円になります。<br>
			プレミアムプランに加入されますか？
		</p>
		
		<div id="button">
			<button onclick="submit(14);">申込む</button>
		
			<button onclick="CloseFrame(14)">閉じる</button>
		</div>
		
	</section>


</body>


