<?php
session_start();
header('Access-control-allow-origin: *');// 通信を許可する接続元ドメインか*（アスタリスク）を指定

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>共通コード確認　- Cloud Invoice</title>

<script src="../../js/jquery-1.6.2.min.js"></script>
<script>
$(document).ready(function()
{

    /**
     * 送信ボタンクリック
     */
    $('#Kyoutu_No',parent.document).keyup(function()
    {
        //POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
        var data = {Kyoutu_No : $('#Kyoutu_No',parent.document).val()};

        /**
         * Ajax通信メソッド
         * @param type  : HTTP通信の種類
         * @param url   : リクエスト送信先のURL
         * @param data  : サーバに送信する値
         */
        $.ajax({
            type: "POST",
            url: "../send.php",
            data: data,
            /**
             * Ajax通信が成功した場合に呼び出されるメソッド
             */
            success: function(data, dataType)
            {
                //successのブロック内は、Ajax通信が成功した場合に呼び出される

                //PHPから返ってきたデータの表示
                document.getElementById('result').innerHTML = data;
                //document.write(data);
            },
            /**
             * Ajax通信が失敗した場合に呼び出されるメソッド
             */
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                //通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

                //this;
                //thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

                //エラーメッセージの表示
                alert('Error : ' + errorThrown);
            }
        });
        
        //サブミット後、ページをリロードしないようにする
        return false;
    });
});





function getText(obj){
	var souhu = obj.innerHTML;
	var code = souhu.substring(53,65);
	send_code = String(code).slice(1,-1);
	parent.document.getElementById("Kyoutu_No").value=code;
	parent.document.getElementById("noneLayer").style.visibility = "hidden";
	parent.document.getElementById("kCommonSearch").style.display = "none";
}

function test(){
	var x = document.getElementById("result").clientHeight;
	if ( x == 0 ){
		parent.document.getElementById("kCommonSearch").style.display = "none";
		parent.document.getElementById("kCommonSearch").style.height = 0 + "px" ;
	}
	else{
		parent.document.getElementById("kCommonSearch").style.height = x + "px" ;
	}
}

</script>
	
<style type="text/css">

html *{margin:0px; padding:0px; outline:none;}
body{background-color:rgb(250,250,250);}
input[type=text].CommonSearch{height:30px;}
input[type=text]{width:350px;}
p{padding:4px 0 4px 5px;margin:0; font-size:13px; width:360px; white-space:nowrap; overflow:hidden; color:rgb(100,100,100);background-color:white; cursor:pointer;}
div#result{overflow:hidden;}

</style>
	
</head>
<body>
	<div id="result"></div>
</body>
</html>