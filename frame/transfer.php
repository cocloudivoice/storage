<?php require("fheader.php");?>

<body>
<script type="text/javascript">
	function sendReserveDate() {
		parent.send_reserve_date.value = document.getElementById('send_date').value;
	}
</script>
	<section id="m-2-box">
		<h2>
		振込代行予約
		</h2>
		
		<div id="close">
			<a  onclick="CloseFrame(5)">
				<img src="/images/cross.png">
			</a>
		</div>


		<p>
			選択した請求書のお振込みを代行致します。<br>
			振り込み代行予約をすると、振込先と金額が記載されたメールが送信されます。
			メールに記載の弊社宛振込みが確認できましたら、約2営業日で請求書に記載された金融機関、もしくは受取の企業様が登録された金融機関に対して行われます。<br>
			また、振込み代行は請求書一通あたり300円の手数料を頂戴しております。ご注意ください。<br>
			振込み代行予約を行ってよろしいですか？
		</p>
		<p>
			<!--
			<input type="text" id="send_date" name="send_date" value="" onkeyup="sendReserveDate()" placeholder="発送希望日があれば日付を入力（例）2014/11/01"/>
			-->
		</p>
		<div id="button">
			<button onclick="submit(18)">振込代行を依頼する</button>
			<button onclick="CloseFrame(18)">閉じる</button>
		</div>
		
	</section>


</body>
