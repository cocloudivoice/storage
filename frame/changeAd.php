<?php 
session_start();
require("fheader.php");

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');

//変数の宣言
if(isset($_SESSION["user_id"])) {
	$login_user = $_SESSION["user_id"];
}


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//ユーザーテーブルのコントローラーを呼び出す
$user_con = new user_control();
$user_arr  = $user_con->user_select_id($pdo,$login_user);

//ここからメールアドレス照合
if (isset($_REQUEST['password'])) {
	//Emailアドレスの存在を確認する。
	$password1 = md5(htmlspecialchars($_REQUEST['password'],ENT_QUOTES));
	if ($password1 == $user_arr['password']) {
		$user_arr['mail_address'] = $_REQUEST['new_email'];
		$user_con -> user_update($pdo,$user_arr);
		echo "<script type='text/javascript'>parent.location.href=parent.location.href;	</script>";
	} else {
		$_SESSION['err_msg'] ="パスワードが間違っている可能性があります。";
	}
	
}

?>

<script type="text/javascript">
function checkSub() {
	if (checkEmail() == true) {
		document.register.submit();
	}
}
	
function checkEmail() {
	var emailval = document.register.new_email.value;
	//alert(emailval);

	if (emailval != "") {
		if(check(emailval) == true) {
			document.register.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function check(val) {
	var flag = 0;
	// 設定開始（チェックする項目を設定）

	if (!val.match(/.+@.+\..+/)) {
		flag = 1;
	}
	// 設定終了
	if (flag) {
		window.alert('メールアドレスが正しくありません'); // メールアドレス以外が入力された場合は警告ダイアログを表示
		return false; // 送信を中止
	}
	else {
		return true; // 送信を実行
	}
}

</script>
<style type="text/css">
	.red { color:red;}
</style>
<body>

	<section id="m-2-box">
		<h2>
		メールアドレスの変更
		</h2>

		<div id="close">
			<a  onclick="CloseFrame(9)">
				<img src="/images/cross.png">
			</a>
		</div>

		<form  name="register" action="" method="post">
		<table id="long">
			<tr id="first">
				<th>新しいアドレス</th>
				<td><input type="text" name="new_email" value="<?php echo $user_arr['mail_address']?>" /></td>
			</tr>
			<tr id="last">
				<th id="last">パスワード</th>
				<td id="last"><input type="password" name="password" /></td>
			</tr>
		</table>
		<?php
			echo '<p class="red">'.$_SESSION['err_msg'].'</p>';
			$_SESSION['err_msg'] = "";
		?>
		<div id="button">
			<button onclick="checkSub();return false;">送信する</button>
			<button onclick="CloseFrame(9)">閉じる</button>
		</div>
		</form>

	</section>


</body>