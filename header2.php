<?php require("main_read.php");?>
<?php require("main_read_af.php");?>
<!DOCTYPE html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
	<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
	
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
	<meta property="og:image" content="//storage.cloudinvoice.co.jp/images/logo.jpg" />
	<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
	<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
	<meta property="og:locale" content="ja_JP" />
	<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
	<link rel="canonical" href="//storage.cloudinvoice.co.jp/" />
	<script src="//storage.cloudinvoice.co.jp/js/jquery-2.1.1.min.js" type="text/javascript" charset="UTF-8"></script>

	<script type="text/javascript" src="//storage.cloudinvoice.co.jp/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="//storage.cloudinvoice.co.jp/js/jquery-ui-i18n.js"></script>
	<link rel="stylesheet" href="//storage.cloudinvoice.co.jp/css/jquery-ui.min.css" />

<script>

function CloseFrame(n){
	document.getElementById("fadeLayer").style.visibility = "hidden";
	document.getElementById("noneLayer").style.visibility = "hidden";
	switch(n){
		case 1 : document.getElementById("default").style.display = "none";
			break;
		case 2 :document.getElementById("report").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			break;
		case 3 : document.getElementById("Mail").style.display = "none";
			document.getElementById("Post").style.display = "none";
			document.getElementById("Issue").style.display = "none";
			document.getElementById("Delete").style.display = "none";
			break;
		case 4 : document.getElementById("souhuEditFrame").style.display = "none";
			break;
		case 5 : document.getElementById("changePass").style.display = "none";
			document.getElementById("changeAd").style.display = "none";
			document.getElementById("leave").style.display = "none";
			break;

		case 6 : document.getElementById("Freeplan").style.display = "none";
			document.getElementById("Standardplan").style.display = "none";
			document.getElementById("Premiumplan").style.display = "none";
			document.getElementById("Passerror").style.display = "none";
			break;

		case 7 : document.getElementById("Idetail").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			document.getElementById("Interval").style.display = "none";
			break;

		case 8 : document.getElementById("SouhuList").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			document.getElementById("CommonSearch").style.display = "none";
			break;

		case 9 : document.getElementById("kSouhuList").style.display = "none";
			document.getElementById("kCommonSearch").style.display = "none";
			break;

		case 10 : document.getElementById("kCommonSearch").style.display = "none";
			break;

		case 11 : document.getElementById("CommonSearch").style.display = "none";
			break;

		case 12 : document.getElementById("Transfer").style.display = "none";
			break;
	}
}

<?php
if (isset($af_id)) {
	echo "function chClient(client) {";
	echo 'document.getElementById("ch_client_form").submit();';
	echo "}";
}
?>
</script>
<style type="text/css"></style>
<link rel="stylesheet" href="../css/<?php if (isset($af_id)){ echo 'header2.css';} else { echo 'header.css';}?>" type="text/css" />

<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>




<body>






<header>
	<?php echo $af_id;?>
	<div id="ih-1-box">
		<div id="ih-1-0-box">
			<div id="ih-1-1-box">
				<?php echo $af_arr['name']."-".$user_company_name[0]['company_name'];?>（ <?php echo substr($af_arr['id'],0,2)." ".substr($af_arr['id'],2,4)." ".substr($af_arr['id'],6,4);?> - <?php echo substr($user_arr['user_id'],0,4)." ".substr($user_arr['user_id'],4,4)." ".substr($user_arr['user_id'],8,4);?>）
			</div>
			<div id="ih-1-2-box">
				<p>現在の表示アカウント：</p>
				<form id="ch_client_form" action="" method="post">
					<select name="af_client_id" onChange="chClient();">
					<?php
						for ($i = 0;$i < $af_client_num;$i++) {
							if ($af_client_arr[$i]["client_id"] == $af_client_id) {
								$af_client_selected = selected;
							} else {
								$af_client_selected = "";
							}
							echo '<option value="'.$af_client_arr[$i]["client_id"].'" '.$af_client_selected.' >'.$af_client_arr[$i]["company_name"].'</option>';
						}
					?>
					</select>
				</form>
			</div>
			<div id="ih-1-3-box" >
				<a href="//storage.cloudinvoice.co.jp/invoice/logout_af">ログアウト</a>
			</div>
		</div>
	</div>
	<div id="ih-2-box">
		<div id="ih-2-1-box">
			<div id="ih-2-1-1-box" class="ih-2-1-box">
				<a href="//storage.cloudinvoice.co.jp/invoice/main">
					<img src="/images/logo2.png"/>
				</a>
			</div>
			<div id="ih-2-1-2-box" class="ih-2-1-box">
				売上管理
				<ul id="ih-3-1-2-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/makeinvoice"><li>請求書作成</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/seikyuhakkou"><li>売上請求書管理</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/intervals"><li>定期請求書</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/seikyucsv"><li>請求CSVインポート</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/seikyuhakkoucsv"><li>請求CSVエクスポート</li></a>
				</ul>
			</div>
			<div id="ih-2-1-3-box" class="ih-2-1-box">
				支払管理
				<ul id="ih-3-1-3-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/shiharaihakkou"><li>支払請求書管理</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/ryoushuhakkou"><li>領収書・レシート管理</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/pdfconvert"><li>紙証憑のデータ化</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/shiharaicsv"><li>支払CSVエクスポート</li></a>
				</ul>
			</div>
			<div id="ih-2-1-4-box" class="ih-2-1-box">
				仕訳管理
				<ul id="ih-3-1-4-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/shiharaiautojournal"><li>自動仕訳作成</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/pastjournal"><li>仕訳帳</li></a>
				</ul>
			</div>
			<div id="ih-2-1-5-box" class="ih-2-1-box">
				分析表
				<ul id="ih-3-1-5-box" class="ih-3-box">
					<li>開発中</li>
				</ul>
			</div>
			<div id="ih-2-1-6-box" class="ih-2-1-box">
				設定
				<ul id="ih-3-1-6-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/office"><li>基本設定</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/office2"><li>帳票設定</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/addressee"><li>送付先管理</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/changeplan"><li>プラン変更</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/credit"><li>クレジットカード登録</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/user"><li>ログイン設定</li></a>
				</ul>
			</div>
			<div id="ih-2-1-7-box" class="ih-2-1-box">
				ヘルプ
				<ul id="ih-3-1-7-box" class="ih-3-box">
					<a href="../help" target="_blank"><li>ヘルプ</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/operator2" target="_blank"><li>お問い合わせ</li></a>
				</ul>
			</div>
		</div>
	</div>


</header>




