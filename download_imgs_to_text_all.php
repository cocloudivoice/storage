<?php
namespace My;

/**
 * ZipArchiveを拡張して、ディレクトリまるまる扱う機能を追加
 */
class MyZipArchive extends \ZipArchive
{
    /**
     * ディレクトリをまるごとzipファイルにします。
     *
     * @param string $dir              ディレクトリパス
     * @param string $inner_path       zipファイル中のディレクトリパス
     * @param bool   $create_empty_dir 空ディレクトリもディレクトリを作成するか
     * @return bool  処理の成否
     */
    public function addDir($dir, $inner_path, $create_empty_dir=false){
        $items = \array_diff(\scandir($dir), ['.','..']);
        $item_count = \count($items);

        if($create_empty_dir || $item_count > 0){
            $this->addEmptyDir($inner_path);
        }

        // 追加するものがないならここで終了する
        if($item_count === 0) return true;       

		
        foreach($items as $_item){ // forで行うなら$itemsは一旦array_values()を通したほうがいい
            $_path = $dir . DIRECTORY_SEPARATOR . $_item;
            $_item_inner_path = $inner_path . DIRECTORY_SEPARATOR . $_item;
            // ディレクトリの場合は再帰的に処理する
            if(\is_dir($_path)){
                $_r = \call_user_func( // "$this->addDir"より保守的に好ましい
                    [$this, __FUNCTION__], $_path, $_item_inner_path);
                if(!$_r) return false;
            }
            // ファイルの場合でかつ処理に失敗したときはこちら
            else if(!$this->addFile($_path, $_item_inner_path)
                  && !$this->on_recursive_error($dir, $inner_path, $create_empty_dir)){
                return false;
            }
            // ファイルの追加成功時は何も他には行わない
        }

        return true;
    }

    /**
     * 再帰的処理のときにエラーが生じたらどうするかを定義しています。
     *
     * @param string $parent_dir              ディレクトリパス
     * @param string $parent_inner_path       zipファイル中のディレクトリパス
     * @param bool   $create_empty_dir        空ディレクトリもディレクトリを作成するか
     * @return bool Falseならその場で中断
     */
    private function on_recursive_error($parent_dir, $parent_inner_path, $create_empty_dir){
        // 自由に定義してください
        return false;        
    }
}

$uid = "apache";
$gid = "dev";
$upload_dir_path = "";
$file_dir_path = "";
$opt = "";
$type = ".txt";

if ($_REQUEST['opt'] == "r") {
	$opt = $_REQUEST['opt'];
}
if ($_REQUEST['path'] != "") {
	$upload_dir_path = $_REQUEST['path'];
}
if ($_REQUEST['path2'] != "") {
	$file_dir_path = $_REQUEST['path2'];
}
if ($_REQUEST['type'] == "all") {
	$type = ".*";
} else if ($_REQUEST['type'] == "*") {
	$type = "";
} else if ($_REQUEST['type'] != "") {
	$type = ".".$_REQUEST['type'];
}else {
	$type = ".txt";
}

//ディレクトリが無ければ再帰的に作る。
recurse_chown_chgrp($upload_dir_path, $uid, $gid);

//Zipクラスロード
$zip = new MyZipArchive();

//Zipファイル名指定
$zipFileName = date('YmdHsi',time()).'_ImgToTextData.zip';

//Zipファイル一時保存ディレクトリ取得
$zipTmpDir = $upload_dir_path;//'/phpdocs/filelesson';

// Zipファイルのパス
$zip_file = $zipTmpDir . DIRECTORY_SEPARATOR . $zipFileName;

//Zipファイルオープン
$result = $zip->open($zip_file, \ZIPARCHIVE::CREATE | \ZIPARCHIVE::OVERWRITE);

if ($result !== true) {
    return false;
}

//処理制限時間を外す
set_time_limit(0);

$zip->addDir($upload_dir_path, 'files'.date('YmdHsi',time()), true);

$zip->close();

// ストリームに出力
header('Content-Type: application/zip; name="' . $zipFileName . '"');
header('Content-Disposition: attachment; filename="' . mb_convert_encoding($zipFileName, 'sjis-win', 'UTF-8'). '"');
header('Content-Length: '.filesize($zip_file));
echo file_get_contents($zip_file);

// 一時ファイルを削除しておく
unlink($zip_file);

if ($upload_dir_path != "" && $upload_dir_path != NULL ) {
	shell_exec("rm -f".$opt." ".$upload_dir_path."/*".$type." "."2>&1");
}
if ($file_dir_path != "" && $file_dir_path != NULL ) {
	shell_exec("rm -f".$opt." ".$file_dir_path."/*".$type." "."2>&1");
	//検索して消す。下の下位層まで消える。
	//shell_exec("find ".$file_dir_path." -type f -name '*.".$type."' | xargs rm 2>&1");
}
exit();

function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 


