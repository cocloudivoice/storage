<?php
session_start();

//必要なクラスを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

$user_arr = array();
if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = $_SESSION['user_id'];
} else {
//	header("Location:./logout");
//	exit();
}
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = "";
$mail_flag = 0;
$uid = "apache";
$gid = "dev";
$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}
//▲企業データの取得▲





if ($_FILES['upfile']) {
	
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	foreach ($file_ary as $file) {
    	//var_dump($file);
        $filename = $file['name'];
        
		//var_dump($_FILES);
		if ($file['name']) {
			$path = "../files/upload_pdf_files/";
			if ( mkdir( $path, '0770', TRUE ) ) {
				chmod( $path, 0775);
				//recurse_chown_chgrp($path, $uid, $gid);
				//chown($path, $uid);
	            chgrp($path, "dev");
			  //echo "ディレクトリ作成成功！！";
			} else {
			  //echo "ディレクトリ作成失敗！！";
			}
		}

		if (is_uploaded_file($file["tmp_name"])) {

			if (move_uploaded_file($file["tmp_name"], $path. "/" . $filename)) {
			    chmod($path. "/" . $filename, 0664);
				$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件のファイルをアップロードしました。";
				$user_arr['user_img'] = $path. "/" . $_FILES['imgurl']["name"];
				$mail_flag = 1;
			} else {
				$_SESSION['up_info_msg'] = $filename . "等のファイルをアップロードできませんでした。";
			}
		}

	}
}
if ($mail_flag == 1) {
	mb_language("ja");
	mb_internal_encoding("UTF-8");
	$email = "info@storage.cloudinvoice.co.jp";
	$header =  mb_encode_mimeheader("From:Cloud Invoice 運営",'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>";
	$subject = "SCANED PDF UPLOADED:".mb_encode_mimeheader($company_name,'ISO-2022-JP-MS');
	$message = $user_id."PDFが ".$file_num." 件アップロードされました。\n ファイル保存場所：".$path."";
	mail($email,$subject,mb_convert_encoding($message, 'ISO-2022-JP-MS'),$header);
}

	//var_dump($user_arr);
	//$result = $user_con->user_update($pdo,$user_arr);
	//var_dump($result);
if ( $_FILES['upfile']['tmp_name'] != NULL ) {
	header("Location:$ahead_url",false);
} else {
	header("Location:$return_url",false);
}

exit();

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 

?>