<?php

// *****************************************
// ワンタイムトークンの生成
// *****************************************
function create_token(){

	$ipad = getenv('REMOTE_ADDR');
	$time = time();
	$rand = mt_rand();

	// 値をハッシュ化
	$ipad = hash( 'sha256', $ipad );
	$time = hash( 'md5', $time );
	$rand = hash( 'md5', $rand );

	$no_token = $ipad.$time.$rand;

	// トークン生成
	$token = hash( 'sha256', $no_token );

	return $token;

}

// *****************************************
// トークンの半券を取得
// *****************************************
function get_harf_token(){

	$original_token = create_token();
	echo "<br/>";
	// フォームに埋め込む半券
	echo $harf = substr( $original_token, 0, 10 );
echo "<br/>";
	// オリジナルのトークンと片割れをSESSIONに保存
	$_SESSION['harf_token'] = substr( $original_token, 10 );
	$_SESSION['original_token'] = $original_token;

	return $harf;

}

// *****************************************
// トークンの照合
// *****************************************
function check_token( $harf_token ){

	// 照合用のトークン取得
	echo $ch_token = $_SESSION['original_token'];
echo "<br/>";
	// 所持していた半券とformから送信された半券を結合
	echo $token = $harf_token.$_SESSION['harf_token'];

	// 照合
	if( strcmp( $ch_token, $token ) === 0 ){

		return true;

	}

	return false;

}

?>