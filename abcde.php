<?php

//---------------------------------------------------------------------------
// imap関数利用
//---------------------------------------------------------------------------
// POP3サーバ接続
$mbox = @imap_open("{mail.co.cloudinvoice.co.jp:110/pop3/notls}INBOX", "akira.hamasaki@co.cloudinvoice.co.jp", "123456aaA");
// メール取得
if ($mbox == false) {
	echo 'メール取得に失敗しました。(POP3サーバへの接続に失敗) ';
	print_r(imap_errors());
} else {
	// 新着メッセージを取得
	// $mboxes = imap_check($mbox);
	// $mail_cnt = $mboxes->Nmsgs;
	$mboxes = imap_mailboxmsginfo($mbox);
	$mail_cnt = $mboxes->Unread;
	if ($mail_cnt > 0) {
		for ($i = 1; $i <= $mail_cnt; $i++) {
			// ヘッダ・ボディを格納
			$head = imap_header($mbox, $i);
			$body = imap_body($mbox, $i, FT_INTERNAL);
			// 本文を改行でセパレートし、配列へ格納
			$bodyMessages = split("\n", mb_convert_encoding($body, "EUC-JP", "JIS"));
			foreach ($bodyMessages as $key => $value) {
				if ($value != '' && (substr($value, 0, 8) == '【申請】')) {
					echo '['.substr($value, 8).']';
					// 既読フラグセット
					if (imap_setflag_full($mbox, $i, "\\Seen \\Flagged \\Deleted ")) {
						// 成功
					} else {
						// 失敗
						echo '既読フラグセットに失敗 ';
						print_r(imap_errors());
						break 2;
					}
					break 1;
				}
			}
		}
	} else {
		echo '新着メッセージはありません ';
	}
	// 削除用にマークされたすべてのメッセージを削除する
	imap_expunge($box);
	// POP3サーバ切断
	imap_close($mbox);
}

?>