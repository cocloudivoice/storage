<?php
session_start();

//必要なクラスを読み込む
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../cfiles/ReadCsvFile.class.php");
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//echo "a<br/>";
//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$auto_journalize_con = new auto_journalize_control();
$read_csv_con = new ReadCsvFile();

//変数の宣言
$success_num = 0;
$all_num = 0;
$insert_num = 0;
$company_num = 0;
$data_count = 0;
$update_num = 0;
$filepath = "/var/www/co.cloudinvoice.co.jp/html/pdf/nru";
$file_name = htmlspecialchars($_GET['fn'],ENT_QUOTES);
$rand_num = rand(10,99);
$journalized_code = rand(1000000000,9999999999);
//var_dump($file_name);
if ($file_name == "") {
	//$file_name = "username.csv";
}
//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$all_num = count($csv) - 1;

if ($all_num != 0) {
	//var_dump($csv);
	//ob_start();
	//CSVデータを一行ずつ取り出してDBに登録する。
	foreach ($csv as $value) {
		
		//変数の初期化
		$mail_address = "";
		
		$data_count++;
		
		if ($value[0] == "電話") {
			
			//カラム名から列番号を取得する
			for ($k = 0; $k < 25; $k++) {
				switch ($value[$k]) {
					case "電話":
						$phn_num = $k;
						break;
					case "補助":
						$prd_num = $k;
						break;
					case "日付":
						$dat_num = $k;
						break;
		   			case "金額":
						$mon_num = $k;
						break;
					case "消費税":
						$stx_num = $k;
						break;
					case "源泉税":
						$wht_num = $k;
						break;
					case "請求者":
						$com_num = $k;
						break;
					case "ファイル名2":
						$fil_num = $k;
						break;
					case "摘要":
						$rem_num = $k;
						break;
					case "書類タイプ":
						//1:
						$doc_num = $k;
						break;
					case "支払者":
						$des_num = $k;
						break;
					case "請求書番号":
						$icd_num = $k;
						break;
					case "請求者フラグ":
						//非正規フラグ 0:正規 1:非正規 2:非正規（検索非表示）
						$inv_num = $k;
						break;
					case "支払者フラグ":
						//非正規フラグ 0:正規 1:非正規 2:非正規（検索非表示）
						$pay_num = $k;
						break;
					case "売上支払":
						//売上支払data_entry_flag 1:売上側に出る 2:支払側に出る 3:どっちにも出ない（資金移動）
						$pl_num = $k;
						break;
					default :
						break;
				}
			}
		} else {
			//存在フラグの初期化
			$exist_flag = 0;
			
			echo "請求：".$value[$com_num];echo "<br>";
			echo "被請求：".$value[$des_num];echo "<br>";
			echo $value[$pl_num];echo "<br>";echo "<br>";
			
			//$journalized_code = $rand_num;//date("YmdHis").$data_count;
			$value[0];//1行目でユーザーの共通コード取得
			$phone_number = $value[$phn_num];
			$user_name = $value[$com_num];
			$paid_date = date('Y-m-d',strtotime($value[$dat_num]));
			$pay_date = date('Ymd',strtotime($value[$dat_num]));
			$total_price = $value[$mon_num];
			$sales_tax = $value[$stx_num];
			if ($value[$wht_num] == ""){
				$withholding_tax = 0;
			} else {
				$withholding_tax = $value[$wht_num];
			}
			$product_name = $value[$prd_num];
			$dlp = $download_password = $value[$fil_num];
			//書類タイプ
			if ($value[$doc_num] == "") {
				$paper_type = 0;
			} else {
				$paper_type = $value[$doc_num];
			}
			
			//請求番号
			if ($value[$icd_num] == "") {
				$invoice_code = "";
			} else {
				$invoice_code = $value[$icd_num];
			}
			
			//請求者フラグ
			if ($value[$inv_num] == "") {
				$non_regular_flag = 1;
			} else {
				$non_regular_flag = $value[$inv_num];
			}
			//支払者フラグ
			if ($value[$pay_num] == "") {
				$payer_non_regular_flag = 1;
			} else {
				$payer_non_regular_flag = $value[$pay_num];
			}

			if ($value[$pl_num] == "") {
				$data_entry_flag = 2;
			} else {
				$data_entry_flag = $value[$pl_num];
			}
			$dest_com_name = $value[$des_num];
			
			if ( $dest_com_name * 1 > 0) {
				echo "（支払者）共通コード照合<br/>";
				$destination_company_id = $dest_com_name;
				//企業データ存在確認（元側）▼
				$flag = "company_data";
				$words = "";
				$destination_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_company_id,$words);
				$destination_company_id = $destination_company_data_arr[0]['company_id'];
			} else {
				echo "（支払者）名前照合<br/>";
				$destination_company_name = $dest_com_name;
				//企業データ存在確認（元側）▼
				$flag = "company_data_from_name";
				$words = "";
				$destination_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_company_name,$words);
				$destination_company_id = $destination_company_data_arr[0]['company_id'];
				//var_dump();
			}
			
			//支払者の企業が存在しない時の処理
			if ($destination_company_id == "" || $destination_company_id == NULL) {
				echo "支払者の企業".$dest_com_name."が存在しない時の処理<br/>\r\n";

				//仮メールアドレス生成
				$temp_mail_address = md5(random(12)).rand(4)."@user.co.cloudinvoice.co.jp";
				$user_con = new user_control();
				
				$user_dest_arr = $user_con -> user_select($pdo,$temp_mail_address);

				//Emailアドレスの存在を確認する。
				if ($user_dest_arr['mail_address'] != NULL) {
					$temp_mail_address = md5(random(12)).rand(4)."@user.co.cloudinvoice.co.jp";
				}

				//ユーザー登録
				$user_dest_arr['nick_name']   = $dest_com_name;
				$user_dest_arr['mail_address'] = $temp_mail_address;
				$user_dest_arr['tel_num'] = $dest_phone_number;
				$user_dest_arr['non_regular_flag'] = $payer_non_regular_flag;
				//$passwordmaker = random(12);
				//$passwordmaker = uniqid();
				$passwordmaker = "1234abcd";
				$user_dest_arr['password'] = md5($passwordmaker);

				
				//共通コードの生成と取得
				if (!$user_dest_arr['user_id']){
					$dest_user_num = $user_con -> user_get_new_num($pdo);
					$user_dest_arr['user_id'] = $dest_user_num;
				}
				$result_dest = $user_con -> user_insert($pdo,$user_dest_arr);

				//企業データ登録▼
				$company_dest_id = "".$user_dest_arr['user_id'];
				$sql = "
				INSERT INTO `COMPANY_TABLE` ( 
					`company_id`, `company_name`, `email`, `tel_num` , `non_regular_flag`, `password`
				) VALUES ( 
					".$user_dest_arr['user_id'].", '".$user_dest_arr['nick_name']."','".$user_dest_arr['mail_address']."','".$user_dest_arr['tel_num']."',".$user_dest_arr['non_regular_flag']."','".$user_dest_arr['password']."
				)";
				$company_dest_arr = $company_con -> company_sql($pdo,$company_dest_id,$sql);
				//企業データ登録▲
				
				//▼自動仕訳用企業DB登録▼
				//企業品目テーブルと企業用レコードテーブルを登録
				$table_dest_name = "".sprintf("%012d", $company_dest_id);
				try {
					$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_dest_name);
					$company_num++;
				} catch (PDOException $e) {
				    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
				    $_SESSION['error_msg'] = $e -> getMessage();
				}
				//▲自動仕訳用企業DB登録▲
				
			}
			//請求書番号が入っていない時には請求書番号を生成して入れる
			if ($invoice_code == "") {
				$invoice_code = "90".$pay_date.$rand_num.$data_count;
			}
			//echo "<br/>\r\n";

			//請求者データの存在確認▼
			//電話番号で存在確認
			if ($phone_number != "") {

				$company_id = "".$user_arr['user_id'];
				$flag = "company_data_from_tel";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$phone_number,$words);
				//var_dump($company_data_arr);
				if (isset($company_data_arr[0]['tel_num'])) {
					//echo "TELあり<br/>\r\n";
					$exist_flag = 1;
				}// else{echo "TELなし<br/>\r\n";}
			}
			//電話番号が無かった場合、企業名で存在確認
			if($exist_flag == 0) {
				$flag = "company_data_from_name";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$user_name,$words);
				//var_dump($company_data_arr);
				if (isset($company_data_arr[0]['company_name'])) {
					//echo "名前あり<br/>\r\n";
					$exist_flag = 1;
				}// else {echo "名前なし<br/>\r\n";}
			}
			//企業データ存在確認▲

			if ($exist_flag == 0) {
				echo "支払先の企業が存在しない時の処理<br/>\r\n";

				//仮メールアドレス生成
				$mail_address = md5(random(12)).rand(4)."@user.co.cloudinvoice.co.jp";
				$user_con = new user_control();
				//Emailアドレスの存在を確認する。		
				$login_arr = $user_con -> user_select_email($pdo,$mail_address);
				if ($login_arr != NULL) {
					$login_arr = $user_con -> user_select_email($pdo,$mail_address);
					//$passwordmaker = random(12);
					//$passwordmaker = uniqid();
					$passwordmaker = "1234abcd";
					$login_arr['password'] = md5($passwordmaker);
					//$login_arr['nick_name'] = $user_name;
					//$user_con -> user_update($pdo,$login_arr);
					$update_data = " `password` = '".$login_arr['password']."' ";
					$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

					$login_arr = $user_con -> user_select_email($pdo,$mail_address);
				}
				
				$user_arr = $user_con -> user_select($pdo,$mail_address);

				//ユーザー登録
				$user_arr['nick_name']   = $user_name;
				$user_arr['mail_address'] = $mail_address;
				$user_arr['tel_num'] = $phone_number;
				$user_arr['non_regular_flag'] = $non_regular_flag;
				
				//共通コードの生成と取得
				if (!$user_arr['user_id']){
					$user_num = $user_con -> user_get_new_num($pdo);
					$user_arr['user_id'] = $user_num;
				}
				$result = $user_con -> user_insert($pdo,$user_arr);

				//企業データ登録▼
				$company_id = "".$user_arr['user_id'];
				$sql = "
				INSERT INTO `COMPANY_TABLE` ( 
					`company_id`, `company_name`, `email`, `tel_num` , `non_regular_flag`
				) VALUES ( 
					".$user_arr['user_id'].", '".$user_arr['nick_name']."','".$user_arr['mail_address']."','".$user_arr['tel_num']."','".$user_arr['non_regular_flag']."' 
				)";
				$company_arr = $company_con -> company_sql($pdo,$company_id,$sql);
				//企業データ登録▲
				
				//▼自動仕訳用企業DB登録▼
				//企業品目テーブルと企業用レコードテーブルを登録
				$table_name = "".sprintf("%012d", $company_id);
				try {
					$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
					$company_num++;
				} catch (PDOException $e) {
				    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
				    $_SESSION['error_msg'] = $e -> getMessage();
				}
				//▲自動仕訳用企業DB登録▲

			} else {
				//企業アカウントが存在する時
				$company_id = $company_data_arr[0]['company_id'];
				//echo "<br/>\r\n";
			}

			//請求書・領収書データの登録
			$path = "/var/www/co.cloudinvoice.co.jp/html/files/".sprintf("%012d", $company_id)."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
		
		echo	$pdf_url = "/var/www/co.cloudinvoice.co.jp/html/files/".sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".pdf";
		echo "<br/>\r\n";
		echo	$png_url = "/var/www/co.cloudinvoice.co.jp/html/files/".sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".png";
		echo "<br/>\r\n";
			//echo "<br/>\r\n";
			//共通コード+請求書番号で請求書データの存在を確認する
			//2行目以降があるデータが入らないのでは？？？？20150417はまさき
			$sql = "SELECT * FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$company_id." AND `invoice_code` = '".$invoice_code."' ";
			$update_flag_arr = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
			//var_dump($update_flag_arr);
			//echo "<br/><br/>\r\n";
			//echo $update_flag_arr[0]['invoice_code'];
			//echo "<br/><br/>\r\n";
			//同一請求書番号のデータが存在する時は処理をしない
//			if (!isset($update_flag_arr[0]['invoice_code']) ) {
				
				$sql = "
				INSERT INTO `INVOICE_DATA_TABLE`(
				`claimant_id`, `destination_id`, `billing_date`,
				`invoice_code`, `invoice_name`, `pay_date`,
				`product_name`, `unit_price`, `quantity`,
				`total_price_excluding_tax`,`sales_tax`, `withholding_tax`, `total_price`,
				`remarks2`, `paid_date`,`journalized_code`,`status`, `download_password`,
				`template_type`,`paper_type`,`data_entry_flag`,`pdf_url`, `insert_date`
				) VALUES (
				".$company_id.",".$destination_company_id.",".$pay_date.",
				'".$invoice_code."','".$product_name."',".$pay_date.",
				'".$product_name."',".($total_price - $sales_tax).",1,
				".($total_price - $sales_tax).",'".$sales_tax."','".$withholding_tax."',".$total_price.",
				'".$product_name."','".$paid_date."','".$journalized_code."',9,'".$download_password."',
				1,".$paper_type.",'".$data_entry_flag."','".$path."','".date('Y-m-d H:i:s')."');
				";
				//画像のアップロードのみ行う場合はここをコメントアウトする。
				$result = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
				//var_dump($result);
				$insert_num++;
				echo "新規挿入処理<br/>\r\n";
/*
			} else {
				$update_num++;
				echo "上書き処理".$update_num;
				echo "<br/>\r\n";
			}
*/
			//var_dump($result);
			
			
			//証憑ファイルの保存		
			if (!file_exists($pdf_url)) {
				//サーバーにPDFファイルが存在しない場合
				//PDFファイルを保存する
				$pdf_file = ''.$dlp.'.pdf';
				//請求元企業のディレクトリに入る
				try {

					if (mkdir( $path, 0775, true)) {
						chmod( $path, 0775, true );
						chgrp($path, "dev", true );
						echo "ディレクトリ作成成功！！";
					} else {
						echo "ディレクトリ作成失敗！！";
					}

				} catch (Exception $e) { 
					echo $e -> getMessage;
				}
			}
			if (!file_exists($png_url)) {
				try {
					echo "PDF、PNG移動";
					echo "<br/>\r\n";
					// ファイルの存在確認
					//PNGファイルを保存
					//削除用
					
					//	echo $path2 = "/var/www/co.cloudinvoice.co.jp/html/files/".$rm_data;
					//	echo "<br/>\r\n";
					//	shell_exec("rm -Rf ".$path2);
					//	shell_exec("rm -Rf ".$path2);
					$pdftoimg_con = new PdfToImg();
					$pdftoimg_con -> moveImage($path,$dlp);

				} catch (Exception $e) {
					echo $e -> getMessage;
				}
			}

		}
	}
}
echo "<br/>\r\n";
echo "新規企業作成件数:".$company_num;
echo "<br/>\r\n";
echo "新規データ作成件数:".$insert_num;
echo "<br/>\r\n";
echo "データ上書き件数:".$update_num;
echo "データ件数：".($data_count - 1);
echo "<br/>\r\n";
fclose($temp);
unlink($file);

//ob_end_flush();

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>非正規ユーザーアップロード完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">非正規ユーザーアップロード完了</h1>
		<h2>非正規ユーザーの登録が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./nruser_upload'">非正規ユーザーのアップロード画面に戻る</button>
		<button onclick="window.location.href='./record_table_upload'">仕訳アップロード画面に移動</button>
	</div>
</body>
</html>