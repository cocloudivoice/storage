<?php session_start(); ?>
<?php require("header.php");?>
<?php


//必要なクラスファイルを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if(isset($_SESSION['user_id'])){
	$user_id = $company_id = $_SESSION['user_id'];
}else{
	header("Location:./logout.php");
	exit();
}

$flag = "company_data";
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();
$company_arr  = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();
if (isset($_REQUEST['company_name'])) {

	if ($_REQUEST['change_place'] == 1) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`segment`=".$_REQUEST['segment'].",`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',
		`fax_num`='".$_REQUEST['fax_num']."',`url`='".$_REQUEST['url']."' 
		 WHERE company_id = ".$company_id."";
	} else if ($_REQUEST['change_place'] == 2) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`email`='".$_REQUEST['email']."',`section`='".$_REQUEST['section']."',
		`clerk`='".$_REQUEST['clerk']."',`bank_id`=".$_REQUEST['bank_id'].",
		`blanch_id`=".$_REQUEST['blanch_id'].",`account_devision`=".$_REQUEST['account_devision'].",
		`account_num`=".$_REQUEST['account_num'].",`account_info`='".$_REQUEST['account_info']."',
		`stamp_image`='".$_REQUEST['stamp_image']."',`logotype`='".$_REQUEST['logotype']."',
		`remarks1`='".$_REQUEST['remarks1']."',`remarks2`='".$_REQUEST['remarks2']."' 
		 WHERE company_id = ".$company_id."";
	}
	$company_con -> company_sql($pdo,$company_id,$sql);

}

//設定変更の処理▲

//請求書テンプレート変更の処理▼
if (isset($_REQUEST['temp_type_flag'])) {
	$temp_type = htmlspecialchars($_REQUEST['temp_type_flag'],ENT_QUOTES);
	$flag = "image_upload";
	$conditions = " `template_type` = ".$temp_type." ";
	$company_con -> company_sql_flag($pdo,$flag,$company_id,$conditions);
}
//請求書テンプレート変更の処理▲

//ユーザー企業の情報を取得する。
$flag = "company_data";
$words = "";
$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$flag="";
$words="";
$template_type = $company_one_arr[0]['template_type'];
//var_dump($company_one_arr[0]['template_type']);

//請求書データのテンプレートタイプを取得
if (isset($invoice_data_receive_arr[0]["template_type"])) {
	$template_type = $invoice_data_receive_arr[0]["template_type"];
}



?>


<script>

function OpenDefaultFrame(){
	document.getElementById("default").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenChangeInvoiceFrame(){
	window.scrollTo(0,0);
	document.getElementById("ChangeInvoice").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenReportFrame(){
	window.scrollTo(0,0);
	document.getElementById("report").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}

//請求書テンプレートの切り替えのためのページ読み込み
function changeTemplate(n) {
	location.href = "../office2?temp_type_flag="+ n +"";
}


</script>
<title>帳票設定 - Cloud Invoice</title>

<iframe id="ChangeInvoice" src="./frame/ChangeInvoice" name="invoice1"></iframe>
<iframe id="report" src="./frame/report"></iframe>


<article>

	<section id="m-2-box">
		<h2>
			テンプレート設定
		</h2>
		
		<table>
			<tr>
				<th id="invoice">請求書</th>
				<td id="invoice" onclick="OpenChangeInvoiceFrame()" style="cursor:pointer;"><img src="../images/invoice<?php echo $template_type;?>.png"></img></td>
			</tr>
		</table>
		<button onclick="OpenChangeInvoiceFrame()">変更する</button>
		
		<div id="clear"></div>
	</section>


	<section id="m-2-box">
		<h2>
			帳票設定
		</h2>
		
		<table>
			<tr>
				<th>口座名義</th>
				<td><?php echo $company_arr[0]['account_name'];?></td>
			</tr>
			<tr>
				<th>帳票口座１</th>
				<td><?php echo nl2br($company_arr[0]['account_info']);?></td>
			</tr>
			<tr>
				<th>ロゴ</th>
				<td>
					<?php 
						if ($company_arr[0]['logotype'] != NULL) { echo '<img src="'.$company_arr[0]['logotype'].'" alt="ロゴ" style="max-width:200px;max-height:100px;" />';}
					?>
				</td>
			</tr>
			<tr>
				<th>印影</th>
				<td>
					<?php 
						if ($company_arr[0]['stamp_image'] != NULL) { echo '<img src="'.$company_arr[0]['stamp_image'].'" alt="印影" style="max-width:60px;max-height:60px;" />';}
					?>
				</td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td><?php echo $company_arr[0]['email'];?></td>
			</tr>
			<tr>
				<th>部署</th>
				<td><?php echo $company_arr[0]['section'];?></td>
			</tr>
			<tr>
				<th>担当者</th>
				<td><?php echo $company_arr[0]['clerk'];?></td>
			</tr>
			<tr>
				<th>備考</th>
				<td><?php echo $company_arr[0]['remarks1'];?></td>
			</tr>
				
		</table>
		<button onclick="OpenReportFrame()">変更する</button>
		
		<div id="clear"></div>
	</section>

</article>
<?php require("footer.php");?>
<div id="fadeLayer" onclick="CloseFrame(2)"></div>
<div id="noneLayer"></div>