<?php	
session_start();
/*
if(isset($_SESSION['user_id'])){
	header("Location:./logout.php");
}
*/
//$_SESSION['user_id']="";
//session_id();

if(isset($_SESSION['check'])){
	$passErr = $_SESSION['check'];
}
if(isset($_SESSION['emailcheck'])){
	$noEmailErr = $_SESSION['emailcheck'];
}
if(isset($_SESSION['duplicationError'])){
	$duplicationEmailErr = $_SESSION['duplicationError'];
}

if(isset($_SESSION['em'])){
	$email_address = $_SESSION['em'];
}
//var_dump($_SESSION);

?>

<!DOCTYPE html>

<html Lang="ja">
<meta>


<head>

        <title>会員登録フォーム - Cloud Invoice</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
	    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
        <!--<link rel="stylesheet" type="text/css" href="css/style.css" />-->
		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/ChunkFive_400.font.js" type="text/javascript"></script>

<script type="text/javascript">

function checkSub() {
	var	pass1 = document.register.login_password.value;
	var pass2 = document.register.login_password2.value;
	var name = document.register.company_name.value;
	var kana = document.register.kana.value;
	var email = document.register.e_mail.value;
	if (pass1 != "" && pass2 !="" && name != "" && email !="") {
		if (pass1 === pass2) {
			var n = $('#chksub:checked').length;
			if (n == 1){
				if (checkEmail() == true) {
					//document.register.submit();
				}
			} else {
				alert("利用規約とプライバシーポリシーの同意にチェックが入っていません。");
				return false;
			}
		} else {
			alert("パスワードが一致しません。");
			return false;
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function checkEmail() {
	var emailval = document.register.e_mail.value;
	//alert(emailval);

	if (emailval != "") {
		if(check(emailval) == true) {
			document.register.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function check(val) {
	var flag = 0;
	// 設定開始（チェックする項目を設定）

	if (!val.match(/.+@.+\..+/)) {
		flag = 1;
	}
	// 設定終了
	if (flag) {
		window.alert('メールアドレスが正しくありません'); // メールアドレス以外が入力された場合は警告ダイアログを表示
		return false; // 送信を中止
	}
	else {
		return true; // 送信を実行
	}
}

</script>

<style type="text/css">


html *{margin:0px; padding:0px;}
body {font: 14px/20px 'メイリオ','Hiragino Kaku Gothic Pro','sans-serif';}
a {text-decoration:none; color:blue}
a:hover{text-decoration:underline; color:rgb(50,180,240);}
button{cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}

div#clear{clear:both;}

header{width:510px; margin:10px auto 0 auto;}
header h1{float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header h1 a ,header h1 a:visited {float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header ul{text-align:right; line-height:35px; margin:0 10px 0 0; font-size:13px;}
header li{display:inline;}



article{width:510px; height:400px; margin:20px auto 0 auto; border-color:blue; border-radius:5px; border-width:1px; border-style:solid; border-color:rgb(180,210,210)}
article h2{margin:12px 0 0 20px; color:rgb(50,50,50); font-size:18px; color:rgb(240,132,0)}
article p{margin:30px 0 0 20px; font-weight:bold; color:rgb(255,120,0);}

div#m-1-box{margin:15px 0 0 0;}
div#m-1-box h3{font-size:14px; float:left; width:220px; text-align:right; line-height:19px;}
div#m-1-box input[type=text]{width:210px; height:17px; margin:0 0 0 3px;}
div#m-1-box input[type=pass]{width:140px; height:17px; margin:0 0 0 3px;}

div#m-2-box{margin:30px 0 0 90px;}
div#m-2-box input[type=checkbox]{float:left; margin:4px 3px 0 0; cursor:pointer;}

article button[type=nyukai]{border:none; width:160px; height:30px; margin:20px 0 0 80px; background:linear-gradient(rgb(80,230,230),rgb(60,200,200)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer;}
article button[type=nyukai]:hover{background:linear-gradient(rgb(95,235,235),rgb(70,212,212));}



footer{width:510px; margin:10px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px;}

span.error{	display:none;	visibility:hidden;	color:red;	font-size:11px;	font-style:italic;}
.right{	float:right;}


</style>



</head>




<body>



<?php include_once("./header.html"); ?>



<article>
	
	<h2>
		アカウント作成
	</h2><a href="./signin.php" rel="login" class="linkform right">すでにアカウント作成済みの方はこちら</a>
	<p>
		アカウントの作成に必要な情報を正しく入力してください。
	</p>
	<?php  if(isset($duplicationEmailErr)){echo '<p style="text-align:center;color:red;">'.$duplicationEmailErr.'</p>';} ?>
	<div id="form_wrapper" class="form_wrapper">
	<form class="register" name="register" method="post" action="./signup_2.php">

	<div id="m-1-box">
		<h3>
			事業所名：
		</h3>
			<INPUT type="text" name="company_name" />
			<span class="error">入力が不正です</span>
	</div>
	
	<div id="m-1-box">
		<h3>
			フリガナ：
		</h3>
			<INPUT type="text" name="kana" />
	</div>
	
	
	<p>
		請求情報はパスワードで保護されます。
	</p>
	
	<div id="m-1-box">
		<h3>
			パスワードを入力してください：
		</h3>
			<INPUT type="password" name="login_password"  oncopy="return false" onpaste="return false" oncontextmenu="return false" />
			<span class="error">入力が不正です</span>
	</div>
	
	<div id="m-1-box">
		<h3>
			もう一度入力してください：
		</h3>
			<INPUT type="password" name="login_password2"  oncopy="return false" onpaste="return false" oncontextmenu="return false" />
			<span class="error">入力が不正です</span>
	</div>
	
	<div id="m-2-box">
			<input id="chksub" type="checkbox" name="personal_data" value="1">
			<a href="/terms">利用規約</a>及び<a href="/privacy">プライバシーポリシー</a>に同意します。
<!--
			<input type="button" value="ユーザー登録"  onclick="checkSub();return false;" />
-->
		<button type="nyukai" onclick="checkSub();return false;" >アカウントの作成</button>
	</div>
	</form>
</article>



<?php include_once("./footer.html"); ?>
	
</body>
<?php 
	$_SESSION['check'] = "";
	//$_SESSION['em'] = "";
?>