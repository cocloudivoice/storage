print "Hello World!"

import pandas as pd

filename = "JDL出納帳-xxxx-xxxx-仕訳.csv"
df = pd.read_csv(filename, encoding="Shift-JIS", skiprows=3)

columns = ["摘要", "借方科目", "借方科目正式名称"]
df_counts = df[columns].dropna()


"""
from janome.tokenizer import Tokenizer

t = Tokenizer()

notes = []
for ix in df_counts.index:
    note = df_counts.ix[ix,"摘要"]
    tokens = t.tokenize(note.replace('　',' '))
    words = ""
    for token in tokens:
        words += " " + token.surface
    notes.append(words.replace(' \u3000', ''))
"""

from sklearn.feature_extraction.text import TfidfVectorizer

vect = TfidfVectorizer()
vect.fit(notes)

X = vect.transform(notes)

y = df_counts.借方科目.as_matrix().astype("int").flatten()

from sklearn import cross_validation

test_size = 0.2
X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=test_size)


from sklearn.svm import LinearSVC

clf = LinearSVC(C=120.0, random_state=42)
clf.fit(X_train, y_train)

clf.score(X_test, y_test)

