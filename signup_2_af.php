<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/accountfirmcontrol.class.php');

//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();
$af_con = new account_firm_control();

$mail_address = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$af_check_arr = $af_con -> af_select($pdo,$mail_address);

$af_num = 0;
$af_arr = array();

//メールアドレス入力確認
if ($mail_address == NULL || $mail_address == "") {
	$_SESSION['duplicationError'] = "メールアドレスが確認できません。もう一度お試しください。";
	header("Location:./signup_af.php");
	exit();
}

//メールアドレス重複確認
if ($af_check_arr["e_mail"] != NULL || $af_check_arr["e_mail"] != "") {
	$_SESSION['duplicationError'] = "同じメールアドレスで登録されたアカウントがあります。";
	header("Location:./signup_af.php");
	exit();
}

//ユーザー登録
$af_arr['e_mail'] = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$af_arr['name'] = htmlspecialchars($_REQUEST['name'],ENT_QUOTES);
$af_arr['zip_code'] = htmlspecialchars($_REQUEST['zip_code'],ENT_QUOTES);
$af_arr['address1'] = htmlspecialchars($_REQUEST['address1'],ENT_QUOTES);
$af_arr['address2'] = htmlspecialchars($_REQUEST['address2'],ENT_QUOTES);
$af_arr['regist_date'] = strftime("%Y/%m/%d %H:%M:%S", time());
$af_num = $af_con -> af_get_new_num($pdo);
$af_arr['id'] = $af_num;

//パスワード作成
//$passwordmaker = random(12);
$passwordmaker = uniqid().random(13);
$af_arr['password'] = md5($passwordmaker);

/*
$sql = "";
$result = $af_con -> af_sql($pdo,$sql,$e_mail);
var_dump($result);
*/
//会計事務所データ登録▼
$accounting_firm_id = $af_arr['id'];
$af_con -> af_insert($pdo,$af_arr);

//Emailアドレスの存在を確認する。
$e_mail = $af_arr['e_mail'];
$af_arr = $af_con -> af_select_email($pdo,$e_mail);
$_SESSION['af_id'] = $af_arr['id'];
if (isset($af_arr['id'])) {
	mb_language("ja");
	mb_internal_encoding("UTF-8");
	$newpassword = $passwordmaker;

	$header = "From:".mb_encode_mimeheader("Cloud Invoice 運営 " ,"ISO-2022-JP-MS")."<info@storage.cloudinvoice.co.jp>";
	$subject = "登録及び仮パスワード発行通知" ;
	$message ="
{$af_arr['name']}様

ご登録ありがとうございます。
仮パスワードを発行いたしました。
仮パスワードは、下記の通りです。

御社の事務所コード：".$af_arr['id']."
ID(E-mailアドレス):".$af_arr['e_mail']."
パスワード:".$newpassword."
サイトURL:http://storage.cloudinvoice.co.jp/invoice/signin_af

ご登録のE-mailアドレスと仮パスワードでログインしてから、
パスワードの変更及び企業情報のご登録をお願いいたします。

－－－－変更方法
パスワードの変更は、設定マーク→【ログイン設定】→パスワード：変更
会計事務所情報の登録は、【設定】→【事業所登録】
－－－－

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数
ではございますが、運営(info@cloudinvoice.co.jp)まで
メールをいただけますでしょうか。

よろしくお願いいたします。

Cloud Invoice運営
";

	mail($e_mail,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$header);
	mail_to_master($e_mail,$af_arr['name']);
	//mail("sign_up_user@cloudinvoice.co.jp",mb_encode_mimeheader('【通知】ユーザー登録', 'ISO-2022-JP-MS'),mb_convert_encoding('ユーザーが登録されました。'.$e_mail.'', 'ISO-2022-JP-MS'),$header);

} else {
	
	//メールアドレス入力確認
	$_SESSION['duplicationError'] = "メールアドレスが重複している可能性があります。もう一度お試しください。";
	header("Location:./signup_af.php");
	exit();
}

function mail_to_master($mail,$name) {
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    $message ="新しく登録された会計事務所ユーザーの情報は次の通りです。\n";
    $message.="────────────────────────────────────\n";
    $message.= $name."\n".$mail."\n\n";
    $message.="DATE: ".date("Y/m/d - H:i:s")."\n";
    $message.="IP: ".$_SERVER['REMOTE_ADDR']."\n";
    $message.="HOST: ".@gethostbyaddr($_SERVER['REMOTE_ADDR'])."\n";
    $message.="USER AGENT: ".$_SERVER['HTTP_USER_AGENT']."\n";
    $message.="────────────────────────────────────\n";
   $headers = mb_encode_mimeheader("From: CloudInvoice",'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>";
   $subject = "会計事務所登録通知 N";
   
   mail("sign_up_user@storage.cloudinvoice.co.jp",mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),$headers);
}

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

//echo "result=".$result."\n";

//****メールでログインを促す。****//
/*E-mailアドレスのみで登録し、仮パスワードを発行してログインする仕組みに変更のためコメントアウト

//メールの設定
$to = $mail_address;
mb_language("ja");
mb_internal_encoding("UTF-8");
//$headers = 'From: Cloud Invoice運営 <info@storage.cloudinvoice.co.jp>' . "\r\n" .'Reply-To: Cloud Invoice運営<info@storage.cloudinvoice.co.jp>' . "\r\n";
$headers = "From: ".mb_encode_mimeheader("Cloud Invoice 運営 " ,'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>\r\n";
$subject = "ご登録ありがとうございます。－ Cloud Invoice 運営" ;
$message ="

このたびはCloud Invoiceにご登録ありがとうございます。

会員登録が完了いたしました。
下記URLからログインしてください。

ID:".$to."
パスワード:登録時に設定したパスワードです。

http://storage.cloudinvoice.co.jp/invoice/signin

Cloud Invoice運営
";
	mail($to,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$headers,"Content-Type: text/html; charset=\"ISO-2022-JP\";\n");
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会計事務所会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仮パスワードを発行しました。</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">会計事務所ユーザー仮登録完了</h1>
		<h2>会計事務所ユーザー仮登録が完了しました。</h2><br/>
		<p>ご登録のメールアドレスに送付いたしました仮パスワードで<br/>
		ログインして認証を完了してください。
		</p>
		<button onclick="window.location.href='//storage.cloudinvoice.co.jp/invoice/signin_af'">Cloud Invoiceのログイン画面へ</button>
	</div>
</body>
</html>