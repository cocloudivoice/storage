<?php

class MailSender {
	
		function MailSender() {
			//require_once('/var/www/inc/shopping/crawler/common/GetPriceCommonTool.class.php');
			//$this->tool = new GetPriceCommonTool("mailsender", $this->shop_id);
		}

	//$reportdata = $ctrler->execute();

	//ここからMailSender
	//error_reporting(E_ALL);

	//$_SERVER['HIKAKU_DEBUG'] = 1; // dev only
	//require_once('/var/www/inc/Memcached.class.php');
	//require_once('HTTP/Request.php');

	//$cache = new Memcached();

	//require_once('/var/www/inc/shopping/components/BatchManager.class.php');
	//$batchMgr = new BatchManager();
	//$_jobId = '117';//JobID

	// バッチ稼動状況テーブルにstart発行
	//$batchMgr->setStartStatus($_jobId);

	function sendMail($sender,$mail_title,$message_file_path,$email_list_path) {

		//echo $operation;
		
		if (isset($sender)) {
			$from = $sender;
		} else {
			$from = "info@co.cloudinvoice.co.jp";
		}
		
		//メールのタイトル設定
		if ($mail_title == NULL) { 
			$mail_title = 'クラウドインボイスからメールです';//date('Y年m月d日').'の報告-クラウドインボイス';
		}
		
		//メッセージの内容設定
		if (isset($message_file_path)) {
			//メッセージファイルのパスが存在したらメッセージの内容を上書きする
			$operation = file_get_contents($message_file_path, true);
			
		} else {

$operation  = "
内容

ここまで";

		}
		
		//メールの送り先設定
		if (isset($email_list_path)) {
			//emailリストのパスが存在したらメールアドレスリストを読み込む
			$fp = fopen($email_list_path, 'r');

			$count = 0;

			if ($fp){
				if (flock($fp, LOCK_SH)){
					while (!feof($fp)) {
						$to = fgets($fp);
						$send_result = $this -> info_mail($mail_title, $operation, $from, $to);
						$count++;
					}
			        
					print($count.'件送信しました。<br>');
					flock($fp, LOCK_UN);
			    
				}else{
					print('ファイルロックに失敗しました');
				}
			}
		
		} else {
			$to = 'akira.hamasaki@co.cloudinvoice.co.jp';
			$send_result = $this -> info_mail($mail_title, $operation, $from, $to);
		}
		
		//$action = 'RESULT';
		//info_log($operation, $action);

		// result発行
		//$batchMgr->setResultStatus( $_jobId, array("status" => "success") );
		if ($send_result == true) {
			$this -> info_log($operation, 'RESULT', $from, $to);
		} else {
			$this -> info_log('送信に失敗しました。', 'RESULT', $from, $to);
		}
		fclose($fp);
	}

	function info_mail($title, $message, $from, $to) {
	    if (isset($_SERVER['HOSTNAME']) && $_SERVER['HOSTNAME'] == 'co.cloudinvoice.co.jp') {
	        //$to = 'akira.hamasaki@co.cloudinvoice.co.jp';
	    }
	    else {
	        //$to = 'akira.hamasaki@cluodinvoice.co.jp shun.ueda@co.cloudinvoice.co.jp taiji.nakamaru@co.cloudinvoice.co.jp';
	        //$to = 'akira.hamasaki@co.cloudinvoice.co.jp,ryobita@yahoo.co.jp,ryobitan@yahoo.co.jp,aquirax121@wcm.ne.jp,info@co.cloudinvoice.co.jp';
	    	//$to = 'akira.hamasaki@co.cloudinvoice.co.jp';
	    }
	    $mailer = new Mailer();
	    $result = $mailer -> send(array(
	        'to'      => $to,
	        'from'    => $from,//'taiji.nakamaru@co.cloudinvoice.co.jp',
	        'subject' => $title,
	        'body'    => $message
	    ));
	    return $result;
	}
	/*
	function httpRequestCached($req, $url, &$cache){
	    echo "request for $url\n";
	    $key = "http:cache:" . $url;
	    $expire = 3600;
	    $val = $cache->get($key);
	    //var_dump($val);
	    if(!$val){
	        $rand = rand(3,7);
	        echo "waiting for {$rand} seconds ";
	        for($i=0;$i<$rand;$i++){
	            echo ">";
	            sleep(1);
	        }
	        echo "\n\n";
	        $req->setUrl($url);
	        $req->sendRequest();
	        //$code = intval($req->getResponseCode());
	        //$header = $req->getResponseHeader();
	        $val = $req->getResponseBody();
	        $val = mb_convert_encoding($val, 'EUC-JP', 'EUC-JP');
	        if (!empty($_SERVER['HIKAKU_DEBUG'])) {
	            $val = $cache->set($key, $val, $expire);
	        }
	        
	    }
	    return $val;
	}
	*/
	
	function info_log($operation, $action, $from, $to) {
	    if (isset($_SERVER['HOSTNAME']) && $_SERVER['HOSTNAME'] == 'test.co.cloudinvoice.co.jp') {
	        $log_file = '/logs/cloudinvoice/mailsender/TestMailSender_' . date('Ymd') . '.log';
	    }
	    else {
	        $log_file = '/logs/cloudinvoice/mailsender/MailSender_' . date('Ymd') . '.log';
	    }
	    if ($action == 'RESULT') {
	        $text = "----------\r\n".$from." から ".$to." 宛 ".date('Y年m月d日 H:i:s')."\r\n".$operation."\r\n";
	    }
	    $fp = fopen($log_file, "a");
	    fwrite($fp, $text. "\n");
	    fclose($fp);
	}
}