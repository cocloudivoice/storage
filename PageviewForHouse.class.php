<?php

//require_once('../../lib/AdminMonitorBaseAction.class.php');
require_once('/var/www/lib/HikakuGoogleAnalyticsApi.class.php');

class PageviewForHouse {
//class PageviewForHouse extends AdminMonitorBaseAction {

/*    var $ga;

    function _init_action() {
        parent::_init_action();
        $this->ga = new HikakuGoogleAnalyticsApi();
    }


        $from_date = explode("-", $request['from_date']);
        $to_date = explode("-", $request['to_date']);

        // 配列化した日付からUnixTimeを算出する
        $fromDateTime = mktime(0, 0, 0, $from_date[1], $from_date[2], $from_date[0]);// 1236265200
        $toDateTime = mktime(0, 0, 0, $to_date[1], $to_date[2], $to_date[0]);        // 1236524400

        // 引き算を行い、差分の秒数を算出する
        //（1236524400 - 1236265200 = 259200）
        $intervalTime = $toDateTime - $fromDateTime;

        // 秒を日単位に変換する（1時間は3600秒→24時間）
        //（259200 ÷ 3600 ÷ 24 = 3）
        $intervalDay = ($intervalTime / 3600 / 24);

        header('Content-Type: text/html; charset=utf-8');

        $cache_key = "shopping:monitor:pageview";
        $expire = 3600 * 6;
        $val = $this->cache->get($cache_key);

        $base_time = $toDateTime;
*/
    function getGoogleData($start_time, $base_time) {
	    $ga;
        $this->ga = new HikakuGoogleAnalyticsApi();
//$this->ga->get_account();
        // 比較ｊｐ
        $this->ga->set_ids(48754435);
//        $this->ga->set_ids(48665773);
//        $this->ga->set_ids(62478857);
//48754435
        $this->ga->set_dimensions('date');
        $this->ga->set_metrics('pageviews', 'entrances');
        $this->ga->set_term($start_time, $base_time);
        $hikaku_pv = $this->ga->get_data_hash();

        $total_pv = array();
        foreach ($hikaku_pv['entry'] as $key => $val) {
            $total_pv[$key]['pageviews'] = $hikaku_pv['entry'][$key]['pageviews'];
            $total_pv[$key]['entrances'] = $hikaku_pv['entry'][$key]['entrances'];

            // 前月・当月合計の算出
            $thisMonth = date('Ym') . '01';
            if ($key < $thisMonth) {
                $total_pv['last_month_total']['pageviews'] += $total_pv[$key]['pageviews'];
                $total_pv['last_month_total']['entrances'] += $total_pv[$key]['entrances'];
            }
            else {
                $total_pv['this_month_total']['pageviews'] += $total_pv[$key]['pageviews'];
                $total_pv['this_month_total']['entrances'] += $total_pv[$key]['entrances'];
            }
        }

        return $total_pv;
    }

/*
    function getGoogleDataID($start_time, $base_time, $id) {
//$this->ga->get_account();
        // 比較（プロファイルID）
        $this->ga->set_ids($id);
        $this->ga->set_dimensions('date');
        $this->ga->set_metrics('pageviews', 'entrances');
        $this->ga->set_term($start_time, $base_time);
        $hikaku_pv = $this->ga->get_data_hash();

        $total_pv = array();
        foreach ($hikaku_pv['entry'] as $key => $val) {
            $total_pv[$key]['pageviews'] = $hikaku_pv['entry'][$key]['pageviews'];
            $total_pv[$key]['entrances'] = $hikaku_pv['entry'][$key]['entrances'];

            // 前月・当月合計の算出
            $thisMonth = date('Ym') . '01';
            if ($key < $thisMonth) {
                $total_pv['last_month_total']['pageviews'] += $total_pv[$key]['pageviews'];
                $total_pv['last_month_total']['entrances'] += $total_pv[$key]['entrances'];
            }
            else {
                $total_pv['this_month_total']['pageviews'] += $total_pv[$key]['pageviews'];
                $total_pv['this_month_total']['entrances'] += $total_pv[$key]['entrances'];
            }
        }

        return $total_pv;
    }

    function parse_data($data, $metrics_arr, $day_key_arr) {
        foreach ($data['entry'] as $key => $val) {
            $m = substr($key, 0, 6);
            foreach ($metrics_arr as $mkey => $mval) {
                if (!isset($val[$mkey])) {
                    continue;
                }
                foreach ($day_key_arr as $dkey => $dval) {
                    if ($dval == $key) {
                        $metrics_arr[$mkey][$dkey] += $val[$mkey];
                    }
                    if ($dval == $m) {
                        $metrics_arr[$mkey][$dkey] += $val[$mkey];
                    }
                }
            }
        }
        return $metrics_arr;
    }*/
}

?>