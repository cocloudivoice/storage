<?php require("header.php");?>


<title>医療費領収書画像のテキスト化 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words = "";
$flag = "";
$sql = "";
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}


$dir_path = dirname(__FILE__)."/../pdf/png/".$af_id;
$dir_path3 = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MDImages";
$dir_path4 = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MDResult";

?>

<article>

	<section id="m-1-box">
		<h2>
			サーバー内医療費領収書画像の全テキスト化
		</h2>
		<p>
		サーバー内の医療費領収書画像を全部テキスト化します。<br>
		ファイルは別途サーバーにアップロードしてこのボタンを押す。
		</p>
		
		<form name="form_sansho" action="./upload_imgs_for_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:0 0 10px 0;">■画像アップロード</p>
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./medical_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./medical_imgs_to_text_upload_all" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="画像アップロード" />
		</form>
		<br><br>
		
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./upload_medical_imgs_to_text_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:0 0 10px 0;">■テキスト化</p>
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./medical_imgs_to_text_upload_all" />
				<input type="hidden" name="ahead_url" value="./medical_imgs_to_text_upload_all" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="テキスト化開始" />
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>
			
	</section>
	
	<section>
		<div style="margin:20px 0 10px 10px;">
		<p style="margin:0 0 10px 0;">■手動ダウンロード</p>
		<input type="button" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path;?>'" value="テキスト化したファイルをダウンロード"/>
		</div>
	</section>
</article>
<?php require("footer.php");?>