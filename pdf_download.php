<!DOCTYPE html>
<html Lang="ja">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



<title>CloudInvoice 請求書ダウンロード</title>





<?php

//クラスファイルの読み込み
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$dl_pass = "";
$words = "";
$flag = "download_invoice_data";
//変数の宣言
$company_id = "";

if (isset($_REQUEST['dl_password'])) {
	$invoice_num = $_REQUEST['det'];
	$dlp = $_REQUEST['dl_password'];
	$sql ="
		SELECT * FROM `INVOICE_DATA_TABLE` 
		WHERE download_password = '".$dlp."' 
	";
	$invoice_sorce_arr = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
	$claimant_id = $invoice_sorce_arr[0]['claimant_id'];
	$str = $invoice_sorce_arr[0]['pay_date'];

	if ($company_id >= 0) {
		//ステータスをダウンロード済みに変更する。
		$downloaded_status = 6;
		$sql ="
		UPDATE `INVOICE_DATA_TABLE` SET `status` = ".$downloaded_status." 
		WHERE download_password = '".$dlp."' AND `claimant_id` = '".$claimant_id."'
		";
		$invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
	}
	//PDFファイルが既にあればそっちを、無ければPDFファイルを作成してダウンロードさせつつ保存する。
	$pdf_url = "../files/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_sorce_arr[0]['download_password'].".pdf";
	if (!file_exists($pdf_url)) {
		//PDF作成＆保存＆ダウンロード
		echo "<script type='text/javascript'>location.href = './invoice2pdf?dlp=".$_REQUEST['dl_password']."&dlps=1&det=".$_REQUEST['det']."';</script>";
	}else {
		//PDFファイルにアクセス
		echo "<script type='text/javascript'>location.href = 'https://storage.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_sorce_arr[0]["download_password"].".pdf';</script>";
	}

/*	//データの取得処理　（ダウンロードの場合は使わない）
	$words = $_REQUEST['dl_password'];
	//請求データの取得
	$invoice_dl_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	var_dump($invoice_dl_arr);
*/
exit();

}
?>





<style type="text/css">


html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue}
a:hover{text-decoration:underline; color:rgb(50,180,240);}
button{cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}

div#clear{clear:both;}

header{width:510px; margin:10px auto 0 auto;}
header h1{float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header h1 a ,header h1 a:visited {float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header ul{text-align:right; line-height:35px; margin:0 10px 0 0; font-size:13px;}
header li{display:inline;}



article{width:510px; margin:20px auto 0 auto; border-color:blue; border-radius:5px; border-width:1px; border-style:solid; border-color:rgb(180,210,210)}
article h2{margin:20px 0 0 20px; color:rgb(50,50,50); font-size:18px;}
article p{font-size:13px;}

div#m-1-box{margin:25px 0 40px 0; padding:25px 0 0 20px; border-width:1px 0 0 0; border-style:solid; border-color:rgb(180,210,210)}
div#m-1-box h3{font-size:14px; ine-height:19px; color:rgb(255,120,0); float:left; margin:0 0 10px 0;}
div#m-1-box h4{font-size:14px;line-height:19px; width:200px; float:left; text-align:right; margin:0 0 7px 0;}
div#m-1-box input[type=text]{width:210px; height:16px;}
div#m-1-box input[type=password]{width:210px; height:16px;}
div#m-1-box input[type=submit]#signup{border:none; width:140px; height:30px; margin:15px 0 0 190px; background:linear-gradient(rgb(80,230,230),rgb(60,200,200)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer;}
div#m-1-box input[type=submit]:hover#signup{background:linear-gradient(rgb(95,235,235),rgb(70,212,212));}
div#m-1-box input[type=submit]#signin{border:none; width:140px; height:30px; margin:15px 0 0 190px; background:linear-gradient(rgb(255,180,60),rgb(255,150,30)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer;}
div#m-1-box input[type=submit]:hover#signin{background:linear-gradient(rgb(255,210,70),rgb(255,175,35));}
div#m-1-box input[type=submit]#download{border:none; width:140px; height:30px; margin:15px 0 0 190px; background:linear-gradient(rgb(10,235,40),rgb(25,210,50)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer;}
div#m-1-box input[type=submit]:hover#download{ background:linear-gradient(rgb(30,255,70),rgb(50,230,40));}

div#m-2-box{margin:30px 0 0 90px;}
div#m-2-box input[type=checkbox]{float:left; margin:4px 3px 0 0; cursor:pointer;}




footer{width:510px; margin:10px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px;}

span.error{	display:none;	visibility:hidden;	color:red;	font-size:11px;	font-style:italic;}
.right{	float:right;}


</style>



<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>



<body>


<?php include_once("./header.html"); ?>


<article>

	
	<h2>
		請求書ダウンロード
	</h2>
	<div id="m-1-box">
		<h3>
			請求書をダウンロードする
		</h3>
		<p>
			（PDFが発行されます）
		</p>
		<div id="clear"></div>
		<form action="" method="post">
			<h4>ダウンロードパスワード：</h4>
			<input name="dl_password" type="text" />
			<div id="clear"></div>
			<input type="hidden" name="dlp_signup" value="1" />
			<input type="submit" id="download" value="ダウンロードする" />
		</form>
	</div>

	<div id="m-1-box">
		<h3>
			アカウントをお持ちの方
		</h3>
		<p>
			（請求書データが御社データベースに格納されます）
		</p>
		<div id="clear"></div>
		<form action="./dlp_login_check" method="post">
			<h4>Eメールアドレス：</h4>
			<input name="id" type="text" />
			<div id="clear"></div>
			<h4>パスワード：</h4>
			<input name="password" type="password" />
			<div id="clear"></div>
			<h4>ダウンロードパスワード：</h4>
			<input name="dl_password" type="text" />
			<div id="clear"></div>
			<input type="hidden" name="dlp_signup" value="1" />
			<input type="submit" id="signin" value="サインイン" />
		</form>
	</div>

	<div id="m-1-box">
		<h3>
			新規登録する
		</h3>
		<p>
			（請求書データが御社データベースに格納されます）
		</p>
		<div id="clear"></div>
		<form action="./dlp_signup" method="post">
			<h4>Eメールアドレス：</h4>
			<input name="e_mail" type="text" />
			<div id="clear"></div>
			<h4>ダウンロードパスワード：</h4>
			<input name="dl_password" type="text" />
			<div id="clear"></div>
			<input type="hidden" name="dlp_signup" value="1" />
			<input type="submit" id="signup" value="新規登録" />
		</form>
	</div>

</article>

<?php include_once("./footer.html"); ?>
</body>


<?php echo $_SESSION['check'];?>
<?php $_SESSION['check'] = "";?>

