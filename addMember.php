<?php session_start(); ?>
<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//アカウント用クラスを読み込む
if (isset($_SESSION['af_id']) || isset($_SESSION['acm_id'])) {
	require_once(dirname(__FILE__)."/../db_con/accountfirmsclientcontrol.class.php");
	require_once(dirname(__FILE__)."/../db_con/accountfirmcontrol.class.php");
}

//変数の宣言
$words = "";
$flag = "";
$sql = "";
//var_dump($_REQUEST);
/*
if (isset($_REQUEST['cid'])) {
	$client_id = $cid = $_REQUEST['cid'];
}
if (isset($_REQUEST['ins'])) {
	$ins_id = $_REQUEST['ins'];
}
if (isset($_REQUEST['client_id'])) {
	if ($_REQUEST['client_id'] == "") {
		$client_id = $_REQUEST['client_id'] = random(11);
	} else {
		$client_id = $_REQUEST['client_id'];
	}
}
if (isset($_REQUEST['id'])) {
	$addressee_table_id = $_REQUEST['id'];
}
*/

//DBに接続する
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_cd = $db_con -> select_db_connect(2);

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
} else {
	header("Location:./logout_af");
	exit();
}

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();


//新しい送付先を作成
//アカウントの追加処理
if ($_REQUEST["name"]!= "") {

	$new_id = $af_con -> af_get_new_num($pdo);
	
	$flag = "insert_af_free";
	$conditions1 = "`id`, `name`, `e_mail`, `profile`, `password`, `zip_code`, `address1`, `address2`, `tel`, `fax`, `plan_status`, `flag1`, `flag2`, `text1`, `text2`, `regist_date`";
	$conditions2 = " '".$new_id."','".$_REQUEST['name']."','".$_REQUEST['e_mail']."','".$_REQUEST['profile']."','".md5($_REQUEST['password'])."','".$_REQUEST['zip_code']."','".$_REQUEST['address1']."','".$_REQUEST['address2']."','".$_REQUEST['tel']."','".$_REQUEST['fax']."','".$_REQUEST['plan_status']."','".$_REQUEST['flag1']."','".$_REQUEST['flag2']."','".$_REQUEST['text1']."','".$_REQUEST['company_name']."','".date("Y-m-d H:i:s")."'";
	$result = $af_con -> af_sql_flag($pdo,$flag,$conditions1,$conditions2);
	if($result === false) {
		echo "<p><span style='color:red;'>メールアドレスが重複しているなどのエラーが発生しました。</span></p>";
	} else {
		echo '<script type="text/javascript">location.href="./authority"</script>';
		exit;
	}
	
/*
	$flag = "select_af_free";
	$conditions1 = " `text1` = '".$af_arr["text1"]."' AND `id`= '".$new_id."'";
	$conditions2 = "ORDER BY `plan_status` DESC";
	$af_all_arr = $af_con -> af_sql_flag($pdo,$flag,$conditions1,$conditions2);
	var_dump($af_all_arr);
*/
}

if ($af_arr["plan_status"] <= 1) {
	echo "アカウント追加権限がありません";
	exit;
}



//設定変更の処理▲

//設定内容の表示データ取得
//echo $company_id;
//echo $client_id;
$flag = "company_data";
$words = "AND company_id = ".$company_id." AND `del_flag` <> 1";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$client_company_id = $company_arr[0]['client_company_id'];
//var_dump($company_arr);
function random($length) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>
<title>新規アカウント作成</title>
<style>
	input[type=text]{ width:350px;}
	#m-2-box {
		margin-right:100%;margin-left:100%;
	}
</style>
<body>

	<section id="m-2-box">
		<h2>
		新規アカウント作成
		</h2>

		<form name="default_form" action="" method="post" >
		<table>
			<tr id="first">
				<th>従業員名</th>
				<td><input type="text" id="name" name="name" value="<?php echo $company_arr[0]['customer_code']; ?>" /></td>
			</tr>
			<tr>
				<th>E-MAIL</th>
				<td><input type="text" name="e_mail" value="<?php echo $company_arr[0]['e_mail']; ?>" /></td>
			</tr>
			<tr>
				<th>パスワード</th>
				<td><input type="text" name="password" value="<?php echo $company_arr[0]['password']; ?>" /></td>
			</tr>
			<tr>
				<th>事業所名</th>
				<td><input type="text" name="company_name" value="<?php echo $company_arr[0]['company_name']; ?>" /></td>
			</tr>
			<tr>
				<th>役職</th>
				<td><input type="text" name="profile" value="<?php echo $company_arr[0]['profile']; ?>" /></td>
			</tr>
			<tr>
				<th>郵便番号</th>
				<td><input type="text" name="zip_code" value="<?php echo $company_arr[0]['zip_code']; ?>" /></td>
			</tr>
			<tr>
				<th>住所１</th>
				<td><input type="text" name="address1" value="<?php echo $company_arr[0]['prefecture'].$company_arr[0]['address1']; ?>" /></td>
			</tr>
			<tr>
				<th>住所２</th>
				<td><input type="text" name="address2" value="<?php echo $company_arr[0]['address2']; ?>" /></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><input type="text" name="tel" value="<?php echo $company_arr[0]['tel_num']; ?>" /></td>
			</tr>
			<tr>
				<th>FAX番号</th>
				<td><input type="text" name="fax" value="<?php echo $company_arr[0]['fax_num']; ?>" /></td>
			</tr>
			<tr id="last">
				<th id="last">権限</th>
				<td id="last" style="vertical-align:middle;">管理者 <input type="radio" name="plan_status" value="2" /> マスター <input type="radio" name="plan_status" value="1" /> 利用者 <input type="radio" name="plan_status" value="0" checked />
					<input type="hidden" id="text1" name="text1" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
					<input type="hidden" name="id" value="<?php echo $company_arr[0]['id']; ?>" />
				</td>
			</tr>

		</table>
		
		<div id="button">
			<button onclick="submit();">作成する</button>
		</div>
		</form>
	</section>
<?php require("footer.php");?>
</body>


