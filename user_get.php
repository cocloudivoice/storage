<?php
session_start();

//必要なクラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
//echo "test1";
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();
$user_con = new user_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}

$sql = "SELECT `user_id` FROM `USER_TABLE` WHERE 1;";
$user_arr = $user_con -> user_sql($pdo,$sql);
$user_all_arr = $user_con -> user_select_all($pdo,$conditions);
foreach ($user_arr as $key => $user) {
	echo $user["user_id"];
	echo "\r\n<br/>";
}