<?php
session_start();

//必要なクラスを読み込む
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
//include_once(dirname(__FILE__)."/../db_con/mysqliconnect.class.php");
include_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../cfiles/ReadCsvFile.class.php");
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
require_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//echo "a<br/>";
//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//mysqliに接続
//$mysqli_con = new mysqliconnect();
//$link = $mysqli_con -> mysql_con();
//var_dump($link);
//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$auto_journalize_con = new auto_journalize_control();
$read_csv_con = new ReadCsvFile();
$pdftoimg_con = new PdfToImg();
$journalized_history_con = new journalized_history_control();

//変数の宣言
$success_num = 0;
$all_num = 0;
$insert_num = 0;
$company_num = 0;
$data_count = 0;
$update_num = 0;
$non_update_num = 0;
$image_update_num = 0;
$com_update_num = 0;
$filepath = "/var/www/co.cloudinvoice.co.jp/html/upfiles/journal";
$file_name = htmlspecialchars($_GET['fn'],ENT_QUOTES);
$rand_num = 0;
$journalized_code = 0;
$rand_num = rand(10,99);
$journalized_code = rand(1000000000,9999999999);
$start_time = date('Y/m/d h-i-s');
$table_name = "JOURNALIZED_HISTORY";
$customer_id = "000000000000";
$serial = random(6).rand(10000,99999);
$current_slip_code = "";
$current_serial_num = 0;
//var_dump($file_name);
if ($file_name == "") {
	//$file_name = "username.csv";
	
	//ファイル名が無かったらこっちの処理はしない。
	header("Location:./jounal_upload;");
}
//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$file_name = str_replace(".csv","",$file_name);//ユーザーの案件一覧企業番号取得
$all_num = count($csv) - 1;

if ($all_num != 0) {
	//var_dump($csv);
	//ob_start();
	//CSVデータを一行ずつ取り出してDBに登録する。
	foreach ($csv as $value) {
		/****
		//テスト・デバッグ用
		if($data_count== 10) {
			break;
		}
		*////
		//変数の初期化
		$mail_address = "";
		
		$data_count++;
		
		if ($value[0] == "日付") {
			
			//カラム名から列番号を取得する
			for ($k = 0; $k < 25; $k++) {
				switch ($value[$k]) {
					case "日付":
						$date_num = $k;
						break;
					case "左勘定":
						$ljou_num = $k;
						break;
					case "左補助":
						$lsub_num = $k;
						break;
		   			case "左部門":
						$lsec_num = $k;
						break;
					case "左税区分":
						$ltax_class_num = $k;
						break;
					case "左金額":
						$lmon_num = $k;
						break;
					case "左消費税":
						$ltax_num = $k;
						break;
					case "右勘定":
						$rjou_num = $k;
						break;
					case "右補助":
						$rsub_num = $k;
						break;
					case "右部門":
						$rsec_num = $k;
						break;
					case "右税区分":
						$rtax_class_num = $k;
						break;
					case "右金額":
						$rmon_num = $k;
						break;
					case "右消費税":
						$rtax_num = $k;
						break;
					case "摘要":
						$sum_num = $k;
						break;
					case "行番号":
						$row_num = $k;
						break;
					case "振替伝票フラグ":
						$trans_num = $k;
						break;
					case "明細番号":
						$ser_num = $k;
						break;
					case "会社コード":
						$cus_num = $k;
						break;
					default :
						$other_num = $k;
						break;
				}
			}
		} else {
			//存在フラグの初期化
			$exist_flag = 0;
			$auto_arr = array();
			
			//echo "--ここから<br/>";
			//var_dump($value);
			
			//$journalized_code = $rand_num;//date("YmdHis").$data_count;
			//振替伝票フラグが0,1の時、伝票コードを作成し、振伝番号に振替伝票フラグと同値を入れる。
			if($value[$trans_num] == 0 || $value[$trans_num] == 1) {
				//伝票コードを新規作成し、現在の伝票コードに代入し、振伝番号と現在の振伝番号に振替伝票フラグと同値を入れる。
				$auto_arr["slip_code"] = $current_slip_code = random(12).rand(2);//伝票コード
				$auto_arr["serial_num"] = $current_serial_num = $value[$trans_num] * 1;//振伝番号
			} else if ($value[$trans_num] >= 2) {
				//現在の伝票コードを伝票コードに入れ、現在の振伝番号に+1して振伝番号に入れる。
				$current_serial_num++;
				$auto_arr["slip_code"] = $current_slip_code;//伝票コード
				$auto_arr["serial_num"] = $current_serial_num;//振伝番号
			}

			if ($data_count == 2) {
				//会社コードカラムの2行目をcustomer_idにする。
				$auto_arr["customer_id"] = $customer_id = $value[$cus_num];
			}
			$auto_arr["customer_id"] = $customer_id;
			$auto_arr["text_data1"] = $file_name;//会社で管理している企業番号
			$auto_arr["text_data2"] = "";
			$auto_arr["insert_date"] = date('Y-m-d',time());
			//$auto_arr["update_date"] = date('Y-m-d H:i:s',time());
										
			/*//明細番号の処理は今後の為に残しておくが、伝票コードと振伝番号を自動で附番するためコメントアウト
			//明細番号の処理
			//echo "<br>serial:".$value[$ser_num]."<br>";
			if ($value[$ser_num] == "") {
				$serial_number = $serial;
			} else {
				$serial_number = $value[$ser_num];
			}
			*/

			$auto_arr["base_amount_of_money"] = $value[$lmon_num];
			$auto_arr["paid_date"] = date('Y-m-d',strtotime($value[$date_num]));//日付
			$auto_arr["debit_account_name"] = $value[$ljou_num];//左勘定科目
			$auto_arr["debit_sub_account_name"] = $value[$lsub_num];//左補助科目
			$auto_arr["debit_section_name"] = $value[$lsec_num];//左部門
			$auto_arr["debit_tax_type"] = $value[$ltax_class_num];//左消費税区分
			$auto_arr["debit"] = $value[$lmon_num];//左金額
			if($value[$ltax_num] == "") {
				$auto_arr["debit_tax"] = 0;//左消費税額
			} else {
				$auto_arr["debit_tax"] = $value[$ltax_num];//左消費税額
			}
			$auto_arr["credit_account_name"] = $value[$rjou_num];//右勘定科目
			$auto_arr["credit_sub_account_name"] = $value[$rsub_num];//右補助科目
			$auto_arr["credit_section_name"] = $value[$rsec_num];//右部門
			$auto_arr["credit_tax_type"] = $value[$rtax_class_num];//右消費税区分
			$auto_arr["credit"] = $value[$rmon_num];//右金額
			if($value[$rtax_num] == "") {
				$auto_arr["credit_tax"] = 0;//右消費税額
			} else {
				$auto_arr["credit_tax"] = $value[$rtax_num];//右消費税額
			}
			$auto_arr["remarks"] = $auto_arr["product_name"] = $auto_arr["company_name"] = $value[$sum_num];//摘要
//echo "<br>";echo			$row_num = $value[$row_num];//行番号
//echo "<br>";echo			$other_num = $value[$other_num];//その他
/*
if ($insert_num == 5) {
	exit;
}
*/
			//支払者の企業が存在しない時の処理
			if ($destination_company_id == "" || $destination_company_id == NULL) {
				//echo "支払者の企業".$dest_com_name."が存在しない時の処理<br/>\r\n";

				//仮メールアドレス生成
				$temp_mail_address = md5(random(12)).rand(4)."@user.cloudinvoice.co.jp";
				$user_con = new user_control();
				
//				$user_dest_arr = $user_con -> user_select($pdo,$temp_mail_address);

				//Emailアドレスの存在を確認する。
				if ($user_dest_arr['mail_address'] != NULL) {
					$temp_mail_address = md5(random(12)).rand(4)."@user.cloudinvoice.co.jp";
				}

				//ユーザー登録
				$user_dest_arr['nick_name']   = $dest_com_name;
				$user_dest_arr['mail_address'] = $temp_mail_address;
				$user_dest_arr['tel_num'] = $dest_phone_number;
				$user_dest_arr['non_regular_flag'] = $payer_non_regular_flag;
				//$passwordmaker = random(12);
				//$passwordmaker = uniqid();
				$passwordmaker = "1234abcd";
				$user_dest_arr['password'] = md5($passwordmaker);

				/*
				//共通コードの生成と取得
				if (!$user_dest_arr['user_id']){
					$dest_user_num = $user_con -> user_get_new_num($pdo);
					$user_dest_arr['user_id'] = $dest_user_num;
				}
//				$result_dest = $user_con -> user_insert($pdo,$user_dest_arr);

				//企業データ登録▼
				$company_dest_id = "".$user_dest_arr['user_id'];
				echo $sql = '
				INSERT INTO `COMPANY_TABLE` ( 
					`company_id`, `company_name`, `email`, `tel_num` , `non_regular_flag`
				) VALUES ( 
					'.$user_dest_arr['user_id'].', "'.$user_dest_arr['nick_name'].'","'.$user_dest_arr['mail_address'].'","'.$user_dest_arr['tel_num'].'","'.$user_dest_arr['non_regular_flag'].'" 
				)';
//				$company_dest_arr = $company_con -> company_sql($pdo,$company_dest_id,$sql);
				//企業データ登録▲
				
				//▼自動仕訳用企業DB登録▼
				//企業品目テーブルと企業用レコードテーブルを登録
				$table_dest_name = "".sprintf("%012d", $company_dest_id);
				try {
//					$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_dest_name);
					$company_num++;
				} catch (PDOException $e) {
				    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
				    $_SESSION['error_msg'] = $e -> getMessage();
				}
				//▲自動仕訳用企業DB登録▲
				*/
			}
			//echo "<br/>\r\n";
			/*
			//請求書・領収書データの登録
			$company_id = sprintf("%012d", $company_id);
			$top_n = substr($company_id,0,4);
			$mid_n = substr($company_id,4,4);
			$path = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $company_id)."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
			echo "<br/>\r\n";
			echo	$pdf_url = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".pdf";
			echo "<br/>\r\n";
			echo	$png_url = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $company_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".$dlp.".png";
			echo "<br/>\r\n";
			//echo "<br/>\r\n";
			*/
			
			/*
			//明細番号で請求書データの存在を確認する
			echo $sql = 'SELECT * FROM `INVOICE_DATA_TABLE` WHERE `serial_number` = "'.$serial_number.'" ';
			$update_flag_arr = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
			$aj_words = " WHERE `text_data2` = ".."";
			$update_flag_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words,$aj_words2);
			//var_dump($update_flag_arr);
			//echo "<br/><br/>\r\n";
			//echo $update_flag_arr[0]['serial_number'];
			//echo ":serial_number<br/><br/>\r\n";
			*/
			
			//同一請求書番号のデータが存在する時は処理をしない
			//if (!isset($update_flag_arr[0]['serial_number'])) {
			
			
			
				//画像のアップロードのみ行う場合はここをコメントアウトする。
				$success_num = $journalized_history_con -> journalized_history_insert($pdo_aj,$table_name,$auto_arr);
				
				//$journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words);

				if ($success_num > 0 && $insert_num == (count($csv)-2)) {
					//CSV取り込み履歴を記録する。
					//CSV_HISTORY_TABLEですが、ついでなのでINVOICE_DATA_TABLEのコントローラーで処理してしまいます。
					$csv_id = md5(random(12).rand(2));
					$sql = "
					INSERT INTO `CSV_HISTORY_TABLE`(
					`company_id`, `file_name`, `csv_id`
					) VALUES (
					".$customer_id.", '".$file_name."', '".$csv_id."'
					)";
					$res = $invoice_con -> invoice_data_sql($pdo,$customer_id,$sql);
					//var_dump($res);
				}

				$insert_num++;
				//echo "新規挿入処理<br/>\r\n";

				//var_dump($auto_arr);
				if ($insert_num==3) {
				//exit;
				}
			//}
			/*
			 else {
				//画像のアップロードのみ行う場合はここをコメントアウトする。
				if ($update_flag_arr[0]['serial_number'] != "") {
					$journalized_history_con -> journalized_history_update($pdo_aj,$table_name,$auto_arr);
					var_dump($result);
					$update_num++;
					echo "上書き処理".$update_num;
					echo "<br/>\r\n";
				} else {
					echo "上書き処理(未実施)".$update_num." 明細番号：".$update_flag_arr[0]['serial_number']."なし";
					echo "<br/>\r\n";
					$non_update_num++;
				}
			}
			*/
			//var_dump($result);
			
			/*
			//証憑ファイルの保存		
			if (!file_exists($pdf_url)) {
				//サーバーにPDFファイルが存在しない場合
				//PDFファイルを保存する
				$pdf_file = ''.$dlp.'.pdf';
				//請求元企業のディレクトリに入る
				try {

					if (mkdir( $path, 0775, true)) {
						chmod( $path, 0775, true );
						chgrp($path, "dev", true );
						//echo "ディレクトリ作成成功！！";
					} else {
						//echo "ディレクトリ作成失敗！！";
					}

				} catch (Exception $e) { 
					echo $e -> getMessage;
				}
			}
			*/
			/*
			if (!file_exists($png_url) || !file_exists($pdf_url)) {
				try {
					$pdftoimg_con -> moveImage($path,$dlp);
					//移動した画像を../pdf/nru/に戻すときに使う
					//$pdftoimg_con -> moveImageReverse($pdf_dir,$dlp);
					$image_update_num++;
				} catch (Exception $e) {
					echo $e -> getMessage;
				}
			}
			*/
		}

	}

}

echo "<br/>\r\n";
echo "新規企業作成件数:".$company_num;
echo "<br/>\r\n";
echo "新規データ作成件数:".$insert_num;
echo "<br/>\r\n";
echo "データ上書き件数:".$update_num;
echo "<br/>\r\n";
echo "企業データ更新件数:".$com_update_num;
echo "<br/>\r\n";
echo "上書き未処理件数".$non_update_num;
echo "<br/>\r\n";
echo "データ件数：".($data_count - 1);
echo "<br/>\r\n";
echo "画像移動件数:".$image_update_num;
echo "<br/>\r\n";
echo "開始時間:".$start_time."<br/>\r\n";
echo "終了時間:".$end_time = date('Y/m/d h-i-s');
fclose($temp);
unlink($file);

//ob_end_flush();

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仕訳CSVアップロード完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">仕訳CSVアップロード完了</h1>
		<h2>仕訳CSVの登録が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./pastjournal'">仕訳日記帳に移動</button>
		<button onclick="window.location.href='./main'">トップ画面に移動</button>
	</div>
</body>
</html>