<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/db_control.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');


//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$company_con = new company_control();


//変数の宣言
$company_id = $_SESSION['user_id'];
$success_num = 0;
$all_num = 0;

//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];

$file = '../files/'.$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
}

ob_start();

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}

$all_num = count($csv) - 1;
$csv_id = "".$_SESSION['file_name'].$company_id.rand();

//取引先コードチェック
foreach ($csv as $value_check) {
	
	if ($value_check[1] == "" ) {
			$_SESSION['up_info_msg'] = "取引先コードが空欄のため、データを登録できません。";
			unlink($file);
			header("Location:./vendor", false);
			exit();
	}
	
}

//CSVデータを一行ずつ取り出してDBに登録する。
foreach ($csv as $value) {

	if ( $value[1] != "取引先コード") {
	
		//得意先共通コードにスペースやハイフンがあれば消す処理
		$value[2] = str_replace(array("　"," ","-"),"",$value[2]);
		
		$sql = "SELECT `client_id` 
		FROM `VENDOR_TABLE` 
		WHERE company_id = ".$company_id." 
		AND `client_id` = '".$value[1]."'
		";
		
		$value[1] = str_replace(array("　"," ","-"),"",$value[1]);
		
		//自社の共通コードおよび取引先コードが一致するものがあればエラーを返して処理を終了する。
		$flag = "vendor_data_csv";
		$client_id_check = $company_con -> company_sql_flag($pdo,$flag,$company_id,$value[1]);
		$client_id = $client_id_check[0]['client_id'];
		if ($value[1] != $client_id) {
			$sql = "
			INSERT INTO `VENDOR_TABLE` 
			(`client_id`, `company_name`, `vendor`, `department`,`position`,
			 `zip_code`, `prefecture`, `address1`, `address2`,
			 `tel_num`, `fax_num`, `email`,
			 `client_company_id`, `company_id`
			) VALUES ( 
			'".$value[1]."', '".$value[3]."', '".$value[4]."', '".$value[5]."', '".$value[6]."',
			'".$value[7]."', '".$value[8]."', '".$value[9]."', '".$value[10]."', 
			'".$value[11]."', '".$value[12]."', '".$value[13]."',
			".intval($value[2]).", ".intval($company_id)."
			)";
		} else {
			$sql ="
			UPDATE `VENDOR_TABLE` SET 
			`client_id`= '".$value[1]."',`company_name`= '".$value[3]."', 
			`vendor`= '".$value[4]."',`department`= '".$value[5]."',`position`= '".$value[6]."', 
			`zip_code`= '".$value[7]."', `prefecture`= '".$value[8]."',`address1`= '".$value[9]."',`address2`= '".$value[10]."',
			`tel_num`= '".$value[11]."',`fax_num`= '".$value[12]."',`email`= '".$value[13]."',
			`client_company_id`= ".intval($value[2]).",`company_id`= ".intval($company_id)." 
			WHERE `client_id`= '".intval($value[1])."' AND `company_id`= ".intval($company_id)." 
			";
		}
		$check_arr = $company_con -> company_sql($pdo,$company_id,$sql);
		$check_arr['chk']['check'];
		if ($check_arr['chk']['check']){
			$success_num += 1;
		}
		//var_dump($check_arr['chk']['check']);
	}
}

fclose($temp);
if ($success_num > 0) {
	//$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込みました。";
	$_SESSION['up_info_msg'] = "<a href='./seikyuhakkou'>".$all_num."件中".$success_num."件のアップロードに成功しました。</a>";
}else {
	$_SESSION['up_info_msg'] = $_SESSION['file_name']."をデータベースに取り込めません。";
}
unlink($file);
header("Location:./vendor", false);
ob_end_flush();
exit();

?>
<button onclick="window.location.href='./vendor'">戻る</button>
</body>