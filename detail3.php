<?php

session_start();

//アナリティクス用のファイル読み込み
include_once("/var/www/analytics/analyticstracking.php");
include_once("/var/www/analytics/analyticstracking1.php");
//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/datadispcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datatitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/sitecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/sitetitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/reviewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/viewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/allpurposecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$site_con = new site_control();

//変数の宣言
$ranking=1;
$user_id;
$site_id;
$sort = "";
$desc = "";
$data_prop;
$count_num = 0;

//site_idから
if(isset($_GET['site_id'])) {
	$site_id = $_GET['site_id'];
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else if (isset($_SESSION['site_id'])) {
	$site_id = $_SESSION['site_id'];
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else {
		header("Location:./index.php");
}

//編集権限確認
if(isset($_SESSION['login_user_id'])) {
	if($_SESSION['login_user_id'] == $user_id) {
		if ($_SESSION['login_user_id'] == "") {
			$edit_on = 0;
			//	echo "ユーザーの画面<br />\n";
		} else {
		
			$edit_on = 1;
			//	echo "オーナーの画面<br />\n";
		}
	} else {
		$edit_on = 0;
		//	echo "ユーザーの画面<br />\n";
	}
}

if(isset($_GET['record_id'])){
	$record_id = $_GET['record_id'];
}
if(isset($_GET['page'])){
	$page = $_GET['page'];
}

//ソートの設定
if (isset($_GET['sort_num']) && $_GET['sort_num'] != NULL) {
	$sort_num = htmlspecialchars($_GET['sort_num'],ENT_QUOTES);
	$sort = "data_body".$sort_num;
} else {
	$sort = "display_rank";//$_GET['sort_name'];//GETか何かでカラム名取得を実装する。
}

if (isset($_GET['sort']) && $_GET['sort'] != NULL) {
	$sort = htmlspecialchars($_GET['sort'],ENT_QUOTES);
}
if (isset($_GET['order']) && $_GET['order'] !=NULL) {
	$desc = htmlspecialchars($_GET['order'],ENT_QUOTES);
}
if (isset($_GET['data_prop']) && $_GET['data_prop'] !=NULL) {
	$data_prop = $_GET['data_prop'];
}
if ($_REQUEST['sort_btn'] == 1) {
	if ($desc =="asc") {
		$desc ="desc";
	} else {
		$desc = "asc";
	}
} else {
	if ($desc =="desc") {
		$desc ="desc";
	} else {
		$desc = "asc";
	}
}


$record_id_rock = 'record_id' . (string)$site_id . (string)$record_id;
$record_kbn = '2';
$record_kbn4 = '4';

//ビューカウント追加
$view_con = new view_control();

if (!$_SESSION[$record_id_rock]){
	$view_arr = $view_con->view_select($pdo,$user_id,$site_id,$record_id);
	$view_arr['user_id'] = $user_id;
	$view_arr['site_id'] = $site_id;
	$view_arr['record_id'] = $record_id;
	$view_arr['view_count'] = (int)$view_arr['view_count'] + 1;
	$result = $view_con->view_insert($pdo,$view_arr);
	$_SESSION[$record_id_rock] = 1;
}

//$_SESSION[{$record_id_rock}] = NULL;

$record_id_from = $record_id;
//$record_id_to = $record_id;
//$sort = 'display_rank';
$col_size;
$disp_index_num_arr = array();
$viewrank = array();
$data_seq_arr=array();
$data_property_arr=array();
$data_disp_arr=array();
$page_conf;
$view_arr;

//dbコントローラー準備
$sitedb = new site_control();
$dispdb = new data_disp_control();
$titledb = new data_title_control();
$datadb = new data_control();
$sitetitledb = new site_title_control();
$reviewdb = new review_control();
$viewdb = new view_control();
$user_con = new user_control();
$all_purpose_con = new all_purpose_control();

//ポジションテーブルから当該カラム名とランク等の情報を取得
$view_sum_data = $viewdb -> view_sum($pdo,$user_id,$site_id);
$site_data = $sitedb -> site_select($pdo,$user_id,$site_id);
$disp_data = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn);
$disp_data4 = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn4);
$title_data = $titledb -> data_title_select($pdo,$user_id,$site_id);
//$col_size = floor(count($disp_data)/3)+6;
$body_data = $datadb -> data_select_all($pdo,$user_id,$site_id,$record_id_from,$record_id_to,$sort);
$site_title_data = $sitetitledb -> site_title_select($pdo,$user_id,$site_id);
$data_num_arr = $datadb -> data_count($pdo,$user_id,$site_id);
$data_num = count($body_data);

//var_dump($site_data);
//var_dump($disp_data);
//var_dump($disp_data4);
//var_dump($title_data);
//var_dump($body_data);
//var_dump($site_title_data);
//var_dump($data_num_arr);


echo "<br/><br/><br/><br/><br/><br/><br/>";

//集計関連処理
	#View合計
	$flag = "sum_view";
	$all_purpose_arr[0]["SUM( `view_count` )"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

	#コメント数
	$flag = "sum_comment";
	$all_purpose_arr[0]["count( * )"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

	#表の信頼度に投票した人の数取得
	$flag = "trust_table";
	$all_purpose_arr[0]["good + bad"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#表を信頼できるとした人の数
	$flag = "trust_num";
	$all_purpose_arr[0]["good"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#表を信頼できるとした人の率
	$flag = "trust_ratio";
	
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo round($all_purpose_arr[0]["good / ( good + bad ) *100"],1);

echo "<br/><br/><br/>ここ★▼▼<br/><br/>";
	#一致するサイトタイトル
	$flag = "search_record_title";
	$words = $body_data[0]['record_title'];
	
	//["record_photo"]
	//["evaluate"]
	//["record_basis"]
	//件数取得
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	$review_all = $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	$site_id_arr = $all_purpose_arr[$i]["site_id"];
	}
	echo "<br/><br/><br/>ここ★▲▲<br/><br/>";

	#☆5つ集計
	$flag = "sum_evaluate_rank5";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank5_num = $all_purpose_arr[0]["count( evaluate )"];
	#☆4つ集計
	$flag = "sum_evaluate_rank4";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank4_num = $all_purpose_arr[0]["count( evaluate )"];

	#☆3つ集計
	$flag = "sum_evaluate_rank3";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank3_num = $all_purpose_arr[0]["count( evaluate )"];

	#☆2つ集計
	$flag = "sum_evaluate_rank2";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank2_num = $all_purpose_arr[0]["count( evaluate )"];

	#☆1つ集計
	$flag = "sum_evaluate_rank1";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank1_num = $all_purpose_arr[0]["count( evaluate )"];

	#☆平均値
	$flag = "sum_evaluate_ave";
	$words = $body_data[0]['record_title'];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$rank_ave = round($all_purpose_arr[0]["avg( evaluate )"],1);


	#レコードタイトルの平均評価を取得する。
	$flag = "ave_evaluate";
	echo "<br/>test0<br/>";
	var_dump($body_data);
	$words = $body_data[0]['record_title'];
	echo $words;echo "<br/>test1<br/>";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	$ave_evalate = $all_purpose_arr[0]["avg( evaluate )"];
	echo "<br/>ここ<br/>";
	
	#レコードタイトルが一致するレビューの数を取得する。
	$flag = "review_num";
	$words = $body_data[0]['record_title'];
	//$words = "1\""." or record_title like \"%小学校%";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	echo $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	echo	$all_purpose_arr[$i]["count( evaluate )"];
	}
	//件数取得
	$test=count($all_purpose_arr);

	#サイトタイトルでのあいまい検索
	$flag = "site_title_search";
	$words = "小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);

	echo $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	echo	$all_purpose_arr[$i]["site_id"];
	}
	//["site_title"]
	//["imgurl"]
	//["introduction"]
	//件数取得
	
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

echo "<br/><br/><br/><br/><br/><br/><br/>";

$ranking = $site_data['rank_define'];


if($_SESSION['max_row']) {
	$max_row = $_SESSION['max_row'];
} else {
	$max_row = $data_num;
}

//data_dispカラムに1（表示）が入っているカラムの末尾番号を取得し、表示用の配列を作る。
for($i = 1;$i <= 50;$i++){
	if($i < 10) {
		$num = "0{$i}";
	} else {
		$num = "{$i}";
	}
	//ここでdata_dispカラムが0(非表示)でなければ、末尾番号を取得して必要なデータのカラム名を
	//末尾番号を付与してつくり、それを表示用のそれぞれの配列に入れる。
	if($disp_data['data_disp'.$num] != 0) {
		$disp_index_num_arr = $num;
		$data_seq_arr[$num]= $disp_data['data_seq'.$num];
		$data_title_arr[$num] = $title_data['data_title'.$num];
	}
}

if ($_SESSION['max_col']){
	$max_col = $_SESSION['max_col'];
}else{
	$max_col = $max_col_first;
}
for($i = 1;$i <= 50;$i++){
	if($i < 10) {
		$num = "0{$i}";
	} else {
		$num = "{$i}";
	}
}

$site_title = $site_title_data['site_title'];
//$site_title = $site_title_data['site_title']."比較 - みんなでつくるソーシャル比較表キュレーションサイト [比較.jp]";
?>

<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://ogp.me/ns/fb#">
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>
-->


<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $body_data[0]['record_title']; ?>,<?php echo $site_title_data['site_title']; ?>比較,<?php echo $site_title_data['titletag01'];echo ',';echo $site_title_data['titletag02'];echo ',';echo $site_title_data['titletag03'];echo ',';echo $site_title_data['titletag04'];echo ',';echo $site_title_data['titletag05'];echo ',';echo $site_data['tag1'];echo ',';echo $site_data['tag2'];echo ',';echo $site_data['tag3'];echo ',';echo $site_data['tag4'];echo ',';echo $site_data['tag5'];echo ',';echo $site_data['tag6'];echo ',';echo $site_data['tag7'];echo ',';echo $site_data['tag8'];echo ',';echo $site_data['tag9'];echo ',';echo $site_data['tag10'];echo ','; ?>,ランキング,カフェ,家電,テレビ,買い物,ショッピング,パソコン,ノートパソコン,タブレット,iphone,食品,食べ物,果物,特産品,服,インテリア,雑貨,小物,金融,証券,FX,投資,冷蔵庫,洗濯機" /> 
		<meta name="description" content="<?php echo $body_data[0]['record_title'].'の詳細ページ - '.$site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />		
		<title><?php echo $body_data[0]['record_title']." - ".$site_title_data['site_title']; ?>比較 - [比較.jp]</title>
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="<?php echo $site_title; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://hikaku.jp/index.php" />
		<meta property="og:image" content="http://hikaku.jp/common/images/logo.jpg" />
		<meta property="og:site_name" content="<?php echo $site_title; ?>" />
		<meta property="og:description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="http://hikaku.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://hikaku.jp/" />
		<script src="http://hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>





<style type="text/css">

/*初期化設定*/
html * {
 margin:0px;
 padding:0px;
}
/*初期化設定*/

div#clear{clear:both;}
div#clearleft{clear:left;}
body {margin:0px; padding:0px; font: 13px/20px "serif";}
div#main-box{width:1180px; height:auto; margin:auto;overflow:hidden; background-color:white; padding-bottom:10px;}

h1 {font:bold 18px/30px "serif"; margin:10px 0 5px 0;}
a{color:#0000ff; text-decoration:none;}


.bol{font-weight:bold;}
.red{font-weight:bold; color:red;/*margin-right:10px;*/}
.blue{font-weight:bold; color:blue;}
.black{font-weight:bold; color:black;}
.tag{font:lighter 13px/22px "ＭＳ 明朝";color:rgb(30,30,30); padding:0 6px 0 0;}
.gray{color:rgb(100,100,100); font-size:11px;}
.aqua{color:rgb(0,150,150); font-size:11px;}
.cblack{margin:0 0 0 10px; font-size:15px; line-height:25px;}
.cred{margin:0 0 0 10px; font-size:15px; line-height:25px; color:red;}
.cblue{margin:0 0 0 10px; font-size:15px; line-height:25px; color:blue;}
.comname{color:green; font-weight:bold;}
.time{}
.invisible { display:none;}

div#h-box{color:white; background-color:black; line-height:35px;}
div#h-box a{color:white; text-decoration:none; margin:0 0 0 10px;}

div#hl-box{float:left; width:500px;}

div#hr-box{text-align:right; margin:0 10px 0 0;}
.popup {display: none;top:35px;left: 1062px; position:absolute; width:200px; height:280px; z-index:1;}
nobr:hover .popup {display:inline; background-color:black;}


div#m1-box{height:368px; width:1180px; border-bottom:2px rgb(255,202,213) solid;}


div#m1l-box{height:368px; width:600px;float:left;border-right:1px rgb(255,202,213) solid;}

div#m1l1-box{height:180px; width:580px; padding:0 10px 0 10px; border-bottom:1px rgb(255,202,213) solid;overflow:hidden;}

div#m1l2-box{height:70px; width:600px; margin:10px 0 10px 10px;}
div#m1l2-box img{float:left; border:0; width:100px; margin:0 10px 0 0;}
div#m1l2-box p{font: 15px/25px "serif"; padding:6px 0 0 0;}

div#m1l3-1-box{float:left; height:53px; width:100px; background-color:rgb(234,234,234); text-align:center; padding:10px 0 0 0; border-radius:5px; margin:0 10px 0 10px;}

div#m1l3-2-box{height:53px; width:600px; padding:6px 0 0 0;}

div#m1r-box{width:1180px;background-color:white;}


div#m2-box{width:1180px;height:400px; border-bottom:1px rgb(255,202,213) solid; border-top:1px rgb(255,202,213) solid;background-color:white;}


div#m2l-box{width:300px;height:390px; float:left; border-right:1px rgb(255,202,213) solid;text-align:center;padding-top:10px;}

div#m2l-box img{width:280px; border:0; margin:10px 0 0 10px;}

div#m2l-box h2{font:bold 15px "serif"; text-align:center; margin:10px 0 0 0;}

div#m2l-box p{margin:30px 0 0 10px;}

div#m2l1-box{margin: 10px 0 0 0;}

div#m2l1-box img{width:100px; margin:0 0 0 10px; float:left; vertical-align:middle;}

div#m2l1-box p{margin:0 0 0 20px;}



div#m2m-box{width:563px; height:400px; float:left; border-right:1px rgb(255,202,213) solid; padding:0 0 0 5px;}

div#m2m-box img{width:40px; float:left;}

div#m2m1-box {margin:5px 0 0 0;}

div#m2m1-box img{width:96px;height:20px; float:left; margin: 0 10px 0 0;}

div#m2m1-box p{font-weight:bold; margin:0;}

div#m2m2-box{height:100px; margin:5px 0 0 2px;}

div#m2m2-box p{margin:0;}

div#m2m3-box{}

div#m2m3-box p{margin:0 250px 0 0; float:left;}

div#m2m4-box{margin:5px 0 0 0;}

div#m2m4-box input[type=text]{border:#a9a9a9 1px solid; width:350px; height:20px;margin:0; float:left;}

div#m2m4-box button[type=comment]{ cursor:pointer;border:none; width:205px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px;}

div#m2m5-box{border:1px rgb(255,202,213) solid; margin:10px 5px 0 0; height:121px; border-radius:10px;}

div#m2m5-1-box{height:103px;}

div#m2m5-2-box{text-align:right; font-size:11px;}

div#m2r-box{height:400px; margin: 0 0 0 876px;line-height:1.2em;padding:10 10 5 10;}

div#m3-box{height:50px; margin:10px 0 0 0; text-align:center;background-color:white;}

div#m3-box button[type=yn]{ cursor:pointer;border:none; width:60px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px; border-radius:5px;}


div#m4-box{margin:15px 0 0 0;background-color:white;}

div#m4-1-box{background-color:rgb(0,0,92); height:30px; width:1170px; color:white; font-weight:bold; font-size:20px; line-height:27px; padding:0 0 0 10px; letter-spacing:1px;}

div#m4-2-box{background-color:rgb(240,240,240); width;1180px; height:auto; overflow:hidden;}

div#m4-2-1box{font-size:19px; font-weight:bold;  margin:0 150px 0 150px; padding:5px 0 0 0; line-height:35px; weight:800px; border-bottom:1px rgb(120,120,120) solid; color:rgb(60,60,60)}

div#m4-2-2box{margin:15px 0 0 300px;}

div#m4-2-2box p{float:left; margin:0;}

div#m4-2-2box input[type=namae]{border:rgb(120,120,120) 1px solid; width:180px; height:22px; margin:0 0 0 10px;}

div#m4-2-3box{margin:10px 0 0 287px;}

div#m4-2-3box p{float:left; margin:0;}

div#m4-2-3box input[type=comment]{border:rgb(120,120,120) 1px solid; width:450px; height:150px; margin:0 0 0 10px;}

div#m4-2-4box{font-size:19px; font-weight:bold;  margin:30px 150px 10px 150px; padding:5px 0 0 0; line-height:35px; weight:800px; border-bottom:1px rgb(120,120,120) solid; color:rgb(60,60,60)}

div#m4-2-5box{margin:0 150px 0 150px;}

div#m4-2-5box p{margin:0;}

div#m4-2-5box a{color:rgb(0,150,150);}

div#m4-2-6box{margin:3px 150px 30px 150px;}

div#m4-2-6box p{margin:0; line-height:13px;}

div#m4-2-6box a{color:rgb(0,150,150);}

div#m4-2-6-1box{color:rgb(100,100,100); font-size:11px; padding:10px 0 0 0;}

div#owner_chat{ padding-left:10px; padding-right:10px; }

/*画像リサイズ設定*/
.image-resize {		max-width: 180px; max-height:130px;}




/*詳細ページのスタイルシート*/
div#dm1-box{position:relative;width:1180px; height:auto; border-bottom:1px rgb(255,202,213) solid;}

div#dm1l-box{float:left; width:800px; min-height:368px; border-right:1px rgb(255,202,213) solid;}

div#dm1l-1-box{margin:0 0 0 10px;}

div#dm1l-1-box img{height: 19px; margin:0 10px 0 0; float:left;}

div#dm1l-1-box p{margin:0 0 0 5px; }

div#dm1l-2-box{margin:10px 0 0 10px;}

div#dm1l-2-box img{width:280px; float:left; margin:0 10px 0 0;}

div#dm1l-2-box p{margin:0;}

div#dm1r-box{position:relative;width:360px;min-height:368px;float:left;}
div#dm1r-2box{}



div#dm2-box{width:1180px; margin:10px;}

h2#dm2-box-h2 {font:bold 15px "serif"; text-align:center; margin:10px 10px 10px 0;}

div#dm2-box img{float:left; width:150px; margin:0 10px 0 10px;}

div#dm2-box p{margin:0 0 0 10px;}

div#dm2l-box{float:left; width:393px; height:150px;}

div#dm2m-box{float:left; width:393px; height:150px;}

div#dm2r-box{float:left; width:392px;}



div#dm3-box{float:left; width:800px; padding:0 0 0 10px;}

div#dm3-box h2{margin:30px 0 10px 0;}

div#dm3l-box{float:left; margin:0 40px 0 0;}

div#dm3l-box a{float:left; font-size:13px;}

div#dm3l-box img{float:left; margin:0 5px 0 5px;}

div#dm3r-box{}

div#dm3r-box img{float:left;}

div#dm3r-box p{margin:5px 0 20px 150px; color:gray;}

div#dm3r-box button[type=revue]{border:none; width:205px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px;}




div#dm4l-box img{float:left; width:150px; margin:10px 15px 0 0;}

div#dm4r-box img{float:left; width:70px; margin:1px 5px 0 0;}

div#dm4r-box p{margin:0;}	






div#dm4f-box{margin:5px 0 60px 150px;}

div#dm4f-box button[type=yn]{border:none; width:60px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px; border-radius:5px;}



div#dn5-box{}

div#dm5-box a{font-size:15px; text-decoration:underline; font-weight:bold;}

div#dm5-box button[type=revue]{border:none; width:205px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px; margin:20px 0 0 0;}



div#dm10-box{height:250px; margin:20px 0 0 0;}



div#dm11-box{margin:20px 0 40px 0;}

div#dm11-box img{float:left; width:80px;}

div#dm11-box p{margin:5px 0 5px 0;}




/*画像リサイズ*/
/*ここから表型ページ用*/
body {
/*
font-family: "Meiryo", "メイリオ", "ヒラギノ角ゴ Pro W3", sans-serif;
font-weight:600;
*/
width:auto;
height:auto;
background-image:url("<?php echo $site_data['site_bg_img'];?>");
background-size: contain;

}

h2
div#wrapper{
	width:1180px;
	height:100%;
	background-color:white;
	margin-left:auto;
	margin-right:auto;
}
#trustBtn {
	margin-left:auto;
	margin-right:auto;
	text-align:center;
}
.image-resize {
	max-width: 180px;
	max-height:130px;
}
.pall10 {
	padding:10px;
}
.mleft10 {
	margin-left:10px;
}
.mleft20 {
	margin-left:20px;
}
.mleft30 {
	margin-left:30px;
}
.mleft40 {
	margin-left:40px;
}
.fleft{
	float:left;
}
.mbottom10 {
	margin-bottom:10px;
}
.mbottom20 {
	margin-bottom:20px;
}
.mbottom30 {
	margin-bottom:30px;
}
.mbottom40 {
	margin-bottom:40px;
}
.mtop10 {
	margin-top:10px;
}
.mtop20 {
	margin-top:20px;
}
.mtop30 {
	margin-top:30px;
}
.mtop40 {
	margin-top:40px;
}
.mlauto{
	margin-left:auto;
}
.mrauto{
	margin-right:auto;
}

.invisible {
	display:none;
}
.tright {
	text-align:right;
}
.tleft {
	text-align:left;
}
.tcenter {
	text-align:center;
}
#starsbox {
	top:0px;
	width:100px;
	margin-bottom:10px;
}
#record_title {
	width:auto;
	margin-top:10px;
}
/*ここまで表型ページ用*/

/*テキストオーバーフロー設定*/
.text-overflow {
    overflow: hidden;
    /*border: 1px solid #ccc;*/
    padding: 0px;
    width: 563px;
    /*background: #eee;*/
    line-height: 1.2em;
    /*white-space: nowrap;*/
}
.text-overflow {
    text-overflow: ellipsis;
    -webkit-text-overflow: ellipsis; /* Safari */
    -o-text-overflow: ellipsis; /* Opera */
}
 /*テキストオーバーフロー設定*/
#owner_space_left {
	width:200px;
}
#ochat_space {
	width:300px;
	margin-left:10px;
}
input[type="submit"],input[type="button"] {
	cursor: pointer;
	margin-left: 10px;
}
li {
	list-style:none;
}
img {
	border:none;
}

#footer {
	margin-right:auto;
	margin-left:auto;
	text-align:center;
	width:1180px;
}

#h-box {
	margin-right:auto;
	margin-left:auto;
	text-align:center;
	width:1180px;
}

#dm1l-2-box table td.label {
    background-color: #F5F5F5;
    color: #666666;
    vertical-align: top;
    width: 35%;
}
#dm1l-2-box table {
    width: 97%;
	table-layout: auto;
	margin-top:15px;
}
#dm1l-2-box table td {
    border-top: 1px dotted #CCCCCC;
    line-height: 18px;
    padding: 3px 1px 3px 10px;
    /*border-top: medium none;*/
}
</style>

</head>

<body>



<div id="main-box">



	<div id="h-box">
		<div id="hl-box">
			<a href="http://hikaku.jp">マンザX</a>
			<a href="">勢いランキング</a>
			<a href="">HOTユーザー</a>
			<a href="">人気検索ワード</a>
		</div>
		<div id="hr-box">
			<a href="./index.php">ログイン</a>
			<a href="./index.php">会員登録</a>
			<a href="">使い方</a>
			<nobr>　メニュー▼
				<span class="popup">aa</span>
			</nobr>
		</div>
	</div>
	
	<div id="clear"></div>
	
	
	
	<div id="dm1-box">
	
	
	
		<div id="dm1l-box">
			
			<div id="dm1l-1-box">
				<h1>
					<?php echo $body_data[0]['record_title']; ?>
				</h1>
				
				<?php
					$stars = round($ave_evalate,1);
					$star_int = intval($stars);
					(float)$half = $stars - $star_int;
					if ($half == 0) {
							echo '<img class="vmiddle" style="display:inline" src="../images/base/star'.$star_int.'.png"/>';
					} else {
							echo '<img class="vmiddle" style="display:inline" src="../images/base/star'.$star_int.'.5.png"/>';
					}
				?>
				<a href="#">(<?php echo $review_all;?> 件のレビュー)</a>
				<div id="clear"></div>
				<p>
					<span class="gray">5つ星のうち、<?php echo round($ave_evalate,1); ?>点</span>
				</p>
			</div>
			
			
			<div id="dm1l-2-box">
				<img src="<?php echo $body_data[0]['record_photo']; ?>">
				<span class="gray">詳細データ</span>	
				<p>

			<?php
				if ($ranking == 1) {
					echo '<span class="bol">順位</span> ： '.$body_data[0]["display_rank"].'位<br />';
				}
			?>
					<span class="bol">評価の根拠</span> ： <?php echo nl2br($body_data[0]['record_basis']); ?><br />
				<?php if ($body_data[0]['record_explain'] != "") { ?>
					<span class="bol">説明</span> ： <?php echo nl2br($body_data[0]['record_explain']); ?><br />
				<?php } ?>
				</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tbody>
						
				<?php 


					if (asort($data_seq_arr, SORT_NUMERIC)) {

						foreach($data_seq_arr as $key => $posi_data){

							//添え字の番号になっている下2文字を取得
							$rank = substr($key,-2,2);
							//添え字番号が一致するdata_titleのデータを取得する。

?>
						<tr><td class="label">
<?php
							echo $title_data['data_title'.$rank];
?>
							</td><td class="value">
<?php
							//var_dump($disp_data);
							$disp_index = 'data_property'.$rank;
							$data_type = substr($disp_data[$disp_index],-2,1);
							$col_name = "data_body".$rank;//＄rankがidからとってしまっている。

							switch( $data_type ) {
								case 1://文字列
										echo $body_data[0][$col_name]."\n";
										break;
								case 2://文字列（複数行）設定しない
										echo $body_data[0][$col_name]."\n";
										break;
								case 3://数字（ソートのため）
										echo (string)$body_data[0][$col_name]."\n";
										break;
								case 4://外部リンク
										echo "<a href=\"".$body_data[0][$col_name]."\" title=\"外部リンク\" rel=\"nofollow\" target=\"_brank\"><img src=\"../images/base/btn_link.png\" onmouseover=\"onBtnL(this)\" onmouseout=\"outBtnA(this)\" style=\"width:100px;height:36px;\" /><span class=\"invisible\">外部リンク</span></a>\n";
										break;
								case 5://アフィリエイトリンク
										echo "<a  href=\"".$body_data[0][$col_name]."\" title=\"リンクURL\" rel=\"nofollow\" target=\"_brank\" ><img src=\"../images/base/btn_link.png\" onmouseover=\"onBtnA(this)\" onmouseout=\"outBtnA(this)\" style=\"width:100px;height:36px;\" /><span class=\"invisible\">リンクURL</span></a>\n";
										break;
								case 6://画像
										echo '<div class="input_form" style="overflow:hidden;"><div style="position:relative;"><img src="'.$body_data[0][$col_name].'" style="width:;min-width:120px;max-width:200px;min-height:60px;max-height:200px" /></div></div>';
										break;
								case null:
										break;
								default:
										break;
							}
				?>
							</td></tr>
								
					
				<?php
						}

					}

				?>
				
						<tr><td class="lAttr">&nbsp;</td><td class="lAttr">&nbsp;</td></tr>
					</tbody>
				</table>

				
				<div id="clear"></div>
			</div>
		</div>
		
		
		<div id="dm1r-box">
			aa
		</div>
		
		
		
		
		
		
		
		<div id="clear"></div>
	</div>

<div id="dm2-box">
	<h2 id="dm2-box-h2">
		<?php echo $body_data[0]['record_title']; ?>を載せているページ
	</h2>

		
<?php	

	#一致するサイトタイトル
	$flag = "search_other_record_title";
	$words = $body_data[0]['record_title'];
	$all_site_arr = $all_purpose_con -> all_purpose_site_info($pdo,$flag,$user_id,$site_id,$words);
    
	$review_all = $num_elm = count($all_site_arr);
	$other_site_num = 1;
	for ($i = 0; $i < $num_elm; $i++) {
		if ($all_site_arr[$i]["site_id"] != $site_id) {
			$other_site_id = $all_site_arr[$i]["site_id"];

			//サイトタイトル取得
			$other_site_title_arr = $sitetitledb -> site_title_select_other($pdo,$other_site_id);

			//集計関連処理
			#View合計
			$flag = "sum_view";
			$site_view = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$other_site_id,$words);
			$site_view[$i]["SUM( `view_count` )"];

			#コメント数
			$flag = "sum_comment";
			$count_comment_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$other_site_id,$words);
			$count_comment = $count_comment_arr[0]['count( * )'];

			#表の信頼度に投票した人の数取得
			$trust_arr = $all_purpose_con -> all_purpose_sql_trust($pdo,$other_site_id);
			$trust_good = $trust_arr[0]["good"];
			$trust_bad = $trust_arr[0]["bad"];
			$trust_all = $trust_good + $trust_bad;
			$trust_ratio = round(($trust_good/$trust_all) * 100,1);

		if ($other_site_num % 3 == 1) {
			echo '<div id="dm2l-box">';
		} else if ($other_site_num % 3 == 2) {
			echo '<div id="dm2m-box">';
		} elseif ($other_site_num % 3 == 0) {
			echo '<div id="dm2r-box">';
		}
?>
			<a href="./main.php?site_id=<?php echo $other_site_id;?>" >
			<img src="<?php echo $all_site_arr[$i]['record_photo'];?>" title="<?php echo $other_site_title_arr["site_title"];?>" />
			<h3>
				<?php echo $other_site_title_arr["site_title"];?>
			</h3>
			<div id="clear"></div>
			<p>
				PV:<span class="bolm"><?php echo $site_view[$i]["SUM( `view_count` )"];?></span>
				コメント:<span class="bolm"><?php echo $count_comment;?></span>
				<!--マイリスト:<span class="bolm">572</span>-->
				<!--勢い:<span class="red">827</span><br>--><br/>
				「この比較は信頼出来る」と投票した人の数:<br/><span class="bol"><?php echo $trust_all;?></span>人中、<span class="red"><?php echo $trust_good;?></span>人（<span class="red"><?php echo $trust_ratio;?>%</span>）
			</p>
			</a>
		</div>
<?php 	
		if ($other_site_num%3 == 0 || $num_elm == $other_site_num) {
			echo '</div>';
		}

		$other_site_num++;
		} else {
			//何もしない
		}
	}
?>


	
	<div id="dm3-box">
		<h2>レビュー</h2>
		<?php 
			//echo $rank_num = $rank5_num + $rank4_num + $rank3_num + $rank2_num + $rank1_num;echo "<br/><br/>";
			//echo $evaluate_all = $rank5_num * 5 + $rank4_num * 4 + $rank3_num * 3 + $rank2_num * 2 + $rank1_num * 1;echo "<br/><br/>";
		?>
		<div id="dm3l-box">
			<a href="">星5つ</a>
			<img class="vmiddle" style="display:inline" src="../images/base/star5.png"/>
			<span class="grayn"><?php if ($rank5_num ==0) { echo 0; } else { echo $rank5_num;} ?></span>
			
			<div id="clear"></div>
			
			<a href="">星4つ</a>
			<img class="vmiddle" style="display:inline" src="../images/base/star4.png"/>
			<span class="grayn"><?php if ($rank4_num ==0) { echo 0; } else { echo $rank4_num;} ?></span>
			
			<div id="clear"></div>
			
			<a href="">星3つ</a>
			<img class="vmiddle" style="display:inline" src="../images/base/star3.png"/>
			<span class="grayn"><?php if ($rank3_num ==0) { echo 0; } else { echo $rank3_num;} ?></span>
			
			<div id="clear"></div>
			
			<a href="">星2つ</a>
			<img class="vmiddle" style="display:inline" src="../images/base/star2.png"/>
			<span class="grayn"><?php if ($rank2_num ==0) { echo 0; } else { echo $rank2_num;} ?></span>
			
			<div id="clear"></div>
			
			<a href="">星1つ</a>
			<img class="vmiddle" style="display:inline" src="../images/base/star1.png"/>
			<span class="grayn"><?php if ($rank1_num ==0) { echo 0; } else { echo $rank1_num;} ?></span>
			
			<div id="clear"></div>
			
		</div>
		
		<div id="dm3r-box">
			
			<img class="vmiddle" style="display:inline" src="../images/base/star5.png"/>
			<!--<img class="vmiddle" style="display:inline" src="../images/base/star<?php echo $stars; ?>.png"/>-->
			
			<a href="">
				(<?php echo $review_all + 1; ?> 件のレビュー)
			</a>
			
			<p>5つ星のうち<?php echo $rank_ave;?>点</p>
			
			<button type="revue">レビューを書く</button>
			
			
		</div>

		<div id="clear"></div>

		<h2>参考になったカスタマーレビュー</h2>

			<!--ここから追加-->

		<div id="dm4-box">
			<span class="gray">
				45人中32人の方が、「このレビューは参考になった」とコメントしています。
			</span>
			
			<div id="dm4l-box">
				<img src="/mile/img2/tukuba.jpg">
			</div>
			
			<div id="dm4r-box">
				<div id="dm4r-1-box">
					<img src="/mile/img2/star.jpg">
					<p>ダメ元で願書を出すべし<p>
				</div>
				<div>
					<p>
						著名人の倉庫。 ここを卒業すれば、三菱系列なら何処でも入れます。<br>
						また、政治家や国家公務員にも著名人も多く、コネを使いやすいです。<br>
						学費も安く、男子ならダメ元でも狙う価値が有ります。（女子でも長期就業を目指すなら有り）<br>
						尚、入試に受かる最も重要な要素は運です（一次と三次の抽選で９割は弾かれる）
					</p>
				</div>
			</div>
			
			<div id="clear"></div>
			
			<div id="dm4f-box">
				<a href="">コメント</a>
				｜
				<span class="bol">このレビューは参考になりましたか？</span>
				<button type="yn">はい</button>
				<button type="yn">いいえ</button>
			</div>
		</div>
		
		<div id="dm5-box">
			<a href="">28件すべてのレビューを表示（新しい順）</a>
			<br>
			<button type="revue">レビューを書く</button>
		</div>

	</div>

	<div id="dm10-box">
		広告
	</div>

	<h2>
		最近のレビュー
	</h2>

	<div id="dm11-box">
		<img src="/mile/img2/star.jpg">
		<span class="bol">No.1小学校！</span>
		<p>
			著名人の倉庫。ここを卒業すれば、三菱系列なら何処でも入れます。また、政治家や公務員にも出身者が多く、コネを使いやすいです。
			学費も安く、男子ならダメ元でも狙う価値が有り…
		<p>
		<span class="grayn">
			投稿日:9日前　投稿者:takesyukohika
		</span>
	</div>
	
	
	<div id="dm11-box">
		<img src="/mile/img2/star.jpg">
		<span class="bol">No.1小学校！</span>
		<p>
			著名人の倉庫。ここを卒業すれば、三菱系列なら何処でも入れます。また、政治家や公務員にも出身者が多く、コネを使いやすいです。
			学費も安く、男子ならダメ元でも狙う価値が有り…
		<p>
		<span class="grayn">
			投稿日:9日前　投稿者:takesyukohika
		</span>
	</div>



</div>


<!--▽フッター▽-->
<div id="footer">
<div id="h-box">

	<p style="text-align:center;">Copyright © 2014 hikaku.jp All rights reserved.</p>
</div>
</div><!--footer-->
<!--△フッター△-->


</body>