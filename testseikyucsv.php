<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}


?>

<script>
	function download_file() {
		location.href = "//storage.cloudinvoice.co.jp/invoice/download.php";
	}
	
	function erase_file(n) {
		
		if (window.confirm('このCSVデータを消去しますか？')) {
			frm_elm = document.getElementById('erase_form'+ n);
			frm_elm.submit();
		} else {
			//alert('キャンセルされました');
			return false;
		}

/*
		if(window.confirm('このCSVデータを消去しますか？')){ // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		}
		else{ // 「キャンセル」時の処理
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false; // 送信を中止
		}
*/
	}
</script>

<article>

	<section id="m-1-box">
		<h2>
			請求CSVインポート
		</h2>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./testread_csv.php" method="post" enctype="multipart/form-data">

		<div id="m-1-box">
			ファイルを選択
			<input type="file" name="upfile" onchange="uv.style.display='inline-block'; uv.value = this.value;"  />
			<input type="text" id="uv" class="uploadValue" disabled />
			<input type="hidden" name="return_url" value="./seikyucsv">
			<input type="hidden" name="ahead_url" value="./checkseikyucsv">
			<input type="hidden" name="colms" value="18">
		</div>
		<div id="kakunin">
			<input type="button" name="kakunin" onclick="submit()" value="確認画面に進む　>" />
		</div>
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>

		<div id="sFormat">
			<a href="./sample_download">csvフォーマットについて</a><br>
		</div>

	</section>

	<section id="m-1-box">
		<h2>
			インポート履歴
		</h2>

		<table id="scsv">
			<tr>
				<th id="name">ファイル名</th>
				<th id="date">取込日時</th>
			</tr>
	<?php
		for ($i = 0; $i < $csv_history_num; $i++) {
	?>
			<tr>
				<td><span><?php echo $csv_history_data_arr[$i]["file_name"];?></span></td>
				<td><?php echo $csv_history_data_arr[$i]["upload_date"];?></td>
				<td>
					<form id="erase_form<?php echo $i;?>" action="./erase_csvfile" method="post" />
						<input type="hidden" name="company_id" value="<?php echo $company_id;?>" />
						<input type="hidden" name="csv_id" value="<?php echo $csv_history_data_arr[$i]['csv_id'];?>" />
						<input type="button" value="消去" onclick="erase_file(<?php echo $i;?>);"/>
					</form>
				</td>
			</tr>
	<?php
		}
	?>
		</table>


	</section>





</article>

<?php require("footer.php");?>