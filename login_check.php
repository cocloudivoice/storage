<?php
session_start();
ini_set('session.use_cookies', 0);//お客様がクッキーを利用しているときは、セッションをクッキーに保存する
ini_set('session.cookie_lifetime', 0);//セッションをクッキーに保存したときは、ブラウザを閉じるまでセッションを切断しない
ini_set('session.gc_maxlifetime', 86400);//クッキーを利用しないときは、セッション有効時間は10時間

require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');

$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//session_id();
//ここからログインチェック
	if (isset($_POST['id']) && isset($_POST['password']) && $_POST['id'] != "" ) {
		//DBからIDとPASSWORDを取得してPOSTされたID,PASSWORDと比較する。
		$id = htmlspecialchars($_POST['id'],ENT_QUOTES);
        //var_dump($id);
//		$password = htmlspecialchars($_POST['password'],ENT_QUOTES);


//ここからパスワードの暗号化対応 便宜的にコメントアウトするので、
//ユーザーデータ作り直しの段階でコメントアウトを解除する。
		$password = md5(str_replace(" ","",htmlspecialchars($_POST['password'],ENT_QUOTES)));
//ここまでパスワードの暗号化対


        
		//$mail_address = $_GET['mail_address'];
		$login_con = new user_control();
		$login_arr = $login_con -> user_select_pass($pdo,$id,$password);
		$_SESSION['user_id'] = $login_arr['user_id'];
		if ($login_arr != NULL) {
        //情報が取得できた場合はマイページへ
		    $_SESSION['check'] = "";
			header("Location:./main");
    		exit();
        } else {
			//情報を取得できなかった場合はログイン画面へ
		    $_SESSION['check'] = "Emailアドレスまたはパスワードが違います";
			header("Location:./signin",FALSE);
    		exit();
        }
	} else {
			//情報を取得できなかった場合はログイン画面へ
		    $_SESSION['check'] = "Emailアドレスまたはパスワードが違います";
			header("Location:./signin",FALSE);
    		exit();
    }
?>