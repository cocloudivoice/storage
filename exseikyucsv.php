<?php 
session_start();

	if ( $_POST['mode'] === 'download' ) {
		include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
		include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
		include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

		//データベースに接続
		$dbpath_con = new dbpath();
		$pdo = $dbpath_con -> db_connect();
		$invoice_data_con = new invoice_data_control();
		$company_con = new company_control();

		//変数
		$words = " and status >= 1 ";
		
		$company_id = $_SESSION['user_id'];
		if ( $_REQUEST['koumoku'] == 1 ) {
			//明細別
			$flag1 = "invoice_count";
			$flag = "invoice_data";
		} else if ($_REQUEST['koumoku'] == 0) {
			//請求別
			$flag1 = "invoice_total_count";
			$flag = "invoice_total_data";
		}
		if ($_REQUEST['update_from']){
				$upfrom = str_replace(" ","",substr($_REQUEST['update_from'],0,4)."/".substr($_REQUEST['update_from'],4,2)."/".substr($_REQUEST['update_from'],6,2));
				$words .= " and insert_date >= ".$upfrom." ";
		}
		if ($_REQUEST['update_to']){
				$upto = str_replace(" ","",substr($_REQUEST['update_to'],0,4)."/".substr($_REQUEST['update_to'],4,2)."/".substr($_REQUEST['update_to'],6,2));
				$words .= " and insert_date <= ".$upto." ";
		}
		if ($_REQUEST['paydate_from']) {
			$payfrom = $_REQUEST['paydate_from'];
			$words .= " and pay_date >= ".$payfrom." ";
		}
		if ($_REQUEST['paydate_to']) {
			$payto = $_REQUEST['paydate_to'];
			$words .= " and pay_date <= ".$payto." ";
		}
		
		//INVOICE_DATA_TABLEからCSVにするデータを取得
		$invoice_data_num = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
		$invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);

		//var_dump($invoice_data_num);
		//var_dump($invoice_data_arr);
		$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
		for ( $i = 0; $i < intval($invoice_data_num[0]["count(*)"]); $i++ ) {
			$csv_data .= $invoice_data_arr[$i]["claimant_id"].",";
			$claimant_id = $invoice_data_arr[$i]["claimant_id"];
			
			//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
			$flag = "company_data";
			if ($claimant_id != 0 ) {
				$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
				//echo "<br/>claimant<br/>\r\n";
				$csv_data .= $claimant_company_name[0]['company_name'].",";
			} else {
				$csv_data .= ",";
			}
			
			//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
			$dest_id = $invoice_data_arr[$i]["destination_id"];
				$flag = "addressee_data_receive_client_id";
				$words = " and company_id = ".$claimant_id."";
				$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
/*
			if ($client_id_arr[0]['client_id'] == NULL) {
				$csv_data .= ",";
			} else {
				$csv_data .= $client_id_arr[0]['client_id'].",";
			}
*/
			$csv_data .= $invoice_data_arr[$i]["billing_date"].",";
			$csv_data .= $invoice_data_arr[$i]["invoice_code"].",";
			$csv_data .= $invoice_data_arr[$i]["invoice_name"].",";
			$csv_data .= $invoice_data_arr[$i]["staff_name"].",";
			$csv_data .= $invoice_data_arr[$i]["pay_date"].",";
			$csv_data .= $invoice_data_arr[$i]["bank_account"].",";
			$csv_data .= $invoice_data_arr[$i]["sale_date"].",";
			$csv_data .= $invoice_data_arr[$i]["product_code"].",";
			$csv_data .= $invoice_data_arr[$i]["product_name"].",";
			$csv_data .= $invoice_data_arr[$i]["unit_price"].",";
			$csv_data .= $invoice_data_arr[$i]["quantity"].",";
			$csv_data .= $invoice_data_arr[$i]["unit"].",";
			if ( $invoice_data_arr[0]['sum(total_price_excluding_tax)'] != NULL ) { 
				$csv_data .= $invoice_data_arr[$i]['sum(total_price_excluding_tax)'].","; 
			} else { 
				$csv_data .= $invoice_data_arr[$i]["total_price_excluding_tax"].",";
			}
			$csv_data .= $invoice_data_arr[$i]["sales_tax"].",";
			$csv_data .= $invoice_data_arr[$i]["withholding_tax"].",";
			if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
				$csv_data .= $invoice_data_arr[$i]['sum(total_price)'].","; 
			} else { 
				$csv_data .= $invoice_data_arr[$i]["total_price"].",";
			}
			$csv_data .= $invoice_data_arr[$i]["remarks1"].",";
			$csv_data .= $invoice_data_arr[$i]["remarks2"].",";
			$csv_data .= $invoice_data_arr[$i]["remarks3"]."\n";
		}
        //出力ファイル名の作成
        $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
      
        //文字化けを防ぐ
        $csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );
          
        //MIMEタイプの設定
        header("Content-Type: application/octet-stream");
        //名前を付けて保存のダイアログボックスのファイル名の初期値
        header("Content-Disposition: attachment; filename={$csv_file}");
      
        // データの出力
        echo($csv_data);
        exit();
    }
?>
<?php require("header.php");?>


<title>支払CSVエクスポート - Cloud Invoice</title>



<article>

	<section id="m-1-box">
		<h2>
			請求CSVエクスポート
		</h2>
		
		<p>
			条件を指定してください。
		</p>
		<form action="checkuriagekanri" method="GET" name="seikyu_csv_export_form">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" name="destination_id" />
			</div>
			
			<div id="ID">
				<h3>送付先コード</h3>
				<input type="text" name="send_code" />
			</div>
			
			<div id="status">
				<h3>状態</h3>
				<label><input name="status" type="checkbox" value="1" checked /> 未発行</label>
				<label><input name="status_1" type="checkbox" value="1" /> 発行済</label>
			</div>
			
			
			<!--
			<div id="hiduke">
				<h3>csv取込日付</h3>
				<input type="text" name="insert_date_from" />
				<p>(yyyymmdd) - </p>
				<input type="text" name="insert_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			-->
			
			<div id="hiduke">
				<h3>請求書日付</h3>
				<input type="text" name="billing_date_from" />
				<p>(yyyymmdd) - </p>
				<input type="text" name="billing_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="hiduke">
				<h3>支払期限日</h3>
				<input type="text" name="pay_date_from" />
				<p>(yyyymmdd) - </p>
				<input type="text" name="pay_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="shiharaEx">
				<button onclick="submit();">確認画面に進む ></button>
			</div>
		</form>
	</section>






</article>

<?php require("footer.php");?>