<?php require("header.php");?>

<?php
//クラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/regularinvoiceformcontrol.class.php');


//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$regular_con = new regular_invoice_form_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$send_count = 0;//送信数の初期化
$status_num = 0;//ステータス変更用の変数初期化

//削除の処理
$del_flag = $_REQUEST['del_flag'];
if ($del_flag == 1) {
		$sql = "DELETE FROM `REGULAR_INVOICE_FORM_TABLE` WHERE template_id = ".$_REQUEST['template_id']." AND `claimant_id`= ".$company_id." ";
		$result = $regular_con -> regular_invoice_form_sql($pdo,$company_id,$sql);
}
//一時停止の処理
$suspended_flag = $_REQUEST['suspended_flag'];
$suspended = $_REQUEST['suspended'];
if ($suspended_flag == 1) {
	if ($suspended == 0) {
		$sql = "UPDATE `REGULAR_INVOICE_FORM_TABLE` SET `suspended` = 1 WHERE template_id = ".$_REQUEST['template_id']." AND `claimant_id`= ".$company_id." ";
		$result = $regular_con -> regular_invoice_form_sql($pdo,$company_id,$sql);
	} else if ($suspended == 1) {
		$sql = "UPDATE `REGULAR_INVOICE_FORM_TABLE` SET `suspended` = 0 WHERE template_id = ".$_REQUEST['template_id']." AND `claimant_id`= ".$company_id." ";
		$result = $regular_con -> regular_invoice_form_sql($pdo,$company_id,$sql);

	}
}

	
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['send_type'] != NULL) {
	$send_type = htmlspecialchars($_REQUEST['send_type'],ENT_QUOTES);
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND destination_id = ".$destination_id;
}

$search_conditions = $words;

//フラグに取得する内容を入れる。
$flag = "invoice_form_data";
$words = " GROUP BY `template_id` ORDER BY `template_id` ASC";
$regular_data_arr = $regular_con -> regular_invoice_form_sql_flag($pdo,$flag,$company_id,$words);
$regular_data_num = count($regular_data_arr);
$flag = "";//フラグの初期化
//var_dump($regular_data_arr);


//▼情報メール送信関連処理▼
//自社企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_self_name = $company_self_arr[0]['company_name'];
}

//var_dump($_REQUEST);
//発行依頼を受けた請求データを変数に入れてダウンロードパスワードを生成し、
//DBのINVOICE_DATA_TABLEに登録する。
if ($send_type == 7) {
	echo "<script type='text/javascript'>if (confirm('本当に削除しますか？')==true){alert('削除実行します')}</script>";

}
if ($send_type != 6 && $send_type != 7 && $send_type != NULL) {
	echo "<script type='text/javascript'>alert('情報を送信します');</script>";
}

$row_num = $_REQUEST['uriagekanri_num'];//echo "<br/>";
//改良の余地あり。送信されるデータの数をチェックする
for ($k = 0; $k < $row_num;$k++) {
	//echo "rownum".$k."<br/>";
	if ($_REQUEST['uriagekanri'.$k] == 1) {
		$send_count++;
		//echo "urikan".$k."<br/>";
	}
}

?>

<script type="text/javascript" src="../js/table-sorter-head.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function allCheck(){
	var check =  document.form.uriagekanriA.checked;
	for (var i = 0; i < parseInt(document.form.uriagekanri_num.value); i++){
		var uriage = "uriagekanri" + i
    	document.getElementById(uriage).checked = check;
	}
}
//-->
</script>

<script>

function OpenMailFrame(){
	window.scrollTo(0,0);
	document.getElementById("Mail").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenPostFrame(){
	window.scrollTo(0,0);
	document.getElementById("Post").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenDeleteFrame(){
	window.scrollTo(0,0);
	document.getElementById("Delete").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenIssueFrame(){
	window.scrollTo(0,0);
	document.getElementById("Issue").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}

function deleteTemplate() {
	
}

function toEdit(clm,c1,c2,det,bd,ic,pd,tl_id) {
	location.href="./makeinvoice_edit?clm="+clm+"&c1="+c1+"&c2="+c2+"&det="+det+"&bd="+bd+"&ic="+ic+"&pd="+pd+"&tl_id="+tl_id+"&st=0&edit_int=1";
}

</script>

<iframe id="Mail" src="./frame/mail"></iframe>
<iframe id="Mail2" src="./frame/mail2"></iframe>
<iframe id="Post" src="./frame/post"></iframe>
<iframe id="Post2" src="./frame/post2"></iframe>
<iframe id="Delete" src="./frame/delete"></iframe>
<iframe id="Delete2" src="./frame/delete2"></iframe>
<iframe id="Issue" src="./frame/issue"></iframe>
<iframe id="Issue2" src="./frame/issue2"></iframe>

<title>定期請求書 - Cloud Invoice</title>
<article style="margin:0 auto 60px auto;">

	<section id="m-3-box">
		<h2>
			定期請求書
		</h2>
		
		<a href="./makeinvoice"><input id="newinterval" type="button" value="請求書新規作成" /></a>
		
		<table id="interval" class="tables tablesorter">
		
			<thead>
				<tr id="first">
					<th>宛先</th>
					<th>請求書名</th>
					<th>支払金額</th>
					<th>スケジュール</th>
					<th>有効期間</th>
					<th>次回作成日</th>
					<th>編集</th>
					<th>状態</th>
					<th>削除</th>
				</tr>
			</thead>
			<tbody>
			<?php
			for ($i = 0; $i < count($regular_data_arr); $i++) {
				unset($company_send_code_check_arr);
				unset($company_arr);
			?>
				<tr>
					<!--
					<?php
					$destination_id = $regular_data_arr[$i]['destination_id'];
					if ($regular_data_arr[$i]['destination_id'] != 0) {
						echo substr($destination_id,0,4)." ".substr($destination_id,4,4)." ".substr($destination_id,8,4);
					}
					?>
					<br/>
					-->
					<?php
					//送付先企業データの取得1▼
					if ($destination_id != "" && $destination_id != 0) {
						$flag = "company_data";
						$company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
					}
					?>

					<td class="name">
					<!--
					<?php 
					//if ($regular_data_arr[$i]["send_code"] != 0) {
						echo $send_code = $regular_data_arr[$i]["send_code"];
					//}
					?>
					<br/>
					-->
					<?php
					//if ($send_code != 0) {
						//送付先企業データの取得2▼
						if ($send_code != "") {
							$flag = "addressee_data_one";
							$conditions = " AND company_id = ".$company_id." ";
							$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
							//var_dump($company_send_code_check_arr);
							//var_dump($company_arr);
							echo $company_send_code_check_arr[0]['company_name'];
						}
					
					//}
					?>
					<?php
					//送付先企業データ表示▼
					if ($destination_id != "" && $destination_id != 0) {
						if ($company_send_code_check_arr[0]['company_name'] != "") {
							echo "<br/>(".$company_arr[0]['company_name'].")";
						} else {
							echo $company_arr[0]['company_name'];
						}
					}
					?>
					</td>
					<td class="name">
					<?php echo $template_name = $regular_data_arr[$i]["template_name"];?>
					</td>
					<td class="money">
					<?php echo number_format($regular_data_arr[$i]["total_price"])."円";?>
					</td>

					<td class="schedule">
					<?php 
					if ($regular_data_arr[$i]['template_cycle'] != 0) {
						$template_cycle = $regular_data_arr[$i]['template_cycle'];
						if ($template_cycle == 0) {
							echo "毎週";
						} else if ($template_cycle == 1 ) {
							echo "毎月";
						} else if ($template_cycle == 2) {
							echo "隔月";
						} else if ($template_cycle == 3) {
							echo "3ヶ月毎";
						} else if ($template_cycle == 6) {
							echo "半年毎";
						} else if ($template_cycle == 12) {
							echo "1年毎";
						} else {
							echo "なし";
						}
					}
					?>
					 / 
					<?php
					if ($regular_data_arr[$i]['reservation_date'] != 0) {
						echo $regular_data_arr[$i]['reservation_date'];
					} else if ($regular_data_arr[$i]['reservation_date'] == 0) {
						echo "月末";
					}
					?>
					</td>
					<td class="period"><?php if ($regular_data_arr[$i]['start_date'] != 0) {echo $regular_data_arr[$i]['start_date'];}?> ～ <?php if ($regular_data_arr[$i]['start_date'] != 0) {echo $regular_data_arr[$i]['end_date'];}?></form></td>
					<td class="date"><?php echo $regular_data_arr[$i]['next_creation_date'];?></td>
					<?php
					
					//フラグに取得する内容を入れる。
					$flag = "invoice_form_data_num";
					$words = "AND `template_id`= ".$regular_data_arr[$i]['template_id']."  ORDER BY `template_id` ASC";
					$regular_data_detail_num = $regular_con -> regular_invoice_form_sql_flag($pdo,$flag,$company_id,$words);
					$flag = "";//フラグの初期化
					//var_dump($regular_data_detail_num);
					$detail_num = $regular_data_detail_num[0]["count(*)"];
					$bd = $regular_data_arr[$i]["billing_date"];
					$ic = $regular_data_arr[$i]["invoice_code"];
					$pd = $regular_data_arr[$i]["pay_date"];
					$tl_id = $regular_data_arr[$i]["template_id"];
					//$ind = $regular_data_arr[$i]["billing_date"];
					?>
					<td class="edit"><input type="button" value="編集" onclick="toEdit(<?php echo $company_id.','.$destination_id.',\''.$send_code.'\','.$detail_num.',\''.$bd.'\',\''.$ic.'\',\''.$pd.'\',\''.$tl_id.'\'';?>)"/></td>
					<td class="edit"><form action="" method="post" ><input type="hidden" name="template_id" value="<?php echo $regular_data_arr[$i]['template_id'];?>" /><input type="hidden" name="suspended" value="<?php echo $regular_data_arr[$i]['suspended'];?>" /><input type="hidden" name="suspended_flag" value="1" /><input type="button" <?php if ($regular_data_arr[$i]['suspended'] == 0) { echo 'value="稼働中" class="yellow"';} else { echo 'value="停止中" class="red"';}?> onclick="submit();" /></form></td>
					<td class="edit"><form action="" method="post" ><input type="hidden" name="template_id" value="<?php echo $regular_data_arr[$i]['template_id'];?>" /><input type="hidden" name="del_flag" value="1" /><input type="button" value="削除" class="red" onclick="submit();" /></form></td>
				</tr>
			<?php
			}
			?>
			</tbody>
		
		</table>
	</section>
</article>
<div id="fadeLayer" onclick="CloseFrame(3)"></div>
<div id="noneLayer"></div>

<?php require("footer.php");?>