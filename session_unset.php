<?php
session_start();

//セッションをリセットする
try {
	unset($_SESSION["check"]);
	unset($_SESSION["site_title"]);
	unset($_SESSION["site_id"]);
	unset($_SESSION["record_kbn"]);
	unset($_SESSION["max_row"]);
	unset($_SESSION["max_col"]);
	unset($_SESSION["page_now"]);
	unset($_SESSION["last_page"]);

}catch (Exception $e){
    echo "select error";
}
header("Location:http://hikaku.jp/invoice/mypage.php");
exit();

?>