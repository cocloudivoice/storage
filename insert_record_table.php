<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../cfiles/ReadCsvFile.class.php');
require_once(dirname(__FILE__).'/../pdf/PdfToImg.class.php');

//自動仕訳用クラスの読み込み
require_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');


//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$auto_journalize_con = new auto_journalize_control();
$read_csv_con = new ReadCsvFile();


//変数の宣言
$ahead_url = htmlspecialchars($_REQUEST['ahead_url'], ENT_QUOTES);
$return_url = htmlspecialchars($_REQUEST['return_url'], ENT_QUOTES);
$filepath = htmlspecialchars($_REQUEST['path'], ENT_QUOTES);
$file_name = htmlspecialchars($_FILES['upfile']['name'][0], ENT_QUOTES);
$success_num = 0;
$all_num = 0;
$update_flag = 0;
$update_num = 0;
$insert_num = 0;
$insert_com_num = 0;
//var_dump($filepath);
//var_dump($file_name);

if ($file_name == "") {
	$filepath = dirname(__FILE__).'/../files/upfiles/csvfiles';
	$file_name = "insert_record_table.csv";
}

//header('Content-Type: text/plain; charset=utf-8');

try {

    // 未定義である・複数ファイルである・$_FILES Corruption 攻撃を受けた
    // どれかに該当していれば不正なパラメータとして処理する

    if (
        !isset($_FILES['upfile']['error'][0]) ||
        !is_int($_FILES['upfile']['error'][0])
    ) {
        throw new RuntimeException('パラメータが不正です');
    }

    // $_FILES['upfile']['error'][0] の値を確認
    switch ($_FILES['upfile']['error'][0]) {
        case UPLOAD_ERR_OK: // OK
            break;
        case UPLOAD_ERR_NO_FILE:   // ファイル未選択
            throw new RuntimeException('ファイルが選択されていません');
        case UPLOAD_ERR_INI_SIZE:  // php.ini定義の最大サイズ超過
        case UPLOAD_ERR_FORM_SIZE: // フォーム定義の最大サイズ超過
            throw new RuntimeException('ファイルサイズが大きすぎます');
        default:
            throw new RuntimeException('その他のエラーが発生しました');
    }

    // ここで定義するサイズ上限のオーバーチェック
    // (必要がある場合のみ)
    if ($_FILES['upfile']['size'][0] > 20000000) {
        throw new RuntimeException('ファイルサイズが大きすぎます');
    }
/*
    // $_FILES['upfile']['mime']の値はブラウザ側で偽装可能なので
    // MIMEタイプに対応する拡張子を自前で取得する
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (!$ext = array_search(
        $finfo->file($_FILES['upfile']['tmp_name'][0]),
        array(
            'gif' => 'image/gif',
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
        ),
        true
    )) {
        throw new RuntimeException('ファイル形式が不正です');
    }
*/
    // ファイルデータからSHA-1ハッシュを取ってファイル名を決定し、保存する
    if (!move_uploaded_file($_FILES['upfile']['tmp_name'][0], $filepath. "/" . $file_name)) {
        throw new RuntimeException('ファイル保存時にエラーが発生しました');
    }

    // ファイルのパーミッションを確実に0644に設定する
    chmod($path, 0644);

    echo 'ファイルは正常にアップロードされました';

} catch (RuntimeException $e) {

    echo $e->getMessage();
    echo "<br/>\r\n";
    //var_dump($_FILES);
    if (!isset($_FILES)) {
    	exit;
    } else {
    	echo "ファイルがアップロードされていません。<br/>\r\n";
    }

}

//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$all_num = count($csv) - 1;

//var_dump($csv);
//ob_start();

//CSVデータを一行ずつ取り出してDBに登録する。
foreach ($csv as $value) {

	//var_dump($value);
	$auto_arr = array();
	if ($value[0] != "") {
		$val_box = $value[0] * 1;
		if ($val_box > 0){
			echo "id照合<br/>";
			$recording_company_id = $value[0];
			//企業データ存在確認（元側）▼
			$flag = "company_data";
			$words = "";
			$recording_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$recording_company_id,$words);
			$recording_company_id = $recording_company_data_arr[0]['company_id'];
		} else {
			echo "名前照合<br/>";
			$recording_company_name = $value[0];
			//企業データ存在確認（元側）▼
			$flag = "company_data_from_name";
			$words = "";
			$recording_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$recording_company_name,$words);
			$recording_company_id = $recording_company_data_arr[0]['company_id'];
		}
		if ($recording_company_id == "" || $recording_company_id == NULL) {
			echo "登録元の企業".$value[0]."のデータが存在しません";
			exit();
		}
	} else {
		//存在フラグの初期化
		$exist_flag = 0;

		$company_name = $value[3];
		$product_name = $value[4];
		//echo "<br/>\r\n";
		//var_dump($company_data_arr);
		//企業データ存在確認▼
		$flag = "company_data_from_name";
		$words = "";
		$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_name,$words);
		//var_dump($company_data_arr);
		if(isset($company_data_arr[0]['company_name'])){
			echo "会社名前あり<br/>\r\n";
			$exist_flag = 1;
		} else {echo "名前なし(新規登録)<br/>\r\n";}
		//企業データ存在確認▲
		$company_id = $claimant_id = $company_data_arr[0]['company_id'];
		
		$table_name = "RECORD".$recording_company_id;
		$search_key = $claimant_id." ".$product_name;
		//search_keyで自社のRECORDテーブルに登録があるかどうか確認する
		//配列を取得
		$auto_arr = $auto_journalize_con -> auto_journalize_select($pdo_aj,$table_name,$search_key);
		/*
		$sql= "SELECT MAX(`id`) FROM `".$table_name."`";
		$max_id_arr = $auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);
		$max_id = $max_id_arr[0]["MAX(`id`)"];
		*/
		//echo "サーチキー有無：".$auto_arr['search_key'];
		if ($auto_arr['search_key'] == NULL) {
			try {
				$update_flag = 0;
			}catch (Exception $e) {
				echo $e -> getMessage;
			}
		} else {
			$update_flag = 1;
		}
		//var_dump($auto_arr);
		//echo "<br><br><br>";
		
		//値の無いものをint型のカラムに合わせる処理
		$int_arr = array(2,5,6,7,8,20,21,22,30,31,32,40,41,42,50,51,52,60,61,62,70,71,72,80,81,82,90,91,92,100,101,102,110,111,112,120,121,122,130,131,132,140,141,142,150,151,152,160,161,162,170,171,172,180,181,182,190,191,192,200,201,202,210,211,212);
		foreach ($int_arr as $int_num) {
			if($value[$int_num] == "" || $value[$int_num] == NULL){
				$value[$int_num] = 0;
			}
		}
		if (isset($max_id)) {
			$auto_arr['id'] = $max_id;
		}
		$auto_arr['search_key'] = $search_key;
		$auto_arr['company_id'] = $company_id;
		$auto_arr['company_name'] = $company_name;
		$auto_arr['product_name'] = $product_name;
		$auto_arr['flag1'] = $value[5];
		$auto_arr['flag2'] = $value[6];
		$auto_arr['flag3'] = $value[7];
		$auto_arr['base_amount_of_money'] = $value[8];
		$auto_arr['text_data1'] = $value[9];
		$auto_arr['text_data2'] = $value[10];
		$auto_arr['text_data3'] = $value[11];
		$auto_arr['remarks'] = $value[12];//.date('Y-m-d H:i:s');//strtotime("%Y/%m/%d %H:%M:%S", time());//$value[8];
		$auto_arr['debit_section_code'] = $value[13];
		$auto_arr['debit_section_name'] = $value[14];
		$auto_arr['debit_account_code'] = $value[15];
		$auto_arr['debit_account_name'] = $value[16];
		$auto_arr['debit_sub_account_code'] = $value[17];
		$auto_arr['debit_sub_account_name'] = $value[18];
		$auto_arr['debit_ratio'] = $value[19];
		$auto_arr['debit_flag1'] = $value[20];
		$auto_arr['debit_flag2'] = $value[21];
		$auto_arr['debit_flag3'] = $value[22];
		$auto_arr['credit_section_code'] = $value[23];
		$auto_arr['credit_section_name'] = $value[24];
		$auto_arr['credit_account_code'] = $value[25];
		$auto_arr['credit_account_name'] = $value[26];
		$auto_arr['credit_sub_account_code'] = $value[27];
		$auto_arr['credit_sub_account_name'] = $value[28];
		$auto_arr['credit_ratio'] = $value[29];
		$auto_arr['credit_flag1'] = $value[30];
		$auto_arr['credit_flag2'] = $value[31];
		$auto_arr['credit_flag3'] = $value[32];
		$auto_arr['debit_section_code1'] = $value[33];
		$auto_arr['debit_section_name1'] = $value[34];
		$auto_arr['debit_account_code1'] = $value[35];
		$auto_arr['debit_account_name1'] = $value[36];
		$auto_arr['debit_sub_account_code1'] = $value[37];
		$auto_arr['debit_sub_account_name1'] = $value[38];
		$auto_arr['debit_ratio1'] = $value[39];
		$auto_arr['debit_flag1_1'] = $value[40];
		$auto_arr['debit_flag1_2'] = $value[41];
		$auto_arr['debit_flag1_3'] = $value[42];
		$auto_arr['credit_section_code1'] = $value[43];
		$auto_arr['credit_section_name1'] = $value[44];
		$auto_arr['credit_account_code1'] = $value[45];
		$auto_arr['credit_account_name1'] = $value[46];
		$auto_arr['credit_sub_account_code1'] = $value[47];
		$auto_arr['credit_sub_account_name1'] = $value[48];
		$auto_arr['credit_ratio1'] = $value[49];
		$auto_arr['credit_flag1_1'] = $value[50];
		$auto_arr['credit_flag1_2'] = $value[51];
		$auto_arr['credit_flag1_3'] = $value[52];
		$auto_arr['debit_section_code2'] = $value[53];
		$auto_arr['debit_section_name2'] = $value[54];
		$auto_arr['debit_account_code2'] = $value[55];
		$auto_arr['debit_account_name2'] = $value[56];
		$auto_arr['debit_sub_account_code2'] = $value[57];
		$auto_arr['debit_sub_account_name2'] = $value[58];
		$auto_arr['debit_ratio2'] = $value[59];
		$auto_arr['debit_flag2_1'] = $value[60];
		$auto_arr['debit_flag2_2'] = $value[61];
		$auto_arr['debit_flag2_3'] = $value[62];
		$auto_arr['credit_section_code2'] = $value[63];
		$auto_arr['credit_section_name2'] = $value[64];
		$auto_arr['credit_account_code2'] = $value[65];
		$auto_arr['credit_account_name2'] = $value[66];
		$auto_arr['credit_sub_account_code2'] = $value[67];
		$auto_arr['credit_sub_account_name2'] = $value[68];
		$auto_arr['credit_ratio2'] = $value[69];
		$auto_arr['credit_flag2_1'] = $value[70];
		$auto_arr['credit_flag2_2'] = $value[71];
		$auto_arr['credit_flag2_3'] = $value[72];
		$auto_arr['debit_section_code3'] = $value[73];
		$auto_arr['debit_section_name3'] = $value[74];
		$auto_arr['debit_account_code3'] = $value[75];
		$auto_arr['debit_account_name3'] = $value[76];
		$auto_arr['debit_sub_account_code3'] = $value[77];
		$auto_arr['debit_sub_account_name3'] = $value[78];
		$auto_arr['debit_ratio3'] = $value[79];
		$auto_arr['debit_flag3_1'] = $value[80];
		$auto_arr['debit_flag3_2'] = $value[81];
		$auto_arr['debit_flag3_3'] = $value[82];
		$auto_arr['credit_section_code3'] = $value[83];
		$auto_arr['credit_section_name3'] = $value[84];
		$auto_arr['credit_account_code3'] = $value[85];
		$auto_arr['credit_account_name3'] = $value[86];
		$auto_arr['credit_sub_account_code3'] = $value[87];
		$auto_arr['credit_sub_account_name3'] = $value[88];
		$auto_arr['credit_ratio3'] = $value[89];
		$auto_arr['credit_flag3_1'] = $value[90];
		$auto_arr['credit_flag3_2'] = $value[91];
		$auto_arr['credit_flag3_3'] = $value[92];
		$auto_arr['debit_section_code4'] = $value[93];
		$auto_arr['debit_section_name4'] = $value[94];
		$auto_arr['debit_account_code4'] = $value[95];
		$auto_arr['debit_account_name4'] = $value[96];
		$auto_arr['debit_sub_account_code4'] = $value[97];
		$auto_arr['debit_sub_account_name4'] = $value[98];
		$auto_arr['debit_ratio4'] = $value[99];
		$auto_arr['debit_flag4_1'] = $value[100];
		$auto_arr['debit_flag4_2'] = $value[101];
		$auto_arr['debit_flag4_3'] = $value[102];
		$auto_arr['credit_section_code4'] = $value[103];
		$auto_arr['credit_section_name4'] = $value[104];
		$auto_arr['credit_account_code4'] = $value[105];
		$auto_arr['credit_account_name4'] = $value[106];
		$auto_arr['credit_sub_account_code4'] = $value[107];
		$auto_arr['credit_sub_account_name4'] = $value[108];
		$auto_arr['credit_ratio4'] = $value[109];
		$auto_arr['credit_flag4_1'] = $value[110];
		$auto_arr['credit_flag4_2'] = $value[111];
		$auto_arr['credit_flag4_3'] = $value[112];
		$auto_arr['debit_section_code5'] = $value[113];
		$auto_arr['debit_section_name5'] = $value[114];
		$auto_arr['debit_account_code5'] = $value[115];
		$auto_arr['debit_account_name5'] = $value[116];
		$auto_arr['debit_sub_account_code5'] = $value[117];
		$auto_arr['debit_sub_account_name5'] = $value[118];
		$auto_arr['debit_ratio5'] = $value[119];
		$auto_arr['debit_flag5_1'] = $value[120];
		$auto_arr['debit_flag5_2'] = $value[121];
		$auto_arr['debit_flag5_3'] = $value[122];
		$auto_arr['credit_section_code5'] = $value[123];
		$auto_arr['credit_section_name5'] = $value[124];
		$auto_arr['credit_account_code5'] = $value[125];
		$auto_arr['credit_account_name5'] = $value[126];
		$auto_arr['credit_sub_account_code5'] = $value[127];
		$auto_arr['credit_sub_account_name5'] = $value[128];
		$auto_arr['credit_ratio5'] = $value[129];
		$auto_arr['credit_flag5_1'] = $value[130];
		$auto_arr['credit_flag5_2'] = $value[131];
		$auto_arr['credit_flag5_3'] = $value[132];
		$auto_arr['debit_section_code6'] = $value[133];
		$auto_arr['debit_section_name6'] = $value[134];
		$auto_arr['debit_account_code6'] = $value[135];
		$auto_arr['debit_account_name6'] = $value[136];
		$auto_arr['debit_sub_account_code6'] = $value[137];
		$auto_arr['debit_sub_account_name6'] = $value[138];
		$auto_arr['debit_ratio6'] = $value[139];
		$auto_arr['debit_flag6_1'] = $value[140];
		$auto_arr['debit_flag6_2'] = $value[141];
		$auto_arr['debit_flag6_3'] = $value[142];
		$auto_arr['credit_section_code6'] = $value[143];
		$auto_arr['credit_section_name6'] = $value[144];
		$auto_arr['credit_account_code6'] = $value[145];
		$auto_arr['credit_account_name6'] = $value[146];
		$auto_arr['credit_sub_account_code6'] = $value[147];
		$auto_arr['credit_sub_account_name6'] = $value[148];
		$auto_arr['credit_ratio6'] = $value[149];
		$auto_arr['credit_flag6_1'] = $value[150];
		$auto_arr['credit_flag6_2'] = $value[151];
		$auto_arr['credit_flag6_3'] = $value[152];
		$auto_arr['debit_section_code7'] = $value[153];
		$auto_arr['debit_section_name7'] = $value[154];
		$auto_arr['debit_account_code7'] = $value[155];
		$auto_arr['debit_account_name7'] = $value[156];
		$auto_arr['debit_sub_account_code7'] = $value[157];
		$auto_arr['debit_sub_account_name7'] = $value[158];
		$auto_arr['debit_ratio7'] = $value[159];
		$auto_arr['debit_flag7_1'] = $value[160];
		$auto_arr['debit_flag7_2'] = $value[161];
		$auto_arr['debit_flag7_3'] = $value[162];
		$auto_arr['credit_section_code7'] = $value[163];
		$auto_arr['credit_section_name7'] = $value[164];
		$auto_arr['credit_account_code7'] = $value[165];
		$auto_arr['credit_account_name7'] = $value[166];
		$auto_arr['credit_sub_account_code7'] = $value[167];
		$auto_arr['credit_sub_account_name7'] = $value[168];
		$auto_arr['credit_ratio7'] = $value[169];
		$auto_arr['credit_flag7_1'] = $value[170];
		$auto_arr['credit_flag7_2'] = $value[171];
		$auto_arr['credit_flag7_3'] = $value[172];
		$auto_arr['debit_section_code8'] = $value[173];
		$auto_arr['debit_section_name8'] = $value[174];
		$auto_arr['debit_account_code8'] = $value[175];
		$auto_arr['debit_account_name8'] = $value[176];
		$auto_arr['debit_sub_account_code8'] = $value[177];
		$auto_arr['debit_sub_account_name8'] = $value[178];
		$auto_arr['debit_ratio8'] = $value[179];
		$auto_arr['debit_flag8_1'] = $value[180];
		$auto_arr['debit_flag8_2'] = $value[181];
		$auto_arr['debit_flag8_3'] = $value[182];
		$auto_arr['credit_section_code8'] = $value[183];
		$auto_arr['credit_section_name8'] = $value[184];
		$auto_arr['credit_account_code8'] = $value[185];
		$auto_arr['credit_account_name8'] = $value[186];
		$auto_arr['credit_sub_account_code8'] = $value[187];
		$auto_arr['credit_sub_account_name8'] = $value[188];
		$auto_arr['credit_ratio8'] = $value[189];
		$auto_arr['credit_flag8_1'] = $value[190];
		$auto_arr['credit_flag8_2'] = $value[191];
		$auto_arr['credit_flag8_3'] = $value[192];
		$auto_arr['debit_section_code9'] = $value[193];
		$auto_arr['debit_section_name9'] = $value[194];
		$auto_arr['debit_account_code9'] = $value[195];
		$auto_arr['debit_account_name9'] = $value[196];
		$auto_arr['debit_sub_account_code9'] = $value[197];
		$auto_arr['debit_sub_account_name9'] = $value[198];
		$auto_arr['debit_ratio9'] = $value[199];
		$auto_arr['debit_flag9_1'] = $value[200];
		$auto_arr['debit_flag9_2'] = $value[201];
		$auto_arr['debit_flag9_3'] = $value[202];
		$auto_arr['credit_section_code9'] = $value[203];
		$auto_arr['credit_section_name9'] = $value[204];
		$auto_arr['credit_account_code9'] = $value[205];
		$auto_arr['credit_account_name9'] = $value[206];
		$auto_arr['credit_sub_account_code9'] = $value[207];
		$auto_arr['credit_sub_account_name9'] = $value[208];
		$auto_arr['credit_ratio9'] = $value[209];
		$auto_arr['credit_flag9_1'] = $value[210];
		$auto_arr['credit_flag9_2'] = $value[211];
		$auto_arr['credit_flag9_3'] = $value[212];
		
		//var_dump($auto_arr);
		//変更するデータを配列に入れて更新もしくは挿入
		//echo $auto_arr['search_key'];
		if ($update_flag == 0) {
			//新規のデータの場合（新規作成）
			$result = $auto_journalize_con -> auto_journalize_insert($pdo_aj,$table_name,$auto_arr);
			//var_dump($result);
			echo "新規<br/>\r\n";
			$insert_num++;
		} else if ($update_flag == 1) {
			//既存のデータの場合（上書き）
			$result = $auto_journalize_con -> auto_journalize_update($pdo_aj,$table_name,$auto_arr);
			echo "上書き<br/>\r\n";
			$update_num++;
		}
		
		//var_dump($result);
		if ($exist_flag == 0) {
			//echo "企業名が存在しない時の処理<br/>\r\n";

			//仮メールアドレス生成
			$mail_address = md5(random(12)).rand(4)."@co.cloudinvoice.co.jp";
			$user_con = new user_control();
			//Emailアドレスの存在を確認する。		
			$login_arr = $user_con -> user_select_email($pdo,$mail_address);
			if ($login_arr != NULL) {
				$login_arr = $login_con -> user_select_email($pdo,$mail_address);
				//$passwordmaker = random(12);
				$passwordmaker = uniqid();
				$login_arr['password'] = md5($passwordmaker);
				$login_con -> user_update($pdo,$login_arr);
				$login_arr = $login_con -> user_select_email($pdo,$mail_address);
			}
			
			$user_arr = $user_con -> user_select($pdo,$mail_address);

			//ユーザー登録
			$user_arr['nick_name']   = $user_name;
			$user_arr['mail_address'] = $mail_address;
			$user_arr['tel_num'] = $phone_number;
			$user_arr['non_regular_flag'] = $non_regular_flag;
			
			//共通コードの生成と取得
			if (!$user_arr['user_id']){
				$user_num = $user_con -> user_get_new_num($pdo);
				$user_arr['user_id'] = $user_num;
			}
			//ユーザーテーブルに新規作成
			$result = $user_con -> user_insert($pdo,$user_arr);
			//企業データ登録▼
			$company_id = "".$user_arr['user_id'];
			$sql = "
			INSERT INTO `COMPANY_TABLE` ( 
				`company_id`, `company_name`, `email`, `tel_num` , `non_regular_flag`
			) VALUES ( 
				".$user_arr['user_id'].", '".$user_arr['nick_name']."','".$user_arr['mail_address']."','".$user_arr['tel_num']."',".$user_arr['non_regular_flag']."
			)";
			//カンパニーテーブルに新規作成
			$company_arr = $company_con -> company_sql($pdo,$company_id,$sql);
			$insert_com_num++;
			//企業データ登録▲
			
			//▼自動仕訳用企業DB登録▼
			//企業品目テーブルと企業用レコードテーブルを登録
			$table_name = "".sprintf("%012d", $company_id);
			try {
				$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
			} catch (PDOException $e) {
			    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
			    $_SESSION['error_msg'] = $e -> getMessage();
			}
			//▲自動仕訳用企業DB登録▲

		} else {
			//企業アカウントが存在する時
			$company_id = $company_data_arr[0]['company_id'];
		}
	}
}
echo "<br/>\r\n";
echo "新規仕訳登録".$insert_num."件<br/>\r\n";
echo "上書き仕訳登録".$update_num."件<br/>\r\n";
echo "新規企業登録".$insert_com_num."件<br/>\r\n";

fclose($temp);
unlink($file);

//ob_end_flush();

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>レコードテーブルへのデータ追加完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">レコードテーブルへのデータ追加完了</h1>
		<h2>レコードテーブルへのデータ追加が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./record_table_upload'">レコードデータのアップロード画面に戻る</button>
	</div>
</body>
</html>