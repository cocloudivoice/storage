<?php require("header.php");?>
<?php 
//クラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。

//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_total_data_send";

//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = $_REQUEST['destination_id'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['send_code'] != NULL) {
	$send_code = $_REQUEST['send_code'];
	$words .= " AND send_code = '".$send_code."' ";
}
if ($_REQUEST['status'] != NULL) {
	$status = $_REQUEST['status'];
	$words .= " AND status = ".$status;
} else {
	$words .= " AND (status = 1 OR status = 0) ";
}
if ($_REQUEST['insert_date_from'] != NULL) {
	$insert_date_from = str_replace(" ","",substr($_REQUEST['insert_date_from'],0,4)."/".substr($_REQUEST['insert_date_from'],4,2)."/".substr($_REQUEST['insert_date_from'],6,2));
	$words .= " AND insert_date >= ".$insert_date_from;
}
if ($_REQUEST['insert_date_to'] != NULL) {
	$insert_date_to = str_replace(" ","",substr($_REQUEST['insert_date_to'],0,4)."/".substr($_REQUEST['insert_date_to'],4,2)."/".substr($_REQUEST['insert_date_to'],6,2));
	$words .= " AND insert_date <= ".$insert_date_to;
}
if ($_REQUEST['billing_date_from'] != NULL) {
	$billing_date_from = $_REQUEST['billing_date_from'];
	$words .= " AND billing_date >= ".$billing_date_from;
}
if ($_REQUEST['billing_date_to'] != NULL) {
	$billing_date_to = $_REQUEST['billing_date_to'];
	$words .= " AND billing_date <= ".$billing_date_to;
}
if ($_REQUEST['pay_date_from'] != NULL) {
	$pay_date_from = $_REQUEST['pay_date_from'];
	$words .= " AND pay_date >= ".$pay_date_from;
}
if ($_REQUEST['pay_date_to'] != NULL) {
	$pay_date_to = $_REQUEST['pay_date_to'];
	$words .= " AND pay_date <= ".$pay_date_to;
}

//var_dump($_REQUEST);

//請求データの取得
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
$flag = "";//フラグの初期化


	//var_dump($_REQUEST);
	//var_dump($invoice_data_receive_arr);
	//var_dump($invoice_data_receive_arr_count);


//自社企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_self_name = $company_self_arr[0]['company_name'];
}


//var_dump($_REQUEST);
//発行依頼を受けた請求データを変数に入れてダウンロードパスワードを生成し、
//DBのINVOICE_DATA_TABLEに登録する。
$row_num = $_REQUEST['uriagekanri_num'];//echo "<br/>";
for ($k = 0; $k < $row_num;$k++) {
	if ($_REQUEST['uriagekanri'.$k] == 1) {
		echo "testinvoice<br/>";
		$send_num = $k;
		$destination_id = $_REQUEST['destination_id'.$k];
		$send_code = $_REQUEST['send_code'.$k];
		$company_name = $_REQUEST['company_name'.$k];
		$billing_date = $_REQUEST['billing_date'.$k];
		$invoice_code = $_REQUEST['invoice_code'.$k];
		$invoice_name = $_REQUEST['invoice_name'.$k];
		$pay_date = $_REQUEST['pay_date'.$k];
		$timestamp_for_code = date("YmdHis");
		$download_password = md5($company_code.$destination_id.$invoice_code.$timestamp_for_code);
		$flag = "up_invoice_download_password";
		$words = " `download_password` = '".$download_password."' WHERE (`destination_id` = ".$destination_id." OR `send_code` = ".$send_code.") AND `claimant_id` = '".$company_id."' AND billing_date = ".$billing_date." AND invoice_code = '".$invoice_code."' AND pay_date = ".$pay_date." AND `status` <> 99 ";
		$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);

		//送付先企業データの取得1▼
		if ($destination_id != "") {
			$flag = "company_data";
			$company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
			//var_dump($company_arr);
			//echo $company_arr[0]["email"];
		}
		//送付先企業データの取得2▼
		if ($send_code != "" && $send_code != 0) {
			$flag = "addressee_data_one";
			$conditions = " AND company_id = ".$company_id." ";
			$company_test_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
			if ($company_test_arr[0]["email"] != NULL){
				$company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
			}
			//var_dump($company_arr);
			//echo $company_arr[0]["email"];
		}
		

		$flag = "invoice_total_data_send_count";
		$words = "'".$invoice_data_receive_arr[$i]['invoice_code']."'";//条件
		$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
		//echo "<br/><br/>test<br/><br/>";
		//var_dump($invoice_data_receive_arr);
		//送付先企業データの取得▲
		
		//メールアドレス
		$mailto = $company_arr[0]['email'];
		//メールに渡す情報
		$download_password;
		$keywords = crypt(md5($mailto.$timestamp_for_code.$download_password));
		$headers = "From: CloudInvoice <ryobita@yahoo.co.jp>\r\n";
		//メールアドレス
		
$message = "".$company_name."様


".$company_self_name."様から請求書が届いております。

下記のURLにアクセスし、請求書PDFのダウンロードをお願いします。


http://www.storage.cloudinvoice.co.jp/invoice/pdf_download.php?key=".$keywords." 
ダウンロードパスワードは、「".$download_password."」です。



------------------------------
Cloud Invoice（クラウドインボイス）は、無料で使える請求書発行・情報管理サービスです。
	
CSVデータの取り込みによってクラウド上で、売上請求書の自動作成、
売上請求書のメール（有料で郵送も）、売上請求書の管理、支払請求書の受取、
支払請求書の管理ができるクラウド請求書発行・請求書情報管理サービスです。

クラウド上での請求書の発行・受け取り、請求データの相互ダウンロードなどで、
煩わしい経理業務の手間を軽減できます。

登録・通常機能は完全無料です。
お気軽にご登録、お試しでのご利用をお待ちしております。
サイトURL: http://www.storage.cloudinvoice.co.jp 

Cloud Invoice
------------------------------
";
		mb_internal_encoding("UTF-8");
		$mail_result = mail(
			$mailto,
			mb_encode_mimeheader($company_self_name."様からの請求書が届いております。", 'ISO-2022-JP-MS'),
			mb_convert_encoding($message, 'ISO-2022-JP-MS'),
			$headers
		);
		if ($mail_result) {
			$flag = "up_invoice_download_password";
			$update_words = " `status` = 1 WHERE `download_password` = '".$download_password."'";
			$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$update_words);
			echo "<script type='text/javascript'>alert('情報を送信しました。');</script>";
		} else {
			echo "<script type='text/javascript'>alert('送信に失敗しました。');</script>";
		}
	}
}

?>

<script language="JavaScript" type="text/javascript">
<!--
function allCheck(){
	var check =  document.form.uriagekanriA.checked;
	for (var i = 0; i < parseInt(document.form.uriagekanri_num.value); i++){
		var uriage = "uriagekanri" + i
    	document.getElementById(uriage).checked = check;
	}
}
//-->
</script>


<script>

function OpenMailFrame(){
	document.getElementById("Mail").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenPostFrame(){
	document.getElementById("Post").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenDeleteFrame(){
	document.getElementById("Delete").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenIssueFrame(){
	document.getElementById("Issue").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}



</script>

<iframe id="Mail" src="./frame/mail"></iframe>
<iframe id="Post" src="./frame/post"></iframe>
<iframe id="Delete" src="./frame/delete"></iframe>
<iframe id="Issue" src="./frame/issue"></iframe>


<article>

	<section id="m-3-box">
		<h2>
			売上管理確認
		</h2>

		<table>
		<form name="form" action="" method="post">
		<?php if($invoice_data_receive_num > 0) { ?>
			<tr id="first">
				<th><label id="checklabel"><input type="checkbox" name="uriagekanriA"  onClick="allCheck();" /></label><input type="hidden" name="uriagekanri_num" value="<?php echo $invoice_data_receive_num;?>"></th>
				<th>共通CD</th>
				<th>管理CD</th>
				<th>宛先</th>
				<th>請求書日付</th>
				<th>請求書番号</th>
				<th>請求書名称</th>
				<th>支払期限</th>
				<th>請求金額</th>
				<th id="meisai">明細数</th>
				<th>csv取込日時</th>
				<th>状態</th>
			</tr>
		<?php
			} else{
				echo "<p>この条件ではデータが見つかりません</p>";
			}
		?>
		<?php
			for ($i = 0; $i < $invoice_data_receive_num; $i++) {
		?>

			<tr>
				<td id="check">
				<?php if ($invoice_data_receive_arr[$i]['status'] != "") { ?>
					<label id="checklabel"><input id="uriagekanri<?php echo $i;?>" type="checkbox" name="uriagekanri<?php echo $i;?>" value="1" /></label>
				<?php }?>
				</td>
				<td id="kyoutuCD"><?php echo $invoice_data_receive_arr[$i]['destination_id'];?><input type="hidden" name="destination_id<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['destination_id'];?>" /></td>
			<?php
				$destination_id = $invoice_data_receive_arr[$i]['destination_id'];
				if ($destination_id != "") {
					$flag = "company_data";
					$company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
					//var_dump($company_arr);
				}

				$flag = "invoice_total_data_send_count";
				$words = "'".$invoice_data_receive_arr[$i]['invoice_code']."'";//条件
				$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
				//echo "<br/><br/>test<br/><br/>";
				//var_dump($invoice_data_receive_arr);
			?>
			<?php
				/*
				//管理コードの表示データ取得
				$flag = "addressee_data";
				$words = "AND company_id = ".$company_id."";
				$client_company_id = "".$invoice_data_receive_arr[$i]['destination_id']."";
				$company_kanri_arr = $company_con -> company_sql_flag($pdo,$flag,$client_company_id,$words);
				var_dump($company_kanri_arr);
				*/
				//var_dump($invoice_data_receive_arr);
			?>
				<td id="kanriCD"><?php echo $invoice_data_receive_arr[$i]['send_code'];?><input type="hidden" name="send_code<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['send_code'];?>" /></td>


				<td id="atesaki"><?php echo $company_arr[0]['company_name'];?><input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $company_arr[0]['company_name'];?>" /></td>
				<td id="seikyuBi"><?php echo $invoice_data_receive_arr[$i]['billing_date'];?><input type="hidden" name="billing_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['billing_date'];?>" /></td>
				<td id="seikyuNo"><?php echo $invoice_data_receive_arr[$i]['invoice_code'];?><input type="hidden" name="invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_code'];?>" /></td>
				<td id="seikyuName"><?php echo $invoice_data_receive_arr[$i]['invoice_name'];?><input type="hidden" name="invoice_name<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_name'];?>" /></td>
				<td id="siharaibi"><?php echo $invoice_data_receive_arr[$i]['pay_date'];?><input type="hidden" name="pay_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['pay_date'];?>" /></td>
				<td id="sumMoney"><?php echo number_format($invoice_data_receive_arr[$i]['sum(total_price)']);?><input type="hidden" name="<?php echo $i;?>" value="<?php echo number_format($invoice_data_receive_arr[$i]['sum(total_price)']);?>" /></td>
				<td id="meisai"><?php echo $invoice_data_receive_arr_count[0]["count(*)"];?><input type="hidden" name="total_price<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr_count[0]["count(*)"];?>" /></td>
				<td id="csvNitiji"><?php echo $invoice_data_receive_arr[$i]['insert_date'];?><input type="hidden" name="insert_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['insert_date'];?>" /></td>
				<td id="status"><input type="hidden" name="status<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['status'];?>" />
				<?php
					if ($invoice_data_receive_arr[$i]['status'] == 0) {
						echo "<a href='./invoice2pdf?c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."' target='_blank'>未発行</a>";
					} else if ($invoice_data_receive_arr[$i]['status'] == 1) {
						echo "<a href='./invoice2pdf?c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."' target='_blank'>発行済</a>";
					} else if ($invoice_data_receive_arr[$i]['status'] == 99) {
						echo "削除済";
					}
				?>
				</td>
			</tr>
		<?php
			}
		?>
				<input type="hidden" name="row_num" value="<?php echo $i;?>" />
				<input type="submit" value="送信" />
		</form>
		</table>

		<div id="uriagekanri">
			<button onclick="OpenMailFrame()">請求書をメール送信する</button>
			<button onclick="OpenPostFrame()">請求書を郵送依頼する</button>
			<button onclick="OpenIssueFrame()">請求書PDFを発行する<br></button>
			<button onclick="OpenDeleteFrame()">請求データを削除する<br>（発行済の請求データは削除できません）</button>
		</div>
	</section>
</article>
<div id="fadeLayer"></div>



<?php require("footer.php");?>