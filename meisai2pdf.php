<?php
session_start();

//クラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//var_dump($_SESSION);
$company_id = $_REQUEST['clm'];
$destination_id = $_REQUEST['c1'];
$send_code = $_REQUEST['c2'];
$detail_num = $_REQUEST['det'];
$billing_date = $_REQUEST['bd'];
$invoice_code = $_REQUEST['ic'];
$pay_date = $_REQUEST['pd'];
$insert_date = $_REQUEST['ind'];
$status = $_REQUEST['st'];

if (isset($_REQUEST['dlp'])) {
	$dlp = $_REQUEST['dlp'];
} else {

	//pdfの存在確認
	$flag = "invoice_get_invoice_total";
	$words = $invoice_code;
	$invoice_data_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	if (isset($invoice_data_arr[0]['download_password'])) {
		$dlp = $invoice_data_arr[0]['download_password'];
	}
}

$param = "c1=".$destination_id."&c2=".$send_code."&bd=".$billing_date."&ic=".$invoice_code."&pd=".$pay_date."&st=".$status."&company_id=".$company_id;
//echo $param;

if (isset($dlp)) {
	$param = "dlp=".$dlp;
	if (isset($_REQUEST['clm'])){
		$sql ="
			SELECT * FROM `INVOICE_DATA_TABLE` 
			WHERE download_password = '".$dlp."' AND `claimant_id` = '".$_REQUEST['clm']."'
		";
	} else {
		$sql ="
			SELECT * FROM `INVOICE_DATA_TABLE` 
			WHERE download_password = '".$dlp."' 
		";
	}
	$invoice_sorce_arr = $invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
	$claimant_id = $company_id = $invoice_sorce_arr[0]['claimant_id'];
	$str = $invoice_sorce_arr[0]['pay_date'];
	
	if ($_REQUEST['downloaded'] == 6) {
		//ステータスをダウンロード済みに変更する。
		$downloaded_status = $_REQUEST['downloaded'];
		$sql ="
		UPDATE `INVOICE_DATA_TABLE` SET `status` = ".$downloaded_status." 
		WHERE download_password = '".$dlp."' AND `claimant_id` = '".$company_id."'
		";
		$invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
	}
	

}
//▼印鑑イメージのための処理▼
$flag = "company_data";
$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$stamp_path = $company_one_arr[0]['stamp_image'];
$template = $company_one_arr[0]['template_type'];
//▲印鑑イメージのための処理▲

/*
$pdf = new myPDF('P', 'mm', 'A4');  // デフォルト単位はmmに指定
$pdf->Ln(30);               // 単位指定されていないのでmm単位に→30mm
$pdf->SetFontSize(12);      // フォントサイズは単位指定無しだと
                            // デフォルト単位に関係なくptに→12pt
$pdf->Ln('3cm');            // 単位指定されているので3cm→30mmと同義
$pdf->SetFontSize('5mm');   // フォントサイズでも単位指定すれば5mm→14.173228346457pt
$pdf->SetFontSize2(5);      // 5mm→14.173228346457pt
                            // フォントサイズでもデフォルト単位が有効なメソッドもあります
*/
//*?destination_id=000000000030&send_code=0&billing_date=20140531&invoice_code=231712-18-552&pay_date=20140630&status=1*/
include("./mpdf/mpdf.php");
//$html = file_get_contents("./invoice/invoicepdf?c1=000000000030&c2=0&bd=20140531&ic=231712-18-552&pd=20140630&st=1&company_id=000000000033");
if ($detail_num <= 10) {
    //明細が10以下の場合
    $html = file_get_contents("https://co.cloudinvoice.co.jp/invoice/invoiceframe/meisai3mpdf?".$param);
} else {
    //明細が11以上の場合
    $html = file_get_contents("https://co.cloudinvoice.co.jp/invoice/invoiceframe/meisai3mpdf2?".$param);
}

//$html .= file_get_contents("./invoice/css/boots.css");
//$mpdf = new mpdf('ja+aCJK');//日本語化
$mpdf = new mPDF('ja',//モード default ''
'A4',//用紙サイズ default''
0,//フォントサイズ default 0
'meiryo',//フォントファミリー
0,//左マージン
0,//右マージン
0,//トップマージン
0,//ボトムマージン
0,//ヘッダーマージン
0,//フッターマージン
'');//L-landscape,P-portrait

$mpdf -> WriteHTML($html);
//画像を入れる
//$mpdf->Image($stamp_path, 160, 65, 18.0);
//$mpdf->Image('../../files/000000000035/images/images_stamp.jpg', 145, 65, 18.0);
//$mpdf->Image('../images/logo.gif', 100, 100, 50.0);
// ウォーターマークを入れる
//$mpdf->SetWatermarkText('DRAFT');
//$mpdf->watermark_font = 'DejaVuSansCondensed';
//$mpdf->showWatermarkText = true;
//スタイルシートを読み込む
/*
$stylesheet = file_get_contents("/var/www/co.cloudinvoice.co.jp/html/invoice/invoiceframe/invoice1mpdf.php" );
$stylesheet = mb_convert_encoding($stylesheet, "UTF-8");
$mpdf->WriteHTML($stylesheet,1);
*/

//発行済み請求書の閲覧
if (isset($dlp)) {
	
	//ファイルの存在を確認する。あればダウンロード、無ければ作成してダウンロードさせて保存。
	$top4 = substr($claimant_id,0,4);
	$mid4 = substr($claimant_id,4,4);
	$pdf_url = "../files/".$top4."/".$mid4."/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$dlp."sm.pdf";
	$png_url = "../files/".$top4."/".$mid4."/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$dlp."sm.png";
	if (file_exists($pdf_url)) {
		//サーバーにPDFファイルが存在する場合
//		download_file($pdf_url);
//		download_file($png_url);
		header("Location:".$pdf_url);
		exit();
	} else {
		//サーバーにPDFファイルが存在しない場合
		//PDFファイルダウンロードさせる。
		$mpdf -> Output(''.$dlp.'sm.pdf', 'D');
		
		//請求書のステータスを変更する
		$downloaded_status = $_REQUEST['downloaded'];
		$sql ="
		UPDATE `INVOICE_DATA_TABLE` SET `status` = ".$downloaded_status." 
		WHERE download_password = '".$dlp."' AND `claimant_id` = '".$invoice_sorce_arr[0]['claimant_id']."' 
		";
		$invoice_con -> invoice_data_sql($pdo,$company_id,$sql);
		
		if (isset($_REQUEST['dlps'])) {
			//PDFファイルを保存する
			$pdf_file = ''.$dlp.'sm.pdf';
			//請求元企業のディレクトリに入る
			//$path = "../files/".$invoice_sorce_arr[0]['claimant_id']."/pdf/".date("Y", strtotime($invoice_sorce_arr[0]['pay_date']))."/".date("m", strtotime($invoice_sorce_arr[0]['pay_date']));
			$path = "../files/".$top4."/".$mid4."/".$invoice_sorce_arr[0]['claimant_id']."/pdf/".date("Y", strtotime($invoice_sorce_arr[0]['pay_date']))."/".date("m", strtotime($invoice_sorce_arr[0]['pay_date']));
			try {
				if (mkdir( $path, 0775, true )) {
					chmod( $path, 0775, true );
					//chgrp($path, "dev", true );
					//echo "ディレクトリ作成成功！！";
				} else {
					//echo "ディレクトリ作成失敗！！";
				}
				
			} catch (Exception $e) { 
				echo $e -> getMessage;
			}
			//ディレクトリを作成する。
			//date("Y", strtotime($pay_date));
			$mpdf -> Output($path."/".$pdf_file."sm", 'F');
			$sql ="
			UPDATE `INVOICE_DATA_TABLE` SET `pdf_url` = ".$path." 
			WHERE download_password = '".$dlp."' AND `claimant_id` = '".$invoice_sorce_arr[0]['claimant_id']."'
			";
			$invoice_con -> invoice_data_sql($pdo,$company_id,$sql);

		}
	}
} else {
	$mpdf -> Output();
}
exit();


/*
set_include_path(get_include_path(). PATH_SEPARATOR .$_SERVER["DOCUMENT_ROOT"] . '/Classes/');
include_once('PHPExcel.php');
include_once('PHPExcel/IOFactory.php');

//include_once('PHPExcel/Writer/Excel2007.php');

// 既存ファイルの読み込みの場合
//$objPHPExcel = PHPExcel_IOFactory::load("../files/common/invoice/sample.xls");

    //PHPExcelをインクルード
//    require_once(dirname(__FILE__) . '/Classes/PHPExcel.php');
//    require_once(dirname(__FILE__) . '/Classes/PHPExcel/IOFactory.php');



    //テンプレート読み込み
    $filepath = "../files/common/invoice/sample.xls";
    $objReader = PHPExcel_IOFactory::createReader('Excel5');
    $book = $objReader->load($filepath);

    //シート設定
    $book->setActiveSheetIndex(0);
    $sheet = $book->getActiveSheet();
    $sheet->setTitle('sheet name'); //シート名指定

    // セルに値を入れる
    $sheet->setCellValue('A1', 'hoge');
    $sheet->setCellValue('A2', 'あいうえおかきくけこさしすせそ');




    // Excel形式で出力する
    $outputFileName = "o.xls";
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');

    $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
    $writer->save('php://output');






// PDF形式で出力する
//    $outputFileName = "o.xls";
    $outputFileName = "o.pdf";
//    header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment;filename="' . $outputFileName . '"');
    header('Cache-Control: max-age=0');

//    $writer = PHPExcel_IOFactory::createWriter($book, "Excel5");
    $writer = PHPExcel_IOFactory::createWriter($book, "PDF");
    $writer->setFont('arialunicid0-japanese');    //日本語文字化け対策
    $writer->save('php://output');
*/

?>

<?php
/*
    引数はターゲットファイルへの相対又は絶対パス
*/
function download_file($path_file) {
    /* ファイルの存在確認 */
    if (!file_exists($path_file)) {
        die("Error: File(".$path_file.") does not exist");
    }

    /* オープンできるか確認 */
    if (!($fp = fopen($path_file, "r"))) {
        die("Error: Cannot open the file(".$path_file.")");
    }
    fclose($fp);

    /* ファイルサイズの確認 */
    if (($content_length = filesize($path_file)) == 0) {
        die("Error: File size is 0.(".$path_file.")");
    }

    /* ダウンロード用のHTTPヘッダ送信 */
    header("Content-Disposition: inline; filename=\"".basename($path_file)."\"");
    header("Content-Length: ".$content_length);
    header("Content-Type: application/octet-stream");
/*
	$mime_type = 'application/pdf';
	// for IE 8 ( see http://support.microsoft.com/kb/323308/ja )
	$expires = 30;
	header("Last-Modified: ". gmdate("D, d M Y H:i:s", filemtime($path_file)). " GMT");
	header("Expires: " . date(DATE_RFC1123, time() + $expires));
	header("Pragma: cache");
	header("Cache-Control: max-age={$expires}");
	header("Cache-Control: cache");
	// for IE 8
	header('Content-Disposition: inline; filename="'.basename($path_file).'"');
	header('Content-Length: '.filesize($path_file));
	header('Content-Type: '.$mime_type);
	readfile($path_file); 
*/






    /* ファイルを読んで出力 */
    if (!readfile($path_file)) {
        die("Cannot read the file(".$path_file.")");
    }
}
?>
