<?php require("header.php");?>


<title>ファイルのアップロード - Cloud Invoice</title>

<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}




?>

<article>
	<section id="m-2-box">
		<h2>
			ファイルのダウンロード
		</h2>
		<p>
		ファイルをダウンロードします。
		</p>
<?php 
	$user_id = sprintf('%012d',$user_id);
	if (isset($_REQUEST['ccd'])) {
		//$_REQUEST['opt'];
		$download_dir_path = $_REQUEST['path'] = "/var/www/co.cloudinvoice.co.jp/html/upfiles/".$user_id."/UpFiles/";
		$path = $_REQUEST['path2'] = "/var/www/co.cloudinvoice.co.jp/html/upfiles/".$user_id."/UpFiles";
		//$_REQUEST['type'];
		//echo "./download_imgs_to_text_all?path=".$download_dir_path."&path2=".$path;
		//header("Location:./download_imgs_to_text_all?path=".$download_dir_path."&path2=".$path,true);
		echo "<script type='text/javascript'>location.href='./download_imgs_to_text_all?path=".$download_dir_path."&type=all'</script>";
		echo "<input type='button' value='ダウンロードページに戻る' onclick='location.href=\"./download_user_files\"'>";
		exit;
	} else {
		echo "no DL<br>";
		exit;
	}


?>



	</section>

<!--
	<section id="m-2-box">
		<h2>
			ファイルのアップロード
		</h2>
		<p>
		ファイルをアップロードします。
		</p>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./upload_user_files" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<input type="hidden" name="user_id" value="<?php echo sprintf('%012d',$company_id);?>"/>
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="/var/www/co.cloudinvoice.co.jp/html/upfiles/<?php echo sprintf('%012d',$company_id);?>/UpFiles" />
				<input type="hidden" name="return_url" value="./user_files_upload" />
				<input type="hidden" name="ahead_url" value="./user_files_upload" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="アップロード" />
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>
			
	</section>
-->
</article>
<?php require("footer.php");?>