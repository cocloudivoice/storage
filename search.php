<?php
session_start();


//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/sitecontrol.class.php');
/*
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/sitetitlecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/datadispcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/datatitlecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/datacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/reviewcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/viewcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/boardcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/freespacecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/ochatcontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/reviewcontrol.class.php');
*/
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/allpurposecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$site_con = new site_control();

//変数
$user_id;
$site_id;
$login_user_id;

//検索用変数
$flag;
$words;

//site_idからuser_idを取得
if(isset($_GET['site_id'])) {
	$_SESSION['site_id'] = $site_id = htmlspecialchars($_GET['site_id'], ENT_QUOTES);
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else if (isset($_SESSION['site_id'])) {
	$site_id = $_SESSION['site_id'];
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else {
	header("Location:./index.php");
}

if(isset($_REQUEST['words'])) {
	$words = htmlspecialchars($_REQUEST['words'], ENT_QUOTES);
}

if(isset($_REQUEST['flag'])) {
	$flag = htmlspecialchars($_REQUEST['flag'], ENT_QUOTES);
}

//dbコントローラー準備
/*
$site_con = new site_control();
$dispdb = new data_disp_control();
$titledb = new data_title_control();
$datadb = new data_control();
$sitetitledb = new site_title_control();
$reviewdb = new review_control();
$viewdb = new view_control();
$board_con = new board_control();
$free_con = new freespace_control();
$user_con = new user_control();
$ochat_con = new ochat_control();
$review_con = new review_control();
*/
$all_purpose_con = new all_purpose_control();

//汎用検索処理
#検索
//$flag = "search_like_record_title";
//$words = "%小学校%";
//
//$search_num = count($all_purpose_arr);
//$all_purpose_arr[$i]["site_title"];
#View合計
//$flag = "sum_view";
//$all_purpose_arr[0]["SUM( `view_count` )"];
#コメント数
//$flag = "sum_comment";
//$all_purpose_arr[0]["count( * )"];
#表の信頼度に投票した人の数取得
//$flag = "trust_table";
//$all_purpose_arr[0]["good + bad"];
#表を信頼できるとした人の数
//$flag = "trust_num";
//$all_purpose_arr[0]["good"];
#表を信頼できるとした人の率
//$flag = "trust_ratio";
//round($all_purpose_arr[0]["good / ( good + bad ) *100"],1);
#一致するサイトタイトル
//$flag = "search_record_title";
//$words = "筑波大学付属小学校";
//$all_purpose_arr[$i]["site_id"];
//["record_photo"]
//["evaluate"]
//["record_basis"]
//件数取得
//$test=count($all_purpose_arr);

#☆5つ集計
//$flag = "sum_evaluate_rank5";
//$words = "筑波大学付属小学校";

#☆4つ集計
//$flag = "sum_evaluate_rank4";
//$words = "筑波大学付属小学校";

#☆3つ集計
//$flag = "sum_evaluate_rank3";
//$words = "筑波大学付属小学校";

#☆2つ集計
//$flag = "sum_evaluate_rank2";
//$words = "筑波大学付属小学校";

#☆1つ集計
//$flag = "sum_evaluate_rank1";
//$words = "筑波大学付属小学校";



#レコードタイトルの平均評価を取得する。
//$flag = "ave_evaluate";
//$words = "筑波大学付属小学校";
//$all_purpose_arr[0]["avg( evaluate )"];
#レコードタイトルが一致するレビューの数を取得する。
//$flag = "review_num";
//$words = "筑波大学付属小学校";
//$words = "1\""." or record_title like \"%小学校%";
//$all_purpose_arr[$i]["count( evaluate )"];
//件数取得
//$test=count($all_purpose_arr);

#サイトタイトルでのあいまい検索
//$flag = "site_title_search";
//$words = "小学校";
//$all_purpose_arr[$i]["site_id"]
//["site_title"]
//["imgurl"]
//["introduction"]
//件数取得
//$test=count($all_purpose_arr);

#レコードタイトルでのあいまい検索
//$flag = "record_title_search";
//$words = "小学校";

//ポジションテーブルから当該カラム名とランク等の情報を取得
/*
$view_sum_data = $viewdb -> view_sum($pdo,$user_id,$site_id);
$site_data = $site_con -> site_select($pdo,$user_id,$site_id);
$disp_data = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn);
$disp_data4 = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn4);
$title_data = $titledb -> data_title_select($pdo,$user_id,$site_id);
$body_data = $datadb -> data_select_all($pdo,$user_id,$site_id,$record_id_from,$record_id_to,$sort_sentence);
$site_title_data = $sitetitledb -> site_title_select($pdo,$user_id,$site_id);
$data_num = $datadb -> data_count($pdo,$user_id,$site_id);
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$ochat_con -> ochat_insert_new_comment($pdo, $ochat_arr);
$ochat_data = $ochat_con -> ochat_select_all($pdo, $user_id, $site_id, $ochat_sort, $ochat_desc, $ochat_limit, $ochat_offset);
$o_comment_num = $ochat_con -> ochat_count($pdo,$user_id);
*/
$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);

var_dump($all_purpose_arr);
echo $test=count($all_purpose_arr);

?>

<html>
	<head>
	<title>検索関連処理ページ</title>
	</head>
	<body>
	<div><a href="./main.php?site_id=<?php echo $site_id;?>">もどる</a></div>
	</body>
</html>