<?php 
session_start();

//必要なクラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
//ページ生成用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = sprintf("%012d",$_SESSION['user_id']);
} else {
	header("Location:./logout");
	exit();
}

//変数
$_REQUEST["koumoku"] = 1;
$_REQUEST["mode"] = "download";

//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = '/var/www/storage.cloudinvoice.co.jp/html/invoice/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            $tax_class = split(",",$buffer);
            $tax_class_arr[$tax_class[0]] = $tax_class[1];
            
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}


if ( $_REQUEST['mode'] === 'download' ) {

	if ( $_REQUEST['koumoku'] == 1 ) {
		//明細別
		$flag1 = "invoice_count";
		$flag = "invoice_data_to_autojournalize2";
	} else if ($_REQUEST['koumoku'] == 0) {
		//請求別
		$flag1 = "invoice_total_count";
		$flag = "invoice_total_data";
	}
	if ($_REQUEST['destination_id']) {
		$claimant_id = $_REQUEST['destination_id'];
		$words .= " AND `claimant_id` = ".$claimant_id." ";
	}
	if ($_REQUEST['update_from']){
			$upfrom = str_replace(" ","",substr($_REQUEST['update_from'],0,4)."/".substr($_REQUEST['update_from'],4,2)."/".substr($_REQUEST['update_from'],6,2));
			$words .= " AND `update_date` >= '".$upfrom."' ";
	}
	if ($_REQUEST['update_to']){
			$upto = str_replace(" ","",substr($_REQUEST['update_to'],0,4)."/".substr($_REQUEST['update_to'],4,2)."/".substr($_REQUEST['update_to'],6,2));
			$words .= " AND `update_date` <= '".$upto."' ";
	}
	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$words .= " AND `pay_date` >= ".$payfrom." ";
	}
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$words .= " AND `pay_date` <= ".$payto." ";
	}
	//statusが99の請求書（削除データ）を除く
	$words .= " AND `status` <> 99 ";
	
	//ソートの設定
	if (isset($_GET['sort_num']) && $_GET['sort_num'] != NULL) {
		$sort_num = htmlspecialchars($_GET['sort_num'],ENT_QUOTES);
		$sort_key = $sort_num;
	} else {
		$sort_key = "display_rank";//$_GET['sort_name'];//GETか何かでカラム名取得を実装する。
	}

	if (isset($_GET['sort_key']) && $_GET['sort_key'] != NULL) {
		$sort_key = htmlspecialchars($_GET['sort_key'],ENT_QUOTES);
	}
	if (isset($_GET['sort_type']) && $_GET['sort_type'] != NULL) {
		$sort_type = htmlspecialchars($_GET['sort_type'],ENT_QUOTES);
	}
	if ($_REQUEST['sort_type'] == 1) {
		if ($sort_type =="ASC" || $sort_type == "") {
			$sort_type ="DESC";
		} else {
			$sort_type = "ASC";
		}
	} else {
		if ($sort_type == "DESC") {
			$sort_type = "DESC";
		} else {
			$sort_type = "ASC";
		}
	}

	if ($_REQUEST['page']) {
		$page_n = $_REQUEST['page'];
	} else {
		$page_n = 1;
	}

	//ページ条件なしでデータ数を取得
	//$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	//仕訳済みフラグのためここで処理を分ける。
	$page_words .= $words."GROUP BY `invoice_code`";
	$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$page_words);
	//var_dump($data_num_arr);
	//echo "<br>";
	//$current_invoice_code = $data_num_arr[$page_n]["invoice_code"];
	//echo $data_num_arr[0]["invoice_code"];
	//echo "<br>";
	//表示数の制御
	//$max_disp = 20;//表示する件数
	if ($_REQUEST['max_disp'] != NULL ||$_REQUEST['max_disp'] != 0) {
		$max_disp = $_REQUEST['max_disp'];//表示する件数
	} else {
		$max_disp = 100;//表示する件数
	}
	$sort_key = "`id`";//ソートするキー
	$sort_type = "ASC";//ASC：昇順DESC：降順
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." ";	
	//$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	//echo $data_num_arr[0]["invoice_code"];
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	$start = ($page_n - 1) * $max_disp;
 	$end = $start + $max_disp;
 	if ($end > $data_num) {$end = $data_num;}
	$words .= "AND (";
	for ($k = $start ;$k < $end; $k++) {
		if ($k == $start){
			if ($data_num_arr[$k]["invoice_code"] == NULL){
				$words .= " `invoice_code` = '".$data_num_arr[$k+1]["invoice_code"]."'";
			} else {
				$words .= " `invoice_code` = '".$data_num_arr[$k]["invoice_code"]."'";
			}
		} else {
			$words .= " OR `invoice_code` = '".$data_num_arr[$k]["invoice_code"]."'";
		}
	}
	$words .= ") ";
	$words .= " ORDER BY ";
	$words .= $conditions;
	
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	$invoice_data_num = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	$invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	//var_dump($invoice_data_arr);

	//▼自動仕訳用企業DB登録▼
	//もし存在しなければ、企業品目テーブルと企業用レコードテーブルを作成
	$table_name = "".$company_id;
	try {
		$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
	} catch (PDOException $e) {
	    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
	    $_SESSION['error_msg'] = $e->getMessage();
	}
	//▲自動仕訳用企業DB登録▲

	//var_dump($invoice_data_num);
	//var_dump($invoice_data_arr);
	//$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
    //出力ファイル名の作成
    $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
  
    //文字化けを防ぐ
    $csv_data = mb_convert_encoding ( $csv_data , "utf-8" , 'utf-8' );
      
    //MIMEタイプの設定
    //header("Content-Type: application/octet-stream");
    //名前を付けて保存のダイアログボックスのファイル名の初期値
    //header("Content-Disposition: attachment; filename={$csv_file}");
  
    // データの出力
    //echo($csv_data);
    //exit();
    $flag = "select_max_journalized_id";
    $table_name = "JOURNALIZED_HISTORY_".sprintf("%012d",$company_id);
    $aj_words = "";
    $max_jh_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
    $max_jh_num = $max_jh_num_arr[0]['max(`text_data3`)'] + 1;
    $flag = "";
    $table_name = "";
    $aj_words = "";

}
?>
<?php require("header.php");?>

<script>

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box3" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box4" ).datepicker();
});

function OpenkCommonSearchFrame(){
	document.getElementById("kCommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}

function FrameHeight() {
	window.setTimeout("window.parent.frames['kCommonSearch'].test()","600");
}

function TransNo() {
	//var destination_code = document.getElementById('Kyoutu_No').value;
	//document.getElementById('Kyotu_No_Invisible').value = destination_code;
}
function getCompanyInfo() {

}

function changeAccountName(n,m) {
	document.getElementById('debit_account_name'+n).value = document.getElementById('account_names'+n).value;
	//document.getElementById('debit_account_name'+n).class = "no_record";
	document.getElementById('count_flag'+n).value = m;
}

function openDDBox(n) {
	document.getElementById('account_names' + n).setAttribute("style", "display:visible");
	document.getElementById('disp_br' + n).setAttribute("style", "display:visible");
}

function closeDDBox(n) {
	document.getElementById('account_names' + n).setAttribute("style", "display:none");
	document.getElementById('disp_br' + n).setAttribute("style", "display:none");
}

function closeSetTime(n) {
	var stclose = setTimeout("closeDDBox("+ n +")",10000);
	//stclose = setTimeout();
}

function checkAccount(n) {
	var debit_ac_name = document.getElementById('debit_account_name' + n).value;
	if ( debit_ac_name == "" ) {
		document.getElementById('debit_account_name' + n).value = document.getElementById('account_names' + n).value;
	}
}

function getTotal(n) {
	document.getElementById('invoice_total_top' + n).value = document.getElementById("invoice_total" + n).value;
}

function addRow(button,num) {
	var tblId = button.getAttribute("id");
	var tblObj = document.getElementById(tblId);
	var rowCnt = tblObj.rows.length;
	if(rowCnt==10) { alert("これ以上登録する事はできません。");return; }
	var row = tblObj.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.setAttribute("id",tblId);
	cell.setAttribute("class","minus");
	cell.setAttribute("name",rowCnt);
	cell.setAttribute("onclick","delRow(this)");
	cell.innerHTML = "\n";
	var cell = row.insertCell(1);
	cell.innerHTML = "<input type='text' style='width:90px' class='account' id='" + tblId + rowCnt + "1' name='" + tblId + "_" + rowCnt + "1' onclick='' onmouseout='' /><br><input type='text' class='supplement' placeholder='補助科目' name='" + tblId + "_" + rowCnt + "8'/>\n";
	document.getElementById(tblId + rowCnt + "1").value = document.getElementById("debit_account_name" + num).value;
	var cell = row.insertCell(2);
	cell.innerHTML = "<input type='text' class='section' placeholder='部門' name='" + tblId + "_" + rowCnt + "9'/>\n";
	var cell = row.insertCell(3);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='text' class='value' id='" + tblId + rowCnt + "2' name='" + tblId + "_" + rowCnt + "2' value='0' />\n";
	var cell = row.insertCell(4);
	cell.innerHTML = "<input type='hidden' class='value' id='" + tblId + rowCnt + "3' name='" + tblId + "_" + rowCnt + "3' value='0' />\n"
	+'<div class="custom">'
	+'<select class="value" id="' + tblId + rowCnt + '3_1" name="' + tblId + '_' + rowCnt + '3_1">'
	<?php
	for ($op_num = 0;$op_num < 128;$op_num++) {
		if ($tax_class_arr[$op_num] != "") {
			echo '+'.'\'<option value="'.$op_num.'">'.str_replace(array("\r\n","\r","\n"),"",$tax_class_arr[$op_num]).'</option>\'';
		}
	}
	?>
	+'</select>'
	+'</div>';
	var cell = row.insertCell(5);
	cell.setAttribute("style","padding:0 0 0 10px;")
	cell.innerHTML = "<input type='text' placeholder='貸方科目' style='width:90px' class='' id='" + tblId + rowCnt + "4' name='" + tblId + "_" + rowCnt + "4' value='' /><br><input type='text' class='supplement' placeholder='補助科目' name='" + tblId + "_" + rowCnt + "10'/>\n";
	var cell = row.insertCell(6);
	cell.innerHTML = "<input type='text' class='section' placeholder='部門' name='" + tblId + "_" + rowCnt + "11'/>\n";
	var cell = row.insertCell(7);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='text' class='value' id='" + tblId + rowCnt + "5' name='" + tblId + "_" + rowCnt + "5' value='0' />\n";
	var cell = row.insertCell(8);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='hidden' class='value' id='" + tblId + rowCnt + "6' name='" + tblId + "_" + rowCnt + "6' value='0' />\n"
	+'<div class="custom">'
	+'<select class="value" id="' + tblId + rowCnt + '6_1" name="' + tblId + '_' + rowCnt + '6_1">'
	<?php
	for ($op_num = 0;$op_num < 128;$op_num++) {
		if ($tax_class_arr[$op_num] != "") {
			echo '+'.'\'<option value="'.$op_num.'">'.str_replace(array("\r\n","\r","\n"),"",$tax_class_arr[$op_num]).'</option>\'';
		}
	}
	?>
	+'</select>'
	+'</div>';
	var cell = row.insertCell(9);
	cell.setAttribute("class","value");
	cell.setAttribute("style","padding:0 0 0 10px")
	cell.innerHTML = "<input type='text' class='remarks' id='" + tblId + rowCnt + "7' name='" + tblId + "_" + rowCnt + "7' />\n";
	document.getElementById(tblId + rowCnt + "7").value = document.getElementById("remarks" + num).value;
	
	document.getElementById(tblId + "rowNow").value=rowCnt + 1;
}

function delRow(button) {
	var tblId = button.getAttribute("id");
	var tblObj = document.getElementById(tblId);
	var tblName = button.getAttribute("name");
	var rowCnt = tblObj.rows.length;
	var i = tblName * 1 + 1;
	for (i; i <rowCnt; i++) {
		try {
			var k = i - 1;
			var cell = tblObj.rows[i].cells[0];
			//alert(showObject(cell));
			cell.setAttribute("name",k);
			//var cell = tblObj.rows[i].cells[1];
			for (var rownum = 1; rownum <= 7; rownum++ ) {
				var cell_data = document.getElementById(tblId + i + rownum);
				cell_data.name = tblId + "_" + k + rownum;
				cell_data.id = tblId + k + rownum;
			}
		} catch (e) {
			alert(e);
		}
	}
	var tbl = document.getElementById(tblId);
	var delRow = button.parentNode.rowIndex;
	var rows = tbl.deleteRow(delRow);
	var i = 0;
	var k = 0;

	document.getElementById(tblId + "rowNow").value = document.getElementById(tblId + "rowNow").value - 1;
}

function unusedData(n) {
	var m = document.getElementById("unused_flag" + n).value;
	if (m == 0) {
		document.getElementById("unused_flag" + n).value = 1;
		document.getElementById("account_table" + n).style = "background-color:gray;disabled:disabled;";
		document.getElementById("unused_button" + n).value = "除外解除";
		var s = document.getElementById("account_table" + n);
		var inp = s.getElementsByTagName("input");
		var slc = s.getElementsByTagName("select");
		for (i = 0;i < inp.length;i++) {
			//inp[i].readOnly=true;
			inp[i].disabled=true;
		}
		for (i = 0;i < slc.length;i++) {
			//inp[i].readOnly=false;
			slc[i].disabled=true;
		}

	} if (m == 1) {
		document.getElementById("unused_flag" + n).value = 0;
		document.getElementById("account_table" + n).style = "background-color:white;";
		document.getElementById("unused_button" + n).value = "除外する";
		var s = document.getElementById("account_table" + n);
		var inp = s.getElementsByTagName("input");
		var slc = s.getElementsByTagName("select");
		for (i = 0;i < inp.length;i++) {
			//inp[i].readOnly=false;
			inp[i].disabled=false;
		}
		for (i = 0;i < slc.length;i++) {
			//inp[i].readOnly=false;
			slc[i].disabled=false;
		}


	}
}


</script>
<script>
$(function() {
	var n = $('#data_num').val();
	for (var i = 0; i < n; i++) {
		try {
			var m = $('#invoice_total' + i).val();
			if (m > 0) {
				l = eval(m).toLocaleString();
				$('#invoice_total_top' + i).text(l);
			}
		}catch (e) { 
			//alert(e);
		}
	}
});


function showObject(elm,type) {
	var str = '「' + typeof elm + "」の中身";
	var cnt = 0;
	for(i in elm) {
		if(type == 'html') {
			str += '<br />\n' + "[" + cnt + "] " + i.bold() + ' = ' + elm[i];
		} else {
			str += '\n' + "[" + cnt + "] " + i + ' = ' + elm[i];
		}
		cnt++;
		status = cnt;
	}
	return str;
}



</script>

<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>


<title>
<?php
if ( $_POST['mode'] === 'download' ) {
	echo "仕訳編集画面 - Cloud Invoice";
} else {
	echo "自動仕訳作成 - Cloud Invoice";
}
?>
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<?php
	if ( $_REQUEST['mode'] != 'download' ) {
?>
<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article>


	<section id="m-1-box">
		<h2>
		<?php
		if ( $_REQUEST['mode'] === 'download' ) {
			echo "仕訳編集";
		} else {
			echo "自動仕訳作成";
		}
		?>

		</h2>

		<p>
			条件を指定してください。
		</p>
		<form name="paymentcsv_form" action="" method="post">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" id="Kyoutu_No" class="kCommonSearch" name="destination_id" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame();" value="" placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
				<!--<input type="text" id="Kyotu_No_Invisible" name="destination_id" value="" />-->
			</div>
			
			<div id="hiduke">
				<!--
				<input type="radio" id="1" name="koumoku" value="0" />
				<label for="1">請求別</label>
				-->
				<input type="hidden" id="2" name="koumoku"value="1">
			</div>
			<!--
			<div id="hiduke">
				<h3>更新日付</h3>
				<input id="box1" name="update_from" type="text" />
				<p>(yyyymmdd) - </p>
				<input id="box2" name="update_to" type="text" />
				<p>(yyyymmdd)</p>
			</div>
			-->
			<div id="hiduke">
				<h3>支払日付</h3>
				<input id="box3" name="paydate_from" type="text" />
				<p>(yyyymmdd) - </p>
				<input id="box4" name="paydate_to"type="text" />
				<p>(yyyymmdd)</p>
			</div>

			<div id="shiharaEx">
				<input type="submit" value="仕訳画面に進む"/>
				<input type="hidden" name="company" value="<?php echo $_SESSION['user_id'];?>">
		        <input type="hidden" name="mode" value="download">
			</div>
		</form>
	</section>
</article>
<?php
	} else {
?>
<?php
//echo $page_con -> create_links();
?>



<article id="autojournal">
	<section>
		<form action="./shiharaiautojournalcsv" method="post">
		<?php
		$count_data_num = count($invoice_data_arr);
		if ( $count_data_num > 0) {
		//echo '<input type="button" onclick="submit()" value="CSVで出力する" /><br/><br/>';
			$invoice_code_checker = "";
			$claimant_id_checker = "";
		?>
		<?php 
			$k = 0;//ヘッダーの番号を覚えておく変数
			for ( $i = 0; $i < intval($count_data_num); $i++ ) {
				//var_dump($invoice_data_arr[$i]);
				$withholding_tax = $invoice_data_arr[$i]['withholding_tax'];
				$withholding_tax_flag = $withholding_tax * 1;
				$journalized_flag = $invoice_data_arr[$i]['journalized'];
				if ($invoice_code_checker != $invoice_data_arr[$i]['invoice_code'] || $claimant_id_checker != $invoice_data_arr[$i]["claimant_id"]) {
					$invoice_code_checker = $invoice_data_arr[$i]['invoice_code'];
					$claimant_id_checker = $invoice_data_arr[$i]["claimant_id"];
					$max_jh_num++;
					if ($i > 0) {
						echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
						$invoice_total = 0;
						$k = $i;
						echo "
							</div>
							";
					}
		?>
			<table id="invoice_table" >
				<!--
				<tr>
					<th>共通コード</th>
					<th>会社名</th>
					<th>請求書日付</th>
					<th>請求書番号</th>
					<th>担当者名</th>
					<th>支払日</th>
				</tr>
				-->
				<tr>
					<!--<td class="long"><?php echo $invoice_data_arr[$i]["claimant_id"];?></td>-->
					<?php
					//var_dump($invoice_data_arr[$i]);
						$claimant_id = $invoice_data_arr[$i]["claimant_id"];
						$destination_id = $invoice_data_arr[$i]["destination_id"];
						$send_code = $invoice_data_arr[$i]["send_code"];
						if ($company_id != "") {
							$flag = "company_data_historical";
						//	$claimant_historical = $company_con -> company_sql_flag($pdo,$flag,$company_id,$cond_a);
							//var_dump($claimant_historical);
						}
						if ($destination_id != "") {
							$flag = "company_data_historical";
						//	$destination_historical = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$cond_b);
							//var_dump($destination_historical);
						}
						if ($send_code != "") {
							$flag = "addressee_data_historical";
							if ($claimant_id != 0) {
								$cond_c = " AND `company_id` = ".$claimant_id." ";
							}
						//	$send_historical = $company_con -> company_sql_flag($pdo,$flag,$send_code,$cond_c);
							//echo $send_historical[0]['historical'];
							//var_dump($send_historical);
						}

					?>
					<td class="long">
					<span class="gray">企業名</span><br/>
					<?php
						$company_id = sprintf("%012d",$company_id);
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
						$flag = "company_data";
						$ind_type_s = "";//初期化
						$ind_info = "";//初期化
							if ($claimant_id == $company_id) {
								
								if ($send_code != "" || $send_code != NULL) {
									echo $invoice_data_arr[$i]["company_name"];
									//echo "a";
									$flag = "addressee_data_one";
									$conditions = " AND company_id = ".$company_id." ";
									//$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
									//var_dump($company_send_code_check_arr);
									if ($company_send_code_check_arr[0]["email"] != NULL && $company_send_code_check_arr[0]["email"] != "") {
										//echo "b";
										$company_email = $company_send_code_check_arr[0]["email"];
										//$company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
										$company_email .= ",".$company_arr[0]["email"];
										echo $invoice_data_arr[$i]["company_name"];
										//echo $company_name = $company_arr[0]["company_name"];
										if ($company_arr[0]["company_name"] == "") {
											//echo "d";
											//$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
											echo $invoice_data_arr[$i]["d_company_name"];
											//echo $destination_company_name[0]['company_name'];
											
										}
									} else if ($company_send_code_check_arr[0]["company_name"] != "") {
										//echo "c";
										echo $invoice_data_arr[$i]["company_name"];
										//echo $company_send_code_check_arr[0]["company_name"];
									}
								} else if ($destination_id != 0) {
									//echo "e";
									//$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
									echo $invoice_data_arr[$i]["d_company_name"];
									//echo $destination_company_name[0]['company_name'];
								}
							} else {
								//echo "f";
								//$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
								echo $invoice_data_arr[$i]["c_company_name"];
								//echo $claimant_company_name[0]['company_name'];
								$ind_type = $claimant_company_name[0]['industry_type'];
								$ind_type_s = $claimant_company_name[0]['industry_type_s'];
								$ind_info = $ind_type.$ind_type_s;
							}
							
					?>
					</td>
					<td class="middle">
					<span class="gray">請求書番号</span><br>
					<?php
					$top4 = substr($claimant_id,0,4);
					$mid4 = substr($claimant_id,4,4);
					$str = $invoice_data_arr[$i]["pay_date"];
					$download_password = $invoice_data_arr[$i]['download_password'];
					$pdf_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";
					if (!file_exists($pdf_url)) {
					?>
						<a href="./invoice2pdf?dlp=<?php echo $invoice_data_arr[$i]['download_password'];?>&dlps=1" target="new"><?php echo $invoice_data_arr[$i]['invoice_code'];?></a>
					<?php 
					} else {
					?>
						<a href="<?php echo 'https://storage.cloudinvoice.co.jp/files/'.$top4."/".$mid4."/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";?>" target='new'><?php echo $invoice_data_arr[$i]['invoice_code'];?></a>
					<?php
					}
					?>
					</td>
					<td>
					<span class="gray">伝票日付</span><br>
					<?php echo date('Y/m/d', strtotime($invoice_data_arr[$i]["billing_date"]));?>
					</td>
					<!--<td><?php echo $invoice_data_arr[$i]['invoice_code'];?></td>-->
					<!--<td class="long"><?php echo $invoice_data_arr[$i]["staff_name"];?></td>-->
					<td>
					<span class="gray">支払期限日</span><br>
					<?php echo date('Y/m/d', strtotime($invoice_data_arr[$i]["pay_date"]));?>
					</td>
					<td>
						<span class="gray">合計金額</span><br>
						<p id="invoice_total_top<?php echo $k?>"></p>
					</td>
					<td>
					<?php
					$str = $invoice_data_arr[$i]["pay_date"];
					$pdf_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";
					$img_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".png";

					if (!file_exists($pdf_url)) {
					?>
						<a href="./invoice2pdf?dlp=<?php echo $invoice_data_arr[$i]['download_password'];?>&dlps=1" target="new"><span class="gray" style='color:#004b91;'>請求書を表示する。</span></a>
						<!--
						<a href="./invoice2pdf?dlp=<?php echo $invoice_data_arr[$i]['download_password'];?>&dlps=1" onclick="window.open( '<?php echo $img_url;?>', 'mywindow6', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes'); return false;" target="new"><span class="gray" style='color:#004b91;'>請求書を表示する。</span></a>
						-->
					<?php 
					} else if (file_exists($img_url)) {
						//"?pdf_dir=".$pdf_url."";
					?>
						<a href="<?php echo $img_url;?>" target='new'><span class="gray" style='color:#004b91;'>証憑を表示する</span></a>
					<?php
					} else {
						$cond_find = '../pdf/'.$invoice_data_arr[$i]['download_password'].".pdf";
					?>
						<a href="<?php echo $pdf_url;?>" target='new'><span class="gray" style='color:#004b91;'>請求書を表示する</span></a>
					<?php
					}
					?>
						<input type="hidden" name="journalized_flag<?php echo $i;?>" value="<?php echo $journalized_flag;?>" />
						<input type="hidden" name="des_cla_flag<?php echo $i;?>" value="<?php if ($claimant_id == $company_id) {echo 2;} else {echo 1;}?>" />
						<input type="hidden" id="unused_flag<?php echo $i;?>" name="unused_flag<?php echo $i;?>" cols="1" width="10px" value="0" /><!--0:過去仕訳に登録する 1:過去仕訳に登録しない-->
					</td>
					<td>
						<input id="unused_button<?php echo $i;?>" type="button" onclick="unusedData('<?php echo $i;?>');" value="除外する" />
					</td>
					</tr>
			</table>
		
		<div id="line" style="width:1010px";></div>
		<div class="account_table">
			<div id="account_table">
				<div class="account_table" style="padding:0 0 0 48px; text-align:center;">
					借方科目<br>
					借方補助科目
				</div>
				<div class="account_table" style="padding:0 0 0 40px">借方部門</div>
				<div class="account_table" style="padding:0 0 0 50px">金額</div>
				<div class="account_table" style="padding:0 0 0 50px">消費税区分</div>
				<div class="account_table" style="padding:0 0 0 48px; text-align:center;">
					貸方科目<br>
					貸方補助科目
				</div>
				<div class="account_table" style="padding:0 0 0 40px">貸方部門</div>
				<div class="account_table" style="padding:0 0 0 50px">金額</div>
				<div class="account_table" style="padding:0 0 0 50px">消費税区分</div>
				<div class="account_table" style="padding:0 0 0 120px">摘要</div>
			</div>
			<?php
				}//ifの終わり
				
			?>
			<table id="account_table<?php echo $i?>" class="account_table">
				<tr>
					<td class="plus" id="account_table<?php echo $i?>" onclick="addRow(this,<?php echo $i?>)" />
						<input type="hidden" name="account_table_dlp<?php echo $i?>" value="<?php echo $download_password;?>" />
						<input type="hidden" name="account_table<?php echo $i?>" value="1" />
						<input type="hidden" name="claimant_id<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['claimant_id'];?>" />
					<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
					<?php
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
						$flag = "company_data";
						if ($claimant_id != 0 ) {
							$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
						} else {}
					?>
						<input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $claimant_company_name[0]['company_name'];?>">
					</td>
					<?php
					/*
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
						$dest_id = $invoice_data_arr[$i]["destination_id"];
						$flag = "addressee_data_receive_client_id";
						$words = " AND company_id = ".$claimant_id."";
						$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					*/
					/*
						if ($client_id_arr[0]['client_id'] == NULL) {
							$csv_data .= ",";
						} else {
							$csv_data .= $client_id_arr[0]['client_id'].",";
						}
					*/
					?>
					<td>
						<input type="hidden" id="account_table<?php echo $i?>rowNow" value="1">
						<input type="hidden" name="billing_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['billing_date'];?>" />
						<input type="hidden" name="invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['invoice_code'];?>" />
						<input type="hidden" name="staff_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['staff_name'];?>" />
						<input type="hidden" name="pay_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['pay_date'];?>" />

					<?php
						//自動仕訳
						//ステップ1"請求元の共通コード 品名"
						
						$flag = "auto_journal_step1";
						//echo $company_id = sprintf("%012d",$company_id);echo " ";echo $invoice_data_arr[$i]["product_name"];echo "<br/>";
						$search_key = $invoice_data_arr[$i]["claimant_id"]." ".$invoice_data_arr[$i]["product_name"];
						$table_name="RECORD"."".$company_id;
						$aj_words = $search_key;
						$auto_journal_data_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
						$credit_account_name = $auto_journal_data_arr[0]["credit_account_name"];
						$debit_account_name = $auto_journal_data_arr[0]["debit_account_name"];
						if ($debit_account_name == "" || $debit_account_name == NULL) {
							echo '<style>#account_table'.$i.' { background-color:#FFA3B1; }</style>';
						}
						//var_dump($auto_journal_data_arr);
					?>
					<?php
							
					?>
					<input class="<?php if ($debit_account_name == '' || $debit_account_name == NULL) {echo 'no_record';}?>" style="width:90px;" type='text' class="account" id='debit_account_name<?php echo $i;?>' name='debit_account_name<?php echo $i;?>' value='<?php if ($debit_account_name != "") { echo $debit_account_name;} else if ($claimant_id == $company_id) { echo "売掛金";};?>' onclick="openDDBox(<?php echo $i;?>);" onmouseout="closeSetTime(<?php echo $i;?>);" /><br/>
					<input type='hidden' id='count_flag<?php echo $i;?>' name='count_flag<?php echo $i;?>' value="1" />
					
					<?php
						//ステップ2"請求元企業の自動仕訳用テーブル"
						$flag = "auto_journal_step2";
						$table_name = $invoice_data_arr[$i]["claimant_id"];
						$aj_words = $invoice_data_arr[$i]["product_name"];
						$auto_journal_data_arr2 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
						//var_dump($auto_journal_data_arr2);
						if (count($auto_journal_data_arr2) > 0) {
							//echo "step2";
					?>
					<style type="text/css">.no_record{color:red;}</style>
							<select id='account_names<?php echo $i;?>' name='step2_data<?php echo $i;?>' onclick='changeAccountName(<?php echo $i;?>,2)' style="display:none" >
							<!--<option value='<?php echo $auto_journal_data_arr[0]["debit_account_name"];?>'><?php echo $auto_journal_data_arr[0]["debit_account_name"];?></option>-->
					<?php
							for ($q = 0;$q < count($auto_journal_data_arr2);$q++) {
					?>
							<option value='<?php echo $auto_journal_data_arr2[$q]["account_name"];?>' onclick='closeDDBox(<?php echo $i;?>);' ><?php echo $auto_journal_data_arr2[$q]['account_name'];?></option>
					<?php
							}
					?>
							</select><br id="disp_br<?php echo $i;?>" style="display:none" />
					<?php
						} else {
							
							if ($ind_info != "") {
							
							//ステップ3 "main"の自動仕訳用テーブル（業種番号で仕訳を探す。）
								//$flag = "auto_journal_step3";
								//$table_name = "MAIN";
								//$aj_words = $ind_info." ".$invoice_data_arr[$i]["product_name"];
								//$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
								//if (!isset($auto_journal_data_arr3)){
									$flag = "auto_journal_step3";
									$table_name = "MAIN";
									$aj_words = $ind_info;
									$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
								//}
							}
							//業種番号で仕訳が出てこなければproduct_nameで仕訳を探す。
							if (count($auto_journal_data_arr3) == 0 || $ind_info == "") {
								$flag = "auto_journal_step3";
								$table_name = "MAIN";
								$aj_words = $invoice_data_arr[$i]["product_name"];
								$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
								if (count($auto_journal_data_arr3) == 0) {
									unset($auto_journal_data_arr3);
									$flag = "auto_journal_step3";
									$table_name = "MAIN";
									$aj_words = "*";
									$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
									
								}
							}
							if (count($auto_journal_data_arr3) > 0) {
								//echo "step3";
					?>
							<select id='account_names<?php echo $i;?>' name='step3_data<?php echo $i;?>' onclick='changeAccountName(<?php echo $i;?>,3);' style="display:none" >
							<!--<option value='<?php echo $auto_journal_data_arr[0]["debit_account_name"];?>'><?php echo $auto_journal_data_arr[0]["debit_account_name"];?></option>-->
					<?php
								for ($q = 0;$q < count($auto_journal_data_arr3);$q++) {
					?>
									<option value='<?php echo $auto_journal_data_arr3[$q]["account_name"];?>' onclick='closeDDBox(<?php echo $i;?>);' ><?php echo $auto_journal_data_arr3[$q]['account_name'];?></option>
					<?php
								}
					?>
								</select><br id="disp_br<?php echo $i;?>" style="display:none" />
					<?php
							}
							//"<input type='text'name='auto_csv_data[]' value='".$auto_journal_data_arr3[$q]['account_name']."' />";
						}
						echo '<script type="text/javascript">checkAccount('.$i.');</script>';
					?>
						<input type='text' class="supplement" placeholder="補助科目" name="debit_sub_account_name<?php echo $i;?>" value="<?php echo $auto_journal_data_arr[0]['debit_sub_account_name'];?>" />
					</td>
					<td>
						<input type='text' class="section" placeholder="部門" name="debit_section_name<?php echo $i;?>" value="<?php echo $auto_journal_data_arr[0]['debit_section_name'];?>" />
					</td>

					<td>
					<?php
						if ( $invoice_data_arr[$i]['sum(total_price_excluding_tax)'] != NULL ) { 
					?>
					<input type="hidden" name="sum(total_price_excluding_tax<?php echo $i;?>)" value="<?php echo $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];?>" />
					<input type="hidden" name="base_amount_of_money<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];?>" />
					</td>
					<td class="value">
					<?php
						} else { 
					?>
					<input type="text" class="value" id="total_price_excluding_tax<?php echo $i;?>" name="total_price_excluding_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) {$debit_ratio = $auto_journal_data_arr[0]['debit_ratio'];} else {$debit_ratio = 1;} if ($withholding_tax_flag > 0 &&($claimant_id != $company_id)) { echo round(($invoice_data_arr[$i]['total_price'] * $debit_ratio + $withholding_tax),0);} else {echo round($invoice_data_arr[$i]['total_price'] * $debit_ratio,0);}?>" />
					<input type="hidden" class="value" id="base_amount_of_money<?php echo $i;?>" name="base_amount_of_money<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />
					<!--<input type="text" class="value" id="total_price<?php echo $i;?>" name="total_price<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />-->
					</td>
					<td class="value">
					<?php
						}
					?>
					<?php
						//税区分の処理
						//借方の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if (count($auto_journal_data_arr) == 0) {
								$debit_tax_num = 2;
							} else if ($auto_journal_data_arr[0]['debit_flag1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag1'];
							} else {
								$debit_tax_num = 2;
							}
						} else {
							//支払の場合
							if (count($auto_journal_data_arr) == 0) {
								$debit_tax_num = 31;
							}else if ($auto_journal_data_arr[0]['debit_flag1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag1'];
							} else {
								$debit_tax_num = 31;
							}
						}
						//貸方の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 51;
							} else if ($auto_journal_data_arr[0]['credit_flag1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag1'];
							} else {
								$credit_tax_num = 51;
							}
						} else {
							//支払の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 2;
							} else if ($auto_journal_data_arr[0]['credit_flag1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag1'];
							} else {
								$credit_tax_num = 2;
							}
						}
						
						$debit_tax_class = $tax_class_arr[$debit_tax_num];
						$credit_tax_class = $tax_class_arr[$credit_tax_num];
						if($debit_tax_num <= 29 || $debit_tax_num == NULL) {$debit_tax_flag = 0;} else {$debit_tax_flag = 1;}
						if($credit_tax_num <= 29 || $credit_tax_num == NULL) {$credit_tax_flag = 0;} else {$credit_tax_flag = 1;}
					?>
					<input type="hidden" class="value" name="sales_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) { echo $debit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * round($auto_journal_data_arr[0]['debit_ratio'],0);} else if ($claimant_id == $company_id) { echo round($invoice_data_arr[$i]['sales_tax'] * 0,0);} else {echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);}?>" />
					<div class="custom">
						<select class="value" name="debit_tax_class<?php echo $i;?>">
						<?php
							for ($op_num = 0;$op_num < 128;$op_num++) {
								if ($tax_class_arr[$op_num] != "") {
									if($op_num == $debit_tax_num){$debit_select_flag = "selected"; } else {$debit_select_flag = "";}
									echo '<option value="'.$op_num.'" '.$debit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
								}
							}
						?>
						</select>
					</div>
					<?php
						/*
						if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) {
							echo $debit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['debit_ratio'],0);
						} else if ($claimant_id == $company_id) {
							echo round($invoice_data_arr[$i]['sales_tax'] * 0,0);
						} else {
							echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);
						}
						*/
					?>

					</td>
					<td style="padding:0 0 0 10px;">
						<input style="width:90px" type="text" class="account" id="credit_account_name<?php echo $i;?>"  name="credit_account_name<?php echo $i;?>" value="<?php if (isset($credit_account_name)) {echo $credit_account_name;} else if ($debit_account_name == '仕入' || $debit_account_name == '商品' || $debit_account_name == '在庫') {echo '買掛金';} else if ($claimant_id == $company_id) { echo '売上高';} else {echo '未払金';}?>" /><br/>
						<input type='text' class="supplement" placeholder="補助科目" name='credit_sub_account_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_sub_account_name'];?>" />
					</td>
					<td>
						<input type='text' class="section" placeholder="部門" name='credit_section_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_section_name'];?>" />
					</td>
					
					<td class="value"><input type="text" class="value" name="credit_amount_of_money<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['credit_ratio'] != NULL) {$credit_ratio = $auto_journal_data_arr[0]['credit_ratio'];} else {$credit_ratio = 1;}if ($withholding_tax_flag > 0 && ($claimant_id == $company_id)) {echo round(($invoice_data_arr[$i]['total_price'] * $credit_ratio + $withholding_tax),0);} else { echo round($invoice_data_arr[$i]['total_price'] * $credit_ratio,0);}?>" /></td>
					<td class="value">
						<input type="hidden" class="value" name="credit_sales_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['credit_ratio'] != NULL) { echo round($credit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['credit_ratio'],0);} else if ($claimant_id == $company_id) { echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);} else {echo 0;}?>" />
					<div class="custom">
						<select class="value" name="credit_tax_class<?php echo $i;?>">
						<?php
							for ($op_num = 0;$op_num < 128;$op_num++) {
								if ($tax_class_arr[$op_num] != "") {
									if ($op_num == $credit_tax_num) {$credit_select_flag = "selected"; } else {$credit_select_flag = "";}
									echo '<option value="'.$op_num.'" '.$credit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
								}
							}
						?>
						</select>
					</div>
					</td>
					<td style="padding:0 0 0 10px;">
					<?php
					
					/*
					$csv_data .= $invoice_data_arr[$i]["withholding_tax"]."</td><td>";
					if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
						$csv_data .= $invoice_data_arr[$i]['sum(total_price)']."</td><td>"; 
					} else { 
						$csv_data .= $invoice_data_arr[$i]["total_price"]."</td><td>";
					}
					*/
					?>
					<input type="text" class="remarks" id="remarks<?php echo $i;?>" name="remarks<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['product_name'].' '.$invoice_data_arr[$i]['remarks1'];?>" />
					<input type="hidden" name="product_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['product_name'];?>" />
					<input type="hidden" name="jounalized_history_code<?php echo $i;?>" value="<?php echo $max_jh_num;?>" />
					</td>
				</tr>
				<?php
					$sub_row_num = $auto_journal_data_arr[0]['flag1'] * 1;
					
					if ($withholding_tax_flag > 0) {
						$sub_row_num++;
					}
					
					for($j = 1;$j <= $sub_row_num;$j++) {
				?>
				<?php
						//税区分の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 2;
							} else if ($auto_journal_data_arr[0]['debit_flag'.$j.'_1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag'.$j.'_1'];
							} else {
								$debit_tax_num = 2;
							}
						} else {
							//支払の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 31;
							} else if ($auto_journal_data_arr[0]['debit_flag'.$j.'_1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag'.$j.'_1'];
							} else {
								$debit_tax_num = 31;
							}
						}
						//貸方の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 51;
							} else if ($auto_journal_data_arr[0]['credit_flag'.$j.'_1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag'.$j.'_1'];
							} else {
								$credit_tax_num = 51;
							}
						} else {
							//支払の場合
							if (count($auto_journal_data_arr) == 0) {
								$credit_tax_num = 2;
							} else if ($auto_journal_data_arr[0]['credit_flag'.$j.'_1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag'.$j.'_1'];
							} else {
								$credit_tax_num = 2;
							}
						}
						$debit_tax_class = $tax_class_arr[$debit_tax_num];
						$credit_tax_class = $tax_class_arr[$credit_tax_num];
						if ($debit_tax_num <= 29 || $debit_tax_num == NULL) {$debit_tax_flag = 0;} else {$debit_tax_flag = 1;}
						if ($credit_tax_num <= 29 || $credit_tax_num == NULL) {$credit_tax_flag = 0;} else {$credit_tax_flag = 1;}

				?>
						<tr>
							<td onclick="delRow(this)" name="<?php echo $i + 1;?>" class="minus" id="account_table<?php echo $i;?>"></td>
							<td>
								<input class="account" style="width:90px;" id="account_table<?php echo $i;?><?php echo $j;?>1" name="account_table<?php echo $i;?>_<?php echo $j;?>1" onclick="" value="<?php echo $auto_journal_data_arr[0]['debit_account_name'.$j];?>" onmouseout="" type="text"/><br/>
								<input type='text' class="supplement" placeholder="補助科目" id='account_table<?php echo $i;?><?php echo $j;?>8' name='account_table<?php echo $i;?>_<?php echo $j;?>8' value="<?php echo $auto_journal_data_arr[0]['debit_sub_account_name'.$j];?>" />
							</td>
							<td>
								<input type='text' class="section" placeholder="部門" id='account_table<?php echo $i;?><?php echo $j;?>9' name='account_table<?php echo $i;?>_<?php echo $j;?>9' value="<?php echo $auto_journal_data_arr[0]['debit_section_name'.$j];?>" />
							</td>
							<td class="value"><input class="value" id="account_table<?php echo $i;?><?php echo $j;?>2" name="account_table<?php echo $i;?>_<?php echo $j;?>2" value="<?php echo round($invoice_data_arr[$i]['total_price'] * $auto_journal_data_arr[0]['debit_ratio'.$j],0);?>" type="text" /></td>
							<td>
								<input class="value" id="account_table<?php echo $i;?><?php echo $j;?>3" name="account_table<?php echo $i;?>_<?php echo $j;?>3" value="<?php echo round($invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['debit_ratio'.$j] * $auto_journal_data_arr[0]['debit_flag'.$j.'_1'],0);?>" type="hidden" />
								<div class="custom">
									<select class="value" id="account_table<?php echo $i;?><?php echo $j;?>3_1" name="account_table<?php echo $i;?>_<?php echo $j;?>3_1">
									<?php
										for ($op_num = 0;$op_num < 128;$op_num++) {
											if ($tax_class_arr[$op_num] != "") {
												if($op_num == $debit_tax_num){$debit_select_flag = "selected"; } else {$debit_select_flag = "";}
												echo '<option value="'.$op_num.'" '.$debit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
											}
										}
									?>
									</select>
								</div>
							</td>
							<td style="padding:0 0 0 10px;">
								<input class="account" style="width:90px;" id="account_table<?php echo $i;?><?php echo $j;?>4" name="account_table<?php echo $i;?>_<?php echo $j;?>4" value="<?php echo $auto_journal_data_arr[0]['credit_account_name'.$j];?>" type="text" /><br/>
								<input type='text' class="supplement" placeholder="補助科目" id="account_table<?php echo $i;?><?php echo $j;?>10" name="account_table<?php echo $i;?>_<?php echo $j;?>10" value="<?php echo $auto_journal_data_arr[0]['credit_sub_account_name'.$j];?>" />
							</td>
							<td>
								<input type='text' class="section" placeholder="部門" id="account_table<?php echo $i;?><?php echo $j;?>11" name="account_table<?php echo $i;?>_<?php echo $j;?>11" value="<?php echo $auto_journal_data_arr[0]['credit_section_name'.$j];?>" />
							</td>
							<td class="value"><input class="value" id="account_table<?php echo $i;?><?php echo $j;?>5" name="account_table<?php echo $i;?>_<?php echo $j;?>5" value="<?php echo round($invoice_data_arr[$i]['total_price'] * $auto_journal_data_arr[0]['credit_ratio'.$j],0);?>" type="text"></td>
							<td class="value">
								<input class="value" id="account_table<?php echo $i;?><?php echo $j;?>6" name="account_table<?php echo $i;?>_<?php echo $j;?>6" value="<?php echo round($invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['credit_ratio'.$j] * $auto_journal_data_arr[0]['credit_flag'.$j.'_1'],0);?>" type="hidden">
								<div class="custom">
									<select class="value" id="account_table<?php echo $i;?><?php echo $j;?>6_1" name="account_table<?php echo $i;?>_<?php echo $j;?>6_1">
									<?php
										for ($op_num = 0;$op_num < 128;$op_num++) {
											if ($tax_class_arr[$op_num] != "") {
												if($op_num == $credit_tax_num){$credit_select_flag = "selected"; } else {$credit_select_flag = "";}
												echo '<option value="'.$op_num.'" '.$credit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
											}
										}
									?>
									</select>
								</div>
							</td>
							<td style="padding:0 0 0 10px" class="value"><input class="remarks" id="account_table<?php echo $i;?><?php echo $j;?>7" name="account_table<?php echo $i;?>_<?php echo $j;?>7" value="<?php echo $invoice_data_arr[$i]['product_name'].' '.$invoice_data_arr[$i]['remarks1'];?>" type="text"></td>
							
				<?php
					if ($withholding_tax_flag > 0) {
						if ($claimant_id == $company_id) {
							echo "<script type='text/javascript'>document.getElementById('account_table".$i.$sub_row_num."2').value = $withholding_tax;document.getElementById('account_table".$i.$sub_row_num."1').value = '仮払税金';document.getElementById('account_table".$i.$sub_row_num."8').value = '源泉所得税';document.getElementById('account_table".$i.$sub_row_num."3_1').options[0].selected = 1;document.getElementById('account_table".$i.$sub_row_num."6_1').options[0].selected = 1;</script>";
						} else {
							echo "<script type='text/javascript'>document.getElementById('account_table".$i.$sub_row_num."5').value = $withholding_tax;document.getElementById('account_table".$i.$sub_row_num."4').value = '預り金';document.getElementById('account_table".$i.$sub_row_num."10').value = '源泉所得税';document.getElementById('account_table".$i.$sub_row_num."3_1').options[0].selected = 1;document.getElementById('account_table".$i.$sub_row_num."6_1').options[0].selected = 1;</script>";
						}
					}
				?>

							
						</tr>
				<?php
					}
				?>
			</table>
			<?php
				$invoice_total += $invoice_data_arr[$i]['total_price'];
				if ($i == $count_data_num - 1) {
					echo '</div><div id="line2"></div>';
					echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
				}
			}//forの終わり
			?>
			<?php
				if ($page_num > 1) {
					echo '<div class="pageBtnBox"><a class="pageBtn" href="?page=1&sort_key='.$sort_key.'&sort_type='.$sort_type.'&mode=download&koumoku=1"><<</a>';
					for($p = 1;$p <= $page_num;$p++) {
						$bc_class = "";//初期化
						if ($page == $p) {$bc_class = " chBackColor";}
						echo '<a class="pageBtn'.$bc_class.'" href="?page='.$p.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&mode=download&koumoku=1">'.$p.'</a>';
					}
					echo '<a class="pageBtn" href="?page='.$page_num.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&mode=download&koumoku=1">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
				}
			?>
			<input type="hidden" id="data_num" name="data_num" value="<?php echo $count_data_num;?>" />
			<input type="button" id="outcsv" onclick="submit()" value="仕訳を保存する" />
		<?php
		} else {
		echo "<p class='red'>未仕訳のデータはありません</p>";
		}//ifの終わり
		?>
		</form>
	</section>
</article>
<?php
	}
?>
<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(10)"></div>

<?php require("footer.php");?>