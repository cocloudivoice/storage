<?php
require_once("Mail.php");
require_once("Mail/mime.php");

$params = array(
  "host" => "mail.co.cloudinvoice.co.jp",
  "port" => 587,
  "auth" => true,
  "username" => "test2@co.cloudinvoice.co.jp",
  "password" => "1234abcd"
);

$mailObject = Mail::factory("smtp", $params);

mb_language("ja");
mb_internal_encoding("UTF-8");



$body = <<<EOS
マルチパートメールのテストです。
ヒアドキュメントを使って複数行の文字列を作成しています。
日本語の場合は文字コードの変換が必要です。
添付ファイルのテストです。tst
EOS;

$body = mb_convert_encoding($body, "ISO-2022-JP", "auto");

$mimeObject = new Mail_Mime("¥n");
$mimeObject -> setTxtBody($body);
$mimeObject -> addAttachment("./fff006s.jpg",'image/jpeg');

$bodyParam = array(
  "head_charset" => "ISO-2022-JP",
  "text_charset" => "ISO-2022-JP"
);

//$body = $mimeObject -> get($bodyParam);

$addHeaders = array(
  "To" => "test2<test2@co.cloudinvoice.co.jp>",
  "From" => "akira<akira.hamasaki@co.cloudinvoice.co.jp>",
  "Subject" => mb_encode_mimeheader("テストメール")
);

$headers = $mimeObject -> headers($addHeaders);
$recipients = "test2@co.cloudinvoice.co.jp";
$mailObject -> send($recipients, $headers, $body);


?>
