<?php require("header.php");?>

<title>HOME - Cloud Invoice</title>

<?php
if (isset($_REQUEST['dl_inv'])) {
	$dl_inv = htmlspecialchars($_REQUEST['dl_inv'],ENT_QUOTES);
}
//必要なクラスファイルを読み込む
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//変数の宣言
$company_id = sprintf("%012d",$company_id);
$words = "";
$words2 = "";
$flag = "";
$sql = "";

$aj_words = "";
$having = "";

//ユーザーテーブルのコントローラーを呼び出す
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$maindb_con = new db_control();

//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}

//各テーブルからデータを取得する
$flag="invoice_total_data_receive";
$words = "";//条件をここに入れる。
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words,$words2);
$invoice_data_receive_num = count($invoice_data_receive_arr);
//rsort($invoice_data_receive_arr);

if ($invoice_data_receive_num > 5) {
	$invoice_data_receive_num = 5;
}

//var_dump($invoice_data_receive_arr);

echo date_default_timezone_set();
$start_d = date('Ymd', mktime(0,0,0,date('m')+1,"01",date('Y')-1));//去年の来月1日

//請求書データから請求者のデータを取得
$claimant_id = $_REQUEST['cl'];
$flag = "invoice_total_data_send_narrow_down_all";
$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."' GROUP BY `invoice_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$cl_invoice_data_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_invoice_data_arr);
$cl_invoice_data_num = count($cl_invoice_data_arr);

//総支払額
$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."'  GROUP BY `destination_id` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$cl_total_payment_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_invoice_data_arr);
$cl_total_payment = $cl_total_payment_arr[0]['sum(total_price)'];

//グラフ用データ
$flag = "invoice_total_data_send_narrow_down_make_graph_all";
$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."'  GROUP BY ymd HAVING 1 ".$having."";
$cl_graph_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_graph_arr);
$graph_num = count($cl_graph_arr);
$graph_num = 12;
$cl_graph_arr[0]['ymd'];

if ($cl_graph_arr[0]['ymd'] != "") {
	$start_date = date("Y-m",strtotime($cl_graph_arr[0]['ymd']));
} else {
	$start_date = date("Y-m");
}
//list($year, $month) = explode("-", $start_date, 2);
$year = date("Y");
$month = date("m") - 11;

for ($k = 0; $k < $graph_num; $k++) {
	
	//echo $cl_labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	$cl_labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$cl_labels .= ",";
	}
	
	$mcount = 0;
	for ($m = 0; $m < $graph_num; $m++) {
		if (!empty($cl_graph_arr[$m]['ymd'])) {
			if ( "'".$cl_graph_arr[$m]['ymd']."'" == $temp_date) {
				$mcount = 1;
			//echo "<br/>\r\n";
				$cl_params .= "".$cl_graph_arr[$m]['sum(`total_price`)'];
			//echo "<br/>\r\n";
			}
		}
		if ($m == 11 && $mcount == 0) {
			$cl_params .= "0";
		}
		if ($m == 11 && $k !=11) {
			$cl_params .= ",";
		}
	}
}
/*
for ($k = 0;$k < $graph_num;$k++) {

	$cl_labels .= "'".$cl_graph_arr[$k]['ymd']."'";
	if ($k != $graph_num - 1) {
		$cl_labels .= ",";
	}
	
	
	//echo "<br/>\r\n";
	$cl_params .= "".$cl_graph_arr[$k]['sum(`total_price`)'];
	//echo "<br/>\r\n";
	if ($k != $graph_num - 1) {
		$cl_params .= ",";
	}
}
*/


//企業データ存在確認（元側）▼
$flag = "company_data";
$words = "";
$cl_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_company_data_arr);
$cl_company_data_arr[0]["company_name"];





//請求書データから請求者のデータを取得
if ($_REQUEST['dst']) {
	$destination_id = $_REQUEST['dst'];
} else {
	$destination_id = "";
}
$flag = "invoice_data_send_total_narrow_down";
//$words = $aj_words." AND `destination_id`= '".$destination_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `invoice_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."'  GROUP BY `invoice_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$dst_invoice_data_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words,$words2);
//var_dump($dst_invoice_data_arr)
if (!empty($dst_invoice_data_arr)) {
	$dst_invoice_data_num = count($dst_invoice_data_arr);
} else {
	$dst_invoice_data_num = 0;
}
//総請求金額
//$words = $aj_words." AND `destination_id`= '".$destination_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `claimant_id` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."'  GROUP BY `claimant_id` HAVING 1 ".$having." ORDER BY `pay_date` ASC";

$dst_total_payment_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($dst_invoice_data_arr);
$dst_total_payment = $dst_total_payment_arr[0]['sum(total_price)'];

//グラフ用データ
$flag = "invoice_total_data_send_narrow_down_make_graph";
//$words = $aj_words." AND `destination_id`= '".$destination_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY ymd HAVING 1 ".$having."";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) AND `pay_date`>='".$start_d."'  GROUP BY ymd HAVING 1 ".$having."";
$dst_graph_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);

//var_dump($dst_graph_arr);
//$graph_num = count($dst_graph_arr);
$graph_num = 12;
$dst_graph_arr[0]['ymd'];

if ($dst_graph_arr[0]['ymd'] != "") {
	$start_date = date("Y-m",strtotime($dst_graph_arr[0]['ymd']));
} else {
	$start_date = date("Y-m");
}


//list($year, $month) = explode("-", $start_date, 2);
$year = date("Y");
$month = date("m") - 11;
if(!isset($graph_num)) {$graph_num = 0;}
for ($k = 0; $k < $graph_num; $k++) {
	//echo $labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	/*
	$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}
	*/
	//echo $cl_labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	//$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}

	$mcount = 0;
	for ($m = 0; $m < $graph_num; $m++) {
		if (isset($dst_graph_arr[$m]['ymd'])){
			if ( "'".$dst_graph_arr[$m]['ymd']."'" == $temp_date) {
				$mcount = 1;
			//echo "<br/>\r\n";
				$params .= "".$dst_graph_arr[$m]['sum(`total_price`)'];
			//echo "<br/>\r\n";
			}
		}
		if ($m == 11 && $mcount == 0) {
			$params .= "0";
		}
		if ($m == 11 && $k !=11) {
			$params .= ",";
		}
	}
}

/*
for ($k = 0;$k < $graph_num;$k++) {
	$labels .= "'".$dst_graph_arr[$k]['ymd']."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}
	//echo "<br/>\r\n";
	$params .= "".$dst_graph_arr[$k]['sum(`total_price`)'];
	//echo "<br/>\r\n";
	if ($k != $graph_num - 1) {
		$params .= ",";
	}
}
*/

//企業データ存在確認（元側）▼
$flag = "company_data";
$words = "";
$dst_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($dst_company_data_arr);
$dst_company_data_arr[0]["company_name"];




?>
<script type="text/javascript" src="//co.cloudinvoice.co.jp/js/Chart.js/Chart.js"></script>
<script src="../js/Chart_Dynamic_View.js"></script>
<!--<link rel="stylesheet" href="../css/chart.css" type="text/css" />-->
<style type="text/css">
#main_box {
	/*
	position:relative;
	overflow:hidden;
	min-height:800px;
	width:auto;
	height:auto;
	*/
}
#left *{
	margin-left:20px;
}
#right *{
	margin-left:20px;
	/*
	position: relative;
	*/
}
canvas {
	margin-left:20px;
	/*
	position: absolute;
	*/
	/*
	position: relative;
    */
    /*
	z-index: 1;
	*/
}
#graph_tbl {
	/*
	position: absolute;
	*/
	top: 300px;
	width:400px;
}
#graph_tbl td{
	width:200px;
}
#graph_tbl th{
	width:100px;
}
</style><script type="text/javascript">

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});
</script>
<!--[if IE]>
<script type="text/javascript" src="../js/excanvas.js"></script>
<![endif]-->

<script type="text/javascript">
<!--
/*
window.onload = function() {
	//描画コンテキストの取得
	var canvas = document.getElementById('graph');
	if (canvas.getContext) {
		var context = canvas.getContext('2d');
		var ctx = context;
		
		var data = {
				//横軸のラベル
				labels : [<?php echo $labels;?>],
				datasets : [

				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $params;?>]
				}

			]

		}
		
		//棒グラフ
		var myNewChart = new Chart(ctx).Bar(data);
		//線グラフ
		//var myNewChart = new Chart(ctx).Line(data);
		//多角グラフ
		//var myNewChart = new Chart(ctx).Radar(data);
	}
	
	
			//描画コンテキストの取得
	var canvas2 = document.getElementById('graph2');
	if (canvas2.getContext) {

		var context2 = canvas2.getContext('2d');
		var ctx2 = context2;
		
		var data2 = {
				//横軸のラベル
				labels : [<?php echo $cl_labels;?>],
				datasets : [
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $cl_params;?>]
				}

			]

		}
		
		//棒グラフ
		var myNewChart2 = new Chart(ctx2).Bar(data2);
		//線グラフ
		//var myNewChart2 = new Chart(ctx2).Line(data2);
		//多角グラフ
		//var myNewChart2 = new Chart(ctx2).Radar(data2);
	}

	
}
*/
-->
</script>
<script type="text/javascript">
	
	function cnt_reload() {
		
		//リロードする。
	    location.href="./main";
	}

	function reload() {
		//alert("reload");
		//関数cnt_reload()を1000ミリ秒間隔で呼び出す
		setTimeout("cnt_reload()",2000);
	}

</script>




<article>
<?php
/*
<!--	<section id="m-1-box">
		<h2>
			記帳代行 納品履歴（最新5件）
		</h2>

		<table id="scsv">
			<tr>
				<th id="date">納品日</th>
			</tr>
	<?php
		for ($i = 0; $i < $csv_history_num; $i++) {
	?>
			<tr>
				<td><span style="color:black;font-weight:bold;"><?php echo $csv_history_data_arr[$i]["upload_date"];?></span> に納品が完了しました。</td>
			</tr>
	<?php
		}
	?>
		</table>


	</section>
-->
*/
?>
	<section id="m-1-box">
		<h2>
			お知らせ
		</h2>
		<?php
			$flag = "comment_receive" ;
			$comment_arr = $maindb_con -> db_control_sql_flag($pdo,$flag,$company_id,$words);
			$comment_arr_num = count($comment_arr);
			if ($comment_arr_num > 3) {
				$comment_arr_num = 3;
			}
			if ($comment_arr_num == 0) {echo "現在、お知らせはありません";} else {
				for ($i = 0; $i < $comment_arr_num; $i++) {
		?>
		<ul>
			<li>
				<a href="./notice_body?id=<?php echo $comment_arr[$i]['id'];?>">
					<?php echo $comment_arr[$i]['title'];?>
				</a>
			</li>
		</ul>
		<?php
				}
			}
		?>
		<!--
		<div id="koushin">
			<a href="./notice">>>過去のお知らせを見る。</a>
		</div>
		-->


	</section>

<section id="customer" style="width:500px;">

</section>


</article>

<?php require("footer.php");?>
