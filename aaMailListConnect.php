<?php
    require_once dirname(__FILE__) . '/aaMailListInc.php';

    //-------------------------------
    //接続＆ログイン
    //-------------------------------
    //接続
    $fp = fsockopen($mail_host, $mail_port);

    //ログイン
    $line = fgets($fp, 512);
    fputs($fp, "USER $mail_user\r\n");    // USER名
    $line = fgets($fp, 512);        
    fputs($fp, "PASS $mail_pwd\r\n");    // パスワード
    $line = fgets($fp, 512);
    if(!preg_match("(OK)", $line)){    // ログイン失敗？
        fclose($fp);
        die("ログイン失敗");
    }