<?php session_start(); ?>
<?php require("header.php");?>

<title>取引先管理 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if (isset($_SESSION['user_id'])){
	$company_id = $user_id = $_SESSION['user_id'];
}

if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}

$del_flag = $_REQUEST['del_flag'];
$num = $_REQUEST['memo_num'];
$memo_id = $_REQUEST['memo_id'.$num];
$memo_text = $_REQUEST['memo'.$num];

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

if ($del_flag == 1) {
	//削除はせず過去データとしてhistoricalに1を入れて見えなくする。
	//過去の請求書にデータを保護する為。
	//識別はclient_idで行ない、編集の場合も元のデータはhistoricalに1を入れて
	//データをコピーした新たな取引先を作成する。
	$sql = "UPDATE `VENDOR_TABLE` SET 
		`del_flag` = 1 WHERE id = ".$_REQUEST['id']."";
	$company_con -> company_sql($pdo,$company_id,$sql);
}

//▼メモ登録処理▼
if (isset($memo_text)) {
	$sql = "UPDATE `VENDOR_TABLE` SET 
		`memo`='".$memo_text."' 
		 WHERE `id`='".$memo_id."';";
	$company_con -> company_sql($pdo,$company_id,$sql);
}

//▲メモ登録処理▲

//現在は使用していないはず。
/*
if (isset($_REQUEST['email'])) {
	$sql = "UPDATE `VENDOR_TABLE` SET 
		`client_id`='".$_REQUEST['client_id']."',`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`vendor`='".$_REQUEST['vendor']."',`department`='".$_REQUEST['department']."',`position`='".$_REQUEST['position']."', 
		`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`address_full`='".$_REQUEST['prefecture'].$_REQUEST['address1'].$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',`fax_num`='".$_REQUEST['fax_num']."',
		`email`='".$_REQUEST['email']."',`client_company_id`='".$_REQUEST['client_company_id']."' 
		 WHERE id = ".$_REQUEST['id']." ";
	$company_con -> company_sql($pdo,$company_id,$sql);

}
*/
//設定変更の処理▲

//設定内容の表示データ取得
$flag = "vendor_data_all";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($company_arr);
?>
<script type="text/javascript" src="../js/table-sorter-head.js"></script>
<script>

function OpenVendorEditFrame(cid){
	document.getElementById("vendorEditFrame").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	document.getElementById("vendorEditFrame").setAttribute('src', './frame/vendorEditFrame?cid='+cid+'');
}

function OpenVendorEditFrameIns(){
	document.getElementById("vendorEditFrame").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	document.getElementById("vendorEditFrame").setAttribute('src', './frame/vendorEditFrame?ins=1');
}

function openText(n) {
	var sw = document.getElementById("sw_flag" + n).value;
	var mem = document.getElementById("memo" + n);
	var td = document.getElementById("yourMemo" + n);
	if (sw == 0) {
		document.getElementById("sw_flag" + n).value = 1;
		mem.style="background-color:white;width:100px;";
		td.onclick="";
	} else if (sw == 1) {
		document.getElementById("sw_flag" + n).value = 0;
		mem.style="display:none;";
		td.onclick="openText(" + n + ")";
	}
}

</script>
<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#vendorTable").colResizable({fixed:false});
		$("#vendorTable").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>

<iframe id="vendorEditFrame" src="./frame/vendorEditFrame"></iframe>


<article>

	<section id="m-3-box">
		<h2>
			取引先管理
		</h2>
		<button onclick="OpenVendorEditFrameIns('')">
		新しい取引先を登録
		</button>
		
		<form name="form_sansho" action="./upload.php" method="post" enctype="multipart/form-data">

			<input type="file" name="upfile" />
			<input type="hidden" name="return_url" value="./vendor">
			<input type="hidden" name="ahead_url" value="./checkvendorcsv">
			<input type="hidden" name="colms" value="18">
			<?php $_SESSION['comment'] = "";?>

			
		<button  onclick="submit();">確認画面に進む</button>
		<a href="../files/common/send_sample.csv" target="_blank">
		雛形について
		</a>
		
		</form>
		<?php echo $up_info_msg;?>
			<table id="vendorTable" class="tablesorter tables dragging">
			<colgroup>
			<!--<col align="center" width="15%">-->
			<col align="center" width="120px">
			<col align="center" width="120px">
			<!--<col align="center" width="10%">-->
			<!--<col align="center" width="10%">-->
			<col align="center" width="60px">
			<!--<col align="center" width="10%">-->
			<col align="center" width="60px">
			<col align="center" width="40px">
			<col align="center" width="200px">
			<col align="center" width="200px">
			<col align="center" width="100px">
			<col align="center" width="10px">
			<col align="center" width="10px">
			</colgroup>
			<thead>
			<tr id="first">
				<!--<th>取引先コード</th>-->
				<th>顧客コード</th>
				<th>宛先</th>
				<!--<th>共通コード</th>-->
				<!--<th>共通コード宛先</th>-->
				<th>担当者部署</th>
				<!--<th>担当者役職</th>-->
				<th>担当者氏名</th>
				<th>電話番号</th>
				<th>住所１</th>
				<th>メールアドレス</th>
				<th>メモ</th>
				<th>編集</th>
				<th>削除</th>
			</tr>
			</thead>
			<tbody>
			<?php for ($i = 0; $i < count($company_arr); $i++) { ;
				$client_comp_id = $company_arr[$i]["client_company_id"];
			?>
			<tr>
				<!--<td id="vendorNo"><?php echo $company_arr[$i]["client_id"];?></td>-->
				<td id="souhuNo"><?php echo $company_arr[$i]["customer_code"];?></td>
				<td id="souhusaki"><?php echo $company_arr[$i]["company_name"];?></td>
				<!--<td id="yourBango"><?php if ($client_comp_id == 0) {echo "";} else { echo $client_comp_id;}?></td>-->
				<?php 
					$flag = "company_data";
					$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$client_comp_id,$words);
				?>
				<!--<td id="souhusaki" style="background-color:<?php if ($client_comp_id != 0 && !isset($company_one_arr[0]['company_name'])) { echo 'red';}?>;"><?php echo $company_one_arr[0]["company_name"];?></td>-->
				<td id="yourBusho"><?php echo $company_arr[$i]["department"];?></td>
				<td id="yourPosition"><?php echo $company_arr[$i]["position"];?></td>
				<!--<td id="yourTanto"><?php echo $company_arr[$i]["vendor"];?></td>-->
				<td id="Bango"><?php echo $company_arr[$i]["tel_num"];?></td>
				<td id="yourAddress1"><?php echo $company_arr[$i]["address1"];?></td>
				<td id="yourAddress"><?php echo $company_arr[$i]["email"];?></td>
				<td id="yourMemo<?php echo $i;?>" onclick="openText(<?php echo $i;?>);"><?php echo nl2br($company_arr[$i]['memo']);?><form action="" name="memo_form<?php echo $i;?>" method="post"><input type="hidden" id="sw_flag<?php echo $i;?>" name="sw_flag<?php echo $i;?>" value="0" /><textarea id="memo<?php echo $i;?>" name="memo<?php echo $i;?>" style="display:none;" onblur="submit();"><?php echo $company_arr[$i]['memo'];?></textarea><input type="hidden" name="memo_num" value="<?php echo $i;?>" /><input type="hidden" name="memo_id<?php echo $i;?>" value="<?php echo $company_arr[$i]['id'];?>" /></form></td>
				<td id="souhuEdit"><button id="vendorEditButton" onclick="OpenVendorEditFrame('<?php echo $company_arr[$i]['client_id'];?>')"></button></td>
				<td id="souhuDelete"><form action="" method="post" ><input type="hidden" name="id" value="<?php echo $company_arr[$i]['id'];?>" /><input type="hidden" name="del_flag" value="1" /><button id="vendorDeleteButton" onclick="submit();"></button></form></td>
			</tr>
	<?php };?>
		
		</tbody>
		</table>
		<div class="tableText">
			<label id="tableTxt"></label>				
		</div>


		</section>
</article>

<div id="fadeLayer" onclick="CloseFrame(4)"></div>
<div id="noneLayer"></div>


<?php require("footer.php");?>