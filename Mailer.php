<?php

require_once("/var/www/co.cloudinvoice.co.jp/lib/PHPMailer_5.2.0/class.phpmailer.php"); // this is PHPMailer for PHP4

// 使用方法
require_once(dirname(__FILE__)."/../invoice/Mailer.php");

class Mailer {
    var $mail;
    function Mailer() {
        $this->__construct();
    }

    function __construct() {
        $this->init();
    }

    function init() {
        mb_language("japanese");
        mb_internal_encoding("UTF-8");
        $this->mail = new PHPMailer();
        //$this->mail->SMTPDebug = 3;
        $this->mail->IsHTML(false);
        $this->mail->CharSet = "iso-2022-jp";
        $this->mail->Encoding = "7bit";
    }

    function useSMTP($host = null) {
        if ($host == null) {
            $host = 'www2561.sakura.ne.jp';
        }
        $this->mail->IsSMTP();
        $this->mail->Host = $host;
    }

    function useSMTPAuth($user = null, $password = null) {
        if ($user == null) {
            return false;
        }
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $user;
        $this->mail->Password = $password;
    }

    function useWordWrap($wordwrap = 70) {
        $this->mail->WordWrap = $wordwrap; // set word wrap to 70 characters
    }

    function setHostName($host = null) {
        if ($host == null) {
            $host = 'www2561.sakura.ne.jp';
        }
        $this->mail->Hostname = $host;  // set word wrap to 70 characters
    }

    function setPriority($priority = 3) { // 1 = High, 3 = Normal, 5 = low
        if ($priority != 1 && $priority != 3 && $priority != 5) {
            trigger_error('invalid arg', E_USER_ERROR);
        }
        $this->mail->Priority = $priority;
    }

    function tmpl_send($args = array()) {
        if (!isset($args['tmpl_file']) || !isset($args['tmpl_vars'])) {
            die('tmpl_file & tmpl_vars are required');
        }
        require_once('/var/www/lib/Smarty/Smarty.class.php');
        $smarty = new Smarty();
		$smarty->compile_dir = '/tmp/smarty/' . $smarty->compile_dir;
        if (isset($args['tmpl_vars'])) {
            $smarty->assign($args['tmpl_vars']);
        }
        $args['body'] = $smarty->fetch($args['tmpl_file']);
        $this->send($args);
    }

    function send($args = array()) {
        foreach (array('to', 'from', 'subject', 'body') as $key) {
            if (!isset($args[$key])) {
                die($key . ' is required');
            }
        }

        $this->mail->From = $args['from'];
        if (isset($args['from'])) {
            $this->mail->FromName = mb_encode_mimeheader(mb_convert_encoding($args['from'],"JIS","UTF-8"));
        }
        else {
            $this->mail->FromName = '';
        }

        $this->mail->ClearAllRecipients(); // clear first
        if (isset($args['to']) && $args['to']) {
            if (is_array($args['to'])) {
                foreach ($args['to'] as $to) {
                    $this->mail->AddAddress($to);
                }
            }
            else {
                $this->mail->AddAddress($args['to']);
            }
        }
        if (isset($args['cc']) && $args['cc']) {
            if (is_array($args['cc'])) {
                foreach ($args['cc'] as $cc) {
                    $this->mail->AddCC($cc);
                }
            }
            else {
                $this->mail->AddCC($args['cc']);
            }
        }
        if (isset($args['bcc']) && $args['bcc']) {
            if (is_array($args['bcc'])) {
                foreach ($args['bcc'] as $bcc) {
                    $this->mail->AddBcc($bcc);
                }
            }
            else {
                $this->mail->AddBcc($args['bcc']);
            }
        }
        $this->mail->Subject = '=?iso-2022-jp?B?'. base64_encode(mb_convert_encoding($args['subject'], "JIS", "UTF-8")).'?=';
        //$this->mail->Subject = mb_encode_mimeheader(mb_convert_encoding($args['subject'],"JIS","EUC-JP"));
        $this->mail->Body  = mb_convert_encoding($args['body'],"JIS","UTF-8");
        
        ////添付ファイル
        //$mail->AddAttachment($attachfile);
         
        if (!$this->mail->Send()){
            error_log("mail sending failed ERRROR:" . $this->mail->ErrorInfo);
            return false;
        }
        return true;
    }
}

?>
