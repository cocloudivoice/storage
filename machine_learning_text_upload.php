<?php require("header.php");?>


<title>仕訳の機械学習 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words = "";
$flag = "";
$sql = "";
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}


$dir_path = dirname(__FILE__)."/../pdf/png/".$af_id;
$dir_path3 = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLUpdir";
$dir_path4 = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLResult";

?>

<article>
	
	<section id="m-1-box">
		<h2>
			機械学習
		</h2>
		<p>
		機械学習のテストです。<br>
		学習用（train）ファイルをサーバーにアップロードしてこのボタンを押す。
		</p>
	
		<form name="form_sansho" action="./upload_machine_learning_for_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:0 0 10px 0;">■画像アップロード</p>
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="ahead_url" value="./machine_learning_text_upload" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="学習用テキストアップロード" />
		</form>
		<br><br>
		
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./upload_machine_learning_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:0 0 10px 0;">■学習</p>
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="ahead_url" value="./machine_learning_text_upload" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="学習開始" />
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>
			
	</section>
	
	<section>
		<div style="margin:20px 0 10px 10px;">
		<p style="margin:0 0 10px 0;">■手動ダウンロード</p>
		<input type="button" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path;?>'" value="テキスト化したファイルをダウンロード"/>
		</div>
	</section>

	<?php //if ($af_id == "5129391867" || $af_id == "3308277917") {?>
		<hr>
		<section style="margin:10px 0 0 10px;">
			
		<h3>
			機械学習汎用
		</h3>
		<p>
		サーバー内の会計データを機械学習します。<br>
		ファイルは別途サーバーにアップロードしてこのボタンを押す。
		</p>
		
		<form name="form_sansho" action="./upload_imgs_for_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:10px 0 10px 0;">■会計データアップロード</p>
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $dir_path3;?>" />
				<input type="hidden" name="return_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="ahead_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="unzip_flag" value="1" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="hidden" name="unzip_flag" value="1" />
			<input type="button" onclick="submit();" value="画像アップロード" />
		</form>
		
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./machine_learning_data_reading" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:10px 0 10px 0;">■会計データの読込</p>
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path3;?>" />
				<input type="hidden" name="return_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="ahead_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="return_flag" value="1" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="テキスト化" />
		</form>
		<form name="form_sansho" action="./machine_learning_upload6" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<p style="margin:10px 0 10px 0;">■仕上げ処理</p>
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path4;?>" />
				<input type="hidden" name="return_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="ahead_url" value="./machine_learning_text_upload" />
				<input type="hidden" name="return_flag" value="1" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="仕上げ処理" />
		</form>

		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>
			
			<div>
			
			<p style="margin:10px 0 10px 0;">■自動アップロードからの手動ダウンロード</p>
			<input type="button" style="margin:0 0 10px 0;" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path4;?>'" value="ファイルサーバーへのダウンロード"/>
			</div>
		</section>
	<?php //}?>

</article>
<?php require("footer.php");?>