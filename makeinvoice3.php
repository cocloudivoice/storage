<?php require("header.php");?>
<?php

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/db_control.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//var_dump($_REQUEST);
$value = array();
//echo $_REQUEST["send_code"];
if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $_REQUEST["destination_id"] != "" || $_REQUEST["send_code"] != "")) {
	if (isset($_REQUEST["seikyu_10"])) { $value[7] = htmlspecialchars($_REQUEST["seikyu_10"],ENT_QUOTES); }//振込先
	if (isset($_REQUEST["seikyu_11"])) { $value[18] = htmlspecialchars($_REQUEST["seikyu_11"],ENT_QUOTES); }//備考　消すかも
	if (isset($_REQUEST["destination_id"])) { $value[0] = htmlspecialchars($_REQUEST["destination_id"],ENT_QUOTES); }//共通コード
	if (isset($_REQUEST["send_code"])) { $value[1] = htmlspecialchars($_REQUEST["send_code"],ENT_QUOTES); }//共通コード
	if (isset($_REQUEST["invoice_code"])) { $value[3] = htmlspecialchars($_REQUEST["invoice_code"],ENT_QUOTES); }
	if (isset($_REQUEST["billing_date"])) { $value[2] = htmlspecialchars($_REQUEST["billing_date"],ENT_QUOTES); }
	if (isset($_REQUEST["pay_date"])) { $value[6] = htmlspecialchars($_REQUEST["pay_date"],ENT_QUOTES); }
	//if (isset($_REQUEST["destination_name"])) { $value[7] = htmlspecialchars($_REQUEST["destination_name"],ENT_QUOTES); }
	//if (isset($_REQUEST["destination_zip_code"])) { $value[7] = htmlspecialchars($_REQUEST["destination_zip_code"],ENT_QUOTES); }
	//if (isset($_REQUEST["destination_address"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address"],ENT_QUOTES); }
	//if (isset($_REQUEST["destination_address2"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address2"],ENT_QUOTES); }
	if (isset($_REQUEST["destination_clerk"])) { $value[5] = htmlspecialchars($_REQUEST["destination_clerk"],ENT_QUOTES).htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }


	$sql = "SELECT `invoice_code` 
	FROM `INVOICE_DATA_TABLE` WHERE claimant_id = ".$company_id." 
	AND `destination_id` = ".$value[0]." 
	AND `invoice_code` = '".$value[3]."'
	";
	//echo "\r\n";

	$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
	if ($invoice_code_check[0] != NULL) {
		//$_SESSION['up_info_msg'] = "請求書番号が重複しているのでデータを登録できません。";
		echo "<script type='text/javascript'>alert('請求書番号が重複しているのでデータを登録できません。');location.href='./makeinvoice';</script>";
		//header("Location:./makeinvoice", false);
		exit();
	} else if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $value[0] != "" || $value[1] != "")) {

		//ループ回数の取得
		if (isset($_REQUEST["roop_times"])) { echo $roop_times = htmlspecialchars($_REQUEST["roop_times"],ENT_QUOTES);}
		$roop_times = 10;//仮置き
		//ループして明細データを取得する
		for ( $i = 1 ; $i <= $roop_times ; $i++ ) {
			if ($_REQUEST["seikyu_1".$i."2"] != "") {
				if (isset($_REQUEST["seikyu_1".$i."1"])) { $value[10] = htmlspecialchars($_REQUEST["seikyu_1".$i."1"],ENT_QUOTES); }//品目
				if (isset($_REQUEST["seikyu_1".$i."2"])) { $value[11] = htmlspecialchars($_REQUEST["seikyu_1".$i."2"],ENT_QUOTES); }//単価
				if (isset($_REQUEST["seikyu_1".$i."3"])) { $value[12] = htmlspecialchars($_REQUEST["seikyu_1".$i."3"],ENT_QUOTES); }//数量
				if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[14] = htmlspecialchars($_REQUEST["seikyu_1".$i."4"],ENT_QUOTES); }//金額
				if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round($value[14] * 108/100,0); }//金額
				if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[15] = round($value[14] * 8/100,0); }//消費税
				if (isset($_REQUEST["seikyu_1".$i."5"])) { $value[16] = htmlspecialchars($_REQUEST["seikyu_1".$i."5"],ENT_QUOTES); }//備考もしくは源泉所得税
			
				$sql = "
					INSERT INTO `INVOICE_DATA_TABLE` (
					`claimant_id`,`destination_id`, `send_code`, `billing_date`, `invoice_code`,
					`invoice_name`, `staff_name`, `pay_date`,
					`bank_account`, `sale_date`, `product_code`,
					`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
					`sales_tax`, `withholding_tax`, `total_price`,
					`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`, `insert_date`
					) VALUES 
					( ".intval($company_id).", ".intval($value[0]).", '".str_replace(',','',$value[1])."', ".intval(str_replace(array(',','/','-'),'',$value[2])).", '".$value[3]."',
					'".$value[4]."', '".$value[5]."', ".intval(str_replace(array(',','/','-'),'',$value[6])).",
					'".$value[7]."', ".intval(str_replace(array(',','/','-'),'',$value[8])).", '".$value[9]."',
					'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval($value[12]).", '".$value[13]."',".intval(str_replace(',','',$value[14])).",
					'".$value[15]."', '".$value[16]."', ".intval(str_replace(',','',$value[17])).",
					'".$value[18]."', '".$value[19]."', '".$value[20]."', '".$csv_id."', '".$_SESSION['file_name']."', cast( now() as datetime)
					)";echo "\r\n";

				$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
				
			}
		}
	}
	//データの登録が成功したらメッセージを表示する
	if ($check_arr['chk']['check']){
		//$_SESSION['up_info_msg'] = "1件の請求書を登録しました";
		echo "<script type='text/javascript'>alert('1件の請求書を登録しました');</script>";
	} else {
		echo "<script type='text/javascript'>alert('入力不備のため請求書を登録できませんでした');</script>";
		echo "<script type='text/javascript'>location.href='./makeinvoice';</script>";
		exit();
	}

}

//送付先管理のデータ取得
$flag = "company_data";
$destination_id_box = $_REQUEST["get_destination_id"];
$destination_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id_box,$words);
//var_dump($destination_data_arr);
$check_destination_data = count($destination_data_arr);
$flag="";
$words="";

//送付先管理のデータ取得
$flag = "addressee_data_all_with_conditions";
$condition_words = "ORDER BY `company_name` ASC ";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$condition_words);
$flag="";
$words="";
//var_dump($company_arr);


if ($destination_data_arr[0]["company_id"] != 0 && isset($destination_data_arr[0]["company_id"])) {
	if (isset($destination_data_arr[0]["company_id"])) { $destination_company_id = $destination_data_arr[0]["company_id"];}
	if (isset($destination_data_arr[0]["company_name"])) { $destination_name = $destination_data_arr[0]["company_name"];}
	if (isset($destination_data_arr[0]["zip_code"])) { $destination_zip_code = $destination_data_arr[0]["zip_code"];}
	if (isset($destination_data_arr[0]["address1"])) { $destination_address = $destination_data_arr[0]["prefecture"].$destination_data_arr[0]["address1"];}
	if (isset($destination_data_arr[0]["address2"])) { $destination_address2 = $destination_data_arr[0]["address2"];}
	if (isset($destination_data_arr[0]["section"])) { $destination_clerk = $destination_data_arr[0]["section"]; }
	if (isset($destination_data_arr[0]["clerk"])) { $destination_clerk2 = $destination_data_arr[0]["clerk"]; }
}
if (isset($_REQUEST["get_client_id"]) && $_REQUEST["get_client_id"] != "") { $get_client_id = htmlspecialchars($_REQUEST["get_client_id"],ENT_QUOTES); }
//一件の送付先管理のデータ取得
$flag = "addressee_data_one";
$words = "AND company_id = ".$company_id."";
$client_company_arr = $company_con -> company_sql_flag($pdo,$flag,$get_client_id,$words);
$flag="";
$words="";
//var_dump($client_company_arr);

if ($client_company_arr[0]["client_id"] != NULL && $client_company_arr[0]["client_id"] != "") {
	if (isset($client_company_arr[0]["client_id"])) { $destination_client_id = $client_company_arr[0]["client_id"];}
	if (isset($client_company_arr[0]["client_company_id"])) { $destination_company_id = $client_company_arr[0]["client_company_id"];}
	if (isset($client_company_arr[0]["company_name"])) { $destination_name = $client_company_arr[0]["company_name"];}
	if (isset($client_company_arr[0]["zip_code"])) { $destination_zip_code = $client_company_arr[0]["zip_code"];}
	if (isset($client_company_arr[0]["address1"])) { $destination_address = $client_company_arr[0]["prefecture"].$client_company_arr[0]["address1"];}
	if (isset($client_company_arr[0]["address2"])) { $destination_address2 = $client_company_arr[0]["address2"];}
	if (isset($client_company_arr[0]["position"])) { $destination_clerk = $client_company_arr[0]["department"].$client_company_arr[0]["position"]; }
	if (isset($client_company_arr[0]["addressee"])) { $destination_clerk2 = $client_company_arr[0]["addressee"]; }
}


//作成済みの請求書の編集処理




//作成済みの請求書の編集処理
?>
<script>

function onLoadData() {
	for (var i = 1 ; i <= 10 ; i++ ) {
		Chen(i);
		Chen("1"+i+"1");
		Chen("1"+i+"2");
		Chen("1"+i+"3");
		Chen("1"+i+"4");
		Chen("1"+i+"5");
	}
}

function Chen(n){
	try {
		if (n == 2 || n == 3) {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			if (Seikyu_No != "") {
				Seikyu_No = Seikyu_No.substring(0, 4)+"/"+Seikyu_No.substring(4, 6)+"/"+Seikyu_No.substring(6, 8);
				parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			}
		} else {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			//parent.invoice1.document.getElementById("seikyu_" + n).innerHTML = Seikyu_No;
		}
	} catch (e) {
	}
}

function Calc(n){
	try {
		n = parseInt(n,10);
		document.fm_left["seikyu_" + (n+2)].value = document.fm_left["seikyu_" + n].value * document.fm_left["seikyu_" + (n+1)].value;
	} catch (e) {
	}
}

function StartBgCalc() {
	try {
		//parent.invoice1.document.fm_right["seikyu_" + n].value
		parent.invoice1.BgCalc();
	} catch (e) {
	}
}


function addRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==11){alert("これ以上登録する事はできません。");return;}
	var colCnt = tblObj.rows[0].cells.length;
	var row = tblObj.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='hinmoku'><input type='text' id='hinmoku' onkeyup='Chen(1" + rowCnt + "1)' onblur='Chen(1" + rowCnt + "1);' name='seikyu_1" + rowCnt + "1'></td>"
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='tanka'><input type='text' id='tanka' onkeyup='Chen(1" + rowCnt + "2)' onblur='Chen(1" + rowCnt + "2);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "2'></td>"
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='suuryou'><input type='text' id='suuryou' onkeyup='Chen(1" + rowCnt + "3)' onblur='Chen(1" + rowCnt + "3);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "3'></td>"
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='goukei'><input type='text' id='goukei' onkeyup='Chen(1" + rowCnt + "4)' onblur='Chen(1" + rowCnt + "4)' name='seikyu_1" + rowCnt + "4' readonly='readonly' /></td>"
	var cell = row.insertCell(4);
	cell.innerHTML = "<td id='shousai'><input type='text' id='shousai' onkeyup='Chen(1" + rowCnt + "5)' onblur='Chen(1" + rowCnt + "5)' name='seikyu_1" + rowCnt + "5'></td>"

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	var colCnt = tblFr.rows[0].cells.length;
	var row = tblFr.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='item'><input type='text' class='item' id='item" + rowCnt + "' name='seikyu_1" + rowCnt + "1' readonly='readonly' /></td>"
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='cost'><input type='text' class='cost' id='cost" + rowCnt + "' name='seikyu_1" + rowCnt + "2' readonly='readonly' /></td>"
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='amount'><input type='text' class='amount' id='amount" + rowCnt + "' name='seikyu_1" + rowCnt + "3' readonly='readonly' /></td>"
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "4' readonly='readonly' /></td>"
//	var cell = row.insertCell(4);
//	cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "5' readonly='readonly' /></td>"
}


function delRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblObj.deleteRow(-1);

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblFr.deleteRow(-1);
}

function OpenChangeInvoiceFrame(){
	document.getElementById("ChangeInvoice").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}



function openClient(){
	document.getElementById("openClient").style.display="block";
}
function kaihei() {
	var elem = document.getElementById('openClient');
	elem.className = (elem.className == 'hide') ? 'show' : 'hide';
}

function DnameInsert() {
	var dname = document.getElementById('select_dname').value;
	document.getElementById('seikyu_4').value = dname;
	Chen(4);
}

function TransClientId() {
	var client_code = document.getElementById('select_dname').value;
	document.getElementById('addressee_company_info').value = client_code;

}

function TransNo() {
	var destination_code = document.getElementById('Kyoutu_No').value;
	document.getElementById('company_info').value = destination_code;

}

function getCompanyInfo() {
	document.getElementById('destination_code_sender').submit();
}

function getAddresseeCompanyInfo() {
	document.getElementById('client_code_sender').submit();
}

function CheckBeforeSend() {
	var seikyu_no = document.getElementById('seikyu_1').value;
	var kyotu_no = document.getElementById('Kyoutu_No').value;
	var kanri_no = document.getElementById('select_dname').value;
	if (seikyu_no == "") {
		alert('請求書番号がありません');return false;
	} else if (kyotu_no == "" && kanri_no == ""){
		alert('共通コードと送付先コードのどちらかを入力してください');return false;
	} else {
		return true;
	}
}


</script>



<?php 
echo "<p style='color:red;'>".$_SESSION['up_info_msg']."</p>";
$_SESSION['up_info_msg'] = "";


?>
<form id="destination_code_sender" name="destination_code_sender" action="" method="post">
	<input id="company_info" name="get_destination_id" type="hidden" value="" />
</form>
<form id="client_code_sender" name="client_code_sender" action="" method="post">
	<input id="addressee_company_info" name="get_client_id" type="hidden" value="" />
</form>
	
<form name="fm_left" action="" method="post" onSubmit="return CheckBeforeSend();">
<iframe id="ChangeInvoice" src="./frame/ChangeInvoice"></iframe>

<title>請求書作成 - Cloud Invoice</title>
<article id="makeinvoice">

	<section id="leftmi">
		<div id="headmi">
			<h2>請求書作成</h2>
			<input type="button" value="テンプレート切り替え" onClick="window.top.invoice1.location.href = './invoiceframe/invoice2'">
		</div>

		<div id="kyoutu">
			<h3>共通コード</h3>
			<script>
				document.write('<input type="text" id="Kyoutu_No" name="destination_id" onChange="TransNo();" value="<?php echo $destination_company_id;?>" />');
			</script>
				<input id="Kyoutu_No" type="button" value="共通番号から会社情報を呼び出す" onClick="TransNo();getCompanyInfo();">
			<div id="clear"></div>
		</div>
		<div id="kyoutu">
			<h3>送付先コード</h3>
			<select id="select_dname" onclick="DnameInsert();TransClientId();" >
				<option value=""></option>
				<?php
				for($k = 0; $k < count($company_arr); $k++ ) {
					echo '<option value="'.$company_arr[$k]["client_id"].'">'.$company_arr[$k]["company_name"].' ( '.$company_arr[$k]["client_id"].' )</option>';
				
				}
				?>

			</select>
			<input id="company_info_button" type="button" value="送付先コードから会社情報を呼び出す" onClick="getAddresseeCompanyInfo()">
		</div>
		<div id="clear"></div>
		<div id="seikyuushobangou">
			<h3>請求書番号</h3>
		
				<input type="text" onkeyup="Chen(1)" onblur="Chen(1)" name="invoice_code" id="seikyu_1">
		
			<div id="clear"></div>
		</div>

		<div id="seikyuubi">
			<h3>請求日(例:20141031)</h3>

				<input type="text" onkeyup="Chen(2)" onblur="Chen(2)" name="billing_date" id="seikyu_2">

			<div id="clear"></div>
		</div>

		<div id="shiharaikigen">
			<h3>支払期限(例:20141130)</h3>

				<input type="text" onkeyup="Chen(3)" onblur="Chen(3)" name="pay_date" id="seikyu_3">

<!--
			<input type="button" value="当月末">
			<input type="button" value="翌月末">
			<input type="button" value="翌々月末">
-->
		</div>
		<div id="clear"></div>

			<script>
				document.write('<input type="hidden" onkeyup="Chen(4)" onchange="Chen(4)" name="destination_name" id="seikyu_' + '4" value="<?php echo $destination_name;?>" readonly="disabled" />');
			</script>
			<input type="hidden" name="send_code" id="send_code" value="<?php echo $destination_client_id;?>" />
			<?php
/*
				for($j = 0; $j < count($company_arr); $j++ ) {
					echo "<input type='hidden' id='client_id".$k."' name='client_id".$k."' value='".$company_arr[$j]["client_id"]."' />";
					echo "<input type='hidden' id='company_name".$k."' name='company_name".$k."' value='".$company_arr[$j]["company_name"]."' />";
					echo "<input type='hidden' id='addressee".$k."' name='addressee".$k."' value='".$company_arr[$j]["addressee"]."' />";
					echo "<input type='hidden' id='department".$k."' name='department".$k."' value='".$company_arr[$j]["department"]."' />";
					echo "<input type='hidden' id='position".$k."' name='position".$k."' value='".$company_arr[$j]["position"]."' />";
					echo "<input type='hidden' id='zip_code".$k."' name='zip_code".$k."' value='".$company_arr[$j]["zip_code"]."' />";
					echo "<input type='hidden' id='prefecture".$k."'name='prefecture".$k."' value='".$company_arr[$j]["prefecture"]."' />";
					echo "<input type='hidden' id='address1".$k."'name='address1".$k."' value='".$company_arr[$j]["address1"]."' />";
					echo "<input type='hidden' id='address2".$k."'name='address2".$k."' value='".$company_arr[$j]["address2"]."' />";
					echo "<input type='hidden' id='tel_num".$k."'name='tel_num".$k."' value='".$company_arr[$j]["tel_num"]."' />";
					echo "<input type='hidden' id='email".$k."'name='email".$k."' value='".$company_arr[$j]["email"]."' />";
					
				}
*/
			?>
			<script>
				document.write('<input type="hidden" onkeyup="Chen(5)" onblur="Chen(5)" name="destination_zip_code" id="seikyu_' + '5" value="<?php echo $destination_zip_code;?>" readonly="readonly" />');
			</script>
		
			<script>
				document.write('<input type="hidden" onkeyup="Chen(6)" onblur="Chen(6)" name="destination_address" id="seikyu_' + '6" value="<?php echo $destination_address;?>" readonly="readonly" />');
			</script>
			<script>
				document.write('<input type="hidden" onkeyup="Chen(7)" onblur="Chen(7)" name="destination_address2" id="seikyu_' + '7" value="<?php echo $destination_address2;?>" readonly="readonly" />');
			</script>

			<script>
				document.write('<input type="hidden" onkeyup="Chen(8)" onblur="Chen(8)" name="destination_clerk" id="seikyu_' + '8" value="<?php echo $destination_clerk;?>" readonly="readonly" />');
			</script>
			<script>
				document.write('<input type="hidden" onkeyup="Chen(9)" onblur="Chen(9)" name="destination_clerk2" id="seikyu_' + '9" value="<?php echo $destination_clerk2;?>" readonly="readonly" />');
			</script>

		<div id="clear"></div>

		<div id="meisai">
			<table id="myTBL">
				<tr id="first">
					<th id="hinmoku">品目</th>
					<th id="tanka">単価</th>
					<th id="suuryou">数量</th>
					<th id="goukei">金額</th>
					<th id="shousai">詳細</th>
				</tr>
				<tr>
					<td id="hinmoku">
						<script>
							document.write('<input type="text" onkeyup="Chen(111)" onblur="Chen(111)" id="hinmoku" name="seikyu_' + '111">');
						</script>
					</td>
					<td id="tanka">
						<script>
							document.write('<input type="text" onkeyup="Chen(112)" onblur="Chen(112);Calc(112);Chen(114);StartBgCalc();" id="tanka" name="seikyu_' + '112">');
						</script>
					</td>
					<td id="suuryou">
						<script>
							document.write('<input type="text" onkeyup="Chen(113)" onblur="Chen(113);Calc(112);Chen(114);StartBgCalc();" id="suuryou" name="seikyu_' + '113">');
						</script>
					</td>
					<td id="goukei">
						<script>
							document.write('<input type="text" onkeyup="Chen(114)" onblur="Chen(114)" id="goukei" name="seikyu_' + '114" readonly="readonly" />');
						</script>
					</td>
					<td id="shousai">
						<script>
							document.write('<input type="text" onkeyup="Chen(115)" onblur="Chen(115)" id="shousai" name="seikyu_' + '115">');
						</script>
					</td>
				</tr>
			</table>
			<input type="button" value="1行挿入" onclick="addRow()" />
			<input type="button" value="1行削除" onclick="delRow()">
		</div>
		<div id="hurikomisaki">
			<!--
			<h3>振込先</h3>
			<script>
				document.write('<textarea onkeyup="Chen(10)" onblur="Chen(10)" id="hurikomisaki" name="seikyu_' + '10"></textarea>');
			</script>
			-->
		</div>
		<div id="bikou">
			<h3>備考</h3>
			<script>
				document.write('<textarea onkeyup="Chen(11)" onblur="Chen(11)" id="bikou" name="seikyu_' + '11"></textarea>');
			</script>
		</div>
		
		<input type="submit" value="請求書を作成する">
</form>

	</section>
	
	<section>
	<iframe src="./invoiceframe/invoice1" scrolling="no" name="invoice1" id="invoice1"></iframe>
	</section>
	<div></div>
</article>

<div id="fadeLayer"></div>

<?php require("footer.php");?>

</body>
