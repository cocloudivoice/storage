<?php 
session_start();

//必要なクラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
//ページ生成用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = sprintf("%012d",$_SESSION['user_id']);
} else {
	header("Location:./logout");
	exit();
}

//変数
$_REQUEST["koumoku"] = 1;
$_REQUEST["mode"] = "download";

//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = '/var/www/storage.cloudinvoice.co.jp/html/invoice/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            $tax_class = split(",",$buffer);
            $tax_class_arr[$tax_class[0]] = $tax_class[1];
            
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}


if ( $_REQUEST['mode'] === 'download' ) {

	if ( $_REQUEST['koumoku'] == 1 ) {
		//明細別
		$flag1 = "invoice_count";
		$flag = "invoice_data_to_autojournalize";
	} else if ($_REQUEST['koumoku'] == 0) {
		//請求別
		$flag1 = "invoice_total_count";
		$flag = "invoice_total_data";
	}
	if ($_REQUEST['destination_id']) {
		$claimant_id = $_REQUEST['destination_id'];
		$words .= " AND `claimant_id` = ".$claimant_id." ";
	}
	if ($_REQUEST['update_from']){
			$upfrom = str_replace(" ","",substr($_REQUEST['update_from'],0,4)."/".substr($_REQUEST['update_from'],4,2)."/".substr($_REQUEST['update_from'],6,2));
			$words .= " AND `update_date` >= '".$upfrom."' ";
	}
	if ($_REQUEST['update_to']){
			$upto = str_replace(" ","",substr($_REQUEST['update_to'],0,4)."/".substr($_REQUEST['update_to'],4,2)."/".substr($_REQUEST['update_to'],6,2));
			$words .= " AND `update_date` <= '".$upto."' ";
	}
	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$words .= " AND `pay_date` >= ".$payfrom." ";
	}
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$words .= " AND `pay_date` <= ".$payto." ";
	}
	//statusが99の請求書（削除データ）を除く
	$words .= " AND `status` <> 99 ";

	
	//ソートの設定
	if (isset($_GET['sort_num']) && $_GET['sort_num'] != NULL) {
		$sort_num = htmlspecialchars($_GET['sort_num'],ENT_QUOTES);
		$sort_key = $sort_num;
	} else {
		$sort_key = "display_rank";//$_GET['sort_name'];//GETか何かでカラム名取得を実装する。
	}

	if (isset($_GET['sort_key']) && $_GET['sort_key'] != NULL) {
		$sort_key = htmlspecialchars($_GET['sort_key'],ENT_QUOTES);
	}
	if (isset($_GET['sort_type']) && $_GET['sort_type'] != NULL) {
		$sort_type = htmlspecialchars($_GET['sort_type'],ENT_QUOTES);
	}
	if ($_REQUEST['sort_type'] == 1) {
		if ($sort_type =="ASC" || $sort_type == "") {
			$sort_type ="DESC";
		} else {
			$sort_type = "ASC";
		}
	} else {
		if ($sort_type == "DESC") {
			$sort_type = "DESC";
		} else {
			$sort_type = "ASC";
		}
	}
	if ($_REQUEST['page']) {
		$page_n = $_REQUEST['page'];
	} else {
		$page_n = 0;
	}
	//ページ条件なしでデータ数を取得
	$sort_key = "`id`";
	$sort_type = "ASC";
	$conditions = " ".$sort_key." ".$sort_type." ";	
	
	//$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	//仕訳済みフラグのためここで処理を分ける。
	$page_words .= $words." GROUP BY `invoice_code` ORDER BY".$conditions;
	$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$page_words);
	//var_dump($data_num_arr);
	//echo "<br>";
	$current_invoice_code = $data_num_arr[$page_n]["invoice_code"];
	//echo "<br>";
	//表示数の制御
	$max_disp = 1;
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	//$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";

	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	$words .= " AND `invoice_code` = '".$current_invoice_code."'";
	$words .= " ORDER BY ";
	$words .= $conditions;
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	$invoice_data_num = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	$invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	//var_dump($invoice_data_arr);

	//▼自動仕訳用企業DB登録▼
	//もし存在しなければ、企業品目テーブルと企業用レコードテーブルを作成
	$table_name = "".$company_id;
	try {
		$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
	} catch (PDOException $e) {
	    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
	    $_SESSION['error_msg'] = $e->getMessage();
	}
	//▲自動仕訳用企業DB登録▲

	//var_dump($invoice_data_num);
	//var_dump($invoice_data_arr);
	//$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
    //出力ファイル名の作成
    $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
  
    //文字化けを防ぐ
    $csv_data = mb_convert_encoding ( $csv_data , "utf-8" , 'utf-8' );
      
    //MIMEタイプの設定
    //header("Content-Type: application/octet-stream");
    //名前を付けて保存のダイアログボックスのファイル名の初期値
    //header("Content-Disposition: attachment; filename={$csv_file}");
  
    // データの出力
    //echo($csv_data);
    //exit();
    $flag = "select_max_journalized_id";
    $table_name = "JOURNALIZED_HISTORY_".sprintf("%012d",$company_id);
    $aj_words = "";
    $max_jh_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
    $max_jh_num = $max_jh_num_arr[0]['max(`text_data3`)'] + 1;
    $flag = "";
    $table_name = "";
    $aj_words = "";

}
?>
<?php require("header.php");?>

<script>

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box3" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box4" ).datepicker();
});

function OpenkCommonSearchFrame(){
	document.getElementById("kCommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}

function FrameHeight() {
	window.setTimeout("window.parent.frames['kCommonSearch'].test()","600");
}

function TransNo() {
	//var destination_code = document.getElementById('Kyoutu_No').value;
	//document.getElementById('Kyotu_No_Invisible').value = destination_code;
}
function getCompanyInfo() {

}

function changeAccountName(n,m) {
	document.getElementById('debit_account_name'+n).value = document.getElementById('account_names'+n).value;
	//document.getElementById('debit_account_name'+n).class = "no_record";
	document.getElementById('count_flag'+n).value = m;
}

function openDDBox(n) {
	document.getElementById('account_names' + n).setAttribute("style", "display:visible");
	document.getElementById('disp_br' + n).setAttribute("style", "display:visible");
}

function closeDDBox(n) {
	document.getElementById('account_names' + n).setAttribute("style", "display:none");
	document.getElementById('disp_br' + n).setAttribute("style", "display:none");
}

function closeSetTime(n) {
	var stclose = setTimeout("closeDDBox("+ n +")",10000);
	//stclose = setTimeout();
}

function checkAccount(n) {
	var debit_ac_name = document.getElementById('debit_account_name' + n).value;
	if ( debit_ac_name == "" ) {
		document.getElementById('debit_account_name' + n).value = document.getElementById('account_names' + n).value;
	}
}

function getTotal(n) {
	document.getElementById('invoice_total_top' + n).value = document.getElementById("invoice_total" + n).value;
}

function addRow(button,num) {
	var tblId = button.getAttribute("id");
	var tblObj = document.getElementById(tblId);
	var rowCnt = tblObj.rows.length;
	if(rowCnt==10) { alert("これ以上登録する事はできません。");return; }
	var row = tblObj.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.setAttribute("id",tblId);
	cell.setAttribute("class","minus");
	cell.setAttribute("name",rowCnt);
	cell.setAttribute("onclick","delRow(this)");
	cell.innerHTML = "\n";
	var cell = row.insertCell(1);
	cell.innerHTML = "<input type='text' style='width:90px' class='account' id='" + tblId + rowCnt + "1' name='" + tblId + "_" + rowCnt + "1' onclick='' onmouseout='' /><br><input type='text' class='supplement' id='" + tblId + rowCnt + "7' name='" + tblId + "_" + rowCnt + "7' /><input type='hidden' class='supplement' placeholder='補助科目' name='" + tblId + "_" + rowCnt + "8'/><input type='hidden' class='section' placeholder='部門' name='" + tblId + "_" + rowCnt + "9'/>\n";
	document.getElementById(tblId + rowCnt + "1").value = document.getElementById("debit_account_name" + num).value;
	var cell = row.insertCell(2);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='text' class='value' id='" + tblId + rowCnt + "2' name='" + tblId + "_" + rowCnt + "2' value='0' />\n<input type='hidden' class='value' id='" + tblId + rowCnt + "3' name='" + tblId + "_" + rowCnt + "3' value='0' />\n"
	+'<div class="custom" style="display:none;">'
	+'<select class="value" id="' + tblId + rowCnt + '3_1" name="' + tblId + '_' + rowCnt + '3_1">'
	<?php
	for ($op_num = 0;$op_num < 128;$op_num++) {
		if ($tax_class_arr[$op_num] != "") {
			echo '+'.'\'<option value="'.$op_num.'">'.str_replace(array("\r\n","\r","\n"),"",$tax_class_arr[$op_num]).'</option>\'';
		}
	}
	?>
	+'</select>'
	+'</div>';
	var cell = row.insertCell(3);
	cell.setAttribute("style","padding:0 0 0 10px;")
	cell.innerHTML = "<input type='text' placeholder='貸方科目' style='width:90px' class='' id='" + tblId + rowCnt + "4' name='" + tblId + "_" + rowCnt + "4' value='' /><br><input type='hidden' class='supplement' placeholder='補助科目' name='" + tblId + "_" + rowCnt + "10'/><input type='hidden' class='section' placeholder='部門' name='" + tblId + "_" + rowCnt + "11'/>\n";
	var cell = row.insertCell(4);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='text' class='value' id='" + tblId + rowCnt + "5' name='" + tblId + "_" + rowCnt + "5' value='0' />\n";
	var cell = row.insertCell(5);
	cell.setAttribute("class","value");
	cell.innerHTML = "<input type='hidden' class='value' id='" + tblId + rowCnt + "6' name='" + tblId + "_" + rowCnt + "6' value='0' />\n"
	+'<div class="custom" style="display:none;">'
	+'<select class="value" id="' + tblId + rowCnt + '6_1" name="' + tblId + '_' + rowCnt + '6_1">'
	<?php
	for ($op_num = 0;$op_num < 128;$op_num++) {
		if ($tax_class_arr[$op_num] != "") {
			echo '+'.'\'<option value="'.$op_num.'">'.str_replace(array("\r\n","\r","\n"),"",$tax_class_arr[$op_num]).'</option>\'';
		}
	}
	?>
	+'</select>'
	+'</div>';
	var cell = row.insertCell(6);
	cell.setAttribute("class","value");
	cell.setAttribute("style","padding:0 0 0 10px")
	document.getElementById(tblId + rowCnt + "7").value = document.getElementById("remarks" + num).value;
	
	document.getElementById(tblId + "rowNow").value=rowCnt + 1;
}

function delRow(button) {
	var tblId = button.getAttribute("id");
	var tblObj = document.getElementById(tblId);
	var tblName = button.getAttribute("name");
	var rowCnt = tblObj.rows.length;
	var i = tblName * 1 + 1;
	for (i; i <rowCnt; i++) {
		try {
			var k = i - 1;
			var cell = tblObj.rows[i].cells[0];
			//alert(showObject(cell));
			cell.setAttribute("name",k);
			//var cell = tblObj.rows[i].cells[1];
			for (var rownum = 1; rownum <= 7; rownum++ ) {
				var cell_data = document.getElementById(tblId + i + rownum);
				cell_data.name = tblId + "_" + k + rownum;
				cell_data.id = tblId + k + rownum;
			}
		} catch (e) {
			alert(e);
		}
	}
	var tbl = document.getElementById(tblId);
	var delRow = button.parentNode.rowIndex;
	var rows = tbl.deleteRow(delRow);
	var i = 0;
	var k = 0;

	document.getElementById(tblId + "rowNow").value = document.getElementById(tblId + "rowNow").value - 1;
}

function unusedData(n) {
	var m = document.getElementById("unused_flag" + n).value;
	if (m == 0) {
		document.getElementById("unused_flag" + n).value = 1;
	} if (m == 1) {
		document.getElementById("unused_flag" + n).value = 0;
	}
}


</script>
<script>
$(function() {
	var n = $('#data_num').val();
	for (var i = 0; i < n; i++) {
		try {
			var m = $('#invoice_total' + i).val();
			if (m > 0) {
				l = eval(m).toLocaleString();
				//$('#invoice_total_top' + i).text(l);
				$('#invoice_total_top' + i).val() = l;
			}
		}catch (e) { 
			//alert(e);
		}
	}
});


function showObject(elm,type) {
	var str = '「' + typeof elm + "」の中身";
	var cnt = 0;
	for(i in elm) {
		if(type == 'html') {
			str += '<br />\n' + "[" + cnt + "] " + i.bold() + ' = ' + elm[i];
		} else {
			str += '\n' + "[" + cnt + "] " + i + ' = ' + elm[i];
		}
		cnt++;
		status = cnt;
	}
	return str;
}

function changeSize() {
	var size = document.getElementById("data_img").alt
	if (size == "証憑画像") {
		document.getElementById("data_img").style = "float:left;max-width:800px;max-height:1000px;cursor:pointer;";
		document.getElementById("data_img").alt = "証憑画像小";
	} else if (size == "証憑画像小") {
		document.getElementById("data_img").style = "float:left;max-width:500px;max-height:800px;cursor:pointer;";
		document.getElementById("data_img").alt = "証憑画像";
	}
}

function changeAOM(n) {
		var amount = document.getElementById("invoice_total_top" + n).value;
		document.getElementById("total_price_excluding_tax" + n).value = amount;
		document.getElementById("base_amount_of_money" + n).value = amount;
		document.getElementById("credit_amount_of_money" + n).value = amount;
		document.getElementById("total_price" + n).value = amount;
}

function changeSubSupple() {
	var n = document.getElementById("chsub_flag").value;
	if (n == 0){
		for (var i = 0; i < <?php echo count($invoice_data_arr);?>; i++ ) {
			document.getElementById("debit_sub_account_name" + i).type="hidden";
			document.getElementById("debit_section_name" + i).type="hidden";
			document.getElementById("credit_sub_account_name" + i).type="hidden";
			document.getElementById("credit_section_name" + i).type="hidden";
		}
		document.getElementById("chsub_flag").value = 1;
	} else if (n == 1) {
		for (var i = 0; i < <?php echo count($invoice_data_arr);?>; i++ ) {
			document.getElementById("debit_sub_account_name" + i).type="text";
			document.getElementById("debit_section_name" + i).type="text";
			document.getElementById("credit_sub_account_name" + i).type="text";
			document.getElementById("credit_section_name" + i).type="text";
		}
		document.getElementById("chsub_flag").value = 0;
	}
}

</script>

<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>

<title>自動仕訳作成 - Cloud Invoice"</title>
<?php
$claimant_id = $data_num_arr[$page_n]["claimant_id"];
$destination_id = $data_num_arr[$page_n]["destination_id"];
$top4 = substr($claimant_id,0,4);
$mid4 = substr($claimant_id,4,4);
$str = $invoice_data_arr[$i]["pay_date"];
$pdf_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";
$img_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".png";
?>
<input type="hidden" name="journalized_flag<?php echo $i;?>" value="<?php echo $journalized_flag;?>" />
<input type="hidden" name="des_cla_flag<?php echo $i;?>" value="<?php if ($claimant_id == $company_id) {echo 2;} else {echo 1;}?>" />
<input type="hidden" id="unused_flag<?php echo $i;?>" name="unused_flag<?php echo $i;?>" cols="1" width="10px" value="0" /><!--0:過去仕訳に登録する 1:過去仕訳に登録しない-->

<article id="autojournal2">

	<?php
	$count_data_num = count($invoice_data_arr);
	if ( $count_data_num > 0) {
		echo '<form action="./shiharaiautojournalcsv_shunju" method="post">';
		//echo '<input type="button" onclick="submit()" value="CSVで出力する" /><br/><br/>';
		$invoice_code_checker = "";
		$claimant_id_checker = "";
	?>
	<?php 
		$k = 0;//ヘッダーの番号を覚えておく変数
		for ( $i = 0; $i < intval($count_data_num); $i++ ) {
			//var_dump($invoice_data_arr[$i]);
			$withholding_tax = $invoice_data_arr[$i]['withholding_tax'];
			$withholding_tax_flag = $withholding_tax * 1;
			$journalized_flag = $invoice_data_arr[$i]['journalized'];
			if ($invoice_code_checker != $invoice_data_arr[$i]['invoice_code'] || $claimant_id_checker != $invoice_data_arr[$i]["claimant_id"]) {
				$invoice_code_checker = $invoice_data_arr[$i]['invoice_code'];
				$claimant_id_checker = $invoice_data_arr[$i]["claimant_id"];
				$max_jh_num++;
				if ($i > 0) {
					echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
					$invoice_total = 0;
					$k = $i;
					echo "
						</div>
						";
				}
	?>
		<?php
			$claimant_id = $invoice_data_arr[$i]["claimant_id"];
			$destination_id = $invoice_data_arr[$i]["destination_id"];
			$send_code = $invoice_data_arr[$i]["send_code"];
		?>
		<?php
			$company_id = sprintf("%012d",$company_id);
			//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
			$flag = "company_data";
			
				if ($claimant_id == $company_id) {
					if ($send_code != "" || $send_code != NULL) {
						//echo "a";
						$flag = "addressee_data_one";
						$conditions = " AND company_id = ".$company_id." ";
						$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
						//var_dump($company_send_code_check_arr[0]);
						if ($company_send_code_check_arr[0]["email"] != NULL && $company_send_code_check_arr[0]["email"] != "") {
							//echo "b";
							$company_email = $company_send_code_check_arr[0]["email"];
							$company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
							$company_email .= ",".$company_arr[0]["email"];
							$remarks_company_name = $company_arr[0]["company_name"];
							if ($company_arr[0]["company_name"] == "") {
								//echo "d";
								$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
								$remarks_company_name = $destination_company_name[0]['company_name'];
							}
						} else if ($company_send_code_check_arr[0]["company_name"] != "") {
							//echo "c";
							$remarks_company_name = $company_send_code_check_arr[0]["company_name"];
						}
					} else if ($destination_id != 0) {
						//echo "e";
						$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
						$remarks_company_name = $destination_company_name[0]['company_name'];
					}
				} else {
					//echo "f";
					$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
					$remarks_company_name = $claimant_company_name[0]['company_name'];
					$ind_type = $claimant_company_name[0]['industry_type'];
					$ind_type_s = $claimant_company_name[0]['industry_type_s'];
					$ind_info = $ind_type.$ind_type_s;
				}
		?>
		<?php
		$str = $invoice_data_arr[$i]["pay_date"];
		$download_password = $invoice_data_arr[$i]['download_password'];
		$pdf_url = '../files/' . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";
		if (!file_exists($pdf_url)) {
		?>
		<?php 
		} else {
		?>
		<?php
		}
		?>
		<?php //echo date('Y/m/d', strtotime($invoice_data_arr[$i]["billing_date"]));?>
		<?php
			$top4 = substr($claimant_id,0,4);
			$mid4 = substr($claimant_id,4,4);
			$str = $invoice_data_arr[$i]["pay_date"];
			$pdf_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".pdf";
			$img_url = '../files/'. $top4 . "/" . $mid4 . "/" . $claimant_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_arr[$i]['download_password'].".png";
		?>
		<input type="hidden" name="journalized_flag<?php echo $i;?>" value="<?php echo $journalized_flag;?>" />
		<input type="hidden" name="des_cla_flag<?php echo $i;?>" value="<?php if ($claimant_id == $company_id) {echo 2;} else {echo 1;}?>" />
		<input type="hidden" id="unused_flag<?php echo $i;?>" name="unused_flag<?php echo $i;?>" cols="1" width="10px" value="0" /><!--0:過去仕訳に登録する 1:過去仕訳に登録しない-->
	
		<div id="LeftBlock">
			<img id="data_img" src="<?php echo $img_url;?>" alt="証憑画像" style="cursor:pointer;" onclick="changeSize()" />
		</div>
		<div id="RightBlock">
			<h2>仕訳作成</h2>
			<!--<input type="button" onclick="changeSubSupple();" value="補助科目・部門">-->
			<input type="button" onclick="location.href='./shiharaiautojournal_insert5';" value="補助科目・部門なし">
			<input type="hidden" id="chsub_flag" value="1">
			<?php
				}//ifの終わり
			?>

			<div class="table">
				<div class="block">
					<p><?php echo $i + 1;?> 取引日</p>
					<input type="text" name="pay_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['pay_date'];?>" />
				</div>
				<div class="block">
					<p>金額</p>
					<input type="text" name="invoice_total_top<?php echo $i;?>" id="invoice_total_top<?php echo $i;?>" onkeyup="changeAOM(<?php echo$i;?>);" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />
				</div>
			</div>
			<div class="table">
				<div class="block">
					<p>借方勘定科目</p>
					<?php
						//自動仕訳
						//ステップ１"請求元の共通コード 品名"
						
						$flag = "auto_journal_step1";
						//echo $company_id = sprintf("%012d",$company_id);echo " ";echo $invoice_data_arr[$i]["product_name"];echo "<br/>";
						$search_key = $invoice_data_arr[$i]["claimant_id"]." ".$invoice_data_arr[$i]["product_name"];
						$table_name="RECORD"."".$company_id;
						$aj_words = $search_key;
						$auto_journal_data_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
						$credit_account_name = $auto_journal_data_arr[0]["credit_account_name"];
						$debit_account_name = $auto_journal_data_arr[0]["debit_account_name"];
						if ($debit_account_name == "" || $debit_account_name == NULL) {
							echo '<style>#account_table'.$i.' { background-color:#FFA3B1; }</style>';
						}
						//var_dump($auto_journal_data_arr);
					?>
					<?php
							
					?>
					<input class="<?php if ($debit_account_name == '' || $debit_account_name == NULL) {echo 'no_record';}?>" style="" type='text' class="" id='debit_account_name<?php echo $i;?>' name='debit_account_name<?php echo $i;?>' value='<?php if ($debit_account_name != "") { echo $debit_account_name;} else if ($claimant_id == $company_id) { echo "売掛金";};?>' onclick="openDDBox(<?php echo $i;?>);" onmouseout="closeSetTime(<?php echo $i;?>);" /><br/>
					<input type='hidden' id='count_flag<?php echo $i;?>' name='count_flag<?php echo $i;?>' value="1" />
					
					<?php
						//ステップ２"請求元企業の自動仕訳用テーブル"
						$flag = "auto_journal_step2";
						$table_name = $invoice_data_arr[$i]["claimant_id"];
						$aj_words = $invoice_data_arr[$i]["product_name"];
						$auto_journal_data_arr2 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
						
						if (count($auto_journal_data_arr2) > 0) {
					?>
					<style type="text/css">.no_record{color:red;}</style>
							<select id='account_names<?php echo $i;?>' name='step2_data<?php echo $i;?>' onclick='changeAccountName(<?php echo $i;?>,2)' style="display:none" >
					<?php
							for ($q = 0;$q < count($auto_journal_data_arr2);$q++) {
					?>
							<option value='<?php echo $auto_journal_data_arr2[$q]["account_name"];?>' onclick='closeDDBox(<?php echo $i;?>);' ><?php echo $auto_journal_data_arr2[$q]['account_name'];?></option>
					<?php
							}
					?>
							</select><br id="disp_br<?php echo $i;?>" style="display:none" />
					<?php
						} else {
							//ステップ3 "main"の自動仕訳用テーブル（業種番号で仕訳を分ける）
							if ($ind_info != "") {
								$flag = "auto_journal_step3";
								$table_name = "MAIN";
								$aj_words = $ind_info;
								$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
							}
							if (count($auto_journal_data_arr3) == 0) {
								$flag = "auto_journal_step3";
								$table_name = "MAIN";
								$aj_words = $invoice_data_arr[$i]["product_name"];
								$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
								if (count($auto_journal_data_arr3) == 0) {
									unset($auto_journal_data_arr3);
									$flag = "auto_journal_step3";
									$table_name = "MAIN";
									$aj_words = "*";
									$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);

								}
							}
							if (count($auto_journal_data_arr3) > 0) {
					?>
							<select id='account_names<?php echo $i;?>' name='step3_data<?php echo $i;?>' onclick='changeAccountName(<?php echo $i;?>,3);' style="display:none" >
					<?php
								for ($q = 0;$q < count($auto_journal_data_arr3);$q++) {
					?>
									<option value='<?php echo $auto_journal_data_arr3[$q]["account_name"];?>' onclick='closeDDBox(<?php echo $i;?>);' ><?php echo $auto_journal_data_arr3[$q]['account_name'];?></option>
					<?php
								}
					?>
								</select><br id="disp_br<?php echo $i;?>" style="display:none" />
					<?php
							}
							//"<input type='text'name='auto_csv_data[]' value='".$auto_journal_data_arr3[$q]['account_name']."' />";
						}
						echo '<script type="text/javascript">checkAccount('.$i.');</script>';
					?>
						<input type='text' class="supplement" placeholder="補助科目" id="debit_sub_account_name<?php echo $i;?>" name="debit_sub_account_name<?php echo $i;?>" value="<?php echo $auto_journal_data_arr[0]['debit_sub_account_name'];?>" />
						<input type='text' class="section" placeholder="部門" id="debit_section_name<?php echo $i;?>" name="debit_section_name<?php echo $i;?>" value="<?php echo $auto_journal_data_arr[0]['debit_section_name'];?>" />
				</div>
				<div class="block">
					<!--
					<p>借方消費税区分</p>
					-->
					<?php
						if ( $invoice_data_arr[$i]['sum(total_price_excluding_tax)'] != NULL ) { 
					?>
					<input type="hidden" name="sum(total_price_excluding_tax<?php echo $i;?>)" value="<?php $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];?>" />
					<input type="hidden" name="base_amount_of_money<?php echo $i;?>" value="<?php $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];?>" />
					</td>
					<td class="">
					<?php
						} else { 
					?>
					<?php //if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) {$debit_ratio = $auto_journal_data_arr[0]['debit_ratio'];} else {$debit_ratio = 1;} if ($withholding_tax_flag > 0 &&($claimant_id != $company_id)) { echo round(($invoice_data_arr[$i]['total_price_excluding_tax'] * $debit_ratio + $withholding_tax),0);} else {echo round($invoice_data_arr[$i]['total_price_excluding_tax'] * $debit_ratio,0);}?>
					<input type="hidden" class="value" id="total_price_excluding_tax<?php echo $i;?>" name="total_price_excluding_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) {$debit_ratio = $auto_journal_data_arr[0]['debit_ratio'];} else {$debit_ratio = 1;} if ($withholding_tax_flag > 0 &&($claimant_id != $company_id)) { echo round(($invoice_data_arr[$i]['total_price'] * $debit_ratio + $withholding_tax),0);} else {echo round($invoice_data_arr[$i]['total_price'] * $debit_ratio,0);}?>" />
					<input type="hidden" class="value" id="base_amount_of_money<?php echo $i;?>" name="base_amount_of_money<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />
					<input type="hidden" class="value" id="total_price<?php echo $i;?>" name="total_price<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />
					<input type="hidden" class="value" name="credit_amount_of_money<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['credit_ratio'] != NULL) {$credit_ratio = $auto_journal_data_arr[0]['credit_ratio'];} else {$credit_ratio = 1;}if ($withholding_tax_flag > 0 && ($claimant_id == $company_id)) {echo round(($invoice_data_arr[$i]['total_price'] * $credit_ratio + $withholding_tax),0);} else { echo round($invoice_data_arr[$i]['total_price'] * $credit_ratio,0);}?>" />
					<?php
						}
					?>
					<?php
						//税区分の処理
						//借方の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if ($auto_journal_data_arr[0]['debit_flag1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag1'];
							} else {
								$debit_tax_num = 2;
							}
						} else {
							//支払の場合
							if ($auto_journal_data_arr[0]['debit_flag1'] != 1) {
								$debit_tax_num = $auto_journal_data_arr[0]['debit_flag1'];
							} else {
								$debit_tax_num = 31;
							}
						}
						//貸方の処理
						if ($claimant_id == $company_id) {
							//売上の場合
							if ($auto_journal_data_arr[0]['credit_flag1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag1'];
							} else {
								$credit_tax_num = 51;
							}
						} else {
							//支払の場合
							if ($auto_journal_data_arr[0]['credit_flag1'] != 0) {
								$credit_tax_num = $auto_journal_data_arr[0]['credit_flag1'];
							} else {
								$credit_tax_num = 2;
							}
						}
						
						$debit_tax_class = $tax_class_arr[$debit_tax_num];
						$credit_tax_class = $tax_class_arr[$credit_tax_num];
						if($debit_tax_num <= 29 || $debit_tax_num == NULL) {$debit_tax_flag = 0;} else {$debit_tax_flag = 1;}
						if($credit_tax_num <= 29 || $credit_tax_num == NULL) {$credit_tax_flag = 0;} else {$credit_tax_flag = 1;}
					?>
					<input type="hidden" class="value" name="sales_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) { echo $debit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * round($auto_journal_data_arr[0]['debit_ratio'],0);} else if ($claimant_id == $company_id) { echo round($invoice_data_arr[$i]['sales_tax'] * 0,0);} else {echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);}?>" />
					<div class="" style="display:none;">
						<select class="value" name="debit_tax_class<?php echo $i;?>" style="width:130px;">
						<?php
							for ($op_num = 0;$op_num < 128;$op_num++) {
								if ($tax_class_arr[$op_num] != "") {
									if($op_num == $debit_tax_num){$debit_select_flag = "selected"; } else {$debit_select_flag = "";}
									echo '<option value="'.$op_num.'" '.$debit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
								}
							}
						?>
						</select>
					</div>
					<?php
						/*
						if ($auto_journal_data_arr[0]['debit_ratio'] != NULL) {
							echo $debit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['debit_ratio'],0);
						} else if ($claimant_id == $company_id) {
							echo round($invoice_data_arr[$i]['sales_tax'] * 0,0);
						} else {
							echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);
						}
						*/
					?>

					<p>貸方勘定科目</p>
					
					<input style="" type="text" class="" id="credit_account_name<?php echo $i;?>"  name="credit_account_name<?php echo $i;?>" value="<?php if (isset($credit_account_name)) {echo $credit_account_name;} else if ($debit_account_name == '仕入' || $debit_account_name == '商品' || $debit_account_name == '在庫') {echo '買掛金';} else if ($claimant_id == $company_id) { echo '売上高';} else {echo '未払金';}?>" /><br/>
					<input type='text' class="supplement" placeholder="補助科目" id='credit_sub_account_name<?php echo $i;?>' name='credit_sub_account_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_sub_account_name'];?>" /><br>
					<input type='text' class="section" placeholder="部門" id='credit_section_name<?php echo $i;?>' name='credit_section_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_section_name'];?>" />







					<p>取引先</p>
					<input type="hidden" name="return_page" value="<?php echo $page;?>">
					<input type="hidden" name="account_table_dlp<?php echo $i?>" value="<?php echo $download_password;?>" />
					<input type="hidden" name="account_table<?php echo $i?>" value="1" />
					<input type="hidden" name="claimant_id<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['claimant_id'];?>" />
					<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
					<?php
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
						$flag = "company_data";
						if ($claimant_id != 0 ) {
							$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
						} else {}
					?>
					<?php
					/*
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
						$dest_id = $invoice_data_arr[$i]["destination_id"];
						$flag = "addressee_data_receive_client_id";
						$words = " AND company_id = ".$claimant_id."";
						$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					*/
					/*
						if ($client_id_arr[0]['client_id'] == NULL) {
							$csv_data .= ",";
						} else {
							$csv_data .= $client_id_arr[0]['client_id'].",";
						}
					*/
					?>
					<input type="hidden" id="account_table<?php echo $i?>rowNow" value="1">
					<input type="hidden" name="billing_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['billing_date'];?>" />
					<input type="hidden" name="invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['invoice_code'];?>" />
					<input type="hidden" name="staff_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['staff_name'];?>" />
					<input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $claimant_company_name[0]['company_name'];?>"/>
					<input type="text" class="" id="remarks<?php echo $i;?>" name="remarks<?php echo $i;?>" value="<?php echo $remarks_company_name;?><?php //echo $invoice_data_arr[$i]['product_name'].' '.$invoice_data_arr[$i]['remarks1'];?>" />

					
				</div>
			</div>
			<!--
			<div class="table">
				<div class="block">
			-->
					<!--
					<p>貸方勘定科目</p>
					
					<input style="" type="text" class="" id="credit_account_name<?php echo $i;?>"  name="credit_account_name<?php echo $i;?>" value="<?php if (isset($credit_account_name)) {echo $credit_account_name;} else if ($debit_account_name == '仕入' || $debit_account_name == '商品' || $debit_account_name == '在庫') {echo '買掛金';} else if ($claimant_id == $company_id) { echo '売上高';} else {echo '未払金';}?>" /><br/>
					<input type='text' class="supplement" placeholder="補助科目" name='credit_sub_account_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_sub_account_name'];?>" /><br>
					<input type='text' class="section" placeholder="部門" name='credit_section_name<?php echo $i;?>' value="<?php echo $auto_journal_data_arr[0]['credit_section_name'];?>" />
					</div>
				-->
				<!--
				<div class="block">
				-->
					<!--
					<p>貸方消費税区分</p>
					-->
						<input type="hidden" class="value" name="credit_sales_tax<?php echo $i;?>" value="<?php if ($auto_journal_data_arr[0]['credit_ratio'] != NULL) { echo round($credit_tax_flag * $invoice_data_arr[$i]['sales_tax'] * $auto_journal_data_arr[0]['credit_ratio'],0);} else if ($claimant_id == $company_id) { echo round($invoice_data_arr[$i]['sales_tax'] * 1,0);} else {echo 0;}?>" />
					<div class="" style="display:none;">
						<select class="value" name="credit_tax_class<?php echo $i;?>" style="width:130px;">
						<?php
							for ($op_num = 0;$op_num < 128;$op_num++) {
								if ($tax_class_arr[$op_num] != "") {
									if ($op_num == $credit_tax_num) {$credit_select_flag = "selected"; } else {$credit_select_flag = "";}
									echo '<option value="'.$op_num.'" '.$credit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
								}
							}
						?>
						</select>
					</div>
			<!--
				</div>
			</div>
			-->
			<div class="table">
				<div class="block">
				</div>
				<div class="block">
					<!--
					<p>メモ</p>
					-->
					<input type="hidden" class="" id="remarks3_<?php echo $i;?>" name="remarks3_<?php echo $i;?>" value="" />
				</div>
			</div>

			<?php
				//}//ifの終わり
			?>
			<?php
			
			/*
			$csv_data .= $invoice_data_arr[$i]["withholding_tax"]."</td><td>";
			if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
				$csv_data .= $invoice_data_arr[$i]['sum(total_price)']."</td><td>"; 
			} else { 
				$csv_data .= $invoice_data_arr[$i]["total_price"]."</td><td>";
			}
			*/
			?>
			<input type="hidden" name="product_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['product_name'];?>" />
			<input type="hidden" name="jounalized_history_code<?php echo $i;?>" value="<?php echo $max_jh_num;?>" />
					
			<?php
				$invoice_total += $invoice_data_arr[$i]['total_price'];
				if ($i == $count_data_num - 1) {
					echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
				}
			?>
			<hr style="width:300px;border-color:#FFF;">
			<?php
			}//forの終わり
			?>
			<input type="hidden" id="data_num" name="data_num" value="<?php echo $count_data_num;?>" />
				<input type="button" id="outcsv" onclick="submit()" value="データを登録する" />
			</form>

			<div id="register">
				<form action="" id="page_controller_before" method="get">
					<input type="hidden" name="page" value="<?php if($page == $page_num) {echo $page_num;} else {echo $_REQUEST['page'] + 1;}?>">
					<input type="button" onclick="submit();" value="次の仕訳">
				</form>
				<form action="" id="page_controller_next" method="get">
					<input type="hidden" name="page" value="<?php if($page == 1) {echo $_REQUEST['page'];} else {echo $_REQUEST['page'] - 1;}?>">
					<input type="button" onclick="submit();" value="前の仕訳">
				</form>
			</div>
			<span style="color:red;font-size:13px;"><?php if (isset($_SESSION['message'])) {echo $_SESSION['message'];unset($_SESSION['message']);}?></span>
		</div>

	<?php
	} else {
	echo "<p class='red'>仕訳データがありません</p>";
	echo "<input type='button' onclick='history.back();' value='前に戻る'>";
	}//ifの終わり
	?>
</article>
<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(10)"></div>

<?php require("footer.php");?>