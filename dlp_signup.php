<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');

//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();
$invoice_con = new invoice_data_control();

$mail_address = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$dl_password = htmlspecialchars($_REQUEST['dl_password'],ENT_QUOTES);

$user1_con = new user_control();
$user1_arr = $user1_con -> user_select($pdo,$mail_address);

//メールアドレス重複確認
if ($user1_arr["mail_address"] != NULL || $user1_arr["mail_address"] != "") {
	$_SESSION['duplicationError'] = "同じメールアドレスで登録されたアカウントがあります。";
	header("Location:./pdf_download.php");
	exit();
}


//ユーザー登録
//$user1_arr['nick_name']   = htmlspecialchars($_REQUEST['company_name'],ENT_QUOTES);
$user1_arr['mail_address'] = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
//$user1_arr['password']    = htmlspecialchars($_REQUEST['login_password'],ENT_QUOTES);
//$user1_arr['kana']    = htmlspecialchars($_REQUEST['kana'],ENT_QUOTES);

//$user1_arr['password'] = md5( $user1_arr['password'] );

if (!$user1_arr['user_id']){
	$user_num = $user1_con -> user_get_new_num($pdo);
	$user1_arr['user_id'] = $user_num;
}
$result = $user1_con -> user_insert($pdo,$user1_arr);
//var_dump($user1_arr);
$user2_arr = $user1_con -> user_select($pdo,$mail_address);


//企業データ登録▼
$company_id = $user1_arr['user_id'];
$sql = "
INSERT INTO `COMPANY_TABLE` ( 
	`company_id`, `company_name`, 
	`company_name_reading`, 
	`email`
) VALUES ( 
	".$user1_arr['user_id'].", '".$user1_arr['nick_name']."', 
	'".$user1_arr['kana']."', 
	'".$user1_arr['mail_address']."' 
)";

$company_con = new company_control();
$company_arr = $company_con -> company_sql($pdo,$company_id,$sql);

//企業データ登録▲

//Emailアドレスの存在を確認する。
$email = $user1_arr['mail_address'];
$login_con = new user_control();
$login_arr = $login_con -> user_select_email($pdo,$email);
$_SESSION['user_id'] = $login_arr['user_id'];

//▼請求書データのあて先企業の共通コード変更▼
$inv_flag = "up_invoice_download_password";
$dlp_word = " `destination_id` = ".$login_arr["user_id"]." WHERE `download_password` = '".$dl_password."' AND `destination_id` = 0 ";
$inv_data = $invoice_con -> invoice_data_sql_flag($pdo,$inv_flag,$login_arr["user_id"],$dlp_word);
//var_dump($inv_data);
//▲請求書データのあて先企業の共通コード変更▲


//▼請求書データのあて先企業の共通コードを送り主企業が登録した送付先コード上の共通コードに上書き▼
//ダウンロードパスワードが一致する請求書データを取り出し、相手の送付先コードを取得する。
$flag="download_invoice_data";
$words;//条件をここに入れる。
$invoice_data_with_downloadpassword = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$dl_password);
$self_send_code = $invoice_data_with_downloadpassword[0]['send_code'];//送付先コード
$flag = "";
$words = "";
//取得した送付先コードと一致する送付先企業の設定した送付先のclient_company_idに自社の共通コードを上書きする。
$flag = "update_addressee_table";
$words = " `client_company_id` = ".$company_id." WHERE `client_id` = '".$self_send_code."' AND `client_company_id` = 0 ";
$company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$flag = "";
$words = "";
//▲請求書データのあて先企業の共通コードを送り主企業が登録した送付先コード上の共通コードに上書き▲



if ($login_arr != NULL) {
	$login_arr = $login_con -> user_select_email($pdo,$email);
	//$passwordmaker = random(12);
	$passwordmaker = uniqid();
	$login_arr['password'] = md5($passwordmaker);
	$login_con -> user_update($pdo,$login_arr);
	$login_arr = $login_con -> user_select_email($pdo,$email);
	//var_dump($login_arr);

	mb_language("ja");
	mb_internal_encoding("UTF-8");
	$newpassword = $passwordmaker;
//	$subject ="仮パスワード発行メール";
//	$header = 'From: Cloud Invoice運営 <info@co.cloudinvoice.co.jp>' . "\r\n" .'Reply-To: Cloud Invoice運営<info@co.cloudinvoice.co.jp>' . "\r\n";

	$header = "From:".mb_encode_mimeheader("Cloud Invoice 運営",'ISO-2022-JP-MS')."<info@co.cloudinvoice.co.jp>";
	$subject = "ご登録ありがとうございます。－ Cloud Invoice 運営";
	$message ="

ご登録ありがとうございます。
仮パスワードを発行いたしました。
仮パスワードは、下記の通りです。

御社の共通コード：".$login_arr['user_id']."
ID(E-mailアドレス):".$login_arr['mail_address']."
パスワード:".$newpassword."
サイトURL:http://co.cloudinvoice.co.jp/

ご登録のE-mailアドレスと仮パスワードでログインしてから、
パスワードの変更及び企業情報のご登録をお願いいたします。

－－－－変更方法
パスワードの変更は、設定マーク→【ログイン設定】→パスワード：変更
企業情報の登録は、【設定】→【事業所登録】
－－－－

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数
ではございますが、運営(info@co.cloudinvoice.co.jp)まで
メールをいただけますでしょうか。

よろしくお願いいたします。

Cloud Invoice運営
";

	mail($email,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$header);
	mail_to_master($email);
	//mail("sign_up_user@co.cloudinvoice.co.jp",mb_encode_mimeheader('【通知】ユーザー登録', 'ISO-2022-JP-MS'),mb_convert_encoding('ユーザーが登録されました。'.$email.'', 'ISO-2022-JP-MS'),$header);
}



function mail_to_master($mail) {
    $message ="新しく登録されたユーザーの情報は次の通りです。\n";
    $message.="────────────────────────────────────\n";
    $message.= $mail."\n\n";
    $message.="DATE: ".date("Y/m/d - H:i:s")."\n";
    $message.="IP: ".$_SERVER['REMOTE_ADDR']."\n";
    $message.="HOST: ".@gethostbyaddr($_SERVER['REMOTE_ADDR'])."\n";
    $message.="USER AGENT: ".$_SERVER['HTTP_USER_AGENT']."\n";
    $message.="────────────────────────────────────\n";
   $headers = "From: CloudInvoice <info@co.cloudinvoice.co.jp>\r\n";
   $subject = "ユーザー登録通知 DL";
   mb_internal_encoding("UTF-8");
    mail("sign_up_user@co.cloudinvoice.co.jp",mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),mb_convert_encoding($headers,'ISO-2022-JP-MS'));
}




function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

//echo "result=".$result."\n";

//****メールでログインを促す。****//
/*E-mailアドレスのみで登録し、仮パスワードを発行してログインする仕組みに変更のためコメントアウト

//メールの設定
$to = $mail_address;
mb_language("ja");
mb_internal_encoding("UTF-8");
//$headers = 'From: Cloud Invoice運営 <info@co.cloudinvoice.co.jp>' . "\r\n" .'Reply-To: Cloud Invoice運営<info@co.cloudinvoice.co.jp>' . "\r\n";
$headers = "From: ".mb_encode_mimeheader("Cloud Invoice 運営 " ,'ISO-2022-JP-MS')."<info@co.cloudinvoice.co.jp>\r\n";
$subject = "ご登録ありがとうございます。－ Cloud Invoice 運営" ;
$message ="

このたびはCloud Invoiceにご登録ありがとうございます。

会員登録が完了いたしました。
下記URLからログインしてください。

ID:".$to."
パスワード:登録時に設定したパスワードです。

http://co.cloudinvoice.co.jp/invoice/signin

Cloud Invoice運営
";
	mail($to,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$headers,"Content-Type: text/html; charset=\"ISO-2022-JP\";\n");
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仮パスワードを発行しました。</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">ユーザー仮登録完了</h1>
		<h2>ユーザー仮登録が完了しました。</h2><br/>
		<p>ご登録のメールアドレスに送付いたしました仮パスワードで<br/>
		ログインして認証を完了してください。
		</p>
		<button onclick="window.location.href='//co.cloudinvoice.co.jp/'">クラウドインボイストップに戻る</button>
	</div>
</body>
</html>