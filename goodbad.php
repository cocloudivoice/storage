<?php
session_start();
//var_dump($_POST);
	require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
	$db_con = new dbpath();
	$pdo = $db_con->db_connect();
    require_once(dirname(__FILE__).'/../db_con/goodbad.class.php');
    require_once(dirname(__FILE__).'/../db_con/reviewcontrol.class.php');
	$good_con  = new goodbad_control();
	$result;
	$user_id;
	$site_id;
	$record_id;
	$review_arr = array();
	$return_url;
	
	//var_dump($_SESSION['user_id']);
	if (isset($_REQUEST['user_id'])) {
    	$user_id = (int)$_REQUEST['user_id'];
	} else if (isset($_SESSION['user_id'])){
    	$user_id = (int)$_SESSION['user_id'];
	} else {
		$user_id = NULL;
	}
	if ($_REQUEST['table_name'] != "USER_TABLE") {
		if (isset($_SESSION['site_id'])){
	    	$site_id = (int)$_SESSION['site_id'];
		} else {
			$site_id = NULL;
		}
	} else {
		$site_id = NULL;
	}
	
	if (isset($_REQUEST['ret_site_id'])) {
		$ret_site_id = $_REQUEST['ret_site_id'];
		$site_id = $_REQUEST['ret_site_id'];
	}
	if (isset($_REQUEST['ret_user_id'])) {
		$user_id = $_REQUEST['ret_user_id'];
	}
	if ($_REQUEST['table_name'] == "DATA_TABLE") {
		if (isset($_SESSION['record_id'])){
	    	$record_id = (int)$_SESSION['record_id'];
		} else {
			$record_id = NULL;
		}
	} else if (isset($_REQUEST['review_record_id'])) {
		$record_id = $_REQUEST['review_record_id'];
	} else {
		$record_id = NULL;
	}
	
	if (isset($_REQUEST['ret_url'])) {
		$return_utl = $_REQUEST['ret_url'];
	}

	
	$table_name = $_REQUEST['table_name'];
	$judge = $_REQUEST['judge'];
	//var_dump($table_name);
	//var_dump($judge);
	if ($table_name == "USER_TABLE") {
		//echo "user<br/>";
		$return_user_id = "voted_".(string)$user_id;
		//echo $return_user_id;
		//var_dump($_SESSION);
		if ($_SESSION[$return_user_id] == 1) {
			//echo "test break";
		} else {
			//echo "test pass";
			$_SESSION[$return_user_id] = 1;
			$result = $good_con->goodbad_add($pdo,$table_name,$judge,$user_id,$ret_site_id,$record_id);
		}
	} else if($table_name == "SITE_TABLE") {
		//echo"site<br/>";
		$return_site_id = "voted_".(string)$site_id;
		//echo $return_site_id;
		//var_dump($_SESSION);
		if ($_SESSION[$return_site_id] == 1) {
			//echo "test break";
		} else {
			//echo "test pass";
			$_SESSION[$return_site_id] = 1;
			$result = $good_con->goodbad_add($pdo,$table_name,$judge,$user_id,$ret_site_id,$record_id);
		}
	} else if($table_name == "REVIEW_TABLE") {
		//echo "REVIEW<br/>";
		//echo"site<br/>";
		//$return_site_id = "voted_".(string)$site_id;
		//echo $return_site_id;
		//var_dump($_SESSION);
			//echo "test pass";
			if (isset($_REQUEST['review_body'])){
				$review_body = $_REQUEST['review_body'];
				$str_length = strlen($review_body);
				if ($str_length > 30) {
					$review_ellipsis = mb_substr($review_body,0,24).'...';
				} else {
					$review_ellipsis = $review_body;
				}
			}
			$review_con = new review_control();
			if ($judge == "good") {
				//echo "gooddayo<br/>";
				$good = 1;
				$bad = 0;
				$normal = 0;
			} else if ($judge == "bad") {
				//echo "baddayo<br/>";
				$good= 0;
				$bad = 1;
				$normal = 0;
			} else {
				$good = 0;
				$bad = 0;
				$normal = 1;
			}
			$update_date = strftime("%Y/%m/%d %H:%M:%S", time());
			echo $sql = "
			INSERT INTO `REVIEW_TABLE`(
			user_id,
			site_id,
			record_id,
			normal,
			review_body,
			review_ellipsis,
			good,
			bad
			) VALUES (
			".$user_id.",
			".$site_id.",
			".$record_id.",
			".$normal.",
			\"".$review_body."\",
			\"".$review_ellipsis."\",
			".$good.",
			".$bad."
			)";
			
			$review_con -> review_sql($pdo,$user_id,$site_id,$record_id,$sql);
			//var_dump($test);
	}
	if ($_REQUEST['ret_num'] == 1) {
		header("Location:http://hikaku.jp/invoice/mypage.php");
		exit();
	} else if($_REQUEST['ret_num'] == 2) {
		header("Location:".$return_utl."");
		exit();
	}else{
		echo "no_ret_num";
	}
?>