<?php //session_start(); 

//必要なクラスの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();
$keyword_con = new search_keywords_control();

/*
for ($i = 0 ;$i < count($company_arr);$i++) {
}
*/
echo "image <br/>";
var_dump($_GET["image"]);
echo "image1 <br/>";
var_dump($_GET["image1"]);
echo "FILES <br/>";
var_dump($_FILES);

?>
<!DOCTYPE html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $meta_keywords;?>" />
		<meta name="description" content="<?php echo $meta_description;?>" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="顧客・営業・業務管理システム「<?php echo $site_title;?>」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//co.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//co.cloudinvoice.co.jp/images/logo.gif" />
		<meta property="og:site_name" content="<?php echo $site_title;?> - <?php echo $site_title;?>" />
		<meta property="og:description" content="<?php echo $meta_description;?>" />
		<meta property="og:locale" content="ja_JP" />
		<meta name="viewport" content="width=device-width, maximum-scale=1,user-scalable=0" />
		<meta charset="UTF-8"/>
		<meta name="description" content="" />
		<meta name="keywords" content=""/>
		<script src="//co.cloudinvoice.co.jp/js/jquery-2.1.1.min.js" type="text/javascript" charset="UTF-8"></script>
		<script type="text/javascript" src="//typesquare.com/accessor/script/typesquare.js?PjtS4ugy5r4%3D" charset="utf-8"></script>
<script type="text/javascript">

var fileArea = document.getElementById('drag-drop-area');
var fileInput = document.getElementById('fileInput');

fileArea.addEventListener('dragover', function(evt){
  evt.preventDefault();
  fileArea.classList.add('dragover');
});

fileArea.addEventListener('dragleave', function(evt){
    evt.preventDefault();
    fileArea.classList.remove('dragover');
});
fileArea.addEventListener('drop', function(evt){
    evt.preventDefault();
    fileArea.classList.remove('dragenter');
    var files = evt.dataTransfer.files;
    fileInput.files = files;
});


</script>
<style type="text/css">
#drag-drop-area{ width:500px;height:200px;background-color:white;padding: 50px;}
#fileInput{ width:100%;height:100%;background-color:white;}
#fileInput { border: 5px dashed #000000;/*padding:50px 50px 0px 50px;*/  margin-left: 20px;  font-size: 24px;  color: #000000;  background-color: #ffffff;  text-align: center;}
#SelectedFiles { width:780px;height:auto;border:1px solid;margin-left:20px;padding:0px 10px 0px 10px;}
#ulList { width:100%;height:auto;}
.drag-drop-info { margin-left:20px;}
#submitBtn { width:100px;height:30px;margin:0px 0px 20px 20px;}
</style>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
	<div id="drag-drop-area">
		<div class="drag-drop-inside">
			<p class="drag-drop-buttons">
			<a class="drag-drop-info">点線---内にファイルをドロップ</a>
			<a>またはボタンでファイルを選択</a>
			<input id="fileInput" type="file" value="ファイルを選択" name="image[]" multiple="multiple"></p>
			<input id="submitBtn" type="submit"  value="アップロード">
		</div>
		<div id="SelectedFiles">
			<p class="mleft20">選択されているファイル</p>
			<ul id="ulList"></ul>
		</div>
	</div>
</form>
</body>
<script type="text/javascript">

    $('#fileInput').change(function (data){
	    $("#ulList").empty();
	    var fp = $("#fileInput");
	    var lg = fp[0]
	    .files.length;
	    var items = fp[0].files;
	    console.log(items);
	    var fragment = "";
	    var fragment2 = "";
	    var fileSizeSum = 0;
	    if (lg > 0) {
	        for (var i = 0; i < lg; i++) {
	            var fileName = items[i].name; // ファイル名を取得
	            var fileSize = convertToFileSize(items[i].size); // ファイルサイズを取得
	            var fileType = items[i].type; // ファイルのコンテンツタイプを取得
	            fileSizeSum += items[i].size; // ファイルサイズを合計
	           // console.log(convertToFileSize(fileSizeSum));
	          // 表示
	         fragment += "<li>ファイル名：" + fileName + " (" + fileSize + "" + ")</li>";
	        }
	       fragment2 = "<p>合計ファイル容量：" + convertToFileSize(fileSizeSum) + "" + "</p>";
	       $("#ulList").append(fragment);
	       $("#SelectedFiles").append(fragment2);
	    }
	});

//単位配列
var units = [" B", " KB", " MB", " GB", " TB"];

//変換処理
function convertToFileSize(size) { 
    for (var i = 0; size > 1024; i++) { 
        size /= 1024;
    }
    if (units[i] == " MB"|| units[i] == " GB"|| units[i] == " TB") {
    	return Math.round(size * 100) / 100 + units[i]; 
    } else {
    	//整数で返すなら以下でOK 
    	return Math.round(size) + units[i];
    }
}
	
</script>
</html>