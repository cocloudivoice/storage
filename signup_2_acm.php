<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/accountmanagementcontrol.class.php");
require_once(dirname(__FILE__)."/../db_con/accountfirmsclientcontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();
$acm_con = new account_management_control();
$af_client_con = new account_firms_client_control();
$company_name_con = new company_control();

$mail_address = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$acm_check_arr = $acm_con -> acm_select($pdo,$mail_address);

$acm_num = 0;
$acm_arr = array();

//メールアドレス入力確認
if ($mail_address == NULL || $mail_address == "") {
	$_SESSION['duplicationError'] = "メールアドレスが確認できません。もう一度お試しください。";
	header("Location:./signup_af.php");
	exit();
}

//メールアドレス重複確認
if ($acm_check_arr["e_mail"] != NULL || $acm_check_arr["e_mail"] != "") {
	$_SESSION['duplicationError'] = "同じメールアドレスで登録されたアカウントがあります。";
	header("Location:./signup_af.php");
	exit();
}

//ユーザー登録
$acm_arr['e_mail'] = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$acm_arr['name'] = htmlspecialchars($_REQUEST['name'],ENT_QUOTES);
$acm_com_id = htmlspecialchars($_REQUEST['company_id'],ENT_QUOTES);
/*
$acm_arr['zip_code'] = htmlspecialchars($_REQUEST['zip_code'],ENT_QUOTES);
$acm_arr['address1'] = htmlspecialchars($_REQUEST['address1'],ENT_QUOTES);
$acm_arr['address2'] = htmlspecialchars($_REQUEST['address2'],ENT_QUOTES);
*/
$acm_arr['regist_date'] = strftime("%Y/%m/%d %H:%M:%S", time());
$acm_num = $acm_con -> acm_get_new_num($pdo);
$acm_arr['id'] = $acm_num;

//パスワード作成
//$passwordmaker = random(12);
$passwordmaker = uniqid().random(13);
$acm_arr['password'] = md5($passwordmaker);

//共通コードから企業情報を取得
$flag = "company_data";
$acm_company_arr = $company_name_con -> company_sql_flag($pdo,$flag,$acm_com_id,$words,$conditions = NULL);
var_dump($acm_company_arr);

/*
$sql = "";
$result = $acm_con -> acm_sql($pdo,$sql,$e_mail);
var_dump($result);
*/
//▼ユーザーデータ登録▼
$acm_id = $acm_arr['id'];
$acm_con -> acm_insert($pdo,$acm_arr);

//▼所属企業データ登録▼
//設定内容の表示データ取得
$flag = "af_client_data_with_conditions";
$words = "AND `af_id` = '".$acm_id."' AND `client_id` = ".$acm_com_id." " ;
$search_duplication = $af_client_con -> af_client_sql_flag($pdo,$flag,$acm_id,$words);
$registered_id = $search_duplication[0]['id'];
$duplication_flag = count($search_duplication);
try {
echo	$sql = "INSERT INTO `ACCOUNTING_FIRMS_CLIENT_TABLE`(
	`client_id`, `customer_code`, `company_name`, `company_name_reading`, `addressee`, `department`, `position`, 
	`zip_code`, `prefecture`, `address1`, `address2`, `tel_num`, `fax_num`,
	`email`, `payer`, `bank_number`, `branch_number`, `bank_account_number`, `client_company_id`, `af_id`
	) VALUES ( 
	'".$acm_com_id."','".$acm_com_id."','".$acm_company_arr[0]['company_name']."','".$acm_company_arr[0]['company_name_reading']."','".$acm_company_arr[0]['addressee']."','".$acm_company_arr[0]['department']."','".$acm_company_arr[0]['position']."', 
	'".$acm_company_arr[0]['zip_code']."','".$acm_company_arr[0]['prefecture']."','".$acm_company_arr[0]['address1']."','".$acm_company_arr[0]['address2']."','".$acm_company_arr[0]['tel_num']."', '".$acm_company_arr[0]['fax_num']."', 
	'".$acm_company_arr[0]['email']."','".$acm_company_arr[0]['payer']."','".$acm_company_arr[0]['bank_number']."','".$acm_company_arr[0]['branch_number']."','".$acm_company_arr[0]['bank_account_number']."','".$acm_company_arr[0]['client_company_id']."',".$acm_id.")";

	$af_client_con -> af_client_sql($pdo,$af_id,$sql);

} catch (Exception $e) {
	//$e -> message();
}
//設定変更の処理▲
//▲所属企業データ登録▲

//Emailアドレスの存在を確認する。
$e_mail = $acm_arr['e_mail'];
$acm_arr = $acm_con -> acm_select_email($pdo,$e_mail);
$_SESSION['acm_id'] = $acm_arr['id'];
if (isset($acm_arr['id'])) {
	mb_language("ja");
	mb_internal_encoding("UTF-8");
	$newpassword = $passwordmaker;

	$header = "From:".mb_encode_mimeheader("Cloud Invoice 運営 " ,"ISO-2022-JP-MS")."<info@storage.cloudinvoice.co.jp>";
	$subject = "仮パスワード発行通知" ;
	$message ="

ご登録ありがとうございます。
仮パスワードを発行いたしました。
仮パスワードは、下記の通りです。

".$acm_arr['name']."様の従業員コード：".$acm_arr['id']."
ID(E-mailアドレス):".$acm_arr['e_mail']."
パスワード:".$newpassword."
サイトURL:http://storage.cloudinvoice.co.jp/invoice/signin_acm

ご登録のE-mailアドレスと仮パスワードでログインしてから、
パスワードの変更及び企業情報のご登録をお願いいたします。

－－－－変更方法
パスワードの変更は、設定マーク→【ログイン設定】→パスワード：変更
事業所情報の登録は、【設定】→【事業所登録】
－－－－

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数
ではございますが、運営(info@storage.cloudinvoice.co.jp)まで
メールをいただけますでしょうか。

よろしくお願いいたします。

Cloud Invoice運営
";

	mail($e_mail,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$header);
	mail_to_master($e_mail);
	//mail("sign_up_user@storage.cloudinvoice.co.jp",mb_encode_mimeheader('【通知】ユーザー登録', 'ISO-2022-JP-MS'),mb_convert_encoding('ユーザーが登録されました。'.$e_mail.'', 'ISO-2022-JP-MS'),$header);

} else {
	
	//メールアドレス入力確認
	$_SESSION['duplicationError'] = "メールアドレスが確認できません。もう一度お試しください。";
	header("Location:./signup_af.php");
	exit();
}

function mail_to_master($mail) {
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    $message ="新しく登録されたユーザーの情報は次の通りです。\n";
    $message.="────────────────────────────────────\n";
    $message.= $mail."\n\n";
    $message.="DATE: ".date("Y/m/d - H:i:s")."\n";
    $message.="IP: ".$_SERVER['REMOTE_ADDR']."\n";
    $message.="HOST: ".@gethostbyaddr($_SERVER['REMOTE_ADDR'])."\n";
    $message.="USER AGENT: ".$_SERVER['HTTP_USER_AGENT']."\n";
    $message.="────────────────────────────────────\n";
   $headers = mb_encode_mimeheader("From: CloudInvoice",'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>";
   $subject = "従業員登録通知 N";
   
   mail("sign_up_user@storage.cloudinvoice.co.jp",mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),$headers);
}

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

//echo "result=".$result."\n";

//****メールでログインを促す。****//
/*E-mailアドレスのみで登録し、仮パスワードを発行してログインする仕組みに変更のためコメントアウト

//メールの設定
$to = $mail_address;
mb_language("ja");
mb_internal_encoding("UTF-8");
//$headers = 'From: Cloud Invoice運営 <info@storage.cloudinvoice.co.jp>' . "\r\n" .'Reply-To: Cloud Invoice運営<info@storage.cloudinvoice.co.jp>' . "\r\n";
$headers = "From: ".mb_encode_mimeheader("Cloud Invoice 運営 " ,'ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>\r\n";
$subject = "ご登録ありがとうございます。－ Cloud Invoice 運営" ;
$message ="

このたびはCloud Invoiceにご登録ありがとうございます。

会員登録が完了いたしました。
下記URLからログインしてください。

ID:".$to."
パスワード:登録時に設定したパスワードです。

http://storage.cloudinvoice.co.jp/invoice/signin_acm

Cloud Invoice運営
";
	mail($to,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'ISO-2022-JP-MS'),$headers,"Content-Type: text/html; charset=\"ISO-2022-JP\";\n");
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ユーザー登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仮パスワードを発行しました。</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">ユーザー仮登録完了</h1>
		<h2>ユーザー仮登録が完了しました。</h2><br/>
		<p>ご登録のメールアドレスに送付いたしました仮パスワードで<br/>
		ログインして認証を完了してください。
		</p>
		<button onclick="window.location.href='//storage.cloudinvoice.co.jp/invoice/signin_acm'">Cloud Invoiceのログイン画面へ</button>
	</div>
</body>
</html>