<?php

class TextConvert {
	
	
	function convertText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$upload_dir_path,$txt_res_dir,$txt_dir,$file_path,$af_id) {

		//txtの内容を取得して分析し、必要な情報（日付、金額、電話番号1,2,3、会社名、商品名、業種）
		//を取得して上書きする工程
//		for($m = 0;$m < count($img_file_arr);$m++) {
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name_arr = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$money_num_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();
		try {
	    	//var_dump($file);
	        //$filename = $img_file_arr[$m];
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$filename  = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$filename );
			$file_name_top = substr($filename , 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($filename , -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			//echo $txt_res_dir."/".$dlp.".txt";
			//echo file_exists($txt_res_dir."/".$dlp.".txt");
			//OCR済みのテキストファイルがあったらOCR処理しない
			if(file_exists($txt_res_dir."/".$dlp.".txt")) {
				//請求書が登録されていないか確認する。
				//$flag ="download_invoice_data_clm";
				//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
				//同じdownload_passwordで請求書が登録済みの場合
				if (substr($filename, -3, 3) == "txt") {
					//請求書登録
					//仮のデータで請求書登録をする。
					//仮の請求書・領収書データの登録
					//テキストを読んで上書き処理
					//echo "テキスト処理<br>";
					$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
					$text_file_path = $txt_dir."/".$dlp.".txt";

					//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
					if(file_exists($text_file_path_en)) {
						$data = file_get_contents($text_file_path_en);
						$data .= file_get_contents($text_file_path);
					} else {
						$data = file_get_contents($text_file_path);
					}
					
					$data_all = $data;
					//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

					//１データとして扱うために分けない。
					//$data = explode( "\n", $data );
					//echo "<br>";
					$cnt = count( $data );
					//echo "<br>";
					//var_dump($data);
					$data_arr = array();
					

					for ( $i = 0; $i < $cnt; $i++ ) {//echo e;
					
						//変数初期化
						$money_num_cnt = 0;
						$telkey_flag = 0;
						
						/*
						$outcode = "SHIFT-JIS";
						$incode = "UTF-8";
						$nl = "\r\n";
						$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
						*/
						
						//全角文字を半角に変換
						//$data = mb_convert_encoding( $data, "UTF-8", "UTF-16" );
						$data = mb_convert_encoding( $data, "UTF-8", "SHIFT-JIS");
						$data2 = $data;//商品名と会社名の文字列を取得する時は数字を加工しない方が良いため分ける。
						$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
						$data = strtr($data, $kana);
						$data = mb_convert_kana($data, 'aKvrn');
						$data = str_replace(array("'","'"," ","内","|",",","，","・","．","　1　","　ｉ　","1口　","1回","1ｕ","　；","；","：","＝","！","」","厂","ミ","ｌ","】","ｉ","/",":",";","|"),"",$data);
						
						$data = str_replace(array("\n","\r"),",",$data);
						$patterns = array ('/,(201)/ism');
						$replace = array ('\1');
						$data = preg_replace($patterns, $replace, $data);
						$data = str_replace(array(",201"),"\n201",$data);
						$data = str_replace(array(",\n\n"),",\n",$data);
						$data = str_replace(array("　"),",",$data);
						$data = str_replace(array("　　	"),",",$data);
						$data = str_replace(array("　　　"),",",$data);
						$data = str_replace(array("　　"),"　",$data);
						$data = str_replace(array("、","	"),",",$data);
						$data = str_replace(array(",,,"),",",$data);
						$data = str_replace(array(",,"),",",$data);
						$data = str_replace(array(",,,"),",",$data);
						$data = str_replace(array(",,"),",",$data);
						
						$data = str_replace(array(",2015","！015"),"2015",$data);
						$data = str_replace(array("叩","m","0c","0C","0(1"),"00",$data);
						$data = str_replace(array("ｊｌ","Ｉ","ｉｌ","ｊ","ｈ"),"/",$data);
						$data = str_replace(array("判"),"\4",$data);
						//$data = str_replace(array("//","{{","[1"),"11",$data);
						$data = str_replace(array("。","。","〝","位","n","ロ","o","U","()","(1"),"0",$data);
						$data = str_replace(array("■","]","L"),"1",$data);
						$data = str_replace(array("z"),"2",$data);
						$data = str_replace(array("會"),"3",$data);
						$data = str_replace(array("縄","繍"),"4",$data);
						$data = str_replace(array("轟","印","§"),"5",$data);
						$data = str_replace(array("s","野"),"6",$data);
						$data = str_replace(array("a","讐","B"),"8",$data);
						$data = str_replace(array("g","q"),"9",$data);
						$data = str_replace(array("事","令","隼","藻","ff"),"年",$data);
						$data = str_replace(array("隷","A"),"月",$data);
						$data = str_replace(array("B","t1","曰"),"日",$data);
						$data = str_replace(array("年明","#明"),"年8月",$data);
						$data = str_replace(array("年9A","#9A"),"年9月",$data);
						$data = str_replace(array("年坍"),"年1月",$data);
						$data = str_replace(array("明旧"),"8月1日",$data);
						$data = str_replace(array("¨","－","一","・","~"),"-",$data);
						//$data = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data);
						$data = str_replace(array("普遍車","普遷車","普通車"),"普通車",$data);
						$data = str_replace(array(",普通車","普通車")," 普通車",$data);
						$data = str_replace(array("輕自動"),"軽自動",$data);
						$data = str_replace(array(",軽自動","軽自動")," 軽自動",$data);
						$data = str_replace(array("合","卦","會"),"合",$data);
						$data = str_replace(array("ＥＮＥｏｓ"),"ＥＮＥＯＳ",$data);
						$data = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data);
						$data = str_replace(array("オ寸","本寸"),"村",$data);
						$data = str_replace(array("言舌"),"話",$data);
						$data = str_replace("」R","ＪＲ",$data);
						$data = str_replace(array("2015","2051","20/5","2015?","2015#","2015：","201ｓ","20！51"),"2015",$data);
						$data = str_replace(array("2016","2061","20/6","2016?","2016#","2016：","Zo/6"),"2016",$data);
						$data = str_replace(array("2017","2071","20/7","2017?","2017#","2017："),"2017",$data);
						//$data = str_replace(array("2015/121",),"2015/12/",$data);
						//$data = str_replace(array("2016/121",),"2016/12/",$data);
						//$data = str_replace(array("2017/121"),"2017/12/",$data);
						$patterns = array ('/(20\d{2})[,.:;]*(\d{1,2})[1,.:;]*(\d{1,2})/');
						$replace = array ('\1/\2/\3,');
						$data = preg_replace($patterns, $replace, $data);
						/*
						$patterns = array ('/(\w{1})[,]{1}(\w{1})/');
						$replace = array ('\1\2/');
						$data = preg_replace($patterns, $replace, $data);
						*/
						$data = str_replace(array(",-")," ‐ ",$data);
						$data = str_replace(array(",,","ｌ,ｌ","ｌ,ｌ,ｌ"),",",$data);
						$data = str_replace(array("ＫＤＤ1"),"ＫＤＤＩ",$data);
						$data = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data);
						$data = str_replace(array("食年代"),"食事代",$data);
						$data = str_replace(array("商年"),"商事",$data);
						$data = str_replace(array("ＥＴＣ","ＥＴｃ","耳Ｔｃ"),"ＥＴＣ ",$data);
						$data = str_replace(array("ＥＴＣ ,"),"ＥＴＣ ",$data);
						$data = str_replace(array("¥","$","#","="),"￥",$data);
						$data = str_replace(array(":-"),".",$data);
						// ファイルのパスを変数に格納
						$FileText = $txt_dir."/".$dlp."ci.txt";
						// ファイルに書き込む
						file_put_contents($FileText,$data,LOCK_EX);
						
						//echo "<br>";
						//echo $data;
						//echo "<br>";
						//-を用いた日付で時間がスペースの後に時間がついていた場合取得できなくなるため、スペースを消す前に移動した。
						
						$pattern_comp_end = "/^[*]{1}/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							$data = str_replace(array("*"),"￥",$data);
						}
						
						//初期加工のみで利用したい場合の変数
						$ori_data = $data;

						
						$pattern_comp_end = "/([0-9]{2,4})[.,- ]{1,2}([0-9]{1,2})[.,- ]{1,2}([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補a<br/>\r\n";
							//var_dump($match);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;
								$got_date = $data_arr = str_replace("/","",$match[0]);
								//echo $data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$company_data_end_line = $i;
							}
						}

						$data = str_replace(array(".","．"),"",$data);
						$pattern_comp_end = "/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補b<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;

								$got_date = $data_arr = str_replace("/","",$match[0]);
								//echo $data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$company_data_end_line = $i;
							}
						}
						
						
						$pattern_comp_end = "/([0-9]{2,4}),([0-9]{1,2}),([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補c<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;

								$got_date = $data_arr = str_replace("/","",$match[0]);
								//echo $data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$company_data_end_line = $i;
							}
						}

						//電話番号用処理
						
						//$tel_test = preg_replace('/[^0-9]/', '', $data);
						//数字だけ抜き出す処理だと合計金額などと区別しづらいかも。
						$tel_test = str_replace(array("_"),"",$data);
						if (intval($tel_test) != 0 ) {
							$data = str_replace(array("(","（","_"),"-",$data);
						}
						$data = str_replace(array(" ","　"),"",$data);
						$data = str_replace(array("(","（"),"-",$data);
						$data = str_replace(array(")","）"),"-",$data);
						
						//電話番号取得（番号を取得）
						//echo $data;echo "aaatel<br>\r\n";
						$pattern_comp_end = "/0{1}[0-9]{1,3}[- ]+[0-9]{2,4}[- ]+[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								if (substr($match[0],0,1) == 1) {
									$match[0] = substr($match[0],1);
								}
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr = $data;
							//$start_line = $i + 1;
						}

						//電話番号取得（番号を取得）
						//echo $data;echo "bbbtel<br>\r\n";
						$pattern_comp_end = "/0{1}[0-9]{9}/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								if (substr($match[0],0,1) == 1) {
									$match[0] = substr($match[0],1);
								}
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr = $data;
							//$start_line = $i + 1;
						}
						
						//電話番号取得（番号を取得）
						//echo $data;echo "\r\n";
						$pattern_comp_end = "/0{1}[0-9]{1,3}\([0-9]{2,4}\)[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{10}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								//echo $match[0];
								if (substr($match[0],0,1) == 1) {
									$match[0] = substr($match[0],1);
								}
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr = $data;
							//$start_line = $i + 1;
						}
						
						if ($tel_cnt == 0) {
							//電話番号取得（番号を取得）
							//echo $data;echo "\r\n";
							$pattern_comp_end = "/[1-9]{1}[0-9]{1,3}-[0-9]{3,4}/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							if ($match_num >= 1) {
								//echo "<br/>";
								$match_result= str_replace("-","",$match[0]);
								$tel_flag = preg_match("/\d{6,8}/", $match_result, $result);
								if($tel_flag == 1){
									//echo "電話番号候補<br/>\r\n";
									$tel_num = $tel_arr[$tel_cnt] = $match[0];
									//echo "<br/>\r\n";
								}
								$tel_cnt++;
								$data_arr = $data;
								//$start_line = $i + 1;
							}
						}
						
						//echo $data;echo "mncnt:".$money_num_cnt;
						if ($money_num_cnt == 0) {
							//echo $data;
							//$pattern_comp_end = "/合計(\\\w*)/";
							$pattern_comp_end = "/合計(￥\d+)/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							if ($match_num >= 1) {
								//echo "合計金額候補a<br/>\r\n";
								$goukei_top = $goukei = $data_arr = str_replace(array("\\","￥","円","計","合",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
							//	$goukei = substr($goukei,2);
								$data_arr = $data;
								$money_cnt++;
								$money_num_cnt++;
							}
						}
						
						if ($money_num_cnt == 0) {
							$pattern_comp_end = "/計￥(\d+)/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							if ($match_num >= 1) {
								//echo "金額候補<br/>\r\n";
								$money_arr[$money_cnt] = $data_arr = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
								if (intval($money_arr[$money_cnt]) > 0) {
									$sum_flag += 1;
									$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
								}
							}
						}
						
						
						if ($money_num_cnt == 0) {
							$pattern_comp_end = "/請求金額(\d+)/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							if ($match_num >= 1) {
								//echo "金額候補<br/>\r\n";
								$goukei_top = $money_arr[$money_cnt] = $data_arr = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
								//echo "<br>金額候補";
								//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
								if (intval($money_arr[$money_cnt]) > 0) {
									$sum_flag += 1;
									$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
								}
							}
						}

					
						if ($money_num_cnt == 0) {
							$data = str_replace(array("Y","F","軍"),"\¥",$data);
							$data = str_replace(array("D","O","〕"),"0",$data);
							$data = str_replace(array("∞"),"00",$data);
							$data = str_replace(array("ヮ","i","ぅ","ゥ","′","り",",","#"),"",$data);
							$data = str_replace(array("\\"),"￥",$data);
							$pattern_comp_end = "/(￥\d+)/";
							//$pattern_comp_end = "/(\d+)/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							//echo"<br>金額:";echo $data;echo "<br><br>\r\n";
							if ($match_num >= 1) {
								//echo "金額候補\<br/>\r\n";
								$money_arr[$money_cnt] = $data_arr = str_replace(array("\\","￥",",","，","。",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
								//echo "<br>金額候補";var_dump($money_arr[$money_cnt]);
								//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
							}
						}
						
						if ($money_num_cnt == 0) {
							//$pattern_comp_end = "/\w*円/";
							$pattern_comp_end = "/[金]*(\d+)円/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							if ($match_num >= 1) {
								//echo "金額候補<br/>\r\n";
							//echo "<br>金額候補 円";echo $data;var_dump($match);
								$money_arr[$money_cnt] = $data_arr = str_replace(array("\\","￥","円",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
								//echo "\r\n";
								$data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
							}
						}
						
						if ($money_num_cnt == 0) {
							$pattern_comp_end = "/(\d{3,7})/";
							$match_num = preg_match($pattern_comp_end, $data, $match);
							
							if ($data[($i - 2)] == "合計"|| $data[($i - 1)] == "合計") {
							
								//echo "金額候補<br/>\r\n";
								$goukei_top = $money_arr[$money_cnt] = $data_arr = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
								//echo "<br>金額候補";
								//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
								if (intval($money_arr[$money_cnt]) > 0) {
									$sum_flag += 1;
									$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
								}
							}
						}
							//echo $ori_data;echo $money_num_cnt;
							//echo "<br>";
						if ($money_num_cnt == 0) {

							//金額が入っていない時の候補
							$pattern_comp_end = "/([1-9]{1}[0-9]{0,2},[0-9]{3})/";
							$match_num = preg_match($pattern_comp_end, $ori_data, $match);
							if ($match_num >= 1) {
								//echo "金額候補<br/>\r\n";
								$money_arr[$money_cnt] = $ori_data = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
								//echo "\r\n";
								//echo "<br>金額候補";
								//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
								$data_arr = $ori_data;
								//$start_line = $i + 1;
								//echo "<br/>\r\n";
								$money_cnt++;
								$money_num_cnt++;
								$company_data_end_line = $i;
								if (intval($money_arr[$money_cnt]) > 0) {
									$sum_flag += 1;
									$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
								}
							}
						}

						
						$data = str_replace(array("-","_","－","＿","・","･",".","．","`",":"),"",$data);
						$data = str_replace(array("午","撃","#"),"年",$data);
						$data = str_replace("l","1",$data);
						//echo "<br/>";
						$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})日/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr = $data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$company_data_end_line = $i;
						}
						
						$pattern_comp_end = "/([0-9]{1,4})[年A#]*([0-9]{1,2})月([0-9]{1,2})/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							$data = str_replace(array("-","_","－",),"",$data);
							//echo "日付候補d<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr = $data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}

						$data = str_replace(array("′","ノ",",-",":-"),"/",$data);//日付用変換
						$pattern_comp_end = "/([0-9]+)\/([0-9]{1,2})\/([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;

							//echo $got_date = $data_arr = str_replace("/","",$match[0]);
							//echo $data_arr = $data;
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$company_data_end_line = $i;
						}

						$data = str_replace("/","1",$data);
						$data = str_replace("-","",$data);
						//echo "<br/>";
						$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})/";
						//$pattern_comp_end = "/.?*年.?*月.?*日/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							//preg_match("/[0-9]+年/", $match[0], $year);
							//preg_match("/[0-9]+月/", $match[0], $month);
							//preg_match("/[0-9]+日/", $match[0], $day);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr = str_replace(array("年","月","日"),"",$match[0]);
							//echo $data_arr = $data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$date_cnt++;
							$date_top_cnt++;
							$company_data_end_line = $i;
						}
						
						$pattern_comp_end = "/([0-9]{2,4}).([0-9]{1,2}).([0-9]{1,2})/";
						$match_num = preg_match($pattern_comp_end, $data, $match);
						if ($match_num >= 1) {
							//echo "日付候補<br/>\r\n";
							//var_dump($match);
							$match[1] = substr($match[1],-2);
							//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
							if ($match[1] >= 20){
								$match[1] = $match[1] - 12;
							}
							
							$year = "20".$match[1];
							$month = sprintf("%02d",substr($match[2],-2));
							$day = sprintf("%02d",substr($match[3],-2));
							if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
								$got_date = $date_arr[$date_cnt] = $year.$month.$day;
								//echo $got_date = $data_arr = str_replace("/","",$match[0]);
								//echo $data_arr = $data;
								//$start_line = $i + 1;
								//echo "<br>\r\n";
								$date_cnt++;
								$company_data_end_line = $i;
							}
						}
						
						if  ($pdt_cnt == 0) {
							//商品名を取得する
							//商品名の為に1をーに変換し直す。
							$data2 = str_replace(array("1"),"ー",$data2);
							//$product_check_arr = array("食事代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENE0Sヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","定期券","乗車券類","特急券","普通券","飲食代","レストラン","宿泊代","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","回数券","乗車券","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",",*航空便.*","簡易書留",".*切手.*","水道料金","サントリー","チケット代","ゴルフプレー.*代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車","収入印紙","印紙");
							$product_check_arr = array("食事代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENEOSヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","飲食代","レストラン","宿泊代","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",".*書留.*",",*航空便.*","簡易書留",".*切手.*","郵便","水道料金","サントリー","チケット代","ゴルフプレー.*代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車");
							$p_cnt = 0;
							$last_product = "";
							$match = "";
							$product_arr = array();
							for ($z = 0;$z < count($product_check_arr);$z++) {
								$pattern_comp_end = "/".$product_check_arr[$z]."/";
								$pattern_comp_end;//echo "<br>";
								$match_num = preg_match($pattern_comp_end, $data2, $match);
								//echo "<br>";
								if ($match_num >= 1) {
									$product_arr[$p_cnt] = $match[0];
									//echo "<br>商品候補<br/>\r\n";
									//var_dump($match);
									//echo $data2;
									//echo "<br/>";
									//exit;
									if ($product_arr[$p_cnt] != $last_product) {
										if ($p_cnt != 0) {$space = " ";}
										$last_product = $product_arr[$p_cnt];
										$product_name_arr[$p_cnt] = $space.$product_arr[$p_cnt];
										$p_cnt++;
										if ($match[0] != "収入印紙" ||$match[0] != "印紙" ) {$pdt_cnt++;}
										break;
									}
								}
							}
						}
						//echo $data2;
						if  ($comp_cnt == 0) {
							//会社名を取得する
							$company_check_arr = array("東海旅客","JR東日本","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","セキショー","宇佐美","小田急.*","東京地下鉄","東日本旅客","ケーズデンキ","ベスト電器","ビックカメラ","Express","エネオス","シェル石油","コスモ石油","Unidy","ユニディ","ケーヨーデイツー","オリンピック","loft",".*JTB.*","ロフト","サブウェイ","CAFE de CRIE","ABC-MART","駐車場","駐輪場","bookshelf","くすりの福太郎","リンガーハット","STARBUCKS","スターバックス","コメダ珈琲店","タリーズコーヒー","ドトールコーヒー","エクセルシオール","カフェ・ド・クリエ","ベローチェ","ジェーソン","イトーヨーカドー","ガスト","ロイヤルホスト","デニーズ","ココス","夢庵","サイゼリヤ","ジョイフル","ジョナサン","バーミヤン","新鮮市場","カインズ",".*株式会社.*",".*有限会社.*",".*（株）.*",".*（有）.*",".*公社",".*水道局","マクドナルド","モスバーガー","ファッションセンターしまむら","ジーユー","ユニクロ","UT","アーバンリサーチ","SHIPS","シップス","ヴィクトリアゴルフ","マルエイ","ダイソー","DAISO","(.*市)会計管理者","武蔵松山カントリークラブ","アップルパーク勝どき第1","タイムズ","ユアーズパーク","コインパーク","タイムパーク","パーク２４","八重洲駐車場","マンゴツリー","東海パッセン","未来堂","ステーキてっぺい",".*パーキング","鉄道","東京急行電鉄","阪急鉄道","南海電鉄","阪神電鉄","東急電鉄","東武電鉄","西武鉄道","日本交通","平和交通","西新宿NSG会",".*税理士",".*行政書士",".*司法書士",".*行政書士協同組合",".*税理士会",".*医師会",".*司法書士会","東京電力","東京ガス","法務局","NEXCO.*日本","NEXCO","NTTファイナンス","NTTファイナンス","パーキング","KDDI","ソフトバンク","softbank","SoftBank","国税局",".*税事務所","スターバックス","ファミリーマート","NewDays","ローソン","LAWSON","セブンイレブン","セブン-イレブン","セブン‐イレブン","サンクス","ココストア","ミニストップ","ポプラ","スリーエフ",".*ホテル",".*カントリークラブ",".*ゴルフ場",".*ゴルフ倶楽部",".警察","郵便局","関西ガス","関西電力","九州電力","東北電力",".*役場",".*市役所",".*区役所",".*金銭出納員",".*出納員",".*会計管理者",".*法律事務所",".*病院",".*医院",".*歯科医院",".*歯科",".*クリニック",".*外科",".*内科",".*皮膚科",".*耳鼻咽喉科",".*整骨院",".*接骨院","ヤマト運輸","クロネコヤマト","佐川急便","ペリカン便","西濃運輸","赤帽","売捌所","売さばき",".*公証人.*","ヤマトフィナンシャル");
							$p_cnt = 0;
							$last_company = "";
							$match = "";
							$company_arr = array();
							for ($z = 0;$z < count($company_check_arr);$z++) {
								$pattern_comp_end = "/".$company_check_arr[$z]."/";//echo "<br>";
								$match_num = preg_match($pattern_comp_end, $data2, $match);
								//echo "<br>";
								if ($match_num >= 1) {
									$company_arr[$p_cnt] = $match[0];
									//echo "<br>会社名候補<br/>\r\n";
									//var_dump($match);
									//echo $data2;
									//echo "<br/>";
									if ($company_arr[$p_cnt] != $last_company) {
										if ($p_cnt != 0) {$space = " ";}
										$last_company = $company_arr[$p_cnt];
										$company_name_arr[$p_cnt] = $space.$company_arr[$p_cnt];
										$p_cnt++;
										$comp_cnt++;
										break;
									}
								}
							}
						}
						//echo $data;echo " ".$i."<br/>";
					}
					
					//情報の整理
					$got_amount_of_money = 0;
					$max_num_times = 0;
					$fixed_flag = 0;
					$max_box = 0;
					//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
					if ($goukei_top == 0) {
						//echo "金額候補を絞る";
						//計を取得できた場合、その最大値を合計と見做す。
						if ($sum_flag >= 1) {
							for ($j = 0;$j < $sum_flag;$j++) {
								if ($max_box <= $sum_box[$j]) {
									$max_box = $sum_box[$j];
								}
							}
		//					echo "z<br>";
							$got_amount_of_money = $max_box;
							$fixed_flag = 1;
						}
						//var_dump($money_arr);
						//echo $goukei;echo "合計<br/>";
						if ($goukei == 0) {
							for ($n = 0;$n < count($money_arr);$n++){
								$temp_money = intval($money_arr[$n]);
								if($fixed_flag == 0) {
									if ($temp_money != 0) {
										//echo "<br/>".$n."tmpmoney".$temp_money."<br>";
										//直後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
										if ($n >= 2) {
											$after2 = abs($temp_money - intval($money_arr[$n-1]));
											if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
												//echo "a<br>";
												$got_amount_of_money = intval($money_arr[$n-2]);
												$fixed_flag = 1;
											}
										}
										
										

										//直前の2つの金額を足したものと同額であれば合計と見做す。(小計と外税と合計)
										if ($n >= 2) {
											$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
											if ($before2 == $temp_money && $before2 != 0) {
												//echo "c<br>";
												$got_amount_of_money = $temp_money;
												$fixed_flag = 1;
											}
										}
										
										//直後の1つを飛ばしたその後2つの金額の差の絶対値と同額であれば合計と見做す。
										if ($n >= 3) {
											$after2 = abs($temp_money - intval($money_arr[$n-1]) && $after2 != 0);
											if ($after2 == intval($money_arr[$n-3])) {
												//echo "d<br>";
												$got_amount_of_money = intval($money_arr[$n-3]);
												$fixed_flag = 1;
											}
										}
										if($fixed_flag == 0) {
											$num_of_same = 0;
											$candidate_box = 0;
											//var_dump($money_arr);
											//echo			count($money_arr);
											//同じ数字が2回以上あるものを取得するロジック
											for ($h = 0;$h < count($money_arr);$h++){
												//echo "<br/>tmp2 temp:";
												$temp_money2 = intval($money_arr[$h]);
												//echo " 2回";echo $temp_money;
												//if ($temp_money == $temp_money2){echo "同じ？？<br>";}
												if ($temp_money == $temp_money2) {
													$num_of_same++;
												}
											}
											//echo "<br>回数：".$num_of_same;
											//2回以上のものの中から回数が最大のもの
											if ($num_of_same >= 2) {

												//2回以上のものの中で最大の金額
												if($got_amount_of_money < $temp_money) {
													//echo "e<br/>";
													$got_amount_of_money = $temp_money;
												}
											}
										}
									}
								}
								
								//▼お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▼
								//直後の2つの金額の上の金額から下の金額を引いた額と同額であれば合計と見做す。（合計と預かりとお釣り）
								if ($n >= 2) {
									//echo $n;echo " b－2のとこ ";
									//var_dump($money_arr);
									//echo intval($money_arr[$n-1])." ".$temp_money." ".$money_arr[$n]."<br/>";
									$after2 = intval($money_arr[$n-1]) - $temp_money;
									if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
										//echo "b-2<br>";
										$got_amount_of_money = intval($money_arr[$n-2]);
										$fixed_flag = 1;
									}
								}

								
								//一つ飛ばした後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
								if ($n >= 3) {
									//echo $n;echo " bのとこ ";echo $temp_money."<br/>";
									//echo intval($money_arr[$n-1])."<br/>";
									$after3 = abs($temp_money - intval($money_arr[$n-1]));
									if ($after3 == intval($money_arr[$n-3]) && $after3 != 0) {
										//echo "b<br>";
										$got_amount_of_money = intval($money_arr[$n-3]);
										$fixed_flag = 1;
									}
								}
								//▲お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▲

								
							}
						} else {
							$got_amount_of_money = $goukei;
						}
					} else {
						//echo "top";
						$got_amount_of_money = $goukei_top;
					}
					//入れる金額が無かった場合、一番上の金額を入れる。
					if ($got_amount_of_money == 0 ) {
						//echo "f<br>";echo $money_arr[0];
						$got_amount_of_money = intval($money_arr[0]);
					}
					//exit;
					//echo "<br/>";echo $got_amount_of_money." 合計<br/><br/>";//exit;
					//var_dump($date_arr);
					//日付
					if ($got_date_top[0] == "") {
						for ($d = 0;$d < count($date_arr);$d++){
							//候補の中の一番最初のものを日付にする。（暫定）
							
							//echo "<br/>日付：";
							//echo strtotime($date_arr[$d]);
							//echo "<br>";
							//echo strtotime(date('Ymd'));
							//echo "<br>";
							if (strtotime($date_arr[$d]) <= strtotime(date('Ymd'))) {
								//echo $date_arr[$d];
								//echo substr($date_arr[0],0,4);
								//echo "<br>";
								//echo date('Y',strtotime("+1 year"));
								//echo substr($date_arr[$d],0,4);echo "<br>";
								//echo date('Y',strtotime("+3 year"));echo "<br>";
								While (substr($date_arr[$d],0,4) < date('Y',strtotime("+2 year")) && substr($date_arr[$d],0,4) >= date('Y',strtotime("-2 year"))) {
									$got_date = $date_arr[$d];
									//echo "<br>";
									break;
									if ($d = count($date_arr) - 1) {
										break;
									}
								}
							}
							//echo "<br/>";
							//echo "\r\n";
						}
					} else {
						
						//for ($dtcnt = 0;$dtcnt < count($got_date_top);$dtcnt++) {
						//	$got_date = $got_date_top[$dtcnt];
						//}
						$got_date = $got_date_top[0];
						
					}
					//var_dump($tel_arr);
					//電話番号
					$exist_flag = 0;
					$comp_name = "";
					if (count($tel_arr) > 0) {
						$tel_arr_num = count($tel_arr);
					} else {
						$tel_arr_num = 1;
					}
					//var_dump($tel_arr_num);
					for ($t = 0;$t < $tel_arr_num;$t++){
						//echo "tel選択：";
						if (substr($tel_arr[$t],0,1) == 0) {
						//echo "<br>電話番号：";
							$tel_num = $tel_arr[$t];
							$exist_flag = 1;
						//echo "<br>";
						}
						
					}//for文
				}//if ( ==png)

				//echo "<br>会社名新:";
				//echo $got_company_name;
				//echo "<br>業種";
				//echo $got_ind;
				//echo "<br>";
				// ファイルのパスを変数に格納
				//echo "<br>ここから";
				$FileText = $file_path.$dlp.'ci.txt';
				//echo "<br>".$FileText."<br>ここまで";
				//商品名を取り出す
				if (count($product_name_arr) > 0) {
					for ($n = 0;$n < count($product_name_arr);$n++ ) {
						if ($n != 0) {$sp = "";}
						$product .= $sp.$product_name_arr[$n];
					}
					//$product_name = $searched_company_name." ".$product;
					$product_name = $product;
				} else {
						$product_name = $searched_company_name;
				}
				//会社名を取り出す
				if (count($company_name_arr) > 0) {
					for ($n = 0;$n < count($company_name_arr);$n++ ) {
						if ($n != 0) {$sp = "";}
						$company_name_one .= $sp.$company_name_arr[$n];
					}
					//$company_names = $searched_company_name." ".$product;
					$company_names = $company_name_one;
				} else {
						$company_names = $searched_company_name;
				}

				
				//日付が未取得の場合の処理
				if ($got_date == "" || date('Ymd',strtotime($got_date)) == "20160101") {
					if ($last_receipt_date != "") {
						$got_date = date('Ym',strtotime($last_receipt_date))."01";
					} else {
						$got_date = date("Y",time())."0101";
					}
				} else {
					//日付が取得できていた場合に前回のレシートの日付として保存する。
					$last_receipt_date = $got_date;
				}
				//電話番号が複数取得できた場合に候補1,2,3がかぶらないようにする処理
				if ($tel_num == $tel_arr[0]) { $tel_num2 = $tel_arr[1];$tel_num3 = $tel_arr[2];} else if ($tel_num == $tel_arr[1]) { $tel_num2 = $tel_arr[0];$tel_num3 = $tel_arr[2];} else { $tel_num2 = $tel_arr[0];$tel_num3 = $tel_arr[1];}
				//if ($searched_company_name == "" || $searched_company_name == "郵便局" ) {$searched_company_name = $company_names;}
				if ($company_names == "") {$company_names = $searched_company_name;}
				if ($industry_type_s == "預入") {$industry_type_s = "";}
				
				//業種に下記の配列内のものがあった場合、商品名を変換する。
				$ind_type_check_arr = array("ケーキ","カフェ","ファミレス","ファストフード","居酒屋","宴会場","ダイニングバー","イタリア料理","ラーメン","うどん","丼もの","弁当","ステーキ","ハンバーグ");
				for ($z = 0;$z < count($ind_type_check_arr);$z++) {
					$pattern_comp_end = "/".$ind_type_check_arr[$z]."/";
					$match_num = preg_match($pattern_comp_end, $industry_type_s, $match);
					if ($match_num > 0) {
						if ($ind_type_check_arr[$z] == "居酒屋"||$ind_type_check_arr[$z] == "ダイニングバー") {
							$product_name = "居酒屋";
						} else {
							$product_name = "飲食代";
						}
						break;
					}
				}
				//鉄道会社
				$cname_conv_arr = array("東海旅客","JR東日本","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","東京地下鉄","東日本旅客","近畿日本鉄道");
				for ($z = 0;$z < count($cname_conv_arr);$z++) {
					$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
					$match_num = preg_match($pattern_comp_end, $company_names, $match);
					if ($match_num > 0) {$company_names = "鉄道会社";}
				}
				
				//コンビニ等店舗ぬき
				$cname_conv_arr = array("セブンイレブン","セブン‐イレブン","ファミリーマート","ニューデイズ","スリーエフ","ローソン","NewDays","ローソン","LAWSON","サンクス","ポプラ","ジェーソン","高島屋","伊勢丹","マルエイ","マルエツ","ミニストップ","くすりの福太郎","ダイソー","DAISO","ファッションセンターしまむら","ジーユー","ユニクロ","ドン・キホーテ","日本郵便株式会社","カメラのキタムラ");
				for ($z = 0;$z < count($cname_conv_arr);$z++) {
					$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
					$match_num = preg_match($pattern_comp_end, $company_names, $match);
					if ($match_num > 0) {
						$company_names = $cname_conv_arr[$z];
						if ($product_name == "コンビニ") {$product_name = "";$industry_type_s = "";}
					}
				}
				
				//タクシー
				$cname_conv_arr = array("基本運賃","乗車料金","タクシー");
				for ($z = 0;$z < count($cname_conv_arr);$z++) {
					$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
					$match_num = preg_match($pattern_comp_end, $product_name.$industry_type_s, $match);
					if ($match_num > 0) {$company_names = "タクシー";$product_name = "";$industry_type_s = "";}
				}
				
				//
				$cname_conv_arr = array("駐車料金");
				for ($z = 0;$z < count($cname_conv_arr);$z++) {
					$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
					$match_num = preg_match($pattern_comp_end, $product_name, $match);
					if ($match_num > 0) {$company_names = "駐車場";$product_name = "";$industry_type_s = "";}
				}

				//書店
				$pattern_comp_end = "/書店/";
				$match_num = preg_match($pattern_comp_end, $industry_type_s, $match);
				if ($match_num > 0) {$product_name = "書籍";}
				
				$company_names = str_replace(array("有限会社","株式会社","（株）","（有）","未登録 &nbsp;","金銭出納員","出納員"),"",$company_names);
				
	//$product_name = $industry_type_s;
				//商品名検索の結果が無かった場合、業種を商品名に入れる
				//if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "第一種定型" || $product_name == "簡易書留") {$product_name = str_replace(array("第一種定型外","簡易書留"),"郵便",$product_name);} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
				if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
				//代金引換用の処理//間違えやすいものにアラートを出すため
				if ($company_names == "ヤマトフィナンシャル") {$company_names = "代金引換";}
				if ($company_names == "売捌所" ||$company_names == "売さばき") {$company_names = "法務局";}
				if ($product_name == "プリカ") {$product_name = "プリペイドカード";}
				//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
//				$AddWords = "\r\n".date('Y/m/d',strtotime($got_date)).",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$industry_type_s;
				//echo "<br>";
//				file_put_contents($FileText, $AddWords, FILE_APPEND);
				
				//全ファイル記録用ファイルに追記
				$FileText2 = $txt_dir."/TextConvertResult/convert_data_all.txt";
				$outcode = "sjis-win";
				$incode = "UTF-8";
				$nl = "\n";
				$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);
				$this ->convertTextCode($FileText , $outcode, $FileText2 , $outcode, $nl);

				// ファイルを出力する
				//readfile($FileText);
				//exit;
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
			//取得したTEXTファイルを削除する。
	//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//$counter++;
			return $FileText;
//		}//for文の終わり
	}//メソッドの終わり

	/**
	* テキストファイルの文字コードを変換し保存する
	* @param string $infname  入力ファイル名
	* @param string $incode   入力ファイルの文字コード
	* @param string $outfname 出力ファイル名
	* @param string $outcode  出力ファイルの文字コード
	* @param string $nl       出力ファイルの改行コード
	* @return string メッセージ
	*/
	function convertCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'wb');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}
	
	function convertTextCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
		$instr = str_replace(array("\r\n","\n\n\n\n","\n\n\n","\n\n","\n\n"),"\n",$instr);
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'a');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}

	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}


	function recurse_chown_chgrp($mypath,$uid, $gid) {
		if (mkdir( $mypath, 0775, true ) ) {
			chmod( $mypath, 0775);
			chown( $mypath, $uid);
		    chgrp( $mypath, $gid);
		  //echo "ディレクトリ作成成功！！";
		} else {
		  //echo "ディレクトリ作成失敗！！";
		}
	} 

	function getFileList($dir) {
	    $files = glob(rtrim($dir, '/') . '/*');
	    $list = array();
	    foreach ($files as $file) {
	        if (is_file($file)) {
	            $list[] = $file;
	        }
	        if (is_dir($file)) {
	            $list = array_merge($list, getFileList($file));
	        }
	    }
	 
	    return $list;
	}

	function random($length = 12) {
	    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
	    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
	}
}//classの終わり

?>