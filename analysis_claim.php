<?php
session_start();

//必要なクラスの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');
//echo "test1";
include_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
include_once(dirname(__FILE__).'/../cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

$start_d = date('Ymd', mktime(0,0,0,date('m')+1,"01",date('Y')-1));//去年の来月1日


if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

if (isset($_POST['aj_words'])) {
	$aj_words = $_POST['aj_words'];

} else {
	//絞込み条件

	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$aj_words .= " AND `pay_date` >= '".$payfrom."' ";
	}/* else {
		$payfrom = $start_d;
		$aj_words .= " AND `pay_date` >= '".$payfrom."' ";
	}*/
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$aj_words .= " AND `pay_date` <= '".$payto."' ";
	}
	if ($_REQUEST['money_from']) {
		$money_from = $_REQUEST['money_from'];
		$having .= "AND sum(`total_price`) >= ".$money_from." ";
	}
	if ($_REQUEST['money_to']) {
		$money_to = $_REQUEST['money_to'];
		$having .= " AND sum(`total_price`) <= ".$money_to." ";
	}
	if ($_REQUEST['cl']) {
		$claimant_id = $_REQUEST['cl'];
	}

	if ($_REQUEST['remarks1']) {
		$remarks1 = $_REQUEST['remarks1'];
		$aj_words .= " AND (`remarks1` LIKE '%".$remarks1."%' OR `product_name` LIKE '%".$remarks1."%') ";
	}
}

//総請求額
$flag = "invoice_data_send_total_narrow_down";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) GROUP BY `destination_id`,`send_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$claim_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($claim_arr);
$claim_num = count($claim_arr);
$total_claim = 0;
for ($i = 0;$i < $claim_num;$i++) {
	$total_claim += $claim_arr[$i]['sum(total_price)'];
}

//総請求金額
//$words = $aj_words." AND `destination_id`= '".$destination_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `claimant_id` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) GROUP BY `claimant_id` HAVING 1 ".$having." ORDER BY `pay_date` ASC";

$dst_total_payment_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($dst_invoice_data_arr);
$dst_total_payment = $dst_total_payment_arr[0]['sum(total_price)'];

//グラフ用データ
$flag = "invoice_total_data_send_narrow_down_make_graph";
//$words = $aj_words." AND `destination_id`= '".$destination_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY ymd HAVING 1 ".$having."";
$words = $aj_words." AND (`status` >= 1 AND `status` <> 99) GROUP BY ymd HAVING 1 ".$having."";
$dst_graph_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);

//var_dump($dst_graph_arr);
//$graph_num = count($dst_graph_arr);
$graph_num = 12;
$dst_graph_arr[0]['ymd'];

if ($dst_graph_arr[0]['ymd'] != "") {
	$start_date = date("Y-m",strtotime($dst_graph_arr[0]['ymd']));
} else {
	$start_date = date("Y-m");
}
list($year, $month) = explode("-", $start_date, 2);

for ($k = 0; $k < $graph_num; $k++) {
	//echo $labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	/*
	$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}
	*/
	//echo $cl_labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}

	
	
	
	$mcount = 0;
	for ($m = 0; $m < $graph_num; $m++) {
		if ( "'".$dst_graph_arr[$m]['ymd']."'" == $temp_date) {
			$mcount = 1;
		//echo "<br/>\r\n";
			$params .= "".$dst_graph_arr[$m]['sum(`total_price`)'];
		//echo "<br/>\r\n";
		}
		if ($m == 11 && $mcount == 0) {
			$params .= "0";
		}
		if ($m == 11 && $k !=11) {
			$params .= ",";
		}
	}
}

//▼自社データ確認▼
$flag = "company_data";
$words = "";
$cl_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($cl_company_data_arr);
$cl_company_data_arr[0]["company_name"];


//ページ条件なしでデータ数を取得
$flag ="select_journalized_history";
$table_name = $company_id;
$data_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words);
//var_dump($journalized_history_arr);

//var_dump($data_num_arr);
//表示数の制御
$max_disp = 100;
$sort_key = "`id`";
$sort_type = "ASC";
$offset = ($page - 1) * $max_disp;
$data_num = count($data_num_arr);

$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";

//ページ数取得
$page_num = ceil($data_num/$max_disp);
//ページ分割のための条件追加
//$words .= " ORDER BY `id` LIMIT 100";
if(!isset($_POST['aj_words'])) {
	$page_words = $aj_words." ORDER BY ";
	$page_words .= $conditions;
}
$flag ="select_journalized_history";
$table_name = $company_id;
$journalized_history_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$page_words);
//var_dump($journalized_history_arr);
//echo count($journalized_history_arr);

?>

<?php require("header.php");?>
<script type="text/javascript" src="//co.cloudinvoice.co.jp/js/Chart.js/Chart.js"></script>
<script src="../js/Chart_Dynamic_View.js"></script>
<!--<link rel="stylesheet" href="../css/chart.css" type="text/css" />-->
<style type="text/css">
#main_box {
	position:relative;
	overflow:hidden;
	min-height:800px;
	width:auto;
	height:auto;
}
#right {
	position: relative;
}
canvas {	
	position: absolute;
	/*
	position: relative;
    */
	z-index: 1;
}
#graph_tbl {
	position: absolute;
	top: 450px;
	width:500px;
}
#graph_tbl td{
	width:200px;
}
#graph_tbl th{
	width:200px;
}
</style><script type="text/javascript">


$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});
</script>
<!--[if IE]>
<script type="text/javascript" src="../js/excanvas.js"></script>
<![endif]-->
<script type="text/javascript">
<!--
window.onload = function() {
	//描画コンテキストの取得
	var canvas = document.getElementById('graph');
	if (canvas.getContext) {

		var context = canvas.getContext('2d');
		var ctx = context;

		var data = {
				//横軸のラベル
				labels : [<?php echo $labels;?>],
				datasets : [
		/*
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : [65,59,90,81,56,55,40]
				},
		*/
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $params;?>]
				}

			]

		}
		
		//棒グラフ
		var myNewChart = new Chart(ctx).Bar(data);
		//線グラフ
		//var myNewChart = new Chart(ctx).Line(data);
		//多角グラフ
		//var myNewChart = new Chart(ctx).Radar(data);
	}
}
-->
</script>

<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>
<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#analysisTable").colResizable({fixed:false});
		$("#analysisTable").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>


<title>
請求一覧 - Cloud Invoice
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article>
<div id="main_box">
	<section id="m-1-box">
		<h2>
		請求一覧
		</h2>
		
		<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<!--
				<div class="kamoku">
					<p>取引先名</p>
					<input type="text" name="" value="<?php echo $cl_company_data_arr[0]['company_name'];?>"/>
				</div>
				-->
				<div class="hiduke">
					<p>取引日付</p>
					<input type="text" id="box1" name="paydate_from" />
					～
					<input type="text" id="box2" name="paydate_to" />
				</div>
				<div class="hiduke">
					<p>金額</p>
					<input type="text"  name="money_from" />
					～
					<input type="text"  name="money_to" />
				</div>
				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
					<input type="hidden" name="cl" value="<?php echo $claimant_id;?>">
				</div>
			</form>
		</section>

		<div class="SearchLine"></div>
		<p><a href="./business_analysis">経営分析に戻る</a></p>
		<section id="customer">
			<div id="left">
				<h3><?php echo $cl_company_data_arr[0]["company_name"];?></h3>
				<p>
					該当期間の総請求金額：<?php echo number_format($total_claim);?> 円
				</p>
				<table id="analysisTable" class="tables tablesorter dragging">
					<colgroup>
						<col align="center" width="200px">
						<col align="center" width="120px">
						<col align="center" width="120px">
						<col align="center" width="120px">
					</colgroup>
					<tr>
						<th>請求先</th>
						<th>請求金額</th>
						<th>請求数</th>
						<th>平均金額</th>
					</tr>
				<?php
				for ($i = 0;$i < $claim_num;$i++) {
					
				//請求書・領収書データの登録
				
				$pay_date = $claim_arr[$i]['pay_date'];
				//$pay_date = $cl_invoice_data_arr[$i]['paid_date'];
				$dlp = $claim_arr[$i]['download_password'];
				//echo $path = "/var/www/co.cloudinvoice.co.jp/html/files/".sprintf("%012d", $cl_invoice_data_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				$path = "../files/".sprintf("%012d", $claim_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				//echo "<br/>\r\n";
				$pdf_url = $path."/".$dlp.".png";
				//$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				if (!file_exists($pdf_url)) {
					$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				}
				
				$customer_id = $claim_arr[$i]['destination_id'];
				$customer_send_code = $claim_arr[$i]['send_code'];
				
				if ($customer_id == "") {
					$customer_id = "000000000000";
				}
				
				//▼取引先データ▼
				$flag = "company_data";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$customer_id,$words);
				//var_dump($cl_company_data_arr);
				$customer_name = $company_data_arr[0]["company_name"];
				if ($customer_name == "") {
					
					//設定内容の表示データ取得
					$flag = "addressee_data_one";
					$words = "AND company_id = ".$company_id." AND `del_flag` <> 1 " ;
					$company_addressee_arr = $company_con -> company_sql_flag($pdo,$flag,$customer_send_code,$words);
					$customer_name = $company_addressee_arr[0]['company_name'];

					
					//$customer_name = "送付先登録顧客";
				}
				//請求書データから請求者のデータを取得
				$destination_id = $company_data_arr[0]["company_id"];
				$flag = "invoice_data_send_total_narrow_down";
				$words = $aj_words." AND `destination_id`= '".$destination_id."' AND `send_code` ='".$customer_send_code."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `invoice_code`,`send_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
				$dst_invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
				//var_dump($dst_invoice_data_arr);
				$dst_invoice_data_num = count($dst_invoice_data_arr);
				if ($dst_invoice_data_num == 0) {
					
					//var_dump($dst_invoice_data_arr);
				}


				?>

					<tr>
						<td id="product" class=""><a href= "./analysis_client?dst=<?php echo $customer_id;?>&sc=<?php echo $customer_send_code;?>"><?php echo $customer_name;?></a></td>
						<td class=""><?php echo number_format($claim_arr[$i]['sum(total_price)']);?> 円</td>
						<td class=""><?php echo number_format($dst_invoice_data_num);?> </td>
						<td class=""><?php echo number_format($claim_arr[$i]['sum(total_price)']/$dst_invoice_data_num);?> 円</td>
					</tr>
				<?php
				}
				?>
				</table><br>
				
			</div>
			<div id="right">
				<canvas id="graph" class="canvas_chart" style="background-color:white;" width="500px" height="400px">
					図形を表示するには、canvasタグをサポートしたブラウザが必要です。
				</canvas>
				<table id="graph_tbl">
				<?php
				$labels_arr = split(",",$labels);
				$params_arr = split(",",$params);
				
				for ($k = 0; $k < count($dst_graph_arr); $k++) {
				?>
				
					<tr><th> <?php echo $dst_graph_arr[$k]['ymd'];?> </th><td> <?php echo number_format($dst_graph_arr[$k]['sum(`total_price`)']);?> 円</td></tr>
				<?php
				}
				?>
				</table>
				<div class="tableText">
					<label id="tableTxt"></label>
				</div>
			</div>
			
			
		</section>
	</section>
</div>
</article>

<?php require("footer.php");?>
