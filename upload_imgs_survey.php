<?php
session_start();
//必要なクラスを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once("/var/www/storage.cloudinvoice.co.jp/html/pdf/PdfToImg.class.php");
//自動仕分け用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/searchkeywordscontrol.class.php');
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = $_REQUEST['path'];
$file_name = $_FILES['upfile']['name'];
$upload_dir_path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$csv_file = "";
$csv_data = "";

//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
$txt_dir = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');


$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}

//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}

//▲企業データの取得▲
if ($_FILES['upfile']) {
	
	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	$counter = 0;
	$error_num = 0;
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	//var_dump($file_ary);
	
	//txtだけを先に読み込む。ブサイクだけどとりあえず。
	foreach ($file_ary as $file) {

		if (substr($file['name'], -3, 3) == "txt"||substr($file['name'], -3, 3) == "pdf") {
			//echo "テキストの処理<br/>\r\n";
			//txtファイルの保存	
			try {
				//ファイル名変換処理
				$file_name_top = substr($file["name"], 0, -4);//echo ":top<br>\r\n";
				$file_extension = substr($file["name"], -4, 4);//echo ":ext<br>\r\n";
				$dlp = md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
				$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
				//$txt_dir;echo "<br>";
				
				
				if (is_uploaded_file($file["tmp_name"])) {
					if (move_uploaded_file($file["tmp_name"], $txt_dir. "/" . $dlp.$file_extension)) {
					    chmod($txt_dir. "/" . $dlp.$file_extension, 0664);
					}
				}
			} catch (Exception $e) { 
				echo $e -> getMessage;
			}
		}

		if ( substr($file["name"], -3, 3) == "pdf") {
			
			//ファイル名変換処理
			$file_name_top = substr($file["name"], 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($file["name"], -4, 4);//echo ":ext<br>\r\n";
			$dlp = md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";

			$pdf_name = substr($filename, 0, -4);
			
			$im = new imagick();
			//for ($m = 1;$m <= 120;$m++) {
				
				//PDFファイルをテキスト化する。
				$file_path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/";
				$pdf_file = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/".$file_name_top.".pdf";
				//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png 2>&1");
				//echo "convert ".$file_path.".pdf -geometry 100% ".$file_path.".png";
				//pdftotextでpdfから
				//shell_exec("pdftotext -raw /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/6988b98f05ca4df774733315f7725519.pdf /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/6988b98f05ca4df774733315f7725519.txt | sed ':loop; N; $!b loop; ;s/\n//g'");
				shell_exec("pdftotext -raw ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
				//shell_exec("pdftotext ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
				//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png ;");
				//shell_exec("convert /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/a_004.pdf /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/a_004.jpg");
				shell_exec("convert -density 600 ".$file_path.$dlp.".pdf ".$file_path.$dlp.".png > /logs/cloudinvoice/makeimg/moveImage/errlog".date('Ymdhis').".log 2>&1");
				//print_r ($output);

				//var_dump($retval);
				//var_dump($last_line);

				//$data = file_get_contents( '/var/www/storage.cloudinvoice.co.jp/html/files/common/0293f3d00a4bef30cdced1d3f615a523.txt' );
				$data = file_get_contents( $file_path.$dlp.'.txt' );
				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				//var_dump($data);
				//$pattern_comp_start = "/^〒/";//郵便記号
				for ( $i = 0; $i < $cnt; $i++ ) {
					//機能停止中
					$match_num = preg_match($pattern_comp_start, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						$company_data_start_line = $i;
					}
					//電話番号取得（TELの文言で取得）機能停止中
					//$pattern_comp_end = "/TEL/";//TELの文言
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					//電話番号取得（番号を取得）
					$pattern_comp_end = "/[0-9]+-[0-9]+-[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							//echo $match[0];
							//echo "<br/>\r\n";
						}
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}

					$pattern_comp_end = "/株式会社/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "企業名候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$company_data_end_line = $i;
					}
					//請求日
					$pattern_comp_end = "/請求日/";
					//$pattern_comp_end = "/[0-9]+\/[0-9]+\/[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$match_result = str_replace(array("請求日","ご請求日","御請求日"," ","　","\t",":","："),"",$data[$i]);
						//echo "請求日候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						//echo $match_result;
						//echo "<br/>\r\n";
						//$start_line = $i + 1;
						$company_data_end_line = $i;
					}
					//支払い日
					$pattern_comp_end = "/支払/";
					//$pattern_comp_end = "/[0-9]+\/[0-9]+\/[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$match_result = str_replace(array("お支払い","御支払","御支払い","お支払","支払","期限","期日"," ","　","\t",":","："),"",$data[$i]);
						//echo "支払期日候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						$match_result;
						//echo "<br/>\r\n";
						//$start_line = $i + 1;
						$company_data_end_line = $i;
					}

				}

				$company_data_start_line;//echo "<br/>\r\n";
				$company_data_end_line;//echo "<br/>\r\n";
				//var_dump($data_arr);



				$pattern1 = "/^品目/";
				for ( $i = 0; $i < $cnt; $i++ ) {
					$match_num = preg_match($pattern1, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						$start_line = $i;
					}
				$pattern2 = "/^小計/";
					$match_num = preg_match($pattern2, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						$end_line = $i - 1;
					}

					//echo($data[$i]);
					//echo "<br/>\r\n";
				}
				$start_line;//echo "<br/>\r\n";
				$end_line;//echo "<br/>\r\n";
				//var_dump($data_arr);

		//	}

			$path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";
		}
	}

	//画像を読み込みながら請求書データを作成し、txtの内容を取得して上書きする。
	foreach ($file_ary as $file) {
		//echo $file["name"];echo "<br>";
		$csv_data .= $file["name"].",";
		//初期化
		$nc_arr = array();
		$ans = array();
		$q[1] = 7;
		$q[2] = 2;
		$q[3] = 4;
		$q[4] = 7;
		$q[5] = 6;
		$q[6] = 2;
		$q[7] = 3;
		$q[8] = 4;
		for ($k = 1;$k <= 8;$k++) {

			for ($i= 1;$i <= $q[$k];$i++) {
				$nc_arr[$k][$i] = 0; 
		 	}
		}
		//var_dump($nc_arr);
		
		$a1_cnt = 0;
		$a2_cnt = 0;
		$a3_cnt = 0;
		$a4_cnt = 0;
		$a5_cnt = 0;
		$a6_cnt = 0;
		$a7_cnt = 0;
		$a8_cnt = 0;
							
		$q1_1 = 0;
		$q1_2 = 0;
		$q1_3 = 0;
		$q1_4 = 0;
		$q1_5 = 0;
		$q1_6 = 0;
		$q1_7 = 0;

		$q2_1 = 0;
		$q2_2 = 0;

		$q3_1 = 0;
		$q3_2 = 0;
		$q3_3 = 0;
		$q3_4 = 0;

		$q4_1 = 0;
		$q4_2 = 0;
		$q4_3 = 0;
		$q4_4 = 0;
		$q4_5 = 0;
		$q4_6 = 0;
		$q4_7 = 0;

		$q5_1 = 0;
		$q5_2 = 0;
		$q5_3 = 0;
		$q5_4 = 0;
		$q5_5 = 0;
		$q5_6 = 0;

		$q6_1 = 0;
		$q6_2 = 0;

		$q7_1 = 0;
		$q7_2 = 0;
		$q7_3 = 0;

		$q8_1 = 0;
		$q8_2 = 0;
		$q8_3 = 0;
		$q8_4 = 0;
		$q8_5 = 0;
		$q8_6 = 0;
		$q8_7 = 0;


		$ans_1 = "";
		$ans_2 = "";
		$ans_3 = "";
		$ans_4 = "";
		$ans_5 = "";
		$ans_6 = "";
		$ans_7 = "";
		$ans_8 = "";

		$a1_st = 0;
		$a2_st = 0;
		$a3_st = 0;
		$a4_st = 0;
		$a5_st = 0;
		$a6_st = 0;
		$a6_st = 0;
		$a6_st = 0;
		
		
		try {
	    	//var_dump($file);
	        $filename = $file['name'];
	        if (substr($filename, -3, 3) == "csv"){
	        	$csv_file_name = $filename;
	        }
			//var_dump($_FILES);
				
			//ファイル名変換処理
			$file_name_top = substr($filename, 0, -4);
			$file_extension = substr($filename, -4, 4);
			$dlp = md5($file_name_top.$rand_words);
			$filename = $dlp.$file_extension;
			
			//請求書が登録されていないか確認する。
			$flag ="download_invoice_data_clm";
			$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
			//同じdownload_passwordで請求書が登録済みの場合
			if (substr($filename, -3, 3) == "txt" ||substr($filename, -3, 3) == "png" || substr($filename, -3, 3) == "jpg" || substr($filename, -3, 3) == "tif" || substr($filename, -3, 3) == "pdf") {
				//請求書登録
				//仮のデータで請求書登録をする。
				//仮の請求書・領収書データの登録
				
				//テキストを読んで上書き処理
				//echo "テキスト処理<br>";
				$text_file_path = $txt_dir."/".$dlp.".txt";
				$text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
				$data_all = $data = file_get_contents($text_file_path);
				//echo "<br/>BBB:";
				$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");
				//echo "<br/>CCC:";echo $data = deleteBom($data);
				//= deleteBom($str);
				//var_dump($data);
				$data = str_replace(array(" ","　"),"",$data);
				$data = str_replace(array("\r\n","\r","\n"," ","　","."),"",$data);
				$data = explode( "\r\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				

				for ( $i = 0; $i < $cnt; $i++ ) {

					//全角文字を半角に変換
					//echo $data[$i] = deleteBom($data[$i]);echo "<br>";
					//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
					$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
					$data[$i] = strtr($data[$i], $kana);
					$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
					$data[$i] = str_replace("肴","有",$data[$i]);
					$data[$i] = str_replace(array("日","■","目","図","〆","西","固","画","日","M","団","卜」","幽","′′","′"),"■",$data[$i]);
					$data[$i] = str_replace(array("ロ","ﾛ","口","□."),"□",$data[$i]);
					$data[$i] = str_replace(array("リ","ﾘ","||"),"り",$data[$i]);
					$data[$i] = str_replace(array("。",".",","),".",$data[$i]);
					$data[$i] = str_replace(array("'","`"," "),"",$data[$i]);
					$data[$i] = str_replace(array("lLEU","1陛]u","11層U/"),"性別",$data[$i]);
					$data[$i] = str_replace(array("ホイベント"),"本イベント",$data[$i]);
					$data[$i] = str_replace(array("爾"),"謝",$data[$i]);
					$data[$i] = str_replace(array("二ユース","二ユ一ス"),"ニュース",$data[$i]);
					$data[$i] = str_replace(array("能","古","露","1;、"),"ふ",$data[$i]);
					$data[$i] = str_replace(array("'、"),"ご",$data[$i]);
					$data[$i] = str_replace(array("護e","滿","We"),"we",$data[$i]);
					
					//echo "<br/>";
					
					
					/*
					$pattern_comp_end = "/[0-9]\./";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "設問候補".($match[0])."<br/>\r\n";
						$toi = str_replace(array("."),"",$match[0]);
						if ($toi == 1) {
							
						} if ($toi == 2) {
							$a1_cnt = 100;
							$a1_st = 0;
							$a2_st = 1;
						} if ($toi == 3) {
							$a2_cnt = 100;
							$a2_st = 0;
							$a3_st = 1;
						} if ($toi == 4) {
							$a3_cnt = 100;
							$a3_st = 0;
							$a4_st = 1;
						} if ($toi == 5) {
							$a4_cnt = 100;
							$a4_st = 0;
							$a5_st = 1;
						} if ($toi == 6) {
							$a5_cnt = 100;
							$a5_st = 0;
							$a6_st = 1;
						} if ($toi == 7) {
							$a6_cnt = 100;
							$a6_st = 0;
							$a7_st = 1;
						} if ($toi == 8) {
							$a7_cnt = 100;
							$a7_st = 0;
							$a8_st = 1;
						}
						
					} else {
						
						$pattern_comp_end = "/性別/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 2;
							//echo "設問候補".$toi."<br/>\r\n";
							$a1_cnt = 100;
							$a1_st = 0;
							$a2_st = 1;

						}
						

						$pattern_comp_end = "/何でお知りに/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 3;
							//echo "設問候補".$toi."<br/>\r\n";
							$a2_cnt = 100;
							$a2_st = 0;
							$a3_st = 1;
						}

						$pattern_comp_end = "/納税の経験/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 3;
							//echo "設問候補".$toi."<br/>\r\n";
							$a3_cnt = 100;
							$a3_st = 0;
							$a4_st = 1;
						}

						$pattern_comp_end = "/納税寄/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 5;
							//echo "設問候補".$toi."<br/>\r\n";
							$a4_cnt = 100;
							$a4_st = 0;
							$a5_st = 1;
						}

						$pattern_comp_end = "/見つかりましたか/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 6;
							//echo "設問候補".$toi."<br/>\r\n";
							$a5_cnt = 100;
							$a5_st = 0;
							$a6_st = 1;
						}

						$pattern_comp_end = "/ベントはいかがで/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 7;
							//echo "設問候補".$toi."<br/>\r\n";
							$a6_cnt = 100;
							$a6_st = 0;
							$a7_st = 1;
						}

						$pattern_comp_end = "/あったら来場/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							$toi = 8;
							//echo "設問候補".$toi."<br/>\r\n";
							$a7_cnt = 100;
							$a7_st = 0;
							$a8_st = 1;
						}

					}
					*/

					//問1
					//if ($a1_cnt != 100) {
						$pattern_comp_end = "/□20代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][1] = 1;
							$q1_1 = 1;
							$a1_cnt++;
						}

						$pattern_comp_end = "/□30代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "30代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][2] = 1;
							$q1_2 = 1;
							$a1_cnt++;
						}
						$pattern_comp_end = "/□40代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "40代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][3] = 1;
							$q1_3 = 1;
							$a1_cnt++;
						}
						$pattern_comp_end = "/□50代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "50代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][4] = 1;
							$q1_4 = 1;
							$a1_cnt++;
						}
						$pattern_comp_end = "/□60代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "60代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][5] = 1;
							$q1_5 = 1;
							$a1_cnt++;
						}
						$pattern_comp_end = "/□70代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "70代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][6] = 1;
							$q1_6 = 1;
							$a1_cnt++;
						}
						$pattern_comp_end = "/□80代/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "80代候補".$match[0]."<br/>\r\n";
							$nc_arr[1][7] = 1;
							$q1_7 = 1;
							$a1_cnt++;
						}
						/*
						echo $a1_cnt;
						if ($a1_cnt >= 1) {
							echo "yes<br/>";
							if ($q1_1 == 1) {
								$q1_arr[1] = 1;
							}
							if ($q1_2 == 1) {
								$q1_arr[2] = 1;
							}
							if ($q1_3 == 1) {
								$q1_arr[3] = 1;
							}
							if ($q1_4 == 1) {
								$q1_arr[4] = 1;
							}
							if ($q1_5 == 1) {
								$q1_arr[5] = 1;
							}
							if ($q1_6 == 1) {
								$q1_arr[6] = 1;
							}
							if ($q1_7 == 1) {
								$q1_arr[7] = 1;
							}
							
							if ($ans_1 == "") {
								
								$nocheck = 0;
								if ($q1_1 == 1) {
									$nocheck += 1;
								}
								if ($q1_2 == 1) {
									$nocheck += 1;
								}
								if ($q1_3 == 1) {
									$nocheck += 1;
								}
								if ($q1_4 == 1) {
									$nocheck += 1;
								}
								if ($q1_5 == 1) {
									$nocheck += 1;
								}
								if ($q1_6 == 1) {
									$nocheck += 1;
								}
								if ($q1_7 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_1 = "未取得";
								} else {
									$ans_1 = "未回答";
								}
								//$ans_1 = rand(1,7);
							}
							
							//echo $ans_1;
							//echo "問1:";
							//echo $ans_1;
							//echo "<br>";
							$a2_cnt = 100;
							$a2_st = 1;
						}
						*/
					//}
					
					//問2
					//if ($a2_cnt != 100 && $a2_st == 1) {
						$pattern_comp_end = "/□男/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[2][1] = 1;
							$q2_1 = 1;
							$a2_cnt++;
						}
						
						$pattern_comp_end = "/□女/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[2][2] = 1;
							$q2_2 = 1;
							$a2_cnt++;
						}
						/*
						if ($a2_cnt >= 1) {
							if ($q2_1 == 0) {
								$ans_2 .= 1;
							}
							if ($q2_2 == 0) {
								if ($ans_2 >= 1) {$ans_2 .= ",";}
								$ans_2 .= 2;
							}							
							
							if ($ans_2 == "") {
								
								$nocheck = 0;
								if ($q2_1 == 1) {
									$nocheck += 1;
								}
								if ($q2_2 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_2 = "未取得";
								} else {
									$ans_2 = "未回答";
								}
								//$ans_2 = rand(1,7);
							}

							//echo "問2:";
							//echo $ans_2;
							//echo "<br>";
							$a2_cnt = 100;
							$a2_st = 0;
							$a3_st = 1;
						}
						*/
					//}


					//if ($a3_cnt != 100 && $a3_st == 1) {
						//echo $data[$i];echo "<br>";
						$pattern_comp_end = "/□.{1,4}るさと/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[3][1] = 1;
							$q3_1 = 1;
							$a3_cnt++;
						}
						
						$pattern_comp_end = "/□.{1,4}友人知人/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "友人知人".$match[0]."<br/>\r\n";
							$nc_arr[3][2] = 1;
							$q3_2 = 1;
							$a3_cnt++;
						}
						
						$pattern_comp_end = "/□webニ/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[3][3] = 1;
							$q3_3 = 1;
							$a3_cnt++;
						}

						$pattern_comp_end = "/□その他/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[3][4] = 1;
							$q3_4 = 1;
							$a3_cnt++;
						}
						/*
						if ($a3_cnt >= 1) {
							if ($q3_1 == 0) {
								$ans_3 .= "1";
							}
							if ($q3_2 == 0) {
								if ($ans_3 >= 1) {$ans_3 .= ",";}
								$ans_3 .= "2";
							}
							if ($q3_3 == 0) {
								if ($ans_3 >= 1) {$ans_3 .= ",";}
								$ans_3 .= "3";
							}
							if ($q3_4 == 0) {
								if ($ans_3 >= 1) {$ans_3 .= ",";}
								$ans_3 .= "4";
							}
							
							if ($ans_3 == "") {
								$nocheck = 0;
								if ($q3_1 == 1) {
									$nocheck += 1;
								}
								if ($q3_2 == 1) {
									$nocheck += 1;
								}
								if ($q3_3 == 1) {
									$nocheck += 1;
								}
								if ($q3_4 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_3 = "未取得";
								} else {
									$ans_3 = "未回答";
								}
								//$ans_3 = rand(1,4);
							}
							
							//echo "問3:";
							//echo $ans_3;
							//echo "<br>";
							$a3_cnt = 100;
							$a3_st = 0;
							$a4_st = 1;
						}
						*/
					//}

						$data[$i] = str_replace(array("\r\n","\r","\n"," ","　","."),"",$data[$i]);
					//if ($a4_cnt != 100 && $a4_st == 1) {
						$pattern_comp_end = "/□8年以上/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][1] = 1;
							$q4_1 = 1;
							$a4_cnt++;
						}
						
						$pattern_comp_end = "/□5年/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][2] = 1;
							$q4_2 = 1;
							$a4_cnt++;
						}
						
						$pattern_comp_end = "/□4年/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][3] = 1;
							$q4_3 = 1;
							$a4_cnt++;
						}

						$pattern_comp_end = "/□3年/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][4] = 1;
							$q4_4 = 1;
							$a4_cnt++;
						}

						$pattern_comp_end = "/□2年/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][5] = 1;
							$q4_5 = 1;
							$a4_cnt++;
						}

						$pattern_comp_end = "/□1年/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][6] = 1;
							$q4_6 = 1;
							$a4_cnt++;
						}

						$pattern_comp_end = "/□無し5/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][7] = 1;
							$q4_7 = 1;
							$a4_cnt++;
						}

						$pattern_comp_end = "/年□無し/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[4][7] = 1;
							$q4_7 = 1;
							$a4_cnt++;
						}

						/*
						//echo $q4;
						if ($a4_cnt >= 1) {
							
							if ($q4_1 == 0) {
								$ans_4 .= "1";
							}
							if ($q4_2 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "2";
							}
							if ($q4_3 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "3";
							}
							if ($q4_4 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "4";
							}
							if ($q4_5 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "5";
							}
							if ($q4_6 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "6";
							}
							if ($q4_7 == 0) {
								if ($ans_4 >= 1) {$ans_4 .= ",";}
								$ans_4 .= "7";
							}

							if ($ans_4 == "") {
								$nocheck = 0;
								if ($q4_1 == 1) {
									$nocheck += 1;
								}
								if ($q4_2 == 1) {
									$nocheck += 1;
								}
								if ($q4_3 == 1) {
									$nocheck += 1;
								}
								if ($q4_4 == 1) {
									$nocheck += 1;
								}
								if ($q4_5 == 1) {
									$nocheck += 1;
								}
								if ($q4_6 == 1) {
									$nocheck += 1;
								}
								if ($q4_7 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_4 = "未取得";
								} else {
									$ans_4 = "未回答";
								}
								//$ans_4 = rand(1,7);
							}
							
							//echo $ans_4;
							//echo "問4:";
							//echo $ans_4;
							//echo "<br>";
							$a4_cnt = 100;
							$a4_st = 0;
							$a5_st = 1;
						}
						*/
					//}
					
					//問5
					//if ($a5_cnt != 100 && $a5_st == 1) {
						$pattern_comp_end = "/□10回以上/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][1] = 1;
							$q5_1 = 1;
							$a5_cnt++;
						}
						
						$pattern_comp_end = "/□5回以上/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][2] = 1;
							$q5_2 = 1;
							$a5_cnt++;
						}
						
						$pattern_comp_end = "/□4回/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][3] = 1;
							$q5_3 = 1;
							$a5_cnt++;
						}

						$pattern_comp_end = "/□3回/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][4] = 1;
							$q5_4 = 1;
							$a5_cnt++;
						}

						$pattern_comp_end = "/□2回/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][5] = 1;
							$q5_5 = 1;
							$a5_cnt++;
						}

						$pattern_comp_end = "/□1回/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[5][6] = 1;
							$q5_6 = 1;
							$a5_cnt++;
						}
						/*
						if ($a5_cnt >= 1) {
							
							if ($q5_1 == 0) {
								$ans_5 .= "1";
							}
							if ($q5_2 == 0) {
								if ($ans_5 >= 1) {$ans_5 .= ",";}
								$ans_5 .= "2";
							}
							if ($q5_3 == 0) {
								if ($ans_5 >= 1) {$ans_5 .= ",";}
								$ans_5 .= "3";
							}
							if ($q5_4 == 0) {
								if ($ans_5 >= 1) {$ans_5 .= ",";}
								$ans_5 .= "4";
							}
							if ($q5_5 == 0) {
								if ($ans_5 >= 1) {$ans_5 .= ",";}
								$ans_5 .= "5";
							}
							if ($q5_6 == 0) {
								if ($ans_5 >= 1) {$ans_5 .= ",";}
								$ans_5 .= "6";
							}
						
						if ($ans_5 == "") {
							
								$nocheck = 0;
								if ($q5_1 == 1) {
									$nocheck += 1;
								}
								if ($q5_2 == 1) {
									$nocheck += 1;
								}
								if ($q5_3 == 1) {
									$nocheck += 1;
								}
								if ($q5_4 == 1) {
									$nocheck += 1;
								}
								if ($q5_5 == 1) {
									$nocheck += 1;
								}
								if ($q5_6 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_5 = "未取得";
								} else {
									$ans_5 = "未回答";
								}
							//$ans_5 = rand(1,6);
						}

							//echo "問5:";
							//echo $ans_5;
							//echo "<br>";
							$a5_cnt = 100;
							$a5_st = 0;
							$a6_st = 1;
						}
						*/
					//}


					//問6
					//if ($a6_cnt != 100 && $a6_st == 1) {
						$pattern_comp_end = "/□有/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[6][1] = 1;
							$q6_1 = 1;
							$a6_cnt++;
						}
						
						$pattern_comp_end = "/□無し7/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[6][2] = 1;
							$q6_2 = 1;
							$a6_cnt++;
						}
						
						$pattern_comp_end = "/り□無し/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "20代候補".$match[0]."<br/>\r\n";
							$nc_arr[6][2] = 1;
							$q6_2 = 1;
							$a6_cnt++;
						}

						
						/*
						if ($a6_cnt >= 1) {
							if ($q6_1 == 0) {
								$ans_6 .= 1;
							}
							if ($q6_2 == 0) {
								if ($ans_6 >= 1) {$ans_6 .= ",";}
								$ans_6 .= 2;
							}
							
							if ($ans_6 == "") {
								
								$nocheck = 0;
								if ($q6_1 == 1) {
									$nocheck += 1;
								}
								if ($q6_2 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_6 = "未取得";
								} else {
									$ans_6 = "未回答";
								}
								//$ans_6 = rand(1,2);
							}

							//echo "問6:";
							//echo $ans_6;
							//echo "<br>";
							$a6_cnt = 100;
							$a6_st = 0;
							$a7_st = 1;
						}
						*/
					//}

					//問7
					//if ($a7_cnt != 100 && $a7_st == 1) {
						//echo "問7番ですよ。";
						//echo $data[$i];echo "<br/>";
						$pattern_comp_end = "/□期待以上/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "7_1候補".$match[0]."<br/>\r\n";
							$nc_arr[7][1] = 1;
							$q7_1 = 1;
							$a7_cnt++;
						}
						
						//echo $data[$i];
						$pattern_comp_end = "/□期待通/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "7_2候補".$match[0]."<br/>\r\n";
							$nc_arr[7][2] = 1;
							$q7_2 = 1;
							$a7_cnt++;
						}

						$pattern_comp_end = "/□改善希望/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "7_3候補".$match[0]."<br/>\r\n";
							$nc_arr[7][3] = 1;
							$q7_3 = 1;
							$a7_cnt++;
						}
						/*
						if ($a7_cnt >= 1) {
							if ($q7_1 == 0) {
								$ans_7 .= 1;
							}
							if ($q7_2 == 0) {
								if ($ans_7 >= 1) {$ans_7 .= ",";}
								$ans_7 .= 2;
							}
							if ($q7_3 == 0) {
								if ($ans_7 >= 1) {$ans_7 .= ",";}
								$ans_7 .= 3;
							}
														
							if ($ans_7 == "") {
								$nocheck = 0;
								if ($q7_1 == 1) {
									$nocheck += 1;
								}
								if ($q7_2 == 1) {
									$nocheck += 1;
								}
								if ($q7_3 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_7 = "未取得";
								} else {
									$ans_7 = "未回答";
								}

								//$ans_7 = rand(1,3);
							}
							
							//echo "問7:";
							//echo $ans_7;
							//echo "<br>";
							$a7_cnt = 100;
							$a7_st = 0;
							$a8_st = 1;
						}
						*/
					//}

					//問8
					//if ($a8_cnt != 100 && $a8_st == 1) {
						//echo "問8";
						$pattern_comp_end = "/□是非来場/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "8_1候補".$match[0]."<br/>\r\n";
							$nc_arr[8][1] = 1;
							$q8_1 = 1;
							$a8_cnt++;
						}
						
						//echo $data[$i];
						$pattern_comp_end = "/□時間が合えば来場したい/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "8_2候補".$match[0]."<br/>\r\n";
							$nc_arr[8][2] = 1;
							$q8_2 = 1;
							$a8_cnt++;
						}

						$pattern_comp_end = "/□わからない/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "8_3候補".$match[0]."<br/>\r\n";
							$nc_arr[8][3] = 1;
							$q8_3 = 1;
							$a8_cnt++;
						}

						$pattern_comp_end = "/□来場したくない/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "8_3候補".$match[0]."<br/>\r\n";
							$nc_arr[8][4] = 1;
							$q8_4 = 1;
							$a8_cnt++;
						}
						/*
						if ($a8_cnt >= 1) {
							if ($q8_1 == 0) {
								$ans_8 = 1;
							}
							if ($q8_2 == 0) {
								$ans_8 = 2;
							}
							if ($q8_3 == 0) {
								$ans_8 = 3;
							}
							if ($q8_4 == 0) {
								$ans_8 = 3;
							}
							
							if ($ans_8 == "") {
								$nocheck = 0;
								if ($q8_1 == 1) {
									$nocheck += 1;
								}
								if ($q8_2 == 1) {
									$nocheck += 1;
								}
								if ($q8_3 == 1) {
									$nocheck += 1;
								}
								echo "空欄数：".$nocheck."<br/>";
								if ($nocheck == 0) {
									$ans_8 = "未取得";
								} else {
									$ans_8 = "未回答";
								}

								//$ans_8 = rand(1,2);
							}

							//echo "問8:";
							//echo $ans_8;
							//echo "<br>";
							$a8_st = 0;
							$a8_cnt = 100;

						}
						*/
					//}
					
				//	echo $data[$i];
				//	echo "<br/>";
				}
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
		//取得したTEXTファイルを削除する。
		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
		$counter++;
	for ($k = 1;$k <= 8;$k++) {
		//var_dump($nc_arr[$k]);
		for ($i= 1;$i <= count($nc_arr[$k]);$i++) {
	 		//echo $nc_arr[$k][$i];echo "<br/>";
	 		if ($nc_arr[$k][$i] == 0) {
	 			if (count($ans[$k]) >= 1){
	 				$ans[$k] .= ";";
	 			}/* else {
	 				$ans[$k] .= $k."-";
	 			}*/
	 			$ans[$k] .= $i;
	 		}
	 	}
	 	//if ($ans[$k] == "") { $ans[$k] = "空欄のみ";}
	}
	
	$csv_data .= $ans[1].",".$ans[2].",".$ans[3].",".$ans[4].",".$ans[5].",".$ans[6].",".$ans[7].",".$ans[8]."\n";
	//echo "<br/>";
	
	
	
	}//foreach文の終わり
}

//出力ファイル名の作成
$csv_file = "enq_data_". date ( "Ymdhis" ) .'.csv';

//文字化けを防ぐ
$csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );

//MIMEタイプの設定
header("Content-Type: application/octet-stream");
//名前を付けて保存のダイアログボックスのファイル名の初期値
header("Content-Disposition: attachment; filename={$csv_file}");

// データの出力
echo($csv_data);
exit();



if ($counter > 0) {
	$counter = $counter;
	$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをアップロードしました。";
}
if($error_num > 0) {
	$_SESSION['up_info_msg'] = $error_num."件のファイルをアップロードできませんでした。";
}
//echo $counter;
$file_list_arr = getFileList($path);
//var_dump($file_list_arr);
/*
header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='https://storage.cloudinvoice.co.jp/invoice/imgs_documents_upload'</script>";
echo "</html>";
echo "<input type='button' value='移動' onclick='location.href=\'./imgs_documents_upload\''>";
*/

//header("Location:https://storage.cloudinvoice.co.jp/invoice/imgs_documents_upload",true);

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

/**
* 文字列からBOMデータを削除する
*
* @param string $str 対象文字列
* @return string $str BOM削除した文字列
*/
function deleteBom($str)
{
    if (($str == NULL) || (mb_strlen($str) == 0)) {
        return $str;
    }
    if (ord($str{0}) == 0xef && ord($str{1}) == 0xbb && ord($str{2}) == 0xbf) {
        $str = substr($str, 3);
    }
    return $str;
}

?>