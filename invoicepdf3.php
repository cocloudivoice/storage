<?php session_start();?>
<?php
//クラスファイルの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$sum_price = 0;
$sales_tax = 0;
$total_price = 0;
$t_price = 0;



//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_data_send";

$destination_id = $_REQUEST['c1'];
$send_code = $_REQUEST['c2'];
$billing_date = $_REQUEST['bd'];
$invoice_code = $_REQUEST['ic'];
$pay_date = $_REQUEST['pd'];
//$insert_date = $_REQUEST['ind'];
$status = $_REQUEST['st'];

//$param = "?destination_id=".$destination_id."&send_code=".$send_code."&billing_date=".$billing_date."&invoice_code=".$invoice_code."&pay_date=".$pay_date."&status=".$status;
//echo $param;

//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['company_id'] != NULL) {
	$company_id = $_REQUEST['company_id'];
}

if ($_REQUEST['c1'] != NULL) {
	$destination_id = $_REQUEST['c1'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['c2'] != NULL) {
	$send_code = $_REQUEST['c2'];
	$words .= " AND send_code = '".$send_code."' ";
}
if ($_REQUEST['st'] != NULL) {
	$status = $_REQUEST['st'];
	$words .= " AND status = ".$status;
} else {
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
/*
if ($_REQUEST['ind'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['ind'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
*/
if ($_REQUEST['bd'] != NULL) {
	$billing_date = $_REQUEST['bd'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pd'] != NULL) {
	$pay_date = $_REQUEST['pd'];
	$words .= " AND pay_date = ".$pay_date;
}

if ($_REQUEST['ic'] != NULL) {
	$invoice_code = $_REQUEST['ic'];
	$words .= " AND invoice_code = '".$invoice_code."' ";
}

//echo $words;


/*
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = $_REQUEST['destination_id'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['send_code'] != NULL) {
	$send_code = $_REQUEST['send_code'];
	$words .= " AND send_code = ".$send_code;
}
if ($_REQUEST['status'] != NULL) {
	$status = $_REQUEST['status'];
	$words .= " AND status = ".$status;
} else {
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
if ($_REQUEST['insert_date'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['insert_date'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
if ($_REQUEST['billing_date'] != NULL) {
	$billing_date = $_REQUEST['billing_date'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pay_date'] != NULL) {
	$pay_date = $_REQUEST['pay_date'];
	$words .= " AND pay_date = ".$pay_date;
}
*/
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
$flag = "";//フラグの初期化


	//var_dump($_REQUEST);
	//var_dump($invoice_data_receive_arr);
	$data_cnt = count($invoice_data_receive_arr);
	//var_dump($invoice_data_receive_arr_count);


?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">



html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}

article{width:790px; height:1125px; background-image:-webkit-radial-gradient(1500px 562px, circle farthest-side, white 96%,rgb(200,255,255) 98%, rgb(100,255,255) 100%);}
section{width:600px; padding:40px 0 0 130px;}



table#top{color:rgb(180,180,180); font-weight:bold; font-size:13px; letter-spacing:0.1em; margin:10px 0 0 0;}
table#top td#img{width:330px; text-align:center;}
img{width:250px;}



div#main1{height:260px; margin:40px 0 0 0;}
div#main1 h2{font-size:16px; margin:0 0 10px 0}
div#main1 ul{list-style-type:none; margin:0 0 30px 0;}
div#main1 p{margin:0 0 10px 0;}



table#main2{border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230);}
table#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); font-size:14px;}
table#main2 th#item{width:300px; text-align:left;}
table#main2 th#cost{width:100px; text-align:right;}
table#main2 th#amount{width:50px; text-align:right;}
table#main2 th#price{width:100px; text-align:right;}
table#main2 td{height:30px; text-align:right;}
table#main2 td#first{text-align:left; padding:10px 0 5px 0;}
table#main2 td#item{text-align:left; padding:0;}



table#main3{border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); margin:0 0 0 300px;}
table#main3 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); font-size:14px;}
table#main3 td#item{width:100px;}
table#main3 td#amount{width:150px;text-align:right;}



table#main4{margin:50px 0 0 90px;}
table#main4 td{}
table#main4 td#left{width:180px; height:35px;}
table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
table#main4 td#notes{width:180px; height:35px;}



</style>




</head>



<body>



<article>

<section>
	<table id="top">
		<tr>
			<td rowspan="7" id="img"><img src="/images/logo2.png"></img></td>
			<td>クラウドインボイス株式会社</td>
		</tr>
		<tr>
			<td><br></td>
		</tr>
		<tr>
			<td>〒180-0004</td>
		</tr>
		<tr>
			<td>東京都武蔵野市吉祥寺本町1-13-6</td>
		</tr>
		<tr>
			<td>芝第3アメレックスビル901</td>
		</tr>
		<tr>
			<td>TEL：03-0000-0000</td>
		</tr>
		<tr>
			<td>FAX：03-0000-0000</td>
		</tr>
	</table>

	<div id="main1">
		<h2>請求書</h2>
		<p>
			請求書番号　　<?php echo $invoice_data_receive_arr[0]['invoice_code'];?><br>
			請求日　　　　<?php echo $invoice_data_receive_arr[0]['billing_date'];?><br>
			　<br>
		</p>
		<p>
			〒108-0073<br>
			東京都港区三田3-12-18<br>
			芝第3アメレックスビル8F
		</p>
	
		<p>
			サンプル得意先<br>
			担当部署　担当者
		</p>
		
	
	</div>


	<table id="main2">
		<tr>
			<th id="item">品目</th>
			<th id="cost">単価</th>
			<th id="amount">数量</th>
			<th id="price">価格</th>
		</tr>
			<?php 
			for ($i = 0; $i < $data_cnt; $i++) {
			?>
			<tr>
				<td id="item"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
				<td><?php echo number_format((int)$invoice_data_receive_arr[$i]['unit_price']);?></td>
				<td><?php echo number_format((int)$invoice_data_receive_arr[$i]['quantity']);?></td>
				<td><!--<?php echo number_format((int)$t_price = $invoice_data_receive_arr[$i]['unit_price']*$invoice_data_receive_arr[$i]['quantity']);?>--><?php echo number_format((int)$invoice_data_receive_arr[$i]['total_price']);?></td>
			</tr>
			<?php
				$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price'];
				$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
				/*
				$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
				*/
				$total_price += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
	</table>
	
	<table id="main3">
		<tr>
			<td id="item">小計</td>
			<td id="amount"><?php echo number_format($sum_price);?></td>
		</tr>
		<tr>
			<td id="item">消費税</td>
			<td id="amount"><?php echo number_format($sales_tax);?></td>
		</tr>
		<tr>
			<td id="item">合計</td>
			<td id="amount"><?php echo number_format($total_price);?></td>
		</tr>
	</table>
	
	
	
	<table id="main4">
		<tr>
			<td id="left">お支払期限</td>
			<td>2014/07/31</td>
		</tr>
		<tr>
			<td>振込先</td>
			<td>○○銀行○○支店
		</tr>
		<tr>
			<td id="gray">お振込手数料は御社</td>
			<td>普通口座　1234567</td>
		</tr>
		<tr>
			<td id="gray">ご負担にてお願い致します。</td>
			<td>口座名義　クラウドインボイス(カ</td>
		</tr>
		<tr>
			<td id="notes">備考</td>
			<td>1234567</td>
		</tr>
	</table>
</section>


</article>
</body>


