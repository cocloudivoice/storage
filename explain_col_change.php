<?php
session_start();

//controlクラスファイルの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/sitecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$sitedb = new site_control();

//変数
$user_id;
$site_id;
$site_update_arr = array();
$ret_url;

//user_id,site_id
if (isset($_REQUEST['site_id'])) {
	$site_id = $site_arr['site_id'] = $_REQUEST['site_id'];
} else if (isset($_SESSION['site_id'])) {
	$site_id = $site_arr['site_id'] = $_SESSION['site_id'];
}
/*
if (isset($_REQUEST['user_id'])) {
	$user_id = $site_update_arr['user_id']  = $_REQUEST['user_id'];
} else if (isset($_SESSION['user_id'])) {
	$user_id = $site_update_arr['user_id']  = $_SESSION['user_id'];
}
*/


//タグのアップデートデータ取得
if (isset($_REQUEST['explain_col_num'])) {
	$site_arr['column'] = "explain_col_num";
	$site_arr['update'] = htmlspecialchars($_REQUEST['explain_col_num'],ENT_QUOTES);
}
if (isset($_REQUEST['col1_size'])) {
	$site_arr['column2'] = "col1_size";
	$site_arr['update2'] = htmlspecialchars($_REQUEST['col1_size'],ENT_QUOTES);
}
if (isset($_REQUEST['col2_size'])) {
	$site_arr['column3'] = "col2_size";
	$site_arr['update3'] = htmlspecialchars($_REQUEST['col2_size'],ENT_QUOTES);
}
if (isset($_REQUEST['col3_size'])) {
	$site_arr['column4'] = "col3_size";
	$site_arr['update4'] = htmlspecialchars($_REQUEST['col3_size'],ENT_QUOTES);
}

if (isset($_REQUEST['ret_url'])) {
	echo $ret_url = htmlspecialchars($_REQUEST['ret_url'],ENT_QUOTES);
} else {
	$ret_url = "http://hikaku.jp";
}

//タグのアップデートの処理
$sitedb -> site_update($pdo,$site_arr);

header("Location:".$ret_url."");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $site_title_data['site_title']; ?>比較,<?php echo $site_title_data['titletag01'];echo ',';echo $site_title_data['titletag02'];echo ',';echo $site_title_data['titletag03'];echo ',';echo $site_title_data['titletag04'];echo ',';echo $site_title_data['titletag05'];echo ',';echo $tag1;echo ',';echo $tag2;echo ',';echo $tag3;echo ',';echo $tag4;echo ',';echo $tag5;echo ',';echo $tag6;echo ',';echo $tag7;echo ',';echo $tag8;echo ',';echo $tag9;echo ',';echo $tag10;echo ','; ?>,ランキング,カフェ,家電,テレビ,買い物,ショッピング,パソコン,ノートパソコン,タブレット,iphone,食品,食べ物,果物,特産品,服,インテリア,雑貨,小物,金融,証券,FX,投資,冷蔵庫,洗濯機" /> 
		<meta name="description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />		
		<title><?php echo $site_title_data['site_title']; ?>比較 - [比較.jp]</title>
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link rel="shortcut icon" href="http://hikaku.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://hikaku.jp/" />
		<script src="http://hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>
		<script src="http://hikaku.jp/sp/common/js/common.js" type="text/javascript" charset="UTF-8"></script>
		<meta name="viewport" content="width=device-width">
		<script type="text/javascript">
			$(function(){
				$("input#change").each(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
				$("input#change").click(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
			});
		</script>
	<!--暫定のスタイルシート20140212報告用▽-->
	<style type="text/css">
	body {
		width:100%;
		height:100%;
	}
	#wrap {
		width:100%;
		height:100%;
	}
	#container {
		padding:10px;
		width:480;
		height:480;
		margin-left:auto;
		margin-right:auto;
	}
	.display_rank {
		text-align:center;
	}
	.display_rank_data{
		text-align:center;
	}
	.image-box{
		text-align:center;
	}
	.input_form{
		padding:5px;
	}
	.closeBtn {
		text-align:left;
		margin-left:auto;
		margin-right:auto;
		margin-top:10px;
	}
	</style>
	<!--暫定のスタイルシート△-->
	</head>
	<body>
		<div id="wrap">
			<div id="container">
	            <!--▽フリースペース投稿フォーム▽-->

				<div class="closeBtn">
					<a href="<?php echo $ret_url;?>">戻る</a>
				</div>
	            	
				<!--△フリースペース投稿フォーム△-->
			</div><!--container-->
		</div><!--wrap-->
	</body>
</html>