<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();

//変数の宣言
$ahead_url = htmlspecialchars($_REQUEST['ahead_url'], ENT_QUOTES);
$return_url = htmlspecialchars($_REQUEST['return_url'], ENT_QUOTES);
$path = htmlspecialchars($_REQUEST['path'], ENT_QUOTES);
$file_name = htmlspecialchars($_FILES['upfile']['name'], ENT_QUOTES);
$mail_flag = 0;
$uid = "apache";
$gid = "dev";
header('Content-Type: text/plain; charset=utf-8');

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = htmlspecialchars($_REQUEST['colms'], ENT_QUOTES);

//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}
//▲企業データの取得▲

if ($_FILES['upfile']) {
	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	foreach ($file_ary as $file) {
	try {
    	//var_dump($file);
        $filename = $file['name'];
        if (substr($filename, -3, 3) == "csv"){
        	$csv_file_name = $filename;
        }
			//var_dump($_FILES);
			if ($file['name']) {
				if ($_REQUEST['path'] == "") {
					$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "pdffiles";
					
				};
				//$path = "../files/" . $user_id. "/". "pdffiles";
				if ( mkdir( $path, 0775, true ) ) {
					chmod( $path, 0775);
					recurse_chown_chgrp($path, $uid, $gid);
					//chown($path, $uid);
		            //chgrp($path, "dev");
				  //echo "ディレクトリ作成成功！！";
				} else {
				  //echo "ディレクトリ作成失敗！！";
				}
			}

			if (is_uploaded_file($file["tmp_name"])) {
				if (move_uploaded_file($file["tmp_name"], $path. "/" . $filename)) {
				    chmod($path. "/" . $filename, 0664);
					$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件のファイルをアップロードしました。";
					$user_arr['user_img'] = $path. "/" . $_FILES['imgurl']["name"];
					$mail_flag = 1;
				} else {
					$_SESSION['up_info_msg'] = $filename . "等のファイルをアップロードできませんでした。";
				}
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}
	}
}
//var_dump($_FILES);
//echo $csv_file_name;
if ( $_FILES['upfile']['tmp_name'] != NULL ) {
	header("Location:{$ahead_url}?fn=".$csv_file_name."",false);
} else {
	header("Location:$return_url",false);
}
exit();

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 

?>