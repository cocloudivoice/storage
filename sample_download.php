<?php

session_start();
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");

$user_arr = array();
if (isset($_SESSION['user_id'])) {
	$user_id = $_SESSION['user_id'];
} else {
	header("Location:./logout.php");
	exit();
}

ob_start();
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="sample.csv"');
	/*header('Content-Length: '.filesize('sample.csv'));*/
	readfile("../files/common/sample.csv");
ob_end_flush();
	exit();

?>
<script type="text/javascript" onload="window.close()" onclick="window.close()">close</script>