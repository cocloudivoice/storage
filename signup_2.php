<?php
session_start();

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');


//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();


$mail_address = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
$user1_con = new user_control();
$user1_arr = $user1_con -> user_select($pdo,$mail_address);

if ($mail_address == NULL || $mail_address == "") {
	$_SESSION['duplicationError'] = "メールアドレスが確認できません。もう一度お試しください。";
	header("Location:./signup.php");
	exit();
}


//メールアドレス重複確認
if ($user1_arr["mail_address"] != NULL || $user1_arr["mail_address"] != "") {
	$_SESSION['duplicationError'] = "同じメールアドレスで登録されたアカウントがあります。";
	header("Location:./signup.php");
	exit();
}

//ユーザー登録
$user1_arr['nick_name']   = "";//htmlspecialchars($_REQUEST['company_name'],ENT_QUOTES);
$user1_arr['mail_address'] = htmlspecialchars($_REQUEST['e_mail'],ENT_QUOTES);
//$user1_arr['password']    = htmlspecialchars($_REQUEST['login_password'],ENT_QUOTES);
$user1_arr['kana'] = "";//htmlspecialchars($_REQUEST['kana'],ENT_QUOTES);

//$user1_arr['password'] = md5( $user1_arr['password'] );

if (!$user1_arr['user_id']){
	$user_num = $user1_con -> user_get_new_num($pdo);
	$user1_arr['user_id'] = $user_num;
}
$result = $user1_con -> user_insert($pdo,$user1_arr);
//var_dump($user1_arr);
$user2_arr = $user1_con -> user_select($pdo,$mail_address);


//企業データ登録▼
$company_id = "".$user1_arr['user_id'];
$sql = "
INSERT INTO `COMPANY_TABLE` ( 
	`company_id`, `company_name`, 
	`company_name_reading`, 
	`email`
) VALUES ( 
	".$user1_arr['user_id'].", '".$user1_arr['nick_name']."', 
	'".$user1_arr['kana']."', 
	'".$user1_arr['mail_address']."' 
)";

$company_con = new company_control();
$company_arr = $company_con -> company_sql($pdo,$company_id,$sql);

//企業データ登録▲
//▼自動仕訳用企業DB登録▼
//企業品目テーブルと企業用レコードテーブルを登録
$table_name = "".$company_id;
try {
	$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
} catch (PDOException $e) {
    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
    $_SESSION['error_msg'] = $e->getMessage();
}

//▲自動仕訳用企業DB登録▲

//Emailアドレスの存在を確認する。
$email = $user1_arr['mail_address'];
$login_con = new user_control();
$login_arr = $login_con -> user_select_email($pdo,$email);
$_SESSION['user_id'] = $login_arr['user_id'];

if ($login_arr != NULL) {
	$login_arr = $login_con -> user_select_email($pdo,$email);
	//$passwordmaker = random(12);
	$passwordmaker = uniqid().random(13);
	$login_arr['password'] = md5($passwordmaker);
	$login_con -> user_update($pdo,$login_arr);
	$login_arr = $login_con -> user_select_email($pdo,$email);
	//var_dump($login_arr);

	mb_language("ja");
    mb_internal_encoding("UTF-8");
	$newpassword = $passwordmaker;
//	$subject ="仮パスワード発行メール";
//	$header = 'From: Cloud Invoice運営 <info@storage.cloudinvoice.co.jp>' . "\r\n" .'Reply-To: Cloud Invoice運営<info@storage.cloudinvoice.co.jp>' . "\r\n";

	$header = "From:".mb_encode_mimeheader("Cloud Invoice 運営 ","ISO-2022-JP-MS")."<info@storage.cloudinvoice.co.jp>";
	$subject = "ご登録ありがとうございます。－ Cloud Invoice 運営" ;
	$message ="
--------------------------------------------------------------
本メールは、システムより自動送信しています。
心当たりがない場合は、下記のお問い合わせ先までご連絡ください。
なお、このメールに返信されても応答できません。ご了承ください。
--------------------------------------------------------------

このたびは、クラウドインボイスのお申込みをいただき
ありがとうございます。

ご利用に必要な情報をお送りしますので
以下の内容をご確認ください。

------------------------------------------------------------

▼初回ログインの流れ

下記のログインURLをクリックして
管理者ID、初期パスワードでログインしてください。

その後、セキュリティ向上のため
パスワードを変更してください。

▼ログインURL
http://storage.cloudinvoice.co.jp/invoice/signin

▼管理者ID
".$login_arr['mail_address']."

▼初期パスワード
".$newpassword."

--------------------------------------------------------------------
ご登録のE-mailアドレスと仮パスワードでログインしてから、
パスワードの変更及び企業情報のご登録をお願いいたします。

パスワードの変更は、設定マーク→【ログイン設定】→パスワード：変更
企業情報の登録は、【設定】→【事業所登録】
--------------------------------------------------------------------

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数
ではございますが、運営(info@cloudinvoice.co.jp)まで
メールをいただけますでしょうか。

ご案内は以上です。
どうぞ、よろしくお願いいたします。
ご登録ありがとうございます。

Cloud Invoice運営
";
	mb_send_mail($email,$subject,$message,$header);
	mail_to_master($email);
	//mail("sign_up_user@cloudinvoice.co.jp",mb_encode_mimeheader('【通知】ユーザー登録', 'ISO-2022-JP-MS'),mb_convert_encoding('ユーザーが登録されました。'.$email.'', 'ISO-2022-JP-MS'),$header);
}


function mail_to_master($mail) {
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    $message ="新しく登録されたユーザーの情報は次の通りです。\n";
    $message.="────────────────────────────────────\n";
    $message.= $mail."\n\n";
    $message.="DATE: ".date("Y/m/d - H:i:s")."\n";
    $message.="IP: ".$_SERVER['REMOTE_ADDR']."\n";
    $message.="HOST: ".@gethostbyaddr($_SERVER['REMOTE_ADDR'])."\n";
    $message.="USER AGENT: ".$_SERVER['HTTP_USER_AGENT']."\n";
    $message.="────────────────────────────────────\n";
   $headers = "From:".mb_encode_mimeheader("CloudInvoice 運営",'ISO-2022-JP-MS')."<info@cloudinvoice.co.jp>";
   $subject = "storageユーザー登録通知 N";
   mb_send_mail("sign_up_user@cloudinvoice.co.jp",$subject,$message,$headers);
}



function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仮パスワードを発行しました。</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">ユーザー仮登録完了</h1>
		<h2>ユーザー仮登録が完了しました。</h2><br/>
		<p>ご登録のメールアドレスに送付いたしました仮パスワードで<br/>
		ログインして認証を完了してください。
		</p>
		<button onclick="window.location.href='//storage.cloudinvoice.co.jp/'">クラウドインボイストップに戻る</button>
	</div>
</body>
</html>