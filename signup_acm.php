<?php	
session_start();
/*
if(isset($_SESSION['user_id'])){
	header("Location:./logout.php");
}
*/
//$_SESSION['user_id']="";
//session_id();

if(isset($_SESSION['acm_check'])){
	$passErr = $_SESSION['acm_check'];
}
if(isset($_SESSION['acm_emailcheck'])){
	$noEmailErr = $_SESSION['acm_emailcheck'];
}
if(isset($_SESSION['acm_duplicationError'])){
	$duplicationEmailErr = $_SESSION['acm_duplicationError'];
}

if(isset($_SESSION['acm_em'])){
	$email_address = $_SESSION['acmacm_con_em'];
}
//var_dump($_SESSION);

?>

<!DOCTYPE html>

<html Lang="ja">

<head>

        <title>ユーザー登録フォーム - Cloud Invoice</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
	    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
        <!--<link rel="stylesheet" type="text/css" href="css/style.css" />-->
		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/ChunkFive_400.font.js" type="text/javascript"></script>

<script type="text/javascript">

function checkSub() {
	//var	pass1 = document.register.login_password.value;
	//var pass2 = document.register.login_password2.value;
	//var name = document.register.company_name.value;
	//var kana = document.register.kana.value;
	var email = document.register.e_mail.value;
	if (pass1 != "" && pass2 !="" && name != "" && email !="") {
		if (pass1 === pass2) {
			var n = $('#chksub:checked').length;
			if (n == 1){
				if (checkEmail() == true) {
					document.register.submit();
				}
			} else {
				alert("利用規約とプライバシーポリシーの同意にチェックが入っていません。");
				return false;
			}
		} else {
			alert("パスワードが一致しません。");
			return false;
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function checkEmail() {
	var emailval = document.register.e_mail.value;
	//alert(emailval);

	if (emailval != "") {
		if(check(emailval) == true) {
			document.register.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function check(val) {
	var flag = 0;
	// 設定開始（チェックする項目を設定）

	if (!val.match(/.+@.+\..+/)) {
		flag = 1;
	}
	// 設定終了
	if (flag) {
		window.alert('メールアドレスが正しくありません'); // メールアドレス以外が入力された場合は警告ダイアログを表示
		return false; // 送信を中止
	}
	else {
		return true; // 送信を実行
	}
}

</script>

<style type="text/css">


html *{margin:0px; padding:0px;}
body {font: 14px/20px 'メイリオ','Hiragino Kaku Gothic Pro','sans-serif';font-size:13px;}
a {text-decoration:none; color:blue}
a:hover{text-decoration:underline; color:rgb(50,180,240);}
button{cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}

div#clear{clear:both;}

header{width:510px; margin:10px auto 0 auto;}
header h1{float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header h1 a ,header h1 a:visited {float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header ul{text-align:right; line-height:35px; margin:0 10px 0 0; font-size:13px;}
header li{display:inline;}



article{width:510px; height:320px; margin:20px auto 0 auto; border-color:blue; border-radius:5px; border-width:1px; border-style:solid; border-color:rgb(180,210,210)}
article h2{margin:12px 0 0 20px; color:rgb(50,50,50); font-size:18px; color:rgb(240,132,0)}
article p{margin:30px 0 0 20px; font-weight:bold; color:rgb(255,120,0); font-size:14px;}
article a#link{margin:0 15px 0 0;}

div#m-1-box{margin:25px 0 0 0;text-align:right;}
div#m-1-box h3{font-size:14px; float:left; margin:0 20px 0 20px; line-height:19px;}
div#m-1-box input{width:300px; height:20px; margin:0 20px 0 3px;}


article button[type=button]{border:none; width:200px; height:30px; margin:30px 0 0 150px; background:linear-gradient(rgb(80,230,230),rgb(60,200,200)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer; float:left;}
article button[type=button]:hover{background:linear-gradient(rgb(95,235,235),rgb(70,212,212));}

div#m-2-box img{width:100px; margin:15px 0 0 20px;}


footer{width:510px; margin:10px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px;}

span.error{	display:none;	visibility:hidden;	color:red;	font-size:11px;	font-style:italic;}
.right{	float:right;}

</style>




<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>




<body>



<?php include_once("./header.html"); ?>



<article>
	
	<h2>
		従業員登録
	</h2>
	<a href="./signin_acm" rel="login" class="linkform right" id="link">すでにアカウント作成済みの方はこちら</a>
	<p>
		メールアドレス・ユーザー名・所属企業コードを設定してください。<br>
		<?php //設定したメールアドレスにアカウント作成のメールを送ります。?>
	</p>
	<?php  if(isset($duplicationEmailErr)){echo '<p style="text-align:center;color:red;">'.$duplicationEmailErr.'</p>';} ?>
	<div id="form_wrapper" class="form_wrapper">
		<form class="register" name="register" method="post" action="./signup_2_acm" onsubmit="checkEmail();return false;">

			<div id="m-1-box">
				<p style="color:#000;margin-bottom:10px;">
					E-mail　　：
					<INPUT type="e-mail" name="e_mail" value="<?php echo $email_address;?>" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"><!--<br/><?php if($duplicationEmailErr){echo nl2br($duplicationEmailErr)."\n";} ?>--><!--メールアドレス重複を通知--></span>
				</p>
				<p style="color:#000;margin-top:10px;">
					ユーザー名 ：
					<INPUT type="name" name="name" value="" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"></span>
				</p>
				<p style="color:#000;margin-top:10px;">
					所属先コード：
					<INPUT type="text" name="company_id" value="" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"></span>
				</p>

<!--
				<h3>
					郵便番号：
				</h3>
					<INPUT type="zip_code" name="zip_code" value="" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"></span>
				<br/>
				<h3>
					住所1：
				</h3>
					<INPUT type="address1" name="address1" value="" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"></span>
				<br/>
				<h3>
					住所2：
				</h3>
					<INPUT type="address2" name="address2" value="" />
					<span class="err" style="margin-left:30px;color:red;font-size:0.5em;"></span>
-->
			</div>
	
	
			<div id="m-2-box">
				<button type="button" onclick="checkEmail();return false;" >メールアドレスを設定</button>
				<a href="http://www.onlinessl.jp/product/rapidssl.html" target="_blank"><img src="/images/rapidssl.png"></a>
			</div>
			<div id="clear"></div>
		</form>
	</div>
</article>


<?php include_once("./footer.html"); ?>
	
</body>
<?php 
	//エラー表示初期化
	$_SESSION['check'] = "";
	//$_SESSION['em'] = "";
	$passErr = $_SESSION['check'] = "";
	$noEmailErr = $_SESSION['emailcheck'] = "";
	$_SESSION['duplicationError'] = "";
	
?>