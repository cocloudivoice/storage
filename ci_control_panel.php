<?php session_start(); ?>
<?php require("header.php");?>

	<title>CI管理画面</title>
</head>
<body>
	<h1 style="display:none;">CI管理画面</h1>
	<section id="m-2-box">
		<h2>管理メニュー</h2>
		<h3>税理士登録</h3>
		<p><a href="./edit_client_af">税理士 顧客管理</a></p>
		<br/>
		<h3>税理士顧客登録</h3>
		<p><a href="./nruser_upload">非正規ユーザーCSV登録</a></p>
		<p><a href="./record_table_upload">RECORDテーブルCSV登録</a></p>
		<p><a href="./indtype_s_upload">業種小分類をMAINテーブルにCSV登録</a></p>
		<br/>
		
		<h3>企業用 仕訳登録</h3>
		<!--<p><a href="./imgs_to_text_journal_upload_all">仕訳処理</a></p>-->
		<p><a href="./journal_upload">仕訳処理</a></p>
		
		<h3>証憑登録</h3>
		<p><a href="./imgs_documents_upload">証憑登録</a></p>
		<br/>
		<h3>通帳データ取込</h3>
		<p><a href="./transaction_history_csv_upload">通帳データ取込</a></p>
		<br/>
		<h3>アカウント管理</h3>
		<p><a href="./authority">従業員アカウント管理</a></p>
		<p><a href="./addMember">従業員アカウント作成</a></p>

		<h3>画像テキスト化</h3>
		<p><a href="./imgs_to_text_upload_all">サーバー内証憑画像の全テキスト化</a></p>

		<h3>テキスト変換</h3>
		<p><a href="./imgs_to_text_convert_upload_all">表形式テキストの変換(MJS仙台)</a></p>
		<p><a href="./text_convert_bankbook_upload_all">通帳テキストの変換</a></p>

		<h3>楽たす請求代行</h3>
		<p><a href="./rakutasu_imgs_to_text_upload_all">楽たす請求書</a></p>
		<p><a href="https://mng-furikomi.bizsky.jp/Bpo/Login?returnUrl=%2fbpo">楽たすログイン</a></p>
		
		<h3>医療費領収書データ化</h3>
		<p><a href="./medical_imgs_to_text_upload_all">医療費領収書</a></p>
		
		<h3>企業リスト</h3>
		<p><a href="./company_list_form">企業リスト登録</a></p>
		
		<h3>機械学習</h3>
		<p><a href="./machine_learning_text_upload">機械学習</a></p>

	</section>
</body>
</html>