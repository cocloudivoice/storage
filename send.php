<?php //session_start();?>
<?php
header('Access-control-allow-origin: *');// 通信を許可する接続元ドメインか*（アスタリスク）を指定
header("Content-type: text/plain; charset=UTF-8");

//必要なクラスファイルを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$company_con = new company_control();


if (isset($_POST['Kyoutu_No'])) {
    //ここに何かしらの処理を書く（DB登録やファイルへの書き込みなど）
	if ($_POST['Kyoutu_No'] != ""){
		$posted_text = $_POST['Kyoutu_No'];
		$flag = "company_data_ajax_search";
		$words = "OR (`company_name` LIKE '%".$posted_text."%' OR `prefecture` LIKE '%".$posted_text."%' OR `address1` LIKE '%".$posted_text."%' OR `address2` LIKE '%".$posted_text."%' OR `address_full` LIKE '%".$posted_text."%' OR `email` LIKE '%".$posted_text."%') AND non_regular_flag <> 2";
		$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$posted_text,$words);
	}
	for ($i = 0; $i < count($company_data_arr); $i++){
		if ($company_data_arr[$i]['company_name'] != "") {
			(int)$data_company_id = (int)$company_data_arr[$i]["company_id"];
			echo '<p onclick="getText(this,'.$data_company_id.')">';
			echo '<span style="color:rgb(50,50,50); font-weight:bold;">';
		    echo $company_data_arr[$i]['company_name']."\r\n";
		    echo "(".$company_data_arr[$i]['company_id'].")<br/>";
		    echo '</span>';
		    echo $company_data_arr[$i]['prefecture'].$company_data_arr[$i]['address1']."\r\n";
		    echo $company_data_arr[$i]['address2']."<br/>";
		    //echo $company_data_arr[$i]['email'];
			echo '</p>';
		} else {
			//echo "そのコードの企業は存在しません<br/>";
		}
	}
} else {
    echo 'リクエストエラーです。';
}
exit;
?>