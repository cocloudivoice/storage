<?php
	
require_once("aaMailImapHead.php");

// メールボックスを開きます
// INBOX は受信トレイになります。
$mbox = imap_open("{{$host}:{$port}/imap/ssl}INBOX", $user, $password) or exit("Connection Error");
 
// 開いたメールボックスの情報を取得
$boxObj = imap_check($mbox);
// 指定メールボックス内の全てのメール情報を取得
$overviewList = imap_fetch_overview($mbox, "1:{$boxObj->Nmsgs}", 0);
$mailList = array();
foreach ($overviewList as $overview) {
    $mailList[$overview->msgno] = array(
        "msgno"        => $overview->msgno,
        "date"        => date("Y-m-d H:i:s", strtotime($overview->date)),
        "from"        => convertMailStr($overview->from),
        "to"        => convertMailStr($overview->to),
        "subject"    => convertMailStr($overview->subject)
    );
}
// 逆順に
$mailList = array_reverse($mailList, true);
// 閉じる
imap_close($mbox);
 
echo '<ul>';
foreach ($mailList as $mail) {
    echo '<li><a href="./IMAP.php?mail='.$mail['msgno'].'">'.$mail['subject'].'</a><br />
    [from]'.$mail['from'].', [to]'.$mail['to'].', ['.$mail['date'].']
    </li>';
}
echo '</ul>';

?>