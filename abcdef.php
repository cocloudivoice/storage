<?php
/**
 * Imap受信テスト(文字コードは環境によって変更してください。)
 *  ※このファイルへのアクセス認証はしっかりかけましょう
 * php-imapモジュールが必要です。
 */
//--------------------------------------------------------------------------------------//
function h($str) {
  return htmlspecialchars($str, ENT_QUOTES);
}
function he($str) {
  echo h($str);
}
function convert_mail_str($str) {
  return mb_convert_encoding(mb_decode_mimeheader($str), "utf-8", mb_detect_encoding($str));
}
//--------------------------------------------------------------------------------------//
mb_language('Japanese');
mb_internal_encoding('ISO-2022-JP');
 
$MAIL_HOST = "mail.co.cloudinvoice.co.jp";
$MAIL_FETCH_COUNT = 20;
$MAIL_USER = "test5@co.cloudinvoice.co.jp";
$MAIL_PASS = "1234abcd";
 
$mbox = @imap_open("{" . $MAIL_HOST . ":143/imap}INBOX", $MAIL_USER, $MAIL_PASS)
             or exit("error.<br />\n");
 
if ($id = intval($_GET["id"])) {
  $head = imap_header($mbox, $id);
  $body = imap_body($mbox, $id, FT_INTERNAL);
  $mail_data = array(
    "date"          => date("Y-m-d H:i:s", strtotime($head->date)),
    "to"            => convert_mail_str($head->toaddress),
    "cc"            => convert_mail_str($head->ccaddress),
    "from"          => convert_mail_str($head->fromaddress),
    "subject"       => convert_mail_str($head->subject),
    "body"          => mb_convert_encoding($body, "utf-8", "auto"),
  );
} else {
  $mboxes = imap_check($mbox);
  $start_cnt = $mboxes->Nmsgs - $MAIL_FETCH_COUNT;
  $overview_list = imap_fetch_overview($mbox,"{$start_cnt}:{$mboxes->Nmsgs}",0);
  $mail_list = array();
  foreach ($overview_list as $_overview) {
    $mail_list[$_overview->msgno] = array(
            "msgno"         => $_overview->msgno,
            "date"           => date("Y-m-d H:i:s", strtotime($_overview->date)),
            "from"           => convert_mail_str($_overview->from),
            "to"             => $_overview->to,
            "subject"       => convert_mail_str($_overview->subject),
    );
  }
  $mail_list = array_reverse($mail_list, true);
}
 
imap_close($mbox);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ImapTest</title>
</head>
<body>
<?php if ($id && $mail_data): ?>
    <?php he($mail_data["date"]); ?><br />
    <b><?php he($mail_data["subject"]); ?></b><br />
    From:<?php he($mail_data["from"]); ?><br />
    To　:<?php he($mail_data["to"]); ?><br />
    Cc　:<?php he($mail_data["cc"]); ?><br />
    <div><?php echo nl2br(h($mail_data["body"])); ?></div>
<?php else: ?>
  <?php foreach ($mail_list as $mail): ?>
    <a href="<?php he($_SERVER['SCRIPT_NAME'])?>?id=<?php he($mail['msgno']);?>">■<?php he($mail["subject"]); ?></a><br />
    <?php he($mail["date"]); ?><br />
    From:<?php he($mail["from"]); ?><br />
    To　:<?php he($mail["to"]); ?><br />
    <hr />
  <?php endforeach ?>
<?php endif ?>
<hr />
<a href="<?php he($_SERVER['SCRIPT_NAME']); ?>" accesskey="0">HOME</a>
</body>
</html>