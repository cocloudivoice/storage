<?php
session_start();

//処理制限時間を外す
set_time_limit(0);

//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
include_once(dirname(__FILE__)."/../pdf/CloudVisionTextDetection.class.php");
//自動仕分け用クラスの読み込み
//include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//テキスト分析加工用クラスの読み込み
include_once(dirname(__FILE__).'/editOCRText2.class.php');
//include_once(dirname(__FILE__).'/editOCRTextAuto.class.php');


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
//$auto_journalize_con = new auto_journalize_control();

//OCR済みテキスト分析編集用コントローラーを起動
$edittext_con = new editOCRText();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = $_REQUEST['path'];
$file_name = $_FILES['upfile']['name'];
$upload_dir_path = $path;//dirname(__FILE__)."/../pdf/png/".$af_id;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;
//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
//$txt_dir = dirname(__FILE__)."/../pdf/png/".$af_id;
$txt_dir = $upload_dir_path;

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//$upload_dir_pathのフォルダが存在しない場合に作る。
recurse_chown_chgrp($upload_dir_path, $uid, $gid);

//setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');


$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}


$file_list_arr = getFileList($upload_dir_path);
$k = 0;
for($i = 0;$i < count($file_list_arr); $i++) {
	if (substr($file_list_arr[$i], -3, 3) == "png" || substr($file_list_arr[$i], -3, 3) == "jpg" || substr($file_list_arr[$i], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp") {
		$img_file_arr[$k] =  basename($file_list_arr[$i]);
		$k++;
	}
	
}

if ($img_file_arr) {
	$counter = 0;
	$error_num = 0;
	
	//画像をテキスト化する工程
	for($m = 0;$m < count ($img_file_arr) ;$m++) {
//		if(!file_exists($txt_dir."/".substr($img_file_arr[$m], 0, -4).".txt")) {
			if ( substr($img_file_arr[$m], -3, 3) == "pdf" || substr($img_file_arr[$m], -3, 3) == "png" || substr($img_file_arr[$m], -3, 3) == "jpg" || substr($img_file_arr[$m], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp") {

				//ファイル名変換処理
//				$img_file_arr[$m] = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
				$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);
				$file_name_top = substr($img_file_arr[$m], 0, -4);//echo ":top<br>\r\n";
				$file_extension = substr($img_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
				$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
				$filename = $dlp.$file_extension;
				$pdf_name = substr($filename, 0, -4);
				//$im = new imagick();
				
				$file_path = $upload_dir_path."/";
				$pdf_file = $file_path.$file_name_top.".pdf";
				if ($file_extension == ".png" || $file_extension == ".jpg" || $file_extension == ".bmp" ) {
					$ImgText = $cloudvision_con -> ImagetoText($file_path.$dlp.$file_extension,$feature);
					//クラウドヴィジョンでテキスト化する。
					if ($ImgText == "") {
						$ImgText = $cloudvision_con -> ImagetoText($file_path.$file_name_top.$file_extension,$feature);
					}
					// ファイルのパスを変数に格納
					$FileText = $file_path.$dlp."ci".'.txt';
					// ファイルに書き込む
					file_put_contents($FileText,$ImgText,LOCK_EX);
					// ファイルを出力する
					//readfile($FileText);
					//exit;
				}
				if ($file_extension == ".tif") {
					shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
				}
				if ($file_extension == ".bmp") {
					shell_exec("convert -geometry 100% ".$file_path.$dlp.$file_extension." ".$file_path.$dlp.".png 2>&1");				
				}
				//PDFファイルをテキスト化する。
				if ($file_extension == ".pdf") {
					//pdftotextでpdfから
					shell_exec("pdftotext -raw ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
					shell_exec("convert -density 600 ".$file_path.$dlp.".pdf ".$file_path.$dlp.".png > /logs/cloudinvoice/makeimg/moveImage/errlog".date('Ymdhis').".log 2>&1");
				}
				$path = $file_path;
			}
//		}
		try {
			//tifファイルを消す。
			shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
		} catch (Exception $e) {
			echo $e -> getMessage;
		}
		
		$edittext_con -> editText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$txt_dir,$file_path,$af_id);
		//$edittext_con -> editText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$txt_dir,$file_path,$af_id);
		$counter++;
	}
	$_SESSION["comment"] = "データを登録しました。";
}

if ($counter > 0) {
	$counter = $counter;
	$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをアップロードしました。";
}
if($error_num > 0) {
	$_SESSION['up_info_msg'] = $error_num."件のファイルをアップロードできませんでした。";
}
$counter;
$file_list_arr = getFileList($path);
//var_dump($file_list_arr);

//テスト用。仮にここで止める。
//exit;

if(count($file_list_arr) > 0) {
	header("Location:./download_imgs_to_text_all?path=".$upload_dir_path,true);
	echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./imgs_to_text_upload_all2\"'>";
	exit;
} else {
	//echo "no DL<br>";
}

header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all2'</script>";
echo "</html>";

echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./imgs_to_text_upload_all2\"'>";


//header("Location:https://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all2",true);
//exit;


/**
* テキストファイルの文字コードを変換し保存する
* @param string $infname  入力ファイル名
* @param string $incode   入力ファイルの文字コード
* @param string $outfname 出力ファイル名
* @param string $outcode  出力ファイルの文字コード
* @param string $nl       出力ファイルの改行コード
* @return string メッセージ
*/
function convertCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'wb');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>