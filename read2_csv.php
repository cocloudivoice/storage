<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/db_control.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$db_con = new db_control();
$invoice_data_con = new invoice_data_control();

//変数の宣言
$company_id = $_SESSION['user_id'];

//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];

$file = '../files/'.$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
}




ob_start();

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}
//var_dump($csv);echo "<br />\n";

//echo "<table border=1>";
$csv_id = "".$_SESSION['file_name'].$company_id.rand();
foreach ($csv as $value) {
	//var_dump($value);echo "<br />\n";
	//echo "<tr>";
/*
	for ( $i = 0; $i < $colms ; $i++ ) {
		//echo $colms;
		//echo "<td>";
		if ($value[$i] != "") {
			//echo $cell = $value[$i];
		} else {
			//*echo $cell = NULL;
		}
		//echo "</td>";
	}
*/
	//ここでDBにインサート
	//var_dump($value);

	if ( $value[0] != "共通コード") {
		$sql = "
		INSERT INTO `INVOICE_DATA_TABLE` (
		`claimant_id`,`destination_id`, `send_code`, `billing_date`, `invoice_code`,
		`invoice_name`, `staff_name`, `pay_date`,
		`bank_account`, `sale_date`, `product_code`,
		`product_name`, `unit_price`, `quantity`, `unit`,
		`sales_tax`, `withholding_tax`, `total_price`,
		`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`
		) VALUES
		( ".$company_id.", ".$value[0].", '".intval(str_replace($value[1])."', ".$value[2].", '".$value[3]."',
		'".$value[4]."', '".$value[5]."', ".intval($value[6]).",
		'".$value[7]."', ".$value[8].", '".$value[9]."',
		'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval($value[12]).", '".$value[13]."',
		'".$value[14]."', '".$value[15]."', ".intval(str_replace(',','',$value[16])).",
		'".$value[17]."', '".$value[18]."', '".$value[19]."', '".$csv_id."', '".$_SESSION['file_name']."'
		)";echo "\r\n";

		$invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
	}
	//echo "</tr>";
}

//CSV取り込み履歴を記録する。
//CSV_HISTORY_TABLEですが、ついでなのでINVOICE_DATA_TABLEのコントローラーで処理してしまいます。
$sql = "
INSERT INTO `CSV_HISTORY_TABLE`(
`company_id`, `file_name`, `csv_id`
) VALUES (
".$company_id.", '".$_SESSION['file_name']."', '".$csv_id."'
)";
$invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);


//echo "</table>";

fclose($temp);
$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込みました。";
header("Location:./seikyucsv", false);
ob_end_flush();
exit();
?>
<button onclick="window.location.href='./main'">戻る</button>
</body>