<?php 
	session_start();
	if(isset($_SESSION['check'])){
		$passErr = $_SESSION['check'];
	}
	if(isset($_SESSION['emailcheck'])){
		$noEmailErr = $_SESSION['emailcheck'];
	}
	if(isset($_SESSION['duplicationError'])){
		$duplicationEmailErr = $_SESSION['duplicationError'];
	}

?>
<!DOCTYPE html>
<html Lang="ja">


<head>
	
<title>パスワードアシスタント - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>
	
<script type="text/javascript">
<!-- 
function checkSub() {
	var	pass1 = document.register.yourpass1.value;
	var pass2 = document.register.yourpass2.value;
	var name = document.register.yourname.value;
	var email = document.register.youraddress.value;
//	alert(pass1 + pass2 + name + email);

	if (pass1 != "" && pass2 !="" && name != "" && email !="") {
		if (pass1 === pass2) {
			var n = $('#chksub:checked').length;
			if (n == 1){
				if(check(email) == true) {
					document.register.submit();
				}
			} else {
				alert("個人情報の送信に同意されていません。");
				return false;
			}
		} else {
			alert("パスワードが一致しません。");
			return false;
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function login() {
	var val1 = document.loginfrm.id.value;
	var val2 = document.loginfrm.password.value;
//	alert(val1 + val2);

	if (val1 != "" && val2 !="") {
//開発効率上、便宜的にコメントアウト。リリース時には有効にする。
		if(check(val1) == true) {
			document.loginfrm.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}
function checkEmail() {
	var emailval = document.reminderfrm.email.value;
	//alert(emailval);

	if (emailval != "") {
		if(check(emailval) == true) {
			document.reminderfrm.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function check(val){
	var flag = 0;
	// 設定開始（チェックする項目を設定）

	if(!val.match(/.+@.+\..+/)){
		flag = 1;
	}
	// 設定終了
	if(flag){
		window.alert('メールアドレスが正しくありません'); // メールアドレス以外が入力された場合は警告ダイアログを表示
		return false; // 送信を中止
	}
	else{
		return true; // 送信を実行
	}
}

// -->
</script>
<style type="text/css">


html *{margin:0px; padding:0px;}
body {font: 14px/20px 'メイリオ','Hiragino Kaku Gothic Pro','sans-serif';}
a {text-decoration:none; color:blue}
a:hover{text-decoration:underline; color:rgb(50,180,240);}
button{cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}

div#clear{clear:both;}

header{width:510px; margin:10px auto 0 auto;}
header h1{float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header h1 a ,header h1 a:visited {float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
header ul{text-align:right; line-height:35px; margin:0 10px 0 0; font-size:13px;}
header li{display:inline;}



article{width:510px; padding:0 0 40px 0; margin:20px auto 0 auto; border-color:blue; border-radius:5px; border-width:1px; border-style:solid; border-color:rgb(180,210,210)}
article h2{margin:12px 0 0 20px; color:rgb(50,50,50); font-size:18px; color:rgb(240,132,0)}
article h3{margin:20px 0 0 20px; font-weight:bold; color:rgb(255,120,0);}
article p{margin:10px 20px 0 20px; font-size:13px;}

div#m-1-box{margin:15px 0 15px 0;}
div#m-1-box h4{font-size:14px; float:left; width:220px; text-align:right; line-height:19px;}
div#m-1-box input[type=text]{width:210px; height:17px; margin:0 0 0 3px;}
div#m-1-box input[type=pass]{width:140px; height:17px; margin:0 0 0 3px;}

div#m-2-box{margin:30px 0 0 90px;}
div#m-2-box input[type=checkbox]{float:left; margin:4px 3px 0 0; cursor:pointer;}

article button[type=nyukai]{border:none; width:140px; height:30px; margin:10px 0 0 190px; background:linear-gradient(rgb(80,230,230),rgb(60,200,200)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100);  cursor: pointer;}
article button[type=nyukai]:hover{background:linear-gradient(rgb(95,235,235),rgb(70,212,212));}
INPUT[type=button] {cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
INPUT[type=button]:hover{background:rgb(230,230,230);}


footer{width:510px; margin:10px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px;}

span.error{	display:none;	visibility:hidden;	color:red;	font-size:11px;	font-style:italic;}
.right{	float:right;}
.clear {clear:both;}

</style>



<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>




<body>



<?php include_once("./header.html"); ?>



<article>
	
	<h2>
		パスワードアシスタント
	</h2>
	<p>
		アカウントにご登録のEメールアドレスを入力して、「次に進む」ボタンをクリックしてください。バスワード再設定用のページへのリンクをEメールでお送りします。
	</p>
	<div id="m-1-box">
		<h4>
			Eメールアドレス：
		</h4>
			<form name="reminderfrm" class="forgot_password" action="reminder.php" method="post">
				<INPUT type="text" name="email"/>
				<p class="right"><INPUT type="button" value="パスワードの再設定" onclick="checkEmail()"/></p><br/>
				<span class="err" style="margin-left:30px;color:red;"><?php if($noEmailErr){echo $noEmailErr."\n";} ?><!--Emailが登録されていないことを通知--></span>
			</form>
			<div class="clear"></div>
	</div>
	<p>
		Eメールアドレスが変わっているなど、パスワード再設定用のEメールを受け取れない場合は、お手数ですが<a href="./operator">お問い合わせ</a>にてご連絡ください。
	</p>
	<p><a href="./signin" class="right">ログインフォームに戻る</a></p>
</article>



<?php include_once("./footer.html"); ?>
<?php 
		$passErr = $_SESSION['check'] = "";
		$noEmailErr = $_SESSION['emailcheck'] = "";
		$duplicationEmailErr = $_SESSION['duplicationError'] = "";
?>

</body>