<?php
session_start();

//必要なクラスの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
require_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');
//echo "test1";
require_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
require_once(dirname(__FILE__).'/../cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}


//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = dirname(__FILE__).'/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            $tax_class = split(",",$buffer);
            $tax_class_arr[$tax_class[0]] = $tax_class[1];
            
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

//if ( $_REQUEST['mode'] === 'download' ) {
	//var_dump($_REQUEST);
	//echo $_POST['aj_words'];
	if (isset($_POST['aj_words'])) {
	//echo "1";
		$aj_words = $_POST['aj_words'];
	} else {
		//echo "2";
		//絞込み条件
		$aj_words = " WHERE 1";

		if ($_REQUEST['paydate_from']) {
			$payfrom = $_REQUEST['paydate_from'];
			$aj_words .= " AND `paid_date` >= ".$payfrom." ";
		}
		if ($_REQUEST['paydate_to']) {
			$payto = $_REQUEST['paydate_to'];
			$aj_words .= " AND `paid_date` <= ".$payto." ";
		}
		if ($_REQUEST['debit_account_name']) {
			$debit_account_name = $_REQUEST['debit_account_name'];
			$aj_words .= " AND `debit_account_name` LIKE '%".$debit_account_name."%' ";
		}
		if ($_REQUEST['debit_sub_account_name']) {
			$debit_sub_account_name = $_REQUEST['debit_sub_account_name'];
			$aj_words .= " AND `debit_sub_account_name` LIKE '%".$debit_sub_account_name."%' ";
		}
		if ($_REQUEST['debit_section_name']) {
			$debit_section_name = $_REQUEST['debit_section_name'];
			$aj_words .= " AND `debit_section_name` LIKE '%".$debit_section_name."%' ";
		}
		if ($_REQUEST['credit_account_name']) {
			$credit_account_name = $_REQUEST['credit_account_name'];
			$aj_words .= " AND `credit_account_name` LIKE '%".$credit_account_name."%' ";
		}
		if ($_REQUEST['credit_sub_account_name']) {
			$credit_sub_account_name = $_REQUEST['credit_sub_account_name'];
			$aj_words .= " AND `credit_sub_account_name` LIKE '%".$credit_sub_account_name."%' ";
		}
		if ($_REQUEST['credit_section_name']) {
			$credit_section_name = $_REQUEST['credit_section_name'];
			$aj_words .= " AND `credit_section_name` LIKE '%".$credit_section_name."%' ";
		}
		if ($_REQUEST['remarks']) {
			$remarks = $_REQUEST['remarks'];
			$aj_words .= " AND `remarks` LIKE '%".$remarks."%' ";
		}

	}

	//ページ条件なしでデータ数を取得
	$flag ="select_journalized_history";
	$table_name = $company_id;
	$aj_words = " WHERE `customer_id` = '".$company_id."' ";
	$data_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words,$aj_words2);
	//var_dump($journalized_history_arr);
	
	//var_dump($data_num_arr);
	//表示数の制御
	$max_disp = 100;
	$sort_key = "`id`";
	$sort_type = "ASC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	if(!isset($_POST['aj_words'])) {
		$aj_words .= " ORDER BY ";
		$aj_words .= $conditions;
	}
	$flag ="select_journalized_history";
	$table_name = $company_id;
	$aj_words = " WHERE `customer_id` = '".$company_id."' ";
	$journalized_history_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words,$aj_words2);

//}

if ( $_REQUEST['csv'] === 'csv' ) {
//incioce_code_checkerとこの行の請求書番号が一致しなければ
//新しい請求書と判断しヘッダーをつける。
//弥生形式の方はヘッダーなし
	for ($i = 0;$i < $data_num;$i++) {
		
		//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
		$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
		$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
	
		$claimant_id = $journalized_history_arr[$i]["company_id"];
		//echo ",会社名：";
		$company_name = $journalized_history_arr[$i]["company_name"];
		//echo ",借方勘定科目：";
		$debit_account_name = $journalized_history_arr[$i]["debit_account_name"];
		//echo ",借方勘定科目：";
		$debit_sub_account_name = $journalized_history_arr[$i]["debit_sub_account_name"];
		//echo ",借方勘定科目：";
		$debit_section_name = $journalized_history_arr[$i]["debit_section_name"];
		//echo ",回数：";
		$flag1 = $journalized_history_arr[$i]["flag1"];
		//echo ",請求日：";
		$billing_date = str_replace("-","",$journalized_history_arr[$i]["paid_date"]);
		//echo ",支払日：";
		$pay_date = $journalized_history_arr[$i]["paid_date"];
		//echo ",借方金額（税込合計）：";
		$total_price = $journalized_history_arr[$i]["debit"];
		//echo ",借方消費税：";
		$sales_tax = $journalized_history_arr[$i]["debit_tax"] * 1;
		//echo ",貸方勘定科目：";
		$credit_account_name = $journalized_history_arr[$i]["credit_account_name"];
		//echo ",貸方補助勘定科目：";
		$credit_sub_account_name = $journalized_history_arr[$i]["credit_sub_account_name"];
		//echo ",貸方部門：";
		$credit_section_name = $journalized_history_arr[$i]["credit_section_name"];
		//echo ",貸方金額：";
		$credit_amount_of_money = $journalized_history_arr[$i]["credit"];
		//echo ",貸方消費税：";
		$credit_sales_tax = $journalized_history_arr[$i]["credit_tax"] * 1;
		
		//echo ",備考：";
		$remarks = $journalized_history_arr[$i]["remarks"];
		//echo ",品名：";
		$product_name = $journalized_history_arr[$i]["product_name"];
		//echo "<br/>\r\n";
		$base_money = $journalized_history_arr[$i]['base_amount_of_money'];
		
		//明細行数カウント
		$row_count = 0;
		$num_box = 0;
		for($row = 1;$row <= 9;$row++) {
			if($journalized_history_arr[$i]['debit'.$row] * 1 > 0 || $journalized_history_arr[$i]['credit'.$row] * 1 > 0) {
				$row_count += 1;
				$num_box += 1;
			}
		}
		//var_dump($tax_class_arr);
		//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
		$debit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['debit_flag1']];
		$credit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['credit_flag1']];
		$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
		$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);


		$flag1 = $row_count;//行数をDBに格納して渡す予定でテスト用に変数に1を入れていたが、無しでも取得できたのでif文にしなくてもいい。
		//echo $row_count;
		if ($num_box == 0) {
			$csv_data .= "2000,";
		} else {
			$csv_data .= "2110,";
		}
		$csv_data .= ",,";
		$csv_data .= substr($billing_date,0,4)."/".substr($billing_date,4,2)."/".substr($billing_date,6,2).",";
		$csv_data .= $debit_account_name.",".$debit_sub_account_name.",".$debit_section_name.",".$debit_sales_tax_status.",";
		$csv_data .= $total_price.",";
		$csv_data .= "0,";
		$csv_data .= $credit_account_name.",".$credit_sub_account_name.",".$credit_section_name.",".$credit_sales_tax_status.",";
		$csv_data .= $credit_amount_of_money.",0,";
		$csv_data .= $remarks.",,,";
		if ($num_box == 0) {
			$csv_data .= "0,";
		} else {
			$csv_data .= "3,";
		}
		$csv_data .= ",,0,0,no";
		
		
	$csv_data .= "\n";
	//echo $num_box;
	//if ($num_box >= 1) {
		for ($k = 1;$k <= $num_box;$k++) {
			
			$debit_section_code = $journalized_history_arr[$i]['debit_section_code'.$k];
			$debit_section_name = $journalized_history_arr[$i]['debit_section_name'.$k];
			$debit_account_code = $journalized_history_arr[$i]['debit_account_code'.$k];
			$debit_account_name = $journalized_history_arr[$i]['debit_account_name'.$k];
			$debit_sub_account_code = $journalized_history_arr[$i]['debit_sub_account_code'.$k];
			$debit_sub_account_name = $journalized_history_arr[$i]['debit_sub_account_name'.$k];
			$debit_ratio = $journalized_history_arr[$i]['debit_ratio'.$k];
			$debit = $journalized_history_arr[$i]['debit'.$k];
			$debit_tax = $journalized_history_arr[$i]['debit_tax'.$k];
			$debit_flag1 = $journalized_history_arr[$i]['debit_flag'.$k.'_1'];
			$debit_flag2 = $journalized_history_arr[$i]['debit_flag'.$k.'_2'];
			$debit_flag3 = $journalized_history_arr[$i]['debit_flag'.$k.'_3'];

			$credit_section_code = $journalized_history_arr[$i]['credit_section_code'.$k];
			$credit_section_name = $journalized_history_arr[$i]['credit_section_name'.$k];
			$credit_account_code = $journalized_history_arr[$i]['credit_account_code'.$k];
			$credit_account_name = $journalized_history_arr[$i]['credit_account_name'.$k];
			$credit_sub_account_code = $journalized_history_arr[$i]['credit_sub_account_code'.$k];
			$credit_sub_account_name = $journalized_history_arr[$i]['credit_sub_account_name'.$k];
			$credit_ratio = $journalized_history_arr[$i]['credit_ratio'.$k];
			$credit = $journalized_history_arr[$i]['credit'.$k];
			$credit_tax = $journalized_history_arr[$i]['credit_tax'.$k];
			$remarks = $journalized_history_arr[$i]['remarks'.$k];
			$credit_flag1 = $journalized_history_arr[$i]['credit_flag'.$k.'_1'];
			$credit_flag2 = $journalized_history_arr[$i]['credit_flag'.$k.'_2'];
			$credit_flag3 = $journalized_history_arr[$i]['credit_flag'.$k.'_3'];
			
			
			
			//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
			$debit_sales_tax_status = $tax_class_arr[$debit_flag1];
			$credit_sales_tax_status = $tax_class_arr[$credit_flag1];
			$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
			$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
			if ($k == $num_box) {
				$csv_data .= "2101,";
			} else {
				$csv_data .= "2100,";
			}
			$csv_data .= ",,";
			$csv_data .= str_replace(" ","",substr($billing_date,0,4)."/".substr($billing_date,4,2)."/".substr($billing_date,6,2)).",";
			$csv_data .= $debit_account_name.",".$debit_sub_account_name.",".$debit_section_name.",".$debit_sales_tax_status.",";
			$csv_data .= $debit.",";
			$csv_data .= "0,";//$debit_tax.",";
			$csv_data .= $credit_account_name.",".$credit_sub_account_name.",".$credit_section_name.",".$credit_sales_tax_status.",";
			$csv_data .= $credit.",";
			$csv_data .= "0,";//$credit_tax.",";
			$csv_data .= $remarks.",,,";
			$csv_data .= "3,";
			$csv_data .= ",,0,0,no";
			$csv_data .= "\n";
		}
	//}

	/*****ここから旧CI形式
	//incioce_code_checkerとこの行の請求書番号が一致しなければ
	//新しい請求書と判断しヘッダーをつける。
	if ($invoice_code_checker != $invoice_code || $claimant_id_checker != $claimant_id) {
		
		$csv_data .= "*,";
		$csv_data .= $claimant_id.",";
		$csv_data .= $company_name.",";
		$csv_data .= $billing_date.",";
		$csv_data .= $invoice_code.",";
		$csv_data .= ",";
		$csv_data .= $staff_name.",";
		$csv_data .= $pay_date."\n";
		
	}
	if ($sales_tax == 0) { $debit_sales_tax_status = "対象外"; } else if ($sales_tax * 1 > 0) { $debit_sales_tax_status = "課対仕入込8%";}
	if ($credit_sales_tax == 0) { $credit_sales_tax_status = "対象外";} else if ($credit_sales_tax * 1 > 0) { $credit_sales_tax_status = "課対仕入込8%";}

	$csv_data .= ",,".$debit_account_name.",".$debit_sub_account_name.",".$debit_section_name.",".$debit_sales_tax_status.",";
	$csv_data .= $total_price.",";
	$csv_data .= $sales_tax.",".$credit_account_name.",".$credit_sub_account_name.",".$credit_section_name.",".$credit_sales_tax_status.",".$credit_amount_of_money.",".$credit_sales_tax.",".$remarks.",,";
	if ($invoice_code_checker != $invoice_code || $claimant_id_checker != $claimant_id) {
		//明細の最初の行だけに請求書の合計金額を表示する。
		$invoice_code_checker = $invoice_code;
		$claimant_id_checker = $claimant_id;
		$flag = "invoice_get_invoice_total";
		$get_total_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$invoice_code);
		$csv_data .= $get_total_arr[0]["sum(total_price)"];

		//download_passwordが一致するINVOICE_DATA_TABLEの請求書データを仕訳済み(journalized = 1)にする。
		$flag = "up_invoice_download_password";
		$words = "`journalized` = 1 WHERE `claimant_id` = ".$claimant_id." AND `download_password` = '".$_POST['account_table_dlp'.$i]."';";
		$invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);

	}
	$csv_data .= ",,";
	$csv_data .= $invoice_data_arr[$i]["withholding_tax"].",";
	if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
		$csv_data .= $invoice_data_arr[$i]['sum(total_price)'].","; 
	} else { 
		$csv_data .= $invoice_data_arr[$i]["total_price"].",";
	}
	//$csv_data .= $remarks.",";
	//$csv_data .= $product_name."\n";
	$csv_data .= "\n";

	$flag1=1;
	$num_box = 0;
	if ($flag1 >= 1) {
		for ($j=1;$j < 10; $j++){
			if ($_POST["account_table".$i."_".$j."1"] != NULL){
				$num_box = $j;
			}
		}
		
			for ($op_num = 0;$op_num < 128;$op_num++) {
				if ($tax_class_arr[$op_num] != "") {
					if($op_num == $debit_tax_num){$debit_select_flag = "selected"; } else {$debit_select_flag = "";}
					echo '<option value="'.$op_num.'" '.$debit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
				}
			}

		
		for ($k = 1;$k <= $num_box;$k++) {
			if ($_POST["account_table".$i."_".$k."3"] * 1  == 0) { $debit_sales_tax_status = "対象外"; } else if ($_POST["account_table".$i."_".$k."3"] * 1 > 0) { $debit_sales_tax_status = "課対仕入込8%";}
			if ($_POST["account_table".$i."_".$k."6"] * 1  == 0) { $credit_sales_tax_status = "対象外";} else if ($_POST["account_table".$i."_".$k."6"] * 1 > 0) { $credit_sales_tax_status = "課税売上込8%";}
			$csv_data .= ",,";
			$csv_data .= $_POST["account_table".$i."_".$k."1"].",".$_POST["account_table".$i."_".$k."8"].",".$_POST["account_table".$i."_".$k."9"].",".$debit_sales_tax_status.",";
			$csv_data .= $_POST["account_table".$i."_".$k."2"].",";
			$csv_data .= $_POST["account_table".$i."_".$k."3"].",";
			$csv_data .= $_POST["account_table".$i."_".$k."4"].",".$_POST["account_table".$i."_".$k."10"].",".$_POST["account_table".$i."_".$k."11"].",".$credit_sales_tax_status.",";
			$csv_data .= $_POST["account_table".$i."_".$k."5"].",";
			$csv_data .= $_POST["account_table".$i."_".$k."6"].",";
			$csv_data .= $_POST["account_table".$i."_".$k."7"].",,,,,,";
			//$csv_data .= $_POST["account_table".$i."_".$k."7"];
			//$csv_data .= $product_name."\n";
			$csv_data .= "\n";
		}
	}
	ここまで旧CI形式*****/
}

	//出力ファイル名の作成
	$csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';

	//文字化けを防ぐ
	$csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );

	//MIMEタイプの設定
	header("Content-Type: application/octet-stream");
	//名前を付けて保存のダイアログボックスのファイル名の初期値
	header("Content-Disposition: attachment; filename={$csv_file}");

	// データの出力
	echo($csv_data);
	exit();

}


?>

<?php require("header.php");?>

<script type="text/javascript">



//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
	//alert(pst);alert(did);alert(cid);alert(icd);
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
//	if (num == 1) {
		var data = { get_payment_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
/*	} else if (num == 2) {
		var data = { get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
*/
//		if (num == 1) {
			send_url = "send_pastjournal";
/*		} else if (num == 2) {
			send_url = "send_send_code";
		}
*/
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される
			
			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			alert(data);
/*
			var address_data = data.split(",");
			//alert(document.getElementById('bikou').value);
			for (i = 0 ;i <= 5;i++) {
				//alert(address_data[i]);
				document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
			}
			//alert(address_data);
			document.getElementById('destination_email').value = address_data[7];
			document.getElementById('Kyoutu_No').value = address_data[0];
			document.getElementById('company_info').value = address_data[6];
			document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
			if (num == 1) {
				document.getElementById('addressee_company_info').value = "";
			}
			//onLoadData();
			//document.write(data);
*/
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}
</script>


<script>


$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});


</script>


<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>


<title>
<?php
if ( $_POST['mode'] === 'download' ) {
	echo "仕訳帳検索 - Cloud Invoice";
} else {
	echo "仕訳帳 - Cloud Invoice";
}
?>
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article>

	<section id="m-1-box">
		<h2>
		<?php
		if ( $_REQUEST['mode'] === 'download') {
			echo "仕訳帳";
		} else {
			echo "仕訳帳";
		}
		?>
		</h2>
<!--
<?php
		//echo	$_GET['search_flag'];
//	if ( $_REQUEST['mode'] != 'download') {
?>



		<p>
			条件を指定してください。
		</p>
		<form name="paymentcsv_form" action="" method="get">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" id="Kyoutu_No" class="kCommonSearch" name="destination_id" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame();" value="" placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
			</div>
			
			<div id="hiduke">
				<input type="hidden" id="2" name="koumoku"value="1">
			</div>
			<div id="hiduke">
				<h3>支払日付</h3>
				<input id="box3" name="paydate_from" type="text" />
				<p>(yyyymmdd) - </p>
				<input id="box4" name="paydate_to"type="text" />
				<p>(yyyymmdd)</p>
			</div>

			<div id="shiharaEx">
				<input type="submit" value="過去の仕訳を見る"/>
				<input type="hidden" name="company" value="<?php echo $_SESSION['user_id'];?>">
		        <input type="hidden" name="mode" value="download">
			</div>
		</form>
	</section>
</article>
<?php
//	}
?>
-->
<?php
//echo $page_con -> create_links();
?>
			<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<div class="hiduke">
					<p>取引日付</p>
					<input type="text" id="box1" name="paydate_from" />
					～
					<input type="text" id="box2" name="paydate_to" />
				</div>
				<div class="kamoku">
					<p>借方勘定科目</p>
					<input type="text" name="debit_account_name" />
				</div>
				<div class="hojo">
					<p>借方補助科目</p>
					<input type="text" name="debit_sub_account_name" />
				</div>
				<div class="bumon">
					<p>借方部門</p>
					<input type="text" name="debit_section_name" />
				</div>
				<div class="kamoku">
					<p>貸方勘定科目</p>
					<input type="text" name="credit_account_name" />
				</div>
				<div class="hojo">
					<p>貸方補助科目</p>
					<input type="text" name="credit_sub_account_name" />
				</div>
				<div class="bumon">
					<p>貸方部門</p>
					<input type="text" name="credit_section_name" />
				</div>
				<div class="tekiyou">
					<p>摘要</p>
					<input type="text" name="remarks" />
				</div>
				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
				</div>
				</form>
			</section>

		<form action="" method="post" name="download" id="journal_form" value="1">
		<?php
		$count_data_num = count($journalized_history_arr);
		
		if ( $count_data_num > 0) {
		//echo '<input type="button" onclick="submit()" value="CSVで出力する" /><br/><br/>';
			$invoice_code_checker = "";
			$claimant_id_checker = "";
		?>
			
			<div class="SearchLine"></div>
			
			<table id="pastjournal">
				<thead>
				<tr>
					<!--<th class="first"><label id="checklabel"><input type="checkbox" /></label></th>-->
					<th class="min2">仕訳No</th>
					<th class="hiduke">取引日付</th>
					<th class="kamoku">借方科目</th>
					<th class="kamoku">補助科目</th>
					<th class="kamoku">部門</th>
					<th class="kingaku">金額</th>
					<th class="zei">消費税区分</th>
					<th class="kamoku">貸方科目</th>
					<th class="kamoku">補助科目</th>
					<th class="kamoku">部門</th>
					<th class="kingaku">金額</th>
					<th class="zei">消費税区分</th>
					<th class="long">摘要</th>
					<th class="min">画像</th>
					<!--<th class="last">編集</th>-->
				</tr>
				</thead>
			</table>
		<?php 
			$k = 0;//ヘッダーの番号を覚えておく変数
			for ( $i = 0; $i < intval($count_data_num); $i++ ) {
				//var_dump($invoice_data_arr[$i]);
//				if ($invoice_code_checker != $journalized_history_arr[$i]['invoice_code'] || $claimant_id_checker != $journalized_history_arr[$i]["claimant_id"]) {
					echo $invoice_code_checker = $journalized_history_arr[$i]['text_data1'];
					echo $claimant_id_checker = $journalized_history_arr[$i]["company"];
					if ($i > 0) {
						echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
						$invoice_total = 0;
						$k = $i;
						//echo "
						//	</div>
						//	";
					}
					$withholding_tax = $journalized_history_arr[$i]['withholding_tax'];
					$withholding_tax_flag = $withholding_tax * 1;
					$debit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['debit_flag1']];
					$credit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['credit_flag1']];
					$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
					$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
		?>

			<table id="pastjournal2">
				<tr>
					<!--<td class="first"><label id="checklabel"><input type="checkbox" value="1" /></label></td>-->
					<td class="min2"><?php if ($journalized_history_arr[$i]["text_data3"] != "") {echo $journalized_history_arr[$i]["text_data3"];} else{ echo "(".$journalized_history_arr[$i]["id"].")";}?></td>
					<td class="hiduke"><?php echo $journalized_history_arr[$i]["paid_date"];?></td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_account_name'];?></td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_sub_account_name'];?></td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_section_name'];?></td>
					<td class="kingaku"><?php echo $journalized_history_arr[$i]['debit'];?></td>
					<td class="zei"><?php $journalized_history_arr[$i]['debit_tax'];?>
					<?php echo $debit_sales_tax_status;?>
					</td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_account_name'];?></td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_sub_account_name'];?></td>
					<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_section_name'];?></td>
					<td class="kingaku"><?php echo $journalized_history_arr[$i]['credit'];?></td>
					<td class="zei"><?php $journalized_history_arr[$i]['credit_tax'];?>
					<?php echo $credit_sales_tax_status;?>
					</td>
					<td class="long"><?php echo $journalized_history_arr[$i]['remarks'];?></td>
					<td class="min">
					<?php
					$journalized_history_arr[$i]['company_id'];
					$str = $journalized_history_arr[$i]["paid_date"];
					$pdf_url = '../files/' .substr($journalized_history_arr[$i]['company_id'],0,4).'/'.substr($journalized_history_arr[$i]['company_id'],4,4).'/'.$journalized_history_arr[$i]['company_id'] . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$journalized_history_arr[$i]['download_password'].".pdf";
					$img_url = '../files/' .substr($journalized_history_arr[$i]['company_id'],0,4).'/'.substr($journalized_history_arr[$i]['company_id'],4,4).'/'.$journalized_history_arr[$i]['company_id'] . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$journalized_history_arr[$i]['download_password'].".png";
					if (!file_exists($pdf_url)) {
					?>
						<a href="./invoice2pdf?dlp=<?php echo $journalized_history_arr[$i]['download_password'];?>&dlps=1" onclick="window.open( '<?php echo $img_url;?>', 'mywindow6', 'width=400, height=300, menubar=no, toolbar=no, scrollbars=yes'); return false;" target="new"><img src="../../images/icons/pdficon.png" /></a>
					<?php 
					} else if (file_exists($img_url)) {
						//"?pdf_dir=".$pdf_url."";
					?>
						<a href="<?php echo $img_url;?>" target='new'><span class="gray" style='color:#004b91;'>表示</span></a>
					<?php
					} else {
						$cond_find = '../pdf/'.$journalized_history_arr[$i]['download_password'].".pdf";
					?>
						<a href="<?php echo $pdf_url;?>" target='new'><span class="gray" style='color:#004b91;'>請求書を表示する</span></a>
					<?php
					}
					?>

					</td>
					<?php
					//echo $pdf_url = '../files/' . $journalized_history_arr[$i]['company_id'] . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$journalized_history_arr[$i]['download_password'].".pdf";
					//echo $img_url = '../files/' . $journalized_history_arr[$i]['company_id'] . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$journalized_history_arr[$i]['download_password'].".png";
					?>
					<!--
					<td class="last">
					
					
					<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
					<?php
						//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
						$flag = "company_data";
						if ($claimant_id != 0 ) {
							$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
						} else {}
					?>
					</td>
					-->
				</tr>
				<?php
				if (intval($journalized_history_arr[$i]["flag1"]) > 0) {
					for ($m = 1;$m < intval($journalized_history_arr[$i]["flag1"]);$m++) {
				?>
						<tr>
							<td class="min2"></td>
							<td class="hiduke"></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_sub_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_section_name'.$m];?></td>
							<td class="kingaku"><?php echo $journalized_history_arr[$i]['debit'.$m];?></td>
							<td class="zei"><?php echo $journalized_history_arr[$i]['debit_tax'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_sub_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_section_name'.$m];?></td>
							<td class="kingaku"><?php echo $journalized_history_arr[$i]['credit'.$m];?></td>
							<td class="zei"><?php echo $journalized_history_arr[$i]['credit_tax'.$m];?></td>
							<td class="long"><?php echo $journalized_history_arr[$i]['remarks'.$m];?></td>
							
							<td class="min"></td>
							<!--
							<td class="last">
							<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
							<?php
								//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
								$flag = "company_data";
								if ($claimant_id != 0 ) {
									$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
								} else {}
							?>
							

							</td>
							-->
						</tr>

				
				
				<?php
					}
					
					if ($withholding_tax_flag > 0) {
						if ($claimant_id == $company_id) {
							$debit_a_name = '仮払税金';
							$debit_sa_name = '源泉所得税';
						} else {
							$credit_a_name = '預り金';
							$credit_sa_name = '源泉所得税';
						}
				?>
						<tr>
							<td class="min2"></td>
							<td class="hiduke"></td>
							<td class="kamoku"><?php echo $debit_a_name;?></td>
							<td class="kamoku"><?php echo $debit_sa_name;?></td>
							<td class="kamoku"><?php?></td>
							<td class="kingaku"><?php?></td>
							<td class="zei"><?php?></td>
							<td class="kamoku"><?php echo $credit_a_name;?></td>
							<td class="kamoku"><?php echo $credit_sa_name;?></td>
							<td class="kamoku"><?php?></td>
							<td class="kingaku"><?php?></td>
							<td class="zei"><?php?></td>
							<td class="long"><?php?></td>
							<td class="min"></td>
						</tr>
				<?php
					}
				}
				?>
			</table>
			<?php
//				}//ifの終わり
			}//forの終わり
			?>
			<br/>
			<input type="button" id="outcsv" onclick="submit()" value="CSVで出力する"/>
			<!--<input type="button" id="sba" onclick="sendByAjax('a','b','c','d')" value="Ajax出力" />-->
			<input type="hidden" name="csv" value="csv">
			<input type="hidden" name="mode" value="download">
			<?php
		}	
			?>
			<input type="hidden" id="data_num" name="data_num" value="<?php echo $count_data_num;?>" />
			<input type="hidden" id="aj_words" name="aj_words" value="<?php echo $aj_words;?>" />
		</form>
	</section>
<?php
	if ($page_num > 1) {
		echo '<div class="pageBtnBox"><a class="pageBtn" href="?page=1&sort_key='.$sort_key.'&sort_type='.$sort_type.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1"><<</a>';
		for($p = 1;$p <= $page_num;$p++) {
			$bc_class = "";//初期化
			if ($page == $p) {$bc_class = " chBackColor";}
			echo '<a class="pageBtn'.$bc_class.'" href="?page='.$p.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1">'.$p.'</a>';
		}
		echo '<a class="pageBtn" href="?page='.$page_num.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
	}
?>


</article>
<div id="fadeLayer" onclick="CloseFrame(12)"></div>
<div id="noneLayer"></div>




<?php require("footer.php");?>
