<?php require("header.php");?>
<?php

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/db_control.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

include_once(dirname(__FILE__).'/../webpay-php-full-2.1.1/webpay-php-full-2.1.1/autoload.php');
//require_once(dirname(__FILE__).'/../payment/webpay-php-2.1.1/vendor/autoload');
use WebPay\WebPay;
include_once(dirname(__FILE__).'/../payment/config.php');
// WebPayインスタンスを非公開
$webpay = new WebPay(SECRET_KEY);


//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$send_count = 0;//送信数の初期化
$status_num = 0;//ステータス変更用の変数初期化
$mail_send_count_num = 0;
$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];//現在のURLパラメーターつき
$company_id;
$words = "";
$flag = "";
$sql = "";
$direct_send_flag = 0;


//var_dump($_REQUEST);
$value = array();
//echo $_REQUEST["send_code"];

//▼直接送信処理用▼
if ($_REQUEST["direct_send"] == 1) {$direct_send_flag = 1;}
//▲直接送信処理用▲

//▼定期請求処理用▼
if ($_REQUEST["save_template"] != "") {
	$save_template_flag = htmlspecialchars($_REQUEST["save_template"],ENT_QUOTES);
	$template_name = htmlspecialchars($_REQUEST["template_name"],ENT_QUOTES);
	$template_cycle = htmlspecialchars($_REQUEST["template_cycle"],ENT_QUOTES);
	$reservation_date = htmlspecialchars($_REQUEST["reservation_date"],ENT_QUOTES);
	$template_maturity_cycle = htmlspecialchars($_REQUEST["template_maturity_cycle"],ENT_QUOTES);
	$template_maturity_date = htmlspecialchars($_REQUEST["template_maturity_date"],ENT_QUOTES);
	$start_date = htmlspecialchars($_REQUEST["start_date"],ENT_QUOTES);
	$end_date = htmlspecialchars($_REQUEST["end_date"],ENT_QUOTES);
	$next_creation_date = htmlspecialchars($_REQUEST["next_creation_date"],ENT_QUOTES);
	$next_payment_date = htmlspecialchars($_REQUEST["next_payment_date"],ENT_QUOTES);
}
//▲定期請求処理用▲

if ($save_template_flag == 1) {
	//定期請求処理
	if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $_REQUEST["destination_id"] != "" || $_REQUEST["send_code"] != "")) {
		if (isset($_REQUEST["seikyu_10"])) { $value[7] = htmlspecialchars($_REQUEST["seikyu_10"],ENT_QUOTES); }//振込先
		if (isset($_REQUEST["seikyu_11"])) { $value[18] = htmlspecialchars($_REQUEST["seikyu_11"],ENT_QUOTES); }//備考消すかも
		if (isset($_REQUEST["destination_id"])) { $value[0] = htmlspecialchars($_REQUEST["destination_id"],ENT_QUOTES); }//共通コード
		if (isset($_REQUEST["send_code"])) { $value[1] = htmlspecialchars($_REQUEST["send_code"],ENT_QUOTES); }//共通コード
		//if (isset($_REQUEST["invoice_code"])) { $value[3] = htmlspecialchars($_REQUEST["invoice_code"],ENT_QUOTES); }
		if (isset($_REQUEST["next_creation_date"])) { $value[2] = htmlspecialchars($_REQUEST["next_creation_date"],ENT_QUOTES); }
		//if (isset($_REQUEST["pay_date"])) { $value[6] = htmlspecialchars($_REQUEST["pay_date"],ENT_QUOTES); }
		if (isset($_REQUEST["template_type"])) { $value[24] = htmlspecialchars($_REQUEST["template_type"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_name"])) { $value[7] = htmlspecialchars($_REQUEST["destination_name"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_zip_code"])) { $value[7] = htmlspecialchars($_REQUEST["destination_zip_code"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address2"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address2"],ENT_QUOTES); }
		if (isset($_REQUEST["destination_clerk"])) { $value[5] = htmlspecialchars($_REQUEST["destination_clerk"],ENT_QUOTES).htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }

//		if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $value[0] != "" || $value[1] != "")) {

			//▼ユーザーの現在の定期請求数を取得し、定期請求テンプレート番号を取得する▼
			$sql = "SELECT max(`template_id`) FROM `REGULAR_INVOICE_FORM_TABLE` WHERE `claimant_id` = ".$company_id."";
			$invoice_template_num_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
			$sql = "";
			//var_dump($invoice_invoice_auto_num_arr);
			$invoice_template_id = 1 + $invoice_template_num_arr[0]['max(`template_id`)'];
			//▲ユーザーの現在の定期請求数を取得する定期請求テンプレート番号を取得する▲

			//ループ回数の取得
			if (isset($_REQUEST["roop_times"])) { $roop_times = htmlspecialchars($_REQUEST["roop_times"],ENT_QUOTES);}
			$roop_times = 10;//仮置き
			//ループして明細データを取得する
			for ( $i = 1 ; $i <= $roop_times ; $i++ ) {
				if ($_REQUEST["seikyu_1".$i."2"] != "") {
					if (isset($_REQUEST["seikyu_1".$i."1"])) { $value[10] = htmlspecialchars($_REQUEST["seikyu_1".$i."1"],ENT_QUOTES); } else { $value[10] = "";}//品目
					if (isset($_REQUEST["seikyu_1".$i."2"])) { $value[11] = htmlspecialchars($_REQUEST["seikyu_1".$i."2"],ENT_QUOTES); } else { $value[11] = 0;}//単価
					if (isset($_REQUEST["seikyu_1".$i."3"])) { $value[12] = htmlspecialchars($_REQUEST["seikyu_1".$i."3"],ENT_QUOTES); } else { $value[12] = 0;}//数量
					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[14] = htmlspecialchars($_REQUEST["seikyu_1".$i."4"],ENT_QUOTES); } else { $value[14] = 0;}//金額

					//消費税と源泉税の有無を判定する
					if (isset($_REQUEST["seikyu_1".$i."6"])) { $sales_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."6"],ENT_QUOTES);} else { $sales_tax = 1;}
					if (isset($_REQUEST["seikyu_1".$i."7"])) { $withholding_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."7"],ENT_QUOTES);} else { $withholding_tax = 0;}
					
					if ($sales_tax == 1) {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = round($value[14] * 8/100,0);}//消費税
					} else {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = 0;}//消費税
					}
					
					//明細の小計額と消費税の合計を計算
					$detail_subtotal = $value[14] + $value[15];
					
					if ($withholding_tax == 1) {
						//源泉所得税がかかる場合
						if ($detail_subtotal <= 1000000) {
							//源泉所得税が100万円以下の場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round($detail_subtotal * 1021/10000,0);}//源泉所得税
						} else {
							//源泉所得税が100万円を超える場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round((102100 + (($detail_subtotal - 1000000) * 2042/10000)),0);}//源泉所得税
						}
					} else {
						//源泉所得税がかからない場合
						$value[16] = 0;//源泉所得税
					}

					if (isset($_REQUEST["seikyu_1".$i."5"])) { $remarks_invoice = htmlspecialchars($_REQUEST["seikyu_1".$i."5"],ENT_QUOTES); }//備考もしくは源泉所得税//現在は未使用。とりあえず置いておく。

						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[17] = round(($detail_subtotal - $value[16]),0);}//合計金額

				
					$sql = "
						INSERT INTO `REGULAR_INVOICE_FORM_TABLE` (
						`claimant_id`,`destination_id`, `send_code`, `billing_date`, `invoice_code`,
						`invoice_name`, `staff_name`, `pay_date`,
						`bank_account`, `sale_date`, `product_code`,
						`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
						`sales_tax`, `withholding_tax`, `total_price`,
						`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`, `template_type`, `template_id`, `template_name`,`template_cycle`, `reservation_date`, `template_maturity_cycle`, `template_maturity_date`, `start_date`, `end_date`, `next_creation_date`, `next_payment_date`, `insert_date`
						) VALUES 
						( ".intval($company_id).", ".intval($value[0]).", '".str_replace(',','',$value[1])."', ".intval(str_replace(array(',','/','-'),'','20150101')).", '".$value[3]."',
						'".$value[4]."', '".$value[5]."', ".intval(str_replace(array(',','/','-'),'',$value[6])).",
						'".$value[7]."', ".intval(str_replace(array(',','/','-'),'',$value[8])).", '".$value[9]."',
						'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval($value[12]).", '".$value[13]."',".intval(str_replace(',','',$value[14])).",
						'".$value[15]."', '".$value[16]."', ".intval(str_replace(',','',$value[17])).",
						'".$value[18]."', '".$value[19]."', '".$value[20]."', '', '', ".$value[24].", ".$invoice_template_id.", '".$template_name."', ".$template_cycle.", '".$reservation_date."', ".$template_maturity_cycle.", ".$template_maturity_date.", ".$start_date.", ".$end_date.", '".$next_creation_date."', '".$next_payment_date."', cast( now() as datetime)
						)";echo "\r\n";
					$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
				}
			}
//		}
		//データの登録が成功したらメッセージを表示する
		if ($check_arr['chk']['check']){
			
			$_SESSION['up_info_msg'] = "1件の請求書を登録しました";
			//echo "<script type='text/javascript'>location.href='./intervals';</script>";
		} else {
			echo "<script type='text/javascript'>alert('入力不備のため定期請求書に登録できませんでした');</script>";
			//echo "<script type='text/javascript'>location.href='./makeinvoice';</script>";
			//exit();
		}

	}
	
} else {
	//通常の請求書作成処理
	if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $_REQUEST["destination_id"] != "" || $_REQUEST["send_code"] != "")) {
		if (isset($_REQUEST["seikyu_10"])) { $value[7] = htmlspecialchars($_REQUEST["seikyu_10"],ENT_QUOTES); }//振込先
		if (isset($_REQUEST["seikyu_11"])) { $value[18] = htmlspecialchars($_REQUEST["seikyu_11"],ENT_QUOTES); }//備考消すかも
		if (isset($_REQUEST["destination_id"])) { $destination_id= $value[0] = htmlspecialchars($_REQUEST["destination_id"],ENT_QUOTES); }//共通コード
		if (isset($_REQUEST["send_code"])) { $send_code = $value[1] = htmlspecialchars($_REQUEST["send_code"],ENT_QUOTES); }//共通コード
		if (isset($_REQUEST["invoice_code"])) { $invoice_code = $value[3] = htmlspecialchars($_REQUEST["invoice_code"],ENT_QUOTES); }
		if (isset($_REQUEST["billing_date"])) { $billing_date = $value[2] = htmlspecialchars($_REQUEST["billing_date"],ENT_QUOTES); }
		if (isset($_REQUEST["pay_date"])) { $pay_date = $value[6] = htmlspecialchars($_REQUEST["pay_date"],ENT_QUOTES); }
		if (isset($_REQUEST["template_type"])) { $value[24] = htmlspecialchars($_REQUEST["template_type"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_name"])) { $value[7] = htmlspecialchars($_REQUEST["destination_name"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_zip_code"])) { $value[7] = htmlspecialchars($_REQUEST["destination_zip_code"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address2"])) { $value[20] = htmlspecialchars($_REQUEST["destination_address2"],ENT_QUOTES); }
		if (isset($_REQUEST["destination_clerk"])) { $value[5] = htmlspecialchars($_REQUEST["destination_clerk"],ENT_QUOTES).htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }

		$sql = "SELECT `invoice_code` 
		FROM `INVOICE_DATA_TABLE` WHERE claimant_id = ".$company_id." 
		AND `invoice_code` = '".$value[3]."'
		";
		//echo "\r\n";

		$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
		if ($invoice_code_check[0] != NULL) {
			//$_SESSION['up_info_msg'] = "請求書番号が重複しているのでデータを登録できません。";
			echo "<script type='text/javascript'>alert('請求書番号が重複しているのでデータを登録できません。');location.href='./makeinvoice';</script>";
			//header("Location:./makeinvoice", false);
			exit();
		} else if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $value[0] != "" || $value[1] != "")) {

			//ループ回数の取得
			if (isset($_REQUEST["roop_times"])) { $roop_times = htmlspecialchars($_REQUEST["roop_times"],ENT_QUOTES);}
			$roop_times = 10;//仮置き
			//ループして明細データを取得する
			for ( $i = 1 ; $i <= $roop_times ; $i++ ) {
				if ($_REQUEST["seikyu_1".$i."2"] != "") {
					if (isset($_REQUEST["seikyu_1".$i."1"])) { $value[10] = htmlspecialchars($_REQUEST["seikyu_1".$i."1"],ENT_QUOTES); } else { $value[10] = "";}//品目
					if (isset($_REQUEST["seikyu_1".$i."2"])) { $value[11] = htmlspecialchars($_REQUEST["seikyu_1".$i."2"],ENT_QUOTES); } else { $value[11] = 0;}//単価
					if (isset($_REQUEST["seikyu_1".$i."3"])) { $value[12] = htmlspecialchars($_REQUEST["seikyu_1".$i."3"],ENT_QUOTES); } else { $value[12] = 0;}//数量
					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[14] = htmlspecialchars($_REQUEST["seikyu_1".$i."4"],ENT_QUOTES); } else { $value[14] = 0;}//金額

					//消費税と源泉税の有無を判定する
					if (isset($_REQUEST["seikyu_1".$i."6"])) { $sales_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."6"],ENT_QUOTES);} else { $sales_tax = 1;}
					if (isset($_REQUEST["seikyu_1".$i."7"])) { $withholding_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."7"],ENT_QUOTES);} else { $withholding_tax = 0;}
					
					if ($sales_tax == 1) {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = round($value[14] * 8/100,0);}//消費税
					} else {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = 0;}//消費税
					}
					
					//明細の小計額と消費税の合計を計算
					$detail_subtotal = $value[14] + $value[15];
					
					if ($withholding_tax == 1) {
						//源泉所得税がかかる場合
						if ($detail_subtotal <= 1000000) {
							//源泉所得税が100万円以下の場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round($detail_subtotal * 1021/10000,0);}//源泉所得税
						} else {
							//源泉所得税が100万円を超える場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round((102100 + (($detail_subtotal - 1000000) * 2042/10000)),0);}//源泉所得税
						}
					} else {
						//源泉所得税がかからない場合
						$value[16] = 0;//源泉所得税
					}

					if (isset($_REQUEST["seikyu_1".$i."5"])) { $remarks_invoice = htmlspecialchars($_REQUEST["seikyu_1".$i."5"],ENT_QUOTES); }//備考もしくは源泉所得税//現在は未使用。とりあえず置いておく。

					//合計金額の計算
	//				if ($sales_tax == 1 && $withholding_tax == 0) {
						//消費税のみの場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round($detail_subtotal,0); }//合計金額
	//				} else if ($sales_tax == 1 && $withholding_tax == 1) {
						//消費税+源泉税の場合
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[17] = round(($detail_subtotal - $value[16]),0);}//合計金額
	//				} else if ($sales_tax == 0 && $withholding_tax == 1) {
						//源泉所得税のみの場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round(($value[14] - $value[16]),0); }//合計金額
	//				} else if ($sales_tax == 0 && $withholding_tax == 0) {
						//税金を引かない場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round($value[14],0); }//合計金額
	//				}

				
					$sql = "
						INSERT INTO `INVOICE_DATA_TABLE` (
						`claimant_id`,`destination_id`, `send_code`, `billing_date`, `invoice_code`,
						`invoice_name`, `staff_name`, `pay_date`,
						`bank_account`, `sale_date`, `product_code`,
						`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
						`sales_tax`, `withholding_tax`, `total_price`,
						`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`, `template_type`,`insert_date`
						) VALUES 
						( ".intval($company_id).", ".intval($value[0]).", '".str_replace(',','',$value[1])."', ".intval(str_replace(array(',','/','-'),'',$value[2])).", '".$value[3]."',
						'".$value[4]."', '".$value[5]."', ".intval(str_replace(array(',','/','-'),'',$value[6])).",
						'".$value[7]."', ".intval(str_replace(array(',','/','-'),'',$value[8])).", '".$value[9]."',
						'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval($value[12]).", '".$value[13]."',".intval(str_replace(',','',$value[14])).",
						'".$value[15]."', '".$value[16]."', ".intval(str_replace(',','',$value[17])).",
						'".$value[18]."', '".$value[19]."', '".$value[20]."', '', '', ".$value[24].", cast( now() as datetime)
						)";echo "\r\n";

					$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
				}
			}
			$flag = "image_upload";
			$conditions = " `template_type` = ".$value[24]." ";
			$company_con -> company_sql_flag($pdo,$flag,$company_id,$conditions);
		}
		//データの登録が成功したらメッセージを表示する
		if ($check_arr['chk']['check']){
			
			//直接送信フラグが1の場合はそのままメール送信する。
			if ($direct_send_flag == 1) {
				
				//自社企業データの取得▼
				if ($company_id != "") {
					$flag = "company_data";
					$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					$company_self_name = $company_self_arr[0]['company_name'];
					$company_self_payment_id = $company_self_arr[0]['payment_id'];
					$company_self_plan_status = $company_self_arr[0]['plan_status'];
				}
				//$timestamp_for_code = date("YmdHis");//ダウンロードパスワードを生成するのに使用したが、毎回変わるのでメールと郵送など送るごとに変わってしまうため一旦はずした。
				$download_password = md5($company_id."-".$destination_id."-".$invoice_code);
				$flag = "up_invoice_download_password";
				$words = " `download_password` = '".$download_password."' WHERE (`destination_id` = ".$destination_id." OR `send_code` = '".$send_code."') AND `claimant_id` = '".$company_id."' AND billing_date = ".$billing_date." AND invoice_code = '".$invoice_code."' AND pay_date = ".$pay_date." AND `status` <> 99 ";
				$invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
				//送付先企業データの取得1▼
				if ($destination_id != "" && $destination_id != 0) {
					//echo "send_code なし<br/>";
					$flag = "company_data";
					$words = "";
					
					$company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
					//var_dump($company_arr);
					//echo $company_arr[0]["email"];
					$company_email = $company_arr[0]["email"];
					$company_name = $company_arr[0]["company_name"];
				}
				//echo $send_code."センド<br/>";
				//送付先企業データの取得2▼
				if ($send_code != "" || $send_code != NULL) {
					//echo "send_code あり<br/>";
					$flag = "addressee_data_one";
					$conditions = " AND company_id = ".$company_id." ";
					$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
					//var_dump($company_send_code_check_arr);
					if ($company_send_code_check_arr[0]["email"] != NULL && $company_send_code_check_arr[0]["email"] != ""){
						$company_email = $company_arr[0]["email"];
						$company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
						$company_email .= ",".$company_arr[0]["email"];
						$company_name = $company_arr[0]["company_name"];
					}
					//var_dump($company_arr);
					//echo $company_arr[0]["email"];
				}


				$flag = "invoice_total_data_send_count";
				$words = "'".$invoice_code."' ORDER BY `insert_date` ASC ";//条件
				$invoice_data_receive_arr_count = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
				//echo "<br/><br/>test<br/><br/>";
				//var_dump($invoice_data_receive_arr);
				//送付先企業データの取得▲

				//メールアドレス
				$mailto = $company_email;
				//メールに渡す情報
				$download_password;
				$keywords = crypt(md5($mailto.$timestamp_for_code.$download_password));
				$headers = "From: CloudInvoice <info@co.cloudinvoice.co.jp>\r\n";
				//メールアドレス
		
$message = "".$company_name."様


".$company_self_name."様から請求書が届いております。

下記のURLにアクセスし、請求書PDFのダウンロードをお願いします。


https://co.cloudinvoice.co.jp/invoice/pdf_download.php?&key=".$keywords."&det=".$detailed_statement." 
ダウンロードパスワードは、「".$download_password."」です。

".$insert_sentence.
"

------------------------------
Cloud Invoice（クラウドインボイス）は、無料で使える請求書発行・情報管理サービスです。

CSVデータの取り込みによってクラウド上で、売上請求書の自動作成、
売上請求書のメール（有料で郵送も）、売上請求書の管理、支払請求書の受取、
支払請求書の管理ができるクラウド請求書発行・請求書情報管理サービスです。

クラウド上での請求書の発行・受け取り、請求データの相互ダウンロードなどで、
煩わしい経理業務の手間を軽減できます。

登録・通常機能は完全無料です。
お気軽にご登録、お試しでのご利用をお待ちしております。
サイトURL: http://co.cloudinvoice.co.jp

Cloud Invoice
------------------------------
";
				mb_internal_encoding("UTF-8");

				if ($send_type == 5) {
					//echo "郵送<br/>";
					//郵送
					$mailto .= ","."mail_send@co.cloudinvoice.co.jp";
					$status_num = 2;
					$charge_flag = 1;
				} else if ($send_type == 51) {
					//echo "ok51<br/>";
					//echo "FAX<br/>";
					$mailto .= ","."fax_send@co.cloudinvoice.co.jp";
				} else if ($send_type == 6) {
					//echo "ok6<br/>";
					//echo "PDF<br/>";
					$mailto = "pdf@co.cloudinvoice.co.jp ";
					$status_num = 3;
					//echo "PDFを発行しました。";
					echo "<script type='text/javascript'>window.open('https://co.cloudinvoice.co.jp/invoice/invoice2pdf?dlp=".$download_password."');</script>";
				} else if ($send_type == 7) {
					//echo "削除<br/>";
					//echo "ok7-1<br/>";
					if ($data_status == 0) {
						//echo "ok7-2<br/>";
						$testx = $invoice_con -> invoice_data_delete($pdo,$company_id,$destination_id,$send_code,$invoice_code);
						//var_dump(testx);
						$mailto = "delete_invoice_data";				
					} else {
						echo "<script type='text/javascript'>alert('請求番号「".$invoice_code."」は発行済みのため、削除できません')</script>";
						echo "<script type='text/javascript'>location.href='".$current_url."'</script>";
						exit();
					}
				} else {
					//echo "メール<br/>";
					//echo $mailto;
					$status_num = 1;
				}


				//メールを送信する
				$mail_result = mail(
					$mailto,
					mb_convert_encoding($company_self_name."様からの請求書が届いております。", 'ISO-2022-JP-MS'),
					mb_convert_encoding($message, 'ISO-2022-JP-MS'),
					$headers
				);

				if ($mail_result == true) {
					echo "mail_send".$status_num."<br/>\r\n";
					$status_num = 1;
					//echo $send_count++;
					$flag = "up_invoice_download_password";
					$update_words = " `status` = ".$status_num." WHERE `download_password` = '".$download_password."'";
					$invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$update_words);
					
					//▼自動仕訳用企業DB登録▼
					$table_name = $company_id;
					try {
						$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
					} catch (PDOException $e) {
					    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
					    $_SESSION['error_msg'] = $e->getMessage();
					}
					//▲自動仕訳用企業DB登録▲
					
					//▼自動仕訳の企業毎テーブル明細追加▼
					$flag = "download_invoice_data";
					$auto_words = $download_password;
					try {
						$invoice_details_data = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$auto_words);
					} catch (PDOException $e) {
					    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
					    $_SESSION['error_msg'] = $e->getMessage();
					}
					//▼自動仕訳用企業DB登録▼
					$table_name = $company_id;
					try {
						$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
					} catch (PDOException $e) {
					    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
					    $_SESSION['error_msg'] = $e->getMessage();
					}
					//▲自動仕訳用企業DB登録▲
					for ($q = 0; $q < count($invoice_details_data); $q++) {
						$flag = "auto_journal_data_product_name_condition";
						$table_name = $company_id;
						$aj_words = $invoice_details_data[$q]['product_name'];
						$aj_data_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
						$check_aj_flag = count($aj_data_arr);
						if ($check_aj_flag == 0) {
							$product_name = $aj_words;
							$auto_journalize_con -> insert_auto_journalize_normal($pdo_aj,$table_name,$product_name);
						}
					}
					//▲自動仕訳の企業毎テーブル明細追加▲
					
				} else {
					echo "<script type='text/javascript'>alert('請求番号「".$invoice_code."」の送信に失敗しました。');</script>";
				}
				//echo "pay<br/>\r\n";
				if ($charge_flag == 1) {
				//決済の実行
				try {
					
						if ($company_self_plan_status == 0) {
							$postage = 180;
						} else if ($company_self_plan_status == 1) {
							$postage = 160;
						} else if ($company_self_plan_status == 2) {
							$postage = 140;
						} else {
							$postage = 180;
						}
//echo "pay0<br/>\r\n";
						$amount = $send_count * $postage;
						$product_name = $company_self_name."様:郵送代行費".number_format($amount)."円(".$postage."円×".$send_count."通分)";
					    // 決済を実行
//echo "pay1<br/>\r\n";
					    $result = $webpay->charge->create(array(
					       "amount" => intval($amount, 10),
					       "currency" => "jpy",
					       "customer"=>$company_self_payment_id,
					       "description" => $product_name
					    ));
//echo "pay2<br/>\r\n";
					    //var_dump($result);
					    $change_plan_flag = $result->id;
					    if (isset($change_plan_flag)){
							date_default_timezone_set('Asia/Tokyo');
							$fp = fopen("/var/www/co.cloudinvoice.co.jp/html/files/".$company_id."/mailsendcharge".date('Ym').".log", "a");
							//$today = getdate();
							fwrite($fp, $company_id.",".$company_self_name.",".$company_self_plan_status.",".$amount.",".date("Ymd His")."\r\n");
							fclose($fp);
						}
//echo "pay3<br/>\r\n";
				// 以下エラーハンドリング
				} catch (\WebPay\Exception\CardException $e) {
				    // カードが拒否された場合
				    print("CardException\n");
				    print('Status is:' . $e->getStatus() . "\n");
				    print('Type is:' . $e->getType() . "\n");
				    print('Code is:' . $e->getCardErrorCode() . "\n");
				    print('Param is:' . $e->getParam() . "\n");
				    print('Message is:' . $e->getMessage() . "\n");
					Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				} catch (\WebPay\Exception\InvalidRequestException $e) {
				    // リクエストで指定したパラメータが不正な場合
				    print("InvalidRequestException\n");
				    print('Param is:' . $e->getParam() . "\n");
				    print('Message is:' . $e->getMessage() . "\n");
					Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				} catch (\WebPay\Exception\AuthenticationException $e) {
				    // 認証に失敗した場合
				    print("AuthenticationException\n");
				    print('Param is:' . $e->getParam() . "\n");
				    print('Message is:' . $e->getMessage() . "\n");
					Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				} catch (\WebPay\Exception\APIConnectionException $e) {
				    // APIへの接続エラーが起きた場合
				    print("APIConnectionException\n");
				    print('Param is:' . $e->getParam() . "\n");
				    print('Message is:' . $e->getMessage() . "\n");
					Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				} catch (\WebPay\Exception\APIException $e) {
				    // WebPayのサーバでエラーが起きた場合
				    print("APIException\n");
				    print('Message is:' . $e->getMessage() . "\n");
					Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				} catch (Exception $e) {
				    // WebPayとは関係ない例外の場合
				    print("Unexpected exception\n");
				    print('Message is:' . $e->getMessage() . "\n");
				    Header("Location: //co.cloudinvoice.co.jp/invoice/checkuriagekanri");
//				    exit('Error');
				}

				echo "<script type='text/javascript'>location.href='".$current_url."'</script>";
				exit();
				
			}
			}
			echo "<script type='text/javascript'>location.href='./checkuriagekanri';</script>";
		} else {
			echo "<script type='text/javascript'>alert('入力不備のため請求書を登録できませんでした');</script>";
			echo "<script type='text/javascript'>location.href='./makeinvoice';</script>";
			exit();
		}

	}
}

//送付先管理のデータ取得
if ($_REQUEST["get_destination_id"]) {
	$flag = "company_data";
	$destination_id_box = $_REQUEST["get_destination_id"];
	$destination_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id_box,$words);
	//var_dump($destination_data_arr);
	$check_destination_data = count($destination_data_arr);
	$flag="";
	$words="";
}
//送付先管理のデータ取得
$flag = "addressee_data_all_with_conditions";
$condition_words = "ORDER BY `company_name` ASC ";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$condition_words);
$flag="";
$words="";
//var_dump($company_arr);

if ($destination_data_arr[0]["company_id"] != 0 && isset($destination_data_arr[0]["company_id"])) {
	if (isset($destination_data_arr[0]["company_id"])) { $destination_company_id = $destination_data_arr[0]["company_id"];}
	if (isset($destination_data_arr[0]["company_name"])) { $destination_name = $destination_data_arr[0]["company_name"];}
	if (isset($destination_data_arr[0]["zip_code"])) { $destination_zip_code = $destination_data_arr[0]["zip_code"];}
	if (isset($destination_data_arr[0]["address1"])) { $destination_address = $destination_data_arr[0]["prefecture"].$destination_data_arr[0]["address1"];}
	if (isset($destination_data_arr[0]["address2"])) { $destination_address2 = $destination_data_arr[0]["address2"];}
	if (isset($destination_data_arr[0]["section"])) { $destination_clerk = $destination_data_arr[0]["section"]; }
	if (isset($destination_data_arr[0]["clerk"])) { $destination_clerk2 = $destination_data_arr[0]["clerk"]; }
}
if (isset($_REQUEST["get_client_id"]) && $_REQUEST["get_client_id"] != "") { 
	$get_client_id = htmlspecialchars($_REQUEST["get_client_id"],ENT_QUOTES);
	//一件の送付先管理のデータ取得
	$flag = "addressee_data_one";
	$words = "AND company_id = ".$company_id."";
	$client_company_arr = $company_con -> company_sql_flag($pdo,$flag,$get_client_id,$words);
	$flag = "";
	$words = "";
	//var_dump($client_company_arr);
}

if ($client_company_arr[0]["client_id"] != NULL && $client_company_arr[0]["client_id"] != "") {
	if (isset($client_company_arr[0]["client_id"])) { $destination_client_id = $client_company_arr[0]["client_id"];}
	if (isset($client_company_arr[0]["client_company_id"])) { $destination_company_id = $client_company_arr[0]["client_company_id"];}
	if (isset($client_company_arr[0]["company_name"])) { $destination_name = $client_company_arr[0]["company_name"];}
	if (isset($client_company_arr[0]["zip_code"])) { $destination_zip_code = $client_company_arr[0]["zip_code"];}
	if (isset($client_company_arr[0]["address1"])) { $destination_address = $client_company_arr[0]["prefecture"].$client_company_arr[0]["address1"];}
	if (isset($client_company_arr[0]["address2"])) { $destination_address2 = $client_company_arr[0]["address2"];}
	if (isset($client_company_arr[0]["position"])) { $destination_clerk = $client_company_arr[0]["department"].$client_company_arr[0]["position"]; }
	if (isset($client_company_arr[0]["addressee"])) { $destination_clerk2 = $client_company_arr[0]["addressee"]; }
}

//ユーザー企業の情報を取得する。
$flag = "company_data";
$words = "";
$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$flag="";
$words="";
$template_type = $company_one_arr[0]['template_type'];
//var_dump($company_one_arr);

//▼ユーザーの現在の総請求書数を取得する▼
$sql = "SELECT max(cast(`invoice_code` as unsigned)) FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$company_id." AND `invoice_code` LIKE '10".date('Ymd')."%' ORDER BY cast(`invoice_code` as unsigned) DESC";
$invoice_auto_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
$sql = "";
//var_dump($invoice_auto_arr);

if ($invoice_auto_arr[0]["max(cast(`invoice_code` as unsigned))"] == NULL) {
	$max_num = "10".date('Ymd')."000";
}else {
	$max_num = $invoice_auto_arr[0]["max(cast(`invoice_code` as unsigned))"];
}
$invoice_auto_num = 1 + intval($max_num);
//▲ユーザーの現在の総請求書数を取得する▲

?>

<script type="text/javascript">
	
function onLoadData() {
	for (var i = 1 ; i <= 15 ; i++ ) {
		Chen(i);
		if (i <= 10) {
				Calc("1"+i+"2");
				Chen("1"+i+"1");
				Chen("1"+i+"2");
				Chen("1"+i+"3");
				Chen("1"+i+"4");
				Chen("1"+i+"5");
				Chen("1"+i+"6");
				Chen("1"+i+"7");
		}
	}
	StartBgCalc();
}

function Chen(n) {
	try {
		if (n == 2 || n == 3) {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			if (Seikyu_No != "") {
				Seikyu_No = Seikyu_No.substring(0, 4)+"/"+Seikyu_No.substring(4, 6)+"/"+Seikyu_No.substring(6, 8);
				parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			}

		} else {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			if (n == 4) {
				if (Seikyu_No != "") {
					if (document.fm_left["seikyu_9"].value == "" && document.fm_left["seikyu_8"].value == "") {
						Seikyu_No += "御中";
					}
				}
			} else if (n == 8) {
				if (Seikyu_No != "") {
					if (document.fm_left["seikyu_9"].value == "") {
						Seikyu_No += "御中";
					}
				}

			} else if(n == 9) {
				if (Seikyu_No != "") {
					Seikyu_No += "様";
				}
			}
			try {
				if ((Seikyu_No * 1) >= 0) {
					
				}
			} catch(e){}
			parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			//parent.invoice1.document.getElementById("seikyu_" + n).innerHTML = Seikyu_No;
		}
	} catch (e) {
	}
}

function Calc(n) {
	try {
		n = parseInt(n,10);
		document.fm_left["seikyu_" + (n+2)].value = document.fm_left["seikyu_" + n].value * document.fm_left["seikyu_" + (n+1)].value;
	} catch (e) {
	}
}

function StartBgCalc() {
	try {
		//parent.invoice1.document.fm_right["seikyu_" + n].value
		parent.invoice1.BgCalc();
	} catch (e) {
	}
}


function addRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==11){alert("これ以上登録する事はできません。");return;}
	var row = tblObj.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='hinmoku'><input type='text' id='hinmoku' onkeyup='Chen(1" + rowCnt + "1)' onblur='Chen(1" + rowCnt + "1);' name='seikyu_1" + rowCnt + "1' /></td>\n";
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='tanka'><input type='text' id='tanka' onfocus='if (this.value == \"0\") this.value = \"\";' onkeyup='chkCode(this);Chen(1" + rowCnt + "2)' onblur='Chen(1" + rowCnt + "2);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "2' /></td>\n";
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='suuryou'><input type='text' id='suuryou' onkeyup='chkCode(this);Chen(1" + rowCnt + "3)' onblur='Chen(1" + rowCnt + "3);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "3' /></td>\n";
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='goukei'><input type='text' id='goukei' onkeyup='Chen(1" + rowCnt + "4)' onblur='Chen(1" + rowCnt + "4)' name='seikyu_1" + rowCnt + "4'  readonly='readonly' /></td>\n";
	var cell = row.insertCell(4);
	cell.innerHTML = "<td id='shousai'><img src='../images/bluepen.png' id='shousai' name='seikyu_1" + rowCnt + "5' onclick='OpenIdetailFrame(" + rowCnt + ")'><input type='hidden' id='seikyu_1" + rowCnt + "6' name='seikyu_1" + rowCnt + "6' value='1' /><input type='hidden' id='seikyu_1" + rowCnt + "7" + rowCnt + "' name='seikyu_1" + rowCnt + "7' value='0' /></td>\n";

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	var row = tblFr.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='item'><input type='text' class='item' id='item" + rowCnt + "' name='seikyu_1" + rowCnt + "1' readonly='readonly' /></td>";
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='cost'><input type='text' class='js-characters-change' id='cost" + rowCnt + "' name='seikyu_1" + rowCnt + "2' readonly='readonly' /></td>";
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='amount'><input type='text' class='js-characters-change' id='amount" + rowCnt + "' name='seikyu_1" + rowCnt + "3' readonly='readonly' /></td>";
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "4' readonly='readonly' /><input type='hidden' class='detail' id='detail" + rowCnt + "' name='seikyu_1" + rowCnt + "5' readonly='readonly' /><input type='hidden' class='sales_tax' id='sales_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "6' readonly='readonly' /><input type='hidden' class='withholding_tax' id='withholding_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "7' readonly='readonly' /></td>";

	//テンプレート側再計算
	onLoadData();
}

function templateConstructer() {
	var tblObjParent = document.getElementById("myTBL");
	var rowCntParent = tblObjParent.rows.length;
	for(var i = 1 ; i < rowCntParent - 1 ; i++ ) {
		var tblFr = invoice1.document.getElementById("frTBL");
		var rowCnt = tblFr.rows.length;
		var row = tblFr.insertRow(rowCnt);
		
		var cell = row.insertCell(0);
		cell.innerHTML = "<td id='item'><input type='text' class='item' id='item" + rowCnt + "' name='seikyu_1" + rowCnt + "1' readonly='readonly' /></td>";
		var cell = row.insertCell(1);
		cell.innerHTML = "<td id='cost'><input type='text' class='js-characters-change' id='cost" + rowCnt + "' name='seikyu_1" + rowCnt + "2' readonly='readonly' /></td>";
		var cell = row.insertCell(2);
		cell.innerHTML = "<td id='amount'><input type='text' class='js-characters-change' id='amount" + rowCnt + "' name='seikyu_1" + rowCnt + "3' readonly='readonly' /></td>";
		var cell = row.insertCell(3);
		cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "4' readonly='readonly' /></td>";
		var cell = row.insertCell(4);
		cell.innerHTML = "<td id='detail'><input type='hidden' class='detail' id='detail" + rowCnt + "' name='seikyu_1" + rowCnt + "5' readonly='readonly' /><input type='hidden' class='sales_tax' id='sales_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "6' readonly='readonly' /><input type='hidden' class='withholding_tax' id='withholding_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "7' readonly='readonly' /></td>";

	}
}



function delRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblObj.deleteRow(-1);

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblFr.deleteRow(-1);
	//テンプレート側再計算
	onLoadData();
}


function OpenCommonSearchFrame(){
	document.getElementById("CommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}


function OpenChangeInvoiceFrame(){
	document.getElementById("ChangeInvoice").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}


function OpenIdetailFrame(n){
	document.getElementById("Idetail").style.display = "block";
	parent.Idetail.document.getElementById('row_num').innerHTML = n;
	parent.Idetail.getData(n);
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}


function OpenSouhuListFrame(){
	document.getElementById("SouhuList").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}



function OpenIntervalFrame(){
	document.getElementById("Interval").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}


function openClient(){
	document.getElementById("openClient").style.display="block";
}
function kaihei() {
	var elem = document.getElementById('openClient');
	elem.className = (elem.className == 'hide') ? 'show' : 'hide';
}

function DnameInsert() {
//	var dname = document.getElementById('select_dname').value;
//	document.getElementById('seikyu_4').value = dname;
//	Chen(4);
}

function TransClientId() {
	var client_code = document.getElementById('select_dname').value;
	document.getElementById('addressee_company_info').value = client_code;
}

function TransNo() {
	var destination_code = document.getElementById('Kyoutu_No').value;
	document.getElementById('company_info').value = destination_code;
}

function getCompanyInfo() {
	document.getElementById('destination_code_sender').submit();
}

function getAddresseeCompanyInfo() {
	document.getElementById('client_code_sender').submit();
}

function CheckBeforeSend(n) {
	var seikyu_no = document.getElementById('seikyu_1').value;
	var kyotu_no = document.getElementById('Kyotu_No_Invisible').value;
	var kanri_no = document.getElementById('send_code').value;
	if (n == 1) {
		document.getElementById('direct_send').value = 1;
	}
	if (seikyu_no == "") {
		alert('請求書番号がありません');return false;
	} else if (kyotu_no == "" && kanri_no == ""){
		alert('共通コードと送付先コードのどちらかを入力してください');return false;
	} else {
		document.fm_left.submit();
	}
}

function saveRegularInvoice() {
	//請求書のテンプレート保存処理
	document.getElementById('save_template').value = 1;
	document.fm_left.submit();
}

function changeTemplate(n) {
	var t_type = document.getElementById("template_type").value = n;
	//alert(t_type);
}

$(function(){
	parent.invoice1.location.href = './invoiceframe/invoice<?php echo $template_type;?>';
	changeTemplate(<?php echo $template_type;?>);
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( ".seikyu_2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#seikyu_3" ).datepicker();
});

$(function(){
    $(".hankaku").change(function(){
        var str = $(this).val();
        str = str.replace( /[Ａ-Ｚａ-ｚ０-９－！”＃＄％＆’（）＝＜＞，．？＿［］｛｝＠＾～￥]/g, function(s) {
            return String.fromCharCode(s.charCodeAt(0) - 65248);
        });
        $(this).val(str);
    }).change();
});


function chkCode(id) {
  work='';
  for (lp=0;lp<id.value.length;lp++) {
    unicode=id.value.charCodeAt(lp);
    if ((0xff0f<unicode) && (unicode<0xff1a)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else if ((0xff20<unicode) && (unicode<0xff3b)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else if ((0xff40<unicode) && (unicode<0xff5b)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else {
      work+=String.fromCharCode(unicode);
    }
  }
  id.value=work; /* 半角処理のみ */
  //id.value=work.toUpperCase(); /* 大文字に統一する場合に使用 */
  //id.value=work.toLowerCase(); /* 小文字に統一する場合に使用 */
}



function FrameHeight(){
	window.setTimeout("window.parent.frames['CommonSearch'].test()","600");
}

function FrameHeight2(){
	window.setTimeout("window.parent.frames['SouhuList'].test()","30");
}

//Ajaxで処理
function sendByAjax(num) {
	
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
	if (num == 1) {
		var data = {get_destination_id : $('#Kyoutu_No').val(),company_id : $('#company_id').val()};
	} else if (num == 2) {
		var data = {get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
	    if (num == 1) {
	    	send_url = "send_company_id";
	    } else if (num == 2){
	    	send_url = "send_send_code";
	    }

	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
	    type: "POST",
	    	url: send_url,
	    data: data,
	    /**
	     * Ajax通信が成功した場合に呼び出されるメソッド
	     */
	    success: function(data, dataType) {
	        //successのブロック内は、Ajax通信が成功した場合に呼び出される

	        //PHPから返ってきたデータの表示
	        //document.getElementById('Kyoutu_No').innerHTML = data;
	        //alert(data);
	        var address_data = data.split(",");
	        //alert(document.getElementById('bikou').value);
	        for (i = 0 ;i <= 5;i++) {
	        	//alert(address_data[i]);
	        	document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
	        }
	        document.getElementById('Kyoutu_No').value = address_data[6];
	        onLoadData();
	        //document.write(data);
	    },
	    /**
	     * Ajax通信が失敗した場合に呼び出されるメソッド
	     */
	    error: function(XMLHttpRequest, textStatus, errorThrown) {
	        //通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

	        //this;
	        //thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

	        //エラーメッセージの表示
	        alert('Error : ' + errorThrown);
	    }
	});
}
</script>


<?php 
echo "<p style='color:red;'>".$_SESSION['up_info_msg']."</p>";
$_SESSION['up_info_msg'] = "";


?>
<form id="destination_code_sender" name="destination_code_sender" action="" method="post">
	<input id="company_info" name="get_destination_id" type="hidden" value="" />
</form>
<form id="client_code_sender" name="client_code_sender" action="" method="post">
	<input id="addressee_company_info" name="get_client_id" type="hidden" value="" />
</form>
<input id="company_id" type="hidden" value="<?php $company_id;?>" />

<iframe id="CommonSearch" name="CommonSearch" src="./frame/CommonSearch" scrolling="no"></iframe>
<iframe id="ChangeInvoice" name="ChangeInvoice" src="./frame/ChangeInvoice"></iframe>
<iframe id="Idetail" name="Idetail" src="./frame/Idetail"></iframe>
<iframe id="SouhuList" name="SouhuList" src="./frame/SouhuList"></iframe>
<iframe id="Interval" name="Interval" src="./frame/Interval"></iframe>

<title>請求書作成 - Cloud Invoice</title>
<article id="makeinvoice">

	<form name="fm_left" action="" method="post" onSubmit="return CheckBeforeSend(0);">
		<section id="leftmi">
			<div id="headmi">
				<h2>請求書作成</h2>
			</div>

			<div id="kyoutu">
				<h3>取引先検索</h3>
				<input type="text" id="Kyoutu_No" class="CommonSearch" name="destination_id_visible" onkeyup="OpenCommonSearchFrame();FrameHeight();" value="<?php if ($destination_company_id != "") {echo $destination_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
				<input type="hidden" id="Kyotu_No_Invisible" name="destination_id" value="<?php echo $destination_company_id;?>" />
				<div id="clear"></div>
			</div>

			<div id="souhu">
				<h3>送付先</h3>
				<input type="hidden" id="select_dname" onkeyup="DnameInsert();TransClientId();" value="" />
				<input type="text" id="souhuname" autocomplete="off" onfocus="FrameHeight2();OpenSouhuListFrame()" onkeyup="DnameInsert();TransClientId();" value="<?php if ($destination_client_id != "") {echo $destination_client_id;}?>" placeholder="送付先を入力" />
			</div>
			<div id="clear"></div>
			<div id="seikyuushobangou">
				<input type="button" value="テンプレート切り替え" onClick="OpenChangeInvoiceFrame()">
				<h3>請求書番号</h3>
				<input type="text" onkeyup="Chen(1)" onblur="Chen(1)" name="invoice_code" id="seikyu_1" value="<?php echo $invoice_auto_num;?>"/>
				<div id="clear"></div>
			</div>

			<div id="seikyuubi">
				<h3>請求日</h3>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 1;
						var d=now.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input type="text" onkeyup="Chen(2)" onchange="Chen(2)" onblur="Chen(2)" name="billing_date" class="seikyu_2" id="seikyu_2" value="' + hiduke + '"/>');
					</script>

				<div id="clear"></div>
			</div>
			<div id="shiharaikigen">
				<h3>支払期限</h3>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 2;
						var d=now.getDate();
						var dt = new Date(y,m,0);
						var y=dt.getFullYear();
						var m=dt.getMonth()+1;
						var d=dt.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input type="text" onkeyup="Chen(3)" onblur="Chen(3)" name="pay_date" id="seikyu_3" value="' + hiduke + '"/>');
					</script>
			</div>
			<div>
				<input id="template_type" type="hidden" name="template_type" value="1" />
			</div>
			<div id="clear"></div>

				<script>
					document.write('<input type="hidden" onkeyup="Chen(4)" onchange="Chen(4)" name="destination_name" id="seikyu_' + '4" value="<?php echo $destination_name;?>"  readonly="readonly" />');
				</script>
				<input type="hidden" name="send_code" id="send_code" value="<?php echo $destination_client_id;?>" />

				<script>
					document.write('<input type="hidden" onkeyup="Chen(5)" onblur="Chen(5)" name="destination_zip_code" id="seikyu_' + '5" value="<?php echo $destination_zip_code;?>" readonly="readonly" />');
				</script>
			
				<script>
					document.write('<input type="hidden" onkeyup="Chen(6)" onblur="Chen(6)" name="destination_address" id="seikyu_' + '6" value="<?php echo $destination_address;?>" readonly="readonly" />');
				</script>
				<script>
					document.write('<input type="hidden" onkeyup="Chen(7)" onblur="Chen(7)" name="destination_address2" id="seikyu_' + '7" value="<?php echo $destination_address2;?>" readonly="readonly" />');
				</script>

				<script>
					document.write('<input type="hidden" onkeyup="Chen(8)" onblur="Chen(8)" name="destination_clerk" id="seikyu_' + '8" value="<?php echo $destination_clerk;?>" readonly="readonly" />');
				</script>
				<script>
					document.write('<input type="hidden" onkeyup="Chen(9)" onblur="Chen(9)" name="destination_clerk2" id="seikyu_' + '9" value="<?php echo $destination_clerk2;?>" readonly="readonly" />');
				</script>

			<div id="clear"></div>

			<div id="meisai">
				<table id="myTBL">
					<tr id="first">
						<th id="hinmoku">品目</th>
						<th id="tanka">単価</th>
						<th id="suuryou">数量</th>
						<th id="goukei">金額</th>
						<th id="shousai">詳細</th>
					</tr>
					<tr>
						<td id="hinmoku">
							<script>
								document.write('<input type="text" onkeyup="Chen(111)" onblur="Chen(111)" id="hinmoku" name="seikyu_' + '111" />');
							</script>
						</td>
						<td id="tanka">
							<script>
								document.write('<input type="text" value="0" onfocus="if (this.value == \'0\') this.value = \'\';" onchange="Chen(112);Calc(112);Chen(114)" onkeyup="chkCode(this);Chen(112);Calc(112);Chen(114)" onblur="Chen(112);Calc(112);Chen(114);StartBgCalc();" id="tanka" name="seikyu_' + '112" />');
							</script>
						</td>
						<td id="suuryou">
							<script>
								document.write('<input type="text" onchange="Chen(112);Calc(112);Chen(114)" onkeyup="chkCode(this);Chen(113);Calc(112);Chen(114)" onblur="Chen(113);Calc(112);Chen(114);StartBgCalc();" id="suuryou" name="seikyu_' + '113" />');
							</script>
						</td>
						<td id="goukei">
							<script>
								document.write('<input type="text" onkeyup="Chen(114)" onblur="Chen(114)" id="goukei" name="seikyu_' + '114" readonly="readonly" />');
							</script>
						</td>
						<td id="shousai">
							<script>
								document.write('<img src="../images/bluepen.png" id="shousai" name="seikyu_' + '115" onclick="OpenIdetailFrame(1)" />');
								document.write('<input type="hidden" id="seikyu_116" name="seikyu_116" value="1" />');
								document.write('<input type="hidden" id="seikyu_117" name="seikyu_117" value="0" />');
								
								
							</script>
						</td>
					</tr>
				</table>
				<input type="button" value="1行挿入" onclick="addRow()" />
				<input type="button" value="1行削除" onclick="delRow()">
			</div>
			<div id="hurikomisaki">
				<!--
				<h3>振込先</h3>
				<script>
					document.write('<textarea onkeyup="Chen(10)" onblur="Chen(10)" id="hurikomisaki" name="seikyu_' + '10"></textarea>');
				</script>
				-->
			</div>
			<div id="bikou">
				<h3>備考</h3>
				<script>
					document.write('<textarea onkeyup="Chen(11)" onblur="Chen(11)" id="bikou" name="seikyu_' + '11"></textarea>');
				</script>
			</div>
			
			<input type="button" value="保存する" class="save" onclick="CheckBeforeSend(0);" />
			<input type="button" value="定期請求書として保存" class="save" onclick="OpenIntervalFrame();" />
			<?php if ($_REQUEST['tst'] == 1) {?>
				<input type="button" value="請求書を送る" class="save" onclick="CheckBeforeSend(1);" />
			<?php }?>
			<input type="hidden" id="template_name" name="template_name" value="" />
			<input type="hidden" id="save_template" name="save_template" value="" />
			<input type="hidden" id="template_cycle" name="template_cycle" value="" />
			<input type="hidden" id="reservation_date" name="reservation_date" value="" />
			<input type="hidden" id="template_maturity_cycle" name="template_maturity_cycle" value="" />
			<input type="hidden" id="template_maturity_date" name="template_maturity_date" value="" />
			<input type="hidden" id="start_date" name="start_date" value="" />
			<input type="hidden" id="end_date" name="end_date" value="" />
			<input type="hidden" id="next_creation_date" name="next_creation_date" value="" />
			<input type="hidden" id="next_payment_date" name="next_payment_date" value="" />
			<input type="hidden" id="direct_send" name="direct_send" value="0" />
		</section>
	</form>
	<section>
	<iframe src="./invoiceframe/invoice1" scrolling="no" name="invoice1" id="invoice1"></iframe>
	</section>
	<div></div>
</article>


<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(8)"></div>

<?php require("footer.php");?>