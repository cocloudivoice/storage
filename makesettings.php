<?php
session_start();

//必要なクラスを読み込む
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
//include_once(dirname(__FILE__)."/../db_con/mysqliconnect.class.php");
include_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../cfiles/ReadCsvFile.class.php");
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
require_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$auto_journalize_con = new auto_journalize_control();
$read_csv_con = new ReadCsvFile();
$pdftoimg_con = new PdfToImg();
$journalized_history_con = new journalized_history_control();
setlocale(LC_ALL, 'ja_JP.UTF-8');
mb_internal_encoding("UTF-8");
//変数の宣言
$success_num = 0;
$all_num = 0;
$insert_num = 0;
$company_num = 0;
$data_count = 0;
$update_num = 0;
$non_update_num = 0;
$image_update_num = 0;
$com_update_num = 0;
$user_id = htmlspecialchars($_GET['user_id'],ENT_QUOTES);
$filepath = "/var/www/co.cloudinvoice.co.jp/html/upfiles/".$user_id."/settings";
$file_name = htmlspecialchars($_GET['fn'],ENT_QUOTES);
$rand_num = 0;
$journalized_code = 0;
$rand_num = rand(10,99);
$journalized_code = rand(1000000000,9999999999);
$start_time = date('Y/m/d h-i-s');
$table_name = "JOURNALIZED_SETTINGS";
$customer_id = "000000000000";
$com_id = 0;
$uid = "apache";
$gid = "apache";
$serial = random(6).rand(10000,99999);
$current_slip_code = "";
$current_serial_num = 0;
$file_path = dirname(__FILE__)."/../files/";
$devision_path = dirname(__FILE__)."/files/settings/";
$tb_arr = array();	
$tax_arr = array();
$account_arr = array();
$sub_account_arr = array();
$section_arr = array();
$tb_json_flag = 0;
$tax_json_flag = 0;
$account_json_flag = 0;
$sub_account_json_flag = 0;
$section_json_flag = 0;
$top4 = "";
$mid4 = "";

//▼試算表項目設定ファイルの読み込み▼
$devision_json_path = $devision_path.'devision.json';
$devision_json = file_get_contents($devision_json_path);
//var_dump($devision_json);
$devision_json = mb_convert_encoding($devision_json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
//var_dump($section_json);
$devision_arr = json_decode($devision_json,true);
//var_dump($devision_json);
//echo $devision_json["現預金"]["type"];
//echo $devision_json["現預金"]["order"];
//$account_json->{""};
//▲試算表項目設定ファイルの読み込み▲

//var_dump($file_name);
if ($file_name == "") {
	//$file_name = "username.csv";
	
	//ファイル名が無かったらこっちの処理はしない。
	header("Location:./jounal_upload;");
}
//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$file_name = str_replace(".csv","",$file_name);//ユーザーの案件一覧企業番号取得
$all_num = count($csv) - 1;

if ($all_num != 0) {
	// $data = file_get_contents($filepath."/".$file_name);
	// $data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');

	ob_start();
	//CSVデータを一行ずつ取り出してDBに登録する。
	foreach ($csv as $value) {
		/****
		//テスト・デバッグ用
		if($data_count== 10) {
			break;
		}
		*////
		//変数の初期化
		$mail_address = "";
		$data_count++;
		//var_dump($value);
		if ($value[0] == "企業番号" || $value[0] == "消費税区分"|| $value[0] == "項目") {
			
			//カラム名から列番号を取得する
			for ($k = 0; $k < count($value); $k++) {
				switch ($value[$k]) {
					case "企業番号":
						$com_num = $k;
						break;
					case "消費税区分":
						$tsec_num = $k;
						break;
					case "消費税コード":
						$taxc_num = $k;
						break;
					case "科目":
						$item_num = $k;
						break;
					case "消費税":
						$stax_num = $k;
						break;
					case "区分":
						$div_num = $k;
						break;
					case "会計コード":
						$acnt_num = $k;
						break;
					case "科目補助":
						$suba_num = $k;
						break;
					case "科目名":
						$atit_num = $k;
						break;
					case "補助":
						$subn_num = $k;
						break;
					case "補助コード":
						$subc_num = $k;
						break;
					case "部門":
						$secn_num = $k;
						break;
					case "部門コード":
						$secc_num = $k;
						break;
					case "項目":
						$tbitem_num = $k;
						break;
					case "種別":
						$tbtype_num = $k;
						break;
					case "順番":
						$tborder_num = $k;
						break;
					default :
						$other_num = $k;
						break;
				}
			}
		} else {
			
			//存在フラグの初期化
			$exist_flag = 0;
			$auto_arr = array();
			
			//企業番号取得
			if ($com_id == 0) {
				//echo "<br>企業番号 ";
				$com_id = sprintf('%012d',$value[$com_num]);
				$top4 = substr($com_id,0,4);
				$mid4 = substr($com_id,4,4);
				$path = $file_path.$top4."/".$mid4."/".$com_id."/settings/";
				//jsonファイル置き場のディレクトリ作成
				if ( mkdir( $path, 0775, true ) ) {
					chmod( $path, 0775);
					recurse_chown_chgrp($path, $uid, $gid);
				  //echo "ディレクトリ作成成功！！";
				} else {
				  //echo "ディレクトリ作成失敗！！";
				}
			}
			
			//echo "<br>消費税区分 ";
			$tac_sec = $value[$tsec_num];
			//echo "<br>消費税コード ";
			$tax_code = $value[$taxc_num];
			//echo "<br>科目 ";
			$item_name = $value[$item_num];
			//echo "<br>消費税 ";
			$sales_tax = $value[$stax_num];
			//echo "<br>区分 ";
			$division = $value[$div_num];
			//echo "<br>会計コード ";
			$account_code = $value[$acnt_num];
			//echo "<br>科目補助 ";
			$sub_account = $value[$suba_num];
			//echo "<br>科目名 ";
			$account_title = $value[$atit_num];
			//echo "<br>補助 ";
			$sub_name = $value[$subn_num];
			//echo "<br>補助コード ";
			$sub_code = $value[$subc_num];
			//echo "<br>部門 ";
			$section_name = $value[$secn_num];
			//echo "<br>部門コード ";
			$section_code = $value[$secc_num];
			//echo "<br>項目<br>";
			$tb_item = $value[$tbitem_num];
			//echo "<br>種別 ";
			$tb_type = $value[$tbtype_num];
			//echo "<br>順番 ";
			$tb_order = $value[$tborder_num];
			
			/*
			//試算表項目情報のjsonファイル化
			//試算表項目は全体用の1回の為コメントアウト20170923hmsk
			if ($tb_item != "") {
				$tb_arr[$tb_item]["type"] = $tb_type;
				$tb_arr[$tb_item]["order"] = $tb_order;
			} else if ($tb_json_flag == 0) {
					$tb_json_flag = 1;
					//var_dump($tb_arr);
					//試算表項目tb_arrのjsonファイル化処理
					$tb_json = json_encode($tb_arr, JSON_UNESCAPED_UNICODE);
					file_put_contents("/var/www/co.cloudinvoice.co.jp/html/invoice/files/settings/"."tb.json" , $tb_json);
			}
			*/
			
			//消費税情報のjsonファイル化
			if ($tac_sec != "") {
				$tax_arr[$tac_sec] = $tax_code;
			} else if ($tax_json_flag == 0) {
					$tax_json_flag = 1;
					//消費税tax_arrのjsonファイル化処理
					$tax_json = json_encode($tax_arr, JSON_UNESCAPED_UNICODE);
					file_put_contents($path."tax.json" , $tax_json);
			}
			
			//勘定科目情報のjsonファイル化
			if ($item_name != "") {
				$account_arr[$item_name]["sales_tax"] = $sales_tax;
				$account_arr[$item_name]["division"] = $division;
				$account_arr[$item_name]["account_code"] = $account_code;
				$account_arr[$item_name]["order"] = $devision_arr[$division]["order"];
			} else if ($account_json_flag == 0) {
					$account_json_flag = 1;
					foreach ((array) $account_arr as $ac_key => $ac_value) {
					    $sort_ac_arr[$ac_key] = $ac_value['order'];
					}
					array_multisort($sort_ac_arr, SORT_ASC, $account_arr);
					//var_dump($account_arr);
					//勘定科目account_arrのjsonファイル化処理
					$account_json = json_encode($account_arr, JSON_UNESCAPED_UNICODE);
					file_put_contents($path."account.json" , $account_json);
			}

			//補助科目情報のjsonファイル化
			if ($sub_name != "") {
				$sub_account_arr[$sub_name]["sub_code"] = $sub_code;
				$sub_account_arr[$sub_name]["account_title"] = $account_title;
			} else if ($sub_account_json_flag == 0) {
					$sub_account_json_flag = 1;
					//補助科目account_arrのjsonファイル化処理
					$sub_account_json = json_encode($sub_account_arr, JSON_UNESCAPED_UNICODE);
					file_put_contents($path."sub_account.json" , $sub_account_json);
			}

			//部門情報のjsonファイル化
			if ($section_name != "") {
				$section_arr[$section_name] = $section_code;
			} else if ($section_json_flag == 0) {
					$section_json_flag = 1;
					//部門account_arrのjsonファイル化処理
					$section_json = json_encode($section_arr, JSON_UNESCAPED_UNICODE);
					file_put_contents($path."section.json" , $section_json);
			}

		}

	}
	ob_end_flush();
}
 "開始時間:".$start_time."<br/>\r\n";
 "終了時間:".$end_time = date('Y/m/d h-i-s');

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>仕訳CSVアップロード完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">仕訳CSVアップロード完了</h1>
		<h2>仕訳CSVの登録が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./pastjournal'">仕訳日記帳に移動</button>
		<button onclick="window.location.href='./main'">トップ画面に移動</button>
	</div>
</body>
</html>