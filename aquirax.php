<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>canvasで図形を描く</title>
<script type="text/javascript"src="../js/jquery-2.1.1.min.js" ></script>
<!--[if IE]>
<script type="text/javascript" src="sample/excanvas.js"></script>
<![endif]-->
<script type="text/javascript">
<!--
function sample() {
//描画コンテキストの取得
var canvas = document.getElementById('sample1');
if (canvas.getContext) {

var context = canvas.getContext('2d');

//左から20上から40の位置に、幅50高さ100の四角形を描く
context.fillRect(20,40,50,100);

//色を指定する
context.strokeStyle = 'rgb(00,00,255)'; //枠線の色は青
context.fillStyle = 'rgb(255,00,00)'; //塗りつぶしの色は赤

//左から200上から80の位置に、幅100高さ50の四角の枠線を描く
context.strokeRect(200,80,100,50);

//左から150上から75の位置に、半径60の半円を反時計回り（左回り）で描く
context.arc(150,75,60,Math.PI*1,Math.PI*2,true);
context.fill();

}
}

	document.write($("#sample1"));
	document.write($("#sample1").get(0).getContext('2d'));
//var ctx = $("#myChart").get(0).getContext("2d");
var ctx = $('#sample1').get(0).getContext('2d');
var data = {
    labels : ["January","February","March","April","May","June","July"],
    datasets : [
        {
            fillColor : "rgba(220,220,220,0.5)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            data : [65,59,90,81,56,55,40]
        },
        {
            fillColor : "rgba(151,187,205,0.5)",
            strokeColor : "rgba(151,187,205,1)",
            pointColor : "rgba(151,187,205,1)",
            pointStrokeColor : "#fff",
            data : [28,48,40,19,96,27,100]
        }
    ]
}
var myNewChart = new Chart(ctx).Line(data,options);


//-->
</script>

</head>
<body onLoad="sample()">
<h2>canvasで図形を描く</h2>
<canvas id="sample1" style="background-color:yellow;">
図形を表示するには、canvasタグをサポートしたブラウザが必要です。
</canvas>
</body>
</html>