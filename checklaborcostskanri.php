<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//変数の宣言
$company_id;
$words = "";
$flag = "";
$sql = "";
$chk_num = 0;
$shiharai_num = $_POST['shiharaikanri_num'];
$total_payment = 0;
$sum_payments = 0;
$payment_detail = "";
$param = $_SERVER['QUERY_STRING'];
$pay_out_stts = "";
//var_dump($_POST);


//ユーザーテーブルのコントローラーを呼び出す
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$maindb_con = new db_control();

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}


//各テーブルからデータを取得する

//請求書のステータス変更（自動化に伴い現在使っていない）
/*
if($_REQUEST['status_chg'] == 1) {
	$change_download_password = htmlspecialchars($_REQUEST['download_password'],ENT_QUOTES);
	$change_status = htmlspecialchars($_REQUEST['select_status'],ENT_QUOTES);
	$change_payment_status = htmlspecialchars($_REQUEST['select_payment_status'],ENT_QUOTES);
	$flag_change_status = "up_invoice_download_password";
	$words_change_status = " `status` = ".$change_status." WHERE `destination_id` = ".$company_id." AND `download_password` = '".$change_download_password."' "; 
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag_change_status,$company_id,$words_change_status);
}
*/

//支払ステータスの変更
if($_REQUEST['payment_status_chg'] == 1) {
	$dl_pass = htmlspecialchars($_REQUEST['download_password'],ENT_QUOTES);
	$change_payment_status = htmlspecialchars($_REQUEST['select_payment_status'],ENT_QUOTES);
	$flag_payment_status = "up_invoice_download_password";
	$words_payment_status = " `payment_status` = '".$change_payment_status."' WHERE `destination_id` = ".$company_id." AND `download_password` = '".$dl_pass."' "; 
	$invoice_con -> invoice_data_sql_flag($pdo,$flag_payment_status,$company_id,$words_payment_status);
}

if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
//請求者
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND claimant_id = ".$destination_id;
}
//仕訳済み判定
if (($_REQUEST['status'] == 1 && $_REQUEST['status_1'] == 1 && $_REQUEST['status_2'] == 1 && $_REQUEST['status_3'] == 1) || ($_REQUEST['status'] == 0 && $_REQUEST['status_1'] == 0 && $_REQUEST['status_2'] == 0 && $_REQUEST['status_3'] == 0)) {
	$words .= " AND (`status` < 6 OR `status` = 6 OR `status` = 8 OR `status` = 9) ";
	//全チェックなしを済みにする操作
	$_REQUEST['status'] = 1;$_REQUEST['status_1'] = 1;$_REQUEST['status_2'] = 1;$_REQUEST['status_3'] = 1;
} else if ($_REQUEST['status'] == 1 && $_REQUEST['status_1'] == 1 && $_REQUEST['status_2'] == 0) {
	$words .= " AND (`status` < 6 OR `status` = 6 )";
} else if ($_REQUEST['status'] == 1 && $_REQUEST['status_1'] == 0 && $_REQUEST['status_2'] == 1) {
	$words .= " AND (`status` < 6 OR `status` = 8 )";
} else if ($_REQUEST['status'] == 0 && $_REQUEST['status_1'] == 1 && $_REQUEST['status_2'] == 1) {
	$words .= " AND (`status` = 6 OR `status` = 8 )";
} else if ($_REQUEST['status'] == 1) {
	$words .= " AND `status` < 6 ";
} else if ($_REQUEST['status_1'] == 1) {
	$words .= " AND `status` = 6 ";
} else if ($_REQUEST['status_2'] == 1) {
	$words .= " AND `status` = 8 ";
} else if ($_REQUEST['status_3'] == 1){
	$words .= " AND `status` = 9 ";
}
//$words .= " OR status = 11 ";

//請求者
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND claimant_id = ".$destination_id;
}

//仕訳済み判定
if ($_REQUEST['no_journalized'] == "" && $_REQUEST['journalized'] == "") {
	$words .= " AND (`journalized` = 0 OR `journalized` = 1) ";
} else if ($_REQUEST['no_journalized'] == 1 && $_REQUEST['journalized'] == 1) {
	$words .= " AND (`journalized` = 0 OR `journalized` = 1) ";
} else if ($_REQUEST['no_journalized'] == 1 && $_REQUEST['journalized'] == 0) {
	$words .= " AND `journalized` = 0 ";
} else if ($_REQUEST['no_journalized'] == 0 && $_REQUEST['journalized'] == 1) {
	$words .= " AND `journalized` = 1 ";
} else if ($_REQUEST['no_journalized'] == 0 && $_REQUEST['journalized'] == 0) {
	$words .= " AND `journalized` < 0 ";
}

//paper_typeはdocumentのタイプで0が請求書、1が領収書・レシート 3が表示されないもの。
$words .= " AND `paper_type` = 2 ";

if ($_REQUEST['billing_date_from'] != NULL) {
	$billing_date_from = htmlspecialchars($_REQUEST['billing_date_from'],ENT_QUOTES);
	$words .= " AND billing_date >= ".$billing_date_from;
}
if ($_REQUEST['billing_date_to'] != NULL) {
	$billing_date_to = htmlspecialchars($_REQUEST['billing_date_to'],ENT_QUOTES);
	$words .= " AND billing_date <= ".$billing_date_to;
}
//支払日
if ($_REQUEST['pay_date_from'] != NULL) {
	$pay_date_from = htmlspecialchars($_REQUEST['pay_date_from'],ENT_QUOTES);
	$words .= " AND pay_date >= ".$pay_date_from;
	$conditions_words .= "支払日:".$pay_date_from."～";
}
if ($_REQUEST['pay_date_to'] != NULL) {
	$pay_date_to = htmlspecialchars($_REQUEST['pay_date_to'],ENT_QUOTES);
	$words .= " AND pay_date <= ".$pay_date_to;
	$conditions_words .= "～ 支払日:".$pay_date_to." ";
}
//支払額
if ($_REQUEST['pay_amount_from'] != NULL) {
	$pay_amount_from = htmlspecialchars($_REQUEST['pay_amount_from'],ENT_QUOTES);
	$words .= " AND `total_price` >= ".$pay_amount_from;
	$conditions_words .= "支払額:".$pay_amount_from."～";
}
if ($_REQUEST['pay_amount_to'] != NULL) {
	$pay_amount_to = htmlspecialchars($_REQUEST['pay_amount_to'],ENT_QUOTES);
	$words .= " AND `total_price` <= ".$pay_amount_to;
	$conditions_words .= "～支払額:".$pay_amount_to." ";
}
//商品名・備考
if ($_REQUEST['product_name'] != NULL) {
	$product_name = htmlspecialchars($_REQUEST['product_name'],ENT_QUOTES);
	$words .= " AND `product_name` LIKE '%".$product_name."%'";
	$conditions_words .= "商品名:".$product_name." ";
}
//請求書番号
if ($_REQUEST['invoice_code'] != NULL) {
	$invoice_code = htmlspecialchars($_REQUEST['invoice_code'],ENT_QUOTES);
	$words .= " AND `invoice_code` LIKE '%".$invoice_code."%'";
	$conditions_words .= "請求書番号:".$invoice_code." ";
}
//会社名
if ($_REQUEST['invoice_company_name'] != NULL) {
	$invoice_company_name = htmlspecialchars($_REQUEST['invoice_company_name'],ENT_QUOTES);
	//企業データ取得
	$comp_flag = "company_data_from_like_name";
	$invoice_company_name_arr = $company_con -> company_sql_flag($pdo,$comp_flag,$invoice_company_name,$comp_words);
	//var_dump($invoice_company_name_arr);
	$invoice_claimant_id = $invoice_company_name_arr[0]["company_id"];
	$words .= " AND `claimant_id` = '".$invoice_claimant_id."'";
	$conditions_words .= "会社名:".$invoice_company_name." ";
}


//支払処理のステータス
if ($_REQUEST['paid'] == 0 && $_REQUEST['unpaid'] == 0 && $_REQUEST['notyet'] == 0){
	$_REQUEST['paid'] = 1;$_REQUEST['unpaid'] = 1;$_REQUEST['notyet'] = 1;
}
if ($_REQUEST['paid'] == 1 ||$_REQUEST['unpaid'] == 1 ||$_REQUEST['notyet'] == 1) {
	$words .= " AND (";
	if ($_REQUEST['paid'] == 1) {
		$pay_out_stts .= "`pay_out_status` = 1 ";
	}
	if ($_REQUEST['unpaid'] == 1) {
		if ($_REQUEST['paid'] == 1) {
			$pay_out_stts .= " OR ";
		}
		$pay_out_stts .= " `pay_out_status` = 2 ";
	}
	if ($_REQUEST['notyet'] == 1) {
		if ($_REQUEST['paid'] == 1 ||$_REQUEST['unpaid'] == 1) {
			$pay_out_stts .= " OR ";
		}
		$pay_out_stts .= " `pay_out_status` = 0 ";
	}
	$words .= $pay_out_stts.") ";
}



//支払機能の為、領収書・レシート管理と請求書管理を分ける予定だったが、
//一括で見られる方が利便性が高いとの判断で、支払管理画面として一元化する。
//上田さんとの話し合いで決定 20150428hamasaki

	//▼ページ関連処理▼
	//ページ条件なしでデータ数を取得
	//ダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down";
	$words;//条件をここに入れる。
	$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 2) ";
	$data_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	
	//表示数の制御
	$max_disp = 100;
	$sort_key = "`update_date`";
	$sort_type = "DESC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 2) ";
	$sum_words = $words;//ページ合計金額取得用
	//$words .= " ORDER BY `id` LIMIT 100";
	$words .= " GROUP BY `invoice_code` ORDER BY ";
	$words .= $conditions;
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	
	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down_check";
	$words;//条件をここに入れる。
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	$invoice_data_receive_num = count($invoice_data_receive_arr);
/*
	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down_check";
	$words;//条件をここに入れる。
	$sum_words .= "GROUP BY `destination_id` ORDER BY ".$conditions;
	$sum_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$sum_words);
	//var_dump($sum_arr);
	$sum_num = count($sum_arr);
*/

	//▲ページ関連処理▲

/*
//ダウンロード済みの請求書取得
$flag="invoice_total_data_receive_narrow_down";
$words;//条件をここに入れる。
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
//rsort($invoice_data_receive_arr);
*/






/*
if ($invoice_data_receive_num > 5) {
	$invoice_data_receive_num = 5;
}
*/
//var_dump($invoice_data_receive_arr);

//var_dump($_REQUEST);


//自社企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_self_name = $company_self_arr[0]['company_name'];
	$company_self_payment_id = $company_self_arr[0]['payment_id'];
	$company_self_plan_status = $company_self_arr[0]['plan_status'];
	$company_self_email = $company_self_arr[0]['email'];
}


//▼請求書の支払い処理▼
for ($n = 0;$n < $shiharai_num;$n++){
	$chk_num += $_REQUEST["shiharaikanri".$n];
}
if ($chk_num > 0) {

	//echo "<br/>";
	$timestamp_for_code = date("YmdHis");
	$payment_code = md5($_POST["destination_id".$k]."-".$_POST["claimant_id".$k]."-".$_POST["invoice_code".$k]."-".$_POST["pay_date".$k]."-".$timestamp_for_code);

	for ($k = 0; $k < $shiharai_num; $k++) {
		
		//チェックが入ったものをメールする処理
		
		
		
		$pay_num = "shiharaikanri".$k;
		
		//チェックが入った請求書の情報と総合計金額をメールに書いて送信する。
		if ($_POST[$pay_num] == 1) {
			
			
			
			
			$claimant_id = $_POST["claimant_id".$k];//echo " ";
			$destination_id = $_POST["destination_id".$k];//echo " ";
			$send_code = $_POST["send_code".$k];//echo " ";
			
			//送付先企業データの取得1▼
			if ($destination_id != "" && $destination_id != 0) {
				//echo "send_code なし<br/>";
				$flag = "company_data";
				$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
				//var_dump($company_arr);
				//echo $company_arr[0]["email"];
				$company_email = $company_arr[0]["email"];
				$company_name = $company_arr[0]["company_name"];
				//echo "<br/>▲取得1▲<br/>";
			}
			//echo $send_code."センド<br/>";
/*
			//送付先企業データの取得2▼
			if ($send_code != "" || $send_code != NULL) {
				//echo "send_code あり<br/>";
				$flag = "addressee_data_one";
				$conditions = " AND company_id = ".$company_id." ";
				$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
				//var_dump($company_send_code_check_arr);
				if ($company_send_code_check_arr[0]["email"] != NULL && $company_send_code_check_arr[0]["email"] != ""){
					$company_email = $company_arr[0]["email"];
					$company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
					$company_email .= ",".$company_arr[0]["email"];
					$company_name = $company_arr[0]["company_name"];
				}
				//var_dump($company_arr);
				//echo $company_arr[0]["email"];
			}
*/
			
			$payment_detail .= $company_name." ";
			$billing_date = $_POST["billing_date".$k];//echo " ";
			$payment_detail .= $invoice_code = $_POST["invoice_code".$k]." ";//echo " ";
			$payment_detail .= $invoice_name = $_POST["invoice_name".$k]." ";//echo " ";
			$staff_name = $_POST["staff_name".$k];//echo " ";
			$payment_detail .= $pay_date = $_POST["pay_date".$k]." ";//echo " ";
			$bank_account = $_POST["bank_account".$k]." ";//echo " ";
			$product_code = $_POST["product_code".$k]." ";//echo " ";
			$product_name = $_POST["product_name".$k]." ";//echo " ";
			$unit_price = $_POST["unit_price".$k];//echo " ";
			$quantity = $_POST["quantity".$k];//echo " ";
			$unit = $_POST["unit".$k];//echo " ";
			$sales_tax = $_POST["sales_tax".$k];//echo " ";
			$withholding_tax = $_POST["withholding_tax".$k];//echo " ";
			$payment_detail .= "".$total_price = number_format($_POST["total_price".$k])."円 + 300円(手数料) ";
			$payment_detail .= $remarks1 = $_POST["remarks1".$k];//echo " ";
			$remarks2 = $_POST["remarks2".$k];//echo " ";
			$remarks3 = $_POST["remarks3".$k];//echo " ";
			$status = $_POST["status".$k];//echo " ";
			$download_password = $_POST["download_password".$k];//echo " ";
			$update_date = $_POST["update_date".$k];//echo " ";
			$payment_detail .= "\n";
			$total_payment += $_POST["total_price".$k] + 300;
			
			//支払いコードを登録する
			
			$flag = "up_invoice_download_password";
			$words = " `payment_code` = '".$payment_code."', `payment_status` = 2 WHERE (`destination_id` = ".$destination_id." OR `send_code` = '".$send_code."') AND `claimant_id` = '".$claimant_id ."' AND billing_date = ".$billing_date." AND invoice_code = '".$invoice_code."' AND pay_date = ".$pay_date." AND `status` <> 99 ";
			$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
			$words = "";
			
		}
	}

	if ($total_payment == 0) {
		
	}
	
	//自社宛に明細と合計金額　+　300円（振込手数料）をメールで送る。
	$mailto = $company_self_email;//送り先


	$flag = "invoice_total_data_send_count";
	$words = "AND `invoice_code` = '".$invoice_data_receive_arr[$i]['invoice_code']."' ORDER BY `insert_date` ASC ";//条件
	$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	$words = "";
	//echo "<br/><br/>test<br/><br/>";
	//var_dump($invoice_data_receive_arr);
	//送付先企業データの取得▲

	//メールアドレス
	//$mailto = $company_email;
	//メールに渡す情報
	//$download_password;
	//$keywords = crypt(md5($mailto.$timestamp_for_code.$download_password));
	//$headers = "From: CloudInvoice <info@co.cloudinvoice.co.jp>\r\n";
	//メールアドレス

$message = "".$company_self_name." ご担当者様

一括支払いの詳細情報をお送りします。

".$payment_detail."
------------------------------------------------
お振込金額 ".number_format($total_payment)."円
お振込先：多摩信用金庫 田無支店（058）普通 0128094
	　　　クラウドインボイス株式会社
共通コード".$destination_id."
------------------------------------------------
※共通コードを振込み人名の後ろにつけてお振込みお願いします。
※入金確認後、2営業程度で各お支払い先に振込みいたします。



------------------------------
Cloud Invoice（クラウドインボイス）は、無料で使える請求書発行・情報管理サービスです。

CSVデータの取り込みによってクラウド上で、売上請求書の自動作成、
売上請求書のメール（有料で郵送も）、売上請求書の管理、支払請求書の受取、
支払請求書の管理ができるクラウド請求書発行・請求書情報管理サービスです。

クラウド上での請求書の発行・受け取り、請求データの相互ダウンロードなどで、
煩わしい経理業務の手間を軽減できます。

登録・通常機能は完全無料です。
お気軽にご登録、お試しでのご利用をお待ちしております。
サイトURL: http://co.cloudinvoice.co.jp

Cloud Invoice
------------------------------
";


	mb_language("ja");
	mb_internal_encoding("UTF-8");
	

	//メールを送信する
	$header =  mb_encode_mimeheader("From:Cloud Invoice 運営",'ISO-2022-JP-MS')."<info@co.cloudinvoice.co.jp>";
	$subject = "一括振込先通知-Cloud Invoice";
	$mailto .= ",info@co.cloudinvoice.co.jp";
	$mail_result = mail(
		$mailto,
		mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),
		mb_convert_encoding($message, 'ISO-2022-JP-MS'),
		$header
	);

	if ($mail_result == true) {
		$status_num = 8;
		//echo $send_count++;
		$flag = "up_invoice_download_password";
		$update_words = " `status` = ".$status_num." WHERE `payment_code` = '".$payment_code."'";
		$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$update_words);
	}
		echo "<script type='text/javascript'>location.href=''</script>";
		exit;

}

//▲請求書の支払い処理▲
//echo "<br/><br/><br/><br/><br/><br/><br/>";
//var_dump($invoice_data_receive_arr);

?>

<title>人件費管理 - Cloud Invoice</title>
<script type="text/javascript" src="../js/table-sorter-head.js"></script>
<!--▼リンク先サムネイル化▼-->
<!-- Jquery本体ホスティング -->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
<!-- /Jquery本体ホスティング -->
<!-- thumbnail.js -->
<!--
<script type="text/javascript" src="../js/thumbnail.js"></script>
-->
<!-- /thumbnail.js -->
<!--▲リンク先サムネイル化▲-->

<script language="JavaScript" type="text/javascript">
<!--
function allCheck() {
	var check =  document.getElementById("shiharaikanriA").checked;
	//alert(check);
		for (var i = 0; i < parseInt(document.getElementById("shiharaikanri_num").value); i++){
			try {
				var shiharai = "shiharaikanri" + i;
				//document.getElementById("shiharaikanriA").checked = check_flag;
	   		 	document.getElementById(shiharai).checked = check;
			}catch(e){}
		}
}

function OpenTransferFrame(){
	window.scrollTo(0,0);
	document.getElementById("Transfer").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	
}

//-->
</script>
<script type="text/javascript">
	
function cnt_reload() {
	
	//リロードする。
    location.href="./checkshiharaikanri?<?php echo $param;?>";
}

function reload() {
	//alert("reload");
	//関数cnt_reload()を1000ミリ秒間隔で呼び出す
	setInterval("cnt_reload()",2000);
}

function ChangeDeposit1(n,icd){
	var St = document.getElementById("depositSt" + n);
	var Y = document.getElementById("depositY" + n);
	var N = document.getElementById("depositN" + n);
	var cid = document.getElementById("p_claimant_id" + n).value;
	var did = "<?php echo $company_id;?>";
	var a = St.value;
	if (a == 0){
	Y.className = "deposit1";
	St.value = 1;
	} else if (a == 1){
	Y.className = "deposit0";
	St.value = 0;
	} else if (a == 2){
	Y.className = "deposit1";
	N.className = "deposit0";
	St.value = 1;
	}
	sendByAjax(St.value,did,cid,icd);
}

function ChangeDeposit2(n,icd){
	var St = document.getElementById("depositSt" + n);
	var Y = document.getElementById("depositY" + n);
	var N = document.getElementById("depositN" + n);
	var cid = document.getElementById("p_claimant_id" + n).value;
	var did = "<?php echo $company_id;?>";
	var a = St.value;
	if (a == 0){
	N.className = "deposit2";
	St.value = 2;
	} else if (a == 2){
	N.className = "deposit0";
	St.value = 0;
	} else if (a == 1){
	Y.className = "deposit0";
	N.className = "deposit2";
	St.value = 2;
	}
	sendByAjax(St.value,did,cid,icd);
}

function deleteInvoice(cla,dst,inc) {
	// 確認ボタン付きのダイアログボックスを表示する
	var result = confirm("このデータを削除しますか？");
		
	if (result) {
		//console.log(" OK が押された");
		sendByAjax("delete_flag",dst,cla,inc);
		
	} else {
		//console.log(" CANCEL が押された");
		return false;
	}
}


//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
	//alert(pst);alert(did);alert(cid);alert(icd);
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};

	if (pst == "delete_flag") {
		var data = { delete_flag : pst,destination_id : did, claimant_id : cid, invoice_code : icd};
	} else {
		var data = { get_pay_out_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
	}
	/*	
	} else if (num == 2) {
			var data = { get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
		}
	*/
		if (pst == "delete_flag") {
			send_url = "send_delete_data";
		} else {
			send_url = "send_shiharaikanri";
		}
/*		
} else if (num == 2) {
			send_url = "send_send_code";
		}
*/
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される

			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			//alert(data);
			location.href="";
/*
			var address_data = data.split(",");
			//alert(document.getElementById('bikou').value);
			for (i = 0 ;i <= 5;i++) {
				//alert(address_data[i]);
				document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
			}
			//alert(address_data);
			document.getElementById('destination_email').value = address_data[7];
			document.getElementById('Kyoutu_No').value = address_data[0];
			document.getElementById('company_info').value = address_data[6];
			document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
			if (num == 1) {
				document.getElementById('addressee_company_info').value = "";
			}
			//onLoadData();
			//document.write(data);
*/
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}

</script>
<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>

<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#pay").colResizable({fixed:false});
		$("#pay").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>

<iframe id="Transfer" src="./frame/transfer"></iframe>
<iframe id="Transfer2" src="./frame/transfer2"></iframe>


<article>

	<section id="m-1-box">
		<h2>
			人件費管理
		</h2>
			<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<div class="hiduke">
					<p>支払期限</p>
					<input type="text" id="box1" name="pay_date_from" />
					～
					<input type="text" id="box2" name="pay_date_to" />
				</div>
				<div class="kamoku">
					<p>会社名</p>
					<input type="text" name="invoice_company_name" />
				</div>
				<div class="hojo">
					<p>請求書番号</p>
					<input type="text" name="invoice_code" />
				</div>
				<div class="bumon">
					<p>支払額</p>
					<input type="text" name="pay_amount_from" />
					～
					<input type="text" id="box2" name="pay_amount_to" />
				</div>
				<div class="hojo">
					<p>商品名</p>
					<input type="text" name="product_name" />
				</div>
				<div class="bumon">
					<p>請求書状態</p>
					<label><input name="status" type="checkbox" value="1" <?php if ($_REQUEST['status'] == 1) {echo "checked";}?>/> 未確認</label>
					<label><input name="status_1" type="checkbox" value="1" <?php if ($_REQUEST['status_1'] == 1) {echo "checked";}?> /> 受領済</label><br>
					<label><input name="status_2" type="checkbox" value="1" <?php if ($_REQUEST['status_2'] == 1) {echo "checked";}?> /> 支払処理中</label>
					<label><input name="status_3" type="checkbox" value="1" <?php if ($_REQUEST['status_3'] == 1) {echo "checked";}?> /> 支払済</label>
				</div>
				<div class="hojo">
					<p>支払確認</p>
					<label><input name="paid" type="checkbox" value="1" <?php if ($_REQUEST['paid'] == 1) {echo "checked";}?>/> 支払済</label><br>
					<label><input name="unpaid" type="checkbox" value="1" <?php if ($_REQUEST['unpaid'] == 1) {echo "checked";}?> /> 未払い</label><br>
					<label><input name="notyet" type="checkbox" value="1" <?php if ($_REQUEST['notyet'] == 1) {echo "checked";}?> /> 未処理</label>
				</div>
				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
				</div>
			</form>
			</section>
		<?php if ($conditions_words != "") {echo '<p>【絞り込み条件】'.$conditions_words.'</p>';}?>
		<p>合計金額: <span id="sum_payments_top"></span> 円</p>
		<form action="" method="post" name="payment_form" id="payment_form">
			<table id="pay" class="tables tablesorter draggable" style="text-align:center;">
				<colgroup>
					<col align="center" width="20px">
					<col align="center" width="140px">
					<col align="center" width="100px">
					<col align="center" width="140px">
					<col align="center" width="40px">
					<col align="center" width="150px">
					<col align="center" width="100px">
					<col align="center" width="70px">
					<col align="center" width="70px">
					<col align="center" width="70px">
					<col align="center" width="70px">
				</colgroup>
			<?php if($invoice_data_receive_num > 0) { ?>
				<thead>
				<tr>
					<th id="first"><label id="checklabel"><input type="checkbox" id="shiharaikanriA" name="shiharaikanriA" onClick="allCheck();" /></label><input type="hidden" id="shiharaikanri_num" name="shiharaikanri_num" value="<?php echo $invoice_data_receive_num;?>"></th>
					<th>会社名</th>
					<th>更新日</th>
					<th>請求書番号</th>
					<th class="{sorter:'metadata'}">支払額</th>
					<th>商品名</th>
					<th>支払期限</th>
					<th>PDF</th>
					<th class="{sorter:'metadata'}">状態</th>
					<th>支払確認</th>
					<th>編集</th>
					<th>削除</th>
				<tbody>
			<?php
				} else{
					echo "<p>この条件ではデータが見つかりません</p>";
				}
			?>
			<?php 
				for ($i = 0; $i < $invoice_data_receive_num; $i++) {
					
			?>
				<tr>
				<?php 
					$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
					if ($claimant_id != "") {
						$flag = "company_data";
						$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
					}
					
					//請求書番号ごとのデータを取得する
					$flag = "invoice_total_data_send_count";
					$words = "AND `invoice_code` = '".$invoice_data_receive_arr[$i]['invoice_code']."' ".$search_conditions;//条件
					$conditions = " ";
					$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words,$conditions);
					$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
					//echo "<br/><br/>test<br/><br/>";
					//var_dump($invoice_data_receive_arr);
					//var_dump($invoice_data_receive_arr_count);

					
					
				?>
					<td id="check">
				<?php
					if ($invoice_data_receive_arr[$i]["status"] != 8 AND $invoice_data_receive_arr[$i]["status"] != 9 ) {
				?>
						<label id="checklabel"><input id="shiharaikanri<?php echo $i;?>" type="checkbox" name="shiharaikanri<?php echo $i;?>" value="1" /></label>
				<?php
					}
				?>
					</td>
					<td id="kaisha"><a href="./analysis_customer?cl=<?php echo $invoice_data_receive_arr[$i]['claimant_id'];?>" title="取引先データ"><?php echo $company_arr[0]["company_name"];?></a><input type="hidden" id="p_claimant_id<?php echo $i;?>" name="p_claimant_id<?php echo $i;?>" value="<?php echo $claimant_id;?>"></td>
					<td id="koushin"><?php echo date("Y/m/d", strtotime($invoice_data_receive_arr[$i]["update_date"]));?></td>
					<td id="seikyuNo"><?php echo $invoice_data_receive_arr[$i]["invoice_code"];?><input type="hidden" name="p_invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_code'];?>"></td>
					<td id="kingaku" style="padding:0 10px 0 0" class="{sortValue: <?php echo $invoice_data_receive_arr[$i]['sum(total_price)'];?>}"><?php echo number_format($invoice_data_receive_arr[$i]["sum(total_price)"]);?><input type="hidden" name="p_total_preice<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['sum(total_price)'];?>"></td>
					<td id="tekiyou"><?php echo $invoice_data_receive_arr[$i]["product_name"];?></td>
					<td id="shiharai"><?php $str = $invoice_data_receive_arr[$i]["pay_date"];if ($str != 0) {echo date('Y/m/d',strtotime($str));} ?><input type="hidden" name="p_pay_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['pay_date'];?>"></td>
					<td id="hakkou">
					<?php

						$flag3 = "download_invoice_data";
						$words3 = $invoice_data_receive_arr[$i]['download_password'];//条件をここに入れる。
						$invoice_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag3,$company_id,$words3);
						//var_dump($invoice_num_arr);
						$invoice_num = count($invoice_num_arr);
					?>
						<!--<?php echo "<a href='./invoice2pdf?clm=".$claimant_id."&c1=".$company_id."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=1' target='_blank'>請求書</a>";?>-->
					<?php 
						$invoice_status = $invoice_data_receive_arr[$i]["status"];
						if ($invoice_status < 6) { $status_param = "&downloaded=6";} else {$status_param = "";}
							$pdf_url = "../files/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf";
							//if (!file_exists($pdf_url)) {
								echo "<a class='thumbxx' href=\"./invoice2pdf?dlp=".$invoice_data_receive_arr[$i]['download_password']."&dlps=1&clm=".$claimant_id."&det=".$invoice_num.$status_param."\" target=\"_blank\" onclick=\"reload();\" >請求書</a>";
							//} else {
								//echo "<a class='thumb' href=\"https://co.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf\" target=\"new\">請求書</a>";
								//echo "<a class='thumbxx' href='javascript:subWin(\"https://co.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf\")"."' target='new'>請求書</a>";
							//	echo "<a class='thumbxx' href='https://co.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]["download_password"].".pdf' target='new'>請求書</a>";
							//}
					?>
							<!--
							<a href="javascript:subWin('//co.cloudinvoice.co.jp/files/<?php echo $claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]["download_password"].".pdf";?>');" style='color:red;' target='new'>請求書</a>
							-->
					
					</td>
					<td class="{sortValue: <?php echo $invoice_status;?>}">
					<?php
						//echo "<form action='' method='post'>";
						if ($invoice_status == 6) {
							echo "<span style='color:#004B91;'>受領済</span>";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6" selected>受領済み</OPTION>
								<!--<OPTION value="7">保留</OPTION>-->
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_status == 7) {
							echo "保留";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6">受領済み</OPTION>
								<!--<OPTION value="7" selected>保留</OPTION>-->
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_status == 8) {
							echo "<span style='color:black;'>支払処理中</span>";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6">受領済み</OPTION>
								<!--<OPTION value="7">保留</OPTION>-->
								<OPTION value="8" selected>処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_status == 9) {
							echo "<span>支払済</span>";
							//echo $invoice_data_receive_arr[$i]["status"];
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6" selected>受領済み</OPTION>
								<OPTION value="7">保留</OPTION>
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
							*/
						} else {
							echo "<span style='color:red;'>未確認</span>";
							//echo $invoice_data_receive_arr[$i]["status"];
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6" selected>受領済み</OPTION>
								<OPTION value="7">保留</OPTION>
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
							*/
						}
						/*
						echo '<input type="hidden" name="status_chg" value="1" />';
						if ($invoice_data_receive_arr[$i]["status"] >= 6 && $invoice_data_receive_arr[$i]["status"] != 11) { echo '<input type="button" onclick="submit();" value="変更" style="margin:0 0 0 5px" />';}
						*/
						//echo '</form>';
					?>
					<!--
					<td class="{sortValue: <?php echo $invoice_data_receive_arr[$i]['pay_out_status'];?>}">
					<?php
						echo "<form action='' method='post'>";
						if ($invoice_data_receive_arr[$i]["pay_out_status"] == 0) {
							//echo "<span style='color:#004B91;'>ダウンロード済</span>";
							
							echo '
							<SELECT name="select_pay_out_status">
								<OPTION value="0" selected></OPTION>
								<OPTION value="1">未払い</OPTION>
								<OPTION value="2">支払済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							
						} else if ($invoice_data_receive_arr[$i]["pay_out_status"] == 1) {
							//echo "保留";
							
							echo '
							<SELECT name="select_pay_out_status">
								<OPTION value="0"></OPTION>
								<OPTION value="1" selected>未払い</OPTION>
								<OPTION value="2">支払済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							
						} else if ($invoice_data_receive_arr[$i]["pay_out_status"] == 2) {
							//echo "<span style='color:black;'>支払処理中</span>";
							
							echo '
							<SELECT name="select_pay_out_status">
								<OPTION value="0"></OPTION>
								<OPTION value="1">未払い</OPTION>
								<OPTION value="2" selected>支払済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							
						} else {
							//echo "<span style='color:red;'>未確認</span>";
							//echo $invoice_data_receive_arr[$i]["pay_out_status"];
							
							echo '
							<SELECT name="select_pay_out_status">
								<OPTION value="0" selected></OPTION>
								<OPTION value="1">未払い</OPTION>
								<OPTION value="2">支払済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
							
						}
						
						echo '<input type="hidden" name="pay_out_status_chg" value="1" />';
						if ($invoice_data_receive_arr[$i]["pay_out_status"] >= 0 && $invoice_data_receive_arr[$i]["pay_out_status"] != 11) { echo '<input type="button" onclick="submit();" value="変更" style="margin:0 0 0 5px" />';}
						
						echo '</form>';
					?>

						<input type="hidden" name="claimant_id<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["claimant_id"];?>' />
						<input type="hidden" name="destination_id<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["destination_id"];?>' />
						<input type="hidden" name="send_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["send_code"];?>' />
						<input type="hidden" name="billing_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["billing_date"];?>' />
						<input type="hidden" name="invoice_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["invoice_code"];?>' />
						<input type="hidden" name="invoice_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["invoice_name"];?>' />
						<input type="hidden" name="staff_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["staff_name"];?>' />
						<input type="hidden" name="pay_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["pay_date"];?>' />
						<input type="hidden" name="product_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["product_code"];?>' />
						<input type="hidden" name="product_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["product_name"];?>' />
						<input type="hidden" name="unit_price<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["unit_price"];?>' />
						<input type="hidden" name="quantity<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["quantity"];?>' />
						<input type="hidden" name="unit<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["unit"];?>' />
						<input type="hidden" name="sales_tax<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["sales_tax"];?>' />
						<input type="hidden" name="withholding_tax<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["withholding_tax"];?>' />
						<input type="hidden" name="total_price<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["sum(total_price)"];?>' />
						<input type="hidden" name="remarks1<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks1"];?>' />
						<input type="hidden" name="remarks2<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks2"];?>' />
						<input type="hidden" name="remarks3<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks3"];?>' />
						<input type="hidden" name="status<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["status"];?>' />
						<input type="hidden" name="download_password<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["download_password"];?>' />
						<input type="hidden" name="update_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["update_date"];?>' />
					-->
					
					<?php
						//ページ内の支払合計金額を算出する処理
						$sum_payments += $invoice_data_receive_arr[$i]['sum(total_price)'];
						if ( $i == ($invoice_data_receive_num - 1)) {
							echo '<input type="hidden" name="sum_payments" id="sum_payments" value="'.number_format($sum_payments).'" />';
							echo '<script type="text/javascript">document.getElementById("sum_payments_top").innerHTML = document.getElementById("sum_payments").value;</script>';
						}
					?>
					</td>
					<td id="deposit">
						<?php $pst = $invoice_data_receive_arr[$i]['pay_out_status'];?>
						<input type="hidden" name="depositSt<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['pay_out_status']; ?>" id="depositSt<?php echo $i;?>"><input id="depositY<?php echo $i;?>" type="button" value="支払済" class="<?php if ($pst == 1){echo 'deposit1';} else {echo 'deposit0';} ?>" style="margin:0 0 2px 0;" onclick="ChangeDeposit1(<?php echo $i.',\''.$invoice_data_receive_arr[$i]['invoice_code'].'\'';?>)" /><br /><input id="depositN<?php echo $i;?>" type="button" value="未払い" class="<?php if ($pst == 2){echo 'deposit2';} else {echo 'deposit0';} ?>" onclick="ChangeDeposit2(<?php echo $i.',\''.$invoice_data_receive_arr[$i]['invoice_code'].'\'';?>)" />
					</td>
					<td id="status"><input type="hidden" name="status<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['status'];?>" />
					<?php
					$data_entry_flag = $invoice_data_receive_arr[$i]['data_entry_flag'];
					if ($data_entry_flag >= 1) {
							echo "<a href='./makereceipt_edit?c1=".$invoice_data_receive_arr[$i]['claimant_id']."&clm=".$invoice_data_receive_arr[$i]['destination_id']."&det=".$invoice_num."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."'>編集</a>";
							echo "<br/>";
					}
							echo "<a href='./makereceipt_edit?c1=".$invoice_data_receive_arr[$i]['claimant_id']."&clm=".$invoice_data_receive_arr[$i]['destination_id']."&det=".$invoice_num."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."&copy=1'>コピー</a>";
					?>
					<input type="hidden" name="dl_pass<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['download_password'];?>" />
					<?php
					//ページ内の支払合計金額を算出する処理
					$sum_payments += $invoice_data_receive_arr[$i]['sum(total_price)'];
					if ( $i == ($invoice_data_receive_num - 1)) {
						echo '<input type="hidden" name="sum_payments" id="sum_payments" value="'.number_format($sum_payments).'" />';
						echo '<script type="text/javascript">document.getElementById("sum_payments_top").innerHTML = document.getElementById("sum_payments").value;</script>';
					}
					?>
					</td>
					<td>
					<?php
					$data_entry_flag = $invoice_data_receive_arr[$i]['data_entry_flag'];
					if ($data_entry_flag >= 1) {
					?>
					<input type="button" id="delete<?php echo $i;?>" name="delete<?php echo $i;?>" onclick="deleteInvoice(<?php echo '\''.$invoice_data_receive_arr[$i]['claimant_id'].'\',\''.$invoice_data_receive_arr[$i]['destination_id'].'\',\''.$invoice_data_receive_arr[$i]['invoice_code'].'\'';?>);" value="削除">
					<?php
					}
					?>
						
					</td>
				</tr>
			<?php 
				}
			?>
			</tbody>
			</table>
			<div class="tableText">
				<label id="tableTxt"></label>				
			</div>

			<input type="button" onclick="OpenTransferFrame()" value="支払処理に進む" />
			<!--
			<button onclick="OpenTransferFrame()">支払処理に進む</button>
			-->
		</form>
	</section>


<?php
	if ($page_num > 1) {
		echo '<div class="pageBtnBox"><a class="pageBtn" href="?'.$param.'&page=1"><<</a>';
		for($p = 1;$p <= $page_num;$p++) {
			$bc_class = "";//初期化
			if ($page == $p) {$bc_class = " chBackColor";}
			echo '<a class="pageBtn'.$bc_class.'" href="?'.$param.'&page='.$p.'">'.$p.'</a>';
		}
		echo '<a class="pageBtn" href="?'.$param.'&page='.$page_num.'">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
	}
?>




</article>
<div id="fadeLayer" onclick="CloseFrame(12)"></div>
<div id="noneLayer"></div>




<?php require("footer.php");?>
