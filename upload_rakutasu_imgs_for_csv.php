<?php
session_start();

//echo "test0";
//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/multipurposecontrol.class.php');
//echo "test";
$user_arr = array();
if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$multi_con = new multi_purpose_control();
//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
//$ahead_url = dirname(__FILE__).'/invoice/file_uploader';
//$return_url = dirname(__FILE__).'/invoice/file_uploader';
$path = $_REQUEST['path'];
$mail_flag = 0;
$uid = "apache";
$gid = "dev";
$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'][0];
$_SESSION['colms'] = $_REQUEST['colms'];
$cs_no = intval(substr($filename,0,10));
$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";


//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}
//▲企業データの取得▲

if ($_FILES['upfile']) {
	
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	foreach ($file_ary as $file) {
    	//var_dump($file);
        $filename = $file['name'];
        
		//var_dump($_FILES);
		if ($file['name']) {
			if ( mkdir( $path, 0777, true ) ) {
				chmod( $path, 0777);
				//recurse_chown_chgrp($path, $uid, $gid);
				chown($path, $uid);
	            chgrp($path, "dev");
			  //echo "ディレクトリ作成成功！！";
			} else {
			  //echo "ディレクトリ作成失敗！！";
			}
		}

		if (is_uploaded_file($file["tmp_name"])) {
			if (move_uploaded_file($file["tmp_name"], $path."/".$filename)) {
			    chmod($path. "/" . $filename, 0664);
				$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件のファイルをアップロードしました。";
				$user_arr['user_img'] = $path. "/" . $_FILES['imgurl']["name"];
				$mail_flag = 1;
			} else {
				$_SESSION['up_info_msg'] = $filename . "等のファイルをアップロードできませんでした。";
			}
		}
	}
}
/*
if ($mail_flag == 1) {
	mb_language("ja");
	mb_internal_encoding("UTF-8");
	$email = "test9@cloudinvoice.co.jp";
	$header =  mb_encode_mimeheader("From:Cloud Invoice 運営",'ISO-2022-JP-MS')."<test9@cloudinvoice.co.jp>";
	$subject = "PDF UPLOADED:".mb_encode_mimeheader($company_name,'ISO-2022-JP-MS');
	$message = $user_id." ".$company_name." 様により\n PDFが ".$file_num." 件アップロードされました。\n ファイル保存場所：".$path."";
	mail($email,$subject,mb_convert_encoding($message, 'ISO-2022-JP-MS'),$header);
}
*/

if ($_REQUEST["unzip_flag"] == 1) {
	$file_type = substr($filename,-3,3);
	if ($file_type == "zip") {
		if ($path != NULL && $path != "") {
			shell_exec("unzip -j ".$path."/*.zip -d ".$path." > /dev/null 2>&1");
			shell_exec("rm -f ".$path."/*.zip > /dev/null 2>&1");
		}
	}
	header("Location:".$return_url,false);
	exit;
}

//▼CSV用の処理▼
if ($_REQUEST["csv_flag"] == 1) {
	
	$k = 1;
	$fp = fopen($path."/".$filename, 'r+');
	// setlocaleをまずは設定
	setlocale(LC_ALL, 'ja_JP.sjis-win');
	$row = 0;
	while (($data = fgetcsv($fp)) !== FALSE) {
		//var_dump($data);
		//mb_convert_encoding($data, 'UTF-8', 'SJIS');
		for ($i = 0;$i < 10;$i++) {
			$data[$i] = mb_convert_encoding($data[$i], 'UTF-8', 'sjis-win');
			//echo "$i<br>";
			if ($data[0] == "請求書番号") {
				//カラムあり。カラムをここで消す。
				//echo "ok";exit;
			} else {
				if ($i == 1) {//入金期限日
					if ($data[$i] != "" && $data[$i] != 0) {//入金期限日が入っている場合
						$csv_data .= '"'.date("Y/m/d",strtotime($data[$i])).'"';
					} else {
						$csv_data .= '""';
					}
				} else if ($i == 3) {//銀行名
					$data[$i] = mb_convert_kana($data[$i], 'RNSKA',"UTF-8");
					$csv_data .= '"'.$data[$i].'"';
				} else if ($i == 7) {//口座名義人
					$patterns = array("ｧ","ｨ","ｩ","ｴ","ｫ","ヵ","ｯ","ｬ","ｭ","ｮ","ー","ｰ","（","）","　");
					$replace = array("ｱ","ｲ","ｳ","ｴ","ｵ","ｶ","ﾂ","ﾔ","ﾕ","ﾖ","-","-","(",")"," ");
					for ($k = 0;$k < count($patterns);$k++) {
						$data[$i] = str_replace($patterns[$k],$replace[$k],$data[$i]);
					}
					$csv_data .= '"'.$data[$i].'"';
				} else if ($i == 8) {//金額
					$csv_data .= $data[$i];
				} else if ($i == 6)  {//口座番号
					if (strlen($data[$i]) > 7){ $_SESSION["raku_csv_msg"] = $row."行目の口座番号が".strlen($data[$i])."桁です。";echo "<script type='text/javascript'>location.href='./rakutasu_imgs_to_text_upload_all'</script>";exit;}
					$data[$i] = sprintf("%07d",$data[$i]);
					$data[$i] = str_replace("0000000","",$data[$i]);
					$csv_data .= '"'.$data[$i].'"';
				} else {//その他
					$csv_data .= '"'.$data[$i].'"';
				}
				if ($i < 9) {
					$csv_data  .= ",";
				} else if ($i == 9) {
					$csv_data  .= "\r\n";
				}
			}

		}
			//電話番号で楽たす銀行テーブルに登録されていないか確認する
			/**ここに楽たす銀行テーブル電話番号検索を作成する**/
			//var_dump($data);echo "<br>";
			$recipient_num = $cs_no;$holder_name = $data[7];$bank_name = $data[3];$branch_name = $data[4];$type = $data[5];$account_number = $data[6];$company_name = $data[2];$due_date = $data[2];$tel = $data[10];
			$remarks2 = intval($recipient_num).intval($account_number);
			if ($tel == "") {$tel = $remarks2;}
			$sql = "SELECT * FROM `RAKUTAS_BANK_ACCOUNT_DATA_TABLE` WHERE `tel` = '".$tel."';";
			$res = $multi_con -> multi_purpose_sql($pdo,$sql);
			if (count($res) > 0 && $tel != "" && $remarks2 != "") {
				//テーブルに情報があれば更新
				//楽たす銀行データテーブルに登録
				$sql = "UPDATE `RAKUTAS_BANK_ACCOUNT_DATA_TABLE` 
				SET holder_name = '".$holder_name."',bank_name = '".$bank_name."',branch_name = '".$branch_name."',type = '".$type."',account_number = '".$account_number."',company_name = '".$company_name."'
				WEHERE `tel` = '".$tel."' OR `remarks2` = '".$remarks2."';";
				//echo "<br>";
				$res2 = $multi_con -> multi_purpose_sql($pdo,$sql);
			} else {
				//テーブルに情報が無ければ挿入
				//楽たす銀行データテーブルに登録
				$sql = "INSERT INTO `RAKUTAS_BANK_ACCOUNT_DATA_TABLE` 
				(recipient_num,holder_name,bank_name,branch_name,type,account_number,company_name,due_date,remarks,remarks2,tel,zip_code,address,address2,section,staff_name) VALUES 
				('".$recipient_num."','".$holder_name."','".$bank_name."','".$branch_name."','".$type."','".$account_number."','".$company_name."','".$due_date."','".$remarks."','".$remarks2."','".$tel."','".$zip_code."','".$address."','".$address2."','".$section."','".$staff_name."');";
				//echo "<br>";
				$multi_con -> multi_purpose_sql($pdo,$sql);
			}
		$row++;
	}
//echo $csv_data;
	fclose($fp);
	shell_exec("rm -f ".$path."/".$filename." dev>null 2>&1");
//exit;
    //出力ファイル名の作成
    $csv_file = $filename;
  	
    //文字化けを防ぐ
    $csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );
    
    //MIMEタイプの設定
    header("Content-Type: application/octet-stream");
    //名前を付けて保存のダイアログボックスのファイル名の初期値
    header("Content-Disposition: attachment; filename={$csv_file}");

    // データの出力
    echo($csv_data);
    exit();
}
	//var_dump($user_arr);
	//$result = $user_con->user_update($pdo,$user_arr);
	//var_dump($result);
if ( $_FILES['upfile']['tmp_name'] != NULL ) {
	header("Location:$ahead_url",true);
} else {
	header("Location:$return_url",true);
}
//▲CSV用の処理▲
exit();

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 

function CapText($str) {
	
	$kana = array("ｧ"=>"ｱ","ｨ"=>"ｲ","ｩ"=>"ｳ","ｪ"=>"ｴ","ｫ"=>"ｵ","ヵ"=>"ｶ","ｯ"=>"ﾂ","ｬ"=>"ﾔ","ｭ"=>"ﾕ","ｮ"=>"ﾖ");
	$str = strtr($str, $kana);
	$str = mb_convert_kana($str, 'aKvrn');

	return $str;
}

?>