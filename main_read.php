<?php
session_start();

//session_cache_limiter('nocache');
//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//税理士事務所用クラスを読み込む
if (isset($_SESSION['af_id']) || isset($_SESSION['acm_id'])) {
	require_once(dirname(__FILE__).'/../db_con/accountfirmsclientcontrol.class.php');
	require_once(dirname(__FILE__).'/../db_con/accountfirmcontrol.class.php');
}

//変数の宣言
$user_id = "";
$company_id = "";
$user_arr = array();
$words = "";

//echo $_SESSION['user_id'];
//user_idチェック
if (isset($_SESSION['acm_id'])) {
	$af_id = $acm_id = (int)$_SESSION['acm_id'];
	$company_id = $user_id = $af_client_id = (int)$_SESSION['user_id'];
} else if (isset($_SESSION['af_id'])) {
	$af_id = (int)$_SESSION['af_id'];
	$company_id = $user_id = $af_client_id = (int)$_SESSION['user_id'];
} else if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
} else {
	header("Location:./logout.php");
	exit();
}

//var_dump($_SESSION);
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$user_con = new user_control();
$company_name_con = new company_control();
//各テーブルのコントローラーを起動する
if (isset($af_id)) {
	$af_client_con = new account_firms_client_control();
	$af_con = new account_firm_control();
}
if (isset($af_id)) {
	$af_arr = $af_con ->  af_select_id($pdo,$af_id);
	$flag = "af_client_data";
	$af_client_arr = $af_client_con -> af_client_sql_flag($pdo,$flag,$af_id,$words);
	$af_client_num = count($af_client_arr);
}

//変数
if (isset($_POST["af_client_id"])) {
	$company_id = $user_id = $_SESSION['user_id'] = $af_client_id = $_POST["af_client_id"];
} else if ($_SESSION['user_id'] == "") {
	$company_id = $user_id = $_SESSION['user_id'] = $af_client_id = $af_client_arr[0]["client_id"];
}

//各テーブルからデータを取得する
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$user_company_name = $company_name_con -> company_data_sql($pdo,$user_id);

?>