<?php session_start();?>
<?php include_once(dirname(__FILE__).'/../invoice/invoicepdf_head.php');?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">



html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
#total_price{text-align:center; font-size:20px;font-weight:7;text-decoration:underline;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}



article#invoice1 {width:790px; height:1125px; background-image:-webkit-radial-gradient(1500px 562px, circle farthest-side, white 96%,rgb(200,255,255) 98%, rgb(100,255,255) 100%);}
article#invoice1 section{width:600px; padding:40px 0 0 90px;}


article#invoice1 h1 {margin:40px 0 10px 0; text-align:center; font-size:20px;}
article#invoice1 table#top{color:rgb(180,180,180); font-weight:bold; font-size:13px; letter-spacing:0.1em; margin:10px 0 80px 50px;}
article#invoice1 table#top td#img{width:60px; text-align:center;vertical-align:bottom;}
article#invoice1 table#top td#spacer_td{width:100px; text-align:center;}
article#invoice1 img{width:250px;}


article#invoice1 table div#main1{width:300px;height:260px; margin:40px 0 0 0;text-align:left;}
article#invoice1 table div#main1 h2{font-size:16px; margin:0 0 10px 0;}
article#invoice1 table div#main1 ul{list-style-type:none; margin:0 0 30px 0;}
article#invoice1 table div#main1 p{margin:0 0 10px 0;}



article#invoice1 table#main2{margin:60px 0 0 55px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230);}
article#invoice1 table#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); font-size:14px; margin: 0 0 20px 0;}
article#invoice1 table#main2 th#item{width:250px; text-align:left;}
article#invoice1 table#main2 th#cost{width:100px; text-align:right;}
article#invoice1 table#main2 th#amount{width:50px; text-align:right;}
article#invoice1 table#main2 th#price{width:100px; text-align:right;}
article#invoice1 table#main2 td{height:35px; text-align:right;}
article#invoice1 table#main2 td#item{text-align:left;}



article#invoice1 table#main3{border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); margin:0 0 0 305px;}
article#invoice1 table#main3 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230); font-size:14px;}
article#invoice1 table#main3 td#item{width:100px;}
article#invoice1 table#main3 td#amount{width:150px;text-align:right;}



article#invoice1 table#main4{margin:50px 0 0 90px; font-size:12px;}
article#invoice1 table#main4 td{}
article#invoice1 table#main4 td#left{width:180px; height:35px;}
article#invoice1 table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
article#invoice1 table#main4 td#notes{width:180px; height:35px;}



</style>




</head>



<body>



<article id="invoice1">

<section>
	<h1>請求書</h1>
	<br><br>
	<table id="top">
		<tr>
			<td rowspan="8">

				<div id="main1">
					<p>
						<?php echo $destination_company_arr[0]["company_name"];?>御中<br>
						<?php /*echo $destination_company_arr[0]["section"];?>　<?php echo $destination_company_arr[0]["clerk"]."様";*/?>
					<br>
					〒	
						<?php

						//var_dump($destination_company_arr);
						echo $destination_company_arr[0]["zip_code"];
						?><br>
						<?php echo $destination_company_arr[0]["address1"];?><br>
						<?php echo $destination_company_arr[0]["address2"];?>
					<br>
						請求書番号　　<?php echo $invoice_data_receive_arr[0]['invoice_code'];?><br>
						請求日　　　　<?php echo str_replace(" ","",substr($invoice_data_receive_arr[0]['billing_date'],0,4)."/".substr($invoice_data_receive_arr[0]['billing_date'],4,2)."/".substr($invoice_data_receive_arr[0]['billing_date'],6,2));?><br>
						　<br>
					</p>

				</div>

			</td>
			<td rowspan="8" id="spacer_td">
			</td>
			<td>
			<?php
			if ($logotype != "") {
			?>
				<img src='<?php echo $logotype;?>' title='ロゴ' alt='ロゴ' style='max-width:100px;max-height:100px;'/></td>
			<?php
			}
			?>
			</td>

		</tr>
		<tr>
			<td><?php echo $company_name;?></td>
			<td rowspan="7" id="img">
			<?php
			if ($stamp_image != "") {
			?>
				<img src='<?php echo $stamp_image;?>' title='印影' alt='印影' style='width:60px;height:60px;float:right;' />
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td>〒<?php echo $zip_code;?></td>

		</tr>
		<tr>
			<td><?php echo $address1;?></td>

		</tr>
		<tr>
			<td><?php echo $address2;?></td>

		</tr>
		<tr>
			<td>TEL：<?php echo $tel_num /*str_replace(" ","","0".substr($tel_num,0,1)."-".substr($tel_num,1,3)."-".substr($tel_num,4,4))*/;?></td>

		</tr>
		<tr>
			<td>FAX：<?php echo $fax_num /*str_replace(" ","","0".substr($fax_num,0,1)."-".substr($fax_num,1,3)."-".substr($fax_num,4,4))*/;?></td>

		</tr>
	</table>

			<?php
			if ($data_cnt <= 6) {
				$data_cnt = 6;
			}
			for ($i = 0; $i < $data_cnt; $i++) {
				$total_price_top += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
			<p id="total_price">ご請求金額　<?php echo number_format($total_price_top);?>円(税込)</p>

	<table id="main2">
		<tr>
			<th id="item">品目</th>
			<th id="cost">単価</th>
			<th id="amount">数量</th>
			<th id="price">価格</th>
		</tr>
			<?php
			for ($i = 0; $i < $data_cnt; $i++) {
			?>
			<tr>
				<td id="item" style="text-align:left;"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
				<td><?php if ($invoice_data_receive_arr[$i]['unit_price'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['unit_price']);} else { echo " ";}?></td>
				<td><?php if ($invoice_data_receive_arr[$i]['quantity'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['quantity']);} else { echo " ";}?></td>
				<td><!--<?php echo number_format((int)$t_price = $invoice_data_receive_arr[$i]['unit_price']*$invoice_data_receive_arr[$i]['quantity']);?>--><?php if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['total_price_excluding_tax']);} else { echo " ";};?></td>
			</tr>
			<?php
				$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				$sales_tax += $invoice_data_receive_arr[$i]['sales_tax'];
				//$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
				/*
				$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
				*/
				$total_price += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
	</table>

	<table id="main3">
		<tr>
			<td id="item">小計</td>
			<td id="amount"><?php echo number_format($sum_price);?></td>
		</tr>
		<tr>
			<td id="item">消費税</td>
			<td id="amount"><?php echo number_format($sales_tax);?></td>
		</tr>
		<tr>
			<td id="item">合計</td>
			<td id="amount"><?php echo number_format($total_price);?></td>
		</tr>
	</table>



	<table id="main4">
		<tr>
			<td id="left">お支払期限</td>
			<td><?php echo str_replace(" ","",substr($invoice_data_receive_arr[0]['pay_date'],0,4)."/".substr($invoice_data_receive_arr[0]['pay_date'],4,2)."/".substr($invoice_data_receive_arr[0]['pay_date'],6,2));?></td>
		</tr>
		<tr>
			<td>振込先</td>
			<td><?php echo $company_arr[0]["account_info"];?></td>
		</tr>
		<tr>
			<td id="gray">お振込手数料は御社</td>
			<td rowspan="2">口座名義　<?php echo $company_arr[0]["account_name"];?></td>
		</tr>
		<tr>
			<td id="gray">ご負担にてお願い致します。</td>
		</tr>
		<tr>
			<td id="notes">備考</td>
			<td><?php echo $company_arr[0]["remarks1"];?><br/><?php echo $invoice_data_receive_arr[0]['remarks1'];?></td>
		</tr>

	</table>
</section>


</article>
</body>


