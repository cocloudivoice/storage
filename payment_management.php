<?php
session_start();

//必要なクラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
//echo "test1";
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];
$start_d = date('Ymd', mktime(0,0,0,date('m')+1,"01",date('Y')-1));//去年の来月1日

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

if (isset($_POST['aj_words'])) {
	$aj_words = $_POST['aj_words'];

} else {
	//絞込み条件

	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$aj_words .= " AND `pay_date` >= '".$payfrom."' ";
	}/* else {
		$payfrom = $start_d;
		$aj_words .= " AND `pay_date` >= '".$payfrom."' ";
	}*/
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$aj_words .= " AND `pay_date` <= '".$payto."' ";
	}
	if ($_REQUEST['money_from']) {
		$money_from = $_REQUEST['money_from'];
		$having .= "AND sum(`total_price`) >= ".$money_from." ";
	}
	if ($_REQUEST['money_to']) {
		$money_to = $_REQUEST['money_to'];
		$having .= " AND sum(`total_price`) <= ".$money_to." ";
	}
	if ($_REQUEST['cl']) {
		$claimant_id = $_REQUEST['cl'];
	}

	if ($_REQUEST['remarks1']) {
		$remarks1 = $_REQUEST['remarks1'];
		$aj_words .= " AND (`remarks1` LIKE '%".$remarks1."%' OR `product_name` LIKE '%".$remarks1."%') ";
	}
	
}

//月次請求額
$start_d;
//echo "<br/>\r\n";
//$end_d = date('Ymd', mktime(0,0,0,date('m')+1,"01",date('Y')-1));//去年の来月1日
$end_d = date('Ymd', mktime(0,0,0,date('m')+1,"01",date('Y')));//去年の来月1日
//echo "<br/>\r\n";
$graph_num = 4;
for  ($k = 1; $k <= $graph_num; $k++) {
	$monthly_arr[$k]= $monthly_start = date('Ymd', mktime(0,0,0,date('m') - 4 + $k, 1, date('Y')));
	//echo "<br/>\r\n";
	$month_end = date('Ymd', mktime(0,0,0,date('m') + $k + 1, 1, date('Y')));
	//echo "<br/>\r\n";
	$flag = "invoice_data_send_total_narrow_down";
	$words = $aj_words."  AND (`pay_date` >= '".$monthly_start."' AND `pay_date` < '".$month_end."') AND (`status` >= 1 AND `status` <> 99) ORDER BY `pay_date` ASC";
	$total_claim_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	//var_dump($total_claim_arr);
	if ($total_claim_arr[0]['sum(total_price)'] == NULL) {
		$total_claim_arr[0]['sum(total_price)'] = 0;
	}
	$total_claim += $claim_arr[$k] = $total_claim_arr[0]['sum(total_price)'];
	//echo "<br/>\r\n";
	//echo $claim_arr[$k];
	//echo "<br/>\r\n";

	$labels .= "'".date('Y/m',strtotime($monthly_arr[$k]))."'";
	if ($k != $graph_num) {
		$labels .= ",";
	}
	//echo "<br/>\r\n";
	$params .= "".$claim_arr[$k];
	//echo "<br/>\r\n";
	if ($k != $graph_num) {
		$params .= ",";
	}
	
	$params2 .= "-".$claim_arr[$k];
	//echo "<br/>\r\n";
	if ($k != $graph_num) {
		$params2 .= ",";
	}

	
}
//echo $labels;echo " ";
//echo $params;
//請求書データから請求者のデータを取得
$claimant_id = $_REQUEST['cl'];
$flag = "invoice_total_data_send_narrow_down";
$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `invoice_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
$cl_invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_invoice_data_arr)
$cl_invoice_data_num = count($cl_invoice_data_arr);

//企業データ存在確認（元側）▼
$flag = "company_data";
$words = "";
$cl_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($cl_company_data_arr);
$cl_company_data_arr[0]["company_name"];

?>

<?php require("header.php");?>
<script type="text/javascript" src="//storage.cloudinvoice.co.jp/js/Chart.js/Chart.js"></script>
<script src="../js/Chart_Dynamic_View.js"></script>
<!--<link rel="stylesheet" href="../css/chart.css" type="text/css" />-->
<script type="text/javascript">



//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
	//alert(pst);alert(did);alert(cid);alert(icd);
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
//	if (num == 1) {
		var data = { get_payment_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
/*	} else if (num == 2) {
		var data = { get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
*/
//		if (num == 1) {
			send_url = "send_pastjournal";
/*		} else if (num == 2) {
			send_url = "send_send_code";
		}
*/
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される
			
			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			alert(data);
/*
			var address_data = data.split(",");
			//alert(document.getElementById('bikou').value);
			for (i = 0 ;i <= 5;i++) {
				//alert(address_data[i]);
				document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
			}
			//alert(address_data);
			document.getElementById('destination_email').value = address_data[7];
			document.getElementById('Kyoutu_No').value = address_data[0];
			document.getElementById('company_info').value = address_data[6];
			document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
			if (num == 1) {
				document.getElementById('addressee_company_info').value = "";
			}
			//onLoadData();
			//document.write(data);
*/
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}
</script>
<script type="text/javascript">


$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});
</script>
<!--[if IE]>
<script type="text/javascript" src="../js/excanvas.js"></script>
<![endif]-->
<script type="text/javascript">
<!--
window.onload = function() {
	//描画コンテキストの取得
	var canvas = document.getElementById('graph');
	if (canvas.getContext) {
		
		//var ctx = $('#lineChartCanvas')[0].getContext("2d");
		var context = canvas.getContext('2d');
		var ctx = context;
		var data = {
				//横軸のラベル
				labels : [<?php echo $labels;?>],
				datasets : [
		
				{
					fillColor : "rgba(0,200,0,0.5)",
					strokeColor : "rgba(0,255,0,1)",
					pointColor : "rgba(255,0,0,1)",
					pointStrokeColor : "#995515",
					data : [<?php echo $params;?>]
				},
		
				{
					fillColor : "rgba(200,0,0,0.5)",
					strokeColor : "rgba(255,0,0,1)",
					pointColor : "rgba(0,255,0,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $params;?>]
/*
					
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $params;?>]
*/
				}

			]

		}
		var option = {
			//縦軸の目盛りの上書き許可。これ設定しないとscale関連の設定が有効にならないので注意。
/*			scaleOverride : true,

			//以下設定で、縦軸のレンジは、最小値0から5区切りで35(0+5*7)までになる。
			//縦軸の区切りの数
			scaleSteps : 7,
			//縦軸の目盛り区切りの間隔
			scaleStepWidth : 5,

*/
			//縦軸の目盛りの最小値
			//scaleStartValue : -500000,
			
			//アニメーション設定
			//animation : false,
/*
			//Y軸の表記（単位など）
			scaleLabel : "<%=value%>A",
*/
			//ツールチップ表示設定
			//showTooltips: false,

			//ドットの表示設定
			//pointDot : false,

			//線を曲線にするかどうか。falseで折れ線になる。
			//bezierCurve : false
		}

		//new Chart(ctx).Line(lineChartData,option);
		//棒グラフ
		var myNewChart = new Chart(ctx).Bar(data,option);
		//線グラフ
		//var myNewChart = new Chart(ctx).Line(data);
		//多角グラフ
		//var myNewChart = new Chart(ctx).Radar(data);
	}
}
-->
</script>

<title>
経営分析 - Cloud Invoice
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article>

	<section id="m-1-box">
		<h2>
		経営分析
		</h2>
		
		<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<!--
				<div class="kamoku">
					<p>取引先名</p>
					<input type="text" name="" value="<?php echo $cl_company_data_arr[0]['company_name'];?>"/>
				</div>
				-->
				<div class="hiduke">
					<p>取引日付</p>
					<input type="text" id="box1" name="paydate_from" />
					～
					<input type="text" id="box2" name="paydate_to" />
				</div>
				<div class="hiduke">
					<p>金額</p>
					<input type="text"  name="money_from" />
					～
					<input type="text"  name="money_to" />
				</div>
				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
					<input type="hidden" name="cl" value="<?php echo $claimant_id;?>">
				</div>
			</form>
		</section>

		<div class="SearchLine"></div>

		<section id="customer">
			
			<div id="">
				
				<h3><?php echo $cl_company_data_arr[0]["company_name"];?></h3>
				<table>
				<tr><th>請求月</th><th>請求金額</th><th>入金済金額</th><th>入金率</th></tr>
				<?php
				for ($k = 1;$k <= $graph_num; $k++) {
					echo "<tr><td>".date('Y年m月',strtotime($monthly_arr[$k]))."</td>";
					echo "<td style='text-align:right;'>".number_format($claim_arr[$k])."円</td>";
					echo "<td style='text-align:right;'>円</td>";
					echo "<td style='text-align:right;'>0%</td>";
					echo "</tr>";
				}
				?>
				</table>
				<p>
					該当期間の総請求金額：<a href= "./analysis_claim"><?php echo number_format($total_claim);?> 円</a>
				</p>
<!--
				<table>
					<tr>
						<th>支払日付</th>
						<th>支払金額</th>
						<th>摘要</th>
						<th>証憑</th>
					</tr>
				<?php
				for ($i = 0;$i < count($cl_invoice_data_arr);$i++) {
					
				//請求書・領収書データの登録
				
				$pay_date = $cl_invoice_data_arr[$i]['pay_date'];
				//$pay_date = $cl_invoice_data_arr[$i]['paid_date'];
				$dlp = $cl_invoice_data_arr[$i]['download_password'];
				//echo $path = "/var/www/storage.cloudinvoice.co.jp/html/files/".sprintf("%012d", $cl_invoice_data_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				$path = "../files/".sprintf("%012d", $cl_invoice_data_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				//echo "<br/>\r\n";
				$pdf_url = $path."/".$dlp.".png";
				//$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				if (!file_exists($pdf_url)) {
					$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				}
				?>

					<tr>
						<td id="date" class="text"><?php if ($pay_date != 0) { echo substr($pay_date,0,4)."/".substr($pay_date,4,2)."/".substr($pay_date,6,2);}?></td>
						<td class="text"><?php echo number_format($cl_invoice_data_arr[$i]['sum(total_price)']);?></td>
						<td id="product" class="text"><?php echo $cl_invoice_data_arr[$i]["product_name"].$cl_invoice_data_arr[$i]["remarks1"];?></td>
						<td class="icon"><a href="<?php echo $pdf_url;?>" target="_blank" title="証憑画像">画像</a></td>
					</tr>
				<?php
				}
				?>
				</table><br>
				
			</div>
-->
			<div id="right">
				<canvas id="graph" class="canvas_chart" style="background-color:white;" width="500px" height="300px">
					図形を表示するには、canvasタグをサポートしたブラウザが必要です。
				</canvas>
				<table id="graph_tbl">
				<?php
				$labels_arr = split(",",$labels);
				$params_arr = split(",",$params);
				
				for ($k = 0; $k < count($cl_graph_arr); $k++) {
				?>
				
					<tr><th> <?php echo $cl_graph_arr[$k]['ymd'];?> </th><td> <?php echo number_format($cl_graph_arr[$k]['sum(`total_price`)']);?> 円</td></tr>
				<?php
				}
				?>
				</table>
			</div>
			
			
		</section>
	</section>
</article>

<?php require("footer.php");?>
<?php 
	unset($total_claim_arr);
	unset($total_payment_arr);
?>