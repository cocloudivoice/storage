<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/db_control.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/changekoyomi.class.php');


//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$koyomi_con = new ChangeKoyomi();


//変数の宣言
$company_id = sprintf("%012d",$_SESSION['user_id']);
$success_num = 0;
$all_num = 0;
//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];
$top4 = substr($company_id,0,4);
$mid4 = substr($company_id,4,4);
$file = '../files/'.$top4."/".$mid4."/".$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
}

$columns_num = sizeof(fgetcsv(fopen($file, "r"), 1000, ","));

ob_start();

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}
//var_dump($csv);echo "<br />\n";

$all_num = count($csv) - 1;
$csv_id = "".$_SESSION['file_name'].$company_id.rand();
//請求番号重複チェック
foreach ($csv as $value_check) {
	if ($value_check[0] == "済") {

		for ($i = 0 ;$i < $columns_num;$i++) {
			switch ($value[$i]) {
				case "済":
					$col1 = $i;
		        	break;
				case "日付":
					$col2 = $i;
		        	break;
				case "コード":
					$col3 = $i;
		        	break;
				case "会社名":
					$col4 = $i;
		        	break;
				case "販売区分":
					$col5 = $i;
		        	break;
				case "商品":
					$col6 = $i;
		        	break;
				case "数量":
					$col7 = $i;
		        	break;
				case "単価":
					$col8 = $i;
		        	break;
				case "金額":
					$col9 = $i;
		        	break;
		        default:
		        	$col10 = $i;
	        }
	    }
		$sale_date = $koyomi_con -> convJtGDate($value_check[$col2]);
		//$value_check[$col3]."-".$sale_date;

		//請求者と被請求者と請求書番号が一致するものを取得する。
		$sql = "SELECT `invoice_code` 
		FROM `INVOICE_DATA_TABLE` WHERE claimant_id = ".$company_id." 
		AND `invoice_code` = '".$value_check[$col3]."-".$sale_date."'
		";
		//echo "\r\n";
		
		//請求者と被請求者と請求書番号が一致するものがあればエラーを返して処理を終了する。
		$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
		//echo $value_check[3];
		//共通CDにスペースやハイフンがあれば消す処理
		$value_check[0] = str_replace(array("　"," ","-"),"",$value_check[0]);
		//var_dump($value_check);
		if ($invoice_code_check[0] != NULL || $value_check[3] == "" ) {
			
			$_SESSION['up_info_msg'] = "請求書番号が空欄、または重複しているため、データを登録できません。";
			unlink($file);
			header("Location:./seikyucsv", false);
			exit();
		} else if ($value_check[0] == "" && $value_check[1] == "") {
			$_SESSION['up_info_msg'] = "共通CDと管理CDが両方とも空欄のため、データを登録できません。";
			unlink($file);
			header("Location:./seikyucsv", false);
			exit();
		}

	} else if ( $value_check[0] != "共通コード") {
		//請求者と被請求者と請求書番号が一致するものを取得する。
		$sql = "SELECT `invoice_code` 
		FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$company_id." 
		AND `invoice_code` = '".$value_check[3]."'
		";
		//echo "\r\n";
		
		//請求者と被請求者と請求書番号が一致するものがあればエラーを返して処理を終了する。
		$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
		//echo $value_check[3];
		//共通CDにスペースやハイフンがあれば消す処理
		$value_check[0] = str_replace(array("　"," ","-"),"",$value_check[0]);
		//var_dump($value_check);
		if ($invoice_code_check[0] != NULL || $value_check[3] == "" ) {
			
			$_SESSION['up_info_msg'] = "請求書番号が空欄、または重複しているため、データを登録できません。";
			unlink($file);
			header("Location:./seikyucsv", false);
			exit();
		} else if ($value_check[0] == "" && $value_check[1] == "") {
			$_SESSION['up_info_msg'] = "共通CDと管理CDが両方とも空欄のため、データを登録できません。";
			unlink($file);
			header("Location:./seikyucsv", false);
			exit();
		}
	}
}

//CSVデータを一行ずつ取り出してDBに登録する。
foreach ($csv as $value) {
	//var_dump($value);echo "<br />\n";
	if ( $value[0] == "共通コード" || $value[0] == "固定" || $value[0] == "済") {
		if ($value[0] == "済") {
			for ($i = 0 ;$i < $columns_num;$i++) {
				switch ($value[$i]) {
					case "済":
						$col1 = $i;
			        	break;
					case "日付":
						$col2 = $i;
			        	break;
					case "コード":
						$col3 = $i;
			        	break;
					case "会社名":
						$col4 = $i;
			        	break;
					case "販売区分":
						$col5 = $i;
			        	break;
					case "商品":
						$col6 = $i;
			        	break;
					case "数量":
						$col7 = $i;
			        	break;
					case "単価":
						$col8 = $i;
			        	break;
					case "金額":
						$col9 = $i;
			        	break;
			        case "請求日"://ないけど必要だから入れてある
						$col10 = $i;
			        	break;
			        case "備考"://ない
						$col11 = $i;
			        	break;
			        default:
			        	$col12 = $i;
		        }
		    }
		}
	} else {
		if ($_SESSION['format'] == 1) {
			if ( $value[0] != "共通コード") {
				if ($value[0] == "") {
					$value[0] = 0;
				}

			//共通CDにスペースやハイフンがあれば消す処理
			$value[0] = str_replace(array("　"," ","-"),"",$value[0]);

			//共通コードでの企業データのヒストリカル取得
			//var_dump($value);//."<br/><br/>";
			if ($company_id != "") {
				$flag = "company_data_historical";
				$claimant_historical = $company_con -> company_sql_flag($pdo,$flag,$company_id,$cond_a);
				$c_historical = $claimant_historical[0]['historical'];
				if ($c_historical == NULL) {
					$c_historical = 0;
				}
			}

			if ($value[0] != "") {
				$flag = "company_data_historical";
				
				$destination_historical = $company_con -> company_sql_flag($pdo,$flag,$value[0],$cond_b);
				$d_historical = $destination_historical[0]['historical'];
				if ($d_historical == NULL) {
					$d_historical = 0;
				}
			}
			//var_dump($company_name);
			if ($value[1] != "") {
				$flag = "addressee_data_historical";
				if ($claimant_id != 0) {
					$cond_c = " AND `company_id` = ".$claimant_id." ";
				}
				$send_historical = $company_con -> company_sql_flag($pdo,$flag,$value[1],$cond_c);
				$s_historical = $send_historical[0]['historical'];
				if ($s_historical == NULL) {
					$s_historical = 0;
				}
			}

			
			$sql = "
				INSERT INTO `INVOICE_DATA_TABLE` (
				`claimant_id`,`destination_id`, `send_code`, `claimant_historical`, `destination_historical`, `send_historical`, `billing_date`, `invoice_code`,
				`invoice_name`, `staff_name`, `pay_date`,
				`bank_account`, `sale_date`, `product_code`,
				`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
				`sales_tax`, `withholding_tax`, `total_price`,
				`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`, `insert_date`
				) VALUES 
				( ".intval($company_id).", ".intval($value[0]).", '".str_replace(',','',$value[1])."', ".$c_historical.", ".$d_historical.", ".$s_historical.", ".intval(str_replace(array(',','/','-'),'',$value[2])).", '".$value[3]."',
				'".$value[4]."', '".$value[5]."', ".intval(str_replace(array(',','/','-'),'',$value[6])).",
				'".$value[7]."', ".intval(str_replace(array(',','/','-'),'',$value[8])).", '".$value[9]."',
				'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval(str_replace(',','',$value[12])).", '".$value[13]."',".intval(str_replace(',','',$value[14])).",
				'".intval(str_replace(',','',$value[15]))."', '".intval(str_replace(',','',$value[16]))."', ".intval(str_replace(',','',$value[17])).",
				'".$value[18]."', '".$value[19]."', '".$value[20]."', '".$csv_id."', '".$_SESSION['file_name']."', cast( now() as datetime)
				)";echo "\r\n";

				$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
				//echo $check_arr['chk']['check'];
				if ($check_arr['chk']['check']){
					$success_num += 1;
				}
				
				//var_dump($check_arr['chk']['check']);
			}
			//echo "</tr>";
		} else if ($_SESSION['format'] == 2) {
			unset($company_name2);
			if ( $value[$col3] != "" ) {
				$flag = "addressee_data_csv";
				$words = str_replace(array(" ","　"),"",$value[$col3]);
				$client_id = "";
				$company_name2 = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
				//var_dump($company_name2);
				$client_company_id = $company_name2[0]["client_company_id"];
				
				/*checkseikyucsvで実施済みの為コメントアウト20141216濱崎
				if (count($company_name2) == 0) {
					echo $sql = "INSERT INTO `ADDRESSEE_TABLE` (`client_id`, `company_name`, `company_id`) VALUES ('".$value[$col3]."','".$value[$col4]."',".$company_id.");";
					$company_con -> company_sql($pdo,$company_id,$sql);
				
					//再度行なう
					$flag = "addressee_data_csv";
					$words = str_replace(array(" ","　"),"",$value[$col3]);
					$client_id = "";
					$company_name2 = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
					//var_dump($company_name2);
					$client_company_id = $company_name2[0]["client_company_id"];

				}
				*/
				
			} 
			//var_dump($value);//."<br/><br/>";
			unset($company_name);
			if ($client_company_id != "") {
				$flag = "company_data";
				$company_name = $company_con -> company_sql_flag($pdo,$flag,$client_company_id,$words);
			}
			
			if (isset($company_name[0]["company_name"])) {
				$company_name = $company_name[0]["company_name"];
			} else {
				$company_name = "";
			}
			
			$send_code = $value[$col3];
			$sale_date_dt = $koyomi_con -> convJtGDate($value[$col2]);
			$sale_date = $koyomi_con -> datetimeToString($sale_date_dt, $string = 'Ymd');
			$invoice_code = $value[$col3]."-".$sale_date;
			$product_name = $product_code = $value[$col6];
			$quantity = $value[$col7];
			$unit_price = $value[$col8];
			$total_price_excluding_tax = $value[$col9];
			$sales_tax = $value[$col9] * 8 / 100;
			$withholding_tax;
			$total_price = $value[$col9] * 108 / 100;
			$remarks1;
			$remarks2;
			$remarks3;
			$csv_id;
			$file_name;
			$insert_date;
			$billing_date_dt = date('Ymd');
			$billing_date = $koyomi_con -> datetimeToString($billing_date_dt, $string = 'Ymd');
			
			//支払日設定
			//if (isset($value[$col10])) {
			//	$pay_date = $value[$col10];//支払日を取得していれる必要がある。
			//} else {
				$pay_date = date('Ymd', strtotime('last day of ' . date("Ymd", strtotime("+1 month"))));
			//}
			
			//共通CDにスペースやハイフンがあれば消す処理
			$value[0] = str_replace(array("　"," ","-"),"",$value[0]);
			
			$sql = "
				INSERT INTO `INVOICE_DATA_TABLE` (
				`claimant_id`,`destination_id`, `send_code`, `billing_date`, `invoice_code`,
				`invoice_name`, `staff_name`, `pay_date`,
				`bank_account`, `sale_date`, `product_code`,
				`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
				`sales_tax`, `withholding_tax`, `total_price`,
				`remarks1`, `remarks2`, `remarks3`, `csv_id`, `file_name`, `insert_date`
				) VALUES 
				( ".intval($company_id).", ".intval($client_company_id).", '".str_replace(',','',$send_code)."', ".intval(str_replace(array(',','/','-'),'',$billing_date)).", '".$invoice_code."',
				'".$value[$col4].$sale_date."', '".$staff_name."', ".intval(str_replace(array(',','/','-'),'',$pay_date)).",
				'".$bank_account."', ".intval(str_replace(array(',','/','-'),'',$sale_date)).", '".$product_code."',
				'".$product_name."', ".intval(str_replace(',','',$unit_price)).", ".intval(str_replace(',','',$quantity)).", '".$unit."',".intval(str_replace(',','',$total_price_excluding_tax)).",
				'".intval(str_replace(',','',$sales_tax))."', '".intval(str_replace(',','',$withholding_tax))."', ".intval(str_replace(',','',$total_price)).",
				'".$remarks1."', '".$remarks2."', '".$remarks3."', '".$csv_id."', '".$_SESSION['file_name']."', cast( now() as datetime)
				)";echo "\r\n";

			$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
			//echo $check_arr['chk']['check'];
			if ($check_arr['chk']['check']){
				$success_num += 1;
			}
				
			//var_dump($check_arr['chk']['check']);
		}
		//echo "</tr>";
	}
}

if ($success_num > 0) {
	//CSV取り込み履歴を記録する。
	//CSV_HISTORY_TABLEですが、ついでなのでINVOICE_DATA_TABLEのコントローラーで処理してしまいます。
	$sql = "
	INSERT INTO `CSV_HISTORY_TABLE`(
	`company_id`, `file_name`, `csv_id`
	) VALUES (
	".$company_id.", '".$_SESSION['file_name']."', '".$csv_id."'
	)";
	$invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
}

//echo "</table>";

fclose($temp);
if ($success_num > 0) {
	//$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込みました。";
	$_SESSION['comment'] = "<a href='./seikyuhakkou'>".$all_num."件中".$success_num."件のアップロードに成功しました。</a>";
}else {
	$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込めません。";
}

unlink($file);

//header("Location:./seikyucsv", false);
ob_end_flush();
//exit();

?>
<button onclick="window.location.href='./main'">戻る</button>
</body>