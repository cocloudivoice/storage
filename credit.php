<?php require("header.php");?>

<title>クレジットカード登録</title>

<?php
session_start();
echo "a";
//クラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../payment/payjp/vendor/autoload.php');
require_once(dirname(__FILE__).'/../payment/config.php');
echo "b";

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
echo "c";
//必要なコントローラーの準備
$maindb_con = new db_control();
$user_con = new user_control();
$company_con = new company_control();
\Payjp\Payjp::setApiKey(SECRET_KEY);
$token_flag = 0;

echo "d";
//各テーブルからデータを取得する
$flag = "company_data";
$words;//条件をここに入れる。
$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($company_data_arr);
$cus_id = $company_data_arr[0]['payment_id'];
$rec_id = $company_data_arr[0]['recursion_id'];
$company_name = $company_data_arr[0]['company_name'];
$email = $company_data_arr[0]['email'];

if (isset($_REQUEST['koumoku'])) {$koumoku = $_REQUEST['koumoku'];}
if (isset($_REQUEST['password'])) {$password = htmlspecialchars($_REQUEST['password'],ENT_QUOTES);}
if (isset($_REQUEST['mode'])) {$mode = $_REQUEST['mode'];}
if (isset($_REQUEST['old_plan'])) {$old_plan = $_REQUEST['old_plan'];}
if (isset($_SESSION['user_id'])) {$company_id = $_SESSION['user_id'];}

if ($old_plan != 0 && $koumoku == 0 ) {
	echo '<script type="text/javascript">';
	echo 'alert("フリープランに変更すると、データが3ヵ月分までしか保存されません。';
	echo 'また、プランの変更当月までの料金は御請求いたします。")';
	echo '</script>';
}

include_once(dirname(__FILE__).'/../payment/payjp/vendor/autoload.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	// トークン
	if (isset($_POST['payjp-token'])) {
		$token = $_POST['payjp-token'];
		$token_flag = 1;
	}


	try {
		$token_info = \Payjp\Token::retrieve($token);
		$card_id = $token_info->card->id;
		$cus_info = \Payjp\Customer::create(array(
	        "description" => $company_name,
	       	"card" => $token,
	       	"email" => $email
		));
		$cus_id = $cus_info->id;
		if (isset($cus_id)) {
			$_SESSION["comment"] = "カード情報を登録しました。";
		}
	} catch (Exception $e)  {
		echo $e ->getMessage();
	}
}

if ($cus_id != "") {
	try {
	$cu = \Payjp\Customer::retrieve($cus_id);
	$card_info = $cu->cards->retrieve($cu->cards->data[0]->id);
	$card_brand = $card_info->brand;
	$card_exp_date = $card_info->exp_year."年".$card_info->exp_month."月";
	$card_name = $card_info->name;
	$card_last4 = $card_info->last4;
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}


?>


<script>

function OpenFreeplanFrame(){
	window.scrollTo(0,0);
	var plan=document.getElementsByName("koumoku");
		document.getElementById("fadeLayer").style.visibility = "visible";
	
	for(i=0;i<3;i++){
		if (plan[i].checked && plan[i].value == '0') {
		document.getElementById("Freeplan").style.display="block";
		break;
		}
		if (plan[i].checked && plan[i].value == '1') {
		document.getElementById("Standardplan").style.display="block";
		break;
		}
		if (plan[i].checked && plan[i].value == '2') {
		document.getElementById("Premiumplan").style.display="block";
		break;
		}
	}
}


function OpenPasserrorFrame(){
	window.scrollTo(0,0);
	document.getElementById("Passerrorplan").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}

function TransPassword() {
	var password_enter = document.getElementById("password_enter").value;
	document.getElementById("password").value = password_enter;
}
/*
function onCreated(response) {
	document.querySelector('#token').innerHTML = 'Token = ' + response.id;
	alert(response);
}
*/
</script>


<iframe id="Freeplan" src="./frame/freeplan"></iframe>
<iframe id="Standardplan" src="./frame/standardplan"></iframe>
<iframe id="Premiumplan" src="./frame/premiumplan"></iframe>
<iframe id="Passerror" src="./frame/passerror"></iframe>


<?php

//プラン変更フォームが送信された場合の処理
if ($mode == "plan_change") {
//	$user_pass_arr = $user_con -> user_select_id($pdo,$user_id);
//	$pass_data = $user_pass_arr['password'];
	//var_dump($user_pass_arr);
//	if ($pass_data == md5($password)) {
		$flag = "image_upload";
		$condition = " plan_status = ".$koumoku." ";
		$company_con -> company_sql_flag($pdo,$flag,$company_id,$condition);
		$flag = "";
		date_default_timezone_set('Asia/Tokyo');
		$fp = fopen(dirname(__FILE__).'../files/common/change_plan_log/changeplan'.date('Ym').".log", "a");
		$today = getdate();

		fwrite($fp, $company_id.",".$koumoku.",".date("Ymd His")."\r\n");
		fclose($fp);

		//echo "<script type='text/javascript'>alert('プランを変更しました');</script>";
		
		
		
//	} else {
//		echo "<script type='text/javascript'>window.onload=alert('パスワードが一致しません');</script>";
//		echo '<div onload="OpenPasserrorFrame();" ></div>';
//	}
}

$flag = "company_data";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan_status = $company_arr[0]['plan_status'];

?>

<article>

	<section id="m-1-box">
		<h2>
			クレジットカード登録
		</h2>
		
		<p id="plan">
			クレジットカード情報を登録します。<br>
			フリープランのまま、郵送代行やPDF変換を利用したい場合は<br>
			ここで登録してください。<br>
			（<a href="https://pay.jp//" target="_blank">Pay.JP</a>の入力フォームが表示されます）
		</p>

		<form action="../payment/customer" method="post">
		<!--<form action="" method="post">-->

		<script
		  type="text/javascript"
		  src="https://checkout.pay.jp/"
		  class="payjp-button"
		  data-key=<?php print(PUBLIC_KEY); ?>
		  data-on-created="onCreated"
		  data-text="クレジットカード情報を入力する"
		  data-submit-text="クレジットカード情報を入力する"
		  data-partial="true">
		</script>

		<input id="input_btn" type="button" onclick="submit();" value="カード情報を登録する" style="margin-top:20px;">
		</form>
		<?php
			echo "<p class='red'>".$_SESSION["comment"]."</p>";
			$_SESSION["comment"] = "";
		?>
		<style type="text/css">
			#cardinfo {width:500px;height:50px;margin-top:20px;}
			#cardinfo table {width:500px;height:50px;}
			#cardinfo table,#cardinfo th,#cardinfo td {border-collapse: collapse;border:1px solid #333;}
			#cardinfo th {border:solid black 1px;text-align:center;}
			#cardinfo td {border:solid black 1px;cellspacing:0px;text-align:center;}
			
		</style>
		<div id="cardinfo">

			<p class="mleft10">現在のカード</p>
			<table>
				<tr><th>ブランド</th><th>有効期限</th><th>名義</th><th>下4桁</th></tr>
				<tr><td><?php echo $card_brand;?></td><td><?php echo $card_exp_date;?></td><td><?php echo $card_name;?></td><td>****-****-****-<?php echo $card_last4;?></td></tr>
			</table>
		</div>

	</section>
</article>

<?php require("footer.php");?>
<div id="fadeLayer" onclick="CloseFrame(6)"></div>
<div id="noneLayer"></div>
