<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
    <title>webmail</title>
</head>
<body>

<?php

	mb_language("ja");
	mb_internal_encoding("UTF-8");
	
    require_once dirname(__FILE__) . '/aaMailListInc.php';

    //-------------------------------
    //パラメータ取得
    //-------------------------------
    $no = $_GET['no'];
    if (empty($no) || !is_numeric($no)) {
        die("エラーです。");
    }

    //-------------------------------
    //接続＆ログイン
    //-------------------------------
    require_once dirname(__FILE__) . '/aaMailListConnect.php';



    //-------------------------------
    //メール詳細を表示
    //-------------------------------
    fputs($fp, "RETR $no\r\n");
    
    do {
        $line = fgets($fp, 512);

        if (preg_match("/^Subject/", $line)) {
            //件名を表示する
            //取りあえず件名くらいはデコードしてあげようかと。
            //mb_convert_encoding($line, $enc, "auto");
            print $subject = mb_decode_mimeheader($line) . "<br>";
			//print mb_convert_encoding($subject, $enc, "auto") . "<br>";
        } else {
            //件名以外を表示する
            print mb_convert_encoding($line, "UTF-8", "auto") . "<br>";
            
            //Message-Idを取得する＆保存
            if(preg_match("/^Message-Id/", $line)){
                //Message-Id抽出
                preg_match('/<(.*)>/', $line, $msgIdAry);
                $messageId = $msgIdAry[1];

                //既読リスト取得
                if(file_exists($arl_file)){
                    $alreadyReadList = unserialize(file_get_contents($arl_file));
                }else{
                    $alreadyReadList = array();
                }

                //既読リストに追加
                $alreadyReadList[$messageId] = "既読";

                //既読リスト保存
                file_put_contents($arl_file, serialize($alreadyReadList));
            }
            
            
            
        }

    } while (!preg_match("/^\.\r\n/", $line));


    //切断
    require_once dirname(__FILE__) . '/aaMailListDisconnect.php';
?>

</body>
</html>