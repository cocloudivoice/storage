<?php
session_start();

//処理制限時間を外す
set_time_limit(0);

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
//$path = $_REQUEST['path'];

$received_date = $_REQUEST['received_date'];//受付番号
$ac_no = $_REQUEST['ac_no'];//受付番号
$cs_name = $_REQUEST['cs_name'];//顧客名
//$cs_name2 = $_REQUEST['cs_name2'];//請求者から省く顧客名の一部
$cs_no = $_REQUEST['cs_no'];//顧客番号
//$p_num = $_REQUEST['p_num'];//申告通数
$ind_num = $_REQUEST['ind_num'];//実通数
$mail_send = $_REQUEST['mail_flag'];//メールフラグ

setlocale(LC_ALL, 'ja_JP.UTF-8');

mb_language("ja");
mb_internal_encoding("UTF-8");

if ($mail_send == 1 && $cs_name != "" && $cs_no != "") {
	
    //$email= "arisa_nagano@mjs.co.jp,ayaka_tamaki@mjs.co.jp";
	$email = "bizsky_office@mjs.co.jp,cc@cloudinvoice.co.jp";
	//$email = "akira.hamasaki@cloudinvoice.co.jp";//テスト用
	$headers = "From:".mb_encode_mimeheader("クラウドインボイス 楽たす事務係","ISO-2022-JP-MS")."<info@cloudinvoice.co.jp>";
	$subject = "請求書が到着しました。【クラウドインボイス 楽たす事務係】" ;
	$message =
"ミロク情報サービス
和田様

お世話になっております。
クラウドインボイスの楽たす事務係です。

請求書が到着致しましたのでご連絡いたします。
到着日：".$received_date."
顧客名：".$cs_name."
顧客番号：".$cs_no."
請求書実数：".$ind_num."
	
以上、よろしくお願い致します。

";
	
	$message = str_replace(array("\r\n","\r"),"\n",$message);
	//$message = mb_convert_encoding($message,"ISO-2022-JP-MS");
	//mail($email,$subject,$message,$headers);
	$message = str_replace(array("\r\n", "\r"),"\n",$message);
	$result_mail = mb_send_mail($email,$subject,$message,$headers);
	//mail($email,mb_encode_mimeheader($subject, 'ISO-2022-JP-MS'),mb_convert_encoding($message, 'SJIS'),$headers);
	if ($result_mail == true) {
		$_SESSION['mail_comment'] = "メールを送信しました。";
	} else {
		$_SESSION['mail_comment'] = "メール送信に失敗しました。";
	}
} else {
	$_SESSION['mail_comment'] = "メール送信に失敗しました。";
}

//テスト用。仮にここで止める。
//exit;

header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://storage.cloudinvoice.co.jp/invoice/rakutasu_imgs_to_text_upload_all'</script>";
echo "</html>";
echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./rakutasu_imgs_to_text_upload_all\"'>";
exit;

?>