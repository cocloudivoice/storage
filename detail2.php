<?php
session_start();

//アナリティクス用のファイル読み込み
include_once('/var/www/analytics/analyticstracking.php');

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/sitecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/sitetitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datadispcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datatitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/reviewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/viewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/boardcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/freespacecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/ochatcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/reviewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/allpurposecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$site_con = new site_control();

//変数
$user_id;
$site_id;
$comment_id;
$edit_on = 0;
$ranking = 0;
$page = 1;
$bpage = 1;
$new_writer;
$new_article;
$new_comment_id;
$new_check;
$login_user_id;
$nick_name;

//コメントの変数
$comment;
$rec_comment_id;
$comment_data=array();

//検索用変数
$flag;
$words;

//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
}
//掲示板のページ番号を取得
if (isset($_GET['bpage'])) {
	$bpage = htmlspecialchars($_GET['bpage'], ENT_QUOTES);
}
//site_idからuser_idを取得
if(isset($_GET['site_id'])) {
	$_SESSION['site_id'] = $site_id = htmlspecialchars($_GET['site_id'], ENT_QUOTES);
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else if (isset($_SESSION['site_id'])) {
	$site_id = $_SESSION['site_id'];
	$site_get_arr = $site_con->site_get_user($pdo,$site_id);
	$user_id = $site_get_arr['user_id'];
	$site_get_arr = $site_con->site_select($pdo,$user_id,$site_id);
} else {
	header("Location:./index.php");
}
//var_dump($_SESSION);
//編集権限確認
if(isset($_SESSION['login_user_id'])) {
	if($_SESSION['login_user_id'] == $user_id) {
		if ($_SESSION['login_user_id'] == "") {
			$edit_on = 0;
			//	echo "ユーザーの画面<br />\n";
		} else {
		
			$edit_on = 1;
			//	echo "オーナーの画面<br />\n";
		}
	} else {
		$edit_on = 0;
		//	echo "ユーザーの画面<br />\n";
	}
}

$site_id_rock = 'site_id' . (string)$site_id;

//サイト公開チェック
if ($edit_on != 1){
	if ($site_get_arr['open'] != 1) {
		header("Location:./index.php",FALSE);
	}
}

//ログインチェック
if (isset($_SESSION['login_user_id'])) {
	$login_user_id = $_SESSION['login_user_id'];
	$nick_name = $_SESSION['nick_name'];
}

//ビューカウント追加
$view_con = new view_control();
if (!$_SESSION[$site_id_rock]){
	$view_arr = $view_con->view_select($pdo,$user_id,$site_id,'0');
	$view_arr['user_id'] = $user_id;
	$view_arr['site_id'] = $site_id;
	$view_arr['record_id'] = '0';
	$view_arr['view_count'] = (int)$view_arr['view_count'] + 1;
	$result = $view_con->view_insert($pdo,$view_arr);
	$_SESSION[$site_id_rock] = 1;
}

$site_basis;
$record_kbn = '1';
$record_kbn4 = '4';
$disp_index_num_arr = array();
$record_id_from = $record_id;
$record_id_to = "";
$sort = "";
$desc = "";
$data_prop;
$col_size;
$cols = 0;
$viewrank = array();
$data_seq_arr = array();
$data_property_arr = array();
$data_disp_arr = array();
$site_update_arr = array();
$page_conf;
$bsort = "comment_id";//掲示板コメントのソートキー
$bdesc;
$bcom_disp_num;
$free_space_kbn = 1;//フリースペース用
$free_record_id = 0;
$free_kbn = 1;
$free_space_record;
$free_delete;
$free_space_a;
$free_space_b;
$free_space_record_a;
$free_space_record_b;
$reply_to;

//ochatの変数
$o_reply_to;
$ochat_writer;
$ochat_article;
$ochat_comment_id;
$ochat_link_comment_rank;
$chat_bg_img;

//ソートの設定
if (isset($_GET['sort_num']) && $_GET['sort_num'] != NULL) {
	$sort_num = htmlspecialchars($_GET['sort_num'],ENT_QUOTES);
	$sort = "data_body".$sort_num;
} else {
	$sort = "display_rank";//$_GET['sort_name'];//GETか何かでカラム名取得を実装する。
}

if (isset($_GET['sort']) && $_GET['sort'] != NULL) {
	$sort = htmlspecialchars($_GET['sort'],ENT_QUOTES);
}
if (isset($_GET['order']) && $_GET['order'] != NULL) {
	$desc = htmlspecialchars($_GET['order'],ENT_QUOTES);
}
if (isset($_GET['data_prop']) && $_GET['data_prop'] != NULL) {
	$data_prop_detail = $data_prop = $_GET['data_prop'];
}
if ($_REQUEST['sort_btn'] == 1) {
	if ($desc =="asc") {
		$desc ="desc";
	} else {
		$desc = "asc";
	}
} else {
	if ($desc == "desc") {
		$desc = "desc";
	} else {
		$desc = "asc";
	}
}

//オーナーチャットの取得情報制御
$ochat_sort = "comment_id";
$ochat_desc = "DESC";
$ochat_limit = 10;
$ochat_offset = 0;
$ochat_present_id;

//オーナーチャットの投稿取得
if (isset($_REQUEST["ochat_article"])) {
	$ochat_link_comment_rank = $_REQUEST["o_reply_to"];
	$ochat_writer = $_REQUEST["ochat_writer"];
	$ochat_article = $_REQUEST["ochat_article"];
	$ochat_comment_id = $_REQUEST["ochat_comment_id"];
	$ochat_present_id = $_REQUEST["ochat_present_id"];
}

//掲示板コメントの投稿取得
if (isset($_REQUEST['new_comment_id'])) {
	$new_comment_id = htmlspecialchars($_REQUEST['new_comment_id'],ENT_QUOTES);
}
if (isset($_REQUEST['new_writer'])) {
	$_SESSION['writer'] = $new_writer = htmlspecialchars($_REQUEST['new_writer'],ENT_QUOTES);
}
if (isset($_REQUEST['new_article'])) {
	$new_article = htmlspecialchars($_REQUEST['new_article'],ENT_QUOTES);
}
if (isset($_REQUEST['comment_id'])) {
	$comment_id = $_REQUEST['comment_id'];
}
if (isset($_REQUEST['reply_to'])) {
	$link_comment_rank = $_REQUEST['reply_to'];
}
if (isset($_REQUEST['new_check'])) {
	$new_check = htmlspecialchars($_REQUEST['new_check'],ENT_QUOTES);
}

//フリースペースデータの上下移動処理
if (isset($_REQUEST['free_space_record_a'])) {
	$free_space_record_a = $_REQUEST['free_space_record_a'];
}
if (isset($_REQUEST['free_space_record_b'])) {
	$free_space_record_b = $_REQUEST['free_space_record_b'];
}
if (isset($_REQUEST['free_space_rank_a'])) {
	$free_space_rank_a = $_REQUEST['free_space_rank_a'];
}
if (isset($_REQUEST['free_space_rank_b'])) {
	$free_space_rank_b = $_REQUEST['free_space_rank_b'];
}

//掲示板のコメント取得
$board_arr = array(
'user_id' => $user_id,
'site_id' => $site_id,
'check' => $new_check,
'writer' => $new_writer,
'article' => $new_article,
'comment_id' => $new_comment_id,
'link_comment_rank' => $link_comment_rank
);
//var_dump($board_arr);

//フリースペースの記事取得
$freesp_arr = array(
'user_id' => $user_id,
'site_id' => $site_id,
'free_space_kbn' => $free_space_kbn
);

//オーナーチャットのコメント取得
$ochat_arr = array(
'user_id' => $user_id,
'site_id' => $site_id,
'writer' => $ochat_writer,
'article' => $ochat_article,
'comment_id' => $ochat_comment_id,
'link_comment_rank' => $ochat_link_comment_rank,
'present_id' => $ochat_present_id
);
//var_dump($ochat_arr);

//表示数の制御
$max_disp = 10;
$offset = ($page - 1) * $max_disp;
if (isset($data_prop) && $data_prop == 3) {
	$sort_sentence = " CAST(".$sort." AS UNSIGNED) ".$desc." limit ".$max_disp." offset ".$offset." ";
} else {
	$sort_sentence = " ".$sort." ".$desc." limit ".$max_disp." offset ".$offset." ";
}
//掲示板の表示数の制御
$blimit = $bmax_disp = 1000;//掲示板のコメント表示数
$boffset = ($bpage - 1) * $bmax_disp;
//dbコントローラー準備
$con_db = new dbpath();
$sitedb = new site_control();
$dispdb = new data_disp_control();
$titledb = new data_title_control();
$datadb = new data_control();
$sitetitledb = new site_title_control();
$reviewdb = new review_control();
$viewdb = new view_control();
$board_con = new board_control();
$free_con = new freespace_control();
$user_con = new user_control();
$ochat_con = new ochat_control();
$review_con = new review_control();
$all_purpose_con = new all_purpose_control();
	
//表の根拠のアップデートデータ取得
if (isset($_REQUEST['site_basis'])) {
	$site_update_arr['update'] = "'".htmlspecialchars($_REQUEST['site_basis'],ENT_QUOTES)."'";
	$site_update_arr['column'] = "site_basis";
	$site_update_arr['site_id'] = $site_id;
	//表の根拠の処理
	//var_dump($site_update_arr);
	//var_dump($site_update_arr);
	$sitedb -> site_update($pdo,$site_update_arr);
}

//表の根拠のアップデートデータ取得
if (isset($_REQUEST['site_bg_img'])) {
	$site_update_arr['update'] = "'".htmlspecialchars($_REQUEST['site_bg_img'],ENT_QUOTES)."'";
	$site_update_arr['column'] = "site_bg_img";
	$site_update_arr['site_id'] = $site_id;
	//表の根拠の処理
	//var_dump($site_update_arr);
	//var_dump($site_update_arr);
	$sitedb -> site_update($pdo,$site_update_arr);
}


//タグのアップデートデータ取得
if (isset($_REQUEST['tag'])) {
	$site_update_arr['update'] = "'".htmlspecialchars($_REQUEST['tag'],ENT_QUOTES)."'";
	$site_update_arr['column'] = "tag";
	$site_update_arr['site_id'] = $site_id;
	//タグのアップデートの処理
	//var_dump($site_update_arr);
	$sitedb -> site_update($pdo,$site_update_arr);
}

//ポジションテーブルから当該カラム名とランク等の情報を取得
$view_sum_data = $viewdb -> view_sum($pdo,$user_id,$site_id);
$site_data = $sitedb -> site_select($pdo,$user_id,$site_id);
$disp_data = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn);
$disp_data4 = $dispdb -> data_disp_select($pdo,$user_id,$site_id,$record_kbn4);
$title_data = $titledb -> data_title_select($pdo,$user_id,$site_id);
$body_data = $datadb -> data_select_all($pdo,$user_id,$site_id,$record_id_from,$record_id_to,$sort);
$site_title_data = $sitetitledb -> site_title_select($pdo,$user_id,$site_id);
$data_num = $datadb -> data_count($pdo,$user_id,$site_id);
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$ochat_con -> ochat_insert_new_comment($pdo, $ochat_arr);
$ochat_data = $ochat_con -> ochat_select_all($pdo, $user_id, $site_id, $ochat_sort, $ochat_desc, $ochat_limit, $ochat_offset);
$o_comment_num = $ochat_con -> ochat_count($pdo,$user_id);
asort($ochat_data);
/*
//集計関連処理
	#View合計
	$flag = "sum_view";
	$all_purpose_arr[0]["SUM( `view_count` )"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

	#コメント数
	$flag = "sum_comment";
	$all_purpose_arr[0]["count( * )"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

	#表の信頼度に投票した人の数取得
	$flag = "trust_table";
	$all_purpose_arr[0]["good + bad"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#表を信頼できるとした人の数
	$flag = "trust_num";
	$all_purpose_arr[0]["good"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#表を信頼できるとした人の率
	$flag = "trust_ratio";
	
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo round($all_purpose_arr[0]["good / ( good + bad ) *100"],1);

	#一致するサイトタイトル
	$flag = "search_record_title";
	$words = "筑波大学付属小学校";
	
	//["record_photo"]
	//["evaluate"]
	//["record_basis"]
	//件数取得
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	echo	$all_purpose_arr[$i]["site_id"];
	}
	#☆5つ集計
	$flag = "sum_evaluate_rank5";
	$words = "筑波大学付属小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);

	#☆4つ集計
	$flag = "sum_evaluate_rank4";
	$words = "筑波大学付属小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#☆3つ集計
	$flag = "sum_evaluate_rank3";
	$words = "筑波大学付属小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#☆2つ集計
	$flag = "sum_evaluate_rank2";
	$words = "筑波大学付属小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#☆1つ集計
	$flag = "sum_evaluate_rank1";
	$words = "筑波大学付属小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	var_dump($all_purpose_arr);

	#レコードタイトルの平均評価を取得する。
	$flag = "ave_evaluate";
	$words = "筑波大学付属小学校";
	$all_purpose_arr[0]["avg( evaluate )"];
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	
	
	#レコードタイトルが一致するレビューの数を取得する。
	$flag = "review_num";
	$words = "筑波大学付属小学校";
	//$words = "1\""." or record_title like \"%小学校%";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	echo $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	echo	$all_purpose_arr[$i]["count( evaluate )"];
	}
	//件数取得
	$test=count($all_purpose_arr);

	#サイトタイトルでのあいまい検索
	$flag = "site_title_search";
	$words = "小学校";
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);

	echo $num_elm = count($all_purpose_arr);
	for ($i = 0; $i < $num_elm; $i++) {
	echo	$all_purpose_arr[$i]["site_id"];
	}
	//["site_title"]
	//["imgurl"]
	//["introduction"]
	//件数取得
	
	$all_purpose_arr = $all_purpose_con -> all_purpose_sql_flag($pdo,$flag,$site_id,$words);
	
	var_dump($all_purpose_arr);
	echo $test=count($all_purpose_arr);
*/
//var_dump($review_data);

//var_dump($site_data);
//チャット関連の処理
if (isset($site_data['chat_bg_img'])) {
	$chat_bg_img = $site_data['chat_bg_img'];
}
//フリースペースの記事削除処理
if ($_REQUEST['freeDelete'] == "削除") {
	$free_space_record = htmlspecialchars($_REQUEST['free_space_record'],ENT_QUOTES);
	$free_con -> freespace_delete($pdo,$site_id,$free_space_record);
}

//フリースペースデータの上下移動処理
if ($_REQUEST['freeUp'] == "▲" || $_REQUEST['freeDown'] == "▼") {
	$free_con -> freespace_updown($pdo,$site_id,$free_space_record_a,$free_space_record_b,$free_space_rank_a,$free_space_rank_b);
}

//フリースペースの処理
$free_data = $free_con -> freespace_select_all($pdo,$freesp_arr);
$free_data_num = $free_con -> freespace_count($pdo,$freesp_arr);
$fdata_num = $free_data_num - 1;
$free_rank_end = $free_data[$fdata_num]['free_space_rank'];
$free_positon = $free_rank_end + 1;

//掲示板の削除処理
if ($_REQUEST['board_delete'] == "削除") {
	$board_con -> board_delete($pdo,$user_id,$site_id,$comment_id);
}
//掲示板の処理
$board_con -> board_insert_new_comment($pdo,$board_arr);
$comment_num = $board_con -> board_count($pdo,$user_id,$site_id,$bsort,$bdesc);
$board_data = $board_con -> board_select_all($pdo,$user_id,$site_id,$bsort,$blimit,$boffset,$bdesc);
$bpage_all = ceil($comment_num/$bmax_disp);
//var_dump($board_data);
if ($board_data[0]['user_id'] == null) {
	$bcom_disp_num = 0;
} else {
	$bcom_disp_num = count($board_data);
}

//新しいコメントIDを取得する処理
$add_num = $comment_num - 1;
$new_com_id= $board_data[$add_num]['comment_id'] + 1;

$trust_num = $site_data['good'];
$trust_sum = $trust_num + $site_data['bad'];
$trust_ratio = ceil(($trust_num/$trust_sum) * 100);

//$sql = "SELECT * FROM BOARD_TABLE";
//$board_data2 = $board_con->board_sql($pdo,$site_id,$sql);
//var_dump($board_data2);echo "owari";

for($i = 0;$i < 50;$i++){
	if($i>0 && $i<10){
		$i = '0'.$i;
	}
	if ($disp_data['data_disp'.$i.''] == 1){
		$cols++;
	}
}

//var_dump($disp_data);

//ページ数取得
$page_num = ceil($data_num/$max_disp);
//var_dump($body_data);
if ($body_data[0]['user_id'] == NULL) {
	$row_num = 2;
} else {
	$row_num = count($body_data);
}
//ランキングありなあし
$ranking = $site_data['rank_define'];

//最大行数
if($_SESSION['max_row']) {
	$max_row = $_SESSION['max_row'];
} else {
	$max_row = $data_num;
}

if ($ranking == 1) {
	$cols += 6;
}else{
	$cols += 3;
}
$col_size = $cols;

//data_dispカラムに1（表示）が入っているカラムの末尾番号を取得し、表示用の配列を作る。
for($i = 1;$i <= 50;$i++){
	if($i < 10) {
		$num = "0{$i}";
	} else {
		$num = "{$i}";
	}
	//ここでdata_dispカラムが0(非表示)でなければ、末尾番号を取得して必要なデータのカラム名を
	//末尾番号を付与してつくり、それを表示用のそれぞれの配列に入れる。
	if($disp_data['data_disp'.$num] != 0) {
		$disp_index_num_arr = $num;
		$data_seq_arr[$num]= $disp_data['data_seq'.$num];
		$data_title_arr[$num] = $title_data['data_title'.$num];
		for($j = 0;$j < $data_num;$j++) {
			$j;
		}
	}
}
//var_dump($disp_data);
//var_dump($data_seq_arr);

if ($_SESSION['max_col']){
	$max_col = $_SESSION['max_col'];
}else{
	$max_col = $max_col_first;
}
for($i = 1;$i <= 50;$i++){
	if($i < 10) {
		$num = "0{$i}";
	} else {
		$num = "{$i}";
	}
}

$site_title = $site_title_data['site_title']."比較 - みんなでつくるソーシャル比較表キュレーションサイト [比較.jp]";
//var_dump($disp_data4);
//echo "<br/>";
//echo $disp_data4['data_seq02'];
?>

<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://ogp.me/ns/fb#">
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>
-->


<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $site_title_data['site_title']; ?>比較,<?php echo $site_title_data['titletag01'];echo ',';echo $site_title_data['titletag02'];echo ',';echo $site_title_data['titletag03'];echo ',';echo $site_title_data['titletag04'];echo ',';echo $site_title_data['titletag05'];echo ',';echo $site_data['tag1'];echo ',';echo $site_data['tag2'];echo ',';echo $site_data['tag3'];echo ',';echo $site_data['tag4'];echo ',';echo $site_data['tag5'];echo ',';echo $site_data['tag6'];echo ',';echo $site_data['tag7'];echo ',';echo $site_data['tag8'];echo ',';echo $site_data['tag9'];echo ',';echo $site_data['tag10'];echo ','; ?>,ランキング,カフェ,家電,テレビ,買い物,ショッピング,パソコン,ノートパソコン,タブレット,iphone,食品,食べ物,果物,特産品,服,インテリア,雑貨,小物,金融,証券,FX,投資,冷蔵庫,洗濯機" /> 
		<meta name="description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />		
		<title><?php echo $site_title_data['site_title']; ?>比較 - [比較.jp]</title>
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="<?php echo $site_title; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://hikaku.jp/index.php" />
		<meta property="og:image" content="http://hikaku.jp/common/images/logo.jpg" />
		<meta property="og:site_name" content="<?php echo $site_title; ?>" />
		<meta property="og:description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="http://hikaku.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://hikaku.jp/" />
		<script src="http://hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>
		<script type="text/javascript">
			$(function() {
				$("input#change").each(function() {
					var chk = $(this).attr('checked');
					if (chk == true) {
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					} else {
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
				$("input#change").click(function() {
					var chk = $(this).attr('checked');
					if (chk == true) {
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					} else {
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
			});
		</script>
		<script type="text/javascript">

			$(function() {
			    $('#comBox').submit(function() {
			        var flag = 0;

			        // フォームチェック
			        $(this).find('textarea').each(function() {
			            if ($(this).val() == '') {

			                // NGの場合
			                alert("コメントが空白です。");
			                flag = 1;
			                return false; // break
			            }
			        });
			        if (flag) {
			            return false; // return
			        }
			    });
			});

			$(function() {
			    $('#comBox2').submit(function() {
			        var flag = 0;

			        // フォームチェック
			        $(this).find('textarea').each(function() {
			            if ($(this).val() == '') {

			                // NGの場合
			                alert("コメントが空白です。");
			                flag = 1;
			                return false; // break
			            }
			        });
			        if (flag) {
			            return false; // return
			        }
			    });
			});

			$(function() {
			    $('#ochat_box').submit(function() {
			        var flag = 0;

			        // フォームチェック
			        $(this).find('textarea').each(function() {
			            if ($(this).val() == '') {

			                // NGの場合
			                alert("コメントが空白です。");
			                flag = 1;
			                return false; // break
			            }
			        });
			        if (flag) {
			            return false; // return
			        }
			    });
			});

			$(function() {
			    $('#basisBox').submit(function() {
			        var flag_basis = 0;

			        // フォームチェック
			        $(this).find('textarea').each(function() {
			            if ($(this).val() == '') {

			                // NGの場合
			                alert("コメントが空白です。");
			                flag_basis = 1;
			                return false; // break
			            }
			        });
			        if (flag_basis) {
			            return false; // return
			        }
			    })
			});
		</script>
		<script type="text/javascript">
			/*ここからコメントの表示の操作をするスクリプト*/
			$(function() {
				$(.disp1).style;
			});
		</script>
		<script type="text/javascript">
			var basis=0;

			function changeBasisBox() {

				if (basis == 0) {
					document.getElementById('basisBox').style.display = '';
					document.getElementById('basis_edit').value = '隠す';
					basis = 1;
				} else {
					document.getElementById('basisBox').style.display = 'none';
					document.getElementById('basis_edit').value = '編集';
					basis = 0;
				}
			}

			function reply(com_num){
				var rep1 = document.getElementById('reply_to').value = com_num;
				document.getElementById('reply_to_low').value = com_num;
				var rep2 = document.getElementById('reply_to2').innerHTML = ">>"+com_num;
				document.getElementById('reply_to_low2').innerHTML = ">>"+com_num;
				window.location.href = "#comBox";
			}

			function tooltip(com_num_to,com_num_from){
				var tip = document.getElementById(com_num_to).innerHTML;
				var tip2 = document.getElementById('tool_'+ com_num_from).innerHTML = tip;
			}

			function o_reply(com_num){
				var rep1 = document.getElementById('o_reply_to').value = com_num;
				var rep2 = document.getElementById('o_reply_to2').innerHTML = ">>"+com_num;
				window.location.href = "#ochat_box";
			}

			function good_add() { //追加処理
					document.getElementById("GoodBox").innerHTML = '<input type="hidden" name="judge" value="good"><input type="hidden" name="table_name" value="USER_TABLE">';
					form3.submit();
			}
			function bad_add() { //追加処理
				document.getElementById("GoodBox").innerHTML = '<input type="hidden" name="judge" value="bad"><input type="hidden" name="table_name" value="USER_TABLE">';
					form3.submit();
			}

			function good_add_trust() { //追加処理
					document.getElementById("trustBox").innerHTML = '<input type="hidden" name="judge" value="good"><input type="hidden" name="table_name" value="SITE_TABLE">';
					form_trust.submit();
			}
			function bad_add_trust() { //追加処理
				document.getElementById("trustBox").innerHTML = '<input type="hidden" name="judge" value="bad"><input type="hidden" name="table_name" value="SITE_TABLE">';
					form_trust.submit();
			}

		</script>
		<script>
		function com_disp() {
			
			for(var k = 0; k < <?php echo $row_num; ?>; k++) {
				var cnt = document.getElementById('com_disp' + k);
			}
				alert(cnt);
				//setTimeout()を含む関数を呼び出す
				setTimeout("com_disp()",5000);
			
		}
		/*
		$(function(){
			com_disp();
		});
		*/
		</script>

<style type="text/css">
	
/*初期化設定*/
html * {
 margin:0px;
 padding:0px;
}
/*初期化設定*/

div#clear{clear:both;}
div#clearleft{clear:left;}
body {margin:0px; padding:0px; font: 13px/20px "serif";}
div#main-box{width:1180px; margin:auto;}

h1 {font:bold 18px/30px "serif"; margin:10px 0 5px 0;}
a{color:#0000ff; text-decoration:none;}


.bol{font-weight:bold;}
.red{font-weight:bold; color:red;margin-right:10px;}
.blue{font-weight:bold; color:blue;}
.black{font-weight:bold; color:black;}
.tag{font:lighter 13px/22px "ＭＳ 明朝";color:rgb(30,30,30); padding:0 6px 0 0;}
.gray{color:rgb(100,100,100); font-size:11px;}
.aqua{color:rgb(0,150,150); font-size:11px;}
.cblack{margin:0 0 0 10px; font-size:15px; line-height:25px;}
.cred{margin:0 0 0 10px; font-size:15px; line-height:25px; color:red;}
.cblue{margin:0 0 0 10px; font-size:15px; line-height:25px; color:blue;}
.comname{color:green; font-weight:bold;}
.time{}
.invisible { display:none;}

div#h-box{color:white; background-color:black; line-height:35px;}
div#h-box a{color:white; text-decoration:none; margin:0 0 0 10px;}

div#hl-box{float:left; width:500px;}

div#hr-box{text-align:right; margin:0 10px 0 0;}
.popup {display: none;top:35px;left: 1062px; position:absolute; width:200px; height:280px; z-index:1;}
nobr:hover .popup {display:inline; background-color:black;}


div#m1-box{height:368px; width:1180px; border-bottom:2px rgb(255,202,213) solid;}


div#m1l-box{height:368px; width:800px;float:left;border-right:1px rgb(255,202,213) solid;}

div#m1l1-box{height:180px; width:780px; padding:0 10px 0 10px; border-bottom:1px rgb(255,202,213) solid;}

div#m1l2-box{height:70px; width:800px; margin:10px 0 10px 10px;}
div#m1l2-box img{float:left; border:0; width:100px; margin:0 10px 0 0;}
div#m1l2-box p{font: 15px/25px "serif"; padding:6px 0 0 0;}

div#m1l3-1-box{float:left; height:53px; width:100px; background-color:rgb(234,234,234); text-align:center; padding:10px 0 0 0; border-radius:5px; margin:0 10px 0 10px;}

div#m1l3-2-box{height:53px; width:600px; padding:6px 0 0 0;}

div#m1r-box{width:1180px;background-color:white;}


div#m2-box{width:1180px;height:400px; border-bottom:1px rgb(255,202,213) solid; border-top:1px rgb(255,202,213) solid;background-color:white;}


div#m2l-box{width:300px;height:390px; float:left; border-right:1px rgb(255,202,213) solid;text-align:center;padding-top:10px;}

div#m2l-box img{width:280px; border:0; margin:10px 0 0 10px;}

div#m2l-box h2{font:bold 15px "serif"; text-align:center; margin:10px 0 0 0;}

div#m2l-box p{margin:30px 0 0 10px;}

div#m2l1-box{margin: 10px 0 0 0;}

div#m2l1-box img{width:100px; margin:0 0 0 10px; float:left; vertical-align:middle;}

div#m2l1-box p{margin:0 0 0 20px;}



div#m2m-box{width:563px; height:400px; float:left; border-right:1px rgb(255,202,213) solid; padding:0 0 0 5px;}

div#m2m-box img{width:40px; float:left;}

div#m2m1-box {margin:5px 0 0 0;}

div#m2m1-box img{width:96px;height:20px; float:left; margin: 0 10px 0 0;}

div#m2m1-box p{font-weight:bold; margin:0;}

div#m2m2-box{height:100px; margin:5px 0 0 2px;}

div#m2m2-box p{margin:0;}

div#m2m3-box{}

div#m2m3-box p{margin:0 250px 0 0; float:left;}

div#m2m4-box{margin:5px 0 0 0;}

div#m2m4-box input[type=text]{border:#a9a9a9 1px solid; width:350px; height:20px;margin:0; float:left;}

div#m2m4-box button[type=comment]{ cursor:pointer;border:none; width:205px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px;}

div#m2m5-box{border:1px rgb(255,202,213) solid; margin:10px 5px 0 0; height:121px; border-radius:10px;}

div#m2m5-1-box{height:103px;}

div#m2m5-2-box{text-align:right; font-size:11px;}

div#m2r-box{height:400px; margin: 0 0 0 876px;line-height:1.2em;padding:10 10 5 10;}

div#m3-box{height:50px; margin:10px 0 0 0; text-align:center;background-color:white;}

div#m3-box button[type=yn]{ cursor:pointer;border:none; width:60px; height:24px; margin:0; background-color:orange; color:white; font-weight:bold; font-size:13px; letter-spacing:1px; border-radius:5px;}


div#m4-box{margin:15px 0 0 0;background-color:white;}

div#m4-1-box{background-color:rgb(0,0,92); height:30px; width:1170px; color:white; font-weight:bold; font-size:20px; line-height:27px; padding:0 0 0 10px; letter-spacing:1px;}

div#m4-2-box{background-color:rgb(240,240,240); width;1180px; height:auto; overflow:hidden;}

div#m4-2-1box{font-size:19px; font-weight:bold;  margin:0 150px 0 150px; padding:5px 0 0 0; line-height:35px; weight:800px; border-bottom:1px rgb(120,120,120) solid; color:rgb(60,60,60)}

div#m4-2-2box{margin:15px 0 0 300px;}

div#m4-2-2box p{float:left; margin:0;}

div#m4-2-2box input[type=namae]{border:rgb(120,120,120) 1px solid; width:180px; height:22px; margin:0 0 0 10px;}

div#m4-2-3box{margin:10px 0 0 287px;}

div#m4-2-3box p{float:left; margin:0;}

div#m4-2-3box input[type=comment]{border:rgb(120,120,120) 1px solid; width:450px; height:150px; margin:0 0 0 10px;}

div#m4-2-4box{font-size:19px; font-weight:bold;  margin:30px 150px 10px 150px; padding:5px 0 0 0; line-height:35px; weight:800px; border-bottom:1px rgb(120,120,120) solid; color:rgb(60,60,60)}

div#m4-2-5box{margin:0 150px 0 150px;}

div#m4-2-5box p{margin:0;}

div#m4-2-5box a{color:rgb(0,150,150);}

div#m4-2-6box{margin:3px 150px 30px 150px;}

div#m4-2-6box p{margin:0; line-height:13px;}

div#m4-2-6box a{color:rgb(0,150,150);}

div#m4-2-6-1box{color:rgb(100,100,100); font-size:11px; padding:10px 0 0 0;}

div#owner_chat{ padding-left:10px; padding-right:10px; height:368px; width:360px; overflow:scroll;}

/*画像リサイズ設定*/
.image-resize {		max-width: 180px; max-height:130px;}

/***フリースペースのスタイル***/
#free img {
	min-height:200px;
	min-width:200px;
	max-height:400px;
	max-width:400px;
}
#free .free_link {
	text-decoration:underline;
	color:blue;
}

#free .free_link:hover,#free .free_link:focus,#free .free_link:active {
	color:red;
}
.mlauto {
	margin-left:auto;
}
.mrauto {
	margin-right:auto;
}



/***コメント欄ツールチップ用の設定***/

.tooltip { display: none; bottom: -2.8em; left: 0.3em; z-index: 9999; padding: 0.3em 0.5em; color: #FFFFFF; background: #c72439; border-radius: 0.5em; }

.tooltip:after { width: 100%; content:""; display: block; position: absolute; left: 0.5em; top: -8px; border-top:8px solid transparent; border-left:8px solid #c72439; }
a:hover .tooltip { display: block; margin-top:20px; }	

/*画像リサイズ*/
.cover {
    display: inline-block;
    background-color: #ccc;
    background-position: center center;
    background-repeat: no-repeat;
    margin: 5px;
    width: 200px;
    height: 200px;
    border: 1px solid #ccc;
    background-size: cover;
}
.pageBtn {
	background-color:#CCC;
	margin-right:5px;
	padding:5px;
}
.pageBtnBox {
	margin-top:20px;
	text-align:center;
	margin-bottom:20px;
}
/*画像リサイズ*/
/*ここから表型ページ用*/
body {
/*
font-family: "Meiryo", "メイリオ", "ヒラギノ角ゴ Pro W3", sans-serif;
font-weight:600;
*/
width:auto;
height:auto;
background-image:url("../images//base/wallpaper/heart.jpeg");
background-size: contain;

}
div#wrapper{
	width:1180px;
	height:100%;
	background-color:white;
	margin-left:auto;
	margin-right:auto;
}
	#trustBtn {
		margin-left:auto;
		margin-right:auto;
		text-align:center;
	}
	.image-resize {
		max-width: 180px;
		max-height:130px;
	}
	.pall10 {
		padding:10px;
	}
	.mleft10 {
		margin-left:10px;
	}
	.mleft20 {
		margin-left:20px;
	}
	.mleft30 {
		margin-left:30px;
	}
	.mleft40 {
		margin-left:40px;
	}
	.fleft{
		float:left;
	}
	.mbottom10 {
		margin-bottom:10px;
	}
	.mbottom20 {
		margin-bottom:20px;
	}
	.mbottom30 {
		margin-bottom:30px;
	}
	.mbottom40 {
		margin-bottom:40px;
	}
	.mtop10 {
		margin-top:10px;
	}
	.mtop20 {
		margin-top:20px;
	}
	.mtop30 {
		margin-top:30px;
	}
	.mtop40 {
		margin-top:40px;
	}
	.mlauto{
		margin-left:auto;
	}
	.mrauto{
		margin-right:auto;
	}

	.invisible {
		display:none;
	}
	.tright {
		text-align:right;
	}
	.tleft {
		text-align:left;
	}
	.tcenter {
		text-align:center;
	}
	#starsbox {
		top:0px;
		width:100px;
		margin-bottom:10px;
	}
	#record_title {
		width:auto;
		margin-top:10px;
	}
/*ここまで表型ページ用*/

/*テキストオーバーフロー設定*/
.text-overflow {
    overflow: hidden;
    /*border: 1px solid #ccc;*/
    padding: 0px;
    width: 563px;
    /*background: #eee;*/
    line-height: 1.2em;
    /*white-space: nowrap;*/
}
.text-overflow {
    text-overflow: ellipsis;
    -webkit-text-overflow: ellipsis; /* Safari */
    -o-text-overflow: ellipsis; /* Opera */
}
 /*テキストオーバーフロー設定*/
#owner_space_left {
	width:200px;
}
#ochat_space {
	width:300px;
	margin-left:10px;
}
input[type="submit"],input[type="button"] {
	cursor: pointer;
	margin-left: 10px;
}
li {
	list-style:none;
}
img {
	border:none;
}

</style>

</head>

<body>
<div id="wrapper">
<div id="main-box">
		<!--▼編集確認▼-->
		<?php
			if ($edit_on == 1) {
				echo '<div style="color:black;text-align:left;width:auto;"><p style="margin-left:5px;margin-right:5px;margin-top:0px;margin-bottom:0px;">';

				if ($site_data["open"] == 0) {
					echo '<span id="siteStatus" style="color:black;text-align:right;" > - 非公開 - </span><span style="width:100px;">メニュー： </span></p>';
				} else {
					echo '<span id="siteStatus" style="color:red;text-align:right;" > - 公開中 - </span><span style="width:100px;">メニュー： </span></p>';
				}
				if ($site_data["open"] == 0) {
					echo '<div style="width:auto;"><a class="mleft10" href="http://hikaku.jp/invoice/site_open.php?site_id='.$site_id.'&open='.$site_data["open"].'"><img class="inline" src="http://hikaku.jp/images/base/btn_open.png" alt="OPEN" width="85" height="24" /></a>';
				} else {
					echo '<div style="width:auto;"><a class="mleft10" href="http://hikaku.jp/invoice/site_open.php?site_id='.$site_id.'&open='.$site_data["open"].'"><img class="inline" src="http://hikaku.jp/images/base/btn_close.png" alt="CLOSE" width="85" height="24" /></a>';
				}
				echo '<form style="float:left;" name="site_title_config" action="./make_title.php" method="post"><input type="submit" name="site_title_config_button" value="タイトルなどの編集" /></form>';
				echo '<form style="float:left;" name="to_make_table_form" action="./make_table.php" method="post"><input type="submit" name="to_make_table_button" value="表の作成" /></form>';
				echo '<form style="float:left;" name="tomypage" action="./mypage.php"><input type="submit" name="mypage_button" value="マイページへ" /></form></div>';
				echo '</div>';
			}
		 ?>
		<!--▲編集確認▲-->

	<div id="h-box">
		<div id="hl-box">
			<a href="http://hikaku.jp">マンザX</a>
			<a href="">勢いランキング</a>
			<a href="">HOTユーザー</a>
			<a href="">人気検索ワード</a>
		</div>
		<div id="hr-box">
			<a href="./index.php">ログイン</a>
			<a href="./index.php">会員登録</a>
			<a href="">使い方</a>
			<nobr>　メニュー▼
				<span class="popup">aa</span>
			</nobr>
		</div>
	</div>

	<div id="clear"></div>

	<div id="m1-box">
		
		
		<div id="m1l-box">
			
			<div id="m1l1-box">
				<h1><?php echo $site_title_data['site_title']; ?>比較</h1>
				<p class="text-overflow"><?php echo nl2br($site_title_data['introduction']); ?></p>
				
			</div>
			
			<div id="m1l2-box">
				<img class="image-resize" src="<?php echo $site_title_data['imgurl'];?>" alt="<?php echo $site_data['site_title']; ?>" />
				<p>
					<?php 
					//var_dump($site_data);
					?>
					PV:<span class="bol"><?php echo $view_sum_data; ?></span>
					コメント:<span class="bol">3,237</span>
					マイリスト:<span class="bol">572</span>
					勢い:<span class="red">827</span>
					<br/>
					「この比較表は信頼出来る」と投票した人の数：<span class="bol"><?php echo $trust_sum; ?></span>人中、<span class="red"><?php echo $trust_num; ?></span>人（<span class="red"><?php echo $trust_ratio; ?>%</span>)
				</p>
			</div>
			
			<div id="clear"></div>
			
			<div id="m1l3-1-box">
			
				<span class="bol">登録タグ</span>
			<?php
				$scrpt = "'./free_tag.php?user_id=".$user_id."&site_id=".$site_id."','タグ編集フォーム','width=500,height=400px,scrollbars=yes'";
				echo '<input type="button" value="【編集】" onclick="window.open('.$scrpt.'); return false" style="float:left;margin-left:20px;color:red;" />';
			?>
			</div>
			
			<div id="m1l3-2-box">
				<span class="tag"><?php if ($site_data['tag1'] != NULL){ echo $site_data['tag1'];} ?></span>
				<span class="tag"><?php if ($site_data['tag2'] != NULL){ echo $site_data['tag2'];} ?></span>
				<span class="tag"><?php if ($site_data['tag3'] != NULL){ echo $site_data['tag3'];} ?></span>
				<span class="tag"><?php if ($site_data['tag4'] != NULL){ echo $site_data['tag4'];} ?></span>
				<span class="tag"><?php if ($site_data['tag5'] != NULL){ echo $site_data['tag5'];} ?></span>
				<span class="tag"><?php if ($site_data['tag6'] != NULL){ echo $site_data['tag6'];} ?></span>
				<span class="tag"><?php if ($site_data['tag7'] != NULL){ echo $site_data['tag7'];} ?></span>
				<span class="tag"><?php if ($site_data['tag8'] != NULL){ echo $site_data['tag8'];} ?></span>
				<span class="tag"><?php if ($site_data['tag9'] != NULL){ echo $site_data['tag9'];} ?></span>
				<span class="tag"><?php if ($site_data['tag10'] != NULL){ echo $site_data['tag10'];} ?></span>
				<form name="refresh" action="#reason" method="post"><input type="submit" value="更新" class="mleft10" /></form>
			</div>
		</div>
		
		<div id="m1r-box">
			<div id="owner_chat">
				 <p style="width:540px;">
					<script>
					  (function() {
					    var cx = '003905097220022197422:v-vw3aksyiq';
					    var gcse = document.createElement('script');
					    gcse.type = 'text/javascript';
					    gcse.async = true;
					    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
					        '//www.google.com/cse/cse.js?cx=' + cx;
					    var s = document.getElementsByTagName('script')[0];
					    s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</p>
			<div id="owner_space" style="position:relative;">
				<div id="owner_space_left" style="position:relative;float:left;">
					<?php echo '<p><img class="image-resize" src="'.$user_arr['user_img'].'" alt="'.$user_arr['nick_name'].'" /></p>'; ?>
					<p>サイトオーナー：<?php echo $user_arr['nick_name']; ?>さん</p>
					<p><form action="../usermail/contact.php" method="post" target="_blank"><input type="hidden" name="site_id" value="<?php echo $site_id; ?>" /><input type="submit" value="オーナーへの問い合わせ"></form></p>
					<p>オーナーの評価: <?php echo $user_arr['good'] - $user_arr['bad']; ?></p>
					<!--<br style="clear:both;" />-->
					<div id="goodBtn" class="">
						<!--
						<input type="button" name="good" size="10" style="width:60px;cursor:pointer;" value="はい" onclick="good_add()"/><input type="button" name="bad"  style="width:60px;cursor:pointer;" value="いいえ" onclick="bad_add()">
						-->
					<?php echo "<span class=\"mtop10\">信頼できる：".$user_arr['good']."</span><span class=\"mleft10\">信頼できない：".$user_arr['bad']."</span>";?>
					</div>
					<form name="form3" action="./goodbad.php" method="post" >
						<input type="hidden" name="ret_num" value="2">
						<input type="hidden" name="ret_user_id" value="<?php echo $user_id; ?>">
						<input type="hidden" name="ret_site_id" value="<?php echo $site_id; ?>">
						<div id="GoodBox">
						</div>
					</form>
					<form  class="mtop10" name="fav_form" action="" method="post">
						<input type="button" name="fav_btn" value="お気に入り登録★" />
					</form>
				</div>
				<div id="ochat_space" style="position:relative;float:left;">
					<div>
						<li class="" style="border:1px solid #999;list-style:none;padding:5px;text-align:center;">オーナーチャット</li>
						<?php
						
							$ochat_num = count($ochat_data);
							for ($i = 9;$i >= 0;$i-- ) {
								echo '<li style="border-bottom:1px solid #999;list-style:none;padding:5px;">';
								/*
								echo '<p>'.$ochat_data[$i]['writer'].' '.$ochat_data[$i]['regist_date'].'</p>';
								*/
								echo '<p>';
								
								echo $ochat_data[$i]['comment_id'].' '.$ochat_data[$i]['writer'].': ';
								if ($ochat_data[$i]['link_comment_rank']) {
									echo '>>'.$ochat_data[$i]['link_comment_rank'].' ';
								}
								echo $ochat_data[$i]['article'];
								if($edit_on == 1){
									echo'<input type="button" value="返信" onclick="o_reply('.$ochat_data[$i]['comment_id'].')" />';
								}
								echo '</p></li>';
							}
							$ochat_com_id = $ochat_data[0]['comment_id'] + 1;
						?>
					</div>
			
	                <!--▽オーナーチャット投稿フォーム▽-->
	                <form id="ochat_box" name="ochat_box" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.2em;border-top:1px solid #999;padding-top:10px;" onsubmit="return checkBlank();">
						<input id="o_reply_to" type="hidden" name="o_reply_to" cols="30" value="NULL" />
						<input id="ochat_present_id" type="hidden" name="ochat_present_id" cols="30" value="<?php echo $login_user_id; ?>" />
	                	<p class="mbottom10">お名前：<input type="text" name="ochat_writer" size="10" value="<?php  if(isset($_SESSION['writer'])) { echo $_SESSION['writer'];} else if(isset($nick_name)){ echo $nick_name; } else { echo '匿名';} ?>" /> 返信先：<span id="o_reply_to2">-</span></p>
	                	<p>コメント：<textarea id="ochat_article" name="ochat_article" cols="30" rows="4"  ></textarea></p>
	                	<p class="tleft" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="投稿する"/></p>
	                	<input type="hidden" name="ochat_comment_id" cols="30" value="<?php echo $ochat_com_id; ?>" />
	                </form>
				</div>
			<div>
				<form action="./sitechat.php?chat_user_id=<?php echo $user_id; ?>&chat_site_id=<?php echo $site_id; ?>" method="post" target="_blank"><input type="hidden" name="chat_user_id" value="<?php echo $user_id; ?>"><input type="hidden" name="chat_site_id" value="<?php echo $site_id; ?>"><input type="hidden" name="site_title" value="<?php echo $site_title_data['site_title']; ?>比較"><input type="hidden" name="login_user_id" value="<?php echo $login_user_id; ?>"><input type="hidden" name="nick_name" value="<?php echo $nick_name; ?>"><input type="hidden" name="chat_bg_img" value="<?php echo $chat_bg_img; ?>"><input type="submit" value=" このページのチャットルームに行く " style="cursor:pointer;"></form>
			</div>

		</div>
		<div id="clear"></div>
	</div>
	<?php
		if ($page_num > 1) {
			echo '<div class="pageBtnBox"><a class="pageBtn" href="?site_id='.$site_id.'&page=1&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'"><<</a>';
			for($p = 1;$p <= $page_num;$p++) {
				echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$p.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'">'.$p.'</a>';
			}
			echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page_num.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'">>></a></div>';
		} 
	?>
	<?php 		
		if (asort($data_seq_arr, SORT_NUMERIC)) {
			for($k = 0;$k < $row_num;$k++) {
	?>

	<div id="m2-box">
		<div id="m2l-box">
<?php
				if ($body_data[$k]['record_photo'] != NULL) {
					echo '<a href="./detail.php?site_id='.$site_data["site_id"].'&record_id='.$body_data[$k]['record_id'].'&page='.$page.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop_detail.'">';
					echo '<img src="'.$body_data[$k]["record_photo"].'" alt="'.$body_data[$k]["record_title"].'" style="max-width:280px;max-height:178px;margin:auto;margin-bottom:5px;" /></a><br/>';
				}
				$stars = $body_data[$k]["evaluate"];
?>
			<h2>
				<a href="./detail.php?site_id=<?php echo $site_data["site_id"].'&record_id='.$body_data[$k]['record_id'].'&page='.$page.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop_detail; ?>"><?php echo $body_data[$k]["record_title"]; ?></a>
			</h2>
			<p>
				<a href=""><?php echo $body_data[$k]["record_title"]; ?><br/>
				を評価している、他の比較表を見る</a>
			</p>
			<div id="m2l1-box">
				<img class="vmiddle" style="display:inline" src="../images/base/star<?php echo $stars; ?>.png"/>
				<a href=""><?php echo "(45件のレビュー)"; ?></a>
				<div id="clear"></div>
				<p>
					<span class="gray">5つ星のうち<?php ""; ?></span>
				</p>
			</div>
		</div>

		
		<div id="m2m-box">
			<div>
			<?php
				$record_id = $body_data[$k]['record_id'];
				$review_data_record_count = $review_con -> review_select_all_record_conunt($pdo,$user_id,$site_id,$record_id);
				$review_num_goodbad = (int)$review_data_record_count['good'] + (int)$review_data_record_count['bad'];
			?>
				<span class="gray"><?php echo $review_num_goodbad; ?>人中<?php echo (int)$review_data_record_count['good']; ?>人の方が、「このレビューは良いと思う」とコメントしています。</span>
			</div>
			<?php
				if ($ranking == 1) { 
					$stars = $body_data[$k]["evaluate"];
					if ($body_data[$k]["display_rank"] == 1) {
						echo '<a href="?site_id='.$site_id.'&sort_num='.$rank.'&order='.$desc.'&data_prop='.$data_prop.'&sort_btn=1" style="display:inline;font-weight:900;float:left;padding-top:5px;"><img src="../images/base/rank1.jpg" title="rank1" /></a>';
					} else if($body_data[$k]["display_rank"] == 2) {
						echo '<a href="?site_id='.$site_id.'&sort_num='.$rank.'&order='.$desc.'&data_prop='.$data_prop.'&sort_btn=1" style="display:inline;font-weight:900;float:left;padding-top:5px;"><img src="../images/base/rank2.jpg" title="rank2" /></a>';
					} else if($body_data[$k]["display_rank"] == 3) {
						echo '<a href="?site_id='.$site_id.'&sort_num='.$rank.'&order='.$desc.'&data_prop='.$data_prop.'&sort_btn=1" style="display:inline;font-weight:900;float:left;padding-top:5px;"><img src="../images/base/rank3.jpg" title="rank3" /></a>';
					} else {
						echo '<a href="?site_id='.$site_id.'&sort_num='.$rank.'&order='.$desc.'&data_prop='.$data_prop.'&sort_btn=1" style="display:inline;font-weight:900;float:left;padding-top:5px;">'.$body_data[$k]["display_rank"].' 位： </a>';
					}
				}
			?>
			
			<div id="m2m1-box" style="float:left;">
				<img src="../images/base/cercle<?php echo $stars; ?>.jpg" title="rank" style="vertical-align:middle;" />
				<div id="clear"></div>
			</div>
			
			<div id="clear"></div>
			
			<div id="m2m2-box" class="text-overflow" style="background-image:url(../images/base/<?php echo floor($stars); ?>n.png);background-repeat:no-repeat;background-position:center;background-atachment:fixed;">
				<p><?php echo nl2br($body_data[$k]["record_explain"]); ?></p>
				<p><?php echo nl2br($body_data[$k]["record_basis"]); ?></p>
			</div>
			<form name="review_comment" action="./goodbad.php" method="post">
				<div id="m2m3-box">
						<span class="gray"><?php echo '<input type="hidden" name="review_record_id" value="'.$body_data[$k]["record_id"].'" /><input type="hidden" name="table_name" value="REVIEW_TABLE" /><input type="hidden" name="ret_num" value="2" /><input type="hidden" name="user_id" value="'.$user_id.'" /><input type="hidden" name="ret_site_id" value="'.$site_id.'" /><input type="hidden" name="login_user_id" value="'.$login_user_id.'" /><p>このレビューは参考になりましたか？<br/><label><input type="radio" name="judge" value="good" /> はい [○] </label><label><input type="radio" name="judge" value="bad" /> いいえ [×] </label><input type="radio" name="judge" value="normal" selected="selected" />どちらでもない [－]</p>' ;?></span>
					<div id="clear"></div>
				</div>
				
				<div id="m2m4-box">
					<input type="text" name="review_body" size="35" style="font-size:1.2em" placeholder="　コメント" />
					<button type="comment" onclicik="submit();">コメントする</button>
				</div>
			</form>
			<div id="m2m5-box">
				<div id="m2m5-1-box">
					<?php 
						$record_id = $body_data[$k]['record_id'];
						$review_data_record = $review_con -> review_select_limit_record($pdo,$user_id,$site_id,$record_id);
						$review_data_record_count = count($review_data_record);
					
						if ($review_data_record_count > 0) {
							/*
							if ($review_data_record_count > 3){
								$review_data_record_count = 3;
							}
							*/
							for($rev = 0;$rev < $review_data_record_count;$rev++) {
								if ($review_data_record[$rev]['good'] == 1) {
									$judge_review = "[○]";
									$review_color = blue;
								}else if ($review_data_record[$rev]['bad'] == 1 ) {
									$judge_review = "[×]";
									$review_color = red;
								}else {
									$judge_review = "[－]";
									$review_color = black;
								}
								$rev_num = ($rev + 1);
								if ($rev_num%5 == 0 || $rev_num == 1) {
									if ($review_data_record_count > 4) {
										echo '<div class="disp'.$rev_num.'">';
									} else {
										echo '<div class="disp'.$rev_num.' disp5 disp9">';
									}
								}
								if ($review_data_record[$rev]['good'] == 1) {
									echo '<p style="line-height:1em;margin-top:0px;margin-bottom:0px;"><span class="cblue">';
								} else if ($review_data_record[$rev]['bad'] == 1) {
									echo '<p style="line-height:1em;margin-top:0px;margin-bottom:0px;"><span class="cred">';
								} else {
									echo '<p style="line-height:1em;margin-top:0px;margin-bottom:0px;"><span class="cblack">';
								}
								if ($review_data_record[$rev]['review_ellipsis'] == "") {
									echo $review_data_record[$rev]['review_ellipsis'].'</span><span class="gray"> '.$judge_review.' '.date('m/d G:i', strtotime($review_data_record[$rev]['regist_date'])).'</span></p>';
								} else {
									echo $review_data_record[$rev]['review_ellipsis'].'</span><span class="gray"> '.$judge_review.' '.date('m/d G:i', strtotime($review_data_record[$rev]['regist_date'])).'</span></p>';
								}
								if ($rev_num%5 == 4 || $rev_num == $review_data_record_count) {
									echo '</div>';
								}
							}
						}
					?>
				</div>
				<div id="m2m5-2-box">
					<?php
					$review_data_record_count = $review_con -> review_select_all_record_conunt($pdo,$user_id,$site_id,$record_id);
					$review_num_all = (int)$review_data_record_count['good'] + (int)$review_data_record_count['bad'] + (int)$review_data_record_count['normal'];
					if ($review_num_all > 0) { echo '<a href="" class="gray">　すべてのコメント:('.$review_num_all.') </a>'; }
					if ($review_num_all > 0) { echo '<a class="blue" href="">　賛成:('.$review_data_record_count['good'].') </a>'; }
					if ($review_num_all > 0) { echo '<a class="red" href="">　反対:('.$review_data_record_count['bad'].')</a>'; }
					
					?>
				</div>
			</div>
		</div>
		
		<div id="m2r-box" style="padding:10 10 5 10;">
			<span class="gray">詳細データ</span><br/>
			<?php
				echo '<li><a href="?site_id='.$site_id.'&sort=evaluate&order='.$desc.'&sort_btn=1"><span class="bol black">説明 : </span></a>'.nl2br($body_data[$k]["record_explain"]).'</li>';
				foreach($data_seq_arr as $key => $posi_data) {
					//添え字の番号になっている下2文字を取得
					$rank = substr($key,-2,2);
					$data_type = $disp_data['data_property'.$rank];
					$col_name = "data_body".$rank;
					$send_name = $col_name.$k;
					
					//添え字番号が一致するdata_titleのデータを取得する。
					$data_prop = $disp_data['data_property'.$rank];
					echo '<li><a href="?site_id='.$site_id.'&sort_num='.$rank.'&order='.$desc.'&data_prop='.$data_prop.'&sort_btn=1"><span class="bol black">'.$title_data["data_title".$rank].'</span></a> : ';
						//テキストエリアじゃないバージョン
						switch($data_type) {
							case 1: 
									echo "".nl2br($body_data[$k][$col_name])."</li>\n";
									break;
							case 2:
									echo "".nl2br($body_data[$k][$col_name])."</li>\n";
									break;
							case 3:
									echo "".(string)$body_data[$k][$col_name]."</li>\n";
									break;
							case 4:
									echo "<a class=\"link_blue\" style=\"display:inline;\" href=\"".$body_data[$k][$col_name]."\" rel=\"nofollow\" target=\"_blank\"><span class=\"\">外部リンク</span></a></li>\n";
									break;
							case 5:
									echo "<a class=\"link_red\"  href=\"".$body_data[$k][$col_name]."\" rel=\"nofollow\" target=\"_blank\"><span class=\"\">ショップへのリンク</span></a></li>\n";
									break;
							case 6:
									echo "<span class=\"input_form image-box\">";
									if (isset($body_data[$k][$col_name])) {
										echo "<img class=\"image-resize\" src=\"".$body_data[$k][$col_name]."\" alt=\"".$body_data[$k]['record_title']."\" style=\"max-height:100px;max-width:100px;\" /></span></li>\n";
									} else {
										echo "</span></li>\n";
									}
									break;
							default:
									echo "".$body_data[$k][$colname]."</li>\n";
									break;
						}
					}
			?>
			
		</div>
		
	</div><br/>
	<div id="clear"></div>
	<?php
					}
			}
	?>

	<?php
		if ($page_num > 1) {
			echo '<div class="pageBtnBox"><a class="pageBtn" href="?site_id='.$site_id.'&page=1&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'"><<</a>';
			for($p = 1;$p <= $page_num;$p++) {
				echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$p.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'">'.$p.'</a>';
			}
			echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page_num.'&sort='.$sort.'&order='.$desc.'&sort_num='.$sort_num.'&data_prop='.$data_prop.'">>></a></div>';
		} 
	?>
	
	<div id="m3-box">
		<span class="gray">この比較表は信頼できますか？</span><br/>
		<button type="yn" name="good" style="width:60px;cursor:pointer;" onclick="good_add_trust()"/>はい</button>
		<button type="yn" name="bad"  style="width:60px;cursor:pointer;" onclick="bad_add_trust()">いいえ</button>
		<form name="form_trust" action="./goodbad.php" method="post" >
			<input type="hidden" name="ret_num" value="2">
			<input type="hidden" name="ret_user_id" value="<?php echo $user_id; ?>">
			<input type="hidden" name="ret_site_id" value="<?php echo $site_id; ?>">
			<div id="trustBox">
			</div>
		</form>

	</div>
	<!--▼表の根拠▼-->
	<div id="site_basis"> 
		<?php
			if (!$site_data['site_basis'] == "") {
		?>
				<hr/>
				<h2 class="tcenter mtop30" style="">表の根拠</h2>
		<?php
			}
    		if ($edit_on == 1) {
    			echo '<input id="basis_edit" type="button" style="cursor:pointer;margin-left:20px;" onclick="changeBasisBox();" value="編集" />';
    		}
		?>
    		<p id="basis"><?php echo nl2br($site_data['site_basis']); ?></p>
		<?php
			if ($edit_on == 1) {
                echo '<form id="basisBox" name="basisBox" action="" method="post" style="display:none;margin-left:20px;margin-right:20px;width:1140px;margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;">';
                echo '<p>表の根拠：</p><p><textarea name="site_basis" cols="72" rows="6">'.$site_data['site_basis'].'</textarea></p>';
                echo '<p class="tleft" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="投稿する"/></p>';
                echo '</form>';
			}
		?>
	</div>
	<!--▲表の根拠▲-->
	<hr/>

	<div id="contentsList">
	<?php
	if (!$free_data[0]["free_space_record"] == "") {
	?>
		<h2 class="tcenter" style="">フリースペース</h2>
	<?php
	}
		if ($edit_on == 1) {
			$scrpt = "'./freespace.php?user_id=".$user_id."&site_id=".$site_id."&record_id=".$free_record_id."&position=".$free_positon."&free_space_kbn=".$free_kbn."','フリースペース編集フォーム','width=500,height=400px,scrollbars=yes'";
			echo '<input type="button" value="編集" onclick="window.open('.$scrpt.'); return false" style="float:left;margin-left:0px;" />';
			echo '<form name="refresh" action="#contentsList" method="post"><input type="submit" value="更新" class="mleft10" /></form><br style="clear:both;" />';
		}
	?>
						<?php
						/*
						//ページ番号の処理
	                	if ($bpage_all > 1 ) echo '<div class="pageBtnBox"><a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage=1#contentsList"> << </a>';
		                for($i = 1;$i <= $bpage_all;$i++) {
		                	if($bpage_all > 1 ) echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage='.$i.'#contentsList">'.$i.'</a>';
		                }
		                if ($bpage_all > 1) echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage='.$bpage_all.'#contentsList"> >> </a></div>';
						*/
					?>
					<ul id="free" class="section">
	                <?php
	                	//記事の表示の処理
	                	for($i = 0;$i < $free_data_num;$i++) {
	                		$k = $i + 1;
	                		/*
	                		echo $free_data[$i]["user_id"];
	                		echo $free_data[$i]["site_id"];
	                		echo $free_data[$i]["record_id"];
	                		echo $free_data[$i]["free_space_kbn"];
	                		echo $free_data[$i]["free_space_record"];
	                		echo $free_data[$i]["free_space_rank"];
	                		echo $free_data[$i]["data_kbn"];
	                		echo $free_data[$i]["article"];
	                		*/
	                		//echo $free_data[$i]["free_space_rank"];
	                		if ($edit_on == 1 && $i != 0) {
		                		echo '<li><form name="freeUp" action="" method="post"><input type="hidden" name="free_space_record_a" value="'.$free_data[$i]["free_space_record"].'" /><input type="hidden" name="free_space_record_b" value="'.$free_data[$i-1]["free_space_record"].'" /><input type="hidden" name="free_space_rank_a" value="'.$free_data[$i]["free_space_rank"].'" /><input type="hidden" name="free_space_rank_b" value="'.$free_data[$i-1]["free_space_rank"].'" /><input type="submit" name="freeUp" value="▲" /></form>';
		                	} else {
		                		echo '<li>';
		                	}
	                		$free_data_kbn = $free_data[$i]["data_kbn"];
	                        if ($free_data_kbn == 1) {
	                        	//テキスト
	                        	echo '<div class="mleft40">';
	                        	echo '<h3 class="title invisible">'.$free_data[$i]["free_space_rank"].'</h3>';
	                        	echo '<p>'.nl2br($free_data[$i]["article"]).'</p>';
	                        	echo '</div>';echo "\n";
	                        } else if ($free_data_kbn == 2) {
	                        	//画像
	                        	/*画像にはリンクを埋めたり説明文を入れたりできるようにする。*/
	                        	echo '<div class="mlauto mrauto tcenter">';
	                        	echo '<h3 class="title invisible">'.$free_data[$i]["free_space_rank"].'</h3>';
	                        	echo '<a href="'.$free_data[$i]["image_link"].'" target="_blank"><img class="image_size" src="'.$free_data[$i]["article"].'" title="'.$free_data[$i]["title"].'" alt="'.$free_data[$i]["title"].'"/></a>';
	                        	echo '<p class="mtop10">'.$free_data[$i]["title"].'</p>';
	                        	echo '<p class="mtop10">'.nl2br($free_data[$i]["words"]).'</p>';
	                        	echo '</div>';echo "\n";
	                        } else if ($free_data_kbn == 3) {
	                        	//外部リンク
	                        	echo '<div class="mleft40">';
	                        	echo '<h3 class="title invisible">'.$free_data[$i]["free_space_rank"].'</h3>';
	                        	echo '<a class="free_link" href="'.$free_data[$i]["article"].'" target="_blank" >'.nl2br($free_data[$i]["title"]).'</a>';
	                        	echo '</div>';echo "\n";
	                        } else if ($free_data_kbn == 4) {
	                        	//動画
	                        	echo '<div class="mlauto mrauto tcenter">';
	                        	echo '<h3 class="title invisible">'.$free_data[$i]["free_space_rank"].'</h3>';
	                        	echo '<p>'.nl2br($free_data[$i]["article"]).'</p>';
	                        	echo '<p class="mtop10">'.$free_data[$i]["title"].'</p>';
	                        	echo '</div>';echo "\n";
	                        }
	                		if ($edit_on == 1 && $free_data[$i]["free_space_rank"] != $free_rank_end) {
		                		echo '<form name="freeDown" action="" method="post"><input type="hidden" name="free_space_record_a" value="'.$free_data[$i]["free_space_record"].'" /><input type="hidden" name="free_space_record_b" value="'.$free_data[$i+1]["free_space_record"].'" /><input type="hidden" name="free_space_rank_a" value="'.$free_data[$i]["free_space_rank"].'" /><input type="hidden" name="free_space_rank_b" value="'.$free_data[$i+1]["free_space_rank"].'" /><input type="submit" name="freeDown" value="▼" /></form>';
		                	} else {
		                		echo '</li>';
		                	}
		                	if($edit_on == 1){
		                		echo '<form name="freeDelete" action="" class="mtop10" method="post"><input type="hidden" name="free_space_record" value="'.$free_data[$i]["free_space_record"].'" /><input type="submit" name="freeDelete" value="削除" /></form></li>';
		                	}
	                	}
	                ?>
					</ul>
					<hr />
				</div>



	<div id="m4-box">
	
		<div id="m4-1-box">
			COMMENT
		</div>
		
		<div id="m4-2-box">
			
			<!--投稿フォーム-->
			<div id="m4-2-1box">
				　コメントする
			</div>
			<form id="comBox" name="comBox" action="" method="post" onsubmit="return checkBlank();">
			<input id="reply_to" type="hidden" name="reply_to" cols="72" rows="4" value="NULL" />
			<div id="m4-2-2box">
				<p>名前:</p>
				<input type="text" name="new_writer" cols="72" rows="4" value="<?php  if(isset($_SESSION['writer'])) { echo $_SESSION['writer'];} else if(isset($nick_name)){ echo $nick_name; } else { echo '匿名';} ?>" /> 返信先：<span id="reply_to2">-</span>
				
			</div>
			<div id="m4-2-2box">
				<p>トリップ:<input type="text" name="new_check" cols="72" rows="4" value="<?php if(isset($new_check)) { echo $new_check;} ?>" /></p><br/>
			</div>
			<div id="m4-2-3box">
				<p>コメント:</p>
				<textarea id="new_article" name="new_article" cols="72" rows="10"  ></textarea>
			</div>
			<div id="m4-2-2box">
				<p class="tleft" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="投稿する"/></p>
		    	<input type="hidden" name="new_comment_id" cols="72" rows="4" value="<?php echo $new_com_id; ?>" />
			</div>
		    </form>
		    <br/>
			<!--投稿フォーム-->
			<!--掲示板コメント欄-->
			<div id="m4-2-4box">
				この比較表へのコメント
			</div>
			<?php
               	for($i = 0;$i < $bcom_disp_num;$i++) {
               		$k = $i+1;
			?>
			<div id="m4-2-5box">
			<?php
				 echo $board_data[$i]["comment_id"]; ?>　名前：
				<span class="comname"><?php echo $board_data[$i]["writer"]; ?></span>
			<?php echo $board_data[$i]["check"]; ?>	
 				<span class="time"><?php echo $board_data[$i]["regist_date"]; ?></span>
			<?php	
 				echo '<input type="button" value="返信する" onclick="reply('.$board_data[$i]["comment_id"].')" />';
			?>	
			</div>
			<div id="m4-2-6box">
				<p>
					<span id="<?php echo $board_data[$i]["comment_id"]; ?>" style="position:relative;"><?php echo nl2br($board_data[$i]["article"]);?></span> 
				</p>
				<div id="m4-2-6-1box">
				<?php
					if ($board_data[$i]["link_comment_rank"] != NULL) {
				?>
					このコメントへの返信：
				<?php
						echo '<a class="aqua" href="#'.$board_data[$i]["link_comment_rank"].'" onmouseover="tooltip('.$board_data[$i]["link_comment_rank"].','.$board_data[$i]["comment_id"].')"> >>'.$board_data[$i]["link_comment_rank"].' <span  id="tool_'.$board_data[$i]["comment_id"].'" class="tooltip"></span></a><br/>';
					}
				?>
					<br/>
				<?php
					if($edit_on == 1){
				    	echo '<form name="boardDelete" action="" class="mtop10" method="post"><input type="hidden" name="comment_id" value="'.$board_data[$i]["comment_id"].'" /><input type="submit" name="board_delete" value="削除" /></form></li>';
				    }
				?>
				</div>
			</div>
			<?php
				}
			?>
			<!--掲示板コメント欄-->
			<!--投稿フォーム-->
			<form id="comBox2" name="comBox" action="" method="post" onsubmit="return checkBlank();">
			<input id="reply_to_low" type="hidden" name="reply_to" cols="72" rows="4" value="NULL" />
			<div id="m4-2-2box">
				<p>名前:</p>
				<input type="text" name="new_writer" cols="72" rows="4" value="<?php  if(isset($_SESSION['writer'])) { echo $_SESSION['writer'];} else if(isset($nick_name)){ echo $nick_name; } else { echo '匿名';} ?>" /> 返信先：<span id="reply_to_low2">-</span>
				
			</div>
			<div id="m4-2-2box">
				<p>トリップ:<input type="text" name="new_check" cols="72" rows="4" value="<?php if(isset($new_check)) { echo $new_check;} ?>" /></p><br/>
			</div>
			<div id="m4-2-3box">
				<p>コメント:</p>
				<textarea id="new_article" name="new_article" cols="72" rows="10"  ></textarea>
			</div>
			<div id="m4-2-2box">
				<p class="tleft" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="投稿する"/></p>
		    	<input type="hidden" name="new_comment_id" cols="72" rows="4" value="<?php echo $new_com_id; ?>" />
			</div>
		    </form>
		    <br/>
			<!--投稿フォーム-->
			
			
			<?php
						/*
						//ページ番号の処理
	                	if ($bpage_all > 1 ) echo '<div class="pageBtnBox"><a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage=1#contentsList"> << </a>';
		                for($i = 1;$i <= $bpage_all;$i++) {
		                	if($bpage_all > 1 ) echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage='.$i.'#contentsList">'.$i.'</a>';
		                }
		                if ($bpage_all > 1) echo '<a class="pageBtn" href="?site_id='.$site_id.'&page='.$page.'&bpage='.$bpage_all.'#contentsList"> >> </a></div>';
						*/
					?>
		</div>
	</div>
</div>





<!--▽フッター▽-->
<div id="footer">
<div id="h-box">

	<p style="text-align:center;">Copyright © 2014 hikaku.jp All rights reserved.</p>
</div>
</div><!--footer-->
<!--△フッター△-->
</div>
</body>
