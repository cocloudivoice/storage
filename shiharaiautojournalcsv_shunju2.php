<?php 
session_start();
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php');
include_once("/var/www/storage.cloudinvoice.co.jp/html/pdf/PdfToImg.class.php");

//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/searchkeywordscontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$pdftoimg_con = new PdfToImg();
$keyword_con = new search_keywords_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

//変数
$csv_data = "";
$data_num = $_POST["data_num"];
$invoice_code_checker = "";
$claimant_id_checker = "";
$table_name = "";
$company_id = "";
$account_table_arr[] = array();
$opr_flg = $_REQUEST["operation_flag"];

//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = '/var/www/storage.cloudinvoice.co.jp/html/invoice/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            $tax_class = split(",",$buffer);
            $tax_class_arr[$tax_class[0]] = $tax_class[1];
            
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

echo "6";
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
//$csv_data .= "ヘッダー印,請求元共通コード,請求元会社名,請求日付,請求書番号,,担当者名,支払日\n";
//$csv_data .= ",,仕訳科目,,,,税抜金額,消費税額,,,,,,,,,合計金額,,,,品名＋備考,品名\n";

//var_dump($_POST);

for ($i = 0;$i < $data_num;$i++) {
	//未使用データフラグ
	//0:登録・出力処理する 1:登録・出力処理しない 
	$unused_flag = $_POST["unused_flag".$i];
	if ( $unused_flag == 0 ) {
		//echo "<br/>\r\n";
		//echo "共通CD：";
		$claimant_id = $st_claimant_id = $_POST["claimant_id".$i];
		//echo ",会社名：";
		$company_name = $_POST["remarks".$i];//$_POST["company_name".$i];//春秋は備考欄に会社名のみを入れる。請求書の会社名も備考欄で変更する。
		//echo ",請求番号：";
		$invoice_code = $_POST["invoice_code".$i];
		//echo ",借方勘定科目：";
		$debit_account_name = $_POST["debit_account_name".$i];
		//echo ",借方勘定科目：";
		$debit_sub_account_name = $_POST["debit_sub_account_name".$i];
		//echo ",借方勘定科目：";
		$debit_section_name = $_POST["debit_section_name".$i];

		//echo ",回数：";
		$count_flag = $_POST["count_flag".$i];
		//echo ",請求日：";
		$billing_date = $_POST["billing_date".$i];
		//echo ",担当者名：";
		$staff_name = $_POST["staff_name".$i];
		//echo ",支払日：";
		$pay_date = $_POST["pay_date".$i];
		//echo ",借方金額（税込合計）：";
		$total_price = $_POST["total_price_excluding_tax".$i];
		//echo ",借方消費税：";
		$sales_tax = $_POST["sales_tax".$i] * 1;
		//echo ",貸方勘定科目：";
		$credit_account_name = $_POST["credit_account_name".$i];
		//echo ",貸方補助勘定科目：";
		$credit_sub_account_name = $_POST["credit_sub_account_name".$i];
		//echo ",貸方部門：";
		$credit_section_name = $_POST["credit_section_name".$i];
		//echo ",貸方消費税：";
		$credit_sales_tax = $_POST["credit_sales_tax".$i] * 1;
		//echo ",貸方金額：";
		$credit_amount_of_money = $_POST["credit_amount_of_money".$i];
		//echo ",電話番号：";
		$tel_num = $_POST["tel_num".$i];
		//echo ",備考：";
		$remarks = $_POST["remarks".$i];
		//echo ",品名：";
		$product_name = $_POST["product_name".$i];
		//echo "<br/>\r\n";
		$base_money = $_POST['base_amount_of_money'.$i];
		//仕訳済状態フラグ
		//0:仕訳なし 1:支払側処理済み 2:請求側処理済 3:請求側・支払い側処理済
		$journalized_flag = $_POST["journalized_flag".$i];
		//支払請求書・売上請求書の判定フラグ
		//1:支払請求書 2:売上請求書
		$des_cla_flag = $_POST["des_cla_flag".$i];
		
		$amount_of_money = $_POST['invoice_total_top'.$i];
		if ($_POST['return_page'] == 1) {
			$return_page = $_POST['return_page'];
		} else {
			$return_page = $_POST['return_page'] - 1;
		}
		$memo = $_POST['remarks3_'.$i];
		
		//明細行数カウント
		$row_count = 0;
		for($row = 1;$row <= 10;$row++) {
			if($_POST['account_table'.$i.'_'.$row.'1']){
				$row_count += 1;
			}
			$account_table_arr = array();
			for ($col = 1;$col <= 10; $col++) {
				$account_table_data = $_POST['account_table'.$i.'_'.$row.$col.''];
				if ($account_table_data != null) {
					//array_push($account_table_arr,$account_table_data);
					$account_table_arr[] = array($row.$col => $account_table_data);
				}
			}
		}

		if ($opr_flg == 8) {
			//会社名変更を4_aから入力の場合実行する。20150716ハマサキ
			//companyに会社名検索をかけ、あれば共通コードを取得し、請求書の共通コードを書き換える。
			//なければ新たに企業データを作成して共通コードを取得し、請求書の共通コードを書き換える。
			
			//変数初期化
			$exist_flag = 0;
			$user_arr = array();
			$login_check_arr = array();
			$login_arr = array();
			$company_data_arr = array();
			
			//商品名に企業名を入れる。
			$product_name = $company_name;
			
			//請求者データの存在確認▼
			//電話番号で存在確認
			if ($tel_num != "") {
				$claimant_company_id = "".$claimant_id;
				$flag = "company_data_from_tel";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$tel_num,$words);
				//var_dump($company_data_arr);
				if (isset($company_data_arr[0]['tel_num'])) {
					echo "TELあり<br/>\r\n";
					$exist_flag = 1;
				} else{echo "TELなし<br/>\r\n";}
			}
			//電話番号が無かった場合、企業名で存在確認
			if($exist_flag == 0) {
				$flag = "company_data_from_name";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_name,$words);
				var_dump($company_data_arr);
				if (isset($company_data_arr[0]['company_name'])) {
					echo "名前あり<br/>\r\n";
					echo $company_data_arr[0]['company_name'];
					$exist_flag = 1;
				} else {echo "名前なし<br/>\r\n";}
			}
			//企業データ存在確認▲
			if ($exist_flag == 0) {
				echo "請求側の企業が存在しない時の処理<br/>\r\n";
				//仮メールアドレス生成
				$mail_address = md5(random(12)).rand(4)."@user.storage.cloudinvoice.co.jp";
				$user_con = new user_control();
				//Emailアドレスの存在を確認する。		
				$login_check_arr = $user_con -> user_select_email($pdo,$mail_address);

				if ($login_check_arr != NULL) {
					$login_arr = $user_con -> user_select($pdo,$mail_address);

					//$passwordmaker = random(12);
					//$passwordmaker = uniqid();
					$passwordmaker = "1234abcd";
					$password = $login_arr['password'] = md5($passwordmaker);
					//$login_arr['nick_name'] = $user_name;
					//$user_con -> user_update($pdo,$login_arr);
					$update_data = " `password` = '".$password."' ";
					$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

				}

				$user_arr = $user_con -> user_select($pdo,$mail_address);

				//ユーザー登録
				$user_arr['nick_name']   = $company_name;
				$user_arr['mail_address'] = $mail_address;
				$user_arr['tel_num'] = $tel_num;
				$user_arr['non_regular_flag'] = 2;//$non_regular_flag;
				$industry_type_s = "小売";//仮置き ****後で変更
				//共通コードの生成と取得
				if (!$user_arr['user_id']){
					$user_num = $user_con -> user_get_new_num($pdo);
					$user_arr['user_id'] = $user_num;
				}
				$result = $user_con -> user_insert($pdo,$user_arr);

				//企業データ登録▼
				$claimant_id = "".$user_arr['user_id'];
				$sql = 'SELECT * FROM `COMPANY_TABLE` WHERE `company_id` = '.$user_arr['user_id'].' AND `company_name` = "'.$user_arr['nick_name'].'" ;';
				$company_arr = $company_con -> company_sql($pdo,$user_arr['user_id'],$sql);
				$com_exists_flag = $company_arr['chk']['select_count'];
				if ($com_exists_flag == 0) {
				echo "挿入<br/>";
				echo	$sql = '
					INSERT INTO `COMPANY_TABLE` ( 
						`company_id`, `company_name`, `email`, `tel_num`, `non_regular_flag`, `industry_type_s`
					) VALUES ( 
						'.$user_arr['user_id'].', "'.$user_arr['nick_name'].'","'.$user_arr['mail_address'].'","'.$user_arr['tel_num'].'","'.$user_arr['non_regular_flag'].'","'.$industry_type_s.'"
					)';
				}
				$company_arr = $company_con -> company_sql($pdo,$claimant_id,$sql);
				//企業データ登録▲
				
				//▼自動仕訳用企業DB登録▼
				//企業品目テーブルと企業用レコードテーブルを登録
				$table_name = "".sprintf("%012d", $claimant_company_id);
				try {
					$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
					$company_num++;
				} catch (PDOException $e) {
				    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
				    $_SESSION['error_msg'] = $e -> getMessage();
				}
				//▲自動仕訳用企業DB登録▲

			} else {
				//企業アカウントが存在する時
				$claimant_id = $company_data_arr[0]['company_id'];
				//▼企業データ更新▼
				//echo "企業データ更新<br/>";
				$sql = '
				UPDATE `COMPANY_TABLE` SET `industry_type_s` = "'.$industry_type_s.'"
				WHERE `company_id` = "'.$claimant_id.'"
				;';
				$results_update = $company_con -> company_sql($pdo,$claimant_id,$sql);
				//var_dump($results_update);
				//▲企業データ更新▲
				//$com_update_num++;
			}
			
			//$flag = "image_upload";
			//$update_com_words = " `company_name` = '".$company_name."' ";
			//$company_con -> company_sql_flag($pdo,$flag,$claimant_id,$update_com_words);
		}

		$table_name = "RECORD".sprintf("%012d",$company_id);
		$search_key = $claimant_id." ".$product_name;
		
		try {

			//search_keyで自社のRECORDテーブルに登録があるかどうか確認する
			//配列を取得
			$auto_arr = $auto_journalize_con -> auto_journalize_select($pdo_aj,$table_name,$search_key);
			//echo "<br/>配列数<br/>";
			//echo count($auto_arr);
			//var_dump($auto_arr);
			
			//$_POST["account_table".$i."_".$k."1"];の形で渡してやる必要がある。
			
			$auto_arr['id'];
			$auto_arr['search_key'] = $search_key;
			$auto_arr['company_id'] = $claimant_id;
			$auto_arr['company_name'] = $company_name;
			$auto_arr['product_name'] = $product_name;
			$auto_arr['flag1'] = "".$row_count;//ここに行数を渡したい
			$auto_arr['flag2'] = $_POST['flag2'.$i];
			$auto_arr['flag3'] = $_POST['flag3'.$i];
			$auto_arr['base_amount_of_money'] = $base_money;
			$auto_arr['text_data1'] = $_POST['text_data1'.$i];
			$auto_arr['text_data2'] = $_POST['text_data2'.$i];
			$auto_arr['text_data3'] = $_POST['text_data3'.$i];
			$auto_arr['remarks'] = $_POST['remarks'.$i];//備考。摘要は覚えない。
			$auto_arr['debit_section_code'] = $_POST['debit_section_code'.$i];
			$auto_arr['debit_section_name'] = $_POST['debit_section_name'.$i];
			$auto_arr['debit_account_code'] = $_POST['debit_account_code'.$i];
			$auto_arr['debit_account_name'] = $debit_account_name;
			$auto_arr['debit_sub_account_code'] = $_POST['debit_sub_account_code'.$i];
			$auto_arr['debit_sub_account_name'] = $_POST['debit_sub_account_name'.$i];
			$auto_arr['debit'] = $total_price;
			$auto_arr['debit_tax'] = $sales_tax;
			$auto_arr['debit_ratio'] = round($_POST["total_price_excluding_tax".$i] / $base_money,15);
			$auto_arr['debit_flag1'] = $_POST['debit_tax_class'.$i];
			$auto_arr['debit_flag2'] = $_POST['debit_flag2'.$i];
			$auto_arr['debit_flag3'] = $_POST['debit_flag3'.$i];
			$auto_arr['credit_section_code'] = $_POST['credit_section_code'.$i];
			$auto_arr['credit_section_name'] = $_POST['credit_section_name'.$i];
			$auto_arr['credit_account_code'] = $_POST['credit_account_code'.$i];
			$auto_arr['credit_account_name'] = $credit_account_name;
			$auto_arr['credit_sub_account_code'] = $_POST['credit_sub_account_code'.$i];
			$auto_arr['credit_sub_account_name'] = $_POST['credit_sub_account_name'.$i];
			$auto_arr['credit'] = $credit_amount_of_money;
			$auto_arr['credit_tax'] = $credit_sales_tax;
			$auto_arr['credit_ratio'] = round($_POST["credit_amount_of_money".$i] / $base_money,15);// = $_POST['credit_ratio'.$i];
			$auto_arr['credit_flag1'] = $_POST['credit_tax_class'.$i];
			$auto_arr['credit_flag2'] = $_POST['credit_flag2'.$i];
			$auto_arr['credit_flag3'] = $_POST['credit_flag3'.$i];
			//仕訳帳用
			$download_password = $auto_arr['download_password'] = $_POST['account_table_dlp'.$i];
			$auto_arr['paid_date'] = $_POST['pay_date'.$i];
			$auto_arr['insert_date'] = date('Y-m-d H:i:s');
			
			//行数を取得
			for ($j = 1;$j < 10; $j++) {
				if ($_POST["account_table".$i."_".$j."1"] != NULL) {
					$num_box = $j;
				}
			}
			
			for ($k = 1;$k <= $num_box;$k++) {
				$auto_arr['debit_section_code'.$k] = $_POST["account_table".$i."_".$k."14"];
				$auto_arr['debit_section_name'.$k] = $_POST["account_table".$i."_".$k."9"];
				$auto_arr['debit_account_code'.$k] = $_POST["account_table".$i."_".$k."12"];
				$auto_arr['debit_account_name'.$k] = $_POST["account_table".$i."_".$k."1"];
				$auto_arr['debit_sub_account_code'.$k] = $_POST["account_table".$i."_".$k."13"];
				$auto_arr['debit_sub_account_name'.$k] = $_POST["account_table".$i."_".$k."8"];
				$auto_arr['debit_ratio'.$k] = round($_POST["account_table".$i."_".$k."2"] / $base_money,15);
				$auto_arr['debit'.$k] = $_POST["account_table".$i."_".$k."2"];
				$auto_arr['debit_tax'.$k] = $_POST["account_table".$i."_".$k."3"];
				$auto_arr['debit_flag'.$k.'_1'] = $_POST["account_table".$i."_".$k."3_1"];
				$auto_arr['debit_flag'.$k.'_2'] = $_POST['debit_flag1_2'.$i];
				$auto_arr['debit_flag'.$k.'_3'] = $_POST['debit_flag1_3'.$i];

				$auto_arr['credit_section_code'.$k] = $_POST["account_table".$i."_".$k."17"];
				$auto_arr['credit_section_name'.$k] = $_POST["account_table".$i."_".$k."11"];
				$auto_arr['credit_account_code'.$k] = $_POST["account_table".$i."_".$k."15"];
				$auto_arr['credit_account_name'.$k] = $_POST["account_table".$i."_".$k."4"];
				$auto_arr['credit_sub_account_code'.$k] = $_POST["account_table".$i."_".$k."16"];
				$auto_arr['credit_sub_account_name'.$k] = $_POST["account_table".$i."_".$k."10"];
				$auto_arr['credit_ratio'.$k] = round($_POST["account_table".$i."_".$k."5"] / $base_money,15);
				$auto_arr['credit'.$k] = $_POST["account_table".$i."_".$k."5"];
				$auto_arr['credit_tax'.$k] = $_POST["account_table".$i."_".$k."6"];
				$auto_arr['remarks'.$k] = $_POST["account_table".$i."_".$k."7"];
				$auto_arr['credit_flag'.$k.'_1'] = $_POST["account_table".$i."_".$k."6_1"];
				$auto_arr['credit_flag'.$k.'_2'] = $_POST['credit_flag'.$k.'_2'.$i];
				$auto_arr['credit_flag'.$k.'_3'] = $_POST['credit_flag'.$k.'_3'.$i];
			}
			
			//▼春秋用 請求書データを変更する。▼
			$tax_ratio = 108;//税率で変更
			$new_total_price_ex_tax = round($amount_of_money * 100 / $tax_ratio);
			$new_sales_tax = $amount_of_money - $new_total_price_ex_tax;
			if ($opr_flg == 7) {
				//会社名変更//会社名を変更すると他社にも影響するので、会社名の変更はしない。上田さんと話して決定。20150518ハマサキ
				//会社名変更をinsert_7から入力の場合実行する。20150601ハマサキ
				$flag = "image_upload";
				$update_com_words = " `company_name` = '".$company_name."' ";
				$company_con -> company_sql_flag($pdo,$flag,$claimant_id,$update_com_words);
			}
			if ($opr_flg == 8) {
				//商品名に企業名を入れる
				
				//請求書データ変更
				$flag = "up_invoice_download_password";
				$update_words = " `claimant_id` = '".$claimant_id."',`billing_date` = '".$pay_date."',`pay_date` = '".$pay_date."', `paid_date` = '".date('Y-m-d',strtotime($pay_date))."', `unit_price` = '".$new_total_price_ex_tax."', `quantity` = 1, `total_price_excluding_tax` = '".$new_total_price_ex_tax."' , `sales_tax` = '".$new_sales_tax."', `total_price` = '".$amount_of_money."', `remarks3` = '".$memo."' ,`product_name` = '".$company_name."' WHERE `claimant_id` = '".$st_claimant_id."' AND `invoice_code` = '".$invoice_code."' ";
				$get_total_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$update_words);
				
				
				
				
				//画像の移動
				//証憑ファイルの保存		
				
				//変更後の請求者の共通コード
				$claimant_id = sprintf("%012d", $claimant_id);
				$top_n = substr($claimant_id,0,4);
				$mid_n = substr($claimant_id,4,4);
				
				//変更前の請求者の共通コード
				$st_claimant_id = sprintf("%012d", $st_claimant_id);
				$st_top_n = substr($st_claimant_id,0,4);
				$st_mid_n = substr($st_claimant_id,4,4);

				//$start_dir = "/var/www/storage.cloudinvoice.co.jp/html/files/0099/9999/009999999999/pdf/2015/01";
				$start_dir = "/var/www/storage.cloudinvoice.co.jp/html/files/".$st_top_n."/".$st_mid_n."/".sprintf("%012d", $st_claimant_id)."/pdf/2015/01";
				$new_dir = "/var/www/storage.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $claimant_id)."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				$new_img = $new_dir."/".$download_password;
				
				//画像の移動先ディレクトリが無かったら作成する。
				try {

					if (mkdir( $new_dir, 0775, true)) {
						chmod( $new_dir, 0775, true );
						chgrp($new_dir, "dev", true );
						//echo "ディレクトリ作成成功！！";
					} else {
						//echo "ディレクトリ作成失敗！！";
					}

				} catch (Exception $e) { 
					echo $e -> getMessage;
				}
				
				//画像を変更前の企業のディレクトリから変更後の企業のディレクトリに移動する。
				$pdftoimg_con -> moveImage2($start_dir,$new_dir,$download_password);
			
				if ($tel_num != "") {
					//企業名検索テーブルにキーワードと企業名を挿入する。
					$flag = "insert_keyword";
					$keyword = $tel_num;
					$keyword_con -> search_keywords_sql_flag($pdo_aj,$flag,$keyword,$company_name);
				}
			
			} else {
				//請求書データ変更
				$flag = "up_invoice_download_password";
				$update_words = " `billing_date` = '".$pay_date."',`pay_date` = '".$pay_date."', `paid_date` = '".date('Y-m-d',strtotime($pay_date))."', `unit_price` = '".$new_total_price_ex_tax."', `quantity` = 1, `total_price_excluding_tax` = '".$new_total_price_ex_tax."' , `sales_tax` = '".$new_sales_tax."', `total_price` = '".$amount_of_money."', `remarks3` = '".$memo."'  WHERE `claimant_id` = '".$claimant_id."' AND `invoice_code` = '".$invoice_code."' ";
				$get_total_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$update_words);
			}
			
			
			//▲春秋用 請求書データを変更する。▲
			
			//変更するデータを配列に入れて更新もしくは挿入
			//$search_result_arr = $auto_journalize_con -> auto_journalize_insert2($pdo_aj,$table_name,$auto_arr);

			//挿入と更新別のパターン
			if ($auto_arr['id'] == NULL) {
				
				//登録が無かった場合、挿入する
				$auto_journalize_con -> auto_journalize_insert($pdo_aj,$table_name,$auto_arr);
			
			} else {
				//登録があった場合、更新する
				$auto_journalize_con -> auto_journalize_update($pdo_aj,$table_name,$auto_arr);
			
			}
			//仕訳帳への登録
			//text_data3に仕訳コードを入れる
			$auto_arr['text_data3'] = $_POST['jounalized_history_code'.$i];
			$journalized_history_con -> journalized_history_insert($pdo_aj,$company_id,$auto_arr);
			
			//請求元仕訳テーブルの使用回数に1を足す
			try {
				$flag = "check_table_product_and_account";
				$check_words = " `account_name` = '".$debit_account_name."' AND `product_name` = '".$product_name."' ";
				$result_get_company_code_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$claimant_id,$check_words);

				//請求元仕訳テーブルに該当の仕訳と品名のセットが無かった場合
				if (count($result_get_company_code_arr) >= 1) {
					//請求元仕訳テーブルの使用回数に1を足す
					$sql = "
						UPDATE `".sprintf("%012d",$claimant_id)."` 
						SET `number_of_times` = `number_of_times` + 1 
						WHERE `product_name` = '".$product_name."' AND `account_name` = '".$debit_account_name."'
					;";
				//請求元仕訳テーブルに該当の仕訳と品名のセットがあった場合
				} else {
				//請求元仕訳テーブルに新たにデータを挿入する
					$sql = "
						INSERT INTO `".sprintf("%012d",$claimant_id)."`
						(`product_name`, `account_name`, `number_of_times`) 
						VALUES 
						('".$product_name."','".$debit_account_name."',1)
						;";
				}
				$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);

			} catch (Exception $e) {
				//例外処理
			}
			
			//請求元の仕訳テーブルを利用した場合、自社のレコードに登録して請求元仕訳テーブルの使用回数を増やす
			if ($count_flag == 2) {
				//echo $table_name;

				//自社のレコードに登録
				$sql = "
					INSERT INTO `".$table_name."`
					(`search_key`, `company_id`, `company_name`, `product_name`, `debit_account_name`) 
					VALUES 
					('".$search_key."',".$claimant_id.",'".$company_name."','".$product_name."','".$debit_account_name."')
					;";
				$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);

				//請求元仕訳テーブルの使用回数に1を足す
				$sql ="
					UPDATE `".sprintf("%012d",$claimant_id)."` 
					SET `number_of_times` = `number_of_times` + 1 
					WHERE `product_name` = '".$product_name."' AND `account_name` = '".$debit_account_name."'
					";
				$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);

			//MAINテーブルを利用した場合、自社のレコードに登録してMAINテーブルの使用回数を増やし、請求元の仕訳テーブルに項目を追加する。
			} else if ($count_flag == 3) {

				//自社のレコードに登録
				$sql = "
					INSERT INTO `".$table_name."`
					(`search_key`, `company_id`, `company_name`, `product_name`, `debit_account_name`) 
					VALUES 
					('".$search_key."',".$claimant_id.",'".$company_name."','".$product_name."','".$debit_account_name."')
					;";
				$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);

				//MAINテーブルの使用回数に1を足す
				$sql = "
					UPDATE `MAIN` 
					SET `number_of_times` = `number_of_times` + 1 
					WHERE `product_name` = '".$product_name."' AND `account_name` = '".$debit_account_name."'
					";
				$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);
			}
			//▼MAINテーブル学習処理▼
			if ($claimant_id != $company_id) {
				//請求者が自分でない時（自分が支払者の場合）、MAINテーブルに登録されていなければ、
				//MAINテーブルに登録し、次回からRECORDや企業側テーブルに無くてもMAINテーブルから提案する
				$sql = "SELECT * FROM `MAIN` WHERE `product_name` = '".$product_name."' AND `account_name` = '".$debit_account_name."' ";
				$main_table_arr = $auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);
				if ( count($main_table_arr) == 0) {
					$sql = "
						INSERT INTO `MAIN`(`product_name`, `account_name`, `number_of_times`)
						VALUES 
						('".$product_name."','".$debit_account_name."','1')
						;";
					$auto_journalize_con -> auto_journalize_sql($pdo_aj,$sql);
				}
			}
			//▲MAINテーブル学習処理▲
		}catch (Exception $e) {
			echo $e -> getMessage();
		}
		//incioce_code_checkerとこの行の請求書番号が一致しなければ
		//新しい請求書と判断しヘッダーをつける。
		//弥生形式の方はヘッダーなし
		if ($invoice_code_checker != $invoice_code || $claimant_id_checker != $claimant_id) {

			//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
			$debit_sales_tax_status = $tax_class_arr[$_POST['debit_tax_class'.$i]];
			$credit_sales_tax_status = $tax_class_arr[$_POST['credit_tax_class'.$i]];
			$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
			$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
			
			$flag1 = $row_count;//行数をDBに格納して渡す予定でテスト用に変数に1を入れていたが、無しでも取得できたのでif文にしなくてもいい。

			if ($num_box == 0) {
				$csv_data .= "2000,";
			} else {
				$csv_data .= "2110,";
			}
			$csv_data .= ",,";
			$csv_data .= str_replace(" ","",substr($billing_date,0,4)."/".substr($billing_date,4,2)."/".substr($billing_date,6,2)).",";
			$csv_data .= $debit_account_name.",".$debit_sub_account_name.",".$debit_section_name.",".$debit_sales_tax_status.",";
			$csv_data .= $total_price.",";
			$csv_data .= "0,";
			$csv_data .= $credit_account_name.",".$credit_sub_account_name.",".$credit_section_name.",".$credit_sales_tax_status.",";
			$csv_data .= $credit_amount_of_money.",0,";
			$csv_data .= $remarks.",,,";
			if ($num_box == 0) {
				$csv_data .= "0,";
			} else {
				$csv_data .= "3,";
			}
			$csv_data .= ",,0,0,no";
			
			/*
			//明細の最初の行だけに請求書の合計金額を表示する。
			$invoice_code_checker = $invoice_code;
			$claimant_id_checker = $claimant_id;
			$flag = "invoice_get_invoice_total";
			$get_total_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$invoice_code);
			$csv_data .= $get_total_arr[0]["sum(total_price)"];
			*/
			
			//download_passwordが一致するINVOICE_DATA_TABLEの請求書データを仕訳済み(journalized = 1)にする。
			$flag = "up_invoice_download_password";
			if ($journalized_flag == 1) {
				$words = "`journalized` = 3 ";
			} else if ($journalized_flag == 2) {
				$words = "`journalized` = 3 ";
			} else if ($journalized_flag == 0) {
				//journalizedに直接$des_cla_flagを入れても良い
				if ($des_cla_flag == 1) {
					$words = "`journalized` = 1 ";
				} else if ($des_cla_flag == 2) {
					$words = "`journalized` = 2 ";
				}
			}
			$words .= "WHERE `claimant_id` = ".$claimant_id." AND `download_password` = '".$_POST['account_table_dlp'.$i]."';";
			
			$invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
		}
		$csv_data .= "\n";

		if ($flag1 >= 1) {
			for ($k = 1;$k <= $num_box;$k++) {
				//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
				$debit_sales_tax_status = $tax_class_arr[$_POST["account_table".$i."_".$k."3_1"]];
				$credit_sales_tax_status = $tax_class_arr[$_POST["account_table".$i."_".$k."6_1"]];
				$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
				$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
				if ($k == $num_box) {
					$csv_data .= "2101,";
				} else {
					$csv_data .= "2100,";
				}
				$csv_data .= ",,";
				$csv_data .= str_replace(" ","",substr($billing_date,0,4)."/".substr($billing_date,4,2)."/".substr($billing_date,6,2)).",";
				$csv_data .= $_POST["account_table".$i."_".$k."1"].",".$_POST["account_table".$i."_".$k."8"].",".$_POST["account_table".$i."_".$k."9"].",".$debit_sales_tax_status.",";
				$csv_data .= $_POST["account_table".$i."_".$k."2"].",";
				$csv_data .= "0,";//$_POST["account_table".$i."_".$k."3"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."4"].",".$_POST["account_table".$i."_".$k."10"].",".$_POST["account_table".$i."_".$k."11"].",".$credit_sales_tax_status.",";
				$csv_data .= $_POST["account_table".$i."_".$k."5"].",";
				$csv_data .= "0,";//$_POST["account_table".$i."_".$k."6"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."7"].",,,3,,,0,0,no";
				$csv_data .= "\n";
			}
		}
		
		/*****ここから旧CI形式
		//incioce_code_checkerとこの行の請求書番号が一致しなければ
		//新しい請求書と判断しヘッダーをつける。
		if ($invoice_code_checker != $invoice_code || $claimant_id_checker != $claimant_id) {
			
			$csv_data .= "*,";
			$csv_data .= $claimant_id.",";
			$csv_data .= $company_name.",";
			$csv_data .= $billing_date.",";
			$csv_data .= $invoice_code.",";
			$csv_data .= ",";
			$csv_data .= $staff_name.",";
			$csv_data .= $pay_date."\n";
			
		}
		if ($sales_tax == 0) { $debit_sales_tax_status = "対象外"; } else if ($sales_tax * 1 > 0) { $debit_sales_tax_status = "課対仕入込8%";}
		if ($credit_sales_tax == 0) { $credit_sales_tax_status = "対象外";} else if ($credit_sales_tax * 1 > 0) { $credit_sales_tax_status = "課対仕入込8%";}
	
		$csv_data .= ",,".$debit_account_name.",".$debit_sub_account_name.",".$debit_section_name.",".$debit_sales_tax_status.",";
		$csv_data .= $total_price.",";
		$csv_data .= $sales_tax.",".$credit_account_name.",".$credit_sub_account_name.",".$credit_section_name.",".$credit_sales_tax_status.",".$credit_amount_of_money.",".$credit_sales_tax.",".$remarks.",,";
		if ($invoice_code_checker != $invoice_code || $claimant_id_checker != $claimant_id) {
			//明細の最初の行だけに請求書の合計金額を表示する。
			$invoice_code_checker = $invoice_code;
			$claimant_id_checker = $claimant_id;
			$flag = "invoice_get_invoice_total";
			$get_total_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$invoice_code);
			$csv_data .= $get_total_arr[0]["sum(total_price)"];

			//download_passwordが一致するINVOICE_DATA_TABLEの請求書データを仕訳済み(journalized = 1)にする。
			$flag = "up_invoice_download_password";
			$words = "`journalized` = 1 WHERE `claimant_id` = ".$claimant_id." AND `download_password` = '".$_POST['account_table_dlp'.$i]."';";
			$invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);

		}
		$csv_data .= ",,";
		$csv_data .= $invoice_data_arr[$i]["withholding_tax"].",";
		if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
			$csv_data .= $invoice_data_arr[$i]['sum(total_price)'].","; 
		} else { 
			$csv_data .= $invoice_data_arr[$i]["total_price"].",";
		}
		//$csv_data .= $remarks.",";
		//$csv_data .= $product_name."\n";
		$csv_data .= "\n";
		
		$flag1=1;
		$num_box = 0;
		if ($flag1 >= 1) {
			for ($j=1;$j < 10; $j++){
				if ($_POST["account_table".$i."_".$j."1"] != NULL){
					$num_box = $j;
				}
			}
			
				for ($op_num = 0;$op_num < 128;$op_num++) {
					if ($tax_class_arr[$op_num] != "") {
						if($op_num == $debit_tax_num){$debit_select_flag = "selected"; } else {$debit_select_flag = "";}
						echo '<option value="'.$op_num.'" '.$debit_select_flag.' >'.$tax_class_arr[$op_num].'</option>';
					}
				}

			
			for ($k = 1;$k <= $num_box;$k++) {
				if ($_POST["account_table".$i."_".$k."3"] * 1  == 0) { $debit_sales_tax_status = "対象外"; } else if ($_POST["account_table".$i."_".$k."3"] * 1 > 0) { $debit_sales_tax_status = "課対仕入込8%";}
				if ($_POST["account_table".$i."_".$k."6"] * 1  == 0) { $credit_sales_tax_status = "対象外";} else if ($_POST["account_table".$i."_".$k."6"] * 1 > 0) { $credit_sales_tax_status = "課税売上込8%";}
				$csv_data .= ",,";
				$csv_data .= $_POST["account_table".$i."_".$k."1"].",".$_POST["account_table".$i."_".$k."8"].",".$_POST["account_table".$i."_".$k."9"].",".$debit_sales_tax_status.",";
				$csv_data .= $_POST["account_table".$i."_".$k."2"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."3"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."4"].",".$_POST["account_table".$i."_".$k."10"].",".$_POST["account_table".$i."_".$k."11"].",".$credit_sales_tax_status.",";
				$csv_data .= $_POST["account_table".$i."_".$k."5"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."6"].",";
				$csv_data .= $_POST["account_table".$i."_".$k."7"].",,,,,,";
				//$csv_data .= $_POST["account_table".$i."_".$k."7"];
				//$csv_data .= $product_name."\n";
				$csv_data .= "\n";
			}
		}
		ここまで旧CI形式*****/
	}
}



//出力ファイル名の作成
$csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';

//文字化けを防ぐ
$csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );
/*
//MIMEタイプの設定
header("Content-Type: application/octet-stream");
//名前を付けて保存のダイアログボックスのファイル名の初期値
header("Content-Disposition: attachment; filename={$csv_file}");

// データの出力
echo($csv_data);
*/
//header("Location:pastjournal?mode=download");
$return_page = $_REQUEST["return_page"];

if ($opr_flg == 8) {
//	header("Location:shiharaiautojournal_insert9?page=".$return_page);
	//$_SESSION{'message'} = "1件の登録が完了しました。";
} else if ($return_page != "") {
	header("Location:".$return_page);
	//$_SESSION{'message'} = "1件の登録が完了しました。";
} else {
	header("Location:shiharaiautojournal_insert4?page=".$return_page);
	//$_SESSION{'message'} = "1件の登録が完了しました。";
}
exit();


function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}



?>
<?php require("header.php");?>

<title>自動仕訳CSVエクスポート - Cloud Invoice</title>


<article>

	<section id="m-1-box">
		<h2>
			自動仕訳CSVエクスポート
		</h2>

		<?php
/*自動仕訳でのエクスポートなしに変更。仕訳帳でエクスポートする。20150306ハマサキ
			//var_dump($_POST);
			$data_num = $_POST["data_num"];
			//echo "<br/>\r\n";
			for ($i = 0;$i < $data_num;$i++) {
				echo "共通CD：";
				echo $_POST["claimant_id".$i];
				echo ",会社名：";
				echo $_POST["company_name".$i];
				echo ",請求番号：";
				echo $_POST["invoice_code".$i];
				echo ",勘定科目：";
				echo $_POST["debit_account_name".$i];
				echo ",回数：";
				echo $_POST["count_flag".$i];
				echo ",請求日：";
				echo $_POST["billing_date".$i];
				echo ",担当者名：";
				echo $_POST["staff_name".$i];
				echo ",支払日：";
				echo $_POST["pay_date".$i];
				echo ",借方勘定科目：";
				echo $_POST["debit_account_name".$i];
				echo ",借方合計金額：";
				echo $_POST["total_price_excluding_tax".$i];
				echo ",借方消費税：";
				echo $_POST["sales_tax".$i];
				echo ",貸方勘定科目：";
				echo $_POST["credit_account_name".$i];
				echo ",貸方消費税：";
				echo $_POST["credit_sales_tax".$i];
				echo ",貸方金額：";
				echo $_POST["credit_amount_of_money".$i];
				echo ",税抜合計：";
				echo $_POST["total_price".$i];
				echo ",消費税：";
				echo $_POST["sales_tax".$i];
				echo $remarks = $_POST["remarks".$i];
				echo ",品名：";
				echo $product_name = $_POST["product_name".$i];
				echo "<br/>\r\n";
				for($low = 1;$low <= 10;$low++) {
					$account_table_arr = array();
					for ($col = 1;$col <= 10; $col++) {
						$account_table_data = $_POST['account_table'.$i.'_'.$low.$col.''];
						if ($account_table_data != null) {
							//array_push($account_table_arr,$account_table_data);
							$account_table_arr[] = array($low.$col => $account_table_data);
							
						}
					}
					for ($h = 0;$h <= 10;$h++) {
						for ($num = 0;$num < 4;$num++) {
							$res = $account_table_arr[$num][$low.$h];
							if ($res != null) {
								//var_dump($account_table_arr);
								echo $res;
								if ($num < 3) {
									echo ",";
								} else {
									echo "<br/>\r\n";
								}
							}
						}
					}
				}
				echo "<br/>\r\n";
				echo "<br/>\r\n";
			}
*/
		?>


	</section>

</article>
<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(10)"></div>

<?php require("footer.php");?>