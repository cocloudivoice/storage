<?php require("header.php");?>
<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$db_con = new db_control();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//SELECT文で対象データのありなしを判定する。
$sql = "
SELECT * FROM `INVOICE_DATA_TABLE` 
WHERE `claimant_id` = ".$_REQUEST['company_id']." AND `csv_id` = '".$_REQUEST['csv_id']."' 
AND `status` = 0 AND `status` <> 99 
";

$result1 = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);

//対象データが無ければ消去失敗コメントを返して元のページに戻る。
if ($result1['chk']['select_count'] == 0) {
	$_SESSION['comment'] .= "CSVファイルの消去に失敗しました。";
	echo "<script>window.location.href='./seikyucsv'</script>";
	//header('Location: //co.cloudinvoice.co.jp/invoice/seikyucsv');
	exit;
} else {
	
	//対象データがある時はデータを消去する。
	$sql = "
	DELETE FROM `INVOICE_DATA_TABLE`  
	WHERE `claimant_id` = ".$_REQUEST['company_id']." AND `csv_id` = '".$_REQUEST['csv_id']."' 
	AND `status` = 0 AND `status` <> 99 
	";
	
	$result2 = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
	
	$top4 = substr($company_id,0,4);
	$mid4 = substr($company_id,4,4);
	$file_path = '../files/'.$top4."/".$mid4."/".$company_id.'/csvfiles/'.$_REQUEST['file_name'];
	unlink($file_path); 
	
	//対象データがある時はCSVインポート履歴を消去する。
	$sql = "
	DELETE FROM `CSV_HISTORY_TABLE` 
	WHERE `company_id` = ".$_REQUEST['company_id']." AND `csv_id` = '".$_REQUEST['csv_id']."' 
	";
	$result3 = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);

	$_SESSION['comment'] = "CSVファイルを消去しました。";

	//処理が終了したらseikyucsvに戻る。
	echo "<script>window.location.href='./seikyucsv'</script>";
	exit();
}

?>
		
<html>
<head>
</head>
<body>
<p><a href="./seikyucsv">戻る</a></p>
</body>
</html>