<?php require("header.php");?>


<title>未確認の請求 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//ユーザーテーブルのコントローラーを呼び出す
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$maindb_con = new db_control();
//各テーブルからデータを取得する
$flag="invoice_total_data_receive";
$words;//条件をここに入れる。
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
//rsort($invoice_data_receive_arr);

if ($invoice_data_receive_num > 100) {
	$invoice_data_receive_num = 100;
}

//var_dump($invoice_data_receive_arr);


?>
<script type="text/javascript">
	
	function cnt_reload() {
		
		//リロードする。
	    location.href="./untreatedinvoice";
	}

	function reload() {
		//alert("reload");
		//関数cnt_reload()を1000ミリ秒間隔で呼び出す
		setTimeout("cnt_reload()",2000);
	}

</script>

<article>

	<section id="m-1-box">
		<h2>
			未確認の請求 （最新100件まで表示）
		</h2>
		
		<table id="main">
			<tr>
				<th id="kaisha">会社名</th>
				<th id="koushin">更新日</th>
				<th id="kingaku">支払額</th>
				<th id="tekiyou">摘要</th>
				<th id="shiharai">支払期限</th>
				<th id="hakkou">請求書</th>
				<th id="joutai">状態</th>
			</tr>
		<?php 
			for ($i = 0; $i < $invoice_data_receive_num; $i++) {
		?>
			<tr>
			<?php 
				$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
				if ($claimant_id != "") {
					$flag = "company_data";
					$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
				}
			?>
				<td><?php echo $company_arr[0]["company_name"];?></th>
				<td><?php echo $invoice_data_receive_arr[$i]["update_date"];?></td>
				<td><?php echo number_format($invoice_data_receive_arr[$i]["sum(total_price)"]);?></td>
				<td><?php echo $invoice_data_receive_arr[$i]["product_name"];?></td>
				<td><?php $str = $invoice_data_receive_arr[$i]["pay_date"];echo substr($str,0,4)."/".substr($str,4,2)."/".substr($str,6,2); ?></td>
				<td>
				<?php
						
					$flag3 = "download_invoice_data";
					$words3 = $invoice_data_receive_arr[$i]['download_password'];//条件をここに入れる。
					$invoice_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag3,$company_id,$words3);
					//var_dump($invoice_num_arr);
					$invoice_num = count($invoice_num_arr);

				?>

				
				
					<!--<?php echo "<a href='./invoice2pdf?clm=".$claimant_id."&c1=".$company_id."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=1' target='_blank'>請求書</a>";?>-->
					<?php echo "<a href=\"./invoice2pdf?dlp=".$invoice_data_receive_arr[$i]['download_password']."&det=".$invoice_num."&downloaded=6\" target=\"_blank\" onclick=\"reload();\">請求書</a>";?>
				</td>
				<td><?php
						$status = $invoice_data_receive_arr[$i]["status"];
					 	if ($status < 6) {
					 		echo "未確認";
					 	} else {
					 		echo "確認済み";
					 	}
					?>
				</td>
			</tr>
		<?php 
			}
		?>
		</table>
		
		
	</section>






</article>




<?php require("footer.php");?>
