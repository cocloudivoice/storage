﻿<?php session_start();?>
<?php include_once(dirname(__FILE__).'/../../invoice/invoicepdf_head.php');?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">



html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
td{vertical-align:top;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}
.left{float:left;}
input[type=text]{font-size:11px; width:200px; color:rgb(50,50,50); background-color:white; border-width:0; cursor:default; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=text].number{width:200px;}
input[type=text].total{width:148px; text-align:right;}
textarea{font-size:13px; height:30px; color:rgb(50,50,50); border-width:0;  background-color:white;cursor:default; resize:none; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
#total_price_value{margin:0 0 0 10px;}


article {width:770px; height:1030px;}
article section{width:600px; padding:40px 0 0 60px;}


article h1 {text-align:center; font-size:27px; font-weight:normal;}
article h3 {font-size:17px;border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 0; padding:0 0 3px 0;}
article table#top{font-size:13px; letter-spacing:0.1em; margin:40px 0 0 0;}
article table#top td#img{width:60px; text-align:center;vertical-align:bottom;}
article table#top td#spacer_td{width:100px; text-align:center;}

article table td#mainleft{width:300px;height:130px; text-align:left; font-weight:normal;}
article table td#mainleft h2{font-size:16px; margin:0 0 10px 0;}
article table td#mainleft ul{list-style-type:none; margin:0 0 30px 0;}
article table td#mainleft p{margin:0 0 10px 0;}

article table td#mainright{width:300px;height:130px; margin:40px 0 0 0;text-align:left; color:rgb(100,100,100); background-image:url('<?php echo $company_arr[0]["stamp_image"];?>'); background-repeat: no-repeat; background-position: 175px 100px; background-size:80px 80px; font-weight:normal;}
article table td#mainright h2{font-size:13px; margin:20px 0 10px 0; font-weight:normal;}
article table td#mainright ul{list-style-type:none; margin:0 0 30px 0;}
article table td#mainright p{margin:10px 0 10px 0;}
article table td#mainright img#logo{max-width:250px; max-height:40px;}



article div#main2{margin:20px 0 0 0; padding:0 0 20px 0; min-height:100px;}
article div#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px; margin: 0 0 20px 0;}
article div#main2 th#item{width:300px; text-align:left;}
article div#main2 input[type=text].item{width:298px;}
article div#main2 th#cost{width:100px; text-align:right;}
article div#main2 input[type=text].cost{width:95px; text-align:right;}
article div#main2 input[type=text].js-characters-change{width:95px; text-align:right;}
article div#main2 th#amount{width:100px; text-align:right;}
article div#main2 input[type=text].amount{width:95px; text-align:right;}
article div#main2 th#price{width:100px; text-align:right;}
article div#main2 input[type=text].price{width:95px; text-align:right;}
article div#main2 td{height:25px; text-align:right;}
article div#main2 td input[type=text].item{text-align:left; padding:0 0 0 5px;}



article table#main3{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 350px;}
article table#main3 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px; vertical-align:middle;}
article table#main3 td#item{width:100px;}
article table#main3 td#amount{width:150px;text-align:right;}



article table#main4{margin:50px 0 0 140px; font-size:12px;}
article table#main4 td{}
article table#main4 td#left{width:180px; height:35px;}
article table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
article table#main4 td#notes{padding:10px 0 0 0;}



article table#main6{}
article table#main6 td#left{width:300px; max-width:300px;}


</style>
<script type="text/javascript">
	function BgCalc() {
		var value_box = 0,st = 0,sales_tax = 0,withholding_tax = 0,total = 0,sales_box = 0,withholding_box = 0,sales_tax_one = 0;
		for (var i = 1 ; i <= 10 ; i++ ) {
			try {
				sales_box = document.fm_right["seikyu_1"+i+"6"].value;
				withholding_box = document.fm_right["seikyu_1"+i+"7"].value;
				value_box = document.fm_right["seikyu_1"+i+"4"].value;

				//小計の入れ物stに小計の金額を足す
				st += parseInt(value_box,10);

				//alert("消フラグ" + sales_box);
				//alert("源フラグ" + withholding_box);alert("合計" + st);
				//消費税フラグが1なら明細の小計に8%をかけて消費税にプラスする。
				if (sales_box == 1) {
					sales_tax_one = Math.round(parseInt(value_box,10) * 8 / 100);
					sales_tax += Math.round(parseInt(value_box,10) * 8 / 100);
					//alert("消費税" + sales_tax);
				}
				//源泉税フラグが1なら（明細の小計 + 消費税）が100万円以下なら10.21％をかけて、
				//100万円を超える場合は102100円 + （（明細の小計 + 消費税）- 100万円）* 20.42%をかけて源泉税にプラスする。
				if (withholding_box == 1) {
					//alert(value_box);//alert(sales_tax_one);
					if (value_box <= 1000000) {
						withholding_tax += Math.round((parseInt(value_box,10) + sales_tax_one) * (1021 / 10000));
						//alert("源泉" + withholding_tax);
					} else {
						withholding_tax += 102100 + Math.round((parseInt(value_box,10) + sales_tax_one - 1000000) * (2042 / 10000));
						//alert("源泉2" + withholding_tax);
					}
				}
			} catch (e) {
			}
		}
		document.fm_right["subtotal"].value = addComma(String(st));//alert("st"+st);
		document.fm_right["sales_tax"].value = addComma(String(sales_tax));//alert("sales_tax"+sales_tax);
		
		if (withholding_tax > 0) {//alert("test");
			document.getElementById("withholding_tr").style.display="";
			
		} else {
			document.getElementById("withholding_tr").style.display="none";
		}
		document.fm_right["withholding_tax"].value = "-" + addComma(String(withholding_tax));//alert("wthd_tax"+withholding_tax);
		total= document.fm_right["total"].value = addComma(String(st + sales_tax - withholding_tax));//alert("total"+total);
		document.getElementById("total_price_value").innerHTML = addComma(total);

	}

	function addCommaExe(){
		for (var i = 1 ; i <= 10 ; i++ ) {
			try {
				document.fm_right["seikyu_1"+i+"2"].value = addComma(document.fm_right["seikyu_1"+i+"2"].value);
				document.fm_right["seikyu_1"+i+"3"].value = addComma(document.fm_right["seikyu_1"+i+"3"].value);
				document.fm_right["seikyu_1"+i+"4"].value = addComma(document.fm_right["seikyu_1"+i+"4"].value);
			} catch (e) {
			}
		}
	}

	function addComma(str){
		try {
			n = Number(str.replace(/[^0-9.]/g,"")); //数値以外は削除
			//n=n.toFixed(2); //小数点以下２桁にする
			return String(n).replace(/(\d)(?=(\d\d\d)+$)/g,'$1,'); //３桁づつカンマ編集
		} catch (e) {
		}
	}

</script>



</head>



<body>


<form name="fm_right">



<article>
<section>
	<table>
		<tr>
			<td style="vertical-align:middle;">
				<h1 style="margin:0 30px 0 0;">INVOICE</h1>
			</td>
			<td style="font-size:14px; padding:5px 60px 5px 60px; border-width:1px; border-style:solid; border-color:rgb(150,150,150);">
				請求書
			</td>
		</tr>
	</table>
	
	<p  style="border-width:1px 0 0 0; border-style:solid; border-color:rgb(150,150,150); font-size:10px; padding:10px 0 0 0; margin:20px 0 0 0;">
	</p>
	
	<table  style="border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 377px; width:200px; height:30px;">
		<tr>
			<td style="vertical-align:bottom;">No.</td>
			<td style="vertical-align:bottom;"><script>document.write('<input type="text" class="number" name="seikyu_1" disabled="disabled" style="text-align:right;"/>');</script></td>
		</tr>
	</table>
	
	<table id="top">
		<tr>
			<td style="color:rgb(200,200,200); font-size:14px; width:50px;">
				FOR
			</td>
			<td id="mainleft" >
				<p>
					〒<script>document.write('<input type="text" name="seikyu_5" disabled="disabled" />');</script>
					<script>
						document.write('<input type="text" name="seikyu_6" disabled="disabled" />');
					</script>
					<br>
					<script>
						document.write('<input type="text" name="seikyu_7" disabled="disabled" />');
					</script>
				</p>
				<p>
					<script>
						document.write('<input type="text" name="seikyu_4" disabled="disabled" />');
					</script>
					<br>
					<script>
						document.write('<input type="text" name="seikyu_8" disabled="disabled" />');
					</script>
					<br>
					<script>
						document.write('<input type="text" name="seikyu_9" disabled="disabled" />');
					</script>
				</p>
				<p>
					<script>
						document.write('<input type="text" name="seikyu_10" readonly="readonly" style="font-size:13px;"/>');
					</script>
				</p>
			</td>
			
			<td>
				<p style="text-align:center; border-width:1px; border-style:solid; border-color:rgb(180,180,180); height:65px; color:rgb(100,100,100); width:130px; padding:10px 0 0 0; line-height:25px;">
					お支払い期限<br>
					<script>
						document.write('<input type="text" id="price" name="seikyu_3" disabled="disabled" style="text-align:center; width:100px; font-size:13px;"/>');
					</script>
					<br>
				</p>
			</td>
			
			<td>
				<p style="text-align:center; border-width:2px; border-style:solid; border-color:rgb(50,50,50); height:63px; color:rgb(100,100,100); width:200px; padding:10px 0 0 0;">
					ご請求金額<br>
					<span  id="total_price_value" style="font-size:20px; color:rgb(50,50,50); line-height:30px; text-align:right; "></span>円
					
				</p>
			</td>
	</table>


	<div id="main2">
		<table id="frTBL">
			<tr>
				<th id="item">品目</th>
				<th id="cost">単価</th>
				<th id="amount">数量</th>
				<th id="price">価格</th>
			</tr>
			<tr>
				<td id="item">
					<script>
						document.write('<input type="text" class="item" id="item1" name="seikyu_111" disabled="disabled" />');
					</script>
				</td>
				<td id="cost">
					<script>
						document.write('<input type="text" class="js-characters-change" id="cost1" name="seikyu_112" disabled="disabled" />');
					</script>
				</td>
				<td id="amount">
					<script>
						document.write('<input type="text" class="js-characters-change" id="amount1" name="seikyu_113" disabled="disabled" />'  );
					</script>
				</td>
				<td id="price">
					<script>
						document.write('<input type="text" class="price" id="price1" name="seikyu_114" disabled="disabled" />');
						document.write('<input type="hidden" class="detail" id="detail1" name="seikyu_115" disabled="disabled" /><input type="hidden" class="" id="detail1" name="seikyu_116" disabled="disabled" /><input type="hidden" class="" id="detail1" name="seikyu_117" disabled="disabled" />');
					</script>
				</td>
			</tr>
		</table>
	</div>
	<p style="border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);"></p>
	<table id="main3">
		<tr>
			<td id="item">小計</td>
			<td id="amount"><input class="total" id="subtotal" name="subtotal" type="text" value="" disabled="disabled" /></td>
		</tr>
		<tr>
			<td id="item">消費税</td>
			<td id="amount"><input class="total" id="sales_tax" name="sales_tax" type="text" value="" disabled="disabled" /></td>
		</tr>
		<tr id="withholding_tr" style="display:none;">
			<td id="item">源泉税</td>
			<td id="amount"><input class="total" id="withholding_tax" name="withholding_tax" type="text" value="" disabled="disabled" /></td>
		</tr>
		<tr>
			<td id="item">合計</td>
			<td id="amount"><input class="total" id="total" name="total" type="text" value="" disabled="disabled" /></td>
		</tr>
	</table>
	<table id="main6" style="margin:50px 0 0 20px; height:180px; width:560px; font-size:11px;">
		<tr>
			<td id="left">
				振込先<br>
				<?php echo $company_arr[0]["account_info"];?>
				<br>
				<script>
					document.write('<textarea type="text" name="seikyu_11" disabled="disabled" style="font-size:11px; height:70px; overflow:hidden;"/></textarea>');
				</script>
				<br>
			</td>
			<td style="font-size:13px; color:rgb(100,100,100); background-image:url('<?php if ($company_arr[0]['stamp_image'] != "") {echo $company_arr[0]['stamp_image'];} else {echo '../../images/blanksign.png';}?>'); background-repeat: no-repeat; background-position: 175px 100px; background-size:80px 80px;">
						<img id="logo" src='<?php if ($company_arr[0]["logotype"] != "") { echo $company_arr[0]["logotype"];} else { echo "../../images/blanklogo.png";}?>' title='ロゴ' alt='ロゴ'style='max-width:250px; max-height:40px;'/>
					<h2 style="font-size:14px;">
						<?php echo $company_arr[0]["company_name"];?>
					</h2>
					<p>
						〒<?php
						//var_dump($company_arr);
						echo $company_arr[0]["zip_code"];
						?><br>
						<?php echo $company_arr[0]["address1"];?><br/>
						<?php echo $company_arr[0]["address2"];?><br/>
						<?php
						if(! ($company_arr[0]["tel_num"] == FALSE)){
							echo "TEL：", $company_arr[0]["tel_num"];
						}
						?>
						<br/>
						<?php
						if(! ($company_arr[0]["fax_num"] == FALSE)){
							echo "FAX：", $company_arr[0]["fax_num"];
						}
						?>
					</p>

			
			</td>
		</tr>
	</table>
	<p style="font-weight:bold; text-align:center; margin:20px 0 0 0; padding:10px 0 0 0; border-width:1px 0 0 0; border-style:solid; border-color:rgb(150,150,150);">
		WE APPRECIATE YOUR BUSINESS!
	</p>
</section>


</article>


</form>



</body>
<script type="text/javascript" src="../../js/makeinvoice-template-footer.js"></script> 