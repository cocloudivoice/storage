<?php session_start();?>
<?php include_once(dirname(__FILE__).'/../../invoice/meisaipdf_head.php');?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>支払明細書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">



html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
div#clear{clear:both;}
.btop {border-top:3px;}
.fbold {font-weight:bold;}
.em13{font-size:1.3em}
.em15{font-size:1.5em}
.minus {color:red;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}
.left{float:left;}
input[type=text]{font-size:13px; width:270px; color:rgb(50,50,50); background-color:white; border-width:0; cursor:default; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=text].number{width:200px;}
input[type=text].total{width:148px; text-align:right;}
textarea{font-size:13px; height:30px; color:rgb(50,50,50); border-width:0;  background-color:white;cursor:default; resize:none; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
#total_price_value{margin:0 0 0 10px;}



article {width:770px; height:1030px;}
article section{width:600px; padding:40px 0 0 70px;}


article h1 {margin:20px 0 30px 0; text-align:center; font-size:20px;}
article table#top td#img{width:60px; text-align:center;vertical-align:bottom;}
article table#top td#spacer_td{width:100px; text-align:center;}

article table div#mainleft p{margin:0 0 10px 0;}

article table div#mainright h2{font-size:13px; margin:20px 0 10px 0; font-weight:normal;}
article table div#mainright ul{list-style-type:none; margin:0 0 30px 0;}
article table div#mainright p{margin:10px 0 10px 0;}
article table div#mainright img#logo{max-width:250px; max-height:40px;}



article div#main2{margin:40px 0 0 60px;}
article div#main2 th{border-width:1px 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); font-size:14px; height:30px;}
article div#main2 td{border-width:1px 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); height:30px;}
article div#main2 th#item{width:250px;}
article div#main2 input[type=text].item{width:243px;}
article div#main2 th#cost{width:100px;}
article div#main2 input[type=text].cost{width:95px; text-align:right;}
article div#main2 th#amount{width:100px;}
article div#main2 input[type=text].amount{width:95px; text-align:right;}
article div#main2 th#price{width:100px;}
article div#main2 input[type=text].price{width:95px; text-align:right;}
article div#main2 td input[type=text].item{text-align:left; padding:0 0 0 5px;}



article table#main3{border-width:0 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 299px;}
article table#main3 td{height:30px; border-width:0 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); font-size:14px;}
article table#main3 td#item{width:200px; padding:0 0 0 5px;}
article table#main3 td#amount{width:100px;text-align:right; padding:0 3px 0 0;}
article table#main3 input[type=text].total{width:93px;}



article table#main4{margin:50px 0 0 140px; font-size:12px;}
article table#main4 td{}
article table#main4 td#left{width:180px; height:35px;}
article table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
article table#main4 td#notes{padding:10px 0 0 0;}



</style>

<script type="text/javascript">
	function BgCalc() {
		var value_box = 0,st = 0,sales_tax = 0,withholding_tax = 0,total = 0,sales_box = 0,withholding_box = 0,sales_tax_one = 0;
		for (var i = 1 ; i <= 10 ; i++ ) {
			try {
				//document.fm_right["seikyu_1"+i+"1"].value;
				//document.fm_right["seikyu_1"+i+"2"].value;
				//document.fm_right["seikyu_1"+i+"3"].value;
				sales_box = document.fm_right["seikyu_1"+i+"6"].value;
				withholding_box = document.fm_right["seikyu_1"+i+"7"].value;
				value_box = document.fm_right["seikyu_1"+i+"4"].value;

				//小計の入れ物stに小計の金額を足す
				st += parseInt(value_box,10);

				//alert("消フラグ" + sales_box);
				//alert("源フラグ" + withholding_box);alert("合計" + st);
				//消費税フラグが1なら明細の小計に8%をかけて消費税にプラスする。
				if (sales_box == 1) {
					sales_tax_one = Math.round(parseInt(value_box,10) * 8 / 100);
					sales_tax += Math.round(parseInt(value_box,10) * 8 / 100);
					//alert("消費税" + sales_tax);
				}
				//源泉税フラグが1なら（明細の小計 + 消費税）が100万円以下なら10.21％をかけて、
				//100万円を超える場合は102100円 + （（明細の小計 + 消費税）- 100万円）* 20.42%をかけて源泉税にプラスする。
				if (withholding_box == 1) {
					//alert(value_box);//alert(sales_tax_one);
					if (value_box <= 1000000) {
						withholding_tax += Math.round((parseInt(value_box,10) + sales_tax_one) * (1021 / 10000));
						//alert("源泉" + withholding_tax);
					} else {
						withholding_tax += 102100 + Math.round((parseInt(value_box,10) + sales_tax_one - 1000000) * (2042 / 10000));
						//alert("源泉2" + withholding_tax);
					}
				}
				
				//document.fm_right["seikyu_1"+i+"5"].value;
			} catch (e) {
			}
		}
		document.fm_right["subtotal"].value = addComma(String(st));//alert("st"+st);
		document.fm_right["sales_tax"].value = addComma(String(sales_tax));//alert("sales_tax"+sales_tax);
		
		if (withholding_tax > 0) {//alert("test");
			document.getElementById("withholding_tr").style.display="";
			
		} else {
			document.getElementById("withholding_tr").style.display="none";
		}
		document.fm_right["withholding_tax"].value = "-" + addComma(String(withholding_tax));//alert("wthd_tax"+withholding_tax);
		total= document.fm_right["total"].value = addComma(String(st + sales_tax - withholding_tax));//alert("total"+total);
		document.getElementById("total_price_value").innerHTML = addComma(total);
	}


	function addComma(str){
		try {
			n = Number(str.replace(/[^0-9.]/g,"")); //数値以外は削除
			//n=n.toFixed(2); //小数点以下２桁にする
			return String(n).replace(/(\d)(?=(\d\d\d)+$)/g,'$1,'); //３桁づつカンマ編集
		} catch (e) {
		}
	}

</script>



</head>



<body>


<form name="fm_right">



<article>
<section>
	<h1>御支払明細書 </h1>
	<table id="top" style="font-weight:bold; font-size:13px; letter-spacing:0.1em; margin:10px 0 0 50px;">
		<tr>
			<td style="width:300px;height:200px; text-align:left; font-weight:normal;">
				<p><?php //var_dump($destination_company_arr);?>
					明細書番号：<?php echo $invoice_data_receive_arr[0]['invoice_code'];?><br>
					御支払日　　：<?php echo str_replace(" ","",substr($invoice_data_receive_arr[0]['billing_date'],0,4)."/".substr($invoice_data_receive_arr[0]['billing_date'],4,2)."/".substr($invoice_data_receive_arr[0]['billing_date'],6,2));?><br>
					　<br>
				</p>
				<p>
					〒<?php	echo $company_arr[0]["zip_code"];?>
					<br>
					<?php echo $company_arr[0]["address1"];?><br>
					<?php echo $company_arr[0]["address2"];?><br>
				</p>
				<p>
					　<br>
					<?php 
						$dest_company_name = $company_arr[0]["company_name"];
						if ($company_arr[0]["section"] != "") {
							$dest_section = $company_arr[0]["section"];
						} else if ($company_arr[0]["department"] != "") {
							$dest_section = $company_arr[0]["department"];
						}
						if ($company_arr[0]["clerk"] != "") {
							$dest_clerk = $company_arr[0]["clerk"];
						} else if ($company_arr[0]["addressee"]) {
							$dest_clerk = $company_arr[0]["addressee"];
						}
						if ($dest_company_name != "") {
							echo $dest_company_name;
						}
						if ($dest_clerk == "" && $dest_section　== "" && $dest_company_name != "") {
							echo "御中";
						} else if ($dest_company_name != "") {
							echo "<br/>";
						}
					?>
					<?php
						if ($dest_section != "") {
							echo $dest_section;
						}
						if ($dest_clerk == "" && $dest_section != "") {
							echo "御中";
						} else if ($dest_section != "") {
						 echo "<br/>";
						}
					?>
					<?php
						if ($dest_clerk != "") {
							echo $dest_clerk;
						}
						if ($dest_clerk != "") {
							echo "様";
						}
					?>
				</p>
			</td>
			
			<td id="mainright" style="width:300px;height:200px; margin:0 0 0 0;text-align:left; color:rgb(100,100,100); background-image:url('<?php if ($destination_company_arr[0]['stamp_image'] != "") {echo $destination_company_arr[0]['stamp_image'];} else {echo '../../images/blanksign.png';}?>'); background-repeat: no-repeat; background-position: right bottom;background-color:rgba(255,255,255,60); font-weight:normal; width:240px;">
						<img id="logo" src='<?php if ($destination_company_arr[0]["logotype"] != "") { echo $destination_company_arr[0]["logotype"];} else { echo "../../images/blanklogo.png";}?>' title='ロゴ' alt='ロゴ' style='max-width:250px;max-height:40px;'>
						<br>
						<br>
					<h2 style="font-size:13px; margin:20px 0 10px 0; font-weight:normal;">
						<?php echo $destination_company_arr[0]["company_name"];?>
					</h2>
						<br>
					<p>
						〒<?php echo $destination_company_arr[0]["zip_code"];?>
						<br>
						<?php echo $destination_company_arr[0]["address1"];?><br/>
						<?php echo $destination_company_arr[0]["address2"];?><br/>
						<?php
						if(! ($destination_company_arr[0]["tel_num"] == FALSE)){
							echo "TEL：", $destination_company_arr[0]["tel_num"];
						}
						?>
						<br/>
						<?php
						if(! ($destination_company_arr[0]["fax_num"] == FALSE)){
							echo "FAX：", $destination_company_arr[0]["fax_num"];
						}
						?>
					</p>
<!--
						<?php
						if ($stamp_image != "") {
						?>
							<img id="stamp" src='<?php echo $company_arr[0]["stamp_image"];?>' title='印影' alt='印影'/>
						<?php
						}
						?>
-->


			</td>
	</table>
	<?php if ($invoice_data_receive_arr[0]['invoice_name'] != "") {echo '<p style="margin-left:50px;">件名：<span>'.$invoice_data_receive_arr[0]['invoice_name'].'</span></p>';}?>
	<?php
	if ($data_cnt <= 7) {
		$data_cnt = 7;
	}
	for ($i = 0; $i < $data_cnt; $i++) {
		$total_price_top += $invoice_data_receive_arr[$i]['total_price'];
	}
	?>
	<h3 style="font-size:17px;border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:30px 0 0 50px; padding:0 0 3px 0;">御支払金額 <?php echo number_format($total_price_top);?>円</h3>

	<div id="main2">
		<table id="frTBL">
			<tr>
				<th id="item">品目</th>
				<th id="cost">単価</th>
				<th id="amount">数量</th>
				<th id="price">価格</th>
			</tr>
			<?php
			for ($i = 0; $i < $data_cnt; $i++) {
			?>
			<tr>
				<td id="item" style="text-align:left; padding:0 0 0 10px;"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
				<td id="cost" style="text-align:right; padding:0 10px 0 0;"><?php if ($invoice_data_receive_arr[$i]['unit_price'] != 0) { echo (double)$invoice_data_receive_arr[$i]['unit_price'];} else { echo " ";}?></td>
				<td id="amount" style="text-align:right; padding:0 10px 0 0;"><?php if ($invoice_data_receive_arr[$i]['quantity'] != 0) { echo (double)$invoice_data_receive_arr[$i]['quantity'];} else { echo " ";}?></td>
				<td id="price" style="text-align:right; padding:0 10px 0 0;" <?php if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] < 0) { echo "class='minus'";}?>><?php if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['total_price_excluding_tax']);} else { echo " ";};?></td>
			</tr>
			<?php
				if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] < 0) {
					$sum_minus = $sum_minus + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				}
				$sum_withholding_tax = $sum_withholding_tax + $invoice_data_receive_arr[$i]['withholding_tax'];
				if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] >= 0) {
					$sum_plus = $sum_plus + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				}
				$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				$sales_tax += $invoice_data_receive_arr[$i]['sales_tax'];
				//$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
				/*
				$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
				*/
				$total_price += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
		</table>
	</div>
	<table id="main3">
	<?php
		if ($sum_minus < 0) {
	?>
		<tr>
			<td id="item">小計</td>
			<td id="amount"><?php echo number_format($sum_plus);?></td>
		</tr>
		<tr>
			<td id="item">相殺</td>
			<td id="amount" class="minus"><?php echo number_format($sum_minus);?></td>
		</tr>
	<?php
		}
	?>
		<tr>
	<?php if ($sum_minus < 0) {?>
			<td id="item">差引合計</td>
	<?php } else { ?>
			<td id="item">小計</td>
	<?php } ?>
			<td id="amount"><?php echo number_format($sum_price);?></td>
		</tr>
		<tr>
			<td id="item">消費税</td>
			<td id="amount"><?php echo number_format($sales_tax);?></td>
		</tr>
		<?php
		if ($sum_withholding_tax > 0) {
		?>
		<tr>
			<td id="item">源泉税</td>
			<td id="amount"><?php echo "－".number_format($sum_withholding_tax);?></td>
		</tr>
		<?php
		}
		?>
		<tr>
			<td id="item" class="">合計</td>
			<td id="amount"><?php echo number_format($total_price);?></td>
		</tr>
	</table>


	<table id="main4">
		<tr>
			<td id="left">御支払日</td>
			<td><?php if ($invoice_data_receive_arr[0]['pay_date'] != 0) {echo str_replace(" ","",substr($invoice_data_receive_arr[0]['pay_date'],0,4)."/".substr($invoice_data_receive_arr[0]['pay_date'],4,2)."/".substr($invoice_data_receive_arr[0]['pay_date'],6,2));}?></td>
		</tr>
		<tr>
			<td id="notes" rowspan="2">備考</td>
			<td><?php echo $company_arr[0]["remarks1"];?></td>
		</tr>
		<tr>
			<td><?php echo $invoice_data_receive_arr[0]['remarks1'];?></td>
		</tr>

	</table>

</section>


</article>


</form>



</body>
<script type="text/javascript" src="../../js/makeinvoice-template-footer.js"></script> 