<?php session_start();?>
<?php include_once(dirname(__FILE__).'/../../invoice/invoicepdf_head.php');?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">





html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}
.left{float:left;}
input[type=text]{font-size:13px; width:270px; color:rgb(50,50,50); background-color:white; border-width:0; cursor:default; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=text].number{width:200px;}
input[type=text].total{width:148px; text-align:right;}
textarea{font-size:13px; height:30px; color:rgb(50,50,50); border-width:0;  background-color:white;cursor:default; resize:none; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
#total_price_value{margin:0 0 0 10px;}


article {width:790px; height:1125px; background-image:-webkit-radial-gradient(1500px 562px, circle farthest-side, white 96%,rgb(200,255,255) 98%, rgb(100,255,255) 100%);}
article section{width:600px; padding:40px 0 0 90px;}


article h1 {margin:40px 0 60px 0; text-align:center; font-size:20px;}
article h3 {font-size:17px;border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:100px 0 0 50px; padding:0 0 3px 0;}
article table#top{font-weight:bold; font-size:13px; letter-spacing:0.1em; margin:10px 0 0 50px;}
article table#top td#img{width:60px; text-align:center;vertical-align:bottom;}
article table#top td#spacer_td{width:100px; text-align:center;}

article table td#mainleft{width:300px; margin:40px 0 0 0;text-align:left; font-weight:normal;}
article table td#mainleft h2{font-size:16px; margin:0 0 10px 0;}
article table td#mainleft ul{list-style-type:none; margin:0 0 30px 0;}
article table td#mainleft p{margin:10px 0 10px 0;}

article table div#mainright{width:300px; margin:40px 0 0 0;text-align:left; color:rgb(100,100,100); font-weight:normal;}
article table div#mainright h2{font-size:13px; margin:20px 0 10px 0; font-weight:normal;}
article table div#mainright ul{list-style-type:none; margin:0 0 30px 0;}
article table div#mainright p{margin:10px 0 10px 0;}
article table div#mainright img#logo{max-width:250px; max-height:40px;}




article div#main2{margin:40px 0 0 50px; padding:0 0 20px 0; min-height:100px;}
article div#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px; margin: 0 0 20px 0;}
article div#main2 th#item{width:250px; text-align:left;}
article div#main2 input[type=text].item{width:248px;}
article div#main2 th#cost{width:100px; text-align:right;}
article div#main2 input[type=text].cost{width:95px; text-align:right;}
article div#main2 th#amount{width:100px; text-align:right;}
article div#main2 input[type=text].amount{width:95px; text-align:right;}
article div#main2 th#price{width:100px; text-align:right;}
article div#main2 input[type=text].price{width:95px; text-align:right;}
article div#main2 td{height:25px; text-align:right;}
article div#main2 td input[type=text].item{text-align:left; padding:0 0 0 5px;}




article table#main3{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 350px;}
article table#main3 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px;}
article table#main3 td#item{width:100px;}
article table#main3 td#amount{width:150px;text-align:right;}



article table#main4{margin:50px 0 0 140px; font-size:12px;}
article table#main4 td{}
article table#main4 td#left{width:180px; height:35px;}
article table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
article table#main4 td#notes{padding:10px 0 0 0;}



</style>




</head>



<body>

<article>

<section>
	<img id="logo" src='<?php if ($company_arr[0]["logotype"] != "") { echo $company_arr[0]["logotype"];} else { echo "../../images/blanklogo.png";}?>' title='ロゴ' alt='ロゴ' style='max-width:250px;max-height:40px; margin:30px 0 30px 55px;'>
	<table id="top">
		<tr>
			<td id="mainleft">
				<h1 style="font-size:16px">請求書</h1>
				<p>
					請求書番号：<?php echo $invoice_data_receive_arr[0]['invoice_code'];?><br>
					請求日　　：<?php echo str_replace(" ","",substr($invoice_data_receive_arr[0]['billing_date'],0,4)."/".substr($invoice_data_receive_arr[0]['billing_date'],4,2)."/".substr($invoice_data_receive_arr[0]['billing_date'],6,2));?><br>
					　<br>
				</p>
				<p>
					〒<?php echo $destination_company_arr[0]["zip_code"];?><br>
					<?php echo $destination_company_arr[0]["address1"];?><br>
					<?php echo $destination_company_arr[0]["address2"];?><br>
				</p>
				<p>
					　<br>
					<?php 
						$dest_company_name = $destination_company_arr[0]["company_name"];
						if ($destination_company_arr[0]["section"] != "") {
							$dest_section = $destination_company_arr[0]["section"];
						} else if ($destination_company_arr[0]["department"] != "") {
							$dest_section = $destination_company_arr[0]["department"];
						}
						if ($destination_company_arr[0]["clerk"] != "") {
							$dest_clerk = $destination_company_arr[0]["clerk"];
						} else if ($destination_company_arr[0]["addressee"]) {
							$dest_clerk = $destination_company_arr[0]["addressee"];
						}
						if ($dest_company_name != "") {
							echo $dest_company_name;
						}
						if ($dest_clerk == "" && $dest_section　== "" && $dest_company_name != "") {
							echo "御中";
						} else if ($dest_company_name != "") {
							echo "<br/>";
						}
					?>
					<?php
						if ($dest_section != "") {
							echo $dest_section;
						}
						if ($dest_clerk == "" && $dest_section != "") {
							echo "御中";
						} else if ($dest_section != "") {
						 echo "<br/>";
						}
					?>
					<?php
						if ($dest_clerk != "") {
							echo $dest_clerk;
						}
						if ($dest_clerk != "") {
							echo "様";
						}
					?>
				</p>
			</td>
			<td id="mainright" style="background-image:url('<?php if ($company_arr[0]['stamp_image'] != "") {echo $company_arr[0]['stamp_image'];} else {echo '../../images/blanksign.png';}?>'); background-repeat: no-repeat; background-position: right bottom; width:240px;">
				<p>
					<?php echo $company_arr[0]["company_name"];?>
					<br>
					　<br>
				</p>
				<p>
					〒<?php
					echo $company_arr[0]["zip_code"];
					?><br>
					<?php echo $company_arr[0]["address1"];?><br/>
					<?php echo $company_arr[0]["address2"];?><br/>
					<?php
					if(! ($company_arr[0]["tel_num"] == FALSE)){
						echo "TEL：", $company_arr[0]["tel_num"];
					}
					?>
					<br/>
					<?php
					if(! ($company_arr[0]["fax_num"] == FALSE)){
						echo "FAX：", $company_arr[0]["fax_num"];
					}
					?>
				</p>
			</td>
	</table>

			<?php if ($invoice_data_receive_arr[0]['invoice_name'] != "") {echo '<p style="margin-left:50px;">件名：<span>'.$invoice_data_receive_arr[0]['invoice_name'].'</span></p>';}?>
			<?php
			if ($data_cnt <= 7) {
				$data_cnt = 7;
			}
			for ($i = 0; $i < $data_cnt; $i++) {
				$total_price_top += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
		<h3>ご請求金額 <?php echo number_format($total_price_top);?>円</h3>

	<div id="main2">
		<table id="frTBL">
			<tr>
				<th id="item">品目</th>
				<th id="cost">単価</th>
				<th id="amount">数量</th>
				<th id="price">価格</th>
			</tr>
			<?php
			for ($i = 0; $i < $data_cnt; $i++) {
			?>
			<tr>
				<td id="item" style="text-align:left"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
				<td id="cost"><?php if ($invoice_data_receive_arr[$i]['unit_price'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['unit_price']);} else { echo " ";}?></td>
				<td id="amount"><?php if ($invoice_data_receive_arr[$i]['quantity'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['quantity']);} else { echo " ";}?></td>
				<td id="price"><?php if ($invoice_data_receive_arr[$i]['total_price_excluding_tax'] != 0) { echo number_format((int)$invoice_data_receive_arr[$i]['total_price_excluding_tax']);} else { echo " ";};?></td>
			</tr>
			<?php
				$sum_withholding_tax = $sum_withholding_tax + $invoice_data_receive_arr[$i]['withholding_tax'];
				$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				$sales_tax += $invoice_data_receive_arr[$i]['sales_tax'];
				//$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
				/*
				$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
				*/
				$total_price += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
		</table>
	</div>

	<p style="border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 50px;"></p>

	<table id="main3">
		<tr>
			<td id="item">小計</td>
			<td id="amount"><?php echo number_format($sum_price);?></td>
		</tr>
		<tr>
			<td id="item">消費税</td>
			<td id="amount"><?php echo number_format($sales_tax);?></td>
		</tr>
		<?php
		if ($sum_withholding_tax > 0) {
		?>
		<tr>
			<td id="item">源泉税</td>
			<td id="amount"><?php echo "－".number_format($sum_withholding_tax);?></td>
		</tr>
		<?php
		}
		?>
		<tr>
			<td id="item">合計</td>
			<td id="amount"><?php echo number_format($total_price);?></td>
		</tr>
	</table>


	<table id="main4">
		<tr>
			<td id="left">お支払期限</td>
			<td><?php if ($invoice_data_receive_arr[0]['pay_date'] != 0) {echo str_replace(" ","",substr($invoice_data_receive_arr[0]['pay_date'],0,4)."/".substr($invoice_data_receive_arr[0]['pay_date'],4,2)."/".substr($invoice_data_receive_arr[0]['pay_date'],6,2));}?></td>
		</tr>
		<tr>
			<td>振込先</td>
			<td><?php echo $company_arr[0]["account_info"];?></td>
		</tr>
		<tr>
			<td id="gray"></td>
			<td rowspan="2">口座名義　<?php echo $company_arr[0]["account_name"];?></td>
		</tr>
		<tr>
			<td id="gray"></td>
		</tr>
		<tr>
			<td id="notes" rowspan="2">備考</td>
			<td><?php echo $company_arr[0]["remarks1"];?></td>
		</tr>
		<tr>
			<td><?php echo $invoice_data_receive_arr[0]['remarks1'];?></td>
		</tr>

	</table>
</section>


</article>
</body>


