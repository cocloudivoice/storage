<?php session_start();?>
<?php
//クラスファイルの読み込み
require_once(dirname(__FILE__).'/../../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../../db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$sum_price = 0;
$subtotal = 0;//小計
$sales_tax = 0;//消費税
$total_price = 0;//合計金額
$t_price = 0;



//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_data_send";

$destination_id = $_REQUEST['c1'];
$send_code = $_REQUEST['c2'];
$billing_date = $_REQUEST['bd'];
$invoice_code = $_REQUEST['ic'];
$pay_date = $_REQUEST['pd'];
//$insert_date = $_REQUEST['ind'];
$status = $_REQUEST['st'];

//$param = "?destination_id=".$destination_id."&send_code=".$send_code."&billing_date=".$billing_date."&invoice_code=".$invoice_code."&pay_date=".$pay_date."&status=".$status;
//echo $param;

//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['company_id'] != NULL) {
	$company_id = $_REQUEST['company_id'];
}
/*
if ($_REQUEST['c1'] != NULL) {
	$destination_id = $_REQUEST['c1'];
	$words .= " destination_id = ".$destination_id;
}
if ($_REQUEST['c2'] != NULL) {
	$send_code = $_REQUEST['c2'];
	$words .= " send_code = '".$send_code."' ";
}
*/
if ($_REQUEST['st'] != NULL) {
	$status = $_REQUEST['st'];
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
/*
if ($_REQUEST['ind'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['ind'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
*/
if ($_REQUEST['bd'] != NULL) {
	$billing_date = $_REQUEST['bd'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pd'] != NULL) {
	$pay_date = $_REQUEST['pd'];
	$words .= " AND pay_date = ".$pay_date;
}

if ($_REQUEST['ic'] != NULL) {
	$invoice_code = $_REQUEST['ic'];
	$words .= " AND invoice_code = '".$invoice_code."' ";
}

//echo $words;


/*
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = $_REQUEST['destination_id'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['send_code'] != NULL) {
	$send_code = $_REQUEST['send_code'];
	$words .= " AND send_code = ".$send_code;
}
if ($_REQUEST['status'] != NULL) {
	$status = $_REQUEST['status'];
	$words .= " AND status = ".$status;
} else {
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
if ($_REQUEST['insert_date'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['insert_date'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
if ($_REQUEST['billing_date'] != NULL) {
	$billing_date = $_REQUEST['billing_date'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pay_date'] != NULL) {
	$pay_date = $_REQUEST['pay_date'];
	$words .= " AND pay_date = ".$pay_date;
}
*/

if (isset($_REQUEST['dlp'])) {
	$flag="download_invoice_data";
	$words = $_REQUEST['dlp'];
}

$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
$flag = "";//フラグの初期化

//var_dump($_REQUEST);
//var_dump($invoice_data_receive_arr);
$data_cnt = count($invoice_data_receive_arr);
//var_dump($invoice_data_receive_arr_count);

//請求元のデータ取得
$flag = "company_data";
$claimant_id = $invoice_data_receive_arr[0]['claimant_id'];
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($company_arr);
$flag = "";//フラグの初期化

//請求先のデータ取得
$claimant_id;
if ($invoice_data_receive_arr[0]['send_code'] != NULL) {
	$flag = "addressee_data_one";
	$send_code = $invoice_data_receive_arr[0]['send_code'];
	$words = " AND company_id = ".$claimant_id." ";
	$destination_company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$words);
	//var_dump($destination_company_arr);
/*
	[0]["client_id"]
	["company_name"]
	["company_name_reading"]
	["addressee"]
	["department"]
	["position"]
	["zip_code"]
	["prefecture"]
	["address1"]
	["address2"]
	["tel_num"]
	["email"]
	["client_company_id"]
	["company_id"]
*/
} else {

	$flag = "company_data";
	$destination_id = $invoice_data_receive_arr[0]['destination_id'];
	$destination_company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
	//var_dump($destination_company_arr);
}
$flag = "";//フラグの初期化






//会社データ
$company_id = $company_arr[0]["company_id"];
$company_name = $company_arr[0]["company_name"];
$company_name_reading = $company_arr[0]["company_name_reading"];
$segment = $company_arr[0]["segment"];
$zip_code = $company_arr[0]["zip_code"];
$prefecture = $company_arr[0]["prefecture"];
$address1 = $company_arr[0]["address1"];
$address2 = $company_arr[0]["address2"];
$tel_num = $company_arr[0]["tel_num"];
$fax_num = $company_arr[0]["fax_num"];
$url = $company_arr[0]["url"];
$email = $company_arr[0]["email"];
$section = $company_arr[0]["section"];
$clerk = $company_arr[0]["clerk"];
$bank_id = $company_arr[0]["bank_id"];
$blanch_id = $company_arr[0]["blanch_id"];
$account_devision = $company_arr[0]["account_devision"];
$account_num = $company_arr[0]["account_num"];
$account_info = $company_arr[0]["account_info"];
$stamp_image = $company_arr[0]["stamp_image"];
$logotype = $company_arr[0]["logotype"];
$remarks1 = $company_arr[0]["remarks1"];
$remarks2 = $company_arr[0]["remarks2"];





?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>


<style type="text/css">





html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
table {border-collapse:collapse;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}
.left{float:left;}
input[type=text]{font-size:13px; width:270px; color:rgb(50,50,50); background-color:white; border-width:0; cursor:default; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=text].number{width:200px;}
input[type=text].total{width:148px; text-align:right;}
textarea{font-size:13px; height:30px; color:rgb(50,50,50); border-width:0;  background-color:white;cursor:default; resize:none; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
#total_price_value{margin:0 0 0 10px;}


article {width:790px; height:1125px; background-image:-webkit-radial-gradient(1500px 562px, circle farthest-side, white 96%,rgb(200,255,255) 98%, rgb(100,255,255) 100%);}
article section{width:600px; padding:40px 0 0 90px;}


article h1 {margin:40px 0 60px 0; text-align:center; font-size:20px;}
article h3 {font-size:17px;border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:100px 0 0 50px; padding:0 0 3px 0;}
article table#top{font-weight:bold; font-size:13px; letter-spacing:0.1em; margin:10px 0 0 50px;}
article table#top td#img{width:60px; text-align:center;vertical-align:bottom;}
article table#top td#spacer_td{width:100px; text-align:center;}

article table td#mainleft{width:300px; margin:40px 0 0 0;text-align:left; font-weight:normal;}
article table td#mainleft h2{font-size:16px; margin:0 0 10px 0;}
article table td#mainleft ul{list-style-type:none; margin:0 0 30px 0;}
article table td#mainleft p{margin:10px 0 10px 0;}

article table div#mainright{width:300px; margin:40px 0 0 0;text-align:left; color:rgb(100,100,100); font-weight:normal;}
article table div#mainright h2{font-size:13px; margin:20px 0 10px 0; font-weight:normal;}
article table div#mainright ul{list-style-type:none; margin:0 0 30px 0;}
article table div#mainright p{margin:10px 0 10px 0;}
article table div#mainright img#logo{max-width:250px; max-height:40px;}



table#main2{margin:60px 0 0 55px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);}
table#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px; margin: 0 0 20px 0;}
table#main2 th#item{width:250px; text-align:left;}
table#main2 th#cost{width:100px; text-align:right;}
table#main2 th#amount{width:50px; text-align:right;}
table#main2 th#price{width:100px; text-align:right;}
table#main2 td{height:35px; text-align:right;}
table#main2 td#item{text-align:left;}



table#main3{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:0 0 0 305px;}
table#main3 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px;}
table#main3 td#item{width:100px;}
table#main3 td#amount{width:150px;text-align:right;}



article table#main4{margin:50px 0 0 140px; font-size:12px;}
article table#main4 td{}
article table#main4 td#left{width:180px; height:35px;}
article table#main4 td#gray{font-size:11px; color:rgb(100,100,100);}
article table#main4 td#notes{padding:10px 0 0 0;}



table#main5{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); margin:40px 0 70px 400px;}
table#main5 td{height:30px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); font-size:14px;}
table#main5 td#txt{width:150px; padding:0 0 0 5px;}
table#main5 td#txt2{width:150px; letter-spacing:7px; padding:0 0 0 5px;}
table#main5 td#amount{width:80px; text-align:right; min-width:80px; padding:0 5px 0 0;}

</style>




</head>



<body>



<article>

<section>
	<img id="logo" src='<?php if ($company_arr[0]["logotype"] != "") { echo $company_arr[0]["logotype"];} else { echo "../../images/blanklogo.png";}?>' title='ロゴ' alt='ロゴ' style='max-width:250px;max-height:40px; margin:30px 0 30px 55px;'>
	<table id="top">
		<tr>
			<td id="mainleft">
				<h1 style="font-size:16px">請求書</h1>
				<p>
					請求書番号：<?php echo $invoice_data_receive_arr[0]['invoice_code'];?><br>
					請求日　　：<?php echo str_replace(" ","",substr($invoice_data_receive_arr[0]['billing_date'],0,4)."/".substr($invoice_data_receive_arr[0]['billing_date'],4,2)."/".substr($invoice_data_receive_arr[0]['billing_date'],6,2));?><br>
					　<br>
				</p>
				<p>
					〒<?php echo $destination_company_arr[0]["zip_code"];?><br>
					<?php echo $destination_company_arr[0]["address1"];?><br>
					<?php echo $destination_company_arr[0]["address2"];?><br>
				</p>
				<p>
					　<br>
					<?php 
						$dest_company_name = $destination_company_arr[0]["company_name"];
						if ($destination_company_arr[0]["section"] != "") {
							$dest_section = $destination_company_arr[0]["section"];
						} else if ($destination_company_arr[0]["department"] != "") {
							$dest_section = $destination_company_arr[0]["department"];
						}
						if ($destination_company_arr[0]["clerk"] != "") {
							$dest_clerk = $destination_company_arr[0]["clerk"];
						} else if ($destination_company_arr[0]["addressee"]) {
							$dest_clerk = $destination_company_arr[0]["addressee"];
						}
						if ($dest_company_name != "") {
							echo $dest_company_name;
						}
						if ($dest_clerk == "" && $dest_section　== "" && $dest_company_name != "") {
							echo "御中";
						} else if ($dest_company_name != "") {
							echo "<br/>";
						}
					?>
					<?php
						if ($dest_section != "") {
							echo $dest_section;
						}
						if ($dest_clerk == "" && $dest_section != "") {
							echo "御中";
						} else if ($dest_section != "") {
						 echo "<br/>";
						}
					?>
					<?php
						if ($dest_clerk != "") {
							echo $dest_clerk;
						}
						if ($dest_clerk != "") {
							echo "様";
						}
					?>
				</p>
			</td>
			<td id="mainright" style="background-image:url('<?php if ($company_arr[0]['stamp_image'] != "") {echo $company_arr[0]['stamp_image'];} else {echo '../../images/blanksign.png';}?>'); background-repeat: no-repeat; background-position: right bottom; width:240px;">
				<p>
					<?php echo $company_arr[0]["company_name"];?>
					<br>
					　<br>
				</p>
				<p>
					〒<?php
					echo $company_arr[0]["zip_code"];
					?><br>
					<?php echo $company_arr[0]["address1"];?><br/>
					<?php echo $company_arr[0]["address2"];?><br/>
					<?php
					if(! ($company_arr[0]["tel_num"] == FALSE)){
						echo "TEL：", $company_arr[0]["tel_num"];
					}
					?>
					<br/>
					<?php
					if(! ($company_arr[0]["fax_num"] == FALSE)){
						echo "FAX：", $company_arr[0]["fax_num"];
					}
					?>
				</p>
			</td>
	</table>

			<?php if ($invoice_data_receive_arr[0]['invoice_name'] != "") {echo '<p style="margin-left:50px;">件名：<span>'.$invoice_data_receive_arr[0]['invoice_name'].'</span></p>';}?>
			<?php
			for ($i = 0; $i < $data_cnt; $i++) {
			?>

			<?php
				$sum_withholding_tax = $sum_withholding_tax + $invoice_data_receive_arr[$i]['withholding_tax'];
				$total_price_top += $invoice_data_receive_arr[$i]['total_price'];
				$sum_price_top += $sum_price + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
				$sales_tax_top += $invoice_data_receive_arr[$i]['sales_tax'];
				
			}
			?>
		<h3>ご請求金額 <?php echo number_format($total_price_top);?>円</h3>

	<table id="main5">
		<tr>
			<td id="txt">内　　訳</td>
			<td></td>
		</tr>
		<tr>
			<td id="txt">税抜金額</td>
			<td id="amount"><?php echo number_format($sum_price_top);?>円</td>
		</tr>
		<tr>
			<td id="txt2">消費税</td>
			<td id="amount"><?php echo number_format($sales_tax_top);?>円</td>
		</tr>
		<?php
		if ($sum_withholding_tax > 0) {
		?>
		<tr>
			<td id="txt2">源泉税</td>
			<td id="amount"><?php echo "－".number_format($sum_withholding_tax);?>円</td>
		</tr>
		<?php
		}
		?>

	</table>


	<table id="main4">
		<tr>
			<td id="left">お支払期限</td>
			<td><?php if ($invoice_data_receive_arr[0]['pay_date'] != 0) {echo str_replace(" ","",substr($invoice_data_receive_arr[0]['pay_date'],0,4)."/".substr($invoice_data_receive_arr[0]['pay_date'],4,2)."/".substr($invoice_data_receive_arr[0]['pay_date'],6,2));}?></td>
		</tr>
		<tr>
			<td>振込先</td>
			<td><?php echo $company_arr[0]["account_info"];?></td>
		</tr>
		<tr>
			<td id="gray"></td>
			<td rowspan="2">口座名義　<?php echo $company_arr[0]["account_name"];?></td>
		</tr>
		<tr>
			<td id="gray"></td>
		</tr>
		<tr>
			<td id="notes" rowspan="2">備考</td>
			<td><?php echo $company_arr[0]["remarks1"];?></td>
		</tr>
		<tr>
			<td><?php echo $invoice_data_receive_arr[0]['remarks1'];?></td>
		</tr>

	</table>
</section>


</article>

<?php
	$loop_cnt = floor($data_cnt / 20);
	$loop_least = $data_cnt % 20;
if ($loop_least != 0) {
	$loop_cnt = $loop_cnt + 1;
}
for ($k = 0;$k < $loop_cnt;$k++) {
	$subtotal = 0;
	if ($k == $loop_cnt - 1) {
		$loop_num = $k * 20 + $loop_least;
		$loop_start = $loop_num - $loop_least;
	} else {
		$loop_num = ($k + 1) * 20;
		$loop_start = $loop_num - 20;
	}
?>

		<article>

		<section>
			<p style="text-align:right"><?php echo ($k + 1)."/".$loop_cnt;?></p>
				<table id="main2">
					<tr>
						<th id="item">品目</th>
						<th id="cost">単価</th>
						<th id="amount">数量</th>
						<th id="price">価格</th>
					</tr>
						<?php
						for ($i = $loop_start; $i < $loop_num; $i++) {
						?>
						<tr>
							<td id="item"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
							<td><?php if ($invoice_data_receive_arr[$i]['unit_price'] != "0") { echo number_format((int)$invoice_data_receive_arr[$i]['unit_price']);}?></td>
							<td><?php if ($invoice_data_receive_arr[$i]['quantity'] != "0") { echo number_format((int)$invoice_data_receive_arr[$i]['quantity']);}?></td>
							<td><!--<?php echo number_format((int)$t_price = $invoice_data_receive_arr[$i]['unit_price']*$invoice_data_receive_arr[$i]['quantity']);?>--><?php echo number_format((int)$invoice_data_receive_arr[$i]['total_price_excluding_tax']);?></td>
						</tr>
						<?php
							$subtotal = $subtotal + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];

							$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price_excluding_tax'];
							$sales_tax += $invoice_data_receive_arr[$i]['sales_tax'];
							//$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
							/*
							$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
							*/
							$total_price += $invoice_data_receive_arr[$i]['total_price'];
						}
						?>
				</table>
			<table id="main3">
				<tr>
					<td id="item">小計</td>
					<td id="amount"><?php echo number_format($subtotal);?></td>
				</tr>
			</table>



		</section>


		</article>
<?php
}
?>

</body>


