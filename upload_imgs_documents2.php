<?php
session_start();
//必要なクラスを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once("/var/www/storage.cloudinvoice.co.jp/html/pdf/PdfToImg.class.php");
//自動仕分け用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
//企業名照合用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/searchkeywordscontrol.class.php');
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = $_REQUEST['path'];
$file_name = $_FILES['upfile']['name'];
$upload_dir_path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
$txt_dir = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');


$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}

//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}

//▲企業データの取得▲
if ($_FILES['upfile']) {
	
	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	$counter = 0;
	$error_num = 0;
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	//var_dump($file_ary);
	
	//txtだけを先に読み込む。ブサイクだけどとりあえず。
	foreach ($file_ary as $file) {

		if (substr($file['name'], -3, 3) == "txt"||substr($file['name'], -3, 3) == "pdf") {
			//echo "テキストの処理\r\n";
			//txtファイルの保存	
			try {
					//ファイル名変換処理
				echo $file_name_top = substr($file["name"], 0, -4);echo ":top<br>\r\n";
				echo $file_extension = substr($file["name"], -4, 4);echo ":ext<br>\r\n";
				echo $dlp = md5($file_name_top.$rand_words);echo ":dlp<br>\r\n";
				echo $filename = $dlp.$file_extension;echo ":filename<br>\r\n";

				
				
				if (is_uploaded_file($file["tmp_name"])) {
					if (move_uploaded_file($file["tmp_name"], $txt_dir. "/" . $dlp.$file_extension)) {
					    chmod($txt_dir. "/" . $dlp.$file_extension, 0664);
					}
				}
			} catch (Exception $e) { 
				echo $e -> getMessage;
			}
		}

		if ( substr($file["name"], -3, 3) == "pdf") {
			
			//ファイル名変換処理
			echo $file_name_top = substr($file["name"], 0, -4);echo ":top<br>\r\n";
			echo $file_extension = substr($file["name"], -4, 4);echo ":ext<br>\r\n";
			echo $dlp = md5($file_name_top.$rand_words);echo ":dlp<br>\r\n";
			echo $filename = $dlp.$file_extension;echo ":filename<br>\r\n";

			echo $pdf_name = substr($filename, 0, -4);
			
			$im = new imagick();
			//for ($m = 1;$m <= 120;$m++) {
				
				//PDFファイルをテキスト化する。
				echo "1\r\n"; 
				echo $file_path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/";
				echo $pdf_file = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/".$file_name_top.".pdf";
				echo "2\r\n";
				//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png 2>&1");
				//echo "convert ".$file_path.".pdf -geometry 100% ".$file_path.".png";
				//pdftotextでpdfから
				//shell_exec("pdftotext -raw /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/6988b98f05ca4df774733315f7725519.pdf /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/6988b98f05ca4df774733315f7725519.txt | sed ':loop; N; $!b loop; ;s/\n//g'");
				shell_exec("pdftotext -raw ".$file_path.$dlp.".pdf ".$file_path.$dlp.".txt | sed ':loop; N; $!b loop; ;s/\n//g'");
				//$output = shell_exec("convert ".$file_path.".pdf -geometry 100% ".$file_path.".png ;");
				//shell_exec("convert /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/a_004.pdf /var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs/a_004.jpg");
				shell_exec("convert -density 300 ".$file_path.$dlp.".pdf ".$file_path.$dlp.".png > /logs/cloudinvoice/makeimg/moveImage/errlog".date('Ymdhis').".log 2>&1");
				//print_r ($output);

				//var_dump($retval);
				//var_dump($last_line);

				//$data = file_get_contents( '/var/www/storage.cloudinvoice.co.jp/html/files/common/0293f3d00a4bef30cdced1d3f615a523.txt' );
				$data = file_get_contents( $file_path.$dlp.'.txt' );
				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				var_dump($data);
				//$pattern_comp_start = "/^〒/";//郵便記号
				for ( $i = 0; $i < $cnt; $i++ ) {
					//機能停止中
					$match_num = preg_match($pattern_comp_start, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						$company_data_start_line = $i;
					}
					//電話番号取得（TELの文言で取得）機能停止中
					//$pattern_comp_end = "/TEL/";//TELの文言
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					//電話番号取得（番号を取得）
					$pattern_comp_end = "/[0-9]+-[0-9]+-[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							//echo $match[0];
							//echo "<br/>\r\n";
						}
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}

					$pattern_comp_end = "/株式会社/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "企業名候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$company_data_end_line = $i;
					}
					//請求日
					$pattern_comp_end = "/請求日/";
					//$pattern_comp_end = "/[0-9]+\/[0-9]+\/[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$match_result = str_replace(array("請求日","ご請求日","御請求日"," ","　","\t",":","："),"",$data[$i]);
						//echo "請求日候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						//echo $match_result;
						//echo "<br/>\r\n";
						//$start_line = $i + 1;
						$company_data_end_line = $i;
					}
					//支払い日
					$pattern_comp_end = "/支払/";
					//$pattern_comp_end = "/[0-9]+\/[0-9]+\/[0-9]+/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$match_result = str_replace(array("お支払い","御支払","御支払い","お支払","支払","期限","期日"," ","　","\t",":","："),"",$data[$i]);
						//echo "支払期日候補<br/>\r\n";
						$data_arr[$i] = $data[$i];
						$match_result;
						//echo "<br/>\r\n";
						//$start_line = $i + 1;
						$company_data_end_line = $i;
					}

				}

				$company_data_start_line;//echo "<br/>\r\n";
				$company_data_end_line;//echo "<br/>\r\n";
				//var_dump($data_arr);



				$pattern1 = "/^品目/";
				for ( $i = 0; $i < $cnt; $i++ ) {
					$match_num = preg_match($pattern1, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						$start_line = $i;
					}
				$pattern2 = "/^小計/";
					$match_num = preg_match($pattern2, $data[$i], $match);
					if ($match_num >= 1) {
						$data_arr[$i] = $data[$i];
						$end_line = $i - 1;
					}

					//echo($data[$i]);
					//echo "<br/>\r\n";
				}
				$start_line;//echo "<br/>\r\n";
				$end_line;//echo "<br/>\r\n";
				//var_dump($data_arr);

		//	}

			$path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/imgs_docs";
		}
	}

	//画像を読み込みながら請求書データを作成し、txtの内容を取得して上書きする。
	foreach ($file_ary as $file) {
		
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$got_amount_of_money = 0;
		$got_date = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$date_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		
		try {
	    	//var_dump($file);
	        $filename = $file['name'];
	        if (substr($filename, -3, 3) == "csv"){
	        	$csv_file_name = $filename;
	        }
			//var_dump($_FILES);
				
			//ファイル名変換処理
			$file_name_top = substr($filename, 0, -4);
			$file_extension = substr($filename, -4, 4);
			$dlp = md5($file_name_top.$rand_words);
			$filename = $dlp.$file_extension;
			
			//請求書が登録されていないか確認する。
			$flag ="download_invoice_data_clm";
			$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
			//同じdownload_passwordで請求書が登録済みの場合
			if (substr($filename, -3, 3) == "png" || substr($filename, -3, 3) == "jpg" || substr($filename, -3, 3) == "tif" || substr($filename, -3, 3) == "pdf") {
				//請求書登録
				//仮のデータで請求書登録をする。
				//仮の請求書・領収書データの登録
				
				//テキストを読んで上書き処理
				//echo "テキスト処理<br>";
				$text_file_path = $txt_dir."/".$dlp.".txt";
				//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
				$data = file_get_contents($text_file_path);
				//var_dump($data);
				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				

				for ( $i = 0; $i < $cnt; $i++ ) {
					
					
					//全角文字を半角に変換
					//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
					$data[$i] = mb_convert_kana($data[$i], 'kvrn');
					$data[$i] = str_replace(array(",","・","･",".","．"),"",$data[$i]);
					
					//-を用いた日付で時間がスペースの後に時間がついていた場合取得できなくなるため、スペースを消す前に移動した。
					$pattern_comp_end = "/([0-9]{2,4})-([0-9]{2})-([0-9]{2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date = $date_arr[$date_cnt] = $year.$month.$day;

						//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br>\r\n";
						$date_cnt++;
						$company_data_end_line = $i;
					}

					$data[$i] = str_replace(array(" ","　"),"",$data[$i]);
					$data[$i] = str_replace(array("(","（"),"-",$data[$i]);
					$data[$i] = str_replace(array(")","）"),"-",$data[$i]);
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "\r\n";
					$pattern_comp_end = "/[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "\r\n";
					$pattern_comp_end = "/[0-9]{2,4}\([0-9]{2,4}\)[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							echo $match[0];
							if (substr($match[0],0,1) == 1) {
								echo $match[0] = substr($match[0],1);
							}
							$tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					if ($tel_cnt == 0) {
						//電話番号取得（番号を取得）
						//echo $data[$i];echo "\r\n";
						$pattern_comp_end = "/[1-9]{1}[0-9]{1,3}-[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{6,8}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
						}
					}
//					$pattern_comp_end = "/株式会社*/";
/*					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						echo "企業名候補<br/>\r\n";
						echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						echo "<br/>\r\n";
						$company_data_end_line = $i;
					}
*/
					
					$pattern_comp_end = "/¥(\w*)/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					//echo $data[$i];echo "<br><br>\r\n";
					if ($match_num >= 1) {
						//echo "金額候補<br/>\r\n";
						$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥",",","，",".","．","_","＿"),"",$match[0]);echo "\r\n";
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$money_cnt++;
						$company_data_end_line = $i;
					}
					
					$pattern_comp_end = "/¥\\w*円/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "金額候補<br/>\r\n";
						$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円",",","，",".","．","_","＿"),"",$match[0]);echo "\r\n";
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$money_cnt++;
						$company_data_end_line = $i;
					}

					
					$data[$i] = str_replace(array("-","_","－","＿","・","･",".","．"),"",$data[$i]);
					$pattern_comp_end = "/([0-9]+)年([0-9]+)月([0-9]+)日/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$company_data_end_line = $i;
					}
					$pattern_comp_end = "/([0-9]+)\/([0-9]+)\/([0-9]+)/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date = $date_arr[$date_cnt] = $year.$month.$day;

						//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br>\r\n";
						$date_cnt++;
						$company_data_end_line = $i;
					}

					
					
					/*
					//キーワード文字列検索
					$pattern_comp_end = "/(東.*鉄道)/imu";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						echo "キーワード候補<br/>\r\n";
						echo $data_arr[$i] = $match[1];
						echo $keywords_arr[$key_cnt] = match[1];
						
						//$start_line = $i + 1;
						echo "<br/>\r\n";
						$key_cnt++;
						$company_data_end_line = $i;
					}
					*/

				}
				
				//情報の整理
				$got_amount_of_money = 0;
				$max_num_times = 0;
				
				//echo "金額候補を絞る";
				for ($m = 0;$m < count($money_arr);$m++){
					$temp_money = substr($money_arr[$m],2);
					$num_of_same = 0;
					$candidate_box = 0;
					//同じ数字が2回以上あるものを取得するロジック
					for ($h = 0;$h < count($money_arr);$h++){
						$temp_money2 = substr($money_arr[$h],2);
						if ($temp_money == $temp_money2) {
							$num_of_same++;
						}
					}
					//2回以上のものの中から回数が最大のもの
					if ($num_of_same >= 2) {
						/*
						if ($max_num_times < $num_of_same) {
							$max_num_times = $num_of_same;
							$got_amount_of_money = $temp_money;
						} else if ($max_num_times == $num_of_same) {
						*/
							//2回以上のものの中で最大の金額
							if($got_amount_of_money <= $temp_money) {
								$got_amount_of_money = $temp_money;
							}
						//}
					}
					
					/*最大の金額を取得するロジック
					if ($got_amount_of_money == $temp_money) {
						$m
					} else {
						if ($got_amount_of_money < $temp_money) {
							//echo "naka1<br>";
							$got_amount_of_money = $temp_money;
						//echo "\r\n";
						}
					}
					*/
				}
				//2回以上のものが無かった場合、一番上の金額を入れる。
				if ($got_amount_of_money == 0 ) {
					$got_amount_of_money = substr($money_arr[0],2);
				}
				
				//日付
				for ($d = 0;$d < count($date_arr);$d++){
					//候補の中の一番最初のものを日付にする。（暫定）
					$date_arr[$d];
					$got_date = $date_arr[0];
					//echo "\r\n";
				}

				//電話番号
				$exist_flag = 0;
				for ($t = 0;$t < count($tel_arr);$t++){
					echo "tel選択：";
					echo substr($tel_arr[$t],0,1);
					if (substr($tel_arr[$t],0,1) == 0) {
						$tel_num = $tel_arr[$t];
					}
					
					
					if ($exist_flag == 0) {
						//echo "\r\n";
						
						$flag = "search_keywords";
						$search_result_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$tel_num,$aj_words2);
						$flag = "";
						//var_dump($search_result_arr);
						for($k = 0;$k < count($search_result_arr);$k++) {
							if ($search_result_arr[$k]["company_name"] != "") {
								echo $search_result_arr[0]["company_name"] = $search_result_arr[$k]["company_name"];
							}
						}
						
						//企業データ取得▼
						//電話番号で存在確認
						if ($tel_num != "") {

							$flag = "company_data_from_tel";
							$words = "";
							$searched_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$tel_num,$words);
							var_dump($company_data_arr);
							if (isset($searched_company_data_arr[0]['tel_num'])) {
								echo "TELあり<br/>\r\n";
								$exist_flag = 1;
							}// else{echo "TELなし<br/>\r\n";}
						}
						//電話番号が無かった場合、企業名で存在確認
						if($exist_flag == 0 && $search_result_arr[0]["company_name"] != "") {
							echo "名前確認";
							$flag = "company_data_from_name";
							$words = "";
							$searched_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$search_result_arr[0]["company_name"],$words);
							var_dump($searched_company_data_arr);

						}
						if (isset($searched_company_data_arr[0]['company_name'])) {
							//キーワード検索で企業データ取得できた場合
							echo "名前あり<br/>\r\n";
							echo $searched_company_name = $searched_company_data_arr[0]['company_name'];
							//	echo "\r\n";
							$claimant_id = $searched_company_data_arr[0]["company_id"];
							//echo "\r\n";
							$ind_type = $searched_company_data_arr[0]["industry_type_s"];
							//echo "\r\n";
							$dlp;
							$exist_flag = 1;
							//$flag = "up_invoice_download_password";
							//$set_sql = "`claimant_id` = '".$claimant_id."' WHERE `download_password` = '".$dlp."' AND `claimant_id` = '009999999999'";
							//$res_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$set_sql);
							//var_dump($res_arr);

						} else {
							echo "名前なし<br/>\r\n";
							//電話番号でも取得できなかった場合、マピオンで電話番号検索をして、情報があれば会社を作って渡す。
							
							//ロコテスト
							echo "searchapiMAP<br/><br/>\r\n\r\n\r\n";
							//$tel_get_file = file_get_contents("http://searchapi-stg.mapion.co.jp/search/ver1/wordsearch/phone/?key=MA10&q=".$tel_num);
							//$xml = simplexml_load_file("http://searchapi-stg.mapion.co.jp/search/ver1/wordsearch/phone/?key=MA10&q=".$tel_num);
							$xml = simplexml_load_file("http://searchapi-stg.mapion.co.jp/search/ver1/wordsearch/phone/?key=MA10&q=".$tel_num);
							//print_r($xml);
							//$json = file_get_contents( "http://searchapi-stg.mapion.co.jp/search/ver1/wordsearch/phone/?key=MA10&q=".$tel_num );
							$json = json_encode($xml);
							//$json = trim(substr($json, 2), '()');
							// パースに失敗した時は処理終了
							$map_result_arr = json_decode($json,TRUE);
							//$obj = json_decode($json);
							//var_dump( $obj->result );
							//var_dump($map_result_arr);
							//var_dump( $map_result_arr["Result"]["ResultList"]["Item"]);
							
							echo $new_company_name = $map_result_arr["Result"]["ResultList"]["Item"]["poi_name"];
							echo "<br>";
							echo $company_name_reading = $map_result_arr["Result"]["ResultList"]["Item"]["poi_yomi"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["lat"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["lon"];
							echo "<br>";
							echo $prefecture = $map_result_arr["Result"]["ResultList"]["Item"]["pref_name"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["city_name"];
							echo "<br>";
							echo $zip_code = $map_result_arr["Result"]["ResultList"]["Item"]["zip"];
							echo "<br>";
							echo $address_full= $address1 = $map_result_arr["Result"]["ResultList"]["Item"]["address"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["address_yomi"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["gnr1_name"];
							echo "<br>";
							echo $industry_type_s = $map_result_arr["Result"]["ResultList"]["Item"]["gnr2_name"];
							echo "<br>";
							echo $map_result_arr["Result"]["ResultList"]["Item"]["phone_number"];
							echo "<br>";
							var_dump($map_result_arr["Result"]["ResultList"]["Item"]["near_station_code"]["Item"]);
							//echo "<br>";
							var_dump($map_result_arr["Result"]["ResultList"]["Item"]["near_station_name"]["Item"]);
							//echo "<br>";
							var_dump($map_result_arr["Result"]["ResultList"]["Item"]["near_station_distance"]["Item"]);
							//echo "<br>";
							//$pattern_1 = '/class="gnr"(?*)</';
							//echo "ロコ";
							//preg_match($pattern_1,$tel_get_file,$match);
							//var_dump($match);
							/*
							<p class="gnr">ホテル,&nbsp;ホテル、旅館</p>
							<div class="lc">
							<p class="exp">東京の新しい文化の拠点「東京ミッドタウン」内に誕生した最高級ホテル。都内一高いビルの高層部に位置...</p>
							<div class="photo">
							<a href="http://loco.yahoo.co.jp/place/g-ZVAlZqva79A/" onmousedown="this.href='http://ord.yahoo.co.jp/o/loco/_ylt=A7dPeB49mK5VEGwADSTONgB8/SIG=120tki9v4/EXP=1437591997/**http%3a//loco.yahoo.co.jp/place/g-ZVAlZqva79A/';" class="img">
							<img src="http://iwiz-locosearch.c.yimg.jp/im_siggebL63ji1uBvBHIm26bCknw---x240-y240-bd1-cwc240-chc240-bdx240-bdy240-pris-q45/p/locosearch/tYNf3VVxwzA5FvyHEjBmDBR1bCSeZU4i7sN7ijoYzfclulZZtW6J7fxAGNF3_sibLWRBd6SP6oqEmoCYeoL8HA2V4TICyN4_2HUfQhvknl2qNbbNWndX.hJ_NsjS_w--" width="240" height="240" alt="ザ・リッツ・カールトン東京">
							</a>
							</div>
							</div><!-- /.lc -->
							*/
							//ロコテスト
							
							echo "tes1<br/>\r\n";
							//会社の情報が存在したら新しい会社のユーザーを作る。
							if ($new_company_name != NULL || $new_company_name != "") {
								echo "tes2<br/>\r\n";
								//echo "請求側の企業が存在しない時の処理<br/>\r\n";
								//仮メールアドレス生成
								$mail_address = md5(random(12)).rand(4)."@user.storage.cloudinvoice.co.jp";
								$user_con = new user_control();
								//Emailアドレスの存在を確認する。		
								$login_check_arr = $user_con -> user_select_email($pdo,$mail_address);

								if ($login_check_arr != NULL) {
									echo "tes3<br/>\r\n";
									$login_arr = $user_con -> user_select($pdo,$mail_address);

									//$passwordmaker = random(12);
									//$passwordmaker = uniqid();
									$passwordmaker = "1234abcd";
									$password = $login_arr['password'] = md5($passwordmaker);
									//$login_arr['nick_name'] = $user_name;
									//$user_con -> user_update($pdo,$login_arr);
									$update_data = " `password` = '".$password."' ";
									$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

								}

								$user_arr = $user_con -> user_select($pdo,$mail_address);

								//ユーザー登録
								$user_arr['nick_name']   = $new_company_name;
								$user_arr['mail_address'] = $mail_address;
								$user_arr['tel_num'] = $tel_num;
								$user_arr['non_regular_flag'] = 2;//$non_regular_flag;
								//共通コードの生成と取得
								if (!$user_arr['user_id']){
									echo "tes4<br/>\r\n";
									$user_num = $user_con -> user_get_new_num($pdo);
									$user_arr['user_id'] = $user_num;
								}
								$result = $user_con -> user_insert($pdo,$user_arr);

								//企業データ登録▼
								$claimant_id = "".$user_arr['user_id'];
								$sql = 'SELECT * FROM `COMPANY_TABLE` WHERE `company_id` = '.$user_arr['user_id'].' AND `company_name` = "'.$user_arr['nick_name'].'" ;';
								$company_arr = $company_con -> company_sql($pdo,$user_arr['user_id'],$sql);
								$com_exists_flag = $company_arr['chk']['select_count'];
								if ($com_exists_flag == 0) {
									echo "tes5<br/>\r\n";
									//echo "挿入<br/>";
								echo	$sql = '
									INSERT INTO `COMPANY_TABLE` ( 
										`company_id`, `company_name`, `company_name_reading`, `zip_code`, `prefecture`, `address1`, `address_full`,`email`, `tel_num`, `non_regular_flag`, `industry_type_s`
									) VALUES ( 
										'.$user_arr['user_id'].', "'.$user_arr['nick_name'].'","'.$company_name_reading.'", "'.$zip_code.'", "'.$prefecture.'", "'.$address1.'", "'.$address_full.'", "'.$user_arr['mail_address'].'","'.$user_arr['tel_num'].'","'.$user_arr['non_regular_flag'].'","'.$industry_type_s.'"
									)';
								}
								$company_arr = $company_con -> company_sql($pdo,$claimant_id,$sql);
								echo "tes6<br/>\r\n";
								//企業データ登録▲
								
								//▼自動仕訳用企業DB登録▼
								//企業品目テーブルと企業用レコードテーブルを登録
								$table_name = "".sprintf("%012d", $claimant_company_id);
								echo "tes7<br/>\r\n";
								try {
									$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
									$company_num++;
								} catch (PDOException $e) {
								    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
								    $_SESSION['error_msg'] = $e -> getMessage();
								}
								echo "tes8<br/>\r\n";
								//▲自動仕訳用企業DB登録▲
								$exist_flag = 1;
							}
							echo "名前照合\r\n";
							//企業データ取得▲
						}
					}
					//企業データ取得▲
				}
				
				
				
				
				//▼仮請求書データの中身▼
				//echo "請求者番号\r\n";
				//echo $claimant_id;
				if ($claimant_id == NULL || $claimant_id == "") {
					$claimant_id = "009999999999";//sprintf("%012d", $claimant_id);
				}
				$top_n = substr($claimant_id,0,4);
				$mid_n = substr($claimant_id,4,4);
				$destination_company_id = $company_id;
				if ($got_date) {
					$pay_date = $paid_date = $got_date;
				} else {
					$pay_date = $paid_date = "20150101";
				}
				if ($claimant_id == NULL || $claimant_id == "") {
					$product_name = "";
				} else {
					$product_name = $searched_company_name;
				}
				$invoice_code = "90".date("Ymd").$rand_num.$counter;
				if ($got_amount_of_money > 0) {
					$total_price = $got_amount_of_money;
					$sales_tax = round($got_amount_of_money /108 * 8);
					$withholding_tax = $got_amount_of_money - $sales_tax;
				} else {
					$total_price = 0;
					$sales_tax = 0;
					$withholding_tax = 0;
				}
				$serial_number = $dlp."0";
				$download_password = $dlp;
				$paper_type = 1;
				$data_entry_flag = 2;
				

				
				//▲仮請求書データの中身▲
				$path = "/var/www/storage.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $claimant_id)."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				//echo "<br/>\r\n";
				$pdf_url = "/var/www/storage.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $claimant_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".file_name_top.".pdf";
				//echo "<br/>\r\n";
				$png_url = "/var/www/storage.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $claimant_id)."/pdf/".date('Y', strtotime($pay_date))."/".date('m', strtotime($pay_date))."/".file_name_top.".png";
				//echo "<br/>\r\n";
				//echo "<br/>\r\n";
				
				//共通コードでの企業データのヒストリカル取得
				//var_dump($value);//."<br/><br/>";
				if ($claimant_id != "") {
					$flag = "company_data_historical";
					$claimant_historical = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$cond_a);
					$c_historical = $claimant_historical[0]['historical'];
					if ($c_historical == NULL) {
						$c_historical = 0;
					}
				}

				if ($destination_company_id != "") {
					$flag = "company_data_historical";
					
					$destination_historical = $company_con -> company_sql_flag($pdo,$flag,$destination_company_id,$cond_b);
					$d_historical = $destination_historical[0]['historical'];
					if ($d_historical == NULL) {
						$d_historical = 0;
					}
				}
				
				echo $sql = '
					INSERT INTO `INVOICE_DATA_TABLE`(
						`claimant_id`, `destination_id`, `claimant_historical`, `destination_historical`, `billing_date`,
						`invoice_code`, `invoice_name`, `pay_date`,
						`product_name`, `unit_price`, `quantity`,
						`total_price_excluding_tax`,`sales_tax`, `withholding_tax`, `total_price`,
						`tel_num`,`serial_number`, `paid_date`,`journalized_code`,`status`, `download_password`,
						`template_type`,`paper_type`,`data_entry_flag`,`pdf_url`, `insert_date`
					) VALUES (
						'.$claimant_id.','.$destination_company_id.', "'.$c_historical.'", "'.$d_historical.'", '.$pay_date.',
						"'.$invoice_code.'","'.$product_name.'",'.$pay_date.',
						"'.$product_name.'",'.($total_price - $sales_tax).',1,
						'.($total_price - $sales_tax).',"'.$sales_tax.'","'.$withholding_tax.'",'.$total_price.',
						"'.$tel_num.'","'.$serial_number.'","'.$paid_date.'","'.$journalized_code.'",9,"'.$download_password.'",
						1,'.$paper_type.',"'.$data_entry_flag.'","'.$path.'","'.date('Y-m-d H:i:s').'");
				';

				$result = $invoice_con -> invoice_data_sql($pdo,$claimant_id,$sql);
				//var_dump($result);
			
				//画像ファイルの場合の処理
				//echo "画像<br>";
				
				if ($file['name']) {
					if ( mkdir( $upload_dir_path, 0775, true ) ) {
						chmod( $upload_dir_path, 0775);
						recurse_chown_chgrp($upload_dir_path, $uid, $gid);
						//chown($upload_dir_path, $uid);
			            //chgrp($upload_dir_path, "dev");
					//  echo "ディレクトリ作成成功！！";
					} else {
					//  echo "ディレクトリ作成失敗！！";
					}
				}

				if (is_uploaded_file($file["tmp_name"])) {
					if (move_uploaded_file($file["tmp_name"], $upload_dir_path. "/" . $filename)) {
					    chmod($upload_dir_path. "/" . $filename, 0664);
					} else {
						$error_num++;
					}
				}

				//証憑ファイルの保存		
				if (!file_exists($pdf_url)) {
					//サーバーにPDFファイルが存在しない場合
					//PDFファイルを保存する
					$pdf_file = ''.$dlp.'.pdf';
					//請求元企業のディレクトリに入る
					try {

						if (mkdir( $path, 0775, true)) {
							chmod( $path, 0775, true );
							chgrp($path, "dev", true );
							//echo "ディレクトリ作成成功！！";
						} else {
							//echo "ディレクトリ作成失敗！！";
						}

					} catch (Exception $e) { 
						echo $e -> getMessage;
					}
				}
				if (!file_exists($png_url) || !file_exists($pdf_url)) {
					try {
//						echo "PDF、PNG移動";
						//echo "<br/>\r\n";
						// ファイルの存在確認
						//PNGファイルを保存
						
						//削除用
						//	echo $path2 = "/var/www/storage.cloudinvoice.co.jp/html/files/".$rm_data;
						//	echo "<br/>\r\n";
						//	shell_exec("rm -Rf ".$path2);
						//	shell_exec("rm -Rf ".$path2);
						$pdftoimg_con -> moveImage2($upload_dir_path,$path,$dlp);
						//移動した画像を../pdf/nru/に戻すときに使う
						//$pdftoimg_con -> moveImageReverse($pdf_dir,$dlp);
					} catch (Exception $e) {
						echo $e -> getMessage;
					}
				}
				
			}
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
		//取得したTEXTファイルを削除する。
		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
		$counter++;
	}//foreach文の終わり
}


if ($counter > 0) {
	$counter = $counter;
	$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをアップロードしました。";
}
if($error_num > 0) {
	$_SESSION['up_info_msg'] = $error_num."件のファイルをアップロードできませんでした。";
}
//echo $counter;
$file_list_arr = getFileList($path);
//var_dump($file_list_arr);
/*
header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='https://storage.cloudinvoice.co.jp/invoice/imgs_documents_upload'</script>";
echo "</html>";
echo "<input type='button' value='移動' onclick='location.href=\'./imgs_documents_upload\''>";

*/
//header("Location:https://storage.cloudinvoice.co.jp/invoice/imgs_documents_upload",true);

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {

            $typepath = $mypath . "/" . $file ;

            //print $typepath. " : " . filetype ($typepath). "<BR>" ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
			
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>