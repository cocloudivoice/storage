<?php

session_start();

if(isset($_SESSION['check'])){
	$passErr = $_SESSION['check'];
}

if(isset($_SESSION['emailcheck'])){
	$noEmailErr = $_SESSION['emailcheck'];
}

if(isset($_SESSION['duplicationError'])){
	$duplicationEmailErr = $_SESSION['duplicationError'];
}

?>


<!DOCTYPE html>

<html Lang="ja">





<head>
        <title>税理士事務所ログイン画面 - Cloud Invoice</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
	    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
        <!--<link rel="stylesheet" type="text/css" href="css/style.css" />-->

		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/ChunkFive_400.font.js" type="text/javascript"></script>

<script type="text/javascript">
<!-- 
function checkSub() {
	var	pass1 = document.register.yourpass1.value;
	var pass2 = document.register.yourpass2.value;
	var name = document.register.yourname.value;
	var email = document.register.youraddress.value;
//	alert(pass1 + pass2 + name + email);

	if (pass1 != "" && pass2 !="" && name != "" && email !="") {
		if (pass1 === pass2) {
			var n = $('#chksub:checked').length;
			if (n == 1){
				if(check(email) == true) {
					document.register.submit();
				}
			} else {
				alert("個人情報の送信に同意されていません。");
				return false;
			}
		} else {
			alert("パスワードが一致しません。");
			return false;
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function login() {
	var val1 = document.loginfrm.id.value;
	var val2 = document.loginfrm.password.value;
//	alert(val1 + val2);

	if (val1 != "" && val2 !="") {
//開発効率上、便宜的にコメントアウト。リリース時には有効にする。
		if(check(val1) == true) {
			document.loginfrm.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function checkEmail() {
alert("teststest");
	var emailval = document.reminderfrm.email.value;
	//alert(emailval);

	if (emailval != "") {
		if(check(emailval) == true) {
			document.reminderfrm.submit();
		}
	} else {
		alert("入力に誤りがあります。");
		return false;
	}
}

function check(val){
	var flag = 0;
	// 設定開始（チェックする項目を設定）

	if(!val.match(/.+@.+\..+/)){
		flag = 1;
	}
	// 設定終了
	if(flag){
		window.alert('メールアドレスが正しくありません'); // メールアドレス以外が入力された場合は警告ダイアログを表示
		return false; // 送信を中止
	}
	else{
		return true; // 送信を実行
	}
}

// -->
</script>


<style type="text/css">


html *{margin:0px; padding:0px;}
body {font: 14px/20px 'メイリオ','Hiragino Kaku Gothic Pro','sans-serif'; outline:none;}
a{text-decoration:none; color:blue}
a:hover{text-decoration:underline; color:rgb(50,180,240);}
button{cursor:pointer; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50); padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}

div#clear{clear:both;}

header{width:510px; margin:10px auto 0 auto;}
header h1{float:left; font-size:25px; line-height:40px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}

header ul{text-align:right; line-height:35px; margin:0 10px 0 0; font-size:13px;}
header li{display:inline;}


article{width:510px;padding:0 0 30px 0; margin:20px auto 0 auto; border-color:blue; border-radius:5px; border-width:1px; border-style:solid; border-color:rgb(180,210,210)}
article h2{margin:12px 0 0 20px; color:rgb(50,50,50); font-size:18px; color:rgb(240,132,0)}
article p{margin:20px 0 0 20px; font-weight:bold; color:rgb(255,120,0);}
article a#link{margin:0 15px 0 0;}

div#m-1-box{margin:15px 0 0 30px;}
div#m-1-box h3{font-size:14px; float:left; text-align:right; line-height:19px;}
div#m-1-box input[type=text]{width:210px; height:17px; margin:0 0 0 3px; border-color:rgb(247,247,247); border-width:1px;}

div#m-2-box{margin:15px 0 0 30px;}
div#m-2-box input[type=radio]{float:left; margin:5px 3px 0 0;}
div#m-2-box input[type=text]{width:130px; height:17px; margin:0 0 0 3px; border-color:rgb(247,247,247); border-width:1px;}
div#m-2-box h3{font-size:14px; float:left;}
div#m-2-box a{font-size:11px; margin:0 0 0 210px;}
div#m-2-box input[type=password]{display:block;margin:0 0 5px 0;}
div#m-2-box input[type=button]{cursor:pointer; border:none; width:350px; height:30px; margin:10px 0 0 0; background:linear-gradient(rgb(80,230,230),rgb(60,200,200)); color:white; font-weight:bold; font-size:14px; letter-spacing:2px; border-radius:5px; box-shadow:1px 1px 2px rgb(100,100,100); float:left;}
div#m-2-box input[type=button]:hover{background:linear-gradient(rgb(95,235,235),rgb(70,212,212));}
div#m-2-box a#pass{font-size:11px; margin:0 }
div#m-2-box img{width:100px; margin:0 0 0 10px;}
div#m-2-box a#ssl{margin:0}


section{width:510px; margin:30px auto 0 auto;}
section h3{margin:0 0 0 20px;}
section p{margin:10px 0 10px 20px;}



footer{width:510px; border-width:1px 0 0 0; border-style:solid; border-color:rgb(180,210,210); margin:30px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:11px;}
small{margin:0;}

span.error{
	visibility:hidden;
	color:red;
	font-size:11px;
	font-style:italic;
}
div#m-2-box a.fsize1{	font-size:1em;}

</style>



<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>




<body>

<?php include_once("./header.html"); ?>


<article>
<form name="loginfrm" class="login active" action="./af_login_check" method="post">
	
	<h2>
		サインイン
	</h2>
	<p>
		Eメールアドレスを入力してください
	</p>

	<div id="m-1-box">
		<h3>
			Eメールアドレス：
		</h3>
		<input type="text" name="id" style="ime-mode: disabled;" />
		<span class="error">Emailの登録がありません</span>
	</div>
	
	<p>
		Cloud Invoice にパスワードは登録しましたか？
	</p>
	
	<div id="m-2-box">
		<h3>
			初めて利用します
		</h3>
		<a href="./signup" rel="register" class="linkform fsize1">新規登録はこちら</a>
	</div>
	
	<div id="clear"></div>
	
	<div id="m-2-box">
		<h3>
			登録している方はパスワードを入力してください：
		</h3>
		<input type="password" name="password" />
		<a id="pass" href="./forgotpassword">パスワードをお忘れですか？</a>
		<span class="err" style="margin-left:30px;color:red;"><?php if($passErr){echo $passErr."\n";} ?><!--ログイン失敗を通知--></span>
		<span class="error">パスワードエラー</span>
		<div id="clear"></div>
		<div>
			<input type="button" value="サインイン（セキュリティシステムを使う）" onclick="login();"></input>
			<a id="ssl" href="http://www.onlinessl.jp/product/rapidssl.html" target="_blank"><img src="/images/rapidssl.png"></a>
		</div>
	</div>
	<div id="clear"></div>
</from>	
</article>



<section>
	<h3>
		サインインのヘルプ
	</h3>

	<p>
		パスワードをお忘れですか？
		<a href="./forgotpassword">パスワードの再設定</a>。
	</p>
	
</section>

<?php include_once("./footer.html"); ?>
	
</body>
<?php unset($_SESSION['check']);
	setcookie('PHPSESSID', $count, time() - 1800);
	unset($_COOKIE['PHPSESSID']);
	session_destroy();
	unset($_SESSION);
?>