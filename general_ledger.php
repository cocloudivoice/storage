<?php
session_start();

//必要なクラスの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
require_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');
//echo "test1";
require_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');
require_once(dirname(__FILE__).'/class/ExAccSoftWareCSV.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
require_once(dirname(__FILE__).'/../cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
$top4 = "";
$mid4 = "";
$user_id = sprintf('%012d',$user_id);
$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);

$setting_path = dirname(__FILE__).'/../files/'.$top4.'/'.$mid4.'/'.$user_id.'/settings/';
$devision_path = dirname(__FILE__).'/files/settings/';

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];
$acc_soft_type = $company_data_num[0]["acc_soft_type"];//会計ソフトの種別
if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}


//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = dirname(__FILE__).'/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            if ($buffer != "") {
            	$tax_class = split(",",$buffer);
            } else {
            	$tax_class[0] = "";
            	$tax_class[1] = "";
            }
           	$tax_class_arr[$tax_class[0]] = $tax_class[1];
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

//▼税区分ファイルの読み込み▼
$file_path = $setting_path.'tax.json';
$tax_json = file_get_contents($file_path);
//var_dump($tax_json);
$tax_json = mb_convert_encoding($tax_json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
//var_dump($tax_json);
$tax_arr = json_decode($tax_json,true);
//var_dump($tax_arr);
//echo $tax_arr["対象外"];
//▲税区分ファイルの読み込み▲

//▼勘定科目ファイルの読み込み▼
$file_path = $setting_path.'account.json';
$account_json = file_get_contents($file_path);
//var_dump($account_json);
$account_json = mb_convert_encoding($account_json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
//var_dump($account_json);
$account_json = json_decode($account_json,true);
//ksort($account_json);
//echo count($account_json);
//var_dump($account_json);

$account_titles_arr = array_keys($account_json);
natsort($account_titles_arr);

//echo $account_json["仮払消費税等"]["sales_tax"];
//echo $account_json["仮払消費税等"]["division"];
//echo $account_json["仮払消費税等"]["account_code"];
//$account_json[$account_name]["account_code"];
//$account_json->{""};
//▲勘定科目ファイルの読み込み▲
//▼試算表項目設定ファイルの読み込み▼
$file_path = $devision_path.'devision.json';
$devision_json = file_get_contents($file_path);
//var_dump($devision_json);
$devision_json = mb_convert_encoding($devision_json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
//var_dump($section_json);
$devision_json = json_decode($devision_json,true);
//var_dump($devision_json);
//echo $devision_json["現預金"]["type"];
//echo $devision_json["現預金"]["order"];
//$account_json->{""};
//▲試算表項目設定ファイルの読み込み▲

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

//if ( $_REQUEST['mode'] === 'download' ) {
	//var_dump($_REQUEST);
	//echo $_POST['aj_words'];
	if (isset($_POST['aj_words'])) {
	//echo "1";
		$aj_words = $_POST['aj_words'];
	} else {
		//echo "2";
		//絞込み条件
		$aj_words = "";
		$aj_words2 .= "";
		$aj_words_bs = "";
		$aj_words2_bs = "";
		if ($_REQUEST['paydate_from']) {
			$payfrom = $_REQUEST['paydate_from'];
			$aj_words .= " AND `paid_date` >= ".$payfrom." ";
			$aj_words_bs .= " AND `paid_date` < ".$payfrom." ";
		} else {
			$aj_words_bs .= " AND `paid_date` < '1970-01-01' ";
		}
		if ($_REQUEST['paydate_to']) {
			$payto = $_REQUEST['paydate_to'];
			$aj_words .= " AND `paid_date` <= ".$payto." ";
		}
		if ($_REQUEST['debit_account_name'] == "") {
			$_REQUEST['debit_account_name'] = "現金";
		}
		if ($_REQUEST['debit_account_name']) {
			$debit_account_name = $_REQUEST['debit_account_name'];
			$aj_words .= " AND (`debit_account_name` LIKE '%".$debit_account_name."%' OR `credit_account_name` LIKE '%".$debit_account_name."%') ";
			$aj_words_bs .= " AND (`debit_account_name` LIKE '%".$debit_account_name."%' OR `credit_account_name` LIKE '%".$debit_account_name."%') ";
		}
		if ($_REQUEST['debit_sub_account_name']) {
			$debit_sub_account_name = $_REQUEST['debit_sub_account_name'];
			$aj_words .= " AND (`debit_sub_account_name` LIKE '%".$debit_sub_account_name."%' OR `credit_sub_account_name` LIKE '%".$debit_sub_account_name."%') ";
			$aj_words_bs .= " AND (`debit_sub_account_name` LIKE '%".$debit_sub_account_name."%' OR `credit_sub_account_name` LIKE '%".$debit_sub_account_name."%') ";
		}
		if ($_REQUEST['debit_section_name']) {
			$debit_section_name = $_REQUEST['debit_section_name'];
			$aj_words .= " AND (`debit_section_name` LIKE '%".$debit_section_name."%'OR `credit_sub_account_name` LIKE '%".$debit_section_name."%') ";
			$aj_words_bs .= " AND (`debit_section_name` LIKE '%".$debit_section_name."%'OR `credit_sub_account_name` LIKE '%".$debit_section_name."%') ";
		}

		if ($_REQUEST['remarks']) {
			$remarks = $_REQUEST['remarks'];
			$aj_words .= " AND `remarks` LIKE '%".$remarks."%' ";
			$aj_words_bs .= " AND `remarks` LIKE '%".$remarks."%' ";
		}

	}

	//ページ条件なしでデータ数を取得
	$flag ="select_journalized_history";
	$table_name = $company_id;
	$data_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words);
	//var_dump($journalized_history_arr);
	
	//var_dump($data_num_arr);
	//表示数の制御
	$max_disp = 100;
	$sort_key = "`paid_date`";
	//$sort_key = "`debit_account_name`,`credit_account_name`,`paid_date`";
	$sort_type = "ASC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	if(!isset($_POST['aj_words'])) {
		$aj_words2 .= " ORDER BY ";
		$aj_words2 .= $conditions;
		$aj_words2_bs .= "  ".$aj_words2;
		
	}
	$flag = "select_journalized_history";
	$table_name = $company_id;
	$journalized_history_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words,$aj_words2);
	//echo "<br>xxx<br>";echo $aj_words_bs." ".$aj_words2_bs;
	$flag = "select_journalized_history_group_sum2";
	$journalized_history_before_start_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$company_id,$aj_words_bs,$aj_words2_bs);
	//var_dump($journalized_history_arr);
	//echo count($journalized_history_arr);
	
	//変数の初期化
	$dan = "";
	$can = "";
	$dres = 0;
	$cres = 0;
	$carryforwards = 0;//繰越金（carryforwards）
	$cfcnt = count($journalized_history_before_start_arr);
	for ($mcnt = 0;$mcnt < $cfcnt;$mcnt++) {
		//echo "<br>";
		$dan = $journalized_history_before_start_arr[$mcnt]["debit_account_name"];//echo " debit_name<br>";
		$can = $journalized_history_before_start_arr[$mcnt]["credit_account_name"];//echo " credit_name<br>";
		$dres = $journalized_history_before_start_arr[$mcnt]["debit"];//echo "<br>";
		$cres = $journalized_history_before_start_arr[$mcnt]["credit"];//echo "<br>";
		
		//資産区分によって貸借が変わるのでそのように変更した20171117hmsk
		$dvs = $account_json[$dan]["devision"];
		//echo " 中区分<br>";
		$dvs_type = $devision_json[$dvs]["type"];
		//echo " 資産区分<br>";
		if($dvs_type == "資産"||$dvs_type == "費用") {
			if ($dan == $debit_account_name) {
				$carryforwards += $dres;
			} else if ($can == $debit_account_name) {
				$carryforwards -= $cres;
			}
		} else if($dvs_type == "負債"||$dvs_type == "資本"||$dvs_type == "収益") {
			if ($dan == $debit_account_name) {
				$carryforwards -= $dres;
			} else if ($can == $debit_account_name) {
				$carryforwards += $cres;
			}
		}
		
		
	}//for文の終わり(繰越金の処理)
	//echo "<br>";echo $carryforwards;
//}

if ( $_REQUEST['csv'] === 'csv' ) {
//incioce_code_checkerとこの行の請求書番号が一致しなければ
//新しい請求書と判断しヘッダーをつける。
//弥生形式の方はヘッダーなし
	//初期化
	$csv_data = "";
	for ($i = 0;$i < $data_num;$i++) {
		//CSV出力時の並べ替え用配列を作成
		$CsvDataArr = array();

		//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
		$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
		$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
		//echo $journalized_history_arr[$i]["serial_num"];
		$CsvDataArr["serial_num"] = $serial_num = $journalized_history_arr[$i]["serial_num"];
		$claimant_id = $journalized_history_arr[$i]["company_id"];
		//echo ",会社名：";
		$CsvDataArr["company_name"] = $company_name = $journalized_history_arr[$i]["company_name"];
		//echo ",借方勘定科目：";
		$CsvDataArr["debit_account_name"] = $debit_account_name = $journalized_history_arr[$i]["debit_account_name"];
		//echo ",借方勘定科目コード：";
		$CsvDataArr["debit_account_code"] = $debit_account_code = $journalized_history_arr[$i]["debit_account_code"];
		//echo ",借方補助科目：";
		$CsvDataArr["debit_sub_account_name"] = $debit_sub_account_name = $journalized_history_arr[$i]["debit_sub_account_name"];
		//echo ",借方補助科目コード：";
		$CsvDataArr["debit_sub_account_code"] = $debit_sub_account_code = $journalized_history_arr[$i]["debit_sub_account_code"];
		//echo ",借方部門：";
		$CsvDataArr["debit_section_name"] = $debit_section_name = $journalized_history_arr[$i]["debit_section_name"];
		//echo ",借方部門コード：";
		$CsvDataArr["debit_section_code"] = $debit_section_code = $journalized_history_arr[$i]["debit_section_code"];
		//echo ",回数：";
		$CsvDataArr["flag1"] = $flag1 = $journalized_history_arr[$i]["flag1"];
		//echo ",請求日：";
		$CsvDataArr["billing_date"] = $billing_date = str_replace("-","",$journalized_history_arr[$i]["paid_date"]);
		//echo ",支払日：";
		$CsvDataArr["paid_date"] = $pay_date = $journalized_history_arr[$i]["paid_date"];
		//echo ",借方金額（税込合計）：";
		$CsvDataArr["debit"] = $total_price = $journalized_history_arr[$i]["debit"];
		//echo ",借方消費税：";
		$CsvDataArr["debit_tax"] = $sales_tax = $journalized_history_arr[$i]["debit_tax"] * 1;
		//echo ",借方消費税区分：";
		$CsvDataArr["debit_tax_type"] = $sales_tax_type = $journalized_history_arr[$i]["debit_tax_type"];
		//echo ",借方消費税コード：";
		$CsvDataArr["debit_tax_code"] = $debit_tax_code = $journalized_history_arr[$i]["debit_tax_code"];
		//echo ",貸方勘定科目：";
		$CsvDataArr["credit_account_name"] = $credit_account_name = $journalized_history_arr[$i]["credit_account_name"];
		//echo ",貸方勘定科目：";
		$CsvDataArr["credit_account_code"] = $credit_account_code = $journalized_history_arr[$i]["credit_account_code"];
		//echo ",貸方補助勘定科目：";
		$CsvDataArr["credit_sub_account_name"] = $credit_sub_account_name = $journalized_history_arr[$i]["credit_sub_account_name"];
		//echo ",貸方補助勘定科目コード：";
		$CsvDataArr["credit_sub_account_code"] = $credit_sub_account_code = $journalized_history_arr[$i]["credit_sub_account_code"];
		//echo ",貸方部門：";
		$CsvDataArr["credit_section_name"] = $credit_section_name = $journalized_history_arr[$i]["credit_section_name"];
		//echo ",貸方部門コード：";
		$CsvDataArr["credit_section_code"] = $credit_section_code = $journalized_history_arr[$i]["credit_section_code"];

		//echo ",貸方金額：";
		$CsvDataArr["credit"] = $credit_amount_of_money = $journalized_history_arr[$i]["credit"];
		//echo ",貸方消費税：";
		$CsvDataArr["credit_tax"] = $credit_sales_tax = $journalized_history_arr[$i]["credit_tax"] * 1;
		//echo ",貸方消費税区分：";
		$CsvDataArr["credit_tax_type"] = $credit_sales_tax_type = $journalized_history_arr[$i]["credit_tax_type"];
		//echo ",貸方消費税コード：";
		$CsvDataArr["credit_tax_code"] = $credit_tax_code = $journalized_history_arr[$i]["credit_tax_code"];		
		//echo ",備考：";
		$CsvDataArr["remarks"] = $remarks = $journalized_history_arr[$i]["remarks"];
		//echo ",品名：";
		$CsvDataArr["product_name"] = $product_name = $journalized_history_arr[$i]["product_name"];
		//echo "<br/>\r\n";
		$CsvDataArr["base_money"] = $base_money = $journalized_history_arr[$i]['base_amount_of_money'];
		
		//会計ソフトごとのCSV変換オブジェクトを作成
		$ExASWC_con = new ExAccSoftWareCSV();
		//1弥生 2MJS 3勘定奉行 4財務会計 5MFクラウド
		$asw = $acc_soft_type;//後で企業テーブルに登録された会計ソフト番号から読み出す仕様に変更する。
		$head_flag = 0;
		if ($i == 0) {
			$head_flag = 1;
		}
		$csv_data .= $ExASWC_con->exCsv($asw,$head_flag,$CsvDataArr);
		
		//var_dump($tax_class_arr);
		//税区分の番号をCSVファイルから取得した税区分名称に変換する処理
//		$debit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['debit_flag1']];
//		$credit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['credit_flag1']];
		$debit_sales_tax_status = $sales_tax_type;
		$credit_sales_tax_status = $credit_sales_tax_type;
		$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
		$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);

		$flag1 = $row_count;//行数をDBに格納して渡す予定でテスト用に変数に1を入れていたが、無しでも取得できたのでif文にしなくてもいい。
	//}
	}
//exit;
//var_dump($csv_data);
	//出力ファイル名の作成
	$csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';

	//文字化けを防ぐ
	$csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );

	//MIMEタイプの設定
	header("Content-Type: application/octet-stream");
	//名前を付けて保存のダイアログボックスのファイル名の初期値
	header("Content-Disposition: attachment; filename={$csv_file}");

	// データの出力
	echo($csv_data);
	exit();

}


?>

<?php require("header.php");?>

<script type="text/javascript">



//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
	//alert(pst);alert(did);alert(cid);alert(icd);
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
//	if (num == 1) {
		var data = { get_payment_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
/*	} else if (num == 2) {
		var data = { get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
*/
//		if (num == 1) {
			send_url = "send_pastjournal";
/*		} else if (num == 2) {
			send_url = "send_send_code";
		}
*/
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される
			
			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			alert(data);
/*
			var address_data = data.split(",");
			//alert(document.getElementById('bikou').value);
			for (i = 0 ;i <= 5;i++) {
				//alert(address_data[i]);
				document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
			}
			//alert(address_data);
			document.getElementById('destination_email').value = address_data[7];
			document.getElementById('Kyoutu_No').value = address_data[0];
			document.getElementById('company_info').value = address_data[6];
			document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
			if (num == 1) {
				document.getElementById('addressee_company_info').value = "";
			}
			//onLoadData();
			//document.write(data);
*/
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}
</script>


<script>


$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});


</script>


<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>


<title>
<?php
if ( $_POST['mode'] === 'download' ) {
	echo "総勘定元帳検索 - Cloud Invoice";
} else {
	echo "総勘定元帳 - Cloud Invoice";
}
?>
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article id="jounalized_history">

	<section id="m-1-box">
		<h2>
		<?php
		if ( $_REQUEST['mode'] === 'download') {
			echo "総勘定元帳";
		} else {
			echo "総勘定元帳";
		}
		?>
		</h2>
			<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<div class="hiduke">
					<p>取引日付</p>
					<input type="text" id="box1" name="paydate_from" value="<?php echo $_REQUEST['paydate_from'];?>"/>
					～
					<input type="text" id="box2" name="paydate_to" value="<?php echo $_REQUEST['paydate_to'];?>"/>
				</div>
				<div class="kamoku">
					<p>勘定科目</p>
					<select name="debit_account_name">
					<?php
						for($ii = 0;$ii < count($account_titles_arr);$ii++) {
					?>
						<option value="<?php echo $account_titles_arr[$ii];?>" <?php if ($account_titles_arr[$ii]==$debit_account_name){echo 'selected';};?>><?php echo $account_titles_arr[$ii];?></option>
					<?php
						}
					?>
					</select>
				</div>
				<!--
				<div class="hojo">
					<p>借方補助科目</p>
					<input type="text" name="debit_sub_account_name" value="<?php echo $_REQUEST['debit_sub_account_name'];?>"/>
				</div>
				<div class="bumon">
					<p>借方部門</p>
					<input type="text" name="debit_section_name" value="<?php echo $_REQUEST['debit_section_name'];?>"/>
				</div>
				<div class="kamoku">
					<p>貸方勘定科目</p>
					<input type="text" name="credit_account_name" value="<?php echo $_REQUEST['credit_account_name'];?>"/>
				</div>
				<div class="hojo">
					<p>貸方補助科目</p>
					<input type="text" name="credit_sub_account_name" value="<?php echo $_REQUEST['credit_sub_account_name'];?>"/>
				</div>
				<div class="bumon">
					<p>貸方部門</p>
					<input type="text" name="credit_section_name" value="<?php echo $_REQUEST['credit_section_name'];?>"/>
				</div>
				-->
				<div class="tekiyou">
					<p>摘要</p>
					<input type="text" name="remarks" value="<?php echo $_REQUEST['remarks'];?>"/>
				</div>
				<div class="search">
					<input type="submit" value="検索"/>
					<input type="hidden" name="mode" value="download" />
				</div>
				</form>
			</section>
<?php 
			//var_dump($devision_json);
?>
		<form action="" method="post" name="download" id="journal_form" value="1">
		<?php
		$count_data_num = count($journalized_history_arr);
		
		if ( $count_data_num > 0) {
		//echo '<input type="button" onclick="submit()" value="CSVで出力する" /><br/><br/>';
			$invoice_code_checker = "";
			$claimant_id_checker = "";
		?>
			
			<div class="SearchLine"></div>
			
			<!--<table id="pastjournal">-->
			<table id="pastjournal2">
				<thead>
				<tr>
					<!--<th class="first"><label id="checklabel"><input type="checkbox" /></label></th>-->
					<!--<th class="kamoku">仕訳CD</th>-->
					<th class="min2">振伝No</th>
					<th class="hiduke">取引日付</th>
					<th class="kamoku">勘定科目</th>
					<th class="kamoku">相手科目</th>
					<th class="kingaku">借方金額</th>
					<th class="kingaku">貸方金額</th>
					<th class="long">摘要</th>
					<th class="kingaku last">残高</th>
					<!--<th class="last">編集</th>-->
				</tr>
				</thead>
		<?php 
			$k = 0;//ヘッダーの番号を覚えておく変数
			$balance = 0;
			$dan = 0;
			$can = 0;
			//繰越額
			$balance = $carryforwards;
		?>
		<tr><td></td><td></td><td></td><td></td><td></td><td></td><td style="width:70px; max-width:70px; text-align:left; padding:0 0px 0 3px;">繰越金</td><td class="last" style="width:70px; max-width:70px; text-align:right; padding:0 2px 0 0;"><?php echo number_format($carryforwards);?></td></tr>
		<?php
			for ( $i = 0; $i < intval($count_data_num); $i++ ) {
				//var_dump($invoice_data_arr[$i]);
//				if ($invoice_code_checker != $journalized_history_arr[$i]['invoice_code'] || $claimant_id_checker != $journalized_history_arr[$i]["claimant_id"]) {
					$invoice_code_checker = $journalized_history_arr[$i]['text_data1'];
					$claimant_id_checker = $journalized_history_arr[$i]["company"];
					if ($i > 0) {
						echo '<input type="hidden" id="invoice_total'.$k.'" name="invoice_total'.$k.'" value="'.$invoice_total.'" />';
						$invoice_total = 0;
						$k = $i;
						//echo "
						//	</div>
						//	";
					}
					$withholding_tax = $journalized_history_arr[$i]['withholding_tax'];
					$withholding_tax_flag = $withholding_tax * 1;
					$debit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['debit_flag1']];
					$credit_sales_tax_status = $tax_class_arr[$journalized_history_arr[$i]['credit_flag1']];
					$debit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $debit_sales_tax_status);
					$credit_sales_tax_status = str_replace(array("\r\n", "\r", "\n"), '', $credit_sales_tax_status);
		?>
		<?php
					$dan = $journalized_history_arr[$i]['debit_account_name'];
					$can = $journalized_history_arr[$i]['credit_account_name'];
		?>
				<tr>
					<td class="min2"><?php if ($journalized_history_arr[$i]["serial_num"] != "") {echo $journalized_history_arr[$i]["serial_num"];}?></td>
					<td class="hiduke"><?php echo $journalized_history_arr[$i]["paid_date"];?></td>
					<td class="kamoku">
					<?php echo $debit_account_name;?>
					</td>
					<td class="kamoku">
					<?php
						$debit_account_name;
						if ($dan == $debit_account_name) {
							echo $can;
						} else if ($can == $debit_account_name) {
							echo $dan;
						} else if ($debit_account_name == "") {
							echo "なし";
						}
					?>
					</td>
					<td class="kingaku">
						<?php 
						if ($dan == $debit_account_name) {
							echo number_format($journalized_history_arr[$i]['credit']);
						} else if ($can == $debit_account_name) {
							echo 0;
						}
						?>
					</td>
					<td class="kingaku">
						<?php 
						if ($dan == $debit_account_name) {
							echo 0;
						} else if ($can == $debit_account_name) {
							echo number_format($journalized_history_arr[$i]['debit']);
						}
						?>
					</td>
					<td class="long"><?php echo $journalized_history_arr[$i]['remarks'];?></td>
					<td class="kingaku">
					<?php
						//資産区分によって貸借が変わるのでそのように変更した20171117hmsk
						$dvs = $account_json[$debit_account_name]["devision"];
						$dvs_type = $devision_json[$dvs]["type"];
						if($dvs_type == "資産"||$dvs_type == "費用") {
							if ($dan == $debit_account_name) {
								$balance += $journalized_history_arr[$i]['debit'];
							} else if ($can == $debit_account_name) {
								$balance -= $journalized_history_arr[$i]['credit'];
							}
						} else if($dvs_type == "負債"||$dvs_type == "資本"||$dvs_type == "収益") {
							if ($dan == $debit_account_name) {
								$balance -= $journalized_history_arr[$i]['debit'];
							} else if ($can == $debit_account_name) {
								$balance += $journalized_history_arr[$i]['credit'];
							}
						}
						echo number_format($balance);
					?>
					</td>
				</tr>
				
				<?php
				if (intval($journalized_history_arr[$i]["flag1"]) > 0) {
					for ($m = 1;$m < intval($journalized_history_arr[$i]["flag1"]);$m++) {
				?>
						<tr>
							<!--<td class="min2"></td>-->
							<td class="hiduke"></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_sub_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['debit_section_name'.$m];?></td>
							<td class="kingaku"><?php echo $journalized_history_arr[$i]['debit'.$m];?></td>
							<td class="zei"><?php echo $journalized_history_arr[$i]['debit_tax'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_sub_account_name'.$m];?></td>
							<td class="kamoku"><?php echo $journalized_history_arr[$i]['credit_section_name'.$m];?></td>
							<td class="kingaku"><?php echo $journalized_history_arr[$i]['credit'.$m];?></td>
							<td class="zei"><?php echo $journalized_history_arr[$i]['credit_tax'.$m];?></td>
							<td class="long"><?php echo $journalized_history_arr[$i]['remarks'.$m];?></td>
							
							<td class="min"></td>
							<!--
							<td class="last">
							<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
							<?php
								//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
								$flag = "company_data";
								if ($claimant_id != 0 ) {
									$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
								} else {}
							?>
							

							</td>
							-->
						</tr>

				
				
				<?php
					}
					
					if ($withholding_tax_flag > 0) {
						if ($claimant_id == $company_id) {
							$debit_a_name = '仮払税金';
							$debit_sa_name = '源泉所得税';
						} else {
							$credit_a_name = '預り金';
							$credit_sa_name = '源泉所得税';
						}
				?>
						<tr>
							<td class="min2"></td>
							<td class="hiduke"></td>
							<td class="kamoku"><?php echo $debit_a_name;?></td>
							<td class="kamoku"><?php echo $debit_sa_name;?></td>
							<td class="kamoku"><?php?></td>
							<td class="kingaku"><?php?></td>
							<td class="zei"><?php?></td>
							<td class="kamoku"><?php echo $credit_a_name;?></td>
							<td class="kamoku"><?php echo $credit_sa_name;?></td>
							<td class="kamoku"><?php?></td>
							<td class="kingaku"><?php?></td>
							<td class="zei"><?php?></td>
							<td class="long"><?php?></td>
							<td class="min"></td>
						</tr>
				<?php
					}
				}
				?>
			
			<?php
//				}//ifの終わり
			}//forの終わり
			?>
			</table>
			<br/>
			<input type="button" id="outcsv" onclick="submit()" value="CSVで出力する"/>
			<!--<input type="button" id="sba" onclick="sendByAjax('a','b','c','d')" value="Ajax出力" />-->
			<input type="hidden" name="csv" value="csv">
			<input type="hidden" name="mode" value="download">
			<?php
		}	
			?>
			<input type="hidden" id="data_num" name="data_num" value="<?php echo $count_data_num;?>" />
			<input type="hidden" id="aj_words" name="aj_words" value="<?php echo $aj_words;?>" />
		</form>
	</section>
<?php
	if ($page_num > 1) {
		echo '<div class="pageBtnBox"><a class="pageBtn" href="?page=1&sort_key='.$sort_key.'&sort_type='.$sort_type.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1"><<</a>';
		for($p = 1;$p <= $page_num;$p++) {
			$bc_class = "";//初期化
			if ($page == $p) {$bc_class = " chBackColor";}
			echo '<a class="pageBtn'.$bc_class.'" href="?page='.$p.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1">'.$p.'</a>';
		}
		echo '<a class="pageBtn" href="?page='.$page_num.'&sort_key='.$sort_key.'&sort_type='.$sort_type.'&sort_num='.$sort_num.'&paydate_from='.$payfrom.'&paydate_to='.$payto.'&mode=download&koumoku=1">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
	}
?>


</article>
<div id="fadeLayer" onclick="CloseFrame(12)"></div>
<div id="noneLayer"></div>




<?php require("footer.php");?>
