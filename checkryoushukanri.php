<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//変数の宣言
$company_id;
$words = "";
$flag = "";
$sql = "";
$ryoushu_num = $_POST['ryoushukanri_num'];
$total_payment = 0;
$sum_payments = 0;
$payment_detail = "";
$param = $_SERVER['QUERY_STRING'];
//echo $param2 = $_SERVER['REQUEST_URI'];
//var_dump($_POST);

//ユーザーテーブルのコントローラーを呼び出す
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$maindb_con = new db_control();


$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}



//各テーブルからデータを取得する

//請求書のステータス変更
/*
if($_REQUEST['status_chg'] == 1) {
	$change_download_password = htmlspecialchars($_REQUEST['download_password'],ENT_QUOTES);
	$change_status = htmlspecialchars($_REQUEST['select_status'],ENT_QUOTES);
	$flag_change_status = "up_invoice_download_password";
	$words_change_status = " `status` = ".$change_status." WHERE `destination_id` = ".$company_id." AND `download_password` = '".$change_download_password."' "; 
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag_change_status,$company_id,$words_change_status);
}
*/

if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
//請求者
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND claimant_id = ".$destination_id;
}
//仕訳済み判定
if ($_REQUEST['no_journalized'] == "" && $_REQUEST['journalized'] == "") {
	$words .= " AND (`journalized` = 0 OR `journalized` = 1) ";
} else if ($_REQUEST['no_journalized'] == 1 && $_REQUEST['journalized'] == 1) {
	$words .= " AND (`journalized` = 0 OR `journalized` = 1) ";
} else if ($_REQUEST['no_journalized'] == 1 && $_REQUEST['journalized'] == 0) {
	$words .= " AND `journalized` = 0 ";
} else if ($_REQUEST['no_journalized'] == 0 && $_REQUEST['journalized'] == 1) {
	$words .= " AND `journalized` = 1 ";
} else if ($_REQUEST['no_journalized'] == 0 && $_REQUEST['journalized'] == 0) {
	$words .= " AND `journalized` < 0 ";
}
//paper_typeは0:請求書1:領収書2:人件費3:非表示
$words .= " AND `paper_type` = 1 ";
$words .= " AND `paper_type` <> 3 ";


if ($_REQUEST['billing_date_from'] != NULL) {
	$billing_date_from = htmlspecialchars($_REQUEST['billing_date_from'],ENT_QUOTES);
	$words .= " AND billing_date >= ".$billing_date_from;
}
if ($_REQUEST['billing_date_to'] != NULL) {
	$billing_date_to = htmlspecialchars($_REQUEST['billing_date_to'],ENT_QUOTES);
	$words .= " AND billing_date <= ".$billing_date_to;
}

//支払日
if ($_REQUEST['pay_date_from'] != NULL) {
	$pay_date_from = htmlspecialchars($_REQUEST['pay_date_from'],ENT_QUOTES);
	$words .= " AND pay_date >= ".$pay_date_from;
	$conditions_words .= "支払日:".$pay_date_from."～";
}
if ($_REQUEST['pay_date_to'] != NULL) {
	$pay_date_to = htmlspecialchars($_REQUEST['pay_date_to'],ENT_QUOTES);
	$words .= " AND pay_date <= ".$pay_date_to;
	$conditions_words .= "～ 支払日:".$pay_date_to." ";
}
//支払額
if ($_REQUEST['pay_amount_from'] != NULL) {
	$pay_amount_from = htmlspecialchars($_REQUEST['pay_amount_from'],ENT_QUOTES);
	$words .= " AND `total_price` >= ".$pay_amount_from;
	$conditions_words .= "支払額:".$pay_amount_from."～";
}
if ($_REQUEST['pay_amount_to'] != NULL) {
	$pay_amount_to = htmlspecialchars($_REQUEST['pay_amount_to'],ENT_QUOTES);
	$words .= " AND `total_price` <= ".$pay_amount_to;
	$conditions_words .= "～支払額:".$pay_amount_to." ";
}
//商品名・備考
if ($_REQUEST['product_name'] != NULL) {
	$product_name = htmlspecialchars($_REQUEST['product_name'],ENT_QUOTES);
	$words .= " AND `product_name` LIKE '%".$product_name."%'";
	$conditions_words .= "商品名:".$product_name." ";
}
//請求書番号
if ($_REQUEST['invoice_code'] != NULL) {
	$invoice_code = htmlspecialchars($_REQUEST['invoice_code'],ENT_QUOTES);
	$words .= " AND `invoice_code` LIKE '%".$invoice_code."%'";
	$conditions_words .= "請求書番号:".$invoice_code." ";
}
//会社名
if ($_REQUEST['invoice_company_name'] != NULL) {
	$invoice_company_name = htmlspecialchars($_REQUEST['invoice_company_name'],ENT_QUOTES);
	//企業データ取得
	$comp_flag = "company_data_from_like_name";
	$invoice_company_name_arr = $company_con -> company_sql_flag($pdo,$comp_flag,$invoice_company_name,$comp_words);
	//var_dump($invoice_company_name_arr);
	$invoice_claimant_id = $invoice_company_name_arr[0]["company_id"];
	$words .= " AND `claimant_id` = '".$invoice_claimant_id."'";
	$conditions_words .= "会社名:".$invoice_company_name." ";
}

	//▼ページ関連処理▼
	//ページ条件なしでデータ数を取得
	//ダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down";
	$words;//条件をここに入れる。
	$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 2) ";
	$data_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	
	//表示数の制御
	$max_disp = 100;
	$sort_key = "`update_date`";
	$sort_type = "DESC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 2) ";
	$sum_words = $words;//ページ合計金額取得用
	//$words .= " ORDER BY `id` LIMIT 100";
	$words .= " GROUP BY `invoice_code` ORDER BY ";
	$words .= $conditions;
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	
	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down_check";
	$words;//条件をここに入れる。
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	$invoice_data_receive_num = count($invoice_data_receive_arr);
/*
	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_receive_narrow_down_check";
	$words;//条件をここに入れる。
	$sum_words .= "GROUP BY `destination_id` ORDER BY ".$conditions;
	$sum_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$sum_words);
	//var_dump($sum_arr);
	$sum_num = count($sum_arr);
*/
	
	//▲ページ関連処理▲


//自社企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_self_name = $company_self_arr[0]['company_name'];
	$company_self_payment_id = $company_self_arr[0]['payment_id'];
	$company_self_plan_status = $company_self_arr[0]['plan_status'];
	$company_self_email = $company_self_arr[0]['email'];
}

?>

<title>領収書・レシート管理 - Cloud Invoice</title>
<script type="text/javascript" src="../js/table-sorter-head.js"></script>
<!--▼リンク先サムネイル化▼-->
<!-- Jquery本体ホスティング -->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
<!-- /Jquery本体ホスティング -->
<!-- thumbnail.js -->
<!--
<script type="text/javascript" src="../js/thumbnail.js"></script>
-->
<!-- /thumbnail.js -->
<!--▲リンク先サムネイル化▲-->

<script language="JavaScript" type="text/javascript">
<!--
function allCheck() {
	var check =  document.getElementById("ryoushukanriA").checked;
	//alert(check);
		for (var i = 0; i < parseInt(document.getElementById("ryoushukanri_num").value); i++){
			try {
				var ryoushu = "ryoushukanri" + i;
				//document.getElementById("ryoushukanriA").checked = check_flag;
	   		 	document.getElementById(ryoushu).checked = check;
			}catch(e){}
		}
}

function OpenTransferFrame(){
	window.scrollTo(0,0);
	document.getElementById("Transfer").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	
}

//-->
</script>
<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>
<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#pay").colResizable({fixed:false});
		$("#pay").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>

<iframe id="Transfer" src="./frame/transfer"></iframe>
<iframe id="Transfer2" src="./frame/transfer2"></iframe>


<article>



	<section id="m-1-box">
		<h2>
			領収書・レシート管理
		</h2>
		<section id="search">
		<form action="" method="GET" id="cond_form" name="cond_form">
			<div class="hiduke">
				<p>支払期限</p>
				<input type="text" id="box1" name="pay_date_from" />
				～
				<input type="text" id="box2" name="pay_date_to" />
			</div>
			<div class="kamoku">
				<p>会社名</p>
				<input type="text" name="invoice_company_name" />
			</div>
			<div class="hojo">
				<p>請求書番号</p>
				<input type="text" name="invoice_code" />
			</div>
			<div class="bumon">
				<p>支払額</p>
				<input type="text" name="pay_amount_from" />
				～
				<input type="text" id="box2" name="pay_amount_to" />
			</div>
			<div class="hojo">
				<p>商品名</p>
				<input type="text" name="product_name" />
			</div>
			<div class="search">
				<input type="button" onclick="submit();" value="検索"/>
				<input type="hidden" name="mode" value="download" />
			</div>

			</form>
		</section>
		<?php if ($conditions_words != "") {echo '<p>【絞り込み条件】'.$conditions_words.'</p>';}?>
		<p>合計金額: <span id="sum_payments_top"></span> 円</p>
		<form action="" method="post" name="payment_form" id="payment_form">
			<table id="pay" class="tables tablesorter dragging" style="width:600px;">
				<colgroup>
					<col align="center" width="20px">
					<col align="center" width="140px">
					<col align="center" width="100px">
					<col align="center" width="140px">
					<col align="center" width="40px">
					<col align="center" width="150px">
					<col align="center" width="150px">
					<col align="center" width="10px">
					<col align="center" width="10px">
				</colgroup>
				<thead  style="text-align:center;">
			<?php if($invoice_data_receive_num > 0) { ?>
				<tr>
					<th id="first"><label id="checklabel"><input type="checkbox" id="ryoushukanriA" name="ryoushukanriA" onClick="allCheck();" /></label><input type="hidden" id="ryoushukanri_num" name="ryoushukanri_num" value="<?php echo $invoice_data_receive_num;?>"></th>
					<th>会社名</th>
					<th>更新日</th>
					<th>請求書番号</th>
					<th class="{sorter:'metadata'}">支払額</th>
					<th>摘要</th>
					<th>支払日</th>
					<th>PDF</th>
					<th class="{sorter:'metadata'}">ステータス</th>
				</tr>
				</thead>
				<tbody style="text-align:center;">
			<?php
				} else{
					echo "<p>この条件ではデータが見つかりません</p>";
				}
			?>
			<?php 
				for ($i = 0; $i < $invoice_data_receive_num; $i++) {
			?>
				<tr>
				<?php 
					$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
					if ($claimant_id != "") {
						$flag = "company_data";
						$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
					}
				?>
					<td id="check">
				<?php
					if ($invoice_data_receive_arr[$i]["status"] != 8 ) {
				?>
						<label id="checklabel"><input id="ryoushukanri<?php echo $i;?>" type="checkbox" name="ryoushukanri<?php echo $i;?>" value="1" /></label>
				<?php
					}
				?>
						</td>
					<td id="kaisha"><a href="./analysis_customer?cl=<?php echo $invoice_data_receive_arr[$i]['claimant_id'];?>" title="取引先データ"><?php echo $company_arr[0]["company_name"];?><input type="hidden" name="p_claimant_id<?php echo $i;?>" value="<?php echo $claimant_id;?>"></a></td>
					<td id="koushin"><?php echo date("Y-m-d", strtotime($invoice_data_receive_arr[$i]["update_date"]));?></td>
					<td id="seikyuNo"><?php echo $invoice_data_receive_arr[$i]["invoice_code"];?><input type="hidden" name="p_invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_code'];?>"></td>
					<td id="kingaku" class="{sortValue: <?php echo $invoice_data_receive_arr[$i]['sum(total_price)'];?>}"><?php echo number_format($invoice_data_receive_arr[$i]["sum(total_price)"]);?><input type="hidden" name="p_total_preice<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['sum(total_price)'];?>"></td>
					<td id="tekiyou"><?php echo $invoice_data_receive_arr[$i]["product_name"];?></td>
					<td id="ryoushu"><?php $str = $invoice_data_receive_arr[$i]["pay_date"];if ($str != 0) {echo date('Y/m/d',strtotime($str));} ?><input type="hidden" name="p_pay_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['pay_date'];?>"></td>
					<td id="hakkou">
					<?php

						$flag3 = "download_invoice_data";
						$words3 = $invoice_data_receive_arr[$i]['download_password'];//条件をここに入れる。
						$invoice_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag3,$company_id,$words3);
						//var_dump($invoice_num_arr);
						$invoice_num = count($invoice_num_arr);
					?>
						<!--<?php echo "<a href='./invoice2pdf?clm=".$claimant_id."&c1=".$company_id."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=1' target='_blank'>請求書</a>";?>-->
					<?php 
						if ($invoice_data_receive_arr[$i]["status"] < 6) { $status_param = "&downloaded=6";}
							$pdf_url = "../files/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf";
							//if (!file_exists($pdf_url)) {
								echo "<a class='thumbxx' href=\"./invoice2pdf?dlp=".$invoice_data_receive_arr[$i]['download_password']."&dlps=1&clm=".$claimant_id."&det=".$invoice_num.$status_param."\" target=\"_blank\">PDF</a>";
							//} else {
								//echo "<a class='thumb' href=\"https://storage.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf\" target=\"new\">請求書</a>";
								//echo "<a class='thumbxx' href='javascript:subWin(\"https://storage.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf\")"."' target='new'>請求書</a>";
							//	echo "<a class='thumbxx' href='https://storage.cloudinvoice.co.jp/files/".$claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]["download_password"].".pdf' target='new'>請求書</a>";
							//}
					?>
							<!--
							<a href="javascript:subWin('//storage.cloudinvoice.co.jp/files/<?php echo $claimant_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]["download_password"].".pdf";?>');" style='color:red;' target='new'>請求書</a>
							-->
					
					</td>
					<td class="{sortValue: <?php echo $invoice_data_receive_arr[$i]['status'];?>}">
					<?php
						//echo "<form action='' method='post'>";
						if ($invoice_data_receive_arr[$i]["status"] == 6) {
							echo "<span style='color:#004B91;'>受領済</span>";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6" selected>受領済み</OPTION>
								<!--<OPTION value="7">保留</OPTION>-->
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_data_receive_arr[$i]["status"] == 7) {
							echo "保留";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6">受領済み</OPTION>
								<!--<OPTION value="7" selected>保留</OPTION>-->
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_data_receive_arr[$i]["status"] == 8) {
							echo "<span style='color:black;'>支払処理中</span>";
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6">受領済み</OPTION>
								<!--<OPTION value="7">保留</OPTION>-->
								<OPTION value="8" selected>処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'"/>';
							*/
						} else if ($invoice_data_receive_arr[$i]["status"] == 9) {
							echo "<span style='color:black;'>支払済</span>";
						} else {
							echo "<span style='color:red;'>未確認</span>";
							//echo $invoice_data_receive_arr[$i]["status"];
							/*
							echo '
							<SELECT name="select_status">
								<OPTION value="6" selected>受領済み</OPTION>
								<OPTION value="7">保留</OPTION>
								<OPTION value="8">処理済</OPTION>
							</SELECT>';
							echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
							*/
						}
						/*
						echo '<input type="hidden" name="status_chg" value="1" />';
						if ($invoice_data_receive_arr[$i]["status"] >= 6 && $invoice_data_receive_arr[$i]["status"] != 11) { echo '<input type="button" onclick="submit();" value="変更" style="margin:0 0 0 5px" />';}
						*/
						//echo '</form>';
					?>
						<input type="hidden" name="claimant_id<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["claimant_id"];?>' />
						<input type="hidden" name="destination_id<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["destination_id"];?>' />
						<input type="hidden" name="send_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["send_code"];?>' />
						<input type="hidden" name="billing_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["billing_date"];?>' />
						<input type="hidden" name="invoice_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["invoice_code"];?>' />
						<input type="hidden" name="invoice_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["invoice_name"];?>' />
						<input type="hidden" name="staff_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["staff_name"];?>' />
						<input type="hidden" name="pay_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["pay_date"];?>' />
						<input type="hidden" name="product_code<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["product_code"];?>' />
						<input type="hidden" name="product_name<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["product_name"];?>' />
						<input type="hidden" name="unit_price<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["unit_price"];?>' />
						<input type="hidden" name="quantity<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["quantity"];?>' />
						<input type="hidden" name="unit<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["unit"];?>' />
						<input type="hidden" name="sales_tax<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["sales_tax"];?>' />
						<input type="hidden" name="withholding_tax<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["withholding_tax"];?>' />
						<input type="hidden" name="total_price<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["sum(total_price)"];?>' />
						<input type="hidden" name="remarks1<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks1"];?>' />
						<input type="hidden" name="remarks2<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks2"];?>' />
						<input type="hidden" name="remarks3<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["remarks3"];?>' />
						<input type="hidden" name="status<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["status"];?>' />
						<input type="hidden" name="download_password<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["download_password"];?>' />
						<input type="hidden" name="update_date<?php echo $i;?>" value='<?php echo $invoice_data_receive_arr[$i]["update_date"];?>' />
						<?php
						//ページ内の支払合計金額を算出する処理
						$sum_payments += $invoice_data_receive_arr[$i]['sum(total_price)'];
						if ( $i == ($invoice_data_receive_num - 1)) {
							echo '<input type="hidden" name="sum_payments" id="sum_payments" value="'.number_format($sum_payments).'" />';
							echo '<script type="text/javascript">document.getElementById("sum_payments_top").innerHTML = document.getElementById("sum_payments").value;</script>';
						}
						?>
					</td>
				</tr>
			<?php
				}
			?>
			</tbody>
			</table>
		</form>
	</section>


<?php
	if ($page_num > 1) {
		echo '<div class="pageBtnBox"><a class="pageBtn" href="?'.$param.'&page=1"><<</a>';
		for($p = 1;$p <= $page_num;$p++) {
			$bc_class = "";//初期化
			if ($page == $p) {$bc_class = " chBackColor";}
			echo '<a class="pageBtn'.$bc_class.'" href="?'.$param.'&page='.$p.'">'.$p.'</a>';
		}
		echo '<a class="pageBtn" href="?'.$param.'&page='.$page_num.'">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
	}
?>




</article>
<div id="fadeLayer" onclick="CloseFrame(12)"></div>
<div id="noneLayer"></div>




<?php require("footer.php");?>
