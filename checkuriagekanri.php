<?php require("header.php");?>


<title>売上管理確認 - Cloud Invoice</title>


<?php
//クラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//決済処理用読み込み
require_once(dirname(__FILE__).'/../payment/payjp/vendor/autoload.php');
require_once(dirname(__FILE__).'/../payment/config.php');

// Payjpインスタンスを非公開
\Payjp\Payjp::setApiKey(SECRET_KEY);

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$send_count = 0;//送信数の初期化
$status_num = 0;//ステータス変更用の変数初期化
$mailto = "";
$company_email = "";
$stts = "";
$payment_stts = "";
$mail_send_count_num = 0;
$return_url = "//storage.cloudinvoice.co.jp/invoice/credit";
$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];//現在のURLパラメーターつき
$param = $_SERVER['QUERY_STRING'];
//表示件数をセッションで保持する
if(isset($_REQUEST['disp'])){
	if ($_REQUEST['disp'] <= 100) {
		$_SESSION['disp'] = $_REQUEST['disp'];
	} else {
		$_SESSION['disp'] =100;
	}
}
/**
 * ランダム文字列生成 (英数字)
 * $length: 生成する文字数
 */
function makeRandStr($length) {
    $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
    $r_str = null;
    for ($i = 0; $i < $length; $i++) {
        $r_str .= $str[rand(0, count($str))];
    }
    return $r_str;
}


$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}


//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
//処理タイプ
//var_dump($_REQUEST);
if ($_REQUEST['send_type'] != NULL) {
	$send_type = htmlspecialchars($_REQUEST['send_type'],ENT_QUOTES);
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND destination_id = ".$destination_id;
}
//送付先コードで分ける
if ($_REQUEST['send_code'] != NULL) {
	$send_code = htmlspecialchars($_REQUEST['send_code'],ENT_QUOTES);
	$words .= " AND send_code = '".$send_code."' ";
}

//請求書ステータス
$words .= " AND `status` <> 99 ";
if ($_REQUEST['status'] == 0 && $_REQUEST['status_1'] == 0 && $_REQUEST['status_2'] == 0 && $_REQUEST['status_3'] == 0) {
	$_REQUEST['status'] = 1;$_REQUEST['status_1'] = 1;$_REQUEST['status_2'] = 1;$_REQUEST['status_3'] = 1;
}
if ($_REQUEST['status'] == 1 ||$_REQUEST['status_1'] == 1 ||$_REQUEST['status_2'] == 1 ||$_REQUEST['status_3'] == 1) {
	$words .= " AND (";
	if ($_REQUEST['status'] == 1) {
		$stts .= "`status` = 0 ";
	}
	if ($_REQUEST['status_1'] == 1) {
		if ($_REQUEST['status'] == 1) {
			$stts .= " OR ";
		}
		$stts .= " (`status` >= 1 AND `status` <= 5) ";
	}
	if ($_REQUEST['status_2'] == 1) {
		if ($_REQUEST['status'] == 1 ||$_REQUEST['status_1'] == 1) {
			$stts .= " OR ";
		}
		$stts .= "(`status` >= 6 AND `status` <= 8) ";
	}
	if ($_REQUEST['status_3'] == 1) {
		if ($_REQUEST['status'] == 1 ||$_REQUEST['status_1'] == 1 ||$_REQUEST['status_2'] == 1) {
			$stts .= " OR ";
		}
		$stts .= "`status` = 9 ";
	}
	$words .= $stts.") ";
}
//入金処理のステータス
if ($_REQUEST['paid'] == 0 && $_REQUEST['unpaid'] == 0 && $_REQUEST['notyet'] == 0){
	$_REQUEST['paid'] = 1;$_REQUEST['unpaid'] = 1;$_REQUEST['notyet'] = 1;
}
if ($_REQUEST['paid'] == 1 ||$_REQUEST['unpaid'] == 1 ||$_REQUEST['notyet'] == 1) {
	$words .= " AND (";
	if ($_REQUEST['paid'] == 1) {
		$payment_stts .= "`payment_status` = 1 ";
	}
	if ($_REQUEST['unpaid'] == 1) {
		if ($_REQUEST['paid'] == 1) {
			$payment_stts .= " OR ";
		}
		$payment_stts .= " `payment_status` = 2 ";
	}
	if ($_REQUEST['notyet'] == 1) {
		if ($_REQUEST['paid'] == 1 ||$_REQUEST['unpaid'] == 1) {
			$payment_stts .= " OR ";
		}
		$payment_stts .= " `payment_status` = 0 ";
	}
	$words .= $payment_stts.") ";
}

if ($_REQUEST['insert_date_from'] != NULL) {
	//$_REQUEST['insert_date_from'] = str_replace(array("/"),"",$_REQUEST['insert_date_from']);
	$insert_date_from = str_replace(" ","",substr($_REQUEST['insert_date_from'],0,4)."/".substr($_REQUEST['insert_date_from'],4,2)."/".substr($_REQUEST['insert_date_from'],6,2));
	$words .= " AND `insert_date` >= ".$insert_date_from;
}
if ($_REQUEST['insert_date_to'] != NULL) {
	//$_REQUEST['insert_date_to'] = str_replace(array("/"),"",$_REQUEST['insert_date_to']);
	$insert_date_to = str_replace(" ","",substr($_REQUEST['insert_date_to'],0,4)."/".substr($_REQUEST['insert_date_to'],4,2)."/".substr($_REQUEST['insert_date_to'],6,2));
	$words .= " AND `insert_date` <= ".$insert_date_to;
}
if ($_REQUEST['billing_date_from'] != NULL) {
	//$_REQUEST['billing_date_from'] = str_replace(array("/"),"",$_REQUEST['billing_date_from']);
	$billing_date_from = htmlspecialchars($_REQUEST['billing_date_from'],ENT_QUOTES);
	$words .= " AND `billing_date` >= ".$billing_date_from;
}
if ($_REQUEST['billing_date_to'] != NULL) {
	//$_REQUEST['billing_date_to'] = str_replace(array("/"),"",$_REQUEST['billing_date_to']);
	$billing_date_to = htmlspecialchars($_REQUEST['billing_date_to'],ENT_QUOTES);
	$words .= " AND `billing_date` <= ".$billing_date_to;
}
if ($_REQUEST['pay_date_from'] != NULL) {
	//echo $_REQUEST['pay_date_from'] = str_replace(array("/"),"",$_REQUEST['pay_date_from']);
	$pay_date_from = htmlspecialchars($_REQUEST['pay_date_from'],ENT_QUOTES);
	$words .= " AND `pay_date` >= ".$pay_date_from;
}
if ($_REQUEST['pay_date_to'] != NULL) {
	//echo $_REQUEST['pay_date_to'] = str_replace(array("/"),"",$_REQUEST['pay_date_to']);
	$pay_date_to = htmlspecialchars($_REQUEST['pay_date_to'],ENT_QUOTES);
	$words .= " AND `pay_date` <= ".$pay_date_to;
}
///キーワード検索
if ($_REQUEST['tags'] != NULL) {
	$tags = htmlspecialchars($_REQUEST['tags'],ENT_QUOTES);
	$tags_arr = explode ( " " , $tags );
	$tag_num = count($tags_arr);
	for ($i = 0;$i < $tag_num;$i++) {
		if ($i == 0) {
			$words .= " AND ( `tags` LIKE '%".$tags_arr[$i]."%' ";
		} else {
			$words .= " OR `tags` LIKE '%".$tags_arr[$i]."%' ";
		}
		if ($i == $tag_num - 1) {
			$words .= ")";
		}
	}
	$conditions_words .= " keyword:".$tags;
}

//会社名
if ($_REQUEST['invoice_company_name'] != NULL) {
	//echo "cn<br/>";
	$invoice_company_name = htmlspecialchars($_REQUEST['invoice_company_name'],ENT_QUOTES);
	//企業データ取得
	//destination_idの検索
	$comp_flag = "company_data_from_like_name";
	$invoice_company_name_arr = $company_con -> company_sql_flag($pdo,$comp_flag,$invoice_company_name,$comp_words);
	//var_dump($invoice_company_name_arr);
	$cname_count = count($invoice_company_name_arr);
	//var_dump($invoice_company_name_arr);
	//echo "<br/>";
	if ($cname_count >= 1) {
		$words .= " AND (";
		for ($i = 0;$i < $cname_count;$i++) {
			$invoice_claimant_id = $invoice_company_name_arr[$i]["company_id"];
			if ($i == 0) {
				$words .= " `destination_id` = '".$invoice_claimant_id."' ";
			} else {
				$words .= " OR `destination_id` = '".$invoice_claimant_id."' ";
			}
		}
	}
	//send_codeの検索
	//echo "send_code<br/>\r\n";
	$comp_flag = "addressee_data_from_any_conditions";
	$addressee_conditions = " `company_id` = '".$company_id."' AND `company_name` LIKE '%".$invoice_company_name."%' ";
	$invoice_company_name_sc_arr = $company_con -> company_sql_flag($pdo,$comp_flag,$addressee_conditions,$cond2,$cond3);
	//var_dump($invoice_company_name_sc_arr);
	$cname_sc_count = count($invoice_company_name_sc_arr);
	if ($cname_sc_count >= 1) {
		if ($cname_count == 0) {
			$words .= " AND (";
		}
		for ($i = 0;$i < $cname_sc_count;$i++) {
			$invoice_claimant_id = $invoice_company_name_sc_arr[$i]["client_id"];
			if ($cname_count == 0 && $i == 0) {
				$words .= " `send_code` = '".$invoice_claimant_id."' ";
			} else {
				$words .= " OR `send_code` = '".$invoice_claimant_id."' ";
			}
		}
		
	}
	if ($cname_sc_count >= 1 || $cname_count >= 1) {
		$words .= ") ";
	}
	$conditions_words .= "会社名:".$invoice_company_name." ";
}


if ($_REQUEST['send_reserve_date'] != NULL) {
	$insert_sentence = $send_reserve_date = htmlspecialchars($_REQUEST['send_reserve_date'],ENT_QUOTES);
	$insert_sentence .= ":郵送指定日";
}
	//書類タイプ3を非表示にする。2は人件費。
	$words .= " AND `paper_type` <> 3 ";

	//▼ページ関連処理▼
	//ページ条件なしでデータ数を取得
	$flag = "invoice_total_data_send";
	$company_id;
	$words;
	$data_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	
	//表示数の制御
	if ($_SESSION['disp'] > 0) {
		$max_disp = $_SESSION['disp'];
	} else {
		$max_disp = 30;
	}
	$sort_key = " `update_date`";
	$sort_type = "DESC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	//$search_conditionsは明細数取得用なのでGROUP BY `invoice_code`が
	//いらないためここで条件を分割してしてあります。へんなコードですみません。20150313ハマサキ
	
	//######とりあえず記帳代行データエントリー作業で入ったデータはdata_entry_flagが売上:1支払:2なので、
	//######デフォルト:0と売上:1のみを表示する。（ただし、data_entry_flagに強制で2が入っているものがあるため一旦2も入れてある･･･下側）
	//######後で0と1のみのものに変更する。
	//$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 1)";
	$words .= " AND (`data_entry_flag` = 0 OR `data_entry_flag` = 1)";
	$search_conditions = $words." ORDER BY ";
	$words .= " GROUP BY `invoice_code` ORDER BY ";
	$words .= $conditions;
	$search_conditions .= $conditions; 
	
	//INVOICE_DATA_TABLEからCSVにするデータを取得

	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_send_narrow_down2";
	$words;//条件をここに入れる。
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	$invoice_data_receive_num = count($invoice_data_receive_arr);
	//var_dump($invoice_data_receive_arr);
	$flag = "";//フラグの初期化

/*
	//条件つきでダウンロード済みの請求書取得
	$flag="invoice_total_data_send_narrow_down2";
	$words;//条件をここに入れる。
	$invoice_data_receive_arr2 = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	$invoice_data_receive_num2 = count($invoice_data_receive_arr2);
	//var_dump($invoice_data_receive_arr2[0]);
	$flag = "";//フラグの初期化
	//▲ページ関連処理▲
*/

/***ページ処理をしたので全体を取得する箇所は消す。
$flag="invoice_total_data_send";
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
***/
$flag = "";//フラグの初期化


//▼情報メール送信関連処理▼
//自社企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_self_name = $company_self_arr[0]['company_name'];
	$company_self_payment_id = $company_self_arr[0]['payment_id'];
	$company_self_plan_status = $company_self_arr[0]['plan_status'];
}

//var_dump($_REQUEST);
//発行依頼を受けた請求データを変数に入れてダウンロードパスワードを生成し、
//DBのINVOICE_DATA_TABLEに登録する。

//削除の場合は確認のダイアログを出す。//上田さん指示で出さないに変更20141208濱崎
if ($send_type == 7) {
	//echo "<script type='text/javascript'>if (confirm('本当に削除しますか？')==true){}</script>";
}
//if ($send_type != 6 && $send_type != 7 && $send_type != NULL) {
	//echo "<script type='text/javascript'>alert('情報を送信します');</script>";
//}



$row_num = $_REQUEST['uriagekanri_num'];//echo "<br/>";
//改良の余地あり。送信されるデータの数をチェックする
for ($k = 0; $k < $row_num;$k++) {
	//echo "rownum".$k."<br/>";
	if ($_REQUEST['uriagekanri'.$k] == 1) {
		$send_count++;
		//echo "urikan".$k."<br/>";
	}
}
//実際に送信するところ。
for ($k = 0; $k < $row_num;$k++) {
	if ($_REQUEST['uriagekanri'.$k] == 1) {
		$send_num = $k;
		$invoice_data_id = $_REQUEST['invoice_data_id'.$k];
		$destination_id = $_REQUEST['destination_id'.$k];
		$send_code = $_REQUEST['send_code'.$k];
		$company_name = $_REQUEST['company_name'.$k];
		$billing_date = $_REQUEST['billing_date'.$k];
		$invoice_code = $_REQUEST['invoice_code'.$k];
		$invoice_name = $_REQUEST['invoice_name'.$k];
		$pay_date = $_REQUEST['pay_date'.$k];
		$detailed_statement = $_REQUEST['detailed_statement'.$k];
		$status_num = $data_status = $_REQUEST['status'.$k];
		$timestamp_for_code = date("YmdHis");//ダウンロードパスワードを生成するのに使用したが、毎回変わるのでメールと郵送など送るごとに変わってしまうため一旦はずした。
		$dl_pass = $_REQUEST['dl_pass'.$k];
		$mail_cc = $_REQUEST['mail_cc'.$k];
		if ($dl_pass=="" || $dl_pass== NULL){
			$str_length = 20;
			$download_password = md5($company_id."-".$destination_id."-".$send_code."-".$invoice_code."-".$timestamp_for_code.makeRandStr($str_length));
		} else {
			$download_password = $dl_pass;
		}
		$flag = "up_invoice_download_password";
		$words = " `download_password` = '".$download_password."' WHERE (`destination_id` = ".$destination_id." OR `send_code` = '".$send_code."') AND `claimant_id` = '".$company_id."' AND billing_date = ".$billing_date." AND invoice_code = '".$invoice_code."' AND pay_date = ".$pay_date." AND `status` <> 99 ";
		$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
		
		//変数の初期化
		$company_email = "";
		$mailto = "";
		$charge_flag = "";
		$status_num = 0;
		$company_arr1 = array();
		$company_arr2 = array();
		
		//送付先企業データの取得1▼
		if ($destination_id != "" && $destination_id != 0) {
			//echo "send_code なし<br/>";
			$flag = "company_data";
			$conditions = " ";
			$company_arr1 = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words,$conditions);
			//var_dump($company_arr1);
			//echo $company_arr1[0]["email"];
			$company_email = $company_arr1[0]["email"];
			$company_name = $company_arr1[0]["company_name"];
		}
		//echo $send_code."センド<br/>";
		//送付先企業データの取得2▼
		if ($send_code != "" || $send_code != NULL) {
			//echo "send_code あり<br/>";
			$flag = "addressee_data_one";
			$conditions = " AND company_id = ".$company_id." ";
			$company_send_code_check_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
			//var_dump($company_send_code_check_arr);
			if ($company_send_code_check_arr[0]["email"] != NULL && $company_send_code_check_arr[0]["email"] != ""){
				$company_arr2 = $company_con -> company_sql_flag($pdo,$flag,$send_code,$conditions);
				if ($company_email != $company_arr2[0]["email"]) {
					if ($company_email != "") {
						$company_email .= ",";
					}
					if ($company_email != $company_arr2[0]["email"]) {
						$company_email .= $company_arr2[0]["email"];
					}
				}
				$company_name = $company_arr2[0]["company_name"];
			}
			//var_dump($company_arr2);
			//echo $company_arr2[0]["email"];
		}


		$flag = "invoice_total_data_send_count";
		$words = "'".$invoice_data_receive_arr[$i]['invoice_code']."' ORDER BY `insert_date` ASC ";//条件
		$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
		//echo "<br/><br/>test<br/><br/>";
		//送付先企業データの取得▲

		//メールアドレス
		//前回の送信内容を初期化する。
		$mailto = $company_email;
		//echo $mailto." :mailto<br>";
		//メールに渡す情報
		$download_password;
		$keywords = crypt(md5($mailto.$timestamp_for_code.$download_password));
		$mail_cc_arr = explode(",",$mail_cc);
		$mail_cc_num = count($mail_cc_arr);
		$mail_cc_add = "";
		for ($m = 0;$m < $mail_cc_num;$m++) {
			$mail_cc_add .= "\r\nCc:<".$mail_cc_arr[$m].">";
		}
		if ($mail_cc_add != "") {
			
			$headers = "From: CloudInvoice <info@co.cloudinvoice.co.jp>".$mail_cc_add."\r\n";
			$o_headers = "From: CloudInvoice 運営 <info@co.cloudinvoice.co.jp>\r\n";
		} else {
			$o_headers = $headers = "From: CloudInvoice 運営 <info@co.cloudinvoice.co.jp>\r\n";
		}
		
		//メールアドレス
		
$message = "".$company_name."様


".$company_self_name."様から請求書が届いております。

下記のURLにアクセスし、請求書PDFのダウンロードをお願いします。


https://storage.cloudinvoice.co.jp/invoice/pdf_download.php?&key=".$keywords."&clm=".$company_id."&det=".$detailed_statement." 
ダウンロードパスワードは、「".$download_password."」です。

".$insert_sentence.
"

------------------------------
Cloud Invoice（クラウドインボイス）は、無料で使える請求書発行・情報管理サービスです。

CSVデータの取り込みによってクラウド上で、売上請求書の自動作成、
売上請求書のメール（有料で郵送も）、売上請求書の管理、支払請求書の受取、
支払請求書の管理ができるクラウド請求書発行・請求書情報管理サービスです。

クラウド上での請求書の発行・受け取り、請求データの相互ダウンロードなどで、
煩わしい経理業務の手間を軽減できます。

登録・通常機能は完全無料です。
お気軽にご登録、お試しでのご利用をお待ちしております。
サイトURL: http://storage.cloudinvoice.co.jp

Cloud Invoice
------------------------------
";
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		if ($send_type == 5) {
			//echo "郵送<br/>";
			//郵送
			$mailto .= ","."mail_send@cloudinvoice.co.jp".","."info@cloudinvoice.co.jp";
			$status_num = 2;
			$charge_flag = 1;
			$download_password;
			$headers = "From: CloudInvoice <info@cloudinvoice.co.jp>\r\n";
			echo "<script type='text/javascript'>window.open('https://storage.cloudinvoice.co.jp/invoice/invoice2pdf?dlp=".$download_password."&dlps=1&clm=".$company_id."&c1=".$invoice_data_receive_arr[$k]['destination_id']."&c2=".$invoice_data_receive_arr[$k]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$k]['billing_date']."&ic=".$invoice_data_receive_arr[$k]['invoice_code']."&chs=".$invoice_data_receive_arr[$k]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$k]['destination_historical']."&shs=".$invoice_data_receive_arr[$k]['send_historical']."&pd=".$invoice_data_receive_arr[$k]['pay_date']."&ind=".$invoice_data_receive_arr[$k]['insert_date']."&st=".$invoice_data_receive_arr[$k]['status']."');</script>";
		}/* else if ($send_type == 51) {
			//echo "ok51<br/>";
			//echo "FAX<br/>";
			$headers = "From: CloudInvoice <info@cloudinvoice.co.jp>\r\n";
			$mailto .= ","."fax_send@cloudinvoice.co.jp";
			echo "<script type='text/javascript'>window.open('https://storage.cloudinvoice.co.jp/invoice/invoice2pdf?dlp=".$download_password."&dlps=1&clm=".$company_id."&c1=".$invoice_data_receive_arr[$k]['destination_id']."&c2=".$invoice_data_receive_arr[$k]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$k]['billing_date']."&ic=".$invoice_data_receive_arr[$k]['invoice_code']."&chs=".$invoice_data_receive_arr[$k]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$k]['destination_historical']."&shs=".$invoice_data_receive_arr[$k]['send_historical']."&pd=".$invoice_data_receive_arr[$k]['pay_date']."&ind=".$invoice_data_receive_arr[$k]['insert_date']."&st=".$invoice_data_receive_arr[$k]['status']."');</script>";
		} */ else if ($send_type == 6) {
			//echo "ok6<br/>";
			//echo "PDF<br/>";
			$mailto = "pdf@cloudinvoice.co.jp ";
			if ($data_status == 0) {
				$status_num = 3;
			} else {
				$status_num = $data_status;
			}
			$headers = "From: CloudInvoice <info@cloudinvoice.co.jp>\r\n";
			//echo "PDFを発行しました。";
			echo "<script type='text/javascript'>window.open('https://storage.cloudinvoice.co.jp/invoice/invoice2pdf?dlp=".$download_password."&dlps=1&clm=".$company_id."&c1=".$invoice_data_receive_arr[$k]['destination_id']."&c2=".$invoice_data_receive_arr[$k]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$k]['billing_date']."&ic=".$invoice_data_receive_arr[$k]['invoice_code']."&chs=".$invoice_data_receive_arr[$k]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$k]['destination_historical']."&shs=".$invoice_data_receive_arr[$k]['send_historical']."&pd=".$invoice_data_receive_arr[$k]['pay_date']."&ind=".$invoice_data_receive_arr[$k]['insert_date']."&st=".$invoice_data_receive_arr[$k]['status']."');</script>";
		} else if ($send_type == 7) {
			//echo "削除<br/>";
			//echo "ok7-1<br/>";
			
			if ($data_status == 0 || ($destination_id == "000000000000" && $data_status == 1) || $data_status == 3) {
				
				//echo "ok7-2<br/>";
				//echo $company_id;echo " ".$destination_id;echo " ".$send_code;echo " ".$invoice_code;
				//$del_result = $invoice_con -> invoice_data_delete($pdo,$company_id,$destination_id,$send_code,$invoice_code);
				$del_result = $invoice_con -> invoice_data_delete($pdo,$company_id,$destination_id,$send_code,$invoice_code);
				$invoice_data_id;
				//var_dump($del_result);
				//exit;
				$headers = "From: CloudInvoice <info@cloudinvoice.co.jp>\r\n";
				$mailto = "delete_invoice_data";
			} else {
				echo "<script type='text/javascript'>alert('請求番号「".$invoice_code."」は発行済みのため、削除できません')</script>";
				echo "<script type='text/javascript'>location.href='".$current_url."'</script>";
				exit();
			}
		} else {
			//echo "<br/>メール送信<br/>";
			//echo $mailto;
			$status_num = 1;
			echo "<script type='text/javascript'>window.open('https://storage.cloudinvoice.co.jp/invoice/invoice2pdf?dlp=".$download_password."&dlps=1&clm=".$company_id."&c1=".$invoice_data_receive_arr[$k]['destination_id']."&c2=".$invoice_data_receive_arr[$k]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$k]['billing_date']."&ic=".$invoice_data_receive_arr[$k]['invoice_code']."&chs=".$invoice_data_receive_arr[$k]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$k]['destination_historical']."&shs=".$invoice_data_receive_arr[$k]['send_historical']."&pd=".$invoice_data_receive_arr[$k]['pay_date']."&ind=".$invoice_data_receive_arr[$k]['insert_date']."&st=".$invoice_data_receive_arr[$k]['status']."');</script>";
		}
		
		//メールを送信する
		$mail_result = mb_send_mail($mailto,$company_self_name."様から請求書が届いております。", mb_convert_encoding($message, 'ISO-2022-JP-MS'), $headers);

		/*
		$mail_result = mail(
			$mailto,
			mb_encode_mimeheader("請求書到着通知-Cloud Invoice", 'ISO-2022-JP-MS'),
			mb_convert_encoding($message, 'ISO-2022-JP-MS'),
			$headers
		);
		*/
		
		//チェック用メールを送る
		mb_send_mail("test2_1@cloudinvoice.co.jp",$company_self_name."様から請求書が届いております。", mb_convert_encoding("宛先：".$mailto.$mail_cc_add."\r\n本文:\r\n".$message, 'ISO-2022-JP-MS'), $o_headers);
		
		/*
		mail (
			"test2_1@cloudinvoice.co.jp",
			mb_encode_mimeheader("請求書到着通知-Cloud Invoice", 'ISO-2022-JP-MS'),
			mb_convert_encoding("宛先：".$mailto."\r\nメッセージ\r\n".$message, 'ISO-2022-JP-MS'),
			$o_headers
		);
		*/
		//連続送信時の不具合を解消するため。
		//変数の初期化
		$mailto = "";
		$message = "";
		$headers = "";
		$company_email = "";
		$company_arr1 = array();
		$company_arr2 = array();

		
		if ($mail_result == true) {
			//echo $send_count++;
			$flag = "up_invoice_download_password";
			$update_words = " `status` = ".$status_num." WHERE `download_password` = '".$download_password."'";
			$invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$update_words);
			
			//▼自動仕訳用企業DB登録▼
			$table_name = $company_id;
			try {
				$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
			} catch (PDOException $e) {
			    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
			    $_SESSION['error_msg'] = $e->getMessage();
			}
			//▲自動仕訳用企業DB登録▲

			
			//▼自動仕訳の企業毎テーブル明細追加▼
			$flag = "download_invoice_data";
			$auto_words = $download_password;
			try {
				$invoice_details_data = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$auto_words);
			} catch (PDOException $e) {
			    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
			    $_SESSION['error_msg'] = $e->getMessage();
			}
			//▼自動仕訳用企業DB登録▼
			$table_name = $company_id;
			try {
				$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
			} catch (PDOException $e) {
			    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
			    $_SESSION['error_msg'] = $e->getMessage();
			}
			//▲自動仕訳用企業DB登録▲

			for ($q = 0; $q < count($invoice_details_data); $q++) {
				$flag = "auto_journal_data_product_name_condition";
				$table_name = $company_id;
				$aj_words = $invoice_details_data[$q]['product_name'];
				$aj_data_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
				$check_aj_flag = count($aj_data_arr);
				if ($check_aj_flag == 0) {
					$product_name = $aj_words;
					$auto_journalize_con -> insert_auto_journalize_normal($pdo_aj,$table_name,$product_name);
				}
			}
			//▲自動仕訳の企業毎テーブル明細追加▲
			
		} else {
			echo "<script type='text/javascript'>alert('請求番号「".$invoice_code."」の送信に失敗しました。');</script>";
		}
	}
	if ($k == $row_num - 1) {
		if ($charge_flag == 1) {
			//決済の実行
			try {
				
					if ($company_self_plan_status == 0) {
						$postage = 180;
					} else if ($company_self_plan_status == 1) {
						$postage = 160;
					} else if ($company_self_plan_status == 2) {
						$postage = 140;
					} else {
						$postage = 180;
					}
					$amount = $send_count * $postage;
					$product_name = $company_self_name."様:郵送代行費".number_format($amount)."円(".$postage."円×".$send_count."通分)";

				    // 決済を実行
				    if (isset($company_self_payment_id)) {
				    	$arg1 = 'customer';
				    	$param1 = $company_self_payment_id;
				    } else {
						$_SESSION['error_msg'] = "クレジットカード情報がありません";
						Header("Location: ".$return_url);
					    exit('Error');
				    }

				    $result = \Payjp\Charge::create(array(
				    	'description' => $product_name,
				    	$arg1 => $param1,
				    	'amount' => intval($amount, 10),
				    	'currency' => 'jpy'
				    ));
				    //var_dump($result);
				    $change_plan_flag = $result->id;
				    if (isset($change_plan_flag)){
						date_default_timezone_set('Asia/Tokyo');
						$fp = fopen(dirname(__FILE__).'/../files/'.$company_id."/mailsendcharge".date('Ym').".log", "a");
						//$today = getdate();
						fwrite($fp, $company_id.",".$company_self_name.",".$company_self_plan_status.",".$amount.",".date("Ymd His")."\r\n");
						fclose($fp);
					}
			    
			// 以下エラーハンドリング
			} catch (\Payjp\Error\Card $e) {
			    // カードが拒否された場合
			    print("CardException\n");
			    //print('Status is:' . $e->getStatus() . "\n");
			    //print('Type is:' . $e->getType() . "\n");
			    //print('Code is:' . $e->getCardErrorCode() . "\n");
			    //print('Param is:' . $e->getParam() . "\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
				Header("Location: ".$return_url);
			    exit('Error');
			} catch (\Payjp\Error\InvalidRequest $e) {
			    // リクエストで指定したパラメータが不正な場合
			    print("InvalidRequestException\n");
			    //print('Param is:' . $e->getParam() . "\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
				Header("Location: ".$return_url);
			    exit('Error');
			} catch (\Payjp\Error\Authentication $e) {
			    // 認証に失敗した場合
			    print("AuthenticationException\n");
			    //print('Param is:' . $e->getParam() . "\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
				Header("Location: ".$return_url);
			    exit('Error');
			} catch (\Payjp\Error\APIConnection $e) {
			    // APIへの接続エラーが起きた場合
			    print("APIConnectionException\n");
			    //print('Param is:' . $e->getParam() . "\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
				Header("Location: ".$return_url);
			    exit('Error');
			} catch (\Payjp\Error\Base $e) {
			    // PAYJPのサーバでエラーが起きた場合
			    print("APIException\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
				Header("Location: ".$return_url);
			    exit('Error');
			} catch (Exception $e) {
			    // PAYJPとは関係ない例外の場合
			    print("Unexpected exception\n");
			    print('Message is:' . $e->getMessage() . "\n");
			    $_SESSION['error_msg'] = "エラー:".$e->getMessage()."\n";
			    Header("Location: ".$return_url);
			    exit('Error');
			}
		}
		echo "<script type='text/javascript'>location.href='".$current_url."'</script>";
		exit();
	}
}


//▲情報メール送信関連処理▲

?>
<script type="text/javascript" src="../js/table-sorter-head.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function allCheck(){
	var check =  document.form.uriagekanriA.checked;
	for (var i = 0; i < parseInt(document.form.uriagekanri_num.value); i++){
		var uriage = "uriagekanri" + i
    	document.getElementById(uriage).checked = check;
	}
}
//-->
</script>


<script>

function OpenMailFrame(){
	window.scrollTo(0,0);
	document.getElementById("Mail").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenPostFrame(){
	var card_check = 
	<?php 
	if (isset($company_self_payment_id)) {
		echo 1;
	} else {
		echo 0;
	}
	?>;
	if (card_check == 0) {
		alert("クレジットカードを登録をしてください。");
	} else if (card_check == 1) {
		window.scrollTo(0,0);
		document.getElementById("Post").style.display="block";
		document.getElementById("fadeLayer").style.visibility = "visible";
	}
	
}
function OpenDeleteFrame(){
	window.scrollTo(0,0);
	document.getElementById("Delete").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenIssueFrame(){
	window.scrollTo(0,0);
	document.getElementById("Issue").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function ChangeDeposit1(n,icd){
	var St = document.getElementById("depositSt" + n);
	var Y = document.getElementById("depositY" + n);
	var N = document.getElementById("depositN" + n);
	var cid = "<?php echo $company_id;?>";
	var did = document.getElementById("destination_id" + n).value;
	var a = St.value;
	if (a == 0){
	Y.className = "deposit1";
	St.value = 1;
	} else if (a == 1){
	Y.className = "deposit0";
	St.value = 0;
	} else if (a == 2){
	Y.className = "deposit1";
	N.className = "deposit0";
	St.value = 1;
	}
	sendByAjax(St.value,did,cid,icd);
}
function ChangeDeposit2(n,icd){
	var St = document.getElementById("depositSt" + n);
	var Y = document.getElementById("depositY" + n);
	var N = document.getElementById("depositN" + n);
	var cid = "<?php echo $company_id;?>";
	var did = document.getElementById("destination_id" + n).value;
	var a = St.value;
	if (a == 0){
	N.className = "deposit2";
	St.value = 2;
	} else if (a == 2){
	N.className = "deposit0";
	St.value = 0;
	} else if (a == 1){
	Y.className = "deposit0";
	N.className = "deposit2";
	St.value = 2;
	}
	sendByAjax(St.value,did,cid,icd);
}

//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
	//alert(pst);alert(did);alert(cid);alert(icd);
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
//	if (num == 1) {
		var data = { get_payment_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
/*	} else if (num == 2) {
		var data = { get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
*/
//		if (num == 1) {
			send_url = "send_uriagekanri";
/*		} else if (num == 2) {
			send_url = "send_send_code";
		}
*/
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される

			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			//alert(data);
/*
			var address_data = data.split(",");
			//alert(document.getElementById('bikou').value);
			for (i = 0 ;i <= 5;i++) {
				//alert(address_data[i]);
				document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
			}
			//alert(address_data);
			document.getElementById('destination_email').value = address_data[7];
			document.getElementById('Kyoutu_No').value = address_data[0];
			document.getElementById('company_info').value = address_data[6];
			document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
			if (num == 1) {
				document.getElementById('addressee_company_info').value = "";
			}
			//onLoadData();
			//document.write(data);
*/
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}
</script>

<!--▼リンク先サムネイル化▼-->
<!-- Jquery本体ホスティング -->
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
<!-- /Jquery本体ホスティング -->
<!-- thumbnail.js -->
<!--
<script type="text/javascript" src="../js/thumbnail.js"></script>
-->
<!-- /thumbnail.js -->

<!-- ★head要素内で「外部CSS」と「外部JS」を読み込む -->
<style type="text/css">
.arc90_linkpic {
    display:none;
    position:absolute;
    left:0; top:1.5em;
    width:160px; height:120px;
}
.arc90_linkpicIMG {
    width:160px; height:120px;
    padding:0 4px 4px 0;
    background:transparent url("linkpic_shadow.gif") no-repeat bottom right;
}
</style>
<!--<script type="text/javascript" src="./include/topic/thumbnail/linkthumb/arc90_linkthumb.js" charset="utf-8"></script>-->
<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>
<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#uriageTable").colResizable({fixed:false});
		$("#uriageTable").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>

<!--▲リンク先サムネイル化▲-->

<iframe id="Mail" src="./frame/mail"></iframe>
<iframe id="Post" src="./frame/post"></iframe>
<iframe id="Delete" src="./frame/delete"></iframe>
<iframe id="Issue" src="./frame/issue"></iframe>

<title>売上管理 - Cloud Invoice</title>
<article>

	<section id="m-3-box">
		<h2>
			売上管理
		</h2>
			<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<div class="hiduke">
					<p>請求日</p>
					<input type="text" class="datepicker" id="box01" name="billing_date_from" />
					～
					<input type="text" class="datepicker" id="box02" name="billing_date_to" />
				</div>
				<div class="hiduke">
					<p>支払期限</p>
					<input type="text" class="datepicker" id="box1" name="pay_date_from" />
					～
					<input type="text" class="datepicker" id="box2" name="pay_date_to" />
				</div>

				<div class="kamoku">
					<p>会社名</p>
					<input type="text" name="invoice_company_name" />
				</div>
				<div class="hojo">
					<p>キーワード</p>
					<input type="text" name="tags" />
				</div>

<!--
				<div class="hojo">
					<p>請求書番号</p>
					<input type="text" name="invoice_code" />
				</div>
-->
				<!--
				<div class="bumon">
					<p>支払額</p>
					<input type="text" name="pay_amount_from" />
					～
					<input type="text" id="box2" name="pay_amount_to" />
				</div>
				-->
<!--
				<div class="hojo">
					<p>商品名</p>
					<input type="text" name="product_name" />
				</div>
-->
				<div class="bumon">
					<p>請求書状態</p>
					<label><input name="status" type="checkbox" value="1" <?php if ($_REQUEST['status'] == 1) {echo "checked";}?>/> 未発行</label>
					<label><input name="status_1" type="checkbox" value="1" <?php if ($_REQUEST['status_1'] == 1) {echo "checked";}?> /> 発行済</label><br/>
					<label><input name="status_2" type="checkbox" value="1" <?php if ($_REQUEST['status_2'] == 1) {echo "checked";}?> /> 顧客受領済</label>
					<label><input name="status_3" type="checkbox" value="1" <?php if ($_REQUEST['status_3'] == 1) {echo "checked";}?> /> 入金済</label>
				</div>
				<div class="hojo">
					<p>入金確認</p>
					<label><input name="paid" type="checkbox" value="1" <?php if ($_REQUEST['paid'] == 1) {echo "checked";}?>/> 入金済</label>
					<label><input name="unpaid" type="checkbox" value="1" <?php if ($_REQUEST['unpaid'] == 1) {echo "checked";}?> /> 未入金</label><br/>
					<label><input name="notyet" type="checkbox" value="1" <?php if ($_REQUEST['notyet'] == 1) {echo "checked";}?> /> 未処理</label>
				</div>

				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
				</div>

			</form>
			</section>
		<?php if ($conditions_words != "") {echo '<p>【絞り込み条件】'.$conditions_words.'</p>';}?>

		<p>合計金額: <span id="sum_payments_top"></span> 円</p>
		<form name="form" action="" method="post">
			<table id="uriageTable" class="tables tablesorter dragging" style="text-align:center;">
				<colgroup>
					<col align="center" width="20px">
					<col align="center" width="140px">
					<col align="center" width="80px">
					<col align="center" width="120px">
					<col align="center" width="150px">
					<col align="center" width="80px">
					<col align="center" width="80px">
					<col align="center" width="30px">
					<col align="center" width="30px">
					<col align="center" width="30px">
				</colgroup>
			
			<?php if($invoice_data_receive_num > 0) { ?>
				<thead>
					<tr id="first">
						<th><label id="checklabel"><input type="checkbox" name="uriagekanriA"  onClick="allCheck();" /></label><input type="hidden" name="uriagekanri_num" value="<?php echo $invoice_data_receive_num;?>"></th>
						<!--
						<th>共通コード宛先</th>
						-->
						<th>顧客コード宛先</th>
						<th>請求書日付</th>
						<th>請求書番号</th>
						<th>請求書名称</th>
						<th>支払期限</th>
						<th class="{sorter:'metadata'}">請求金額</th>
						<th>キーワード</th>
						<th>状態</th>
						<th>入金確認</th>
						<th>編集</th>
					</tr>
				</thead>
				<tbody>
			<?php
				} else{
					echo "<p>この条件ではデータが見つかりません</p>";
				}
			?>
			<?php
				for ($i = 0; $i < $invoice_data_receive_num; $i++) {
			?>
					<tr>
						<td id="check">
						<?php //ステータスがあればすべて表示
						if ($invoice_data_receive_arr[$i]['status'] != "") {
						?>
							<label id="checklabel"><input id="uriagekanri<?php echo $i;?>" type="checkbox" name="uriagekanri<?php echo $i;?>" value="1" /></label>
						<?php
						}
						?>

						</td>
					
					<?php
						$destination_id = $invoice_data_receive_arr[$i]['destination_id'];
						if ($destination_id != "") {
							$flag = "company_data";
							$conditions = " ";
							//$company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words,$conditions);
							//var_dump($company_arr);echo count($company_arr);echo "<br><br>";
						}
						
						//請求書番号ごとのデータを取得する
						$flag = "invoice_total_data_send_count";
						$words = "AND `invoice_code` = '".$invoice_data_receive_arr[$i]['invoice_code']."' ".$search_conditions;//条件
						$conditions = " ";
						$invoice_data_receive_arr_count = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words,$conditions);
						$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
						//echo "<br/><br/>test<br/><br/>";
						//var_dump($invoice_data_receive_arr);
						//var_dump($invoice_data_receive_arr_count);
						
							//送付先コードの表示データ取得
							$flag = "addressee_data_one";
							$words = "AND `company_id` = ".$company_id."";
							$conditions = " AND `historical` = ".$invoice_data_receive_arr[$i]['send_historical']." ";
							$send_code = "".$invoice_data_receive_arr[$i]['send_code']."";//historicalも見る
							$company_kanri_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$words,$conditions);
							if ($company_id == '000000000035') {
								//var_dump($invoice_data_receive_arr[$i]['sum(total_price)']);
							//var_dump($company_kanri_arr);
							}
							//var_dump($company_kanri_arr[0]['company_name']);echo count($company_kanri_arr);echo "<br><br>";
							//var_dump($invoice_data_receive_arr[$i]["company_name"]);echo "<br><br>";
							$dlp = $invoice_data_receive_arr[$i]['download_password'];
						if ($company_id == '000000000035') {
							//var_dump($invoice_data_receive_arr[$i]);
							if ( count($invoice_data_receive_arr[$i]['sender_data']) > 0) {
								$addresee_data_arr = explode(",",$invoice_data_receive_arr[$i]['sender_data']);
							//echo "ad1";echo	
							$kanri_cname = $addresee_data_arr[0];
							} else {
							//echo "ad";echo	
							$kanri_cname = $company_kanri_arr[0]['company_name'];
							}
							if ( count($invoice_data_receive_arr[$i]['claimant_data']) > 0) {
								$claimant_data_arr = explode(",",$invoice_data_receive_arr[$i]['claimant_data']);
							//echo "cl";echo	
							$claimant_name = $claimant_data_arr[0];
							}
							if ( count($invoice_data_receive_arr[$i]['destination_data']) > 0) {
								$destination_data_arr = explode(",",$invoice_data_receive_arr[$i]['destination_data']);
							//echo "ds";echo	
							$destination_name = $destination_data_arr[0];
							}
							//echo "<br><br>";
						}
					?>	
						<td id="atesaki">
						<!--
						<td id="atesaki">
						-->
							<input type="hidden" name="send_code<?php echo $i;?>" value="<?php echo $send_code;?>" />
							<?php 
								if(isset($invoice_data_receive_arr[$i]['company_name'])){ 
									if ($kanri_cname != "") {$kanri_company_name = $kanri_cname;} else {$kanri_company_name = $invoice_data_receive_arr[$i]['company_name'];}
									
									echo "<a href='./analysis_client?dst=".$destination_id."&sc=".$send_code."'>".$kanri_company_name."</a>";
								}
							?>
								<input type="hidden" name="company_name<?php echo $i;?>" value="<?php if(isset($invoice_data_receive_arr[$i]['company_name'])){ echo $kanri_company_name;}?>" />
							
							<?php
							if ($send_code != "" && $destination_id != 0 && $invoice_data_receive_arr[$i]['c_company_name'] != "") {
								echo "<br/>";
							?>
							<input type="hidden" id="destination_id<?php echo $i;?>" name="destination_id<?php echo $i;?>" value="<?php echo $destination_id;?>" />
							<?php echo "(".$invoice_data_receive_arr[$i]['c_company_name'].")";?><input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['c_company_name'];?>" />
							<?php
							} else {
							?>
							<input type="hidden" id="destination_id<?php echo $i;?>" name="destination_id<?php echo $i;?>" value="<?php echo $destination_id;?>" />
							<?php echo "<a href='./analysis_client?dst=".$destination_id."&sc=".$send_code."'>".$invoice_data_receive_arr[$i]['c_company_name']."</a>";?><input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['c_company_name'];?>" />
							<?php
							}
							?>
						<!--
						</td>
						-->
						</td>
						<td id="seikyuBi"><?php echo date('Y/m/d',strtotime($invoice_data_receive_arr[$i]['billing_date']));?><input type="hidden" name="billing_date<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['billing_date'];?>" /></td>
						<td id="seikyuNo"><?php echo $invoice_data_receive_arr[$i]['invoice_code'];?><input type="hidden" name="invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_code'];?>" /></td>
						<td id="seikyuName"><?php echo $invoice_data_receive_arr[$i]['invoice_name'];?><input type="hidden" name="invoice_name<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['invoice_name'];?>" /></td>
						<td id="siharaibi"><?php echo date('Y/m/d',strtotime($invoice_data_receive_arr[$i]['pay_date']));?><input type="hidden" name="pay_date<?php echo $i;?>" value="<?php echo $str = $invoice_data_receive_arr[$i]['pay_date'];?>" /></td>
						<td id="sumMoney" class="{sortValue: <?php echo $invoice_data_receive_arr[$i]['sum(total_price)'];?>}"><?php echo number_format($invoice_data_receive_arr[$i]['sum(total_price)']);?><input type="hidden" name="total_price<?php echo $i;?>" value="<?php echo number_format($invoice_data_receive_arr[$i]['sum(total_price)']);?>" /></td>
						<td id="siharaibi"><?php echo $invoice_data_receive_arr[$i]['tags'];?><input type="hidden" name="tags<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['tags'];?>" /></td>
						<td id="status"><input type="hidden" name="status<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['status'];?>" />
						<?php
							if ($invoice_data_receive_arr[$i]['status'] == 0) {
								echo "<a class='thumbxx' href='./invoice2pdf?clm=".$company_id."&c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&chs=".$invoice_data_receive_arr[$i]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$i]['destination_historical']."&shs=".$invoice_data_receive_arr[$i]['send_historical']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."' target='_blank'>未発行</a>";
							} else {
								//$pdf_url = '../files/' . $company_id . "/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf";
								//if (!file_exists($pdf_url)) {
									//class='thumb'に修正するとリンク先がサムネイルで見られるようになる。
									echo "<a class='thumbxx' href='./invoice2pdf?clm=".$company_id."&c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&chs=".$invoice_data_receive_arr[$i]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$i]['destination_historical']."&shs=".$invoice_data_receive_arr[$i]['send_historical']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=".$invoice_data_receive_arr[$i]['status']."&dlp=".$dlp."&dlps=1' style='color:red;' target='_blank'>";
								//} else {
								//	echo "<a class='thumbxx' href=\"https://storage.cloudinvoice.co.jp/files/".$company_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf\" style='color:red;' target='new'>";
									//echo "<a class='thumbxx' href=\"subWin('https://storage.cloudinvoice.co.jp/files/'".$company_id."/pdf/".date('Y', strtotime($str))."/".date('m', strtotime($str))."/".$invoice_data_receive_arr[$i]['download_password'].".pdf")."\" style='color:red;' target='new'>";
									//echo "<script type='text/javascript'>".window.open("https://storage.cloudinvoice.co.jp/files/".$company_id."/pdf/".date("Y", strtotime($str))."/".date("m", strtotime($str))."/".	$invoice_data_receive_arr[$i]['download_password'].".pdf", "new" )."</script>";
								//}
								//echo $invoice_data_receive_arr[$i]['status'];
								if ($invoice_data_receive_arr[$i]['status'] == 1) {
									echo "メール済";
								} else if ($invoice_data_receive_arr[$i]['status'] == 2) {
									echo "郵送済";
								} else if ($invoice_data_receive_arr[$i]['status'] == 3) {
									echo "PDF済";
   								} else if ($invoice_data_receive_arr[$i]['status'] == 6) {
									echo "受領済";
   								} else if ($invoice_data_receive_arr[$i]['status'] == 8) {
   									echo "振込中";
   								} else if ($invoice_data_receive_arr[$i]['status'] == 9) {
   									echo "入金済";
   								}
   								echo "</a>";
							}
						?>
						<?php $pst = $invoice_data_receive_arr[$i]['payment_status'];?>
						</td>
						<td id="deposit"><input type="hidden" name="depositSt<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['payment_status']; ?>" id="depositSt<?php echo $i;?>"><input id="depositY<?php echo $i;?>" type="button" value="入金済" class="<?php if ($pst == 1){echo 'deposit1';} else {echo 'deposit0';} ?>" style="margin:0 0 2px 0;" onclick="ChangeDeposit1(<?php echo $i.',\''.$invoice_data_receive_arr[$i]['invoice_code'].'\'';?>)" /><br /><input id="depositN<?php echo $i;?>" type="button" value="未入金" class="<?php if ($pst == 2){echo 'deposit2';} else {echo 'deposit0';} ?>" onclick="ChangeDeposit2(<?php echo $i.',\''.$invoice_data_receive_arr[$i]['invoice_code'].'\'';?>)" /></td>
						
						<td id="status"><input type="hidden" name="status<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['status'];?>" />
						<?php
							if ($invoice_data_receive_arr[$i]['status'] == 0) {
								echo "<a href='./makeinvoice_edit?clm=".$company_id."&c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&chs=".$invoice_data_receive_arr[$i]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$i]['destination_historical']."&shs=".$invoice_data_receive_arr[$i]['send_historical']."&st=".$invoice_data_receive_arr[$i]['status']."'>編集</a>";
								echo "<br/>";
								echo "<a href='./makeinvoice_edit?clm=".$company_id."&c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&chs=".$invoice_data_receive_arr[$i]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$i]['destination_historical']."&shs=".$invoice_data_receive_arr[$i]['send_historical']."&st=".$invoice_data_receive_arr[$i]['status']."&copy=1'>コピー</a>";
							} else if ($invoice_data_receive_arr[$i]['status'] >= 1) {
								echo "<a href='./makeinvoice_edit?clm=".$company_id."&c1=".$invoice_data_receive_arr[$i]['destination_id']."&c2=".$invoice_data_receive_arr[$i]['send_code']."&det=".$invoice_data_receive_arr_count[0]['count(*)']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&chs=".$invoice_data_receive_arr[$i]['claimant_historical']."&dhs=".$invoice_data_receive_arr[$i]['destination_historical']."&shs=".$invoice_data_receive_arr[$i]['send_historical']."&st=".$invoice_data_receive_arr[$i]['status']."&copy=1'>コピー</a>";
							}
						?>
						<input type="hidden" name="dl_pass<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['download_password'];?>" />
						<input type="hidden" name="mail_cc<?php echo $i;?>" value="<?php echo $invoice_data_receive_arr[$i]['mail_cc'];?>" />
						<?php
						//ページ内の支払合計金額を算出する処理
						$sum_payments += $invoice_data_receive_arr[$i]['sum(total_price)'];
						if ( $i == ($invoice_data_receive_num - 1)) {
							echo '<input type="hidden" name="sum_payments" id="sum_payments" value="'.number_format($sum_payments).'" />';
							echo '<script type="text/javascript">document.getElementById("sum_payments_top").innerHTML = document.getElementById("sum_payments").value;</script>';
						}
						?>
						</td>
						
					</tr>
			<?php
				}
			?>
			</tbody>
			
			</table>
			<div class="tableText">
				<label id="tableTxt"></label>				
			</div>

			<div style="display:none;">
					<input id="send_type" type="hidden" name="send_type" value="<?php echo $i;?>" />
					<input type="hidden" name="row_num" value="<?php echo $i;?>" />
					<input type="hidden" name="<?php echo $i;?>" value="<?php echo $i;?>" />
					<input type="hidden" name="send_reserve_date" id="send_reserve_date" value="" />
			</div>

			</form>
				
<?php
	if ($page_num > 1) {
		echo '<div class="pageBtnBox"><a class="pageBtn" href="?'.$param.'&page=1"><<</a>';
		for($p = 1;$p <= $page_num;$p++) {
			$bc_class = "";//初期化
			if ($page == $p) {$bc_class = " chBackColor";}
			echo '<a class="pageBtn'.$bc_class.'" href="?'.$param.'&page='.$p.'">'.$p.'</a>';
		}
		echo '<a class="pageBtn" href="?'.$param.'&page='.$page_num.'">>></a>'." - ".$page.'/'.$page_num.'ページ -'.'</div>';
	}
?>

			<!--
			<div id="jquery-pager" class="jquery-pager">
				<form>
					<button class="first"><<</button>
					<button class="prev"><</button>
					<input type="text" class="pagedisplay"/>
					<button class="next">></button>
					<button class="last">>></button>
					<select class="pagesize">
						<option value="10">10</option>
						<option selected="selected" value="20">20</option>
						<option value="30">30</option>
						<option value="40">40</option>
						<option value="50">50</option>
					</select>
				</form>
			</div>
			-->
		<div id="uriagekanri">
			<button onclick="OpenMailFrame()">請求書をメール送信する</button>
			<button onclick="OpenPostFrame()">請求書を郵送依頼する</button>
			<button onclick="OpenIssueFrame()">請求書PDFを発行する<br></button>
			<button onclick="OpenDeleteFrame()">請求データを削除する<br>（発行済の請求データは削除できません）</button>
		<?php
			if (isset($_SESSION['error_msg'])) {
				echo "<p>".$_SESSION['error_msg']."</p>";
				unset($_SESSION['error_msg']);
			}
		?>
		</div>
		
	</section>
</article>
<div id="fadeLayer" onclick="CloseFrame(3)"></div>
<div id="noneLayer"></div>

<?php require("footer.php");?>