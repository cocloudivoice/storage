<?php session_start(); ?>
<?php require("header.php");?>
<?php


//必要なクラスファイルを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if(isset($_SESSION['user_id'])){
	$user_id = $company_id = $_SESSION['user_id'];
}else{
	header("Location:./logout.php");
	exit();
}

$flag = "company_data";
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();
$company_arr  = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);

//設定変更の処理▼
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();
if (isset($_REQUEST['company_name'])) {

	if ($_REQUEST['change_place'] == 1) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`company_name`='".$_REQUEST['company_name']."',`company_name_reading`='".$_REQUEST['company_name_reading']."',
		`segment`=".$_REQUEST['segment'].",`zip_code`='".$_REQUEST['zip_code']."',`prefecture`='".$_REQUEST['prefecture']."',
		`address1`='".$_REQUEST['address1']."',`address2`='".$_REQUEST['address2']."',`tel_num`='".$_REQUEST['tel_num']."',
		`fax_num`='".$_REQUEST['fax_num']."',`url`='".$_REQUEST['url']."' 
		 WHERE company_id = ".$company_id."";
	} else if ($_REQUEST['change_place'] == 2) {
		$sql = "UPDATE `COMPANY_TABLE` SET 
		`email`='".$_REQUEST['email']."',`section`='".$_REQUEST['section']."',
		`clerk`='".$_REQUEST['clerk']."',`bank_id`=".$_REQUEST['bank_id'].",
		`blanch_id`=".$_REQUEST['blanch_id'].",`account_devision`=".$_REQUEST['account_devision'].",
		`account_num`=".$_REQUEST['account_num'].",`account_info`='".$_REQUEST['account_info']."',
		`stamp_image`='".$_REQUEST['stamp_image']."',`logotype`='".$_REQUEST['logotype']."',
		`remarks1`='".$_REQUEST['remarks1']."',`remarks2`='".$_REQUEST['remarks2']."' 
		 WHERE company_id = ".$company_id."";
	}
	$company_con -> company_sql($pdo,$company_id,$sql);

}

//設定変更の処理▲

?>


<script>

function OpenDefaultFrame(){
	document.getElementById("default").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}



</script>
<title>基本設定 - Cloud Invoice</title>


<iframe id="default" name="default" src="./frame/default"></iframe>


<article>

	<section id="m-2-box">
		<h2>
			基本設定
		</h2>
		
		<table>
			<tr id="first">
				<th>事業署名</th>
				<td><?php echo $company_arr[0]['company_name']; ?> </td>
			</tr>
			<tr>
				<th>ヨミガナ</th>
				<td><?php echo $company_arr[0]['company_name_reading']; ?></td>
			</tr>

			<tr>
				<th>事業所区分</th>
				<td><?php if ($company_arr[0]['segment'] == 0 ) { echo "個人事業主";} else if ($company_arr[0]['segment'] == 1 ) { echo "法人";} ?></td>
			</tr>
			<tr>
				<th>事業所種別</th>
				<td><?php echo $company_arr[0]['industry_type']."-".$company_arr[0]['industry_type_s']; ?></td>
			</tr>
			<tr>
				<th>郵便番号</th>
				<td><?php echo $company_arr[0]['zip_code'];?></td>
			</tr>
			<tr>
				<th>都道府県</th>
				<td><?php echo $company_arr[0]['prefecture'];?></td>
			</tr>
			<tr>
				<th>住所１</th>
				<td><?php echo $company_arr[0]['address1'];?></td>
			</tr>
			<tr>
				<th>住所２</th>
				<td><?php echo $company_arr[0]['address2'];?></td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td><?php echo $company_arr[0]['tel_num'];?></td>
			</tr>
			<tr>
				<th>FAX番号</th>
				<td><?php echo $company_arr[0]['fax_num'];?></td>
			</tr>
			<tr id="last">
				<th id="last">URL</th>
				<td id="last"><?php echo $company_arr[0]['url'];?></td>
			</tr>
		
		
		</table>
		<button onclick="OpenDefaultFrame()">変更する</button>
		
		<div id="clear"></div>
	</section>

</article>
<?php require("footer.php");?>
<div id="fadeLayer" onclick="CloseFrame(1);"></div>
<div id="noneLayer"></div>

<?php
/*
	//ReadXmlのクラスファイルを読み込む
	include_once("/var/www/storage.cloudinvoice.co.jp/html/cfiles/ReadXml.class.php");
	
	//ReadXmlのオブジェクトを生成する
	$readxml_con = new ReadXml();
	//渡すファイルのパス
	$xml_file_pass = "/var/www/storage.cloudinvoice.co.jp/html/files/common/type_of_industry.xml";
	$xml_data_arr = $readxml_con -> getXmlData($xml_file_pass);
	$rows = $xml_data_arr['rows'];

	for ($i = 0; $i < $rows; $i++) {
		echo $xml_data_arr[$i]['itype_code']." ".$xml_data_arr[$i]['itype_name'];
		echo "<br/>\r\n";
	}
*/
?>