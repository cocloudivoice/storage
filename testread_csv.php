<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/db_control.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/multipurposecontrol.class.php');


//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$multi_purpose_con = new multi_purpose_control();


//変数の宣言
$company_id = $_SESSION['user_id'];
$success_num = 0;
$all_num = 0;
//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];

$file = '../files/'.$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
}

ob_start();

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}
//var_dump($csv);echo "<br />\n";

//echo "<table border=1>";
$all_num = count($csv) - 1;
$csv_id = "".$_SESSION['file_name'].$company_id.rand();





$sql = "
SELECT id , inport_conv FROM INDI_CONV_TABLE
WHERE id = $company_id.
";
	
$in_conv = $multi_purpose_con -> multi_purpose_sql($pdo,$sql);
include_once('/var/www/storage.cloudinvoice.co.jp/html/indi/' . $in_conv[0]['inport_conv'] . 'in.php');



foreach ($csv as $value_check) {
	if ( $value_check[0] != "共通コード") {
		$sql = "SELECT `invoice_code` 
		FROM `INVOICE_DATA_TABLE` WHERE claimant_id = ".$company_id." 
		AND `destination_id` = ".$value_check[0]." 
		AND `invoice_code` = '".$value_check[3]."'
		";
		echo "\r\n";

		$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
		if ($invoice_code_check[0] != NULL) {
			//echo "戻す。\n";
			$_SESSION['up_info_msg'] = "請求書番号が重複しているのでデータを登録できません。";
			header("Location:./seikyucsv", false);
			exit();
		} else {
			//echo "戻さない。\n";
		}
	}
}




if ($success_num > 0) {
	//CSV取り込み履歴を記録する。
	//CSV_HISTORY_TABLEですが、ついでなのでINVOICE_DATA_TABLEのコントローラーで処理してしまいます。
	$sql = "
	INSERT INTO `CSV_HISTORY_TABLE`(
	`company_id`, `file_name`, `csv_id`
	) VALUES (
	".$company_id.", '".$_SESSION['file_name']."', '".$csv_id."'
	)";
	$invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
}

//echo "</table>";

fclose($temp);
if ($success_num > 0) {
	//$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込みました。";
	$_SESSION['comment'] = "<a href='./testseikyuhakkou'>".$all_num."件中".$success_num."件のアップロードに成功しました。</a>";
}else {
	$_SESSION['comment'] = $_SESSION['file_name']."をデータベースに取り込めません。";
}

header("Location:./testseikyucsv", false);
ob_end_flush();
exit();

?>
<button onclick="window.location.href='./main'">戻る</button>
</body>