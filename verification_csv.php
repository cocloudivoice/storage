<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/db_control.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$db_con = new db_control();
$invoice_data_con = new invoice_data_control();

//変数の宣言
$company_id = (string)$_REQUEST['company_id'];

//CSVフォーマットのカラム数
$colms = $_REQUEST['colms'];

//読み込むファイル名
/*
if ($_REQUEST['inport_file']) {
	$file = $_REQUEST['inport_file'];
} else {
	header("Location:../view/company_home.php");
	exit();
}
*/
//company_id取得
/*
if ($_REQUEST['company_id']) {
	$company_id = $_REQUEST['company_id'];
} else {
	header("Location:../view/company_home.php");
	exit();
}
*/

$top4 = substr($company_id,0,4);
$mid4 = substr($company_id,4,4);

$file = "../files/" .$top4."/". $mid4."/".$company_id. "/csvfiles/datacsv.csv";
if(!file_exists($file)) {
	$file = '../files/'.$company_id.'/csvfiles/datacsv.csv';
}
if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();
	 
	fwrite($temp, $data);
	rewind($temp);
}





?>

<!DOCTYPE html>

<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
		<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
		<title>HOME - [Cloud Invoice]</title>
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//hikaku.jp/common/images/logo.jpg" />
		<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
		<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://storage.cloudinvoice.co.jp/" />
		<script src="//hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>
</head>
<body>


<?php

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}
//var_dump($csv);echo "<br />\n";

echo "<table border=1>";

foreach ($csv as $value) {
	//var_dump($value);echo "<br />\n";
	echo "<tr>";
	for ( $i = 0; $i < $colms ; $i++ ) {
		//echo $colms;
		echo "<td>";
		if ($value[$i] != "") {
			echo $cell = $value[$i];
		} else {
			echo $cell = NULL;
		}
		echo "</td>";
	}
	
//ここでDBにインサート
var_dump($value);

echo "</table>";

fclose($temp);

//header("Location:./seikyucsv");
//exit();
}
?>
<button onclick="window.location.href='./main'"></button>
</body>