﻿<?php require("header.php");?>

<script type="text/javascript">

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box3" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box4" ).datepicker();
});


function OpenkCommonSearchFrame(){
	document.getElementById("kCommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}

function OpenkSouhuListFrame(){
	document.getElementById("kSouhuList").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}

function FrameHeight(){
	window.setTimeout("window.parent.frames['kCommonSearch'].test()","600");
}

function FrameHeight2(){
	window.setTimeout("window.parent.frames['kSouhuList'].test()","30");
}

</script>

<title>売上管理 - Cloud Invoice</title>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>
<iframe id="kSouhuList" name="kSouhuList" src="./frame/kSouhuList"></iframe>

<article>

	<section id="m-1-box">
		<h2>
			売上管理
		</h2>
		
		<p>
			条件を指定してください。
		</p>
		<form action="checkuriagekanri" method="GET" name="seikyuhakkou_form">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" id="Kyoutu_No" name="destination_id" class="kCommonSearch" name="destination_id_visible" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame()" value="<?php if ($destination_company_id != "") {echo $destination_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
			</div>
			
			<div id="ID">
				<h3>送付先コード</h3>
				<input type="hidden" id="select_dname"  name="send_code" value="" />
				<input type="text" id="souhuname" onfocus="FrameHeight2();OpenkSouhuListFrame();" onkeyup="FrameHeight2();" value="<?php if ($destination_client_id != "") {echo $destination_client_id;}?>" placeholder="送付先コードで検索する"/>
			</div>
			
			<div id="status">
				<h3>状態</h3>
				<label><input name="status" type="checkbox" value="1" checked /> 未発行</label>
				<label><input name="status_1" type="checkbox" value="1" checked /> 発行済</label>
			</div>
			
			
			
			<div id="hiduke">
				<h3>請求書日付</h3>
				<input id="box1" type="text" name="billing_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box2" type="text" name="billing_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="hiduke">
				<h3>支払期限日</h3>
				<input id="box3" type="text" name="pay_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box4" type="text" name="pay_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="shiharaEx">
				<button onclick="submit();">確認画面に進む ></button>
			</div>
		</form>
	</section>






</article>

<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(9)"></div>

<?php require("footer.php");?>