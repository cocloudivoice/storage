<?php require("header.php");?>


<title>紙証憑のデータ化 - Cloud Invoice</title>


<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];
$up_id = uniqid();

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

?>

<script>

/*
function download_file() {
	location.href = "//storage.cloudinvoice.co.jp/invoice/download";
}
*/
/*
function erase_file(n) {
	
	if (window.confirm('このCSVデータを消去しますか？')) {
		frm_elm = document.getElementById('erase_form'+ n);
		frm_elm.submit();
	} else {
		//alert('キャンセルされました');
		return false;
	}
*/
/*
	if(window.confirm('このCSVデータを消去しますか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}

}
*/

</script>
<script>
$(document).ready(function() {
  var show_bar = 0;
  $('input[type="file"]').click(function(){
    show_bar = 1;
  });
  //show iframe on form submit
  $('#submit-btn').click(function () {
    if (show_bar === 1) { 
      $('#upload_frame').show();
      function set () {
$('#upload_frame').attr('src','upload_frame.php?up_id=<?php echo $up_id; ?>');
      }
      setTimeout(set);
    }
  });
});
</script>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>-->
<script type="text/javascript" src="./js/some.js"></script>
<script type="text/javascript" src="./js/script.js"></script>
<article>

	<section id="m-1-box">
		<h2>
			紙証憑のデータ化
		</h2>
		<p>
			データ化するPDF、PNG、JPEG等の画像データを選択してください（複数選択可能）<br>
			最終確認を人が行うため、データ化には最大2営業日掛かります。<br>
			また、一明細につき10円～の変換料金が必要になります。<br>
			詳細は<a href="../plan.php" target="_blank">こちら</a>をご覧ください。
		<p>
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" id="form_sansho" action="./upload_pdf" method="post" enctype="multipart/form-data">
		<div id="m-1-box">
			
			<input type="hidden" name="<?php echo ini_get("session.upload_progress.name"); ?>" value="example" />
			<input type="file" name="upfile[]"  multiple="multiple"/>
			<input type="hidden" name="return_url" value="./pdfconvert" />
			<input type="hidden" name="ahead_url" value="./pdfconvert" />
			<?php $_SESSION['comment'] = "";?>
			
		</div>
			<input type="submit" name="create" value="アップロード" />
			
		</form>
		<div id="progress_box">
		</div>

		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>

	</section>


<!--
	<section id="m-1-box">
		<h2>
			取込履歴
		</h2>

		<table id="scsv">
			<tr>
				<th id="name">ファイル名</th>
				<th id="date">取込日時</th>
			</tr>
	<?php
		for ($i = 0; $i < $csv_history_num; $i++) {
	?>
			<tr>
				<td><span><?php echo $csv_history_data_arr[$i]["file_name"];?></span></td>
				<td><?php echo $csv_history_data_arr[$i]["upload_date"];?></td>
				<td>
					<form id="erase_form<?php echo $i;?>" action="./erase_csvfile" method="post" />
						<input type="hidden" name="company_id" value="<?php echo $company_id;?>" />
						<input type="hidden" name="csv_id" value="<?php echo $csv_history_data_arr[$i]['csv_id'];?>" />
					</form>
				</td>
			</tr>
	<?php
		}
	?>
		</table>


	</section>
-->




</article>
<?php require("footer.php");?>