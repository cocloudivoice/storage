<?php

require_once("aaMailImapHead.php");

// メールボックスを開きます
// INBOX は受信トレイになります。
$mbox = imap_open("{{$host}:{$port}/imap/ssl}INBOX", $user, $password) or exit("Connection Error");
 
// メッセージナンバー
$msgno = 5;
// メッセージヘッダ取得
$header = imap_headerinfo($mbox, $msgno);
// メッセージ本文取得
$body = imap_body($mbox, $msgno, FT_INTERNAL);
$mailData = array(
    "date"        => date("Y-m-d H:i:s", strtotime($header->date)),
    "to"        => convertMailStr($header->toaddress),
    "cc"        => isset($header->ccaddress) ? convertMailStr($header->ccaddress) : '',
    "from"        => convertMailStr($header->fromaddress),
    "subject"    => convertMailStr($header->subject),
    "body"        => mb_convert_encoding($body, 'utf-8', 'auto'),
);
// 閉じる
imap_close($mbox);
 
echo "<p>To : {$mailData['to']}<br />
CC : {$mailData['cc']}<br />
From : {$mailData['from']}<br />
Subject : {$mailData['subject']}</p>";
echo '<p>Body : '.nl2br($mailData['body']).'</p>';

?>