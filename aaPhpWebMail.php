<?php
require_once("Mail.php");             // メール
require_once("Mail/mimeDecode.php");      // PEAR MimeDecode

$sender = "test2@co.cloudinvoice.co.jp";
$from   = $sender;
$to     = "akira.hamasaki@co.cloudinvoice.co.jp";
$subject = "日本語のタイトル。結構長めに設定してmime変換されているかチェック。";
//$subject = mb_convert_encoding($subject, 'UTF-8', 'auto');
//$subject = mb_encode_mimeheader($subject, 'ISO-2022-JP', 'auto');

$mail_text = <<<EOS
From: $from 
Sender: $sender 
To: $to 
Subject: $subject 
Content-Type: text/plain; charset="ISO-2022-JP"
Content-Transfer-Encoding: 7bit

実際はテンプレートエンジンとかを利用して、
メールの本文も作成します。
今回はサンプルなので、
こんな感じにしてます。
EOS;

// JIS変換
$mail_text = mb_convert_encoding( $mail_text, "ISO-2022-JP", "UTF-8" );

// メールを分解
$decoder = & new Mail_mimeDecode( $mail_text ); // MIME分解
$parts = $decoder->getSendArray();  // それぞれのパーツを格納
list( $recipients, $headers, $body ) = $parts; // 各パーツを配列に格納
// サブジェクトの処理
//$original = mb_internal_encoding('UTF-8');
//mb_internal_encoding( "ISO-2022-JP" );
$subject = $headers['Subject'];
//$subject = mb_encode_mimeheader($subject, 'ISO-2022-JP', 'auto');
//mb_internal_encoding( $original );

// SMTPサーバ
$mail_options = array(
'host'    => 'mail.co.cloudinvoice.co.jp', // ホスト名
'port'    => 587,      // ポート番号
'auth'    => true,      // 認証必要？
'username'  => "test2@co.cloudinvoice.co.jp",    // ユーザー名
'password'  => "1234abcd",    // パスワード
'localhost' => 'localhost',   // HELO名
);

// Create the mail object using the Mail::factory method
$mail_object =& Mail::factory("SMTP",$mail_options);  // SMTP送信準備

$mail_object->send($recipients, $headers, $body);

?>