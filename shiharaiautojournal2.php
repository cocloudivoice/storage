<?php 
session_start();

//必要なクラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/journalizedhistorycontrol.class.php');
//ページ生成用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = sprintf("%012d",$_SESSION['user_id']);
} else {
	header("Location:./logout");
	exit();
}

//▼税区分ファイルの読み込み▼
$tax_class_arr = array();
$file_path = '/var/www/storage.cloudinvoice.co.jp/html/invoice/files/tax_class.csv';
$fp = fopen($file_path, 'r');
$count = 0;
if ($fp){
    if (flock($fp, LOCK_SH)) {
        while (!feof($fp)) {
            $buffer = fgets($fp);
            $buffer = mb_convert_encoding($buffer, 'UTF-8', 'sjis-win');
            //print($buffer.'<br>');
            $tax_class = split(",",$buffer);
            $tax_class_arr[$tax_class[0]] = $tax_class[1];
            
            $count++;
        }
        flock($fp, LOCK_UN);
    } else {
        //print('ファイルロックに失敗しました');
    }
}
fclose($fp);
//var_dump($tax_class_arr);
//▲税区分ファイルの読み込み▲

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

	$flag1 = "invoice_count";
	$flag = "invoice_data_to_autojournalize";

	if ($_REQUEST['destination_id']) {
		$claimant_id = $_REQUEST['destination_id'];
		$words .= " AND `claimant_id` = ".$claimant_id." ";
	}
	if ($_REQUEST['update_from']){
			$upfrom = str_replace(" ","",substr($_REQUEST['update_from'],0,4)."/".substr($_REQUEST['update_from'],4,2)."/".substr($_REQUEST['update_from'],6,2));
			$words .= " AND `update_date` >= '".$upfrom."' ";
	}
	if ($_REQUEST['update_to']){
			$upto = str_replace(" ","",substr($_REQUEST['update_to'],0,4)."/".substr($_REQUEST['update_to'],4,2)."/".substr($_REQUEST['update_to'],6,2));
			$words .= " AND `update_date` <= '".$upto."' ";
	}
	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$words .= " AND `pay_date` >= ".$payfrom." ";
	}
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$words .= " AND `pay_date` <= ".$payto." ";
	}
	
	
	//ソートの設定
	if (isset($_GET['sort_num']) && $_GET['sort_num'] != NULL) {
		$sort_num = htmlspecialchars($_GET['sort_num'],ENT_QUOTES);
		$sort_key = $sort_num;
	} else {
		$sort_key = "display_rank";//$_GET['sort_name'];//GETか何かでカラム名取得を実装する。
	}

	if (isset($_GET['sort_key']) && $_GET['sort_key'] != NULL) {
		$sort_key = htmlspecialchars($_GET['sort_key'],ENT_QUOTES);
	}
	if (isset($_GET['sort_type']) && $_GET['sort_type'] != NULL) {
		$sort_type = htmlspecialchars($_GET['sort_type'],ENT_QUOTES);
	}
	if ($_REQUEST['sort_type'] == 1) {
		if ($sort_type =="ASC" || $sort_type == "") {
			$sort_type ="DESC";
		} else {
			$sort_type = "ASC";
		}
	} else {
		if ($sort_type == "DESC") {
			$sort_type = "DESC";
		} else {
			$sort_type = "ASC";
		}
	}
	
	
	//ページ条件なしでデータ数を取得
	//$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	//仕訳済みフラグのためここで処理を分ける。
	$data_num_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	var_dump($data_num_arr);
	//表示数の制御
	$max_disp = 100;
	$sort_key = "`id`";
	$sort_type = "ASC";
	$offset = ($page - 1) * $max_disp;
	$data_num = count($data_num_arr);
	
	$conditions = " ".$sort_key." ".$sort_type." limit ".$max_disp." offset ".$offset." ";
	
	//ページ数取得
	$page_num = ceil($data_num/$max_disp);
	//ページ分割のための条件追加
	//$words .= " ORDER BY `id` LIMIT 100";
	$words .= " ORDER BY ";
	$words .= $conditions;
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	$invoice_data_num = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	$invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
	//var_dump($invoice_data_arr);

	//▼自動仕訳用企業DB登録▼
	//もし存在しなければ、企業品目テーブルと企業用レコードテーブルを作成
	$table_name = "".$company_id;
	try {
		$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
	} catch (PDOException $e) {
	    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
	    $_SESSION['error_msg'] = $e->getMessage();
	}
	//▲自動仕訳用企業DB登録▲

	//var_dump($invoice_data_num);
	//var_dump($invoice_data_arr);
	//$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
    //出力ファイル名の作成
    $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
  
    //文字化けを防ぐ
    $csv_data = mb_convert_encoding ( $csv_data , "utf-8" , 'utf-8' );
      
    //MIMEタイプの設定
    //header("Content-Type: application/octet-stream");
    //名前を付けて保存のダイアログボックスのファイル名の初期値
    //header("Content-Disposition: attachment; filename={$csv_file}");
  
    // データの出力
    //echo($csv_data);
    //exit();
    $flag = "select_max_journalized_id";
    $table_name = "JOURNALIZED_HISTORY_".sprintf("%012d",$company_id);
    $aj_words = "";
    $max_jh_num_arr = $journalized_history_con -> journalized_history_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
    $max_jh_num = $max_jh_num_arr[0]['max(`text_data3`)'];
    $flag = "";
    $table_name = "";
    $aj_words = "";

?>

<?php require("header.php");?>

<title>自動仕訳確認 - Cloud Invoice</title>

<article id="autojournal2">
	
	<div id="LeftBlock">
		<h2>仕訳作成</h2>
		<div class="table">
			<div class="block">
				<p>取引日</p>
				<input type="text" />
			</div>
			<div class="block">
				<p>金額</p>
				<input type="text" />
			</div>
		</div>
		<div class="table">
			<div class="block">
				<p>借方勘定科目</p>
				<input type="text" />
			</div>
			<div class="block">
				<p>貸方勘定科目</p>
				<input type="text" />
			</div>
		</div>
		<div class="table">
			<div class="block">
				<p>取引先</p>
				<input type="text" />
			</div>
			<div class="block">
				<p>メモ</p>
				<input type="text" />
			</div>
		</div>
		<div id="register">
			<input type="button" value="登録" />
		</div>

	</div>
	
	
	<div id="RightBlock">
		<img src="../../images/invoice1.png">
	</div>
	
</article>


<?php require("footer.php");?>