<?php require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbcontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;

//ユーザーテーブルのコントローラーを呼び出す
$invoice_con = new invoice_data_control();
$company_con = new company_control();
$maindb_con = new db_control();

//各テーブルからデータを取得する

//請求書のステータス変更
if($_REQUEST['status_chg'] == 1) {
	$change_download_password = htmlspecialchars($_REQUEST['download_password'],ENT_QUOTES);
	$change_status = htmlspecialchars($_REQUEST['select_status'],ENT_QUOTES);
	$flag_change_status = "up_invoice_download_password";
	$words_change_status = " `status` = ".$change_status." WHERE `destination_id` = ".$company_id." AND `download_password` = '".$change_download_password."' "; 
	$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag_change_status,$company_id,$words_change_status);
}

//ダウンロード済みの請求書取得
$flag="invoice_total_data_receive_past";
$words;//条件をここに入れる。
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
//rsort($invoice_data_receive_arr);
/*
if ($invoice_data_receive_num > 5) {
	$invoice_data_receive_num = 5;
}
*/
//var_dump($invoice_data_receive_arr);


?>

<article>

	<section id="m-1-box">
		<h2>
			確認済みの請求
		</h2>
		
		<table id="main">
			<tr>
				<th id="kaisha">会社名</th>
				<th id="koushin">更新日</th>
				<th id="kingaku">支払額</th>
				<th id="tekiyou">摘要</th>
				<th id="shiharai">支払期限</th>
				<th id="hakkou">請求書</th>
				<th id="joutai">状態</th>
			</tr>
		<?php 
			for ($i = 0; $i < $invoice_data_receive_num; $i++) {
		?>
			<tr>
			<?php 
				$claimant_id = $invoice_data_receive_arr[$i]['claimant_id'];
				if ($claimant_id != "") {
					$flag = "company_data";
					$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
				}
			?>
				<td><?php echo $company_arr[0]["company_name"];?></th>
				<td><?php echo $invoice_data_receive_arr[$i]["update_date"];?></td>
				<td><?php echo number_format($invoice_data_receive_arr[$i]["sum(total_price)"]);?></td>
				<td><?php echo $invoice_data_receive_arr[$i]["product_name"];?></td>
				<td><?php $str = $invoice_data_receive_arr[$i]["pay_date"];echo substr($str,0,4)."/".substr($str,4,2)."/".substr($str,6,2); ?></td>
				<td>
				<?php
						
					$flag3 = "download_invoice_data";
					$words3 = $invoice_data_receive_arr[$i]['download_password'];//条件をここに入れる。
					$invoice_num_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag3,$company_id,$words3);
					//var_dump($invoice_num_arr);
					$invoice_num = count($invoice_num_arr);

				?>

				
				
					<!--<?php echo "<a href='./invoice2pdf?clm=".$claimant_id."&c1=".$company_id."&c2=".$invoice_data_receive_arr[$i]['send_code']."&bd=".$invoice_data_receive_arr[$i]['billing_date']."&ic=".$invoice_data_receive_arr[$i]['invoice_code']."&pd=".$invoice_data_receive_arr[$i]['pay_date']."&ind=".$invoice_data_receive_arr[$i]['insert_date']."&st=1' target='_blank'>請求書</a>";?>-->
					<?php echo "<a href=\"./invoice2pdf?dlp=".$invoice_data_receive_arr[$i]['download_password']."&det=".$invoice_num."\" target=\"_blank\">請求書</a>";?>
				</td>
				<td>
				<?php
					echo "<form action='' method='post'>";
					if ($invoice_data_receive_arr[$i]["status"] == 6) {
						echo '
						<SELECT name="select_status">
							<OPTION value="6" selected>DL済み</OPTION>
							<OPTION value="7">保留</OPTION>
							<OPTION value="8">処理済</OPTION>
						</SELECT>';
						echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
					} else if ($invoice_data_receive_arr[$i]["status"] == 7) {
						echo '
						<SELECT name="select_status">
							<OPTION value="6">DL済み</OPTION>
							<OPTION value="7" selected>保留</OPTION>
							<OPTION value="8">処理済</OPTION>
						</SELECT>';
						echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
					} else if ($invoice_data_receive_arr[$i]["status"] == 8) {
						echo '
						<SELECT name="select_status">
							<OPTION value="6">DL済み</OPTION>
							<OPTION value="7">保留</OPTION>
							<OPTION value="8" selected>処理済</OPTION>
						</SELECT>';
						echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
					} else {
						echo "ステータス不明";
						echo '
						<SELECT name="select_status">
							<OPTION value="6" selected>DL済み</OPTION>
							<OPTION value="7">保留</OPTION>
							<OPTION value="8">処理済</OPTION>
						</SELECT>';
						echo '<input type="hidden" name="download_password" value="'.$invoice_data_receive_arr[$i]['download_password'].'">';
					}
					echo '<input type="hidden" name="status_chg" value="1" />';
					echo '<input type="button" onclick="submit();" value="変更" /></form>';
				?>
				</td>
			</tr>
		<?php 
					
			}
		?>
		</table>
		
		
	</section>






</article>




<?php require("footer.php");?>
