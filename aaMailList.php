<?php
	

	mb_language("ja");
	mb_internal_encoding("UTF-8");
	
    //-------------------------------
    //パラメータ定義
    //-------------------------------
    //文字エンコード
    //$enc = "UTF-8";
    $enc = "ISO-2022-JP";
    //POPサーバ
    $mail_host = "mail.co.cloudinvoice.co.jp";
    //ポート
    $mail_port = 110;
    //ユーザ
    $mail_user = "test2@co.cloudinvoice.co.jp";
    //パスワード
    $mail_pwd = "1234abcd";

    //-------------------------------
    //接続＆ログイン
    //-------------------------------
    //接続
    $fp = fsockopen($mail_host, $mail_port);

    //ログイン
    $line = fgets($fp, 512);
    fputs($fp, "USER $mail_user\r\n");    // USER名
    $line = fgets($fp, 512);        
    fputs($fp, "PASS $mail_pwd\r\n");    // パスワード
    $line = fgets($fp, 512);
    if(!preg_match("(OK)", $line)){    // ログイン失敗？
        fclose($fp);
        die("ログイン失敗");
    }

    //-------------------------------
    //受信メール件数取得
    //-------------------------------
    fputs($fp, "STAT\r\n");
    $line = fgets($fp, 512);
    print $line . "<br>";    //STAT結果表示

    //STAT結果を変数に展開
    list($status, $cnt, $size) = explode(' ', $line);

    //受信メール件数チェック
    if(intVal($cnt) == 0){
        print "受信メール無し<br><br>";
    }else{
        print "受信メールあり<br><br>";
    }

    //テスト用に件数制限入れておきます（100件を超える場合は100件まで）
    $cnt = ($cnt > 100 ? 100 : $cnt);

    //-------------------------------
    //受信メール一覧表示
    //-------------------------------
    for($i = 1; $i <= $cnt; $i++){
        //メッセージヘッダ取得
        //「TOP」が使えない環境の人は「RETR」で代用してください
        //fputs($fp, "RETR $i\r\n");
        fputs($fp, "TOP $i 0\r\n");

        do{
            $line = fgets($fp, 512);

            //件名を表示する
            if(preg_match("/^Subject/", $line)){
                $subject = mb_decode_mimeheader($line);
                $subject = preg_replace("/^Subject:/", "", $subject);
                print '<a href="aaMailListDetail?no=' . $i . '" target="blank">';
                print $i . ":" . $subject . "</a><br>";
                //print $i . ":" . mb_convert_encoding($subject, $enc, "auto") . "<br>";
            }
        }while (!preg_match("/^\.\r\n/", $line));
    }

    //切断
    fputs($fp, "QUIT\r\n");
    fclose($fp);
?>