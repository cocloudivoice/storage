<?php





?>
<html>
<head>

<script type="text/javascript">
	function preview(ele) {
	    if (!ele.files.length) return;  // ファイル未選択
	    
	    var file = ele.files[0];
	    if (!/^image\/(png|jpeg|gif)$/.test(file.type)) return;  // typeプロパティでMIMEタイプを参照

	    var img = document.createElement('img');
	    var fr = new FileReader();
	    fr.onload = function() {
	        img.src = fr.result;  // 読み込んだ画像データをsrcにセット
	        img.style = "max-width:100;max-height:100px;";
	        document.getElementById('preview_field').appendChild(img);
	    }
	    fr.readAsDataURL(file);  // 画像読み込み

	    // 画像名・MIMEタイプ・ファイルサイズ
	    
	    document.getElementById('preview_field').innerHTML =
	    '';
	    /*    'file name: ' + file.name + '<br />' +
	        'file type: ' + file.type + '<br />' +
	        'file size: ' + file.size + '<br />';
		*/
	}
</script>

</head>
<body>

<form name="form" method="post" action="http://lemoned.pw/lemon/title_check.php" enctype="multipart/form-data">

	<div id="preview_field">
		<div>
			<span class="name_tag4">
				<img src="<?php echo $title_arr['imgurl']; ?>" alt="preview" class="image-resize" />
			</span>
		</div>
	</div>
	<input id="pre_image" type="file" name="imgurl" class="textarea" style="width:300px" value="<?php echo $title_arr['imgurl']; ?>" onchange="preview(this)" />
</form>
</body>
</html>