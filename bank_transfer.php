<?php
session_start();
require("main_read.php");
//必要なクラスの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$company_con = new company_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
$b = "<br>";
$b2 = "<br><br>";
$user_id = sprintf('%012d',$user_id);
$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
$uid = "apache";
$gid = "apache";
$bank_transfer_path = dirname(__FILE__).'/../files/'.$top4.'/'.$mid4.'/'.$user_id.'/bank_transfer/';

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

//$bank_transfer_pathのフォルダが存在しない場合に作る。
if (!file_exists($bank_transfer_path)) {
	recurse_chown_chgrp($bank_transfer_path, $uid, $gid);
}


//▼銀行振込ファイルの読み込み▼
$table_name = "";
$words1 = "";
$words2 = "";
$file_path = $bank_transfer_path.'bank_transfer.csv';
$bank_transfer_csv = file_get_contents($file_path);
//var_dump($tax_csv);
$bank_transfer_csv = mb_convert_encoding($bank_transfer_csv, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
//var_dump($bank_transfer_csv);
$bank_transfer_arr = explode("\r\n",$bank_transfer_csv);
//$tax_arr = json_decode($tax_json,true);
//var_dump($bank_transfer_arr);echo "<br>";echo $b;echo $b;
//echo $tax_arr["対象外"];
//▲銀行振込ファイルの読み込み▲


function reArrayFiles(&$file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);
    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}

function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
	try {
	while(($file = readdir($d)) !== false) {
		if ($file != "." && $file != "..") {
            $typepath = $mypath . "/" . $file ;
			if (filetype ($typepath) == 'dir') {
                recurse_chown_chgrp ($typepath, $uid, $gid);
			}
            chown($typepath, $uid);
            chgrp($typepath, $gid);
		}
	}
	}catch (Exception $e) {
		echo $e->getMessage();
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
    return $list;
}

?>

<?php require("header.php");?>

<script type="text/javascript">

//Ajaxで処理
function sendByAjax(pst,did,cid,icd) {
		var data = { get_payment_status : pst, destination_id : did, claimant_id : cid, invoice_code : icd};
			send_url = "send_pastjournal";
	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
		type: "POST",
			url: send_url,
		data: data,
		/**
		 * Ajax通信が成功した場合に呼び出されるメソッド
		 */
		success: function(data, dataType) {
			//successのブロック内は、Ajax通信が成功した場合に呼び出される
			
			//PHPから返ってきたデータの表示
			//document.getElementById('Kyoutu_No').innerHTML = data;
			alert(data);
		},
		/**
		 * Ajax通信が失敗した場合に呼び出されるメソッド
		 */
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

			//this;
			//thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

			//エラーメッセージの表示
			alert('Error : ' + errorThrown);
		}
	});
}
</script>

<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>
<style type="text/css">
table#trial_balance { width:1200px;margin-left:auto;margin-right:auto;margin-top:10px;margin-bottom:10px;}
table#trial_balance th { text-align:center;padding:0 10px;}
table#trial_balance td { text-align:center;padding:0 10px;}
table#trial_balance td.num { text-align:right;}
table#trial_balance th.str { text-align: justify;text-justify: inter-ideograph;  -moz-text-align-last: justify;text-align-last: justify;}
table#trial_balance td.str { text-align: justify;text-justify: inter-ideograph;  -moz-text-align-last: justify;text-align-last: justify;}
table.tcenter { margin-left:auto;margin-right:auto;margin-top:20px;}
h3.tcenter { text-align:center;text-decoration:underline;margin-bottom:10px;}
#jounalized_history table th { text-align:center;padding:0 10px;}
#jounalized_history table td { text-align:center;padding:0 10px;}
#jounalized_history table td.num { text-align:right;}
#jounalized_history table th.str { text-align: justify;text-justify: inter-ideograph;  -moz-text-align-last: justify;text-align-last: justify;}
#jounalized_history table td.str { text-align: justify;text-justify: inter-ideograph;  -moz-text-align-last: justify;text-align-last: justify;}

</style>

<title>
<?php
if ( $_POST['mode'] === 'download' ) {
	echo "振込代行 - Cloud Invoice";
} else {
	echo "振込代行 - Cloud Invoice";
}
?>
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';exit;
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article id="jounalized_history">

	<section id="m-1-box">
		<h2>
		<?php
		if ( $_REQUEST['mode'] === 'download') {
			echo "振込代行";
		} else {
			echo "振込代行";
		}
		?>
		</h2>
		<div class="SearchLine"></div>
<?php
			//var_dump($bank_transfer_arr);
			echo '<h3 class="tcenter">振込先一覧</h3>';
			echo '<table id="trial_balance">';
			echo '<col width="300" align="center" span=""><col width="130" align="center" span=""><col width="130" align="center" span=""><col width="400" align="center" span=""><col width="300" align="center" span=""><col width="100" align="center" span=""><col width="130" align="center" span=""><col width="300" align="center" span="">';
			echo '<th class="str">会社名</th><th class="str">金額</th><th class="str">手数料</th><th class="str">銀行</th><th class="str">支店</th><th class="str">種別</th><th class="str">口座番号</th><th class="str">宛名</th>';

			for ($i = 0;$i < count($bank_transfer_arr);$i++) {
				$bt_data_arr = explode(",",$bank_transfer_arr[$i]);
			//	var_dump($bt_data_arr[0]);
				echo '<tr><td class="str">'.$bt_data_arr[0].'</td><td class="num">'.number_format($bt_data_arr[6]).'</td><td class="str">'.$account_title.'</td><td class="str">'.$bt_data_arr[1].'</td><td class="str">'.$bt_data_arr[2].'</td><td class="str">'.$bt_data_arr[3].'</td><td class="num">'.sprintf("%07d",$bt_data_arr[4]).'</td><td class="str">'.$bt_data_arr[5].'</td></tr>';
			}
				echo '<tr><td colspan="">合計</td><td class="num">'.number_format($sum_bab_credit).'</td><td></td><td class="num">'.$sum_debit.'</td><td class="num">'.$sum_credit.'</td><td class="num"></td><td class="num"></td><td class="num"></td></tr>';
			echo '</table>';
?>
			<!--
			<input type="button" id="outcsv" onclick="submit()" value="CSVで出力する"/>
			-->
			<!--<input type="button" id="sba" onclick="sendByAjax('a','b','c','d')" value="Ajax出力" />-->
			<input type="hidden" name="csv" value="csv">
			<input type="hidden" name="mode" value="download">
			<input type="hidden" id="data_num" name="data_num" value="<?php echo $count_data_num;?>" />
			<input type="hidden" id="aj_words" name="aj_words" value="<?php echo $aj_words;?>" />
			<input type="hidden" id="bab_words" name="bab_words" value="<?php echo $bab_words;?>" />
			<input type="hidden" id="cf_words" name="cf_words" value="<?php echo $cf_words;?>" />
		</form>
	</section>

</article>
<div id="fadeLayer" onclick="CloseFrame(12)"></div>
<div id="noneLayer"></div>




<?php require("footer.php");?>
