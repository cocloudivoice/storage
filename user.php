<?php 
session_start();
require("header.php");?>
<?php

//必要なクラスファイルを読み込む
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');

//変数の宣言

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//ユーザーテーブルのコントローラーを呼び出す
$user_con = new user_control();
$user_arr  = $user_con->user_select_id($pdo,$user_id);

//$_SESSION['conf_arr'] = $user_arr;
//var_dump($user_arr);


if(isset($user_arr['nick_name'])){
	$nickname = $user_arr['nick_name'];
}
if(isset($user_arr['user_img'])){
	$image = $user_arr['user_img'];
}
if(isset($user_arr['kana'])){
	$sex = $user_arr['kana'];
}
if(isset($user_arr['profile'])){
	$profile = $user_arr['profile'];
}

if(isset($user_arr['real_name'])){
	$real_name = $user_arr['real_name'];
}

if(isset($user_arr['bank'])){
	$bank = $user_arr['bank'];
}

if(isset($user_arr['siten'])){
	$siten = $user_arr['siten'];
}

if(isset($user_arr['kouza_name'])){
	$kouza_name = $user_arr['kouza_name'];
}
if(isset($user_arr['kouza_type'])){
	$kouza_type = $user_arr['kouza_type'];
}
if(isset($user_arr['password'])){
	$password = $user_arr['password'];
}




?>
<title>ログイン設定</title>
<script type="text/javascript">
	function checkPass() {
		var p1 = form3.password.value;
		var p2 = form3.password2.value;
		
		if (p1 == p2) {
			form3.submit();
			return true;
			
		}else{
			alert('パスワードが一致していません');
			return false;
		}
		
	}

</script>

<script>

function OpenchangeAdFrame(){
	document.getElementById("changeAd").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenchangePassFrame(){
	document.getElementById("changePass").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}
function OpenleaveFrame(){
	document.getElementById("leave").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}

</script>


<iframe id="changeAd" src="./frame/changeAd"></iframe>
<iframe id="changePass" src="./frame/changePass"></iframe>
<iframe id="leave" src="./frame/leave"></iframe>


<article>

	<section id="m-2-box">
		<h2>
			ログイン設定
		</h2>
		
		<table>
			<tr id="first">
				<th>メールアドレス</th>
				<td><?php echo $user_arr["mail_address"];?></td>
				<td class="button"><button onclick="OpenchangeAdFrame()">変更</button></td>
			</tr>
			<tr>
				<th>パスワード</th>
				<td>**********</td>
				<!--
				<form name="form3" method="post" action="./mod_userinfo_up.php" onsubmit="return checkPass();false;"><?php if ($_SESSION['check_temp_password'] != "") {echo "<p class=\"err\">".$_SESSION['check_temp_password']."</p>";} ?>

				<td>現在のパスワード:<input name="temp_password" size="40" maxlength="40" type="password" value="<?php echo $user_arr['password'];?>" /><br/>
				新しいパスワード:<input name="password" size="40" maxlength="40" type="password" /><br/>
				新しいパスワード(再):<input name="password2" size="40" maxlength="40" type="password" /><br/>
				</td>
				<button  onclick="return checkPass();false" name="toroku">変更</button>
				</form>
				-->
				<td class="button"><button onclick="OpenchangePassFrame()">変更</button></td>
			

			</tr>
			<tr>
				<th>退会</th>
				<td></td>
				<td class="button"><button onclick="OpenleaveFrame()">退会</button></td>
			</tr>
		
		
		</table>
		<p id="msg_box" class="red"></p>
 

		<div id="clear"></div>
	</section>



</article>
<?php require("footer.php");?>
<div id="fadeLayer" onclick="CloseFrame(5)"></div>
<div id="noneLayer"></div>
