<?php
//クラスファイルの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$sum_price = 0;
$sales_tax = 0;
$total_price = 0;
$t_price = 0;



//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_data_send";

$destination_id = $_REQUEST['c1'];
$send_code = $_REQUEST['c2'];
$billing_date = $_REQUEST['bd'];
$invoice_code = $_REQUEST['ic'];
$pay_date = $_REQUEST['pd'];
//$insert_date = $_REQUEST['ind'];
$status = $_REQUEST['st'];

//$param = "?destination_id=".$destination_id."&send_code=".$send_code."&billing_date=".$billing_date."&invoice_code=".$invoice_code."&pay_date=".$pay_date."&status=".$status;
//echo $param;

//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['company_id'] != NULL) {
	$company_id = $_REQUEST['company_id'];
}
/*
if ($_REQUEST['c1'] != NULL) {
	$destination_id = $_REQUEST['c1'];
	$words .= " destination_id = ".$destination_id;
}
if ($_REQUEST['c2'] != NULL) {
	$send_code = $_REQUEST['c2'];
	$words .= " send_code = '".$send_code."' ";
}
*/
if ($_REQUEST['st'] != NULL) {
	$status = $_REQUEST['st'];
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND status <> 99 ";
}
/*
if ($_REQUEST['ind'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['ind'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
*/
if ($_REQUEST['bd'] != NULL) {
	$billing_date = $_REQUEST['bd'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pd'] != NULL) {
	$pay_date = $_REQUEST['pd'];
	$words .= " AND pay_date = ".$pay_date;
}

if ($_REQUEST['ic'] != NULL) {
	$invoice_code = $_REQUEST['ic'];
	$words .= " AND invoice_code = '".$invoice_code."' ";
}

//echo $words;


/*
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = $_REQUEST['destination_id'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['send_code'] != NULL) {
	$send_code = $_REQUEST['send_code'];
	$words .= " AND send_code = ".$send_code;
}
if ($_REQUEST['status'] != NULL) {
	$status = $_REQUEST['status'];
	$words .= " AND status = ".$status;
} else {
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
if ($_REQUEST['insert_date'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['insert_date'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
if ($_REQUEST['billing_date'] != NULL) {
	$billing_date = $_REQUEST['billing_date'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pay_date'] != NULL) {
	$pay_date = $_REQUEST['pay_date'];
	$words .= " AND pay_date = ".$pay_date;
}
*/

if (isset($_REQUEST['dlp'])) {
	$flag="download_invoice_data_clm";
	$words = "'".$_REQUEST['dlp']."'";
	//$words = "'".$_REQUEST['dlp']."' AND `claimant_id` = '".$company_id."' ";
}
$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
$flag = "";//フラグの初期化

//var_dump($_REQUEST);
//var_dump($invoice_data_receive_arr);
$data_cnt = count($invoice_data_receive_arr);
//var_dump($invoice_data_receive_arr_count);

//請求元のデータ取得
$flag = "company_data";
$claimant_id = $invoice_data_receive_arr[0]['claimant_id'];
if ($claimant_id == NULL) {
	$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
} else {
	$company_arr = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
}
//var_dump($company_arr);
$flag = "";//フラグの初期化

//請求先のデータ取得
$claimant_id;
//	var_dump($invoice_data_receive_arr[0]['send_code']);

if ($invoice_data_receive_arr[0]['send_code'] != NULL) {
	$flag = "addressee_data_one";
	$send_code = $invoice_data_receive_arr[0]['send_code'];
	$words = " AND company_id = ".$claimant_id." ";
	$destination_company_arr = $company_con -> company_sql_flag($pdo,$flag,$send_code,$words);
	//var_dump($destination_company_arr);
/*
	[0]["client_id"]
	["company_name"]
	["company_name_reading"]
	["addressee"]
	["department"]
	["position"]
	["zip_code"]
	["prefecture"]
	["address1"]
	["address2"]
	["tel_num"]
	["email"]
	["client_company_id"]
	["company_id"]
*/
} else {
	$flag = "company_data";
	$destination_id = $invoice_data_receive_arr[0]['destination_id'];
	$destination_company_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id,$words);
	//var_dump($destination_company_arr);
}
$flag = "";//フラグの初期化











//会社データ
$company_id = $company_arr[0]["company_id"];
$company_name = $company_arr[0]["company_name"];
$company_name_reading = $company_arr[0]["company_name_reading"];
$segment = $company_arr[0]["segment"];
$zip_code = $company_arr[0]["zip_code"];
$prefecture = $company_arr[0]["prefecture"];
$address1 = $company_arr[0]["address1"];
$address2 = $company_arr[0]["address2"];
$tel_num = $company_arr[0]["tel_num"];
$fax_num = $company_arr[0]["fax_num"];
$url = $company_arr[0]["url"];
$email = $company_arr[0]["email"];
$section = $company_arr[0]["section"];
$clerk = $company_arr[0]["clerk"];
$bank_id = $company_arr[0]["bank_id"];
$blanch_id = $company_arr[0]["blanch_id"];
$account_devision = $company_arr[0]["account_devision"];
$account_num = $company_arr[0]["account_num"];
$account_info = $company_arr[0]["account_info"];
$stamp_image = $company_arr[0]["stamp_image"];
$logotype = $company_arr[0]["logotype"];
$remarks1 = $company_arr[0]["remarks1"];
$remarks2 = $company_arr[0]["remarks2"];
//この二つの条件分岐はシェルでの定期的なPDF作成時に、
//相対パスだと画像が出ない現象が起こった為、ここで変換する。
//良い方法があったら修正してください。20150521ハマサキ
//▼パス変換▼
if ($_REQUEST['sh'] == 1) {
	if ($company_arr[0]["stamp_image"] != "") {
		$company_arr[0]['stamp_image'] = str_replace("../../","https://storage.cloudinvoice.co.jp/",$company_arr[0]['stamp_image']);
	}
	if ($company_arr[0]["logotype"] != "") {
		$logo = $company_arr[0]['logotype'] = str_replace("../../","https://storage.cloudinvoice.co.jp/",$company_arr[0]['logotype']);
	//echo "<img src='".$logo."' title='ロゴです' alt='ロゴですよ'/>";
	//echo "<img src='../../files/000000000041/images/logo2.png' title='ロゴです' alt='ロゴですよ'/>";
	}
}
//▲パス変換▲



?>