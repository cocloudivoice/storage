<?php

class editOCRText {
	
	
	function editText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$txt_dir,$file_path,$af_id) {
		//txtの内容を取得して分析し、必要な情報（日付、金額、電話番号1,2,3、会社名、商品名、業種）
		//を取得して上書きする工程
//		for($m = 0;$m < count($img_file_arr);$m++) {
		//初期化
		$exist_flag = 0;
		$claimant_id = "";
		$tel_num = "";
		$tel_num2 = "";
		$tel_num3 = "";
		$got_company_name = "";
		$got_ind = "";
		$goukei = 0;
		$goukei_top = 0;
		$got_amount_of_money = 0;
		$got_date = "";
		$got_date_top = "";
		$searched_company_name = "";
		$search_result_arr = array();
		$searched_company_data_arr = array();
		$product = "";
		$product_name_arr = "";
		$company_name_one = "";
		$company_names = "";
		$company_name_arr = "";
		$industry_type_s = "";
		//初期化
		$tel_cnt = 0;
		$money_cnt = 0;
		$money_num_cnt = 0;
		$date_cnt = 0;
		$date_top_cnt = 0;
		$key_cnt = 0;
		$tel_arr = array();
		$money_arr = array();
		$date_arr = array();
		$keywords_arr = array(); 
		$sum_box = 0;
		$sum_flag = 0;
		$last_receipt_date = "";
		$receipt_flag = 0;
		$pdt_cnt = 0;
		$comp_cnt = 0;
		$data2 = array();

		try {
	    	//var_dump($file);
	        //$filename = $img_file_arr[$m];
			//var_dump($_FILES);
				
			//ファイル名変換処理
//			$filename  = str_replace(array("(",")","~","年","月","日","時","分","秒"),"",$filename );
			$file_name_top = substr($filename , 0, -4);//echo ":top<br>\r\n";
			$file_extension = substr($filename , -4, 4);//echo ":ext<br>\r\n";
			$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
			$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
			$pdf_name = substr($filename, 0, -4);
			
			//請求書が登録されていないか確認する。
			//$flag ="download_invoice_data_clm";
			//$result = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$dlp);
			//同じdownload_passwordで請求書が登録済みの場合
			if (substr($filename, -3, 3) == "png" || substr($filename, -3, 3) == "jpg" || substr($filename, -3, 3) == "tif" || substr($filename, -3, 3) == "pdf"|| substr($filename , -3, 3) == "bmp" ) {
				//請求書登録
				//仮のデータで請求書登録をする。
				//仮の請求書・領収書データの登録
				
				//テキストを読んで上書き処理
				//echo "テキスト処理<br>";
				$text_file_path_en = $txt_dir."/".$dlp."_1.txt";
				$text_file_path = $txt_dir."/".$dlp.".txt";
				//echo $text_file_path = mb_convert_encoding( $text_file_path, "UTF-8", "auto" );
				if(file_exists($text_file_path_en)) {
					$data = file_get_contents($text_file_path_en);
					$data .= file_get_contents($text_file_path);
				} else {
					$data = file_get_contents($text_file_path);
				}
				
				$data_all = $data;
				//$data = mb_convert_encoding($data,"UTF-8","UTF-16LE");

				$data = explode( "\n", $data );
				$cnt = count( $data );
				$data_arr = array();
				

				for ( $i = 0; $i < $cnt; $i++ ) {//echo e;
				
					//変数初期化
					$money_num_cnt = 0;
					
					//全角文字を半角に変換
					//$data[$i] = mb_convert_encoding( $data[$i], "UTF-8", "UTF-16" );
					$data2[$i] = $data[$i];//商品名と会社名の文字列を取得する時は数字を加工しない方が良いため分ける。
					$kana = array("１"=>"1","２"=>"2","３"=>"3","４"=>"4","５"=>"5","６"=>"6","７"=>"7","８"=>"8","９"=>"9","０"=>"0","θ"=>"0");
					$data[$i] = strtr($data[$i], $kana);
					$data[$i] = mb_convert_kana($data[$i], 'aKvrn');
					$data[$i] = str_replace(array("'","'"," ","内","|"),"",$data[$i]);
					$data[$i] = str_replace(array("、"),",",$data[$i]);
					$data[$i] = str_replace(array("叩","m","0c","0C","0(1"),"00",$data[$i]);
					$data[$i] = str_replace(array("刀","力"),"77",$data[$i]);
					$data[$i] = str_replace(array("判"),"\4",$data[$i]);
					$data[$i] = str_replace(array("//","{{","[1"),"11",$data[$i]);
					$data[$i] = str_replace(array("。","。","〝","位","n","ロ","o","U","()","(1"),"0",$data[$i]);
					$data[$i] = str_replace(array("■","ー","]","L"),"1",$data[$i]);
					$data[$i] = str_replace(array("z"),"2",$data[$i]);
					$data[$i] = str_replace(array("會"),"3",$data[$i]);
					$data[$i] = str_replace(array("縄","繍"),"4",$data[$i]);
					$data[$i] = str_replace(array("轟","印","§"),"5",$data[$i]);
					$data[$i] = str_replace(array("s","野"),"6",$data[$i]);
					$data[$i] = str_replace(array("a","讐","B"),"8",$data[$i]);
					$data[$i] = str_replace(array("g","q"),"9",$data[$i]);
					$data[$i] = str_replace(array("事","令","隼","藻","ff"),"年",$data[$i]);
					$data[$i] = str_replace(array("隷","A"),"月",$data[$i]);
					$data[$i] = str_replace(array("B","t1","曰"),"日",$data[$i]);
					$data[$i] = str_replace(array("年明","#明"),"年8月",$data[$i]);
					$data[$i] = str_replace(array("年9A","#9A"),"年9月",$data[$i]);
					$data[$i] = str_replace(array("年坍"),"年1月",$data[$i]);
					$data[$i] = str_replace(array("明旧"),"8月1日",$data[$i]);
					$data[$i] = str_replace(array("¨","－","一","・","~"),"-",$data[$i]);
					//$data[$i] = str_replace(array(",","・","･",".","．","。","口","■","=","\""),"",$data[$i]);
					$data[$i] = str_replace(array("合言 "),"合計",$data[$i]);
					$data[$i] = str_replace(array("合","卦","會"),"合",$data[$i]);
					$data[$i] = str_replace(array("小","′」ヽ"),"小",$data[$i]);
					$data[$i] = str_replace(array("言十","膏十","言t","曹十","叶","富十","貰十","註","8十","1十","言 ","言†","雷十","言十","雷寸","書十"),"計",$data[$i]);
					$data[$i] = str_replace(array("オ寸","本寸"),"村",$data[$i]);
					$data[$i] = str_replace(array("言舌"),"話",$data[$i]);
					$data[$i] = str_replace("」R","JR",$data[$i]);
					$data[$i] = str_replace("西新宿NSGi会","西新宿NSG会",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/5","2015?","2015#"),"2015年",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/6","2016?","2016#","Zo/6年"),"2016年",$data[$i]);
					$data[$i] = str_replace(array("20151年","2051年","20/6","2017?","2017#"),"2017年",$data[$i]);
					$data[$i] = str_replace("F]","円",$data[$i]);
					$data[$i] = str_replace(array("1EL","1E1","Te1","TE1","E1","e1"),"TEL",$data[$i]);
					$data2[$i] = str_replace(array("馬主"),"駐",$data2[$i]);
					$data2[$i] = str_replace(array(".r束日本",".r東日本"),"JR東日本",$data2[$i]);
					$data2[$i] = str_replace(array("食年代"),"食事代",$data2[$i]);
					$data2[$i] = str_replace(array("商年"),"商事",$data2[$i]);
					$data2[$i] = str_replace(array("ゆつメル"),"ゆうメール",$data2[$i]);
					$data2[$i] = str_replace(array("収入5紙"),"収入印紙",$data2[$i]);
					$data2[$i] = str_replace(array("7ット"),"フット",$data2[$i]);
					$data2[$i] = str_replace(array("ライ7"),"ライフ",$data2[$i]);
					$data2[$i] = str_replace(array("SEKthSHO"),"セキショー",$data2[$i]);
					$data2[$i] = str_replace(array("JT8"),"JTB",$data2[$i]);
					$data2[$i] = str_replace(array("ENE0S","ENEOS","ENEoS"),"エネオス",$data2[$i]);
					$data2[$i] = str_replace(array("サ0ン"),"サロン",$data2[$i]);
					$data2[$i] = str_replace(array("DAIS0","DAISO"),"ダイソー",$data2[$i]);
					$data2[$i] = str_replace(array("MINIST0P","MINISTOP"),"ミニストップ",$data2[$i]);
					$data2[$i] = str_replace(array("LAWS0N","LAWSON"),"ローソン",$data2[$i]);
					$data2[$i] = str_replace(array("NewDays"),"ニューデイズ",$data2[$i]);
					$data2[$i] = str_replace(array("70ジャポン"),"フロジャポン",$data2[$i]);
					$data2[$i] = str_replace(array("プ0"),"プロ",$data2[$i]);
					$data2[$i] = str_replace(array("武蔵松山"),"武蔵松山カントリークラブ",$data2[$i]);
					$data[$i] = str_replace(array("¥","$","#","="),"￥",$data[$i]);
					$data[$i] = str_replace(array(":-"),".",$data[$i]);
					
					//echo "<br>";
					//echo $data[$i];
					//echo "<br>";
					//-を用いた日付で時間がスペースの後に時間がついていた場合取得できなくなるため、スペースを消す前に移動した。
					
					$pattern_comp_end = "/^[*]{1}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data[$i] = str_replace(array("*"),"￥",$data[$i]);
					}
					
					//初期加工のみで利用したい場合の変数
					$ori_data = $data[$i];

					
					$pattern_comp_end = "/([0-9]{2,4})[.,- ]{1,2}([0-9]{1,2})[.,- ]{1,2}([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補a<br/>\r\n";
						//var_dump($match);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}

					$data[$i] = str_replace(array(".","．"),"",$data[$i]);
					$pattern_comp_end = "/([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補b<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;

							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					
					$pattern_comp_end = "/([0-9]{2,4}),([0-9]{1,2}),([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補c<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;

							$got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}

					//電話番号用処理
					
					//$tel_test = preg_replace('/[^0-9]/', '', $data[$i]);
					//数字だけ抜き出す処理だと合計金額などと区別しづらいかも。
					$tel_test = str_replace(array("_"),"",$data[$i]);
					if (intval($tel_test) != 0 ) {
						$data[$i] = str_replace(array("(","（","_"),"-",$data[$i]);
					}
					$data[$i] = str_replace(array(" ","　"),"",$data[$i]);
					$data[$i] = str_replace(array("(","（"),"-",$data[$i]);
					$data[$i] = str_replace(array(")","）"),"-",$data[$i]);
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "aaatel<br>\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}[- ]+[0-9]{2,4}[- ]+[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}

					//電話番号取得（番号を取得）
					//echo $data[$i];echo "bbbtel<br>\r\n";
					$pattern_comp_end = "/0{1}[0-9]{9}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10,11}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					//電話番号取得（番号を取得）
					//echo $data[$i];echo "\r\n";
					$pattern_comp_end = "/0{1}[0-9]{1,3}\([0-9]{2,4}\)[0-9]{3,4}/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "<br/>";
						$match_result= str_replace("-","",$match[0]);
						$tel_flag = preg_match("/\d{10}/", $match_result, $result);
						if($tel_flag == 1){
							//echo "電話番号候補<br/>\r\n";
							//echo $match[0];
							if (substr($match[0],0,1) == 1) {
								$match[0] = substr($match[0],1);
							}
							$tel_num = $tel_arr[$tel_cnt] = $match[0];
							//echo "<br/>\r\n";
						}
						$tel_cnt++;
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
					}
					
					if ($tel_cnt == 0) {
						//電話番号取得（番号を取得）
						//echo $data[$i];echo "\r\n";
						$pattern_comp_end = "/[1-9]{1}[0-9]{1,3}-[0-9]{3,4}/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "<br/>";
							$match_result= str_replace("-","",$match[0]);
							$tel_flag = preg_match("/\d{6,8}/", $match_result, $result);
							if($tel_flag == 1){
								//echo "電話番号候補<br/>\r\n";
								$tel_num = $tel_arr[$tel_cnt] = $match[0];
								//echo "<br/>\r\n";
							}
							$tel_cnt++;
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
						}
					}
					
					//echo $data[$i];echo "mncnt:".$money_num_cnt;
					if ($money_num_cnt == 0) {
						//echo $data[$i];
						//$pattern_comp_end = "/合計(\\\w*)/";
						$pattern_comp_end = "/合計(￥\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "合計金額候補a<br/>\r\n";
							$goukei_top = $goukei = $data_arr[$i] = str_replace(array("\\","￥","円","計","合",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//	$goukei = substr($goukei,2);
							$data_arr[$i] = $data[$i];
							$money_cnt++;
							$money_num_cnt++;
						}
					}
					
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/計￥(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}
					
					
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/請求金額(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
							$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

				
					if ($money_num_cnt == 0) {
						$data[$i] = str_replace(array("Y","F","軍"),"\¥",$data[$i]);
						$data[$i] = str_replace(array("D","O","〕"),"0",$data[$i]);
						$data[$i] = str_replace(array("∞"),"00",$data[$i]);
						$data[$i] = str_replace(array("ヮ","i","ぅ","ゥ","′","り",",","#"),"",$data[$i]);
						$data[$i] = str_replace(array("\\"),"￥",$data[$i]);
						$pattern_comp_end = "/(￥\d+)/";
						//$pattern_comp_end = "/(\d+)/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//echo"<br>金額:";echo $data[$i];echo "<br><br>\r\n";
						if ($match_num >= 1) {
							//echo "金額候補\<br/>\r\n";
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥",",","，","。",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";var_dump($money_arr[$money_cnt]);
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					if ($money_num_cnt == 0) {
						//$pattern_comp_end = "/\w*円/";
						$pattern_comp_end = "/[金]*(\d+)円/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
						//echo "<br>金額候補 円";echo $data[$i];var_dump($match);
							$money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円",",","，",".","．","_","＿","‐","ヮ","-","金"),"",$match[0]);
							//echo "\r\n";
							$data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					/*
					if ($money_num_cnt == 0) {
						$pattern_comp_end = "/(\d{3,7})/";
						$match_num = preg_match($pattern_comp_end, $data[$i], $match);
						//echo "金額候補<br/>\r\n";
						$goukei_top = $money_arr[$money_cnt] = $data_arr[$i] = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
						//echo "\r\n";
						//echo "<br>金額候補";
						//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
						$data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$money_cnt++;
						$money_num_cnt++;
						$company_data_end_line = $i;
						if (intval($money_arr[$money_cnt]) > 0) {
							$sum_flag += 1;
							$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
						}
					}
					*/
						//echo $ori_data;echo $money_num_cnt;
						//echo "<br>";
					if ($money_num_cnt == 0) {

						//金額が入っていない時の候補
						$pattern_comp_end = "/([1-9]{1}[0-9]{0,2},[0-9]{3})/";
						$match_num = preg_match($pattern_comp_end, $ori_data, $match);
						if ($match_num >= 1) {
							//echo "金額候補<br/>\r\n";
							$money_arr[$money_cnt] = $ori_data = str_replace(array("\\","￥","円","計",",","，",".","．","_","＿","‐","-","ヮ"),"",$match[0]);
							//echo "\r\n";
							//echo "<br>金額候補";
							//$money_arr[$money_cnt] = substr($money_arr[$money_cnt],2);
							$data_arr[$i] = $ori_data;
							//$start_line = $i + 1;
							//echo "<br/>\r\n";
							$money_cnt++;
							$money_num_cnt++;
							$company_data_end_line = $i;
							if (intval($money_arr[$money_cnt]) > 0) {
								$sum_flag += 1;
								$sum_box[$sum_flag] = intval($money_arr[$money_cnt]);
							}
						}
					}

					
					$data[$i] = str_replace(array("-","_","－","＿","・","･",".","．","`",":"),"",$data[$i]);
					$data[$i] = str_replace(array("午","撃","#"),"年",$data[$i]);
					$data[$i] = str_replace("l","1",$data[$i]);
					//echo "<br/>";
					$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})日/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}
					
					$pattern_comp_end = "/([0-9]{1,4})[年A#]*([0-9]{1,2})月([0-9]{1,2})/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						$data[$i] = str_replace(array("-","_","－",),"",$data[$i]);
						//echo "日付候補d<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$company_data_end_line = $i;
					}

					$data[$i] = str_replace(array("′","ノ",",-",":-"),"/",$data[$i]);//日付用変換
					$pattern_comp_end = "/([0-9]+)\/([0-9]{1,2})\/([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;

						//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}

					$data[$i] = str_replace("/","1",$data[$i]);
					$data[$i] = str_replace("-","",$data[$i]);
					//echo "<br/>";
					$pattern_comp_end = "/([0-9]{1,4})年([0-9]{1,2})月([0-9]{1,2})/";
					//$pattern_comp_end = "/.?*年.?*月.?*日/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//preg_match("/[0-9]+年/", $match[0], $year);
						//preg_match("/[0-9]+月/", $match[0], $month);
						//preg_match("/[0-9]+日/", $match[0], $day);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						$got_date_top[$date_top_cnt] = $got_date = $date_arr[$date_cnt] = $year.$month.$day;
						//echo $got_date = $data_arr[$i] = str_replace(array("年","月","日"),"",$match[0]);
						//echo $data_arr[$i] = $data[$i];
						//$start_line = $i + 1;
						//echo "<br/>\r\n";
						$date_cnt++;
						$date_top_cnt++;
						$company_data_end_line = $i;
					}
					
					$pattern_comp_end = "/([0-9]{2,4}).([0-9]{1,2}).([0-9]{1,2})/";
					$match_num = preg_match($pattern_comp_end, $data[$i], $match);
					if ($match_num >= 1) {
						//echo "日付候補<br/>\r\n";
						//var_dump($match);
						$match[1] = substr($match[1],-2);
						//20を超える数字の場合、平成とみなして-12で西暦に修正する。2020年を超える前にロジックを変えます。
						if ($match[1] >= 20){
							$match[1] = $match[1] - 12;
						}
						
						$year = "20".$match[1];
						$month = sprintf("%02d",substr($match[2],-2));
						$day = sprintf("%02d",substr($match[3],-2));
						if(($month >= 1 && $month <= 12) && ($day >= 1 && $day <= 31)) {
							$got_date = $date_arr[$date_cnt] = $year.$month.$day;
							//echo $got_date = $data_arr[$i] = str_replace("/","",$match[0]);
							//echo $data_arr[$i] = $data[$i];
							//$start_line = $i + 1;
							//echo "<br>\r\n";
							$date_cnt++;
							$company_data_end_line = $i;
						}
					}
					
					if  ($pdt_cnt == 0) {
						//商品名を取得する
						//商品名の為に1をーに変換し直す。
						$data2[$i] = str_replace(array("1"),"ー",$data2[$i]);
						//$product_check_arr = array("食事代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENE0Sヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","定期券","乗車券類","特急券","普通券","飲食代","レストラン","宿泊代","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","回数券","乗車券","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",",*航空便.*","簡易書留",".*切手.*","水道料金","サントリー","チケット代","ゴルフプレー.*代","商品代","品代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車","収入印紙","印紙");
						$product_check_arr = array("食事代","食事","コピー代","収入証紙","評価証明","ヒョウカ証明","証紙",".*住民票",".*特別税","均等割額",".*民税.*","電子定款","謄本",".*旅行券.*","ガソリン","レギュラーガソリン","エネオスヴィーゴ","ENEOSヴィーゴ","プレミアム手洗い",".*プリペイドカード.*","シナジーレギュラー","レギュラー","ハイオク","灯油","軽油","軽油税","ゼアス","ワックス","洗車","代金引換","書籍代","家電製品","通行料金","チャージ","飲食代","レストラン","宿泊代","住民税","源泉所得税","講習","乗車料金","駐車料金","タクシー","ホテル","旅館","宿泊料","カラオケ","焼肉","スナック","Bar","BAR","居酒屋","紅茶","カフェオレ","コーヒー","お茶","固定資産税証明","戸籍附票","原除籍謄抄本","印鑑登録証明書","住民票","寿司","ケーキ","和菓子","洋菓子","菓子パン","菓子","弁当","定食","カフェ","雑貨","航空券","乗船券","運賃","宅急便","宅配便","ゆうパック","ゆうメール",".*はがき.*",".*第一種定形.*",".*書留.*",",*航空便.*","簡易書留",".*切手.*","水道料金","サントリー","チケット代","ゴルフプレー.*代","商品代","品代","更新申請手数料","講習手数料","両替手数料","振込手数料","手数料","修理代","プリカ",".*洗車","収入印紙","印紙");
						$p_cnt = 0;
						$last_product = "";
						$match = "";
						$product_arr = array();
						for ($z = 0;$z < count($product_check_arr);$z++) {
							$pattern_comp_end = "/".$product_check_arr[$z]."/";
							$pattern_comp_end;//echo "<br>";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							//echo "<br>";
							if ($match_num >= 1) {
								$product_arr[$p_cnt] = $match[0];
								//echo "<br>商品候補<br/>\r\n";
								//var_dump($match);
								//echo $data2[$i];
								//echo "<br/>";
								//exit;
								if ($product_arr[$p_cnt] != $last_product) {
									if ($p_cnt != 0) {$space = " ";}
									$last_product = $product_arr[$p_cnt];
									$product_name_arr[$p_cnt] = $space.$product_arr[$p_cnt];
									$p_cnt++;
									if ($match[0] != "収入印紙" ||$match[0] != "印紙" ) {$pdt_cnt++;}
									break;
								}
							}
						}
					}
					//echo $data2[$i];
					if  ($comp_cnt == 0) {
						//会社名を取得する
						$company_check_arr = array("東海旅客","JR東日本","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","セキショー","宇佐美","小田急.*","東京地下鉄","東日本旅客","ケーズデンキ","ベスト電器","ビックカメラ","Express","エネオス","シェル石油","コスモ石油","Unidy","ユニディ","ケーヨーデイツー","オリンピック","loft",".*JTB.*","ロフト","サブウェイ","CAFE de CRIE","ABC-MART","駐車場","駐輪場","bookshelf","くすりの福太郎","リンガーハット","STARBUCKS","スターバックス","コメダ珈琲店","タリーズコーヒー","ドトールコーヒー","エクセルシオール","カフェ・ド・クリエ","ベローチェ","ジェーソン","イトーヨーカドー","ガスト","ロイヤルホスト","デニーズ","ココス","夢庵","サイゼリヤ","ジョイフル","ジョナサン","バーミヤン","新鮮市場","カインズ",".*株式会社.*",".*有限会社.*",".*（株）.*",".*（有）.*",".*公社",".*水道局","マクドナルド","モスバーガー","ファッションセンターしまむら","ジーユー","ユニクロ","UT","アーバンリサーチ","SHIPS","シップス","ヴィクトリアゴルフ","マルエイ","ダイソー","DAISO","(.*市)会計管理者","武蔵松山カントリークラブ","アップルパーク勝どき第1","タイムズ","ユアーズパーク","コインパーク","タイムパーク","パーク２４","八重洲駐車場","マンゴツリー","東海パッセン","未来堂","ステーキてっぺい",".*パーキング","鉄道","東京急行電鉄","阪急鉄道","南海電鉄","阪神電鉄","東急電鉄","東武電鉄","西武鉄道","日本交通","平和交通","西新宿NSG会",".*税理士",".*行政書士",".*司法書士",".*行政書士協同組合",".*税理士会",".*医師会",".*司法書士会","東京電力","東京ガス","法務局","NEXCO.*日本","NEXCO","NTTファイナンス","NTTファイナンス","パーキング","KDDI","ソフトバンク","softbank","SoftBank","国税局",".*税事務所","スターバックス","ファミリーマート","NewDays","ローソン","LAWSON","セブンイレブン","セブン-イレブン","サンクス","ココストア","ミニストップ","ポプラ","スリーエフ",".*ホテル",".*カントリークラブ",".*ゴルフ場",".*ゴルフ倶楽部",".警察","郵便局","関西ガス","関西電力","九州電力","東北電力",".*役場",".*市役所",".*区役所",".*金銭出納員",".*出納員",".*会計管理者",".*法律事務所",".*病院",".*医院",".*歯科医院",".*歯科",".*クリニック",".*外科",".*内科",".*皮膚科",".*耳鼻咽喉科",".*整骨院",".*接骨院","ヤマト運輸","クロネコヤマト","佐川急便","ペリカン便","西濃運輸","赤帽","売捌所","売さばき",".*公証人.*","ヤマトフィナンシャル");
						$p_cnt = 0;
						$last_company = "";
						$match = "";
						$company_arr = array();
						for ($z = 0;$z < count($company_check_arr);$z++) {
							$pattern_comp_end = "/".$company_check_arr[$z]."/";//echo "<br>";
							$match_num = preg_match($pattern_comp_end, $data2[$i], $match);
							//echo "<br>";
							if ($match_num >= 1) {
								$company_arr[$p_cnt] = $match[0];
								//echo "<br>会社名候補<br/>\r\n";
								//var_dump($match);
								//echo $data2[$i];
								//echo "<br/>";
								if ($company_arr[$p_cnt] != $last_company) {
									if ($p_cnt != 0) {$space = " ";}
									$last_company = $company_arr[$p_cnt];
									$company_name_arr[$p_cnt] = $space.$company_arr[$p_cnt];
									$p_cnt++;
									$comp_cnt++;
									break;
								}
							}
						}
					}
					//echo $data[$i];echo " ".$i."<br/>";
				}
				
				//情報の整理
				$got_amount_of_money = 0;
				$max_num_times = 0;
				$fixed_flag = 0;
				$max_box = 0;
				//echo "<br>合計トップ：";echo $goukei_top;echo "<br>".$money_cnt;
				if ($goukei_top == 0) {
					//echo "金額候補を絞る";
					//計を取得できた場合、その最大値を合計と見做す。
					if ($sum_flag >= 1) {
						for ($j = 0;$j < $sum_flag;$j++) {
							if ($max_box <= $sum_box[$j]) {
								$max_box = $sum_box[$j];
							}
						}
	//					echo "z<br>";
						$got_amount_of_money = $max_box;
						$fixed_flag = 1;
					}
					//var_dump($money_arr);
					//echo $goukei;echo "合計<br/>";
					if ($goukei == 0) {
						for ($n = 0;$n < count($money_arr);$n++){
							$temp_money = intval($money_arr[$n]);
							if($fixed_flag == 0) {
								if ($temp_money != 0) {
									//echo "<br/>".$n."tmpmoney".$temp_money."<br>";
									//直後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
									if ($n >= 2) {
										$after2 = abs($temp_money - intval($money_arr[$n-1]));
										if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
											//echo "a<br>";
											$got_amount_of_money = intval($money_arr[$n-2]);
											$fixed_flag = 1;
										}
									}
									
									

									//直前の2つの金額を足したものと同額であれば合計と見做す。(小計と外税と合計)
									if ($n >= 2) {
										$before2 = intval($money_arr[$n-1]) + intval($money_arr[$n-2]);
										if ($before2 == $temp_money && $before2 != 0) {
											//echo "c<br>";
											$got_amount_of_money = $temp_money;
											$fixed_flag = 1;
										}
									}
									
									//直後の1つを飛ばしたその後2つの金額の差の絶対値と同額であれば合計と見做す。
									if ($n >= 3) {
										$after2 = abs($temp_money - intval($money_arr[$n-1]) && $after2 != 0);
										if ($after2 == intval($money_arr[$n-3])) {
											//echo "d<br>";
											$got_amount_of_money = intval($money_arr[$n-3]);
											$fixed_flag = 1;
										}
									}
									if($fixed_flag == 0) {
										$num_of_same = 0;
										$candidate_box = 0;
										//var_dump($money_arr);
										//echo			count($money_arr);
										//同じ数字が2回以上あるものを取得するロジック
										for ($h = 0;$h < count($money_arr);$h++){
											//echo "<br/>tmp2 temp:";
											$temp_money2 = intval($money_arr[$h]);
											//echo " 2回";echo $temp_money;
											//if ($temp_money == $temp_money2){echo "同じ？？<br>";}
											if ($temp_money == $temp_money2) {
												$num_of_same++;
											}
										}
										//echo "<br>回数：".$num_of_same;
										//2回以上のものの中から回数が最大のもの
										if ($num_of_same >= 2) {

											//2回以上のものの中で最大の金額
											if($got_amount_of_money < $temp_money) {
												//echo "e<br/>";
												$got_amount_of_money = $temp_money;
											}
										}
									}
								}
							}
							
							//▼お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▼
							//直後の2つの金額の上の金額から下の金額を引いた額と同額であれば合計と見做す。（合計と預かりとお釣り）
							if ($n >= 2) {
								//echo $n;echo " b－2のとこ ";
								//var_dump($money_arr);
								//echo intval($money_arr[$n-1])." ".$temp_money." ".$money_arr[$n]."<br/>";
								$after2 = intval($money_arr[$n-1]) - $temp_money;
								if ($after2 == intval($money_arr[$n-2]) && $after2 != 0) {
									//echo "b-2<br>";
									$got_amount_of_money = intval($money_arr[$n-2]);
									$fixed_flag = 1;
								}
							}

							
							//一つ飛ばした後の2つの金額の差の絶対値と同額であれば合計と見做す。（合計と預かりとお釣り）
							if ($n >= 3) {
								//echo $n;echo " bのとこ ";echo $temp_money."<br/>";
								//echo intval($money_arr[$n-1])."<br/>";
								$after3 = abs($temp_money - intval($money_arr[$n-1]));
								if ($after3 == intval($money_arr[$n-3]) && $after3 != 0) {
									//echo "b<br>";
									$got_amount_of_money = intval($money_arr[$n-3]);
									$fixed_flag = 1;
								}
							}
							//▲お釣りが最後に来ることが多く、可能性が高いが他のものが成立する方が早い場合が多いので、fixedの後に置いてみる。▲

							
						}
					} else {
						$got_amount_of_money = $goukei;
					}
				} else {
					//echo "top";
					$got_amount_of_money = $goukei_top;
				}
				//入れる金額が無かった場合、一番上の金額を入れる。
				if ($got_amount_of_money == 0 ) {
					//echo "f<br>";echo $money_arr[0];
					$got_amount_of_money = intval($money_arr[0]);
				}
				//exit;
				//echo "<br/>";echo $got_amount_of_money." 合計<br/><br/>";//exit;
				//var_dump($date_arr);
				//日付
				if ($got_date_top[0] == "") {
					for ($d = 0;$d < count($date_arr);$d++){
						//候補の中の一番最初のものを日付にする。（暫定）
						
						//echo "<br/>日付：";
						//echo strtotime($date_arr[$d]);
						//echo "<br>";
						//echo strtotime(date('Ymd'));
						//echo "<br>";
						if (strtotime($date_arr[$d]) <= strtotime(date('Ymd'))) {
							//echo $date_arr[$d];
							//echo substr($date_arr[0],0,4);
							//echo "<br>";
							//echo date('Y',strtotime("+1 year"));
							//echo substr($date_arr[$d],0,4);echo "<br>";
							//echo date('Y',strtotime("+3 year"));echo "<br>";
							While (substr($date_arr[$d],0,4) < date('Y',strtotime("+2 year")) && substr($date_arr[$d],0,4) >= date('Y',strtotime("-2 year"))) {
								$got_date = $date_arr[$d];
								//echo "<br>";
								break;
								if ($d = count($date_arr) - 1) {
									break;
								}
							}
						}
						//echo "<br/>";
						//echo "\r\n";
					}
				} else {
					
					//for ($dtcnt = 0;$dtcnt < count($got_date_top);$dtcnt++) {
					//	$got_date = $got_date_top[$dtcnt];
					//}
					$got_date = $got_date_top[0];
					
				}
				//var_dump($tel_arr);
				//電話番号
				$exist_flag = 0;
				$comp_name = "";
				if (count($tel_arr) > 0) {
					$tel_arr_num = count($tel_arr);
				} else {
					$tel_arr_num = 1;
				}
				//var_dump($tel_arr_num);
				for ($t = 0;$t < $tel_arr_num;$t++){
					//echo "tel選択：";
					if (substr($tel_arr[$t],0,1) == 0) {
					//echo "<br>電話番号：";
						$tel_num = $tel_arr[$t];
					//echo "<br>";
					}

////////電話番号会社名検索▼なくてもいい。ない方が速い。

					if ($exist_flag == 0) {
						//企業データ取得▼
						//電話番号が無かった場合、検索キーワードテーブルから電話番号で存在確認
						if($exist_flag == 0) {
							//echo "電話キーワード検索A<br/>";
							//検索キーワードテーブルからキーワードで検索
							$flag = "search_keywords";
							$keywords = $tel_num;
							$search_result_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,$keywords,$aj_words2);
							if (isset($search_result_arr[0]['company_name']) && $search_result_arr[0]["company_name"] != "") {
								//echo "TELキーあり<br/>\r\n";
								$comp_name = $search_result_arr[0]["company_name"];
							} else{
								/*
								//電話番号が企業テーブルにも検索キーワードにもない場合、検索キーワードテーブルからキーワードで存在確認								
								//全キーワードでテキストデータの全文検索をかけて企業名を絞り込み、キーワード企業名検索に渡す。
								//キーワードが増えると負荷が大きくなるため、あとでキーワードの絞り込みをする処理を考える必要がある。2015/09/15ハマサキ
								//echo "キーワード検索<br/>";
								$flag = "search_keywords_free";
								$search_result_arr = $keywords_con -> search_keywords_sql_flag($pdo_aj,$flag,NULL,NULL);
								$flag = "";
								//var_dump($search_result_arr);
								for($k = 0;$k < count($search_result_arr);$k++) {
									
									$keywords = $search_result_arr[$k]["key_words"];
									//echo "<br/>";
									$pattern_comp_end = "/".$keywords."/imu";
									$match_num = preg_match($pattern_comp_end, $data_all, $match);
									if ($match_num > 0) {
										if ($search_result_arr[$k]["company_name"] != "") {
											$comp_name = $search_result_arr[$k]["company_name"];
											//echo "<br/>";
										}
									}
								}
								*/
							}

							//echo "名前確認<br/>\r\n";
							$flag = "company_search_data_from_name";
							$words = "";
							if (isset($comp_name) && $comp_name != "") {
//								$searched_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$comp_name,$words);
								//var_dump($searched_company_data_arr);
								if (isset($searched_company_data_arr[0]['company_name'])) {
									//echo "名前あり<br/>\r\n";
									$exist_flag = 1;
								} else{
									//echo "名前なし<br/>\r\n";
								}
							}

						}
						
						if (isset($searched_company_data_arr[0]['company_name'])) {
							//キーワード検索で企業データ取得できた場合
							//echo "名前あり下<br/>\r\n";
							$got_company_name = $searched_company_name = $searched_company_data_arr[0]['company_name'];
							//	echo "<br/>\r\n";
							$claimant_id = sprintf('%012d',$searched_company_data_arr[0]["company_id"]);
							//echo "<br/>\r\n";
							$industry_type_s = $searched_company_data_arr[0]["industry_type_s"];
							//echo "<br/>\r\n";
							//$dlp;
							//$flag = "up_invoice_download_password";
							//$set_sql = "`claimant_id` = '".$claimant_id."' WHERE `download_password` = '".$dlp."' AND `claimant_id` = '009999999999'";
							//$res_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$set_sql);
							//var_dump($res_arr);

						} else {
							//echo "名前なし <br/>マピオン<br/>\r\n";
							//電話番号でも取得できなかった場合、マピオンで電話番号検索をして、情報があれば会社を作って渡す。
							/*
							//マピオン
							$tel_test = preg_replace('/[^0-9]/', '', $tel_num);
							$tel_get_file = file_get_contents("http://www.mapion.co.jp/phonebook/M01010/13116/G".$tel_test."-001/");
							//var_dump($tel_get_file);
							$pattern_tel_search = '/ content="(.+?),地図/imu';
							
							preg_match($pattern_tel_search, $tel_get_file, $match_tel3);
							//var_dump( $match_tel3);
							$com_res_arr = explode(",",$match_tel3[1]);
							echo  $com_name_data = $com_res_arr[0];//社名
							//echo "<br/>";
							echo $match_ind[1] = $com_res_arr[2];//業種

							echo "<br>電話帳ナビ";
							$tel_get_file_nav = file_get_contents("https://www.telnavi.jp/phone/".str_replace("-","",$tel_num));
							//var_dump($tel_get_file_nav);
							$pattern_tel_search = '/data-hashtags="電話帳ナビ" data-text=\"(.+?)\">/imu';
							$pattern_ind_type = '/<td class="longtext"><a href=".+?">(.+?)<\/a>/imu';
							preg_match($pattern_tel_search, $tel_get_file_nav, $match_tel2);
							preg_match($pattern_ind_type, $tel_get_file_nav, $match_ind2);
							
							$com_name_data = explode(" ",$match_tel2[1]);//社名
							echo $com_name_data = $match_tel2[1];
							//echo "<br/>";
							echo $match_ind[1] = $match_ind2[1];//業種
							*/
							
							
							$options = array(
							  'http' => array(
							    'method' => 'GET',
							    'header' => 'User-Agent: DoCoMo/2.0 P903i(c100;TB;W24H12)',
							  ),
							);
							$context = stream_context_create($options);
							
//							if ($new_company_name == "") {
								//echo "<br>ヤフーロコ";echo "http://search.loco.yahoo.co.jp/search?p=".$tel_num;
								$tel_get_file = file_get_contents("http://search.loco.yahoo.co.jp/search?p=".$tel_num, false, $context);
								$pattern_tel_search = '/class="ic"><\/span>(.+?)<\/a>/imu';
								$pattern_ind_type = '/p class="gnr">(.+?)<\/p>/imu';
								preg_match($pattern_tel_search, $tel_get_file, $match_tel);
								preg_match($pattern_ind_type, $tel_get_file, $match_ind);
								//echo "<br>tel_test▼<br>";
								//var_dump($match_tel);
								//echo "<br/>";
								//echo $match_ind[1];//業種
								//echo "<br>tel_test▲<br>";
								if ($match_ind[1] != "") {
									$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_ind[1]);
								}
								$com_name_data_arr = explode(" ",$match_tel[1]);//社名
								$new_company_name = $com_name_data_arr[0];
								//$industry_type_s = $match_ind[1];//業種
								if($new_company_name != "") {
									$searched_company_name = $got_company_name = $new_company_name;
								}
//							}

							if ($new_company_name == "") {
								$telephone_num = str_replace("-","_",$tel_num);
								//echo "<br>jpnumber";echo "http://www.jpnumber.com/numberinfo_".$telephone_num.".html";
								$tel_get_file = file_get_contents("http://www.jpnumber.com/numberinfo_".$telephone_num.".html", false, $context);
								//var_dump($tel_get_file);
								$pattern_tel_search = '/td class="autonewline">(.+?)<\/td>/imu';
								//$pattern_ind_type = '/td class="autonewline">(.+?)<\/td>/imu';
								preg_match_all($pattern_tel_search, $tel_get_file, $match_tel);
								//preg_match_all($pattern_ind_type, $tel_get_file, $match_ind);
								//echo "<br>tel_test▼<br>";
								//echo $match_tel[1][0];
								//echo $match_tel[1][1];
								if ($match_ind[1] != "") {
									$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_tel[1][1]);
								}
								//var_dump($com_name_data_arr);
								$new_company_name = $match_tel[1][0];//社名
								$industry_type_s = $match_tel[1][1];//業種
								if($new_company_name != "") {
									$searched_company_name = $got_company_name = $new_company_name;
								}
							}


							//電話帳ナビテスト
							if ($new_company_name == "") {
								//echo "<br>電話帳ナビ";echo "https://www.telnavi.jp/phone/".str_replace("-","",$tel_num);
								$tel_get_file_nav = file_get_contents("https://www.telnavi.jp/phone/".str_replace("-","",$tel_num), false, $context);
								//var_dump($tel_get_file_nav);
								$pattern_tel_search = '/data-hashtags="電話帳ナビ" data-text=\"(.+?)\">/imu';
								$pattern_ind_type = '/<td class="longtext"><a href=".+?">(.+?)<\/a>/imu';
								preg_match($pattern_tel_search, $tel_get_file_nav, $match_tel2);
								preg_match($pattern_ind_type, $tel_get_file_nav, $match_ind2);
								//echo "<br>tel_test▼<br>";
								//var_dump($match_tel);
								//echo "<br/>";
								//echo $match_ind[1];//業種
								//echo "<br>tel_test▲<br>";
								if ($match_ind[1] != "") {
									$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_ind[1]);
								}
								$com_name_data_arr = explode(" ",$match_tel[1]);//社名
								$new_company_name = $com_name_data_arr[0];
								//$industry_type_s = $match_ind[1];//業種
								if($new_company_name != "") {
									$searched_company_name = $got_company_name = $new_company_name;
								}
								//電話帳ナビテスト
							}

							if ($new_company_name == "") {
								$telephone_num = str_replace("-","",$tel_num);
								//echo "<br>japhones";echo "http://www.japhones.com/phones/".$telephone_num.".html";
								$tel_get_file = file_get_contents("http://www.japhones.com/phones/".$telephone_num.".html", false, $context);
								//var_dump($tel_get_file);
								$pattern_tel_search = '/<td>(.+?)<a href="#phonedetailsubmit"/imu';
								//$pattern_ind_type = '/td class="tit">業&nbsp;&nbsp;種<\/td>[\n\s\t]{0,10}<td>(.+?)<\/td>"/imu';
								//$pattern_ind_type = '/td class="tit">(.+?)<\/td>"/imu';
								preg_match_all($pattern_tel_search, $tel_get_file, $match_tel);
								//preg_match_all($pattern_ind_type, $tel_get_file, $match_ind);
								//echo "<br>tel_test▼<br>";
								//var_dump($match_tel);
								//var_dump($match_ind);
								//echo $match_tel[0][0];
								//echo $match_tel[0][1];
								if ($match_ind[1] != "") {
									$industry_type_s = $got_ind = str_replace(array("&nbsp;"),"",$match_tel[0][1]);
								}
								//var_dump($com_name_data_arr);
								$new_company_name = $match_tel[1][0];//社名
								//$industry_type_s = $match_tel[1][1];//業種
								if($new_company_name != "") {
									$searched_company_name = $got_company_name = $new_company_name;
								}
							}

							
							
							//会社の情報が存在したら新しい会社のユーザーを作る。
							if ($new_company_name != NULL || $new_company_name != "") {
								//echo "請求側の企業が存在しない時の処理<br/>\r\n";
								//仮メールアドレス生成
								$mail_address = md5($this -> random(12)).rand(4)."@user.cloudinvoice.co.jp";
								$user_con = new user_control();
								//Emailアドレスの存在を確認する。		
								//$login_check_arr = $user_con -> user_select_email($pdo,$mail_address);

								if ($login_check_arr != NULL) {
									//$login_arr = $user_con -> user_select($pdo,$mail_address);

									//$passwordmaker = $this -> random(12);
									//$passwordmaker = uniqid();
									$passwordmaker = "1234abcd";
									$password = $login_arr['password'] = md5($passwordmaker);
									//$login_arr['nick_name'] = $user_name;
									//$user_con -> user_update($pdo,$login_arr);
									$update_data = " `password` = '".$password."' ";
									//$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

								}

								//$user_arr = $user_con -> user_select($pdo,$mail_address);

								//ユーザー登録
								$user_arr['nick_name']   = $new_company_name;
								$user_arr['mail_address'] = $mail_address;
								$user_arr['tel_num'] = $tel_num;
								$user_arr['non_regular_flag'] = 2;//$non_regular_flag;
								//共通コードの生成と取得
								if (!$user_arr['user_id']){
									$user_num = $user_con -> user_get_new_num($pdo);
									$user_arr['user_id'] = $user_num;
								}
								//$result = $user_con -> user_insert($pdo,$user_arr);

								//企業データ登録▼
								$claimant_id = "".sprintf('%012d',$user_arr['user_id']);
								$sql = 'SELECT * FROM `COMPANY_SEARCH_TABLE` WHERE `company_id` = '.$user_arr['user_id'].' AND `company_name` = "'.$user_arr['nick_name'].'" ;';
								$company_arr = $company_con -> company_sql($pdo,$user_arr['user_id'],$sql);
								$com_exists_flag = $company_arr['chk']['select_count'];
								if ($com_exists_flag == 0) {
								$sql = '
									INSERT INTO `COMPANY_SEARCH_TABLE` ( 
										`company_id`, `company_name`, `company_name_reading`, `zip_code`, `prefecture`, `address1`, `address_full`,`email`, `tel_num`, `non_regular_flag`, `industry_type_s`
									) VALUES ( 
										'.$user_arr['user_id'].', "'.$user_arr['nick_name'].'","'.$company_name_reading.'", "'.$zip_code.'", "'.$prefecture.'", "'.$address1.'", "'.$address_full.'", "'.$user_arr['mail_address'].'","'.$user_arr['tel_num'].'","'.$user_arr['non_regular_flag'].'","'.$industry_type_s.'"
									)';
								}
								$company_arr = $company_con -> company_sql($pdo,$claimant_id,$sql);
								//企業データ登録▲
								
								//▼自動仕訳用企業DB登録▼
								//企業品目テーブルと企業用レコードテーブルを登録
								/*
								//$table_name = "".sprintf("%012d", $claimant_company_id);
								//$table_name = "CUSTOMER_JOURNALIZE_TABLE";
								try {
									$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
									$company_num++;
								} catch (PDOException $e) {
								    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
								    $_SESSION['error_msg'] = $e -> getMessage();
								}
								*/
								//▲自動仕訳用企業DB登録▲
								$exist_flag = 1;
							}
							//echo "名前照合\r\n";
							//企業データ取得▲

						}
					}
					//企業データ取得▲
////////電話番号会社名検索/////▲なくてもいい

					
					
				}//for文
			}//if ( ==png)

			//echo "<br>会社名新:";
			//echo $got_company_name;
			//echo "<br>業種";
			//echo $got_ind;
			//echo "<br>";
			// ファイルのパスを変数に格納
			//echo "<br>ここから";
			$FileText = $file_path.$dlp.'.txt';
			//echo "<br>".$FileText."<br>ここまで";
			//商品名を取り出す
			if (count($product_name_arr) > 0) {
				for ($n = 0;$n < count($product_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$product .= $sp.$product_name_arr[$n];
				}
				//$product_name = $searched_company_name." ".$product;
				$product_name = $product;
			} else {
					$product_name = $searched_company_name;
			}
			//会社名を取り出す
			if (count($company_name_arr) > 0) {
				for ($n = 0;$n < count($company_name_arr);$n++ ) {
					if ($n != 0) {$sp = "";}
					$company_name_one .= $sp.$company_name_arr[$n];
				}
				//$company_names = $searched_company_name." ".$product;
				$company_names = $company_name_one;
			} else {
					$company_names = $searched_company_name;
			}

			
			//日付が未取得の場合の処理
			if ($got_date == "" || date('Ymd',strtotime($got_date)) == "20160101") {
				if ($last_receipt_date != "") {
					$got_date = date('Ym',strtotime($last_receipt_date))."01";
				} else {
					$got_date = date("Y",time())."0101";
				}
			} else {
				//日付が取得できていた場合に前回のレシートの日付として保存する。
				$last_receipt_date = $got_date;
			}
			//電話番号が複数取得できた場合に候補1,2,3がかぶらないようにする処理
			if ($tel_num == $tel_arr[0]) { $tel_num2 = $tel_arr[1];$tel_num3 = $tel_arr[2];} else if ($tel_num == $tel_arr[1]) { $tel_num2 = $tel_arr[0];$tel_num3 = $tel_arr[2];} else { $tel_num2 = $tel_arr[0];$tel_num3 = $tel_arr[1];}
			//if ($searched_company_name == "" || $searched_company_name == "郵便局" ) {$searched_company_name = $company_names;}
			if ($company_names == "") {$company_names = $searched_company_name;}
			if ($industry_type_s == "預入") {$industry_type_s = "";}
			
			//業種に下記の配列内のものがあった場合、商品名を変換する。
			$ind_type_check_arr = array("ケーキ","カフェ","ファミレス","ファストフード","居酒屋","宴会場","ダイニングバー","イタリア料理","ラーメン","うどん","丼もの","弁当","ステーキ","ハンバーグ");
			for ($z = 0;$z < count($ind_type_check_arr);$z++) {
				$pattern_comp_end = "/".$ind_type_check_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $industry_type_s, $match);
				if ($match_num > 0) {
					if ($ind_type_check_arr[$z] == "居酒屋"||$ind_type_check_arr[$z] == "ダイニングバー") {
						$product_name = "居酒屋";
					} else {
						$product_name = "飲食代";
					}
					break;
				}
			}
			//鉄道会社
			$cname_conv_arr = array("東海旅客","JR東日本","京王電鉄","西日本旅客","東京メトロ","京浜急行電鉄","東京地下鉄","東日本旅客","近畿日本鉄道");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $company_names, $match);
				if ($match_num > 0) {$company_names = "鉄道会社";}
			}
			
			//コンビニ等店舗ぬき
			$cname_conv_arr = array("セブンイレブン","ファミリーマート","ニューデイズ","スリーエフ","ローソン","NewDays","ローソン","LAWSON","サンクス","ポプラ","ジェーソン","高島屋","伊勢丹","マルエイ","マルエツ","ミニストップ","くすりの福太郎","ダイソー","DAISO","ファッションセンターしまむら","ジーユー","ユニクロ","ドン・キホーテ");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $company_names, $match);
				if ($match_num > 0) {
					$company_names = $cname_conv_arr[$z];
					if ($product_name = "コンビニ") {$product_name = "";$industry_type_s = "";}
				}
			}

			
			//タクシー
			$cname_conv_arr = array("基本運賃","乗車料金","タクシー");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $product_name.$industry_type_s, $match);
				if ($match_num > 0) {$company_names = "タクシー";$product_name = "";$industry_type_s = "";}
			}
			
			//
			$cname_conv_arr = array("駐車料金");
			for ($z = 0;$z < count($cname_conv_arr);$z++) {
				$pattern_comp_end = "/".$cname_conv_arr[$z]."/";
				$match_num = preg_match($pattern_comp_end, $product_name, $match);
				if ($match_num > 0) {$company_names = "駐車場";$product_name = "";}
			}


			//書店
			$pattern_comp_end = "/書店/";
			$match_num = preg_match($pattern_comp_end, $industry_type_s, $match);
			if ($match_num > 0) {$product_name = "書籍";}
			
			$company_names = str_replace(array("有限会社","株式会社","（株）","（有）","未登録 &nbsp;","金銭出納員","出納員"),"",$company_names);
			
//$product_name = $industry_type_s;
			//商品名検索の結果が無かった場合、業種を商品名に入れる
			//if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "第一種定型" || $product_name == "簡易書留") {$product_name = str_replace(array("第一種定型外","簡易書留"),"郵便",$product_name);} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
			if ($product_name == "") {$product_name = $industry_type_s;} else if ($product_name == "タクシー" || $product_name == "駐車場") {$company_names = $product_name;$product_name = "";}
			//代金引換用の処理//間違えやすいものにアラートを出すため
			if ($company_names == "ヤマトフィナンシャル") {$company_names = "代金引換";}
			if ($company_names == "売捌所" ||$company_names == "売さばき") {$company_names = "法務局";}
			if ($product_name == "プリカ") {$product_name = "プリペイドカード";}
			//テキスト化ファイルの末尾に日付,金額,電話番号1，2，3,店舗名,業種をカンマ区切りで追加する。
			$AddWords = "\r\n".date('Y/m/d',strtotime($got_date)).",".$got_amount_of_money.",".str_replace("-","",$tel_num).",".str_replace("-","",$tel_num2).",".str_replace("-","",$tel_num3).",".$company_names.",".$product_name.",".$industry_type_s;
			//echo "<br>";
			file_put_contents($FileText, $AddWords, FILE_APPEND);
			$outcode = "SHIFT-JIS";
			$incode = "UTF-8";
			$nl = "\r\n";
			$this ->convertCode($FileText , $incode, $FileText , $outcode, $nl);echo "end";
			//$outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);
			
			// ファイルを出力する
			//readfile($FileText);
			//exit;
		} catch (Exception $e) {
			echo $e -> getMessage();
		}//try-catch節の終わり
			//取得したTEXTファイルを削除する。
	//		shell_exec("rm -f ".$file_path.$dlp.".txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp."_1.txt 2>&1");
	//		shell_exec("rm -f ".$file_path.$dlp.".tif 2>&1");
			//$counter++;
			return $FileText;
//		}//for文の終わり
	}//メソッドの終わり

	/**
	* テキストファイルの文字コードを変換し保存する
	* @param string $infname  入力ファイル名
	* @param string $incode   入力ファイルの文字コード
	* @param string $outfname 出力ファイル名
	* @param string $outcode  出力ファイルの文字コード
	* @param string $nl       出力ファイルの改行コード
	* @return string メッセージ
	*/
	function convertCode($infname, $incode, $outfname, $outcode, $nl) {
	    $instr = @file_get_contents($infname);
	    if ($instr == FALSE) {
	        return "変換失敗：{$infname} が見あたりません．";
	    }
	    $outstr = mb_convert_encoding($instr, $outcode, $incode);
	    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

	    $outfp = fopen($outfname, 'wb');
	    if ($outfp == FALSE) {
	        return "変換失敗：{$outfname} に書き込むことができません．";
	    }
	    fwrite($outfp, $outstr);
	    fclose($outfp);

	    return "変換成功：{$infname} => {$outfname}";
	}

	function reArrayFiles(&$file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}


	function recurse_chown_chgrp($mypath,$uid, $gid) {
		if (mkdir( $mypath, 0775, true ) ) {
			chmod( $mypath, 0775);
			chown( $mypath, $uid);
		    chgrp( $mypath, $gid);
		  //echo "ディレクトリ作成成功！！";
		} else {
		  //echo "ディレクトリ作成失敗！！";
		}
	} 

	function getFileList($dir) {
	    $files = glob(rtrim($dir, '/') . '/*');
	    $list = array();
	    foreach ($files as $file) {
	        if (is_file($file)) {
	            $list[] = $file;
	        }
	        if (is_dir($file)) {
	            $list = array_merge($list, getFileList($file));
	        }
	    }
	 
	    return $list;
	}

	function random($length = 12) {
	    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
	    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
	}
}//classの終わり

?>