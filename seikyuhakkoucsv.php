<?php
	session_start();

	//headerで読み込んでいないinvoicedatacontrolクラスを読み込む
		include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
		include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/invoicedatacontrol.class.php');
		include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

		//データベースに接続
		$dbpath_con = new dbpath();
		$pdo = $dbpath_con -> db_connect();
		$invoice_data_con = new invoice_data_control();
		$company_con = new company_control();

	//変数の宣言
	$flag = "";//フラグの初期化
	$words = "";//条件をここに入れる。
	$send_count = 0;//送信数の初期化

//var_dump($_REQUEST);

//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_data_send";
//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}

//CSVフォーマット
if ($_REQUEST['format'] != NULL) {
	$format = htmlspecialchars($_REQUEST['format'],ENT_QUOTES);
}

if ($_REQUEST['send_type'] != NULL) {
	$send_type = htmlspecialchars($_REQUEST['send_type'],ENT_QUOTES);
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = htmlspecialchars($_REQUEST['destination_id'],ENT_QUOTES);
	$words .= " AND destination_id = ".$destination_id;
}
//送付先コードで分ける
if ($_REQUEST['send_code'] != NULL) {
	$send_code = htmlspecialchars($_REQUEST['send_code'],ENT_QUOTES);
	$words .= " AND send_code = '".$send_code."' ";
}

if (($_REQUEST['status'] == 1 && $_REQUEST['status_1'] == 1) || ($_REQUEST['status'] == 0 && $_REQUEST['status_1'] == 0)) {
//	$status = $_REQUEST['status'];
	$words .= " AND status <> 99 ";
} else if ($_REQUEST['status'] == 1) {
//	$status = $_REQUEST['status'];
	$words .= " AND status = 0 ";
} else if ($_REQUEST['status_1'] == 1) {
//	$status = $_REQUEST['status_1'];
	$words .= " AND status = 1 ";
}
if ($_REQUEST['insert_date_from'] != NULL) {
	$insert_date_from = str_replace(" ","",substr($_REQUEST['insert_date_from'],0,4)."/".substr($_REQUEST['insert_date_from'],4,2)."/".substr($_REQUEST['insert_date_from'],6,2));
	$words .= " AND insert_date >= ".$insert_date_from;
}
if ($_REQUEST['insert_date_to'] != NULL) {
	$insert_date_to = str_replace(" ","",substr($_REQUEST['insert_date_to'],0,4)."/".substr($_REQUEST['insert_date_to'],4,2)."/".substr($_REQUEST['insert_date_to'],6,2));
	$words .= " AND insert_date <= ".$insert_date_to;
}
if ($_REQUEST['billing_date_from'] != NULL) {
	$billing_date_from = htmlspecialchars($_REQUEST['billing_date_from'],ENT_QUOTES);
	$words .= " AND billing_date >= ".$billing_date_from;
}
if ($_REQUEST['billing_date_to'] != NULL) {
	$billing_date_to = htmlspecialchars($_REQUEST['billing_date_to'],ENT_QUOTES);
	$words .= " AND billing_date <= ".$billing_date_to;
}
if ($_REQUEST['pay_date_from'] != NULL) {
	$pay_date_from = htmlspecialchars($_REQUEST['pay_date_from'],ENT_QUOTES);
	$words .= " AND pay_date >= ".$pay_date_from;
}
if ($_REQUEST['pay_date_to'] != NULL) {
	$pay_date_to = htmlspecialchars($_REQUEST['pay_date_to'],ENT_QUOTES);
	$words .= " AND pay_date <= ".$pay_date_to;
}

$search_conditions = $words." ORDER BY `update_date` DESC";

$invoice_data_send_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_send_arr);
$flag = "";//フラグの初期化

//var_dump($invoice_data_receive_arr);

	if ( $_REQUEST['mode'] === 'download' ) {
		if ($format == 1) {
			$csv_data .= "請求元共通コード,請求元企業名,請求先共通コード,請求先企業名,送付コード,送付先企業名,請求書日付,請求書番号,請求書名,当社担当者,支払日付,振込口座,納品日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税額,源泉徴収税,合計金額,備考1,備考2,備考3\n";
			//$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
			for ( $i = 0; $i < $invoice_data_receive_num; $i++ ) {
				$claimant_id = $invoice_data_send_arr[$i]["claimant_id"];
				$csv_data .= $claimant_id.",";
				

				//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。
				$flag = "company_data";
				if ($claimant_id != 0 ) {
					$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
					//echo "<br/>claimant<br/>\r\n";
					$csv_data .= $claimant_company_name[0]['company_name'].",";
				} else {
					$csv_data .= ",";
				}

				//請求先のdestination_idを元に請求元の会社名を検索してCSVデータに挿入する。
				$dest_id = $invoice_data_send_arr[$i]["destination_id"];
				$csv_data .= $dest_id.",";
				
				$flag = "company_data";
				if ($dest_id != 0 ) {
					$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$dest_id,$words);
					//echo "<br/>claimant<br/>\r\n";
					$csv_data .= $destination_company_name[0]['company_name'].",";
				} else {
					$csv_data .= ",";
				}

				//請求先のsend_codeを元に請求元の会社名を検索してCSVデータに挿入する。
				$send_code = $invoice_data_send_arr[$i]["send_code"];
				$csv_data .= $send_code.",";
					$flag = "addressee_data_receive_client_id_from_client_id";
					$words = " AND `client_id` = '".$send_code."' ";
					$cond = "";
					$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words,$cond);
					//var_dump($client_id_arr);
				if ($send_code == NULL ||$send_code == "" ) {
					$csv_data .= ",";
				} else {
					$csv_data .= $client_id_arr[0]['company_name'].",";
				}

				$csv_data .= $invoice_data_send_arr[$i]['billing_date'].",";
				$csv_data .= $invoice_data_send_arr[$i]["invoice_code"].",";
				$csv_data .= $invoice_data_send_arr[$i]["invoice_name"].",";
				$csv_data .= $invoice_data_send_arr[$i]["staff_name"].",";
				$csv_data .= $invoice_data_send_arr[$i]["pay_date"].",";
				$csv_data .= $invoice_data_send_arr[$i]["bank_account"].",";
				$csv_data .= $invoice_data_send_arr[$i]["sale_date"].",";
				$csv_data .= $invoice_data_send_arr[$i]["product_code"].",";
				$csv_data .= $invoice_data_send_arr[$i]["product_name"].",";
				$csv_data .= $invoice_data_send_arr[$i]["unit_price"].",";
				$csv_data .= $invoice_data_send_arr[$i]["quantity"].",";
				$csv_data .= $invoice_data_send_arr[$i]["unit"].",";
				if ( $invoice_data_send_arr[0]['sum(total_price_excluding_tax)'] != NULL ) { 
					$csv_data .= $invoice_data_send_arr[$i]['sum(total_price_excluding_tax)'].","; 
				} else { 
					$csv_data .= $invoice_data_send_arr[$i]["total_price_excluding_tax"].",";
				}
				$csv_data .= $invoice_data_send_arr[$i]["sales_tax"].",";
				$csv_data .= $invoice_data_send_arr[$i]["withholding_tax"].",";
				if ( $invoice_data_send_arr[0]['sum(total_price)'] != NULL ) { 
					$csv_data .= $invoice_data_send_arr[$i]['sum(total_price)'].","; 
				} else { 
					$csv_data .= $invoice_data_send_arr[$i]["total_price"].",";
				}
				$csv_data .= $invoice_data_send_arr[$i]["remarks1"].",";
				$csv_data .= $invoice_data_send_arr[$i]["remarks2"].",";
				$csv_data .= $invoice_data_send_arr[$i]["remarks3"]."\n";
			}
		} else if ($format == 2) {
			//$csv_data .= "固定,,,請求書日付,固定,,,固定,合計金額,固定,固定,,,固定,合計金額,消費税額,送付先コード,,,固定,,固定,,,固定\n";
			for ( $i = 0; $i < $invoice_data_receive_num; $i++ ) {
				$claimant_id = $invoice_data_send_arr[$i]["claimant_id"];
				$csv_data .= "2000,,,";
				if ($invoice_data_send_arr[$i]['billing_date'] != 0) {
					$csv_data .= str_replace(" ","",substr($invoice_data_send_arr[$i]['billing_date'],0,4)."/".substr($invoice_data_send_arr[$i]['billing_date'],4,2)."/".substr($invoice_data_send_arr[$i]['billing_date'],6,2)).",";
				} else {
					$csv_data .= ",";
				}
				$csv_data .= "売掛金,";
				
				//請求先のsend_codeを元に請求元の会社名を検索してCSVデータに挿入する。
				$send_code = $invoice_data_send_arr[$i]["send_code"];
				$dest_id = $invoice_data_send_arr[$i]["destination_id"];
				$flag = "addressee_data_receive_client_id";
				$words = " AND `company_id` = ".$claimant_id."";
				$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
				if ($send_code != NULL || $send_code != "" ) {
					$csv_data .= $client_id_arr[0]['company_name']."-1,";
				} else if ($dest_id != 0) {
				//請求先のdestination_idを元に請求元の会社名を検索してCSVデータに挿入する。
				$flag = "company_data";
					$destination_company_name = $company_con -> company_sql_flag($pdo,$flag,$dest_id,$words);
					//echo "<br/>claimant<br/>\r\n";
					$csv_data .= $destination_company_name[0]['company_name']."-2,";
				} else {
					$csv_data .= "no,";
				}

				
				$csv_data .= ",対象外,";
				//if ( $invoice_data_send_arr[0]['sum(total_price)'] != NULL ) { 
				//	$csv_data .= $invoice_data_send_arr[$i]['sum(total_price)'].","; 
				//} else { 
					$csv_data .= $invoice_data_send_arr[$i]["total_price"].",";
				//}
				$csv_data .= "0,売上高,,,課税売上内8%,";
				//if ( $invoice_data_send_arr[0]['sum(total_price)'] != NULL ) { 
				//	$csv_data .= $invoice_data_send_arr[$i]['sum(total_price)'].","; 
				//} else { 
					$csv_data .= $invoice_data_send_arr[$i]["total_price"].",";
				//}
				$csv_data .= $invoice_data_send_arr[$i]["sales_tax"].",";
				$csv_data .= $invoice_data_send_arr[$i]["send_code"].",";
echo				$csv_data .= ",,0,,Cloud Invoiceからインポートされました。,,,no\n";
			}

		}
        //出力ファイル名の作成
        $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
      
        //文字化けを防ぐ
        $csv_data = mb_convert_encoding ( $csv_data , "sjis-win" , 'utf-8' );
          
        //MIMEタイプの設定
        header("Content-Type: application/octet-stream");
        //名前を付けて保存のダイアログボックスのファイル名の初期値
        header("Content-Disposition: attachment; filename={$csv_file}");
      
        // データの出力
        echo($csv_data);
        exit();
    }

?>


<?php require("header.php");?>

<script>

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box3" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box4" ).datepicker();
});


function OpenkCommonSearchFrame(){
	document.getElementById("kCommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}

function OpenkSouhuListFrame(){
	document.getElementById("kSouhuList").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}

function FrameHeight(){
	window.setTimeout("window.parent.frames['kCommonSearch'].test()","600");
}

function FrameHeight2(){
	window.setTimeout("window.parent.frames['kSouhuList'].test()","30");
}

</script>
<title>請求CSVエクスポート - Cloud Invoice</title>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>
<iframe id="kSouhuList" name="kSouhuList" src="./frame/kSouhuList"></iframe>


<article>

	<section id="m-1-box">
		<h2>
			請求CSVエクスポート
		</h2>
		
		<p>
			条件を指定してください。
		</p>
		<form action="" method="GET" name="seikyuhakkou_form">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" id="Kyoutu_No" name="destination_id" class="kCommonSearch" name="destination_id_visible" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame()" value="<?php if ($destination_company_id != "") {echo $destination_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
				<input type="hidden" id="Kyotu_No_Invisible" name="destination_id" value="<?php echo $destination_company_id;?>" />
			</div>
			
			<div id="ID">
				<h3>送付先コード</h3>
				<input type="hidden" id="select_dname" onkeyup="DnameInsert();TransClientId();" value="" />
				<input type="text" id="souhuname" onfocus="FrameHeight2();OpenkSouhuListFrame()" onkeyup="DnameInsert();TransClientId();" value="<?php if ($destination_client_id != "") {echo $destination_client_id;}?>" placeholder="送付先コードで検索する"/>
			</div>
			
			<div id="status">
				<h3>状態</h3>
				<label><input name="status" type="checkbox" value="1" checked /> 未発行</label>
				<label><input name="status_1" type="checkbox" value="1" checked /> 発行済</label>
			</div>
			
			
			<div id="hiduke">
				<h3>請求書日付</h3>
				<input id="box1" type="text" name="billing_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box2" type="text" name="billing_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="hiduke">
				<h3>支払期限日</h3>
				<input id="box3" type="text" name="pay_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box4" type="text" name="pay_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="hiduke">
				<h3>出力フォーマット</h3>
				<select name="format">
					<option value="1">Cloud Invoice形式</option>
					<option value="2">弥生会計形式</option>
				</select>
			</div>
			
			<div id="shiharaEx">
				<input type="hidden" name="company" value="<?php echo $_SESSION['user_id'];?>">
		        <input type="hidden" name="mode" value="download">
				<button onclick="submit();">エクスポート</button>
			</div>
		</form>
	</section>






</article>

<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(9)"></div>

<?php require("footer.php");?>