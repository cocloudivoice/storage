<?php
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../pdf/PdfToImg.class.php');
include_once(dirname(__FILE__).'/../pdf/CloudVisionTextDetection.class.php');
//自動仕分け用クラスの読み込み

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//テキスト分析加工用クラスの読み込み
include_once(dirname(__FILE__).'/../lib/igo-php/lib/Igo.php');
include_once(dirname(__FILE__).'/../cfiles/FullTextSearch.class.php');
include_once(dirname(__FILE__).'/editOCRText.class.php');

$user_id = "3308277917";
$path = "";
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。

//OCR済みテキスト分析編集用コントローラーを起動
$edittext_con = new editOCRText();
$fulltext = new FullTextSearch();
//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path  = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLUpdir";
$upload_dir_path = $path;//dirname(__FILE__)."/../pdf/png/".$af_id;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;

$journalized_code = rand(1000000000,9999999999);
$txt_res_dir = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLResult";
//中間ファイル用ディレクトリに移す
$txt_dir = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLMiddle";
//テキスト化処理済みログ用ディレクトリ
$txt_log_dir = "/var/www/co.cloudinvoice.co.jp/html/pdf/png/MLLog";

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);

//$upload_dir_pathのフォルダが存在しない場合に作る。
if (!file_exists($upload_dir_path)) {
	recurse_chown_chgrp($upload_dir_path, $uid, $gid);
}
//setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');

$img_file_org_arr = getFileList($upload_dir_path);
$img_file_arr = array_reverse ($img_file_org_arr);
$k = 0;

for($i = 0;$i < count($img_file_arr); $i++) {
	if (substr($img_file_arr[$i], -3, 3) == "png" || substr($img_file_arr[$i], -3, 3) == "jpg" || substr($img_file_arr[$i], -3, 3) == "tif"|| substr($img_file_arr[$i], -3, 3) == "bmp"|| substr($img_file_arr[$i], -3, 3) == "csv") {
		//$encode = checkEncode($img_file_arr[$i]);
		//$img_file_arr[$k] = mb_convert_encoding(basename($img_file_arr[$i]), "UTF-8", "sjis-win");
		$img_file_arr[$k] = basename($img_file_arr[$i]);
		$k++;
	}
}
$keyword = "千葉信金";
$keyword_data = "";
mb_detect_encoding($keyword);
//var_dump($img_file_arr);

if ($img_file_arr) {
	$counter = 0;
	$error_num = 0;

	//画像をテキスト化する工程
	for($m = 0;$m < count ($img_file_arr) ;$m++) {
		//ファイル名変換処理
		
		$encode = checkEncode($img_file_arr[$m]);
		
		//echo mb_internal_encoding("UTF-8");
		$img_file_arr[$m] = mb_convert_encoding($img_file_arr[$m], "UTF-8", $encode);
		$img_file_arr[$m] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$img_file_arr[$m]);
		$img_file_arr[$m] = str_replace(array(".PNG"),".png",$img_file_arr[$m]);
		$img_file_arr[$m] = str_replace(array(".tiff"),".tif",$img_file_arr[$m]);

		//未処理かどうかの判断
		//中間・結果ディレクトリのテキストをチェックする仕様からテキスト化処理済みログ用ディレクトリのテキストをチェックする仕様に変更
		if(!file_exists($txt_log_dir."/".substr($img_file_arr[$m], 0, -4)."ci.txt")) {
			if ( substr($img_file_arr[$m], -3, 3) == "pdf" || substr($img_file_arr[$m], -3, 3) == "png" || substr($img_file_arr[$m], -3, 3) == "jpg" || substr($img_file_arr[$m], -3, 3) == "tif"|| substr($img_file_arr[$m], -3, 3) == "bmp"|| substr($img_file_arr[$m], -3, 3) == "csv") {

				setlocale(LC_ALL, 'ja_JP.UTF-8');
				//echo $img_file_arr[$m];
				//echo "<br>";
				
				$csv  = array();
				$file = $path."/".$img_file_arr[$m];
				$fp   = fopen($file, "r");
				$data = file_get_contents($file);
				$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
				$temp = tmpfile();
				$csv  = array();

				fwrite($temp, $data);
				rewind($temp);
				$columns_num = sizeof(fgetcsv(fopen($file, "r"), 1000, ","));

				while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
				  $csv[] = $data;
				}
				fclose($fp);
				 
				//var_dump($csv);		
				
				//CSVデータを一行ずつ取り出してDBに登録する。
				$l = 0;
				//echo count( $csv );
				foreach ($csv as $value) {
					//var_dump($value);echo "<br />\n";
					if ( $l == 0) {
						for ($i = 0 ;$i < count( $csv );$i++) {
							switch ($value[$i]) {
								case "日付":
									$col1 = $i;
									break;
								case "左科目コード":
									$col2 = $i;
									break;
								case "左補助科目コード":
									$col3 = $i;
									break;
								case "左部門コード":
									$col4 = $i;
									break;
								case "右科目コード":
									$col5 = $i;
									break;
								case "右補助科目コード":
									$col6 = $i;
									break;
								case "右部門コード":
									$col7 = $i;
									break;
								case "金額":
									$col8 = $i;
									break;
								case "消費税額":
									$col9 = $i;
									break;
								case "消費税コード":
									$col10 = $i;
									break;
								case "消費税率":
									$col11 = $i;
									break;
								case "外税同時入力区分":
									$col12 = $i;
									break;
								case "摘要":
									$col13 = $i;
									break;
								case "左科目名":
									$col14 = $i;
									break;
								case "左補助科目名":
									$col15 = $i;
									break;
								case "左部門名":
									$col16 = $i;
									break;
								case "右科目名":
									$col17 = $i;
									break;
								case "右補助科目名":
									$col18 = $i;
									break;
								case "右部門名":
									$col19 = $i;
									break;
								default:
									$col20 = $i;
									break;
					        }
						}
					} else {
						//共通コードでの企業データのヒストリカル取得

						$processing_date = $value[$col1];
						$l_ac_code = $value[$col2];
						$l_sub_ac_code = $value[$col3];
						$l_sec_ac_code = $value[$col4];
						$r_ac_code = $value[$col5];
						$r_sub_ac_code = $value[$col6];
						$r_sec_ac_code = $value[$col7];
						$money_amount = $value[$col8];
						$consumption_tax = $value[$col9];
						$c_tax_code = $value[$col10];
						$c_tax_code_rate = $value[$col11];
						$tax_exclusive_price_flag = $value[$col12];
						$summary = str_replace(array("㈱","㈲"),array("株式会社","有限会社"),mb_convert_kana($value[$col13], "c", "utf-8"));
						//mb_convert_kana($str, "c", "utf-8")
						$mecab = new MeCab_Tagger($options);
						$nodes = $mecab->parseToNode($summary);

						$summary_data = "";
						$summary_data1 = "";
						$summary_data2 = "";
						$str = "";
						/*
						if ($keyword_data == "") {
							$mecab1 = new MeCab_Tagger($options);
							$key_nodes0 = $mecab1->parseToNode($keyword);
						    foreach ($key_nodes as $m) {
								$keyword_data1 = $m->getSurface()." ";
								$m->getFeature();// . "<br />";
								$feature2 = explode(",", $m->getFeature());
								$keyword_data .= isset($feature2[7]) ? $feature2[7] : $m->getSurface();
							}
							
							//echo "<br>keyword_data1:";
							//var_dump($keyword_data1);
							//echo "<br>feature2:";
							//var_dump($feature2);
							//echo "<br>";
							$keyword_data = han_kaku_to_jen_kaku($keyword_data);
							//var_dump($keyword_data);
							//echo "<br>";
						}
						*/
					    foreach ($nodes as $n) {
							$summary_data1 .= $n->getSurface()." ";
							$n->getFeature();// . "<br />";
							$feature = explode(",", $n->getFeature());
							$summary_data .= isset($feature[7]) ? $feature[7] : $n->getSurface();
						}
						$summary_data1;
						$summary_data = han_kaku_to_jen_kaku($summary_data);

						
						//摘要欄が近いものを取得する処理
						//$text[$l] = $summary_data;
						/*
						$layers = [3, 5, 1];
						$ann = fann_create_standard_array(count($layers), $layers);
						
						fann_set_activation_function_hidden($ann, FANN_ELLIOT_SYMMETRIC);
						fann_set_activation_function_output($ann, FANN_ELLIOT_SYMMETRIC);
						*/

						$l_ac_name = $value[$col14];
						$l_sub_ac_name = $value[$col15];
						$l_sec_ac_name = $value[$col16];
						$r_ac_name = $value[$col17];
						$r_sub_ac_name = $value[$col18];
						$r_sec_ac_name = $value[$col19];
						//摘要欄その他が近いものを取得する処理
echo					$text[$l] = $value[$col1].",".$value[$col2].",".$value[$col3].",".$value[$col4].",".$value[$col5].",".$value[$col6].",".$value[$col7].",".$value[$col8].",".$value[$col9].",".$value[$col10].",".$value[$col11].",".$value[$col12].",".$value[$col13].",".$value[$col14].",".$value[$col15].",".$value[$col16].",".$value[$col17].",".$value[$col18].",".$value[$col19].",".$value[$col20].",".$summary_data1.",".$summary_data."\n";
//echo					$text[$l] = $value[$col13].",".$summary_data1.",".$summary_data."\n";

echo "<br>";
					}
					
					//300件ずつインデックスに登録
					if ($l != 0 && ($l%300 == 0 || $l == (count($csv) - 1))) {
						// インデックスを作成
						//var_dump($text);
						foreach($text as $value){
						    $fulltext->index($value);
						}
						$text = array();
					}
					
					
					
					$l++;
				}

				/*
				$csv_data = file_get_contents($path."/".$img_file_arr[$m]);
				//$csv_data = fgets($path."/".$img_file_arr[$m]);
				//$encode = checkEncode($csv_data);
				$csv_data = mb_convert_encoding($csv_data, "UTF-8", "sjis-win");
				$csv_data = str_replace('"','',$csv_data);
				$csv_data = explode( "\r", $csv_data );
				//var_dump($csv_data);
				//var_dump($csv_data);
				for($line = 0;$line < count($csv_data);$line++) {
					$csv_line = $csv_data[$line];echo "<br>";
					$csv_line_data = explode(",",$csv_line);echo "<br>";
					var_dump($csv_line_data);
				}
				*/
			}
		}
		$counter++;
	}

	//摘要欄が近いものを取得する処理
	//$text[$l] = $summary_data;
	/*
	$layers = [3, 5, 1];
	$ann = fann_create_standard_array(count($layers), $layers);
	
	fann_set_activation_function_hidden($ann, FANN_ELLIOT_SYMMETRIC);
	fann_set_activation_function_output($ann, FANN_ELLIOT_SYMMETRIC);
	*/



	echo "<br>結果<br>";
	$results = $fulltext->search($keyword);
	//$results = $fulltext->search($keyword_data);
	//var_dump($results);
	for ($n = 0; $n < count($results);$n++) {
		echo $results[$n];
		echo "<br>";
	}
	exit;
}
exit;
header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://co.cloudinvoice.co.jp/invoice/machine_learning_text_upload'</script>";
echo "</html>";

echo "<input type='button' value='送信フォームに移動' onclick='location.href=\"./machine_learning_text_upload\"'>";

if ($_REQUEST["return_flag"] == 1) {
	header("Location:".$return_url,false);
	exit;
}

/**
* テキストファイルの文字コードを変換し保存する
* @param string $infname  入力ファイル名
* @param string $incode   入力ファイルの文字コード
* @param string $outfname 出力ファイル名
* @param string $outcode  出力ファイルの文字コード
* @param string $nl       出力ファイルの改行コード
* @return string メッセージ
*/
function convertCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'wb');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

function checkEncode($str){
	foreach(array('UTF-8','SJIS','EUC-JP','ASCII','JIS') as $charcode){
		if(mb_convert_encoding($str, $charcode, $charcode) == $str){
			return $charcode;
		}
	}

	return null;
}

function by_stream_filter($file) {
    require_once 'Stream/Filter/Mbstring.php';
 
    $ret = stream_filter_register("convert.mbstring.*", "Stream_Filter_Mbstring");
 
    $fp = fopen($file, 'r');
    $filter_name = 'convert.mbstring.encoding.SJIS-win:UTF-8';
    $filter = stream_filter_append($fp, $filter_name, STREAM_FILTER_READ);
 
    $current_locale = setlocale(LC_ALL, '0'); // 現在のロケールを取得
    setlocale(LC_ALL, 'ja_JP.UTF-8');
    while ($values = fgetcsv($fp)) {
        // ここでcsvを処理する
    }
    setlocale(LC_ALL, $current_locale); // ロケールを戻す
    fclose($fp);
}

function han_kaku_to_jen_kaku($str) {
	$replace_of = array('ｳﾞ','ｶﾞ','ｷﾞ','ｸﾞ',
	                    'ｹﾞ','ｺﾞ','ｻﾞ','ｼﾞ',
	                    'ｽﾞ','ｾﾞ','ｿﾞ','ﾀﾞ',
	                    'ﾁﾞ','ﾂﾞ','ﾃﾞ','ﾄﾞ',
	                    'ﾊﾞ','ﾋﾞ','ﾌﾞ','ﾍﾞ',
	                    'ﾎﾞ','ﾊﾟ','ﾋﾟ','ﾌﾟ','ﾍﾟ','ﾎﾟ');
	$replace_by = array('ヴ','ガ','ギ','グ',
	                    'ゲ','ゴ','ザ','ジ',
	                    'ズ','ゼ','ゾ','ダ',
	                    'ヂ','ヅ','デ','ド',
	                    'バ','ビ','ブ','ベ',
	                    'ボ','パ','ピ','プ','ペ','ポ');
	$_result = str_replace($replace_of, $replace_by, $str);

	$replace_of = array('ｱ','ｲ','ｳ','ｴ','ｵ',
	                    'ｶ','ｷ','ｸ','ｹ','ｺ',
	                    'ｻ','ｼ','ｽ','ｾ','ｿ',
	                    'ﾀ','ﾁ','ﾂ','ﾃ','ﾄ',
	                    'ﾅ','ﾆ','ﾇ','ﾈ','ﾉ',
	                    'ﾊ','ﾋ','ﾌ','ﾍ','ﾎ',
	                    'ﾏ','ﾐ','ﾑ','ﾒ','ﾓ',
	                    'ﾔ','ﾕ','ﾖ','ﾗ','ﾘ',
	                    'ﾙ','ﾚ','ﾛ','ﾜ','ｦ',
	                    'ﾝ','ｧ','ｨ','ｩ','ｪ',
	                    'ｫ','ヵ','ヶ','ｬ','ｭ',
	                    'ｮ','ｯ','､','｡','ｰ',
	                    '｢','｣','ﾞ','ﾟ');
	$replace_by = array('ア','イ','ウ','エ','オ',
	                    'カ','キ','ク','ケ','コ',
	                    'サ','シ','ス','セ','ソ',
	                    'タ','チ','ツ','テ','ト',
	                    'ナ','ニ','ヌ','ネ','ノ',
	                    'ハ','ヒ','フ','ヘ','ホ',
	                    'マ','ミ','ム','メ','モ',
	                    'ヤ','ユ','ヨ','ラ','リ',
	                    'ル','レ','ロ','ワ','ヲ',
	                    'ン','ァ','ィ','ゥ','ェ',
	                    'ォ','ヶ','ヶ','ャ','ュ',
	                    'ョ','ッ','、','。','ー',
	                    '「','」','”','');        
	$_result = str_replace($replace_of, $replace_by, $_result);
	return $_result;
}


?>