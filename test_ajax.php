<?php
session_start();
header('Access-control-allow-origin: *');// 通信を許可する接続元ドメインか*（アスタリスク）を指定

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>共通コード確認　- Cloud Invoice</title>

<script src="../js/jquery-1.6.2.min.js"></script>
<script>
$(document).ready(function()
{

    /**
     * 送信ボタンクリック
     */
    $('#request').keyup(function()
    {
        //POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
        var data = {request : $('#request').val()};

        /**
         * Ajax通信メソッド
         * @param type  : HTTP通信の種類
         * @param url   : リクエスト送信先のURL
         * @param data  : サーバに送信する値
         */
        $.ajax({
            type: "POST",
            url: "send.php",
            data: data,
            /**
             * Ajax通信が成功した場合に呼び出されるメソッド
             */
            success: function(data, dataType)
            {
                //successのブロック内は、Ajax通信が成功した場合に呼び出される

                //PHPから返ってきたデータの表示
                document.getElementById('result').innerHTML = data;
                //document.write(data);
            },
            /**
             * Ajax通信が失敗した場合に呼び出されるメソッド
             */
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                //通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

                //this;
                //thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

                //エラーメッセージの表示
                alert('Error : ' + errorThrown);
            }
        });
        
        //サブミット後、ページをリロードしないようにする
        return false;
    });
});

/*
function sendData(){
	document.code_form.submit();
}
*/




function getText(obj){
	var souhu = obj.innerHTML;
	var code = souhu.substring(0,12);
	send_code = String(code).slice(1,-1);
	document.getElementById("request").value=code;
}


</script>
	
<style type="text/css">
body{background-color:rgb(250,250,250);}
input[type=text].CommonSearch{height:30px;}
input[type=text]{width:350px;}
p{padding:2px 0 2px 0;margin:0; font-size:13px; width:350px; white-space:nowrap; overflow:hidden; color:rgb(100,100,100);background-color:white;}
div#result{width:350px; max-height:300px; overflow:hidden;}

</style>
	
</head>
<body>
	<form name="code_form" method="post">
	</form>
	<input type="text" class="CommonSearch" id="request" placeholder="共通コード、会社名、住所、メールアドレスで検索する" />
	<div id="result"></div>
</body>
</html>