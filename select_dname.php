<?php require("main_read.php");?>
<!DOCTYPE html>

<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
		<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//storage.cloudinvoice.co.jp/images/logo.jpg" />
		<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
		<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="//storage.cloudinvoice.co.jp/" />
		<script src="//storage.cloudinvoice.co.jp/js/jquery-2.0.2.min.js" type="text/javascript" charset="UTF-8"></script>
		<script type="text/javascript" src="./tablesorter/jquery-latest.js"></script>
		<script type="text/javascript" src="./tablesorter/jquery.tablesorter.min.js"></script>
		<script src="./tablesorter/jquery.metadata.js" type="text/javascript"></script>
		<link rel="stylesheet" href="./tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />

<script>



function openMenu(n){
	if(n==1){
		OpenTime=setTimeout('document.getElementById("li1").style.display="block";',350);
	}
	if(n==2){
		OpenTime=setTimeout('document.getElementById("li2").style.display="block";',350);
	}
	if(n==3){
		OpenTime=setTimeout('document.getElementById("li3").style.display="block";',350);
	}
}

function openMenu2(n){
	clearTimeout(CloseTime);
	clearTimeout(OpenTime);
	if(n==1){
		setTimeout('document.getElementById("li1").style.display="block";',320);
	}
	if(n==2){
		setTimeout('document.getElementById("li2").style.display="block";',320);
	}
	if(n==3){
		setTimeout('document.getElementById("li3").style.display="block";',320);
	}
}

function closeMenu(n) {
	if(n==1){
		CloseTime=setTimeout('document.getElementById("li1").style.display="none";',350);
	}
	if(n==2){
		CloseTime=setTimeout('document.getElementById("li2").style.display="none";',350);
	}
	if(n==3){
		CloseTime=setTimeout('document.getElementById("li3").style.display="none";',350);
	}
}



function openSet(){
	document.getElementById("Set").style.display="block";
}

function openSet2(){
	clearTimeout(CloseTime);
}

function closeSet() {
	CloseTime=setTimeout('document.getElementById("Set").style.display = "none";',100);
}




function CloseFrame(n){
	parent.document.getElementById("fadeLayer").style.visibility = "hidden";
	switch(n){
		case 1 : parent.document.getElementById("default").style.display = "none";
			break;
		case 2 :parent.document.getElementById("report").style.display = "none";
			parent.document.getElementById("ChangeInvoice").style.display = "none";
			break;
		case 3 : parent.document.getElementById("Mail").style.display = "none";
			parent.document.getElementById("Post").style.display = "none";
			parent.document.getElementById("Issue").style.display = "none";
			parent.document.getElementById("Delete").style.display = "none";
			break;
		case 4 : parent.document.getElementById("souhuEditFrame").style.display = "none";
			break;
		case 5 : parent.document.getElementById("changePass").style.display = "none";
			parent.document.getElementById("changeAd").style.display = "none";
			parent.document.getElementById("leave").style.display = "none";
			break;

		case 6 : parent.document.getElementById("Freeplan").style.display = "none";
			parent.document.getElementById("Standardplan").style.display = "none";
			parent.document.getElementById("Premiumplan").style.display = "none";
			parent.document.getElementById("Passerror").style.display = "none";
			break;

		case 7 : parent.document.getElementById("Idetail").style.display = "none";
			parent.document.getElementById("ChangeInvoice").style.display = "none";
			break;
	}
}



</script>
<script type="text/javascript">
	//テーブルソーターのスクリプト
   //行の色を交互に変えてゼブラカラーに
/*	jQuery(function($) {
		$('.tables').tablesorter({
			widgets: ['zebra'],
//			sortList: [[0, 0]/*,[5,1],[4,0]],//初期ソート条件を指定
			headers: {
				8: {sorter:false},
				9: {sorter:false}
			},
			sortMultiSortKey: 'altKey'
		});
	}); //jQuery
*/

	$(document).ready(function() { 
		$("#uriageTable").tablesorter({
			widgets: ['zebra'],
			sortList: [[11, 1]],//初期ソート条件を指定
			headers: {
				0: {sorter:false},
				12: {sorter:false},
				13: {sorter:false}
			},
			sortMultiSortKey: 'altKey'
		});
		$("#main").tablesorter({
			widgets: ['zebra'],
			sortList: [[1, 1],[0,0]],//初期ソート条件を指定
			headers: {
				6: {sorter:false}
			},
			sortMultiSortKey: 'altKey'
		});
		$("#addresseeTable").tablesorter({
			widgets: ['zebra'],
			sortList: [[0, 0],[5,1],[4,0]],//初期ソート条件を指定
			headers: {
				8: {sorter:false},
				9: {sorter:false}
			},
			sortMultiSortKey: 'altKey'
		});
	}); 

/*
   $(document).ready(function() 
       { 
           $("#sampleTable1").tablesorter();
           $("#yonpleTable2").tablesorter();
           $("#gowpleTable3").tablesorter();
       } 
   ); 
*/
</script>



<style type="text/css">


html *{margin:0px; padding:0px; outline:none;}
body {min-width:970px; font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:rgb(0,75,145); cursor:pointer;}
div#clear{clear:both;}
button{cursor:pointer; font-size:14px; line-height:14px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240));}
button:hover{background:rgb(230,230,230);}
table{text-overflow: ellipsis;}
table.tablesorter tbody tr.odd td {background-color:rgb(250,250,250);}
td{text-overflow: ellipsis;}
tr{text-overflow: ellipsis;}
th{text-overflow: ellipsis;}
input{min-height:16px;}
input[type=button] {cursor:pointer; font-size:14px; line-height:13px; padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240)); font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=button]:hover{background:rgb(230,230,230);}
input[type=submit] {cursor:pointer; font-size:14px; line-height:13px; padding:5px; color:rgb(35,150,228); border:1px solid rgb(230,230,230); border-radius:5px; background:linear-gradient(rgb(250,250,250),rgb(240,240,240)); font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=submit]:hover{background:rgb(230,230,230);}
input[type=text]{font-size:13px; padding:0 0 0 5px; width:125px; height:25px; box-shadow:1px 1px 1px 0 rgba(200,200,200,0.2) inset;  font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=hidden]{font-size:13px; width:130px; height:25px; box-shadow:1px 1px 1px 0 rgba(200,200,200,0.2) inset;  font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}
input[type=password]{height:18px; box-shadow:1px 1px 1px 0 rgba(200,200,200,0.2) inset;}
input[type=file]{cursor:pointer; font-size:14px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; padding:5px; color:rgb(35,150,228);}
textarea{font-size:13px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif;}

.hide{display:none;}
.show{display:block;}	
.strong{font-weight:bold; color:rgb(60,60,60);}

header{height:45px; background:linear-gradient(white 95%, rgb(240,240,240)); border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230);}

div#hm-box{}

div#hm-1-box{float:left;}
div#hm-1-box h1{font-size:25px; line-height:45px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
div#hm-1-box img{width:250px; vertical-align:-5px;}

div#hm-2-box{float:right;}
div#hm-2-box p{margin:13px 10px 0 0;}

div#hm-3-box{float:right; padding:1px 15px 0 0;}
div#hm-3-box img{width:40px;}

div#hm-4-box{float:right; padding:1px 15px 0 0; position:relative;}
div#hm-4-box img{width:40px;}
ul#Set{display:none; top:46px; right:0px; width:120px; position:absolute; z-index:5; box-shadow:3px 1px 3px 0 rgba(0,0,0,0.15); border-width:0px 1px 1px 1px; border-style:solid; border-color:rgb(230,230,230); background:white; line-height:35px; padding:5px 0 5px 0; list-style-type:none;}
ul#Set li{padding:0 0 0 20px;}
ul#Set li:hover{background-color:rgb(215,250,255);}


nav#l-box{width:150px; float:left; box-shadow:3px 1px 3px 0 rgba(0,0,0,0.15);border-width:0px 1px 1px 0; border-style:solid; border-color:rgb(230,230,230);}

ul#menu li{height:60px; padding:0 0 0 20px;font-size:15px; line-height:60px; letter-spacing:2px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230);}
ul#menu li:hover{background-color:rgb(210,250,255);}
li#menu{background-image:url("/images/arrow_blue.jpg");}
li#menu:hover{background-image:url("/images/arrow_afterblue.jpg");}
ul#li1,ul#li2,ul#li3{display:none; left:150px; position:absolute;line-height:60px; list-style-type:none;}
ul#li1 li,ul#li2 li,ul#li3 li{font-size:14px; height:60px; width:160px; z-index:5; box-shadow:3px 3px 3px 0 rgba(0,0,0,0.15);border-width:0px 1px 1px 1px; border-style:solid; border-color:rgb(230,230,230); background:white; padding:0 0 0 10px;}
ul#li1{top:107px;}
ul#li1 li{}
ul#li2{top:168px;}
ul#li3{top:229px;}










article{margin:20px 0 60px 170px;}

section#m-1-box{margin:10px 0 40px 10px;}
section#m-1-box h2{font-size:15px; color:rgb(100,100,100); padding:3px 0 3px 10px; margin:0 0 15px 0;  border-width:0 0 1px 10px; border-style:solid; border-color:rgb(150,150,150);}
section#m-1-box ul{line-height:25px; margin:0 0 0 20px;}
section#m-1-box table{border-collapse:collapse; text-align:left; font-size:13px;}
section#m-1-box th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150); height:30px; font-size:14px; color:rgb(100,100,100); padding:0 0 0 10px;}
section#m-1-box td{border-width:1px 0 1px 0; border-style:solid; border-color:rgb(150,150,150); height:30px; padding:0 0 0 10px;}


table#main th#kaisha{width:220px;}
table#main th#koushin{width:80px;}
table#main th#kingaku{width:70px;}
table#main th#tekiyou{width:220px;}
table#main th#shiharai{width:80px;}
table#main th#hakkou{width:50px;}
table#main th#joutai{width:120px;}

div#koushin1{margin:20px 0 0 550px;}
div#koushin{margin:20px 0 0 600px;}



div#m-1-box {}

div#kakunin{margin:5px 0 10px 0;}
div#kakunin button{width:120px;}

div#sFormat{}
div#sFormat a{font-size:12px;}

table#scsv th#name{width:250px;}
table#scsv th#date{width:150px;}
table#scsv td{height:35px;}


div#hiduke{margin:10px 0 15px 0;}
div#hiduke h3{font-size:14px; font-weight:normal; display:inline;}
div#hiduke p{display:inline; font-size:12px; vertical-align:3px;}
div#hiduke input[type=text]{width:95px; height:20px;}
div#hiduke input[type=radio]{margin:0 0 0 5px; vertical-align:-3px;}
div#hiduke label{margin:0 10px 0 0;}

div#ID{margin:10px 0 15px 0;}
div#ID h3{font-size:14px; font-weight:normal; display:inline; margin:0 5px 0 0;}
div#ID p{display:inline; font-size:12px; vertical-align:3px;}
div#ID input[type=text]{width:95px; height:20px;}
div#ID input[type=radio]{margin:0 15px 0 2px; vertical-align:-1px;}

div#status label{margin:0 0 0 20px; color:rgb(100,100,100);}
div#status input[type=checkbox]{vertical-align:-3px;}
div#status h3{font-size:14px; font-weight:normal; display:inline; margin:0 5px 0 0;}

div#OP{margin:10px 0 15px 0;}
div#OP h3{font-size:14px; font-weight:normal; display:inline; margin:0 5px 0 0px; width:140px; float:left; text-align:right;}
div#OP input#name{}
div#OP input#address{width:200px;}
div#OP textarea{width:400px; height:200px;  margin:10px 0 0 20px;}
div#OP button{margin:0 0 0 180px;}



section#m-2-box{margin:10px 0 40px 10px;}
section#m-2-box h2{font-size:15px; color:rgb(100,100,100); padding:3px 0 3px 10px; margin:0 0 10px 0;  border-width:0 0 1px 10px; border-style:solid; border-color:rgb(150,150,150);}
section#m-2-box table{border-collapse:collapse; float:left; margin:10px 0 0 0;}
section#m-2-box tr{height:50px; margin:0; padding:0;}
section#m-2-box th{font-size:14px;text-align:left; width:120px; border-width:1px 1px 1px 0; border-style:solid; border-color:rgb(200,200,200); padding:0 0 0 10px; color:rgb(100,100,100); background-color:rgb(220,255,255);}
section#m-2-box td{text-align:left; width:400px; border-width:1px 0 1px 0; border-style:solid; border-color:rgb(200,200,200); padding:0 0 0 10px; color:rgb(0,0,0);}
section#m-2-box button{height:30px; margin:10px 0 0 20px;}
section#m-2-box img{margin:5px 0 5px 0;}
section#m-2-box img#logo{width:200px; margin:10px 0 10px 0;}

section#m-2-box th#invoice{border-width:1px 1px 1px 0; border-style:solid; border-color:rgb(200,200,200); height:300px;}
section#m-2-box td#invoice{border-width:1px 0 1px 0; border-style:solid; border-color:rgb(200,200,200);}
section#m-2-box td#invoice img{width:230px;box-shadow:3px 1px 1px 1px rgba(0,0,0,0.15);border-width:1px 1px 1px 1px; border-style:solid; border-color:rgb(200,200,200); margin:10px 0 10px 0;}

iframe#default{position:absolute; display:none; width:590px;height:630px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#report{position:absolute; display:none; width:590px;height:630px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Mail{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Mail2{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Delete{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Delete2{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Post{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Post2{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Issue{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Issue2{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#souhuEditFrame{position:absolute; display:none; width:590px;height:790px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#changeAd{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#changePass{position:absolute; display:none; width:590px;height:300px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#leave{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#ChangeInvoice{position:absolute; display:none; width:610px;height:610px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Freeplan{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Standardplan{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Premiumplan{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Passerror{position:absolute; display:none; width:590px;height:250px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#Idetail{position:absolute; display:none; width:590px;height:350px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
iframe#SouhuList{position:absolute; display:none; width:590px;height:350px; left:50%; margin:0 0 0 -285px; z-index:7; background-color:white; border-width:0px; border-radius:4px;}
#fadeLayer{position:fixed; top:0px;left:0px; width:100%; height:100%; background-color:#000000; opacity:0.5; visibility:hidden; z-index:1;}


section#m-3-box{margin:10px 0 40px 10px;}
section#m-3-box h2{font-size:15px; color:rgb(100,100,100); padding:3px 0 3px 10px; margin:0 0 20px 0;  border-width:0 0 1px 10px; border-style:solid; border-color:rgb(150,150,150);}
section#m-3-box table{border-collapse:collapse; text-align:left; font-size:13px; margin:10px 0 0 0;}
section#m-3-box th{font-size:13px; border-width:1px 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); height:35px; color:rgb(100,100,100);overflow:hidden; white-space:nowrap; text-align:center; background-color:rgb(235,235,235);}
section#m-3-box button{height:30px; margin:0 0 0 10px;}

section#m-3-box td{font-size:12px; border-width:1px 1px 1px 1px; border-style:solid; border-color:rgb(150,150,150); height:35px;overflow:hidden; white-space:nowrap;text-align:center;}
section#m-3-box td img{vertical-align:-7px;}

section#m-3-box td#check{width:30px;max-width:30px;}
section#m-3-box label#checklabel{display: block; width:100%;height:100%;}
section#m-3-box input[type="checkbox"]{vertical-align:-10px;}
section#m-3-box td#kyoutuCD{width:100px;max-width:100px;}
section#m-3-box td#kanriCD{width:100px;max-width:100px;}
section#m-3-box td#atesaki{width:130px;max-width:130px;}
section#m-3-box td#seikyuBi{width:80px;max-width:80px;}
section#m-3-box td#seikyuNo{width:100px;max-width:100px;}
section#m-3-box td#seikyuName{width:140px;max-width:140px;}
section#m-3-box td#thisTanto{width:80px;max-width:80px;}
section#m-3-box td#siharaibi{width:80px;max-width:80px;}
section#m-3-box td#thisKoza{width:200px;max-width:200px;}
section#m-3-box td#sumMoney{width:75px;max-width:75px;text-align:right;padding:0 5px 0 0;}
section#m-3-box td#meisai{width:60px;max-width:80px;}
section#m-3-box td#csvNitiji{width:150px;max-width:150px;}
section#m-3-box td#status{width:80px;max-width:80px;}

div#uriagekanri{margin:20px 0 0 0; padding:0;}
div#uriagekanri button{vertical-align:middle; height:50px;}


section#m-3-box td#souhuNo{width:100px;max-width:100px;}
section#m-3-box td#souhusaki{width:130px;max-width:130px;}
section#m-3-box td#yourTanto{width:70px;max-width:70px;}
section#m-3-box td#yourBusho{width:70px;max-width:70px;}
section#m-3-box td#yourBango{width:100px;max-width:100px;}
section#m-3-box td#yourAddress{width:200px;max-width:200px;}
section#m-3-box td#souhuEdit{width:35px;max-width:35px;}
section#m-3-box td#souhuDelete{width:35px;max-width:35px;}
section#m-3-box td button#souhuEditButton{background-image:url("/images/edit.png"); width:24px; background-repeat:no-repeat; background-color:white; border-width:0; vertical-align:-7px; margin:0; padding:0;}
section#m-3-box td button#souhuDeleteButton{background-image:url("/images/delete.png"); width:24px; background-repeat:no-repeat; background-color:white; border-width:0; vertical-align:-7px; margin:0; padding:0;}






article#makeinvoice{min-width:1175px;}
article#makeinvoice h3{font-size:13px; margin:15px 0 0 3px;}

input#blank{border:0;}

section#leftmi{float:left; width:380px; height:1025px; background-color:rgb(250,250,250); border-radius:3px; box-shadow:1px 1px 1px 0 rgba(200,200,200,0.5) inset; margin:0 0 0 10px; padding:5px 0 0 10px;}

section#leftmi div#headmi h2{display:inline; font-size:17px; color:rgb(50,50,50); margin:0 0 0 10px;}
section#leftmi div#headmi input[type=button]{margin:5px 0 0 35px; height:50px; width:200px;}

section#leftmi div#kyoutu{margin:0;}
section#leftmi div#kyoutu input[type=text]#Kyoutu_No{float:left; margin:0 5px 0 0;}
section#leftmi div#kyoutu input[type=button]#Kyoutu_No{margin:0; font-size:13px;}
section#leftmi div#kyoutu select{width:130px; height:27px; margin:0 0 20px 0; float:left;}
section#leftmi div#kyoutu input[type=button]#company_info_button{font-size:13px; margin:0 0 0 5px;}

section#leftmi div#seikyuushobangou{border-width:1px 0 0 0;border-style:solid; border-color:rgb(200,200,200); margin:0 0 0 -10px; padding:0 0 0 10px;}

section#leftmi div#torihikisakiname{position:relative;}
section#leftmi div#torihikisakiname input[type=text]#seikyu_4{margin:0; padding:0; float:left; width:250px;}
section#leftmi div#torihikisakiname input[type=button]#torihikisaki_button{border:1px 1px 1px 0; border-radius:2px; background:white;}
section#leftmi div#torihikisakiname input[type=button]#torihikisaki_button:hover{background:rgb(245,245,245);}
ul#openClient {width:280px; top:49px; border-radius:5px; position:absolute; z-index:5; box-shadow:3px 1px 3px 0 rgba(0,0,0,0.15); border-width:0px 1px 1px 1px; border-style:solid; border-color:rgb(230,230,230); background:white; line-height:35px; padding:5px 0 5px 0; list-style-type:none;}
ul#openClient li{padding:0 0 0 20px;}
ul#openClient li:hover{background-color:rgb(215,250,255);}

section#leftmi div#torihikisakijuusho input[type=text]#seikyu_6{width:360px; margin:0 0 3px 0;}

section#leftmi div#torihikisakijuusho input[type=text]#seikyu_7{width:360px;}

section#leftmi div#torihikisakitantou input[type=text]#seikyu_8{margin:0 0 3px 0;}

section#leftmi div#seikyuubi {float:left; margin:0 10px 0 0;}

section#leftmi div#meisai table{margin:15px 0 0 0;border-collapse:collapse; width:377px;}
section#leftmi div#meisai table tr{height:30px;}
section#leftmi div#meisai table tr#first{height:20px;}
section#leftmi div#meisai table th{text-align:left; font-size:13px; padding:0 0 0 2px;}
section#leftmi div#meisai table td#hinmoku{width:160px;}
section#leftmi div#meisai input[type=text]#hinmoku{width:150px;}

section#leftmi div#meisai table td#tanka{width:67px;}
section#leftmi div#meisai input[type=text]#tanka{width:57px;}

section#leftmi div#meisai table td#suuryou{width:50px;}
section#leftmi div#meisai input[type=text]#suuryou{width:40px;}

section#leftmi div#meisai table td#goukei{width:70px;}
section#leftmi div#meisai input[type=text]#goukei{width:60px; background:rgb(245,245,245); border-style:solid; border-color:rgb(250,250,250); color:rgb(50,50,50);}

section#leftmi div#meisai table td#shousai{width:35px;}
section#leftmi div#meisai img#shousai{width:25px; vertical-align:-5px; cursor:pointer; background-color:rgb(255,255,255);}

section#leftmi div#hurikomisaki textarea{width:365px; height:80px;}

section#leftmi div#bikou textarea{width:365px; height:60px; margin:0 0 10px 0;}

section#leftmi input[type=submit]{margin:30px 0 0 110px; height:45px; width:150px;}


iframe#invoice1{width:775px; height:1030px; border-style:none; border-radius:5px;}



div#plan{margin:5px 0 20px 0;}
p#plan{margin:0 0 20px 0;}
div#plan h3{font-size:14px; font-weight:normal; display:inline;}
div#plan input[type=radio]{margin:0 0 0 5px; vertical-align:-3px;}
div#plan label{margin:0 10px 0 0;}

div#planpass{margin:0 0 20px 0;}
div#planpass p{display:inline; font-size:14px; vertical-align:3px;}
div#planpass input[type=password]{vertical-align:2px;}






footer{width:510px; margin:10px auto 20px auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px; color:rgb(50,50,50);}






</style>




<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>




<body>






<header>
	<div id="hm-box">
		<div id="hm-1-box">
			<h1>
				<a href="./main">
					<img src="/images/logo2.png"/>
				</a>
			</h1>
		</div>
		<div id="hm-3-box">
			<a href="../help" target="_blank"><img src="/images/question.png" alt="Cloud Invoiceのヘルプ"></a>
		</div>
		<div id="hm-4-box" onMouseover="openSet()">
			<img src="/images/wheel.png" alt="Cloud Invoiceの基本設定・ログアウト等">
			<ul id="Set" onMouseover="openSet2()" onMouseout="closeSet()">
				<a href="./user"><li>ログイン設定</li></a>
				<a href="./office"><li>基本設定</li></a>
				<a href="./office2"><li>帳票設定</li></a>
				<a href="./changeplan"><li>プラン変更</li></a>
				<a href="./operator2"><li>お問い合わせ</li></a>
				<a href="./logout"><li>ログアウト</li></a>
			</ul>
		</div>
		<div id="hm-2-box">
			<p>
				<strong><?php echo $user_company_name[0]['company_name'];?></strong>（共通コード:<strong><?php echo substr($user_arr['user_id'],0,4)." ".substr($user_arr['user_id'],4,4)." ".substr($user_arr['user_id'],8,4);?></strong>）
			</p>
		</div>
		<div id ="clear"></div>
	</div>
</header>




<nav id="l-box">
	
	<ul id="menu">
		<a href="./main"><li onMouseover="closeMenu()">HOME</li></a>
		<li id="menu" onMouseover="openMenu(1)" onMouseout="closeMenu(1)">売上管理</li>
		<li id="menu" onMouseover="openMenu(2)" onMouseout="closeMenu(2)">支払管理</li>
		<li id="menu" onMouseover="openMenu(3)" onMouseout="closeMenu(3)">設定</li>
	</ul>
	
	<ul id="li1" onMouseover="openMenu2(1)" onMouseout="closeMenu(1)">
		<a href="./makeinvoice"><li>請求書作成</li></a>
		<a href="./seikyuhakkou"><li>売上管理</li></a>
		<a href="./exseikyucsv"><li>請求CSVエクスポート</li></a>
		<a href="./seikyucsv"><li>請求CSVインポート</li></a>
	</ul>
	
	<ul id="li2" onMouseover="openMenu2(2)" onMouseout="closeMenu(2)">
		<a href="./shiharaihakkou"><li>支払管理</li></a>
		<a href="./shiharaicsv"><li>支払CSVエクスポート</li></a>
	</ul>
	
	<ul id="li3" onMouseover="openMenu2(3)" onMouseout="closeMenu(3)">
		<a href="./office"><li>基本設定</li></a>
		<a href="./office2"><li>帳票設定</li></a>
		<a href="./addressee"><li>送付先管理</li></a>
		<a href="./changeplan"><li>プラン変更</li></a>
	</ul>
	
</nav>
