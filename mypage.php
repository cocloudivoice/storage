<?php
session_start();
//print "session_id = " . session_id() . "<br>"; 
//echo "テストここから<br />";
//echo $_SESSION['test'];
//echo "<br />テストここまで<br />";

//必要なクラスを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/sitetitlecontrol.class.php");
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/sitecontrol.class.php");
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");

//変数の宣言
$user_id;
$site_row;
$site_arr = array();
$site_count_arr = array();
$user_arr = array();

//user_idチェック
if (isset($_SESSION['user_id'])) {
	$user_id = (int)$_SESSION['user_id'];
} else {
	header("Location:./logout.php");
	exit();
}
if (isset($_SESSION['max_col'])) {
	echo $_SESSION['max_col'];
	echo $_SESSION['max_row'];
}
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$site_con = new site_control();
$title_con = new site_title_control();
$user_con = new user_control();

//各テーブルからデータを取得する
$site_count_arr = $site_con->site_count($pdo,$user_id);
$site_arr = $site_con->site_select_all($pdo,$user_id);
$site_row = $site_count_arr['count(*)'];
$user_arr = $user_con->user_select_id($pdo,$user_id);



//var_dump($site_row);
/*
 // 出力する画像サイズの指定
$width = 100;
$height = 100;

// サイズを指定して、背景用画像を生成
$canvas = imagecreatetruecolor($width, $height);

// コピー元画像の指定
$targetImage = $user_arr['user_img'];

// ファイル名から、画像インスタンスを生成
$image = imagecreatefromjpeg($targetImage);
// コピー元画像のファイルサイズを取得
list($image_w, $image_h) = getimagesize($targetImage);

// 背景画像に、画像をコピーする
imagecopyresampled($canvas,  // 背景画像
                   $image,   // コピー元画像
                   0,        // 背景画像の x 座標
                   0,        // 背景画像の y 座標
                   0,        // コピー元の x 座標
                   0,        // コピー元の y 座標
                   $width,   // 背景画像の幅
                   $height,  // 背景画像の高さ
                   $image_w, // コピー元画像ファイルの幅
                   $image_h  // コピー元画像ファイルの高さ
                  );

// 画像を出力する
imagejpeg($canvas,           // 背景画像
          //"./output.jpg",    // 出力するファイル名（省略すると画面に表示する）
          100                // 画像精度（この例だと100%で作成）
         );

// メモリを開放する
imagedestroy($canvas); 
*/

?>

<!DOCTYPE html>

<html Lang="ja">
<meta>


<head>



<style type="text/css">



html *{margin:0px; padding:0px;}
body {min-width:970px;font: 14px/20px 'メイリオ','Hiragino Kaku Gothic Pro','sans-serif';}
a {text-decoration:none;}
div#clear{clear:both;}



header{height:50px; background:linear-gradient(white 90%, rgb(240,240,240)); border-width:0 0 1px 0; border-style:solid; border-color:rgb(230,230,230);}

div#hm-box{}

div#hm-1-box{float:left;}
div#hm-1-box h1{font-size:30px; line-height:45px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}
div#hm-1-box h1 a{font-size:30px; line-height:45px; font-style:italic; margin:0 0 0 10px; color:rgb(0,0,110);}

div#hm-2-box{float:right;}
div#hm-2-box p{font-weight:bold; margin:13px 10px 0 0;}

div#hm-3-box{float:right;}
div#hm-3-box button[type=login]{width:80px; height:30px; margin:7px 20px 0 0; border:thin; border-style:solid; border-color:rgb(200,200,200); background:rgb(245,245,245); font:14px/24px "serif"; font-height:40px; letter-spacing:1px; border-radius:1px; cursor: pointer;}



nav#l-box{width:180px; float:left; background-color:rgb(250,250,250);}

li#l-1-box{height:60px; padding:0 0 0 10px;font-size:18px; line-height:60px; letter-spacing:2px; text-align:center; border-width:0 0 1px 0; border-style:solid; border-color:rgb(200,200,200);}



article{margin:0 0 0 180px;}

section#m-1-box{margin:10px 0 0 10px;}
section#m-1-box h2{font-size:18px; color:rgb(100,100,100); padding:10px 0 0 10px; margin:0 0 5px 0;  border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);}
section#m-1-box table{border-collapse:collapse;}
section#m-1-box ul{line-height:25px; margin:0 0 0 20px;}
section#m-1-box p{margin:0 0 0 600px; }


footer{width:510px; margin:10px auto 0 auto;}

div#f-1-box{text-align:center; margin:5px 0 0 0;}
div#f-1-box a{margin:0 1px 0 1px; font-size:12px;}



</style>



</head>




<body>


<header>
	<div id="hm-box">
		<div id="hm-1-box">
			<h1>
				<a href="">
					Cloud Invoice
				</a>
			</h1>
		</div>
		<div id="hm-3-box">
			<button type="login"  onclick="location.href='./logout.php'">ログアウト</button>
		</div>
		<div id="hm-2-box">
			<p>
				<?php echo $user_arr['nick_name'];?>様（ID:<?php echo $user_arr['user_id'];?>）
			</p>
		</div>
		<div id ="clear"></div>
	</div>
</header>

<nav id="l-box">
	<ul>
		<li id="l-1-box">HOME
		<li id="l-1-box">請求管理
		<li id="l-1-box">支払管理
		<li id="l-1-box">設定
	</ul>
</nav>

<article>

	<section id="m-1-box">
		<h2>
			更新履歴
		</h2>
		
		<table>
			<tr>
				<td>2014/03/26 12:06 CSVデータ登録</td>
			</tr>
			<tr>
				<td>2014/03/26 12:08 CSVデータ削除</td>
			</tr>
			<tr>
				<td>2014/03/26 19:06 CSVデータ登録</td>
			</tr>
			<tr>
				<td>2014/04/26 20:06 CSVデータ登録</td>
			</tr>
		</table>
		
		<p>
			<a href="">>>過去の更新履歴を見る。</a>
		</p>
		
	</section>



	<section id="m-1-box">
		<h2>
			お知らせ
		</h2>
		<ul>
			<li><a href="">2014/02/24 弊社サービス「Cloud Invoice」のサービス障害について</a>
			<li><a href="">2013/12/24 改ページ表示機能の不具合が修正されました。</a>
			<li><a href="">2013/02/24 改ページ表示機能が追加されました。</a>
			<li><a href="">2014/02/24 入金予定額と入金額の不具合が修正されました。
			<li><a href="">2014/02/24 単位表記に対応しました。
		</ul>

		<p>
			<a href="">>>過去のお知らせを見る。</a>
		</p>
		
		
	
	</section>


</article>

<div id="clear"></div>

<footer>
	<div id="f-1-box">
		<a href="">利用規約</a>
		<a href="">プライバシーポリシー</a>
		<br>
		<small>
			Cloud Invoice, Inc.
		</small>
	</div>
</footer>




</body>
