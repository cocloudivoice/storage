<?php
session_start();
	    require('/var/www/co.cloudinvoice.co.jp/html/db_con/sitecontrol.class.php');
        require("/var/www/co.cloudinvoice.co.jp/html/db_con/datatitlecontrol.class.php");
        require("/var/www/co.cloudinvoice.co.jp/html/db_con/datadispcontrol.class.php");
        require("/var/www/co.cloudinvoice.co.jp/html/db_con/datacontrol.class.php");
		require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
		$db_con = new dbpath();
		$pdo = $db_con->db_connect();
		$direction =$_GET['direction'];
		$_SESSION['num'] =$_GET['num'];

        //$record_kbn = $_GET['record_kbn'];
        $num_g = $_SESSION['num'];
		if($num_g < 10) {
			$num = "0{$num_g}";
		} else {
			$num = "{$num_g}";
		}
        //var_dump($direction);
		$title_con = new data_title_control();
		$disp_con  = new data_disp_control();
		$data_con  = new data_control();
		$site_con  = new site_control();
        $user_id = $_SESSION['user_id'];
        $site_id = $_SESSION['site_id'];
		$site_arr = $site_con -> site_select($pdo,$user_id,$site_id);
		if ($_SESSION['record_kbn']) {
			$record_kbn = $_SESSION['record_kbn'];
		} else {
			$record_kbn = '1';
			$_SESSION['record_kbn'] = '1';
		}		
		$disp_arr = $disp_con->data_disp_select($pdo,$user_id,$site_id,$record_kbn);
        $disp_arr['user_id'] = $_SESSION['user_id'];
        $disp_arr['site_id'] = $_SESSION['site_id'];
        $disp_arr['record_kbn'] = $_SESSION['record_kbn'];
		if ($site_arr['col_num']){
			$max_col = $site_arr['col_num'];
			$keep_now = '0';
		}else{
			$keep_now = '1';
		}
		//var_dump($disp_arr);

		for($k = 1;$k <= $max_col;$k++){
			if($k < 10) {
				$k_d = "0{$k}";
			} else {
				$k_d = "{$k}";
			}
			for($i = 1;$i <= 50;$i++){
				if($i < 10) {
					$num_d = "0{$i}";
				} else {
					$num_d = "{$i}";
				}
		        $num2 = 'data_seq'.$num_d;
			    if ($disp_arr[$num2] == $k){
		        //echo "num=".$num2;
			        $orderbody[$k_d] = $num_d;
			        break;
			    }
			}
		}
        //left
        if ($direction=='left'){
        //var_dump($direction);
        //var_dump($num);
	        $numm1 = (int)$num - 1;
			if($numm1 < 10) {
				$numm1 = "0{$numm1}";
			} else {
				$numm1 = "{$numm1}";
			}
            if (!$orderbody[$numm1] or $num <= '1'){
				$keep_now = '1';
            }else{
				$keep_now = '0';
			}
	        $bufdisp = $disp_arr['data_seq'.$orderbody[$num]];
        //var_dump($orderbody[$num]);
	        $disp_arr['data_seq'.$orderbody[$num]] = $disp_arr['data_seq'.$orderbody[$numm1]];
	        $disp_arr['data_seq'.$orderbody[$numm1]] = $bufdisp;
        }
        //right
        elseif ($direction=='right'){
        //var_dump($direction);
        //var_dump($num);
	        $nump1 = (int)$num + 1;
			if($nump1 < 10) {
				$nump1 = "0{$nump1}";
			} else {
				$nump1 = "{$nump1}";
			}
            if ($nump1 > 50 or !$orderbody[$nump1] or (int)$orderbody[$num] >= 50){
				$keep_now = '1';
            }else{
				$keep_now = '0';
			}
			//echo "orderbody[$nump1]=" . $orderbody[$nump1];
			//echo "keep_now=" . $keep_now;
	        $bufdisp = $disp_arr['data_seq'.$orderbody[$num]];
        //var_dump($orderbody[$num]);
	        $disp_arr['data_seq'.$orderbody[$num]] = $disp_arr['data_seq'.$orderbody[$nump1]];
	        $disp_arr['data_seq'.$orderbody[$nump1]] = $bufdisp;
	        $bufdisp = $disp_arr['data_seq'.$orderbody[$num]];
        //var_dump($orderbody[$num]);
	        //$disp_arr['data_property'.$orderbody[$num]] = $disp_arr['data_property'.$orderbody[$nump1]];
	        //$disp_arr['data_property'.$orderbody[$nump1]] = $bufdisp;
	        //$bufdisp = $disp_arr['data_property'.$orderbody[$num]];
        //var_dump($orderbody[$num]);
	        //$disp_arr['data_disp'.$orderbody[$num]] = $disp_arr['data_disp'.$orderbody[$nump1]];
	        //$disp_arr['data_disp'.$orderbody[$nump1]] = $bufdisp;
        }else{
            //echo "move_error";
        }
		if($keep_now != '1'){
			$result = $disp_con->data_disp_insert($pdo,$disp_arr);
		}
		$_SESSION['direction'] = NULL;
		$_SESSION['num'] = NULL;

		//require("make_table.php");
		header("Location:http://hikaku.jp/invoice/make_table.php");
		//header("Location:table_check.php?change_position=1");
    	exit();

?>