<?php session_start() ?>
<!DOCTYPE html>

<html Lang="ja">


<head>

<title>Cloud Invoiceへのお問い合わせ - Cloud Invoice「クラウドインボイス」請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>

<?php require("../iheader2.php");?>





<article id="operator">

	
	<h2>
		お問い合わせ
	</h2>

	<form action="../send" method="post">

	<div id="m-1-box">
		<h4>
			件名：
		</h4>
		<select name="subject">
		<option value="【法人様】質問">【法人様】質問</option>
		<option value="【法人様】その他">【法人様】その他</option>
		<option value="【税理士様】質問">【税理士様】質問</option>
		<option value="【税理士様】その他">【税理士様】その他</option>
		</select>
		<!--
		<input type="name" name="subject" value="<?php echo $_SESSION['subject'];?>" />
		-->
	</div>

	<div id="m-1-box">
		<h4>
			お名前：
		</h4>
		<input type="name" name="name" value="<?php echo $_SESSION['name'];?>" />
	</div>
	
	<div id="m-1-box">
		<h4>
			メールアドレス：
		</h4>
		<input type="address" name="mail" value="<?php echo $_SESSION['mail'];?>" />
	</div>
	
	<div id="m-1-box">
		<h5>
			お問い合わせ内容：
		</h5>
		<textarea name="message"><?php echo $_SESSION['mail'];?></textarea>
	</div>
	
	<div id="m-2-box">
		<img id="siimage" src="./php/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left">
		<a tabindex="-1"  href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = './php/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false">別の画像を表示</a><br>
		文字を入力<br />
		<input type="text" name="ct_captcha">
	</div>
	
	<div id="clear"></div>

	
	<div id="center">
		<input type="submit" value="送信する" />
	</div>
	</form>
</article>
<?php
	$_SESSION['name'] = "";
	$_SESSION['message'] = "";
	$_SESSION['subject'] = "";
	$_SESSION['mail'] = "";
?>


<?php require("../ifooter.php");?>
