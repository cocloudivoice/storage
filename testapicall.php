<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>API Test</title>
</head>
 
<body>
<?php
    $url ="http://localhost/invoice/testapi.php?a=".$_REQUEST['a']."&b=".$_REQUEST['b']."";
 
    // APIをリクエスト
    error_reporting(0);
    try {
        $result = simplexml_load_file($url);
    } catch (Exception $e) {
        error_reporting(E_ALL);
        return(0);
    } 
    error_reporting(E_ALL);
 
    if(isset($result->Error)){ 
        // エラー表示
        echo $result->Error;
    } else {
        // 計算結果を表示
        if(isset($result->Wa)){ 
            echo "和：".$result->Wa."<br/>";
        }
        if(isset($result->Sa)){ 
            echo "差：".$result->Sa."<br/>";
        }
        if(isset($result->Seki)){ 
            echo "積：".$result->Seki."<br/>";
        }
        if(isset($result->Sho)){ 
            echo "商：".$result->Sho."<br/>";
        }
    }
?>
 
</body>
</html>