<?php session_start();?>
<?php
//クラスファイルの読み込み
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
require_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$invoice_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$flag = "";//フラグの初期化
$words = "";//条件をここに入れる。
$sum_price = 0;
$sales_tax = 0;
$total_price = 0;
$t_price = 0;



//各テーブルからデータを取得する
//フラグに取得する内容を入れる。
$flag="invoice_data_send";

//データ取得の条件設定
if (isset($_SESSION['user_id'])) {
	$company_id = $_SESSION['user_id'];
}
if ($_REQUEST['destination_id'] != NULL) {
	$destination_id = $_REQUEST['destination_id'];
	$words .= " AND destination_id = ".$destination_id;
}
if ($_REQUEST['send_code'] != NULL) {
	$send_code = $_REQUEST['send_code'];
	$words .= " AND send_code = ".$send_code;
}
if ($_REQUEST['status'] != NULL) {
	$status = $_REQUEST['status'];
	$words .= " AND status = ".$status;
} else {
	//未発行か発行済ステータスのみを表示する。
	$words .= " AND (status = 1 OR status = 0) ";
}
if ($_REQUEST['insert_date'] != NULL) {
	$insert_date = str_replace(" ","",substr($_REQUEST['insert_date'],0,4)."/".substr($_REQUEST['insert_date'],4,2)."/".substr($_REQUEST['insert_date'],6,2));
	$words .= " AND insert_date = ".$insert_date;
}
if ($_REQUEST['billing_date'] != NULL) {
	$billing_date = $_REQUEST['billing_date'];
	$words .= " AND billing_date = ".$billing_date;
}
if ($_REQUEST['pay_date'] != NULL) {
	$pay_date = $_REQUEST['pay_date'];
	$words .= " AND pay_date = ".$pay_date;
}

$invoice_data_receive_arr = $invoice_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$invoice_data_receive_num = count($invoice_data_receive_arr);
$flag = "";//フラグの初期化


	//var_dump($_REQUEST);
	var_dump($invoice_data_receive_arr);
	echo $data_cnt = count($invoice_data_receive_arr);
	//var_dump($invoice_data_receive_arr_count);


?>

<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>請求書 - Cloud Invoice「クラウドインボイス」 - 請求書のやりとりや請求データをクラウドで管理できるＷｅｂサービス</title>




<style type="text/css">



html *{margin:0px; padding:0px;}
body { font-size:14px; line-height:20px; font-family: "Meiryo","メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","MS PGothic","Osaka",arial,helvetica,clean,sans-serif; color:rgb(50,50,50);}
a {text-decoration:none; color:blue;}
div#clear{clear:both;}
.right{float:right;}
.bol{font-weight:bold;}
.min{font-size:10px; color:rgb(100,100,100);}

article{width:790px; height:1000px;background:radial-gradient(ellipse 800px 1300px at 770px 560px, white 90%,aqua);}
section{width:600px; padding:40px 0 0 130px;}

div#top{height:170px;}

div#topleft{float:left; width:300px;}
div#topleft img{width:250px; margin:50px 0 0 20px;}

div#topright{margin:0 0 0 350px; color:rgb(180,180,180); font-weight:bold; font-size:12px;}
div#topright p{margin:0 0 10px 0;}



div#main1{height:250px;}
div#main1 h2{font-size:16px; margin:0 0 10px 0}
div#main1 ul{list-style-type:none; margin:0 0 30px 0;}
div#main1 p{margin:0 0 10px 0;}



div#main2{height:200px; width:530px; border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);}
div#main2 table{border-collapse:collapse;}
div#main2 tr{height:30px;}
div#main2 th{border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);}
div#main2 th#item{width:280px; text-align:left;}
div#main2 th#cost{width:100px; text-align:right;}
div#main2 th#amount{width:50px; text-align:right;}
div#main2 th#price{width:100px; text-align:right;}
div#main2 td{text-align:right;}
div#main2 td#item{text-align:left;}



div#main3{width:250px; margin:0 0 0 280px;}
div#main3 p{margin:10px 0 0 0;border-width:0 0 1px 0; border-style:solid; border-color:rgb(150,150,150);}



div#main4{margin:60px 0 0 80px;}
div#main4-left{float:left; margin:0 30px 0 0;}
div#main4-right{}
div#main4 p{margin:0 0 5px 0;}



@media print{
div#pdf{display:none;}
}

</style>




</head>



<body>

<section>

	<div id="top">


		<div id="topleft">
			<img src="/images/logo2.png"></img>
		</div>

		<div id="topright">
			<p>
				クラウドインボイス株式会社
			</p>
			<p>
				〒180-0004<br>
				東京都武蔵野市吉祥寺本町1-13-6<br>
				芝第3アメレックスビル901<br>
				TEL：03-0000-0000<br>
				FAX：03-0000-0000<br>
			</p>

		</div>




	<div id="clear"></div>
	</div>

	<div id="main1">
		<h2>請求書</h2>
		<ul>
			<li>請求書番号　　<?php echo $invoice_data_receive_arr[0]['invoice_code'];?></li>
			<li>請求日　　　　<?php echo $invoice_data_receive_arr[0]['billing_date'];?></li>
		</ul>
		<p>
			〒108-0073<br>
			東京都港区三田3-12-18<br>
			芝第3アメレックスビル8F
		</p>

		<p>
			サンプル得意先<br>
			担当部署　担当者
		</p>


	</div>


	<div id="main2">
		<table>
			<tr>
				<th id="item">品目</th>
				<th id="cost">単価</th>
				<th id="amount">数量</th>
				<th id="price">価格</th>
			</tr>
			<?php 
			for ($i = 0; $i < $data_cnt; $i++) {
			?>
			<tr>
				<td id="item"><?php echo $invoice_data_receive_arr[$i]['product_name'];?></td>
				<td><?php echo number_format((int)$invoice_data_receive_arr[$i]['unit_price']);?></td>
				<td><?php echo number_format((int)$invoice_data_receive_arr[$i]['quantity']);?></td>
				<td><!--<?php echo number_format((int)$t_price = $invoice_data_receive_arr[$i]['unit_price']*$invoice_data_receive_arr[$i]['quantity']);?>--><?php echo number_format((int)$invoice_data_receive_arr[$i]['total_price']);?></td>
			</tr>
			<?php
				$sum_price = $sum_price + $invoice_data_receive_arr[$i]['total_price'];
				$sales_tax += round($invoice_data_receive_arr[$i]['total_price'] * $invoice_data_receive_arr[$i]['sales_tax'] / 100);
				/*
				$total_price += number_format((int)$invoice_data_receive_arr[$i]['total_price']);
				*/
				$total_price += $invoice_data_receive_arr[$i]['total_price'];
			}
			?>
		</table>

	</div>




	<div id="main3">
		<p><span class="right"><?php echo number_format($sum_price);?></span>小計</p>
		<p><span class="right"><?php echo number_format($sales_tax);?></span>消費税</p>
		<p class="bol"><span class="right"><?php echo number_format($total_price);?></span>合計</p>

	</div>



	<div id="main4">
		<div id="main4-left">
			<p>お支払期限</p>
			<p>振込先<br>
			<span class="min">お振込み手数料は御社<br>ご負担にてお願い致します。</span>

		</div>
		<div id="main4-right">
			<p>
				2014/07/31
			</p>
			<p>
				○○銀行○○支店<br>
				普通口座　×××××××<br>
				口座名義　クラウドインボイス（カ
			</p>
		</div>
	</div>

</section>


</article>
<div id="pdf">
	<a href="./invoice2pdf.php">pdf化</a>
</div>
</body>


