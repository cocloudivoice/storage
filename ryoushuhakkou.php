<?php require("header.php");?>


<title>領収書・レシート管理 - Cloud Invoice</title>

<script>

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box3" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box4" ).datepicker();
});

function OpenkCommonSearchFrame(){
	document.getElementById("kCommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}

function FrameHeight(){
	window.setTimeout("window.parent.frames['kCommonSearch'].test()","600");
}

</script>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>

<article>

	<section id="m-1-box">
		<h2>
			領収書・レシート管理
		</h2>
		
		<p>
			条件を指定してください。
		</p>
		<form action="checkryoushukanri" method="GET" name="seikyuhakkou_form">
			<div id="ID">
				<h3>共通コード</h3>
				<input type="text" id="Kyoutu_No" name="destination_id" class="kCommonSearch" name="destination_id_visible" onkeyup="OpenkCommonSearchFrame();FrameHeight();" onChange="OpenkCommonSearchFrame()" value="<?php if ($destination_company_id != "") {echo $destination_company_id;}?>"  placeholder="共通コード、会社名、住所、メールアドレスで検索する" autocomplete="off" />
			</div>
			
			<div id="status">
				<h3>状態</h3>
				<label><input name="no_journalized" type="checkbox" value="1" checked /> 仕訳未作成</label>
				<label><input name="journalized" type="checkbox" value="1" checked /> 仕訳作成済</label>
			</div>
				
			<div id="hiduke">
				<h3>請求書日付</h3>
				<input id="box1" type="text" name="billing_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box2" type="text" name="billing_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="hiduke">
				<h3>支払期限日</h3>
				<input id="box3" type="text" name="pay_date_from" />
				<p>(yyyymmdd) - </p>
				<input id="box4" type="text" name="pay_date_to" />
				<p>(yyyymmdd)</p>
			</div>
			
			<div id="shiharaEx">
				<button onclick="submit();">確認画面に進む ></button>
			</div>
		</form>
	</section>






</article>

<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(10)"></div>

<?php require("footer.php");?>