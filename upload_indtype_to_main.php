<?php
session_start();

//必要なクラスを読み込む
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
include_once("/var/www/storage.cloudinvoice.co.jp/html/cfiles/ReadCsvFile.class.php");
//自動仕訳用クラスの読み込み
include_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/autojournalizecontrol.class.php');
//自動仕訳用コントローラーの準備

//DBに接続
$db_con = new dbpath();
$pdo = $db_con -> db_connect();
$pdo_aj = $db_con -> change_db_connect();

//csv読み込み用コントローラーの準備
$read_csv_con = new ReadCsvFile();
//自動仕訳用コントローラーの準備
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$success_num = 0;
$all_num = 0;
$insert_num = 0;
$skip_num = 0;
$company_num = 0;
$data_count = 0;
$update_num = 0;
$filepath = "/var/www/storage.cloudinvoice.co.jp/html/pdf/nru";
$file_name = htmlspecialchars($_GET['fn'],ENT_QUOTES);
//var_dump($file_name);

if ($file_name == "") {
	//$file_name = "indtypes.csv";
	//ファイル名が無かったらこっちの処理はしない。
	header("Location:./indtype_s_upload;");
}

//CSVファイルを渡して読み取ったCSVファイルを取得する
$csv = $read_csv_con -> readCsvFile($filepath,$file_name);
$all_num = count($csv) - 1;

if ($all_num != 0) {
	//var_dump($csv);
	//ob_start();
	//CSVデータを一行ずつ取り出してDBに登録する。
	foreach ($csv as $value) {
		
		//変数の初期化
		
		if ($value[0] == "小分類入力") {
			//var_dump($value);
			//カラム名から列番号を取得する
			for ($k = 0; $k < 12; $k++) {
				switch ($value[$k]) {
					case "小分類":
						$ind_s_num = $k;
						break;
					case "勘定科目1":
						$act1_num = $k;
						break;
					case "勘定科目2":
						$act2_num = $k;
						break;
		   			case "勘定科目3":
						$act3_num = $k;
						break;
					case "勘定科目4":
						$act4_num = $k;
						break;
					case "勘定科目5":
						$act5_num = $k;
						break;
					case "勘定科目6":
						$act6_num = $k;
						break;
					case "勘定科目7":
						$act7_num = $k;
						break;
					case "勘定科目8":
						$act8_num = $k;
						break;
					case "勘定科目9":
						$act9_num = $k;
						break;
					case "勘定科目10":
						$act10_num = $k;
						break;
					default :
						 "default";
						break;
				}
			}
		} else {
			//勘定科目の配列と存在フラグの初期化
			$account_title = array();
			$exist_flag = 0;
			
			$industry_type_s = $value[$ind_s_num];
			$account_title[] = $value[$act1_num];
			$account_title[] = $value[$act2_num];
			$account_title[] = $value[$act3_num];
			$account_title[] = $value[$act4_num];
			$account_title[] = $value[$act5_num];
			$account_title[] = $value[$act6_num];
			$account_title[] = $value[$act7_num];
			$account_title[] = $value[$act8_num];
			$account_title[] = $value[$act9_num];
			$account_title[] = $value[$act10_num];
			
			for ($i= 0;$i < 10;$i++) {
				//echo "<br/>".$i."<br/>";
				$flag = "main_select";
				$table_name = "MAIN";
				//$aj_words = "`product_name` = '".$industry_type_s."'";
				//var_dump($account_title);
				$aj_words = "`product_name` = '".$industry_type_s."' AND `account_name` = '".$account_title[$i]."' ";
				$main_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
				//var_dump($main_arr);
				if (count($main_arr) == 0) {
				echo	$sql = "
					INSERT INTO `MAIN` (
					`product_name`,`account_name`,`number_of_times`
					) VALUES (
					'".$industry_type_s."','".$account_title[$i]."',0
					)
					";
					$auto_journalize_con -> insert_auto_journalize_sql($pdo_aj,$sql);
					$insert_num++;
					
				} else {
					//echo count($main_arr);
					$skip_num++;
				}
			}
		}
	}
}



echo "<br/>\r\n";
echo "新規データ作成件数:".$insert_num;
echo "<br/>\r\n";
echo "登録済み件数:".$skip_num;
echo "<br/>\r\n";
echo "データ件数：".($insert_num + $skip_num);
echo "<br/>\r\n";
fclose($temp);
unlink($file);

//ob_end_flush();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>会員登録の確認画面</title>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}
</style>
<title>非正規ユーザーアップロード完了</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">MAINテーブルへの小分類追加完了</h1>
		<h2>MAINテーブルへの小分類追加が完了しました。</h2><br/>
		<p></p>
		<button onclick="window.location.href='./indtype_s_upload'">小分類アップロード画面に戻る</button>
	</div>
</body>
</html>