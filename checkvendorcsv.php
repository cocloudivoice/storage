<?php require("header.php");?>


<title>送付先CSV確認画面 - Cloud Invoice</title>


<?php
session_start();
setlocale(LC_ALL, 'ja_JP.UTF-8');

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$db_con = new db_control();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//変数の宣言
$company_id = $_SESSION['user_id'];

//CSVフォーマットのカラム数
$colms = $_SESSION['colms'];

$file = '../files/'.$company_id.'/csvfiles/'.$_SESSION['file_name'];

if (isset($file)) {
	  // 読み込むファイル名の指定
	  // ファイルポインタを開く
	  $fp = fopen( $file, 'r' );

	$data = file_get_contents($file);
	$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
	$temp = tmpfile();
	$csv  = array();

	fwrite($temp, $data);
	rewind($temp);
}

while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
  $csv[] = $data;
}

?>
<style>
.bk_red {
	background-color:red;
}
</style>
<article>

	<section id="m-3-box">
		<h2>
			請求CSV－確認画面
		</h2>
		
		<table>
			<tr id="first">
				<th>送付先コード</th>
				<th>共通コード</th>
				<th>事業署名</th>
				<th>担当者</th>
				<th>所属部署</th>
				<th>担当者役職</th>
				<th>郵便番号</th>
				<th>都道府県</th>
				<th>住所１</th>
				<th>住所２</th>
				<th>電話番号</th>
				<th>FAX番号</th>
				<th>メールアドレス</th>
				<!--
				<th id="meisai">明細数</th>
				-->
			</tr>
<?php
foreach ($csv as $value) {
	//var_dump($value);echo "<br />\n";
	if ( $value[1] == "送付先コード" ) {
	} else {
?>

			<tr>
<!--
				<td id="atesaki" <?php if ($value[0] != "" && $company_name[0]['company_name'] == "") { echo 'class="bk_red"';}?>><?php echo $company_name[0]['company_name'];?></td>
-->
				<td id="kanriCD"><?php echo $value[1];?></td>
				<?php
				unset($company_name);
				if ( $value[1] != "" ) {
					$flag = "vendor_data_csv";
					$words = str_replace(array(" ","　"),"",$value[1]);
					$client_id = $value[0];
					$company_name = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
				}
				?>
<!--
				<td id="atesaki" <?php if ($value[1] != "" && $company_name2[0]['company_name'] == "") { echo 'class="bk_red"';}?>><?php echo $company_name2[0]['company_name'];?></td>
-->
				<td id="seikyuBi"><?php echo $value[2];?></td>
				<td id="seikyuNo"><?php echo $value[3];?></td>
				<td id="seikyuName"><?php echo $value[4];?></td>
				<td id="thisTanto"><?php echo $value[5];?></td>
				<td id="siharaibi"><?php echo $value[6];?></td>
				<td id="thisKoza"><?php echo $value[7];?></td>
				<td id="thisKoza"><?php echo $value[8];?></td>
				<td id="thisKoza"><?php echo $value[9];?></td>
				<td id="siharaibi"><?php echo $value[10];?></td>
				<td id="siharaibi"><?php echo $value[11];?></td>
				<td id="seikyuNo"><?php echo $value[12];?></td>
				<td id="seikyuNo"><?php echo $value[13];?></td>
			</tr>
<?php
	}
}

fclose($temp);

?>
		
		
		</table>

		<form action="./read_csv_vendor.php" method="post" name="read_csv_vendor">
			<input type="hidden" name="company_id" value="<?php echo $company_id;?>">
			<input type="hidden" name="colms" value="14">
			<!--
			<input type="button" name="kakunin" onclick="submit()" value="CSV取り込み" />
			-->
			<button name="kakunin" onclick="submit()">CSV取り込み</button>
			<button type="button" onclick="location.href='./vendor'">キャンセル</button>
		</form>
					
	</section>
</article>


<?php require("footer.php");?>