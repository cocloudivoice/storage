<?php require("main_read.php");?>
<!DOCTYPE html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
		<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//storage.cloudinvoice.co.jp/images/logo.jpg" />
		<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
		<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
		<meta property="og:locale" content="ja_JP" />
		<meta name="google-site-verification" content="un5bQON1I7C90gBjKdw8M0OC7-1Ws8IENGdkIh8a7YA" />
		<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="//storage.cloudinvoice.co.jp/" />
		<script src="//storage.cloudinvoice.co.jp/js/jquery-2.1.1.min.js" type="text/javascript" charset="UTF-8"></script>

		<script type="text/javascript" src="//storage.cloudinvoice.co.jp/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="//storage.cloudinvoice.co.jp/js/jquery-ui-i18n.js"></script>
		<link rel="stylesheet" href="//storage.cloudinvoice.co.jp/css/jquery-ui.min.css" />
		
		<script>
			//カレンダーから日付入力をするjquery
			jQuery(function($) {
				try {
					$(document).on('focus', '.datepicker', function () {
						$(".datepicker").datepicker();
						$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
					});
				} catch (e) {
					console.log ("Outer catch caught " + e.message);
				}
			});
		</script>




<script type="text/javascript">

function CloseFrame(n){
	document.getElementById("fadeLayer").style.visibility = "hidden";
	document.getElementById("noneLayer").style.visibility = "hidden";
	switch(n){
		case 1 : document.getElementById("default").style.display = "none";
			break;
		case 2 :document.getElementById("report").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			break;
		case 3 : document.getElementById("Mail").style.display = "none";
			document.getElementById("Post").style.display = "none";
			document.getElementById("Issue").style.display = "none";
			document.getElementById("Delete").style.display = "none";
			break;
		case 4 : document.getElementById("souhuEditFrame").style.display = "none";
			break;
		case 5 : document.getElementById("changePass").style.display = "none";
			document.getElementById("changeAd").style.display = "none";
			document.getElementById("leave").style.display = "none";
			break;

		case 6 : document.getElementById("Freeplan").style.display = "none";
			document.getElementById("Standardplan").style.display = "none";
			document.getElementById("Premiumplan").style.display = "none";
			document.getElementById("Passerror").style.display = "none";
			break;

		case 7 : document.getElementById("Idetail").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			document.getElementById("Interval").style.display = "none";
			break;

		case 8 : document.getElementById("SouhuList").style.display = "none";
			document.getElementById("ChangeInvoice").style.display = "none";
			document.getElementById("CommonSearch").style.display = "none";
			break;

		case 9 : document.getElementById("kSouhuList").style.display = "none";
			document.getElementById("kCommonSearch").style.display = "none";
			break;

		case 10 : document.getElementById("kCommonSearch").style.display = "none";
			break;

		case 11 : document.getElementById("CommonSearch").style.display = "none";
			break;

		case 12 : document.getElementById("Transfer").style.display = "none";
			break;
	}
}

<?php
if (isset($af_id)) {
	echo "function chClient(client) {";
	echo 'document.getElementById("ch_client_form").submit();';
	echo "}";
}
?>

</script>

<style type="text/css"></style>
<link rel="stylesheet" href="../css/header.css" type="text/css" />


<LINK REL="SHORTCUT ICON" HREF="../images/5423cb7c9f8b2.ico"> 

</head>
<body>

<header>
	<div id="ih-1-box">
		<div id="ih-1-0-box">
			<div id="ih-1-1-box">
				<?php if (isset($acm_id)) { echo $user_company_name[0]['company_name']." - ".$af_arr['name'];} else if (isset($af_id)) { echo $af_arr['name']."-".$user_company_name[0]['company_name'];} else {echo $user_company_name[0]['company_name'];}?>（ <?php if (isset($acm_id)) { echo substr($user_arr['user_id'],0,4)." ".substr($user_arr['user_id'],4,4)." ".substr($user_arr['user_id'],8,4)." - ".substr($af_arr['id'],0,2)." ".substr($af_arr['id'],2,4)." ".substr($af_arr['id'],6,4);} else if (isset($af_id)) { echo substr($af_arr['id'],0,2)." ".substr($af_arr['id'],2,4)." ".substr($af_arr['id'],6,4);?> - <?php echo substr($user_arr['user_id'],0,4)." ".substr($user_arr['user_id'],4,4)." ".substr($user_arr['user_id'],8,4);} else {echo substr($user_arr['user_id'],0,4)." ".substr($user_arr['user_id'],4,4)." ".substr($user_arr['user_id'],8,4);}?> )
				<?php
				if (!isset($af_id)) {
				?>
					<a href="//storage.cloudinvoice.co.jp/invoice/logout"><span style="float:right; color:white;">ログアウト</span></a>
				<?php
				}
				?>
			</div>
			<div id="ih-1-2-box" <?php if (!isset($af_id) || isset($acm_id)){echo 'style="display:none;";';}?>>
				<form id="ch_client_form" action="" method="post">
				<p><?php if (!isset($af_id)){ echo "現在の表示アカウント：";}?></p>
					<select name="af_client_id" onChange="chClient();">
					<?php
						for ($i = 0;$i < $af_client_num;$i++) {
							$af_client_arr[$i]["client_id"];
							$af_client_id;
							if ($af_client_arr[$i]["client_id"] == $af_client_id) {
								$af_client_selected = selected;
							} else {
								$af_client_selected = "";
							}
							echo '<option value="'.$af_client_arr[$i]["client_id"].'" '.$af_client_selected.' >'.$af_client_arr[$i]["company_name"].'</option>';
						}
					?>
					</select>
				</form>
			</div>
			<?php 
			//CI事務所と五島洋税理士事務所のみにテキスト化を表示する。
			//CI-5129391867 //五島-8421716526 //3308277917//3308277917
			if ($af_id == "5129391867"|| $af_id == "3308277917") {
			?>
			<div id="ih-1-3-box" >
				<a href="//storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all">画像テキスト化</a>
			<?php
				if ($af_id == "5129391867"|| $af_id == "3308277917") {
			?>
				　<a href="//storage.cloudinvoice.co.jp/invoice/rakutasu_imgs_to_text_upload_all">楽たす請求書</a>
			<?php
				}
				if ($af_id == "5129391867"|| $af_id == "3308277917") {
			?>
					<a href="//storage.cloudinvoice.co.jp/invoice/imgs_to_text_convert_upload_all">テキスト変換</a>
			<?php
				}
			?>
			</div>
			<?php
			}
			?>
			<?php 
			if (isset($af_id) || isset($acm_id)) {
			?>
			<div id="ih-1-3-box" >
				<a href="//storage.cloudinvoice.co.jp/invoice/change_password2">パスワード変更</a>
				<a href="//storage.cloudinvoice.co.jp/invoice/logout_af">ログアウト</a>
			</div>
			<?php
			}
			?>
		</div>
	</div>
	<div id="ih-2-box">
		<div id="ih-2-1-box">
			<div id="ih-2-1-1-box" class="ih-2-1-box" ontouchstart="">
				<a href="//storage.cloudinvoice.co.jp/invoice/main">
					<img src="/images/logo2.png"/>
				</a>
			</div>
			<a href="//storage.cloudinvoice.co.jp/invoice/main" style="color:black;">
					<div id="ih-2-1-2-box" class="ih-2-1-box">
				証憑管理
				<ul id="ih-3-1-2-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/syouhyoukanri"><li>証憑管理</li></a>
				</ul>
			</div>
			<div id="ih-2-1-3-box" class="ih-2-1-box">
				-
			</div>
			<div id="ih-2-1-4-box" class="ih-2-1-box">
				-
			</div>
			<div id="ih-2-1-5-box" class="ih-2-1-box">
				-
			</div>
			<div id="ih-2-1-6-box" class="ih-2-1-box">
				設定
				<ul id="ih-3-1-6-box" class="ih-3-box">
					<a href="//storage.cloudinvoice.co.jp/invoice/office"><li>基本設定</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/office2"><li>帳票設定</li></a>
					<a href="//storage.cloudinvoice.co.jp/invoice/user"><li>ログイン設定</li></a>
				</ul>
			</div>
			<div id="ih-2-1-7-box" class="ih-2-1-box" ontouchstart="">
				-
			</div>
		</div>
	</div>


</header>




