<?php
session_start();
//print "session_id = " . session_id() . "<br>"; 
//echo "テストここから<br />";
//echo $_SESSION['test'];
//echo "<br />テストここまで<br />";

//必要なクラスを読み込む
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/sitetitlecontrol.class.php");
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/sitecontrol.class.php");
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");

//変数の宣言
$user_id;
$site_row;
$site_arr = array();
$site_count_arr = array();
$user_arr = array();

//user_idチェック
if (isset($_SESSION['user_id'])) {
	$user_id = (int)$_SESSION['user_id'];
} else {
	header("Location:http://hikaku.jp/invoice/logout.php");
	exit();
}
if (isset($_SESSION['max_col'])) {
	echo $_SESSION['max_col'];
	echo $_SESSION['max_row'];
}
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$site_con = new site_control();
$title_con = new site_title_control();
$user_con = new user_control();

//各テーブルからデータを取得する
$site_count_arr = $site_con->site_count($pdo,$user_id);
$site_arr = $site_con->site_select_all($pdo,$user_id);
$site_row = $site_count_arr['count(*)'];
$user_arr = $user_con->user_select_id($pdo,$user_id);



//var_dump($site_row);
/*
 // 出力する画像サイズの指定
$width = 100;
$height = 100;

// サイズを指定して、背景用画像を生成
$canvas = imagecreatetruecolor($width, $height);

// コピー元画像の指定
$targetImage = $user_arr['user_img'];

// ファイル名から、画像インスタンスを生成
$image = imagecreatefromjpeg($targetImage);
// コピー元画像のファイルサイズを取得
list($image_w, $image_h) = getimagesize($targetImage);

// 背景画像に、画像をコピーする
imagecopyresampled($canvas,  // 背景画像
                   $image,   // コピー元画像
                   0,        // 背景画像の x 座標
                   0,        // 背景画像の y 座標
                   0,        // コピー元の x 座標
                   0,        // コピー元の y 座標
                   $width,   // 背景画像の幅
                   $height,  // 背景画像の高さ
                   $image_w, // コピー元画像ファイルの幅
                   $image_h  // コピー元画像ファイルの高さ
                  );

// 画像を出力する
imagejpeg($canvas,           // 背景画像
          //"./output.jpg",    // 出力するファイル名（省略すると画面に表示する）
          100                // 画像精度（この例だと100%で作成）
         );

// メモリを開放する
imagedestroy($canvas); 
*/

?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" /><!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
</script><script src="selectivizr.js">
</script><![endif]-->
<title>マイページ - manzaX</title>
<meta http-equiv="Content-Script-Type" content="text/javascript">
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/imgLiquid-min.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">google.load("jquery", "1.2");</script>
<script>
	
	
(function($) {

    $.fn.fixAspect = function(option) {

        var init = function(){
            var self = $(this);
            // 表示時の大きさ
            var _height = $(this).attr('height');
            var _width = $(this).attr('width');
            var trueHeight ,trueWidth  = 0;

            var img = new Image();
                img.onload = function(){
                trueHeight = this.height;
                trueWidth = this.width;

                // 縦またはどちらも同じの場合
                if(_height >= _width){
                    d = _height / trueHeight;
                    trueHeight = trueHeight * d;
                    trueWidth = trueWidth * d;
                    self.attr('height',trueHeight);
                    self.attr('width',trueWidth);
                }
                // 横の場合
                else{
                    d = _width / trueWidth;
                    trueHeight = trueHeight * d;
                    trueWidth = trueWidth * d;
                    self.attr('height',trueHeight);
                    self.attr('width',trueWidth);
                }

                // とどめたいサイズ内におさまっていない場合
                if(trueWidth > _width || trueHeight > _height){
                    // 縦横どちら基準で縦横比を変更するのか

                    // 縦またはどちらも同じの場合
                    if(trueHeight >= trueWidth){
                        d = _height / trueHeight;
                        trueHeight = trueHeight * d;
                        trueWidth = trueWidth * d;
                        self.attr('height',trueHeight);
                        self.attr('width',trueWidth);
                    }
                    // 横の場合
                    else{
                        d = _width / trueWidth;
                        trueHeight = trueHeight * d;
                        trueWidth = trueWidth * d;
                        self.attr('height',trueHeight);
                        self.attr('width',trueWidth);
                    }

                    if(trueWidth > _width){
                        d = _width / trueWidth;
                        trueHeight = trueHeight * d;
                        trueWidth = trueWidth * d;
                        self.attr('height',trueHeight);
                        self.attr('width',trueWidth);
                    }

                    if(trueHeight > _height){
                        d = _height / trueHeight;
                        trueHeight = trueHeight * d;
                        trueWidth = trueWidth * d;
                        self.attr('height',trueHeight);
                        self.attr('width',trueWidth);
                    }

                }

                if(option.parentFixture == true){
                    self.parent().css('height',_height);
                    self.parent().css('width',_width);
                    self.parent().css('text-align','center');
                    self.css('padding-top',(_height/2)-((trueHeight)/2)+'px');
                }
            }
            img.src = $(this).attr('src');

        }

        this.each(function(){
            init.apply($(this));
        })

        return this;
    };
})(jQuery);

(function($){
    $('.fixAspect').fixAspect({parentFixture: true});
})(jQuery);

function good_add() { //追加処理
document.getElementById("GoodBox").innerHTML = '<input type="hidden" name="judge" value="good"><input type="hidden" name="table_name" value="USER_TABLE">';

	form3.submit();
}
function bad_add() { //追加処理
document.getElementById("GoodBox").innerHTML = '<input type="hidden" name="judge" value="bad"><input type="hidden" name="table_name" value="USER_TABLE">';

	form3.submit();
}

</script>


<style type="text/css">
html *{
	margin:0px;
	padding:0px;
}
body {
  font-family: "Meiryo", "メイリオ", "ヒラギノ角ゴ Pro W3", sans-serif;
}

#form_wrapper form *{
	font-size:1em;
}
#content{
	margin-left:auto;
	margin-right:auto;
	width:980px;
	height:auto;
	margin-bottom:20px;
	margin-top:20px;
	padding:5px;
	background-color:#FFECCF;
	
}
#form_wrapper{
	margin-left:auto;
	margin-right:auto;
	width:800px;
	height:auto;
	margin-bottom:20px;
	margin-top:20px;
	padding:5px;
	background-color:#FFF;
}
body {
	text-align:center;
	vertical-align:middle;
	background-color:#FFB84E;
}
.button{
	width:130px;
	height:60px;
	background-color:#BBB;
	background-image:url(../images/base/button.png);
}
.invisible{
	display:none;
}
.center {
	text-align:center;
	margin-left:auto;
	margin-right:auto;
}
.block {
	display:block;
}
#button_new {
	
}
#incentive {
	margin-left:auto;
	margin-right:auto;
	width:800px;
	height:200px;
	margin-bottom:20px;
	margin-top:20px;
	padding:5px;
	background-color:#FFF;
}
.border {
	border:2px solid #000;
}

#profile_box {
	position:relative;
	width:800px;
	height:400px;
	margin-left:auto;
	margin-right:auto;
	margin-top:40px;
	margin-bottom:10px;
	padding:5px;
	background-color:#FFF;
}
#profile_box a {
	text-decoration:none;
	font-weight:bold;
	color:#000;
}
#icon {
	position:relative;
	float:left;
	width:200px;
	height:auto;
	margin-left:10px;
	margin-top:10px;
}
#box2 {
	position:relative;
	float:left;
	width:500px;
	height:50px;
	margin-left:40px;
	margin-top:10px;
	background-color:#FFF;
}
#profile {
	position:relative;
	float:left;
	width:500px;
	height:250px;
	margin-left:40px;
	margin-top:10px;
}
.clear {
	clear:both;
}
#logout{
	float:right;
}
#logout{
	display:block;
	position:relative;
	margin-right:50px;
	margin-bottom:20px;
}
#user_img{
	text-align:center;
	margin-left:auto;
	margin-right:auto;
}

.mbottom10{
	margin-bottom:10px;
}
.mtop10{
	margin-top:10px;
}
.mtop20{
	margin-top:20px;
}
.invisible{display:none;}


/*ここからボタン用*/
/*
 * main
 */

body {
  font-family: "Meiryo", "メイリオ", "ヒラギノ角ゴ Pro W3", sans-serif;
}

h1 {
  font-size: 16px;
}

#pic div {
	margin-left:auto;
	margin-right:auto;
	text-align:center;
}

.button-blue {
	background-color:#FFECCF;
	color:#999;
	width: 144px;
	height: 25px;
	cursor: pointer;
}_hikaku.jp	

#dummy-file-button {
    display: none;
}

/*ここまでボタン用*/


.image-resize {
	max-width: 180px;
	max-height:180px;
 	-ms-interpolation-mode: bicubic;
}

#img-container {
	overflow:hidden;
}
.button {
	width:130px;
	height:60px;
	background-color:#BBB;
	background-image:url(../images/base/button.png);
}
#profile_box #button:hover,#profile_box #button:active,#profile_box #button:focus {
	background-color:#FCC;
	color:#555;
}

#profile_box #button {
	background-color:#FFECCF;
	color:#999;
	margin-bottom:10px;
}
</style>

</head>
<body>

	<div id="content" class="border">
		<h1 class="mtop10"><span class="invisible">マイページ</span></h1><p id="logout"><a id="logout" href="./logout.php">ログアウト</a></p>
		<div id="profile_box" class="border">
			<h2 class="mtop10">プロフィールボックス</h2>
			<div id="icon" class="border">
				<h3 class="invisible">画像</h3>
				<div id="img-container" style="display:block;">
					<img id="user_img" class="mtop20 image-resize fixAspect" src="<?php if(isset($user_arr['user_img'])){echo $user_arr['user_img'];}else{echo '../images/base/no_image.jpg';} ?>"/>
				</div>
				<p id="username" class="mtop10 mbottom10"><?php echo $user_arr['nick_name']; ?></p>
				<div id="pic">
					<div class="button-blue" id="button" onclick="button_click(this)">画像を替える</div>
					<form name="form2" method="post" action="http://hikaku.jp/invoice/upload_image.php" enctype="multipart/form-data">
						<input id="dummy-file-button" class="invisible" type="file" name="imgurl" size="10" onchange="submit()" >
					</form>
				</div>
				<p id="" class="mtop10 mbottom10"><a id="button" href="http://hikaku.jp/invoice/mod_userinfo.php">ユーザーデータ変更</a></p>
			</div>
					
			<div id="box2" class="border" style="padding-left:10px;">
				<div id="goodBtn"><input type="button" name="good" size="10" style="width:60px;background-color:blue;color:white;cursor:pointer;" value="good" onclick="good_add()"/><input type="button" name="bad"  style="width:60px;background-color:red;cursor:pointer;" value="bad" onclick="bad_add()">
				<?php echo "<span>信頼できる：".$user_arr['good']."</span><span>あまり信頼できない：".$user_arr['bad']."</span>";?>
				</div>
				<!--
				<p><?php echo $user_arr['nick_name']; ?></p>
				-->
					<?php //var_dump($user_arr); ?>
			</div>
			<div id="profile" class="border" style="padding-left:10px;">
				<h3>プロフィール</h3>
				<p class="mtop10" style="text-align:left;p">
				<?php
					if (isset($user_arr['profile'])) {
						$user = nl2br($user_arr['profile']);
						echo $user;
					}
				?>
				</p>
			</div>
		</div>
		<br class="clear" />
		<div id="incentive" class="border">
			<h2>インセンティブ情報</h2>
				<?php
					if(isset($incentive)) {
						echo "<p class=\"mtop10\">インセンティブ情報</p>";
					} else {
						echo "<p class=\"mtop10\"> 情報がありません </p>";
					}
				
				  ?>
		</div>
		<div id="form_wrapper" class="form_wrapper border">
			<h2 id="addBox" class="mbottom10"><span>作成済み比較一覧</span></h2>
			<?php
				if($site_row>0) {
					print("<table id=\"sitename_table\" style=\"margin-left:auto;margin-right:auto;\">");
					for($i=0 ; $i < (int)$site_row ; $i++) {
						if (($i % 3) == 0) {
							echo "<tr>";
						}
				        $title_arr = $title_con->site_title_select($pdo,$user_id,$site_arr[$i]["site_id"]);
						//var_dump($site_arr[$i]["site_id"]);
					    $title_str = mb_convert_encoding($title_arr["site_title"], "UTF-8", "auto");
					    $site_id = ($title_arr["site_id"]);
				        //現在までに作られたページの一覧を表示する        
						//print('<p><a href="http://hikaku.jp/invoice/make_menu.php?site_title='.$title_str.'">'.$title_str.'</a></p>');
						print('<td style="padding-left:5px;padding-right:5px;text-align:left;"><a href="http://hikaku.jp/invoice/main.php?site_id='.$site_id.'">'.$title_str.'</a>');
						//$_SESSION['site_id'] = $site;
						//$_SESSION['site_title'] = $str;
						print("</td>\n");
						if (($i % 3) == 2) {
							echo "</tr>\n";
						}
					}
					print("</table>");
				} else {
					print "<br/>\n<p>なし</p>\n";
				}
			?>
		</div>
		<p id="button_new" ><a class="button center block" href="./make_site.php"><span class="invisible">あたらしくサイトをつくる</span></a></p>
	</div>
		<form name="form3" action="./goodbad.php" method="get" >
		<input type="hidden" name="ret_num" value="1">
		<div id="GoodBox">
		</div>
		</form>
</body>
</html>
<?php
$_SESSION['login_user_id'] = $user_arr['user_id'];
$_SESSION['nick_name'] = $user_arr['nick_name'];
?>
<script type="text/javascript">
var button = document.getElementById("button");
var dummyFileButton = document.getElementById("dummy-file-button");

function button_click() {
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    dummyFileButton.dispatchEvent( evt );
};
</script>
