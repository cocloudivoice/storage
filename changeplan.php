<?php require("header.php");?>


<title>プラン変更 - Cloud Invoice</title>


<?php
session_start();

//クラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/dbcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/usercontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//必要なコントローラーの準備
$maindb_con = new db_control();
$user_con = new user_control();
$company_con = new company_control();

//var_dump($_SESSION);
if (isset($_REQUEST['koumoku'])) {$koumoku = $_REQUEST['koumoku'];}
if (isset($_REQUEST['password'])) {$password = htmlspecialchars($_REQUEST['password'],ENT_QUOTES);}
if (isset($_REQUEST['mode'])) {$mode = $_REQUEST['mode'];}
if (isset($_REQUEST['old_plan'])) {$old_plan = $_REQUEST['old_plan'];}
if (isset($_SESSION['user_id'])) {$company_id = $_SESSION['user_id'];}

if ($old_plan != 0 && $koumoku == 0 ) {
	echo '<script type="text/javascript">';
	echo 'alert("フリープランに変更すると、データが6ヵ月分までしか保存されません。';
	echo 'また、プランの変更当月までの料金は御請求いたします。")';
	echo '</script>';
}

?>


<script>

function OpenFreeplanFrame(){
	window.scrollTo(0,0);
	var plan=document.getElementsByName("koumoku");
		document.getElementById("fadeLayer").style.visibility = "visible";
	
	for(i=0;i<3;i++){
		if (plan[i].checked && plan[i].value == '0') {
		document.getElementById("Freeplan").style.display="block";
		break;
		}
		if (plan[i].checked && plan[i].value == '1') {
		document.getElementById("Standardplan").style.display="block";
		break;
		}
		if (plan[i].checked && plan[i].value == '2') {
		document.getElementById("Premiumplan").style.display="block";
		break;
		}
	}
}


function OpenPasserrorFrame(){
	window.scrollTo(0,0);
	document.getElementById("Passerrorplan").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
}

function TransPassword() {
	var password_enter = document.getElementById("password_enter").value;
	document.getElementById("password").value = password_enter;
}

</script>


<iframe id="Freeplan" src="./frame/freeplan"></iframe>
<iframe id="Standardplan" src="./frame/standardplan"></iframe>
<iframe id="Premiumplan" src="./frame/premiumplan"></iframe>
<iframe id="Passerror" src="./frame/passerror"></iframe>


<?php

//プラン変更フォームが送信された場合の処理
if ($mode == "plan_change") {
//	$user_pass_arr = $user_con -> user_select_id($pdo,$user_id);
//	$pass_data = $user_pass_arr['password'];
	//var_dump($user_pass_arr);
//	if ($pass_data == md5($password)) {
		$flag = "image_upload";
		$condition = " plan_status = ".$koumoku." ";
		$company_con -> company_sql_flag($pdo,$flag,$company_id,$condition);
		$flag = "";
		date_default_timezone_set('Asia/Tokyo');
		$fp = fopen("/var/www/co.cloudinvoice.co.jp/html/files/common/change_plan_log/changeplan".date('Ym').".log", "a");
		$today = getdate();

		fwrite($fp, $company_id.",".$koumoku.",".date("Ymd His")."\r\n");
		fclose($fp);

		//echo "<script type='text/javascript'>alert('プランを変更しました');</script>";
		
		
		
//	} else {
//		echo "<script type='text/javascript'>window.onload=alert('パスワードが一致しません');</script>";
//		echo '<div onload="OpenPasserrorFrame();" ></div>';
//	}
}

$flag = "company_data";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan_status = $company_arr[0]['plan_status'];

?>


<article>

	<section id="m-1-box">
		<h2>
			プラン変更
		</h2>
		
		<p id="plan">
			ご利用のプランを変更します。<br>
			プラン詳細については<a href="../plan" target="blank">こちら</a>をご覧ください。
		</p>
		<p>変更するプランを選択する</p>
		<p>
			現在のプラン：
			<span class="strong">
				<?php
					if ($plan_status == 0) {
						echo "フリープラン";
					} else if ($plan_status == 1) {
						echo "スタンダードプラン";
					} else if ($plan_status == 2) {
						echo "プレミアムプラン";
					}
				?>
			</span>
		</p>
		
		<form id="plan_select" action="../payment/pay" method="post">
			<div id="plan">
				<input type="radio" id="1" name="koumoku" value="0" <?php if ($plan_status == 0) { echo "checked";}?>></input>
				<label for="1">フリープラン</label>
				<input type="radio" id="2" name="koumoku"value="1" <?php if ($plan_status == 1) { echo "checked";}?>></input>
				<label for="2">スタンダードプラン</label>
				<input type="radio" id="3" name="koumoku"value="2" <?php if ($plan_status == 2) { echo "checked";}?>></input>
				<label for="3">プレミアムプラン</label>
			</div>
				<input type="hidden" name="old_plan" value="<?php echo $plan_status;?>">
				<input type="hidden" id="mode" name="mode" value="plan_change">
				<input type="hidden" id="password" name="password" value="">
		</form>
<!--
			<div id="planpass">
				
				<p>パスワード</p>
				
				<input type="password" id="password_enter" name="password_enter" onblur="TransPassword()" />
				
			</div>
-->			
			<div id="plansub">
				<input type="button" onclick="OpenFreeplanFrame()" value="確認画面に進む　＞"/>
			</div>
			<?php
				if (isset($_SESSION['error_msg'])) {
					echo "<p>".$_SESSION['error_msg']."</p>";
					unset($_SESSION['error_msg']);
				}
			?>


	</section>
</article>

<?php require("footer.php");?>
<div id="fadeLayer" onclick="CloseFrame(6)"></div>
<div id="noneLayer"></div>
