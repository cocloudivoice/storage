<?php
	
//メールリマインダー
//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//ここからメールアドレス照合
	if (isset($_REQUEST['email'])) {
		//Emailアドレスの存在を確認する。
		$email = htmlspecialchars($_REQUEST['email'],ENT_QUOTES);
		$login_con = new user_control();
		$login_arr = $login_con->user_select_email($pdo,$email);
		$_SESSION['user_id'] = $login_arr['user_id'];
		
		if ($login_arr != NULL) {
			$login_arr = $login_con->user_select_email($pdo,$email);
			//$passwordmaker = random(12);
			$passwordmaker = uniqid();
			$login_arr['password'] = md5($passwordmaker);
			$login_con->user_update($pdo,$login_arr);
			$login_arr = $login_con->user_select_email($pdo,$email);
			//var_dump($login_arr);
			$newpassword = $passwordmaker;
			$subject ="仮パスワード発行メール";
			$header = "From:Cloud Invoice運営<".$email.">";
$message =$login_arr['nick_name']."様

ご登録ありがとうございます。
仮パスワードを発行いたしました。
仮パスワードは、下記の通りです。

ID(E-mailアドレス):".$login_arr['mail_address']."
パスワード:".$newpassword."
サイトURL:http://co.cloudinvoice.co.jp/

ご登録のE-mailアドレスと仮パスワードでログインしてから、パスワードの変更をお願いいたします。

尚、仮パスワード発行を依頼した覚えが無い場合は、お手数ではございますが、
運営(info@co.cloudinvoice.co.jp)までメールをいただけますでしょうか。

よろしくお願いいたします。

Cloud Invoice運営
";

			mail($email,$subject,$message,$header);
        } else {
			//情報を取得できなかった場合はリマインダーへ
		    $_SESSION['emailcheck'] = "メールアドレスが登録されておりません。";
			header("Location:./forgotpassword.php",FALSE);
    		exit();
        }
	}

function random($length = 13) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ユーザ詳細登録画面</title>
<script type="text/javascript" language="JavaScript">
<!--
function close_win(){
	var nvua = navigator.userAgent;
	if (nvua.indexOf('MSIE') >= 0){
		if (nvua.indexOf('MSIE 5.0') == -1) {
			top.opener = '';
		}
	}
    else if (nvua.indexOf('Gecko') >= 0) {
      top.name = 'CLOSE_WINDOW';
      wid = window.open('','CLOSE_WINDOW');
    }
    top.close();
}


-->
</script>
<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:450px;
	height:750px;
	margin-left:auto;
	margin-right:auto;
}
#wrap p {
	text-align:left;
	text-indent:1em;
}
#wrap input {
	text-align:center;
	margin-left:auto;
	margin-right:auto;
}
#frame{
	width:400px;
	height:750px;
	text-align:left;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
#name_tag{
	margin-right:20px;
	text-align:center;
}
.hidden{
	display:none;
}

</style>
<title>仮パスワード発行</title>
</head>
<body>

	<div id="wrap">
		<h1 class="hidden">仮パスワード発行</h1>
		<h2>仮パスワードを発行いたしました。</h2>
		<p>ご登録のメールアドレスに仮パスワードを送信いたしましたので、
		<br />仮パスワードでログインして任意のパスワードに変更してください。
		</p>
		<p><input type="button" value="閉じる" onclick="window.close(); return false;" />
		</p>
		<p onclick="window.open('about:blank','_self').close()" style="cursor:pointer;">（ブラウザがFirefoxの場合、ボタンでは閉じない事があります。）</p>
		<button onclick="location.href='//co.cloudinvoice.co.jp'">サイトのトップに戻る</button>
	</div>
</body>
</html>