<?php require("header.php");?>


<title>OCR済み通帳等テキストの再加工</title>


<?php

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');

//変数の宣言
$company_id;
$words;
$flag;
$sql;
if ($_SESSION['up_info_msg']){
	$up_info_msg = $_SESSION['up_info_msg'];
	unset($_SESSION['up_info_msg']);
}
//ユーザーテーブルのコントローラーを呼び出す
$company_con = new company_control();

/*
//各テーブルからデータを取得する
$flag="csv_history_data";
$csv_history_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$csv_history_num = count($csv_history_data_arr);
//最新の5件を取得
if ($csv_history_num > 5) {
	$csv_history_num = 5;
}
*/

//CSVフォーマット用セッションスコープ解除
unset($_SESSION['format']);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$up_dir_path = dirname(__FILE__)."/../pdf/png/TextConvert";
//$dir_path = dirname(__FILE__)."/../pdf/png/".$af_id;
$dir_path = dirname(__FILE__)."/../pdf/png/TextConvert/TextConvertResult";
$dir_path4 = dirname(__FILE__)."/../pdf/png/TextConvert/TextConvertResult";
?>

<article>

	<section id="m-1-box">
		<h2>
			OCR済み通帳テキストの再加工
		</h2>
		<p>
		OCR済み通帳テキストを見やすく加工し直します。<br>
		ファイルは別途サーバーにアップロードしてこのボタンを押す。
		</p>
		
		<form name="form_sansho" action="./upload_convert_text_all" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<input type="file" name="upfile[]"  multiple="multiple"/>
				<input type="hidden" name="path" value="<?php echo $up_dir_path;?>" />
				<input type="hidden" name="return_url" value="./text_convert_bankbook_upload_all" />
				<input type="hidden" name="ahead_url" value="./text_convert_bankbook_upload_all" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="text/zipアップロード" />
		</form>
		<br><br>

		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form name="form_sansho" action="./text_convert_bankbook_upload" method="post" enctype="multipart/form-data">
			<div id="m-1-box" style="margin:0px 0 5px 0;">
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				社名等の一部(除外用)<input type="text" name="com_name" value="" style="margin:0 0 5px 0;"/><br>
				会計年度
				<select name="fyear">
					<?php for($k = 2010;$k <= 2020;$k++) {?>
					<option value="<?php echo $k;?>" <?php if ($k==2016) {echo "selected";}?>><?php echo $k;?></option>
					<?php }?>
				</select>
				決算月
				<select name="fmonth">
					<?php for($k = 1;$k <= 12;$k++) {?>
					<option value="<?php echo $k;?>" <?php if ($k==3) {echo "selected";}?>><?php echo $k;?></option>
					<?php }?>
				</select>

				<input type="hidden" name="path" value="<?php echo $dir_path;?>" />
				<input type="hidden" name="return_url" value="./text_convert_bankbook_upload_all" />
				<input type="hidden" name="ahead_url" value="./text_convert_bankbook_upload_all" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="テキスト化開始" />
			<label><input type="checkbox" name="output" value="1" checked />出力方法2</label>
		</form>
			
		<?php if (isset($_REQUEST['sansho'])) {echo $_REQUEST['return_url'];}?>
		<form style="margin:10px 0 10px 0;" name="form_sansho" action="./text_convert_upload_g" method="post" enctype="multipart/form-data">
			<div id="m-1-box">
				<!--<input type="file" name="upfile[]"  multiple="multiple"/>-->
				<input type="hidden" name="return_url" value="./text_convert_bankbook_upload_all" />
				<input type="hidden" name="ahead_url" value="./text_convert_bankbook_upload_all" />
				<?php $_SESSION['comment'] = "";?>
			</div>
			<input type="button" onclick="submit();" value="グーグルテキスト化開始" />
		</form>
		<?php echo "<span style='color:blue;'>".$up_info_msg."</span>";?>
		<?php echo "<span style='color:blue;'>".$_SESSION['comment']."</span>";?>
		<?php $_SESSION['comment'] = "";?>

<!--
	<section>
		<div style="margin-top:280px;">
		<input type="button" style="margin-left:10px;" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path?>'" value="テキスト化したファイルをダウンロード"/>
		</div>
	</section>
-->
<?php if ($af_id == "5129391867" || $af_id == "3308277917" || $af_id = "7265126278") {?>
	<section>
		<div style="margin-top:50px;">
		<input type="button" style="margin-left:0px;" name="journalize" onclick="location.href='./download_imgs_to_text_all?path=<?php echo $dir_path4;?>'" value="ブラウザからダウンロード"/>
		</div>
	
<?php }?>
</section>
</article>
<?php require("footer.php");?>