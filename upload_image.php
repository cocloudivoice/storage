<?php
session_start();
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
require_once("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/companycontrol.class.php');

$user_arr = array();
$company_id;

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = $_SESSION['user_id'];
} else {
	header("Location:./invoice/logout.php");
	exit();
}

//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
//各テーブルのコントローラーを起動する
$user_con = new user_control();
$company_con = company_control();
$user_arr = $user_con->user_select_id($pdo,$user_id);

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";


//var_dump($_FILES['imgurl']);
if ($_FILES['stamp']["tmp_name"]) {
	  $path = "../files/" . $user_id. "/". "images";
	  //$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "images";
	if ( mkdir( $path, '0755', TRUE ) ) {
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
}

if (!isset($_FILES['stamp']['error']) || !is_int($_FILES['stamp']['error']){

	// $_FILES['stamp']['mime']の値はブラウザ側で偽装可能なので
	// MIMEタイプを自前でチェックする
	if (!$info = @getimagesize($_FILES['stamp']['tmp_name'])) {
		throw new RuntimeException("有効な画像ファイルを指定してください");
	}
	if (!in_array(
		    $info[2],
		    array(
		        IMAGETYPE_GIF,
		        IMAGETYPE_JPEG,
		        IMAGETYPE_PNG,
		    ),
		    true
		)) {
		    throw new RuntimeException("未対応の画像形式です");
	}
	// 画像処理に使う関数名を決定する
	$tmp = explode('/', $info['mime']);
	$create = "imagecreatefrom{$tmp[1]}";
	$output = "image{$tmp[1]}";

	// 縦横比を維持したまま 120 * 120 以下に収まるサイズを求める
	if ($info[0] >= $info[1]) {
	    $dst_w = 80;
	    $dst_h = ceil(80 * $info[1] / max($info[0], 1));
	} else {
	    $dst_w = ceil(80 * $info[0] / max($info[1], 1));
	    $dst_h = 80;
	}

// 元画像リソースを生成する
if (!$src = @$create($_FILES['upfile']['tmp_name'])) {
    throw new RuntimeException(" 画像リソースの生成に失敗しました");
}

// リサンプリング先画像リソースを生成する
$dst = imagecreatetruecolor(80, 80);

// getimagesize関数で得られた情報も利用してリサンプリングを行う
imagecopyresampled(
    $dst, $src,
    0, 0, 0, 0,
    80, 80, $info[0], $info[1]
);
/*
 // ファイルデータからSHA-1ハッシュを取ってファイル名を決定する
if (!$output(
    $dst,
    sprintf('./resized/%s%s',
        sha1_file($_FILES['upfile']['tmp_name']),
        image_type_to_extension($info[2])
    )
)) {
    throw new RuntimeException("ファイル保存時にエラーが発生しました");
}
*/


	if (is_uploaded_file($_FILES['stamp']["tmp_name"]) || is_uploaded_file($_FILES['logo']["tmp_name"])) {

//		if (move_uploaded_file($_FILES['stamp']["tmp_name"], $path. "/" . $_FILES['stamp']["name"])) {
		if (move_uploaded_file($dst, $path. "/" . $dst)) {
			chmod($path. "/" . $_FILES['stamp']["name"], 0644);
		    $SESSION['file_comment'] .= $path. "/" . $_FILES['stamp']["name"] . "をアップロードしました。";
			$stamp_image = $path. "/" . $_FILES['imgurl']["name"];
			// リソースを解放
			imagedestroy($src);
			imagedestroy($dst);

		} else {
	    	$SESSION['file_comment'] .= "印影のイメージをアップロードできませんでした。";
		}


		if (move_uploaded_file($_FILES['logo']["tmp_name"], $path. "/" . $_FILES['logo']["name"])) {
		    chmod($path. "/" . $_FILES['logo']["name"], 0644);
		    $SESSION['file_comment'] .= $path. "/" . $_FILES['logo']["name"] . "をアップロードしました。";
			$logotype = $path. "/" . $_FILES['logo']["name"];
		} else {
			$SESSION['file_comment'] .= "ロゴのイメージをアップロードできませんでした。";
		}

		$flag = "image_upload";
		$words = "logotype =".$logotype." , stamp_image =".$stamp_image." " ;
		//var_dump($user_arr);
		$result = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);

	} 
	//var_dump($result);
}

header("Location:./invoice/frame/report.php");
exit();

?>