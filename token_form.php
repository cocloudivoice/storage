<?php
	ini_set('session.use_cookies', 1);//お客様がクッキーを利用しているときは、セッションをクッキーに保存する
	ini_set('session.cookie_lifetime', 0);//セッションをクッキーに保存したときは、ブラウザを閉じるまでセッションを切断しない
	ini_set('session.gc_maxlifetime', 7200);//クッキーを利用しないときは、セッション有効時間は２時間

	session_start();
?>
<!DOCTYPE html>

<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="請求書,クラウド化,無料,自動化,CSV,郵送代行,入金確認自動化,クラウド請求システム" /> 
		<meta name="description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />		
		<title>HOME - [Cloud Invoice]</title>
		
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta property="og:title" content="請求情報の集積システム「Cloud Invoice (クラウドインボイス)」" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="//storage.cloudinvoice.co.jp/index.php" />
		<meta property="og:image" content="//hikaku.jp/common/images/logo.jpg" />
		<meta property="og:site_name" content="Cloud Invoice - クラウドインボイス" />
		<meta property="og:description" content="請求の送受信をクラウド化。Cloud Invoiceでは、得意先に送付する請求情報だけでなく、取引先から受取る請求情報も一括してクラウド管理を行います。そのため、請求情報の管理を容易にし、過去の請求情報の蓄積が可能です。" />
		<meta property="og:locale" content="ja_JP" />
		<link rel="shortcut icon" href="//storage.cloudinvoice.co.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://storage.cloudinvoice.co.jp/" />

<?php

    $tokendir = dirname( __FILE__ ). DIRECTORY_SEPARATOR ."token" . DIRECTORY_SEPARATOR;
    $_SESSION['em'] = $email = $_GET["email"];
    $success_page ="http://www.storage.cloudinvoice.co.jp/invoice/signup/";
    if (isset($_GET["key"])) {
        if (delete_old_token($_GET["key"])) {
        	
        	echo "認証に成功しました。サインアップ画面に切り替わります。";
        	echo '<script type="text/javascript">alert("認証に成功しました。");</script>';
        	echo '<script type="text/javascript">document.location.href="//www.storage.cloudinvoice.co.jp/invoice/signup"</script>';
        	echo "ページが遷移しない場合はこちらのボタンを押してください。";
        	echo '<a href="//www.storage.cloudinvoice.co.jp/invoice/signup/"><button>サインアップページへ</button></a>';
        	/*
            mail_to_master("info@storage.cloudinvoice.co.jp");//管理者にメール
            mail_to_success($_GET["email"]);//ユーザーにメール
            print "登録完了メールを送信しました。<a href=\"".$success_page."\">ログインページへ</a>";
        	*/
        } else {
            print 'もう一度初めからやり直してください。';
        }
	} else if ($_POST["mail"] == "") {
		        print "メールアドレスを入力してください";
		        echo '<form action="token_form" method="post">
					<p>メールアドレス<input name="mail" type="text" /></p>
					<p><input type="submit" value="送信" /></p>
				</form>';
	} else if (mb_strlen($_POST["mail"])> 0 && !preg_match("/^([a-z0-9_]|\-|\.|\+)+@(([a-z0-9_]|\-)+\.)+[a-z]{2,6}$/i",$_POST["mail"])) {
	        print "メールアドレスの書式に誤りがあります。";
	} else {
	        print "確認メールを送信しました";
	        mail_to_token($_POST["mail"]);
	        exit();
	}
     
     
    function mail_to_token($address)
    {
        global $tokendir;
        $limit = (time()+1800);//秒 1800秒=30分間
       
        $token= rand(0,100).uniqid();//トークン
       
        touch($tokendir.$token.".log");//トークンファイル作成
     
        $url = "http://storage.cloudinvoice.co.jp/invoice/token_form"."?key=".$token."&email=".$address;
        //$url = $_SERVER["HTTP_REFERER"]."?key=".$token;
        
        file_put_contents($tokendir.$token.".log", $limit, LOCK_EX);//期限保存
       
        delete_old_token();//古いトークン削除
		mb_language("ja");
		mb_internal_encoding("UTF-8");
        $message="このメールは会員登録用トークン認証メールです。以下のアドレスを開いて認証を完了してください。\n30分以内にアクセスが無かった場合は無効となります。\n";
        $message.= $url."\n\n";
		$headers = "From:".mb_encode_mimeheader('Cloud Invoice (クラウドインボイス) 運営','ISO-2022-JP-MS')."<info@storage.cloudinvoice.co.jp>\r\n";
		$subject = 'Cloud Invoice（クラウドインボイス） 仮登録トークン認証メール';
        mail($address,mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),$headers);
    }
     
    function mail_to_success($mailto)
    {
       global $success_page;
        $message="認証が完了しました。\n\nダウンロードパスワード：password\n\n";
        $message.="こちらでダウンロードできます:".$success_page."\n\n";
		$headers = "From: Cloud Invoice <info@storage.cloudinvoice.co.jp>\r\n";
		$subject = 'Cloud Invoice登録完了';
		mb_internal_encoding("UTF-8");
        mail($mailto,mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),mb_convert_encoding($headers,'ISO-2022-JP-MS'));
    }
     
    function mail_to_master($mail)
    {
        $message ="新しく登録されたユーザーの情報は次の通りです。\n";
        $message.="────────────────────────────────────\n";
        $message.= $mail."\n\n";
        $message.="DATE: ".date("Y/m/d - H:i:s")."\n";
        $message.="IP: ".$_SERVER['REMOTE_ADDR']."\n";
        $message.="HOST: ".@gethostbyaddr($_SERVER['REMOTE_ADDR'])."\n";
        $message.="USER AGENT: ".$_SERVER['HTTP_USER_AGENT']."\n";
        $message.="────────────────────────────────────\n";
       $headers = "From: CloudInvoice <info@storage.cloudinvoice.co.jp>\r\n";
       $subject = "ユーザー登録通知";
       mb_internal_encoding("UTF-8");
        mail($mail,mb_encode_mimeheader($subject,'ISO-2022-JP-MS'),mb_convert_encoding($message,'ISO-2022-JP-MS'),mb_convert_encoding($headers,'ISO-2022-JP-MS'));
    }
     
    function delete_old_token($token = NULL)
    {
        global $tokendir,$email;
        ;
       //echo $tokendir;echo $token;echo $email;
        if (is_dir($tokendir)) {
        	//echo "test0<br/>";
            if ($dh = opendir($tokendir)) {
            	//echo "test0-1<br/>";echo readdir($dh);
                while (($file = readdir($dh)) !== false) {
                	//echo "test0-2<br/>";$tokendir.$file;echo "test0-2-1<br/>";
                	//echo is_file($tokendir.$file);echo "test0-2-2<br/>";
                	//echo is_null($token);echo $token."test0-2-3<br/>";
                    if(is_file($tokendir.$file) && is_null($token)){
                       //echo "test1-0<br/>";
                       $log = file_get_contents($tokendir.$file);
                       //echo "<br/>test1<br/>";
                       list($data,$mail) = split("<>",$log);
                       //echo "test2<br/>";
                       $email = $mail;
                       //echo "test3<br/>";
                        if(time()> $data) @unlink($tokendir.$file);
                       
                    }else if(basename($file,".log")==$token && !is_null($token)){
                       //echo "test4<br/>";
                        if(time() <(filemtime($tokendir.$token.".log")+180) ){
                       //echo "test4-1<br/>";
                            $log = file_get_contents($tokendir.$token.".log");
                            list($data,$mail) = split("<>",$log);
                            //echo $data;
                            $email = $mail;
                       
                            @unlink($tokendir.$token.".log");
                            return true;
                        }else{
                            @unlink($tokendir.$token.".log");
                            return false;
                        }
                    }
                }
                closedir($dh);
            }
        }
    }
     
    function my_send_mail($mailto, $subject, $message)
    {
       mb_internal_encoding("UTF-8");
        $message = mb_convert_encoding($message, "JIS", "auto");
        $subject = mb_convert_encoding($subject, "JIS", "auto");
       
        $header ="From: CloudInvoice <info@storage.cloudinvoice.co.jp>\n";
       
        mb_send_mail($mailto, $subject, $message, $header);
    }

?><!--
<form action="token_form.php" method="post">
	<p>メールアドレス<input name="mail" type="text" /></p>
	<p><input type="submit" value="送信" /></p>
</form>-->