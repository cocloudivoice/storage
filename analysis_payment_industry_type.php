<?php
session_start();

//必要なクラスの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');
//echo "test1";
include_once(dirname(__FILE__).'/../db_con/journalizedhistorycontrol.class.php');
//echo "test2";
//ページ生成用クラスの読み込み
include_once(dirname(__FILE__).'/../cfiles/MYNETS_Pagination.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();
$page_con = new MYNETS_Pagination();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();
$journalized_history_con = new journalized_history_control();

if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
//var_dump($_REQUEST);
$num_box = 0;
//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3) {
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}

$page = 1;
//ページ番号を取得
if (isset($_GET['page'])) {
	$page = htmlspecialchars($_GET['page'], ENT_QUOTES);
} else {
	$page = 1;
}

if (isset($_POST['aj_words'])) {
	$aj_words = $_POST['aj_words'];

} else {
	//絞込み条件

	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$aj_words .= " AND `pay_date` >= '".$payfrom."' ";
	}
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$aj_words .= " AND `pay_date` <= '".$payto."' ";
	}
	if ($_REQUEST['money_from']) {
		$money_from = $_REQUEST['money_from'];
		$having .= "AND sum(`total_price`) >= ".$money_from." ";
	}
	if ($_REQUEST['money_to']) {
		$money_to = $_REQUEST['money_to'];
		$having .= " AND sum(`total_price`) <= ".$money_to." ";
	}
	if ($_REQUEST['cl']) {
		$claimant_id = $_REQUEST['cl'];
	}

	if ($_REQUEST['remarks1']) {
		$remarks1 = $_REQUEST['remarks1'];
		$aj_words .= " AND (`remarks1` LIKE '%".$remarks1."%' OR `product_name` LIKE '%".$remarks1."%') ";
	}
}

//総支払額
$flag = "invoice_data_receive_total_indtype_narrow_down";
$words = $aj_words." AND (ID.`status` >= 1 AND ID.`status` <> 99) AND `paper_type` < 2 ".$having." GROUP BY C.`industry_type_s` ORDER BY ID.`pay_date` ASC";
$payment_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);
$payment_num = count($payment_arr);
//var_dump($payment_arr);

$total_payment = 0;
for ($i = 0;$i < $payment_num;$i++) {
	$total_payment += $payment_arr[$i]['sum(`total_price`)'];
}

//グラフ用データ
$flag = "invoice_total_data_send_narrow_down_make_graph_all";
$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) AND `paper_type` < 2 GROUP BY ymd HAVING 1 ".$having."";
$cl_graph_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
//var_dump($cl_graph_arr);
//$graph_num = count($cl_graph_arr);
$graph_num = 12;
$cl_graph_arr[0]['ymd'];

if ($cl_graph_arr[0]['ymd'] != "") {
	$start_date = date("Y-m",strtotime($cl_graph_arr[0]['ymd']));
} else {
	$start_date = date("Y-m");
}
list($year, $month) = explode("-", $start_date, 2);

for ($k = 0; $k < $graph_num; $k++) {
	//echo $labels .= "'".date('Y-m', strtotime('+'.$k.'month'))."'";
	$labels .= $temp_date = "'".date('Y-m', mktime(0, 0, 0, $month + $k, 1, $year))."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}
	$mcount = 0;
	for ($m = 1; $m <= $graph_num; $m++) {
		if ( "'".$cl_graph_arr[$m]['ymd']."'" == $temp_date) {
			$mcount = 1;
		//echo "<br/>\r\n";
			$params .= "".$cl_graph_arr[$m]['sum(`total_price`)'];
		//echo "<br/>\r\n";
		}
		if ($m == 12 && $mcount == 0) {
			$params .= "0";
		}
		if ($m == 12 && $k !=11) {
			$params .= ",";
		}
	}
}
/*
for ($k = 0;$k < $graph_num;$k++) {
	$labels .= "'".$cl_graph_arr[$k]['ymd']."'";
	if ($k != $graph_num - 1) {
		$labels .= ",";
	}
	//echo "<br/>\r\n";
	$params .= "".$cl_graph_arr[$k]['sum(`total_price`)'];
	//echo "<br/>\r\n";
	if ($k != $graph_num - 1) {
		$params .= ",";
	}
}
*/

//▼自社データ確認▼
$flag = "company_data";
$words = "";
$cl_company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
//var_dump($cl_company_data_arr);
$cl_company_data_arr[0]["company_name"];

?>

<?php require("header.php");?>
<script type="text/javascript" src="//co.cloudinvoice.co.jp/js/Chart.js/Chart.js"></script>
<script src="../js/Chart_Dynamic_View.js"></script>
<!--<link rel="stylesheet" href="../css/chart.css" type="text/css" />-->
<style type="text/css">
#main_box {
	position:relative;
	overflow:hidden;
	min-height:1200px;
	width:auto;
	height:auto;
}
#right {
	position: relative;
}
canvas {	
	position: absolute;
	/*
	position: relative;
    */
	z-index: 1;
}
#graph_tbl {
	position: absolute;
	top: 450px;
	width:500px;
}
#graph_tbl td{
	width:200px;
}
#graph_tbl th{
	width:200px;
}
</style>
<script type="text/javascript">

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box1" ).datepicker();
});
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#box2" ).datepicker();
});
</script>
<!--[if IE]>
<script type="text/javascript" src="../js/excanvas.js"></script>
<![endif]-->
<script type="text/javascript">
<!--
window.onload = function() {
	//描画コンテキストの取得
	var canvas = document.getElementById('graph');
	if (canvas.getContext) {

		var context = canvas.getContext('2d');
		var ctx = context;

		var data = {
				//横軸のラベル
				labels : [<?php echo $labels;?>],
				datasets : [
		/*
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : [65,59,90,81,56,55,40]
				},
		*/
				{
					fillColor : "rgba(151,187,205,0.5)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					data : [<?php echo $params;?>]
				}

			]

		}
		
		//棒グラフ
		var myNewChart = new Chart(ctx).Bar(data);
		//線グラフ
		//var myNewChart = new Chart(ctx).Line(data);
		//多角グラフ
		//var myNewChart = new Chart(ctx).Radar(data);
	}
}
-->
</script>

<script type="text/javascript"><!--
function subWin(url) {
	window.open(url, 'mywindow2', 'width=400, height=screen.height, menubar=no, toolbar=no, scrollbars=yes');
}
// --></script>
<?php //▼カラム幅調節▼?>
<script  src="../js/colResizable-1.5.min.js"></script>
<script type="text/javascript">
	$(function(){	
		
		var onSampleResized = function(e){
			var columns = $(e.currentTarget).find("th");
			var msg = "columns widths: ";
			columns.each(function(){ msg += $(this).width() + "px; "; })
			$("#tableTxt").html(msg);
			
		};	
		$("#analysisTable").colResizable({fixed:false});
		$("#analysisTable").colResizable({
			liveDrag:true, 
			gripInnerHtml:"<div class='grip'></div>", 
			draggingClass:"dragging", 
			onResize:onSampleResized});
		
	});	
</script>
<?php //▲カラム幅調節▲ ?>


<title>
支払一覧 - Cloud Invoice
</title>

<?php
//echo '<p class="red"> メンテナンス作業中です。しばらくお待ちください。</p>';
?>

<iframe id="kCommonSearch" name="kCommonSearch" src="./frame/kCommonSearch" scrolling="no"></iframe>
<article>
<div id="main_box">
	<section id="m-1-box">
		<h2>
		支払一覧
		</h2>
		
		<section id="search">
			<form action="" method="GET" id="cond_form" name="cond_form">
				<!--
				<div class="kamoku">
					<p>取引先名</p>
					<input type="text" name="" value="<?php echo $cl_company_data_arr[0]['company_name'];?>"/>
				</div>
				-->
				<div class="hiduke">
					<p>取引日付</p>
					<input type="text" id="box1" name="paydate_from" />
					～
					<input type="text" id="box2" name="paydate_to" />
				</div>
				<div class="hiduke">
					<p>金額</p>
					<input type="text"  name="money_from" />
					～
					<input type="text"  name="money_to" />
				</div>
				<div class="kamoku">
					<p>摘要</p>
					<input type="text" name="remarks1" />
				</div>
				<div class="search">
					<input type="button" onclick="submit();" value="検索"/>
					<input type="hidden" name="mode" value="download" />
					<input type="hidden" name="cl" value="<?php echo $claimant_id;?>">
				</div>
			</form>
		</section>

		<div class="SearchLine"></div>
		<!--
		<p><a href="#" onClick="window.close(); return false;">ウィンドウを閉じる</a></p>
		-->
		<p><a href="./business_analysis">経営分析に戻る</a></p>
		<p><a style="margin-right:5px;" href="./analysis_payment"><button>会社別</button></a><a href="./analysis_payment_personnel_expenses"><button>人件費</button></a></p>
		<section id="customer">
			
			<div id="left">
				<h3><?php echo $cl_company_data_arr[0]["company_name"];?></h3>
				<p>
					該当期間の総支払金額：<?php echo number_format($total_payment);?> 円
				</p>
				<table id="analysisTable" class="tables tablesorter dragging">
					<colgroup>
						<col align="center" width="200px">
						<col align="center" width="120px">
						<col align="center" width="120px">
						<col align="center" width="120px">
					</colgroup>
					<tr>
						<th>支払先</th>
						<th>支払金額</th>
						<th>支払数</th>
						<th>平均金額</th>
					</tr>
				<?php
				for ($i = 0;$i < $payment_num;$i++) {
					
				//請求書・領収書データの登録
				
				$pay_date = $payment_arr[$i]['pay_date'];
				//$pay_date = $cl_invoice_data_arr[$i]['paid_date'];
				$dlp = $payment_arr[$i]['download_password'];
				//echo $path = "/var/www/co.cloudinvoice.co.jp/html/files/".sprintf("%012d", $cl_invoice_data_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				$path = "../files/".sprintf("%012d", $payment_arr[$i]['claimant_id'])."/pdf/".date("Y", strtotime($pay_date))."/".date("m", strtotime($pay_date));
				//echo "<br/>\r\n";
				$pdf_url = $path."/".$dlp.".png";
				//$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				if (!file_exists($pdf_url)) {
					$pdf_url = $path."/".$dlp.".pdf";
				//echo "<br/>\r\n";
				}
				
				$customer_id = $payment_arr[$i]['claimant_id'];
				
				//▼取引先データ▼
				$flag = "company_data";
				$words = "";
				$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$customer_id,$words);
				//var_dump($cl_company_data_arr);
				$customer_name = $company_data_arr[0]["industry_type_s"];
				if ($customer_name == NULL) {
					$customer_name = "その他";
				}

				//請求書データから請求者のデータを取得
				$claimant_id = $customer_id;
				//echo $company_id;
				//echo "<br/>";
				//echo $claimant_id;
				//echo "<br/>";
				$flag = "invoice_total_data_send_narrow_down";
				$words = $aj_words." AND `destination_id`= '".$company_id."' AND (`status` >= 1 AND `status` <> 99) GROUP BY `invoice_code` HAVING 1 ".$having." ORDER BY `pay_date` ASC";
				$cl_invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$claimant_id,$words);
				//var_dump($cl_invoice_data_arr);
				$cl_invoice_data_num = count($cl_invoice_data_arr);

				?>

					<tr>
						<?php //var_dump($payment_arr[$i]);?>
						<td id="product" class=""><!--<a href= "./analysis_customer?cl=<?php echo $customer_id;?>">--><?php echo $customer_name;?><!--</a>--></td>
						<td class=""><?php echo number_format($payment_arr[$i]['sum(`total_price`)']);?> 円</td>
						<td class=""><?php echo number_format($cl_invoice_data_num);?> </td>
						<td class=""><?php echo number_format($payment_arr[$i]['sum(`total_price`)']/$cl_invoice_data_num);?> 円</td>
					</tr>
				<?php
				}
				?>
				</table><br>
				
			</div>
			<div id="right">
				<canvas id="graph" class="canvas_chart" style="background-color:white;" width="500px" height="400px">
					図形を表示するには、canvasタグをサポートしたブラウザが必要です。
				</canvas>
				<table id="graph_tbl">
				<?php
				$labels_arr = split(",",$labels);
				$params_arr = split(",",$params);
				
				for ($k = 1; $k < count($cl_graph_arr); $k++) {
				?>
				
					<tr><th> <?php echo $cl_graph_arr[$k]['ymd'];?> </th><td> <?php echo number_format($cl_graph_arr[$k]['sum(`total_price`)']);?> 円</td></tr>
				<?php
				}
				?>
				</table>
				<div class="tableText">
					<label id="tableTxt"></label>				
				</div>
			</div>
			
			
		</section>
	</section>
</div>
</article>

<?php require("footer.php");?>
