<?php

//マイムタイプ定義
$mime_content_types = array(
  'ez'      => 'application/andrew-inset',
  'atom'    => 'application/atom+xml',
  'atomcat' => 'application/atomcat+xml',
  'avi'     => 'video/x-msvideo',
  'movie'   => 'video/x-sgi-movie',
  'ice'     => 'x-conference/x-cooltalk'
);

//送信先メールアドレス
$to = 'akira.hamasaki@co.cloudinvoice.co.jp';
if ( $to != "" ) {
	$to .= implode( ",", $to );
}

//送信元メールアドレス
$from = 'test2@co.cloudinvoice.co.jp';

$cc = "test6@co.cloudinvoice.co.jp";
if ( $cc != "" ) {
	$cc .= implode( ",", $cc );
}

$bcc = "test4@co.cloudinvoice.co.jp";
if ( $bcc != "" ) {
	$bcc .= implode( ",", $bcc );
}

//件名
$subject = '添付ファイルのテスト';

//メール本文
$message  = "テストメール。\n";
$message .= "添付ファイル送信のテストです。\n";

//添付ファイル
$files = array('./fff006s.jpg');

//件名・本文をエンコード
$subject = mb_convert_encoding($subject, 'ISO-2022-JP', 'auto');
$message = mb_convert_encoding($message, 'ISO-2022-JP', 'auto');

$subject = '=?iso-2022-jp?B?' . base64_encode($subject) . '?=';

//バウンダリ文字列を定義
if (empty($files)) {
  $boundary = null;
} else {
  $boundary = md5(uniqid(rand(), true));
}

//メールボディを定義
if (empty($files)) {
  $body = $message;
} else {
  $body  = "--$boundary\n";
  $body .= "Content-Type: text/plain; charset=\"iso-2022-jp\"\n";
  $body .= "Content-Transfer-Encoding: 7bit\n";
  $body .= "\n";
  $body .= "$message\n";

  foreach($files as $file) {
    if (!file_exists($file)) {
      continue;
    }

    $info    = pathinfo($file);
    $content = $mime_content_types[$info['extension']];

    $filename = basename($file);

    $body .= "\n";
    $body .= "--$boundary\n";
    $body .= "Content-Type: $content; name=\"$filename\"\n";
    $body .= "Content-Disposition: attachment; filename=\"$filename\"\n";
    $body .= "Content-Transfer-Encoding: base64\n";
    $body .= "\n";
    $body .= chunk_split(base64_encode(file_get_contents($file))) . "\n";
  }

  $body .= '--' . $boundary . '--';
}

//メールヘッダを定義
$header  = "X-Mailer: PHP5\n";
$header .= "From: $from\n";
$header .= "CC: $cc\n";
$header .= "BCC: $bcc\n";
$header .= "MIME-Version: 1.0\n";
if (empty($files)) {
  $header .= "Content-Type: text/plain; charset=\"iso-2022-jp\"\n";
} else {
  $header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
}
$header .= "Content-Transfer-Encoding: 7bit";

//メール送信
if (mail($to, $subject, $body, $header)) {
  echo 'メールが送信されました。';
} else {
  echo 'メールの送信に失敗しました。';
}

?>