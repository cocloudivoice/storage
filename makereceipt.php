<?php require("header.php");?>
<?php

//controlクラスファイルの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/db_control.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");

//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $db_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$send_count = 0;//送信数の初期化
$status_num = 0;//ステータス変更用の変数初期化
$mail_send_count_num = 0;
$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];//現在のURLパラメーターつき
$company_id;
$words = "";
$flag = "";
$sql = "";
$send_flag = 0;
$download_password = "";
$user_num = 0;
$user_arr =array();


if (isset($_REQUEST['send_flag'])) {
	$send_flag = $_REQUEST['send_flag'];
}

//画像変更ありなしの判断
if ($_FILES["receipt_image"]["size"] > 0) { $change_image = 1;}

//var_dump($_REQUEST);
//var_dump($_FILES);

//Imajikの画像PDF化コマンド convert page.png page.pdf
//を用いて、画像をPDF化し、jpegはpngに変換して画像用ディレクトリに移動する。


$value = array();
//echo $_REQUEST["send_code"];

if ($_REQUEST['det'] != NULL) {
	$detail_num = htmlspecialchars($_REQUEST['det'],ENT_QUOTES);
}

if ($_REQUEST["company_addressee_flag"] == 1) {
	//送付先企業データの取得1▼
	if ($_REQUEST["destination_id"] != "" && $_REQUEST["destination_id"] != 0) {
		//echo "send_code なし<br/>";
		$flag = "company_data";
		$words = "";
		$company_check_arr = $company_con -> company_sql_flag($pdo,$flag,$_REQUEST["destination_id"],$words);
		//var_dump($company_check_arr);
		//echo $company_check_arr[0]["email"];
		
		$t_company_name = $company_check_arr[0]["company_name"];
		$t_zip_code = $company_check_arr[0]["zip_code"];
		$t_address = $company_check_arr[0]["prefecture"].$company_check_arr[0]["address1"];
		$t_address2 = $company_check_arr[0]["address2"];
		$t_section = $company_check_arr[0]["section"];
		$t_clerk = $company_check_arr[0]["clerk"];
		$t_email = $company_check_arr[0]["email"];
		
		if ($t_company_name != $_REQUEST["destination_name"]) {
			//echo $_REQUEST["destination_name"];
			$mod_cid_flag = 1;
			$mod_destination = 1;
		}
		if ($t_address != $_REQUEST["destination_address"]) {
			//echo $_REQUEST["destination_address"];
			$mod_cid_flag = 1;
			$mod_destination = 1;
		}
		if ($t_zip_code != $_REQUEST["destination_zip_code"]) {
			//echo $_REQUEST["destination_zip_code"];
			$mod_cid_flag = 1;
		}
		if ($t_address2 != $_REQUEST["destination_address2"]) {
			//echo $_REQUEST["destination_address2"];
			$mod_cid_flag = 1;
		}
		if ($t_section != $_REQUEST["destination_clerk"]) {
			//echo $_REQUEST["destination_clerk"];
			$mod_cid_flag = 1;
		}
		if ($t_clerk != $_REQUEST["destination_clerk2"]) {
			//echo $_REQUEST["destination_clerk2"];
			$mod_cid_flag = 1;
		}
		if ($t_email != $_REQUEST["destination_email"]) {
			//echo $_REQUEST["destination_email"];
			$mod_cid_flag = 1;
		}
	}
}

if ($send_flag == 1) {
	//echo "a";
	//var_dump($_REQUEST);
	
	//echo "save_template_flag=1";
	//通常の請求書作成処理
	if ($new_client_id != "") { $_REQUEST["send_code"] = $new_client_id;}
	if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && $_REQUEST["destination_id_visible"] != "") {
		//echo "b";
		if (isset($_REQUEST["seikyu_10"])) { $value[7] = htmlspecialchars($_REQUEST["seikyu_10"],ENT_QUOTES); }//振込先
		if (isset($_REQUEST["seikyu_11"])) { $value[18] = htmlspecialchars($_REQUEST["seikyu_11"],ENT_QUOTES); }//備考消すかも
		if (isset($_REQUEST["destination_id"])) { $destination_id = $cl_company_id = $value[0] = htmlspecialchars($_REQUEST["destination_id"],ENT_QUOTES); }//共通コード
		if (isset($_REQUEST["send_code"])) { $send_code = $value[1] = htmlspecialchars($_REQUEST["send_code"],ENT_QUOTES); }//共通コード
		if (isset($_REQUEST["invoice_code"])) { $invoice_code = $value[3] = htmlspecialchars($_REQUEST["invoice_code"],ENT_QUOTES); }
		if (isset($_REQUEST["invoice_name"])) { $value[4] = htmlspecialchars($_REQUEST["invoice_name"],ENT_QUOTES); }
		if (isset($_REQUEST["billing_date"])) { $billing_date = $value[2] = htmlspecialchars($_REQUEST["billing_date"],ENT_QUOTES); }
		if (isset($_REQUEST["pay_date"])) { $pay_date = $value[6] = htmlspecialchars($_REQUEST["pay_date"],ENT_QUOTES); }
		if (isset($_REQUEST["paid_date"])) { $paid_date = $value[25] = date('Y-m-d',strtotime(htmlspecialchars($_REQUEST["paid_date"],ENT_QUOTES))); }
		if (isset($_REQUEST["template_type"])) { $value[24] = htmlspecialchars($_REQUEST["template_type"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_name"])) { $value[35] = htmlspecialchars($_REQUEST["destination_name"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_zip_code"])) { $value[31] = htmlspecialchars($_REQUEST["destination_zip_code"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address"])) { $value[32] = htmlspecialchars($_REQUEST["destination_address"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_address2"])) { $value[33] = htmlspecialchars($_REQUEST["destination_address2"],ENT_QUOTES); }
		if (isset($_REQUEST["destination_clerk"])) { $value[5] = htmlspecialchars($_REQUEST["destination_clerk"],ENT_QUOTES).htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }
		//if (isset($_REQUEST["destination_clerk2"])) { $value[34] = htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES).htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }
		if (isset($_REQUEST["pay"])) { $pay = $value[26] = htmlspecialchars($_REQUEST["pay"],ENT_QUOTES); }
		if (isset($_REQUEST["paper_type"])) { $paper_type = $value[26] = htmlspecialchars($_REQUEST["paper_type"],ENT_QUOTES); }
		
		//▼請求者データ作成関連処理▼
		//売上請求書作成をベースに作ってあるため、destinationとなっているが、実際は請求者のデータ
		if (isset($_REQUEST["destination_tel"])) { $phone_number = htmlspecialchars($_REQUEST["destination_tel"],ENT_QUOTES); }//電話番号
		if (isset($_REQUEST["destination_id_visible"])) { $user_name = htmlspecialchars($_REQUEST["destination_id_visible"],ENT_QUOTES); }//企業名
		if (isset($_REQUEST["industry_type_s"])) { $industry_type_s = htmlspecialchars($_REQUEST["industry_type_s"],ENT_QUOTES); }//共通コード
		//請求者データの存在確認▼
		//電話番号で存在確認
		if ($phone_number != "") {
			//echo "c";
			$flag = "company_data_from_tel";
			$words = "";
			$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$phone_number,$words);
			//var_dump($company_data_arr);
			if (isset($company_data_arr[0]['tel_num'])) {
				//echo "TELあり<br/>\r\n";
				$exist_flag = 1;
			}// else{echo "TELなし<br/>\r\n";}
		}
		//電話番号が無かった場合、企業名で存在確認
		if($exist_flag == 0) {
			//echo "d";
			$flag = "company_data_from_name";
			$words = "";
			$company_data_arr = $company_con -> company_sql_flag($pdo,$flag,$user_name,$words);
			//var_dump($company_data_arr);
			if (isset($company_data_arr[0]['company_name'])) {
				//echo "名前あり<br/>\r\n";
				$exist_flag = 1;
			}
		}
		//企業データ存在確認▲

		if ($exist_flag == 0) {
			//echo "e";
			//echo "支払先の企業が存在しない時の処理<br/>\r\n";

			if (isset($_REQUEST["destination_zip_code"])) { $t_zip_code = htmlspecialchars($_REQUEST["destination_zip_code"],ENT_QUOTES); }
			if (isset($_REQUEST["destination_address"])) { 
				$t_address_all = htmlspecialchars($_REQUEST["destination_address"],ENT_QUOTES);
				preg_match('/(東京都|北海道|(?:京都|大阪)府|.{6,9}県)(.*)/', $t_address_all, $matches);
				$t_pref = $matches[1];
				//echo "<br/>\r\n";
				$t_address = $matches[2];
			}
			if (isset($_REQUEST["destination_address2"])) { $t_address2 = htmlspecialchars($_REQUEST["destination_address2"],ENT_QUOTES); }
			if (isset($_REQUEST["destination_clerk"])) { $t_section = htmlspecialchars($_REQUEST["destination_clerk"],ENT_QUOTES); }
			if (isset($_REQUEST["destination_clerk2"])) { $t_clerk = htmlspecialchars($_REQUEST["destination_clerk2"],ENT_QUOTES); }
			if (isset($_REQUEST["destination_email"])) { $t_email = htmlspecialchars($_REQUEST["destination_email"],ENT_QUOTES); }

			//仮メールアドレス生成
			if ($t_email != "") {
				$mail_address = $t_email;
			} else {
				$mail_address = md5(random(12)).rand(4)."@user.co.cloudinvoice.co.jp";
			}
			$non_regular_flag = 2;
			//$user_con = new user_control();//ヘッダーにあるためコメントアウト
			//Emailアドレスの存在を確認する。
			$login_arr = $user_con -> user_select_email($pdo,$mail_address);
			if ($login_arr != NULL) {
				$login_arr = $user_con -> user_select_email($pdo,$mail_address);
				//$passwordmaker = random(12);
				//$passwordmaker = uniqid();
				$passwordmaker = "1234abcd";
				$login_arr['password'] = md5($passwordmaker);
				//$login_arr['nick_name'] = $user_name;
				//$user_con -> user_update($pdo,$login_arr);
				$update_data = " `password` = '".$login_arr['password']."' ";
				$user_con -> user_update_data($pdo,$login_arr['user_id'],$update_data);

				$check = $user_con -> user_select_email($pdo,$mail_address);
			}
			
			$user_arr = $user_con -> user_select($pdo,$mail_address);

			//ユーザー登録
			$user_arr['nick_name']   = $user_name;
			$user_arr['mail_address'] = $mail_address;
			$user_arr['tel_num'] = $phone_number;
			$user_arr['non_regular_flag'] = $non_regular_flag;
			
			//共通コードの生成と取得
			if (!$user_arr['user_id']){
				$user_num = $user_con -> user_get_new_num($pdo);
				$user_arr['user_id'] = $user_num;
			}
			$result = $user_con -> user_insert($pdo,$user_arr);

			//企業データ登録▼
			$value[0] = $cl_company_id = sprintf("%012d", $user_arr['user_id']);
			$sql = "
			INSERT INTO `COMPANY_TABLE` ( 
				`company_id`, `company_name`, `email`, `tel_num`, `non_regular_flag`,
				`industry_type_s`, `zip_code`, `prefecture`, `address1`, `address2`,
				`address_full`, `section`, `clerk`
			) VALUES ( 
				".$user_arr['user_id'].", '".$user_arr['nick_name']."','".$user_arr['mail_address']."','".$user_arr['tel_num']."','".$user_arr['non_regular_flag']."',
				'".$industry_type_s."', '".$t_zip_code."', '".$t_pref."', '".$t_address."', '".$t_address2."'
				, '".$t_zip_code.$t_address.$t_address2."', '".$t_section."', '".$t_clerk."'
			)";
			$company_arr = $company_con -> company_sql($pdo,$cl_company_id,$sql);
			$_REQUEST["destination_id"] = $cl_company_id;
			//企業データ登録▲
			
			//▼自動仕訳用企業DB登録▼
			//企業品目テーブルと企業用レコードテーブルを登録
			$table_name = "".sprintf("%012d", $cl_company_id);
			try {
				$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
				$company_num++;
			} catch (PDOException $e) {
			    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
			    $_SESSION['error_msg'] = $e -> getMessage();
			}
			//▲自動仕訳用企業DB登録▲

		} else {
			//存在した場合
			$destination_id = $cl_company_id = $value[0] = $company_data_arr[0]['company_id'];
		}
		//▲請求者データ作成関連処理▲

		//echo "f";
		$sql = "SELECT `invoice_code` 
		FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$value[0]." 
		AND `invoice_code` = '".$value[3]."'
		";
		//echo "\r\n";

		$invoice_code_check = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
		//var_dump($invoice_code_check);
		if ($invoice_code_check[0] != NULL) {
			//$_SESSION['up_info_msg'] = "請求書番号が重複しているのでデータを登録できません。";
			echo "<script type='text/javascript'>alert('請求書番号が重複しているのでデータを登録できません。');location.href='./makereceipt';</script>";
			header("Location:./makereceipt", false);
			exit();
		} else if (isset($_REQUEST["invoice_code"]) && $_REQUEST["invoice_code"] != "" && ( $value[0] != "" || $value[1] != "")) {
			//ループ回数の取得
			if (isset($_REQUEST["roop_times"])) { $roop_times = htmlspecialchars($_REQUEST["roop_times"],ENT_QUOTES);}
			$roop_times = 10;//仮置き
			//ループして明細データを取得する
			for ( $i = 1 ; $i <= $roop_times ; $i++ ) {
				if ($_REQUEST["seikyu_1".$i."2"] != "") {
					if (isset($_REQUEST["seikyu_1".$i."1"])) { $value[10] = htmlspecialchars($_REQUEST["seikyu_1".$i."1"],ENT_QUOTES); } else { $value[10] = "";}//品目
					if (isset($_REQUEST["seikyu_1".$i."2"])) { $value[11] = htmlspecialchars($_REQUEST["seikyu_1".$i."2"],ENT_QUOTES); } else { $value[11] = 0;}//単価
					if (isset($_REQUEST["seikyu_1".$i."3"])) { $value[12] = htmlspecialchars($_REQUEST["seikyu_1".$i."3"],ENT_QUOTES); } else { $value[12] = 0;}//数量
					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[14] = htmlspecialchars($_REQUEST["seikyu_1".$i."4"],ENT_QUOTES); } else { $value[14] = 0;}//金額

					//消費税と源泉税の有無を判定する
					if (isset($_REQUEST["seikyu_1".$i."6"])) { $sales_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."6"],ENT_QUOTES);} else { $sales_tax = 1;}
					if (isset($_REQUEST["seikyu_1".$i."7"])) { $withholding_tax = htmlspecialchars($_REQUEST["seikyu_1".$i."7"],ENT_QUOTES);} else { $withholding_tax = 0;}
					
					if ($sales_tax == 1) {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = round($value[14] * 8/100,0);}//消費税
					} else {
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[15] = 0;}//消費税
					}
					
					//明細の小計額と消費税の合計を計算
					$detail_subtotal = $value[14] + $value[15];
					
					if ($withholding_tax == 1) {
						//源泉所得税がかかる場合
						if ($detail_subtotal <= 1000000) {
							//源泉所得税が100万円以下の場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round($detail_subtotal * 1021/10000,0);}//源泉所得税
						} else {
							//源泉所得税が100万円を超える場合
							if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[16] = round((102100 + (($detail_subtotal - 1000000) * 2042/10000)),0);}//源泉所得税
						}
					} else {
						//源泉所得税がかからない場合
						$value[16] = 0;//源泉所得税
					}

					if (isset($_REQUEST["seikyu_1".$i."5"])) { $remarks_invoice = htmlspecialchars($_REQUEST["seikyu_1".$i."5"],ENT_QUOTES); }//備考もしくは源泉所得税//現在は未使用。とりあえず置いておく。

					//合計金額の計算
	//				if ($sales_tax == 1 && $withholding_tax == 0) {
						//消費税のみの場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round($detail_subtotal,0); }//合計金額
	//				} else if ($sales_tax == 1 && $withholding_tax == 1) {
						//消費税+源泉税の場合
						if (isset($_REQUEST["seikyu_1".$i."4"])) {$value[17] = round(($detail_subtotal - $value[16]),0);}//合計金額
	//				} else if ($sales_tax == 0 && $withholding_tax == 1) {
						//源泉所得税のみの場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round(($value[14] - $value[16]),0); }//合計金額
	//				} else if ($sales_tax == 0 && $withholding_tax == 0) {
						//税金を引かない場合
	//					if (isset($_REQUEST["seikyu_1".$i."4"])) { $value[17] = round($value[14],0); }//合計金額
	//				}
					
					
					//共通コードでの企業データのヒストリカル取得
					//var_dump($value);//."<br/><br/>";
					if ($company_id != "") {
						$flag = "company_data_historical";
						$claimant_historical = $company_con -> company_sql_flag($pdo,$flag,$company_id,$cond_a);
						$c_historical = $claimant_historical[0]['historical'];
						if ($c_historical == NULL) {
							$c_historical = 0;
						}
					}

					if ($value[0] != "") {
						$flag = "company_data_historical";
						
						$destination_historical = $company_con -> company_sql_flag($pdo,$flag,$value[0],$cond_b);
						$d_historical = $destination_historical[0]['historical'];
						if ($d_historical == NULL) {
							$d_historical = 0;
						}
					}

					
					
					//echo "<br><br><br><br>";
					//var_dump($value);
					if ($paper_type == "") {
						//書類タイプが入っていなければ2：領収書・レシートに入れる。
						$paper_type = 1;
					}
					$str_length = 20;
					$download_password = md5($company_id."-".$destination_id."-".$invoice_code."-".$timestamp_for_code.makeRandStr($str_length));
					$sql = "
						INSERT INTO `INVOICE_DATA_TABLE` (
						`claimant_id`,`destination_id`, `claimant_historical`, `destination_historical`, `billing_date`, `invoice_code`,
						`invoice_name`, `staff_name`, `pay_date`,
						`bank_account`, `sale_date`, `product_code`,
						`product_name`, `unit_price`, `quantity`, `unit`,`total_price_excluding_tax`,
						`sales_tax`, `withholding_tax`, `total_price`,
						`remarks1`, `remarks2`, `remarks3`, `paid_date`, `csv_id`, `file_name`, `status`, `download_password`, `template_type`, `paper_type`, `data_entry_flag`,`insert_date`
						) VALUES 
						( ".intval($value[0]).", ".intval($company_id).", ".$c_historical.", ".$d_historical.", ".intval(str_replace(array(',','/','-'),'',$value[2])).", '".$value[3]."',
						'".$value[4]."', '".$value[5]."', ".intval(str_replace(array(',','/','-'),'',$value[6])).",
						'".$value[7]."', ".intval(str_replace(array(',','/','-'),'',$value[8])).", '".$value[9]."',
						'".$value[10]."', ".intval(str_replace(',','',$value[11])).", ".intval($value[12]).", '".$value[13]."',".intval(str_replace(',','',$value[14])).",
						'".$value[15]."', '".$value[16]."', ".intval(str_replace(',','',$value[17])).",
						'".$value[18]."', '".$value[19]."', '".$value[20]."', '".$value[25]."', '', '', ".$pay.", '".$download_password."', ".$value[24].", '".$paper_type."', 2, cast( now() as datetime)
						)";//echo "\r\n";
					//echo "ここでinsert<br/>";
					$check_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
				}
			}
		}
		if ($change_image == 1) {
			if ($_FILES['receipt_image']) {
				if (is_uploaded_file($_FILES['receipt_image']["tmp_name"])) {
					if ($_FILES['receipt_image']["type"] == "image/jpeg") {
						if (move_uploaded_file($_FILES['receipt_image']["tmp_name"], "../pdf/png/" . $download_password.".png")) {
							$file_path = "../pdf/png/" . $download_password.".jpg";
						    chmod("../pdf/png/" . $download_password.".jpg", 0664);
						    
						  
						    //JPEG画像をPNG画像に変換する処理
							//shell_exec("convert ".$file_path." /var/www/co.cloudinvoice.co.jp/html/pdf/nru/".$download_password.".png 2>&1");
							//shell_exec("convert ".$file_path." -geometry 50% /var/www/co.cloudinvoice.co.jp/html/pdf/nru/".$download_password.".png 2>&1");
							/*
							
							$olddir = '../pdf/nru/';
							 // 呼び出し元の画像ファイルのディレクトリ 
							$newdir = '../pdf/nru/';
							 // 書き込み先の画像ファイルのディレクトリ 
							$oldname= Trim($download_password.".jpg");
							 // 送られてきた画像の名前 
							$newname = Preg_Replace("/jpg/", "png", $oldname);
							 // 新しい名前の拡張子を変更しておく 
							$oldimgpath = $olddir . $oldname;
							 // 呼び出し元の画像へのパス 
							$newimgpath = $newdir . $newname;
							 // 書き込み先の画像へのパス 
							$dummy = "";
							 // 生成された画像の一時保管庫 
							echo $dummy = @ImageCreateFromJPEG("$oldimgpath") or die();
							 // jpegの画像IDを取得。@はエラー解説を表示するのに必要 
							echo $img = ImagePNG($dummy, "$newimgpath");
							 // pngで保存 
							ImageDestroy($dummy);
							// リソース解放
							*/
						} else {
							$_SESSION['up_info_msg'] = $_FILES['receipt_image']["name"] . "等のファイルをアップロードできませんでした。";
						}
					} else if ($_FILES['receipt_image']["type"] == "image/gif") {
						if (move_uploaded_file($_FILES['receipt_image']["tmp_name"], "../pdf/png/" . $download_password.".gif")) {
							chmod("../pdf/png/" . $download_password.".gif", 0664);
							$file_path = "../pdf/png/" . $download_password.".gif";
							shell_exec("convert ".$file_path." /var/www/co.cloudinvoice.co.jp/html/pdf/png/".$download_password.".png 2>&1");
						} else {
							$_SESSION['up_info_msg'] = $_FILES['receipt_image']["name"] . "等のファイルをアップロードできませんでした。";
						}
					} else if ($_FILES['receipt_image']["type"] == "image/tiff") {
						if (move_uploaded_file($_FILES['receipt_image']["tmp_name"], "../pdf/png/" . $download_password.".tif")) {
							chmod("../pdf/png/" . $download_password.".tif", 0664);
							$file_path = "../pdf/png/" . $download_password.".tif";
							shell_exec("convert ".$file_path." /var/www/co.cloudinvoice.co.jp/html/pdf/png/".$download_password.".png 2>&1");
						} else {
							$_SESSION['up_info_msg'] = $_FILES['receipt_image']["name"] . "等のファイルをアップロードできませんでした。";
						}
					} else if ($_FILES['receipt_image']["type"] == "application/pdf") {
						if (move_uploaded_file($_FILES['receipt_image']["tmp_name"], "../pdf/png/" . $download_password.".pdf")) {
							chmod("../pdf/png/" . $download_password.".pdf", 0664);
							shell_exec("convert ../pdf/png/". $download_password.".pdf -geometry 100% /var/www/co.cloudinvoice.co.jp/html/pdf/png/".$download_password.".png 2>&1");
						} else {
							$_SESSION['up_info_msg'] = $_FILES['receipt_image']["name"] . "等のファイルをアップロードできませんでした。";
						}
					} else {
						if (move_uploaded_file($_FILES['receipt_image']["tmp_name"], "../pdf/png/" . $download_password.".png")) {
						    chmod("../pdf/png/" . $download_password.".png", 0664);
						} else {
							$_SESSION['up_info_msg'] = $_FILES['receipt_image']["name"] . "等のファイルをアップロードできませんでした。";
						}
					}
				}
				//png画像をpdfに変換（300dpi A4サイズで）
				//shell_exec("convert -density 300 ../pdf/nru/". $download_password.".png -page A4 /var/www/co.cloudinvoice.co.jp/html/pdf/nru/".$download_password.".pdf 2>&1");
				shell_exec("convert -resize 100% ../pdf/png/". $download_password.".png /var/www/co.cloudinvoice.co.jp/html/pdf/png/".$download_password.".pdf 2>&1");

				//請求書・領収書データの登録
				$company_id = sprintf("%012d", $value[0]);
				$top_n = substr($value[0],0,4);
				$mid_n = substr($value[0],4,4);
				$path = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $value[0])."/pdf/".date("Y", strtotime($value[6]))."/".date("m", strtotime($value[6]));
				$tmp_path = "/var/www/co.cloudinvoice.co.jp/html/pdf/png";
				$pdf_url = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $value[0])."/pdf/".date('Y', strtotime($value[6]))."/".date('m', strtotime($value[6]))."/".$download_password.".pdf";
				//echo "<br/>\r\n";
				$png_url = "/var/www/co.cloudinvoice.co.jp/html/files/".$top_n."/".$mid_n."/".sprintf("%012d", $value[0])."/pdf/".date('Y', strtotime($value[6]))."/".date('m', strtotime($value[6]))."/".$download_password.".png";
				//echo "<br/>\r\n";

				//証憑ファイルの保存		
				if (!file_exists($pdf_url)) {
					//サーバーにPDFファイルが存在しない場合
					//PDFファイルを保存する
					$pdf_file = ''.$dlp.'.pdf';
					//請求元企業のディレクトリに入る
					try {
						
						if (mkdir( $path, 0775, true)) {
							chmod( $path, 0775, true );
							chgrp($path, "dev", true );
							//echo "ディレクトリ作成成功！！";
						} else {
							//echo "ディレクトリ作成失敗！！";
						}

					} catch (Exception $e) { 
						echo $e -> getMessage;
					}
				}
				if (!file_exists($png_url)) {
					try {
						//echo "PDF、PNG移動";
						//echo "<br/>\r\n";
						// ファイルの存在確認
						//PNGファイルを保存
						//削除用
						
						//	echo $path2 = "/var/www/co.cloudinvoice.co.jp/html/files/".$rm_data;
						//	echo "<br/>\r\n";
						//	shell_exec("rm -Rf ".$path2);
						//	shell_exec("rm -Rf ".$path2);
						$pdftoimg_con = new PdfToImg();
						$pdftoimg_con -> copyImage($tmp_path,$path,$download_password);
					} catch (Exception $e) {
						echo $e -> getMessage;
					}
				}
			}
		}
		
		//データの登録が成功したらメッセージを表示する
		if ($check_arr['chk']['check']){
			//echo "成功";
			shell_exec("rm -f /var/www/co.cloudinvoice.co.jp/html/pdf/png/*.*");
			echo "<script type='text/javascript'>location.href='./checkshiharaikanri';</script>";
			exit();
		} else {
			echo "<script type='text/javascript'>alert('入力不備のため支払い請求書を登録できませんでした');</script>";
			shell_exec("rm -f /var/www/co.cloudinvoice.co.jp/html/pdf/png/*.*");
			echo "<script type='text/javascript'>alert('a');location.href='./makereceipt';</script>";
			exit();
		}
	}
}

//送付先管理のデータ取得
if ($_REQUEST["get_destination_id"]) {
	$flag = "company_data";
	$destination_id_box = $_REQUEST["get_destination_id"];
	$destination_data_arr = $company_con -> company_sql_flag($pdo,$flag,$destination_id_box,$words);
	//var_dump($destination_data_arr);
	$check_destination_data = count($destination_data_arr);
	$flag="";
	$words="";
}
//送付先管理のデータ取得
$flag = "addressee_data_all_with_conditions";
$condition_words = "ORDER BY `company_name` ASC ";
$company_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$condition_words);
$flag="";
$words="";
//var_dump($company_arr);
if ($destination_data_arr[0]["company_id"] != 0 && isset($destination_data_arr[0]["company_id"])) {
	if (isset($destination_data_arr[0]["company_id"])) { $destination_company_id = $destination_data_arr[0]["company_id"];}
	if (isset($destination_data_arr[0]["company_name"])) { $destination_name = $destination_data_arr[0]["company_name"];}
	if (isset($destination_data_arr[0]["tel_num"])) { $destination_tel_num = $destination_data_arr[0]["tel_num"];}
	if (isset($destination_data_arr[0]["zip_code"])) { $destination_zip_code = $destination_data_arr[0]["zip_code"];}
	if (isset($destination_data_arr[0]["address1"])) { $destination_address = $destination_data_arr[0]["prefecture"].$destination_data_arr[0]["address1"];}
	if (isset($destination_data_arr[0]["address2"])) { $destination_address2 = $destination_data_arr[0]["address2"];}
	if (isset($destination_data_arr[0]["section"])) { $destination_clerk = $destination_data_arr[0]["section"]; }
	if (isset($destination_data_arr[0]["clerk"])) { $destination_clerk2 = $destination_data_arr[0]["clerk"]; }
	if (isset($destination_data_arr[0]["email"])) { $destination_email = $destination_data_arr[0]["email"]; }
}
if (isset($_REQUEST["get_client_id"]) && $_REQUEST["get_client_id"] != "") { 
	$get_client_id = htmlspecialchars($_REQUEST["get_client_id"],ENT_QUOTES);
	//一件の送付先管理のデータ取得
	$flag = "addressee_data_one";
	$words = "AND company_id = ".$company_id."";
	$client_company_arr = $company_con -> company_sql_flag($pdo,$flag,$get_client_id,$words);
	$flag = "";
	$words = "";
	//var_dump($client_company_arr);
}

if ($client_company_arr[0]["client_id"] != NULL && $client_company_arr[0]["client_id"] != "") {
	if (isset($client_company_arr[0]["client_id"])) { $destination_client_id = $client_company_arr[0]["client_id"];}
	if (isset($client_company_arr[0]["client_company_id"])) { $destination_company_id = $client_company_arr[0]["client_company_id"];}
	if (isset($client_company_arr[0]["company_name"])) { $destination_name = $client_company_arr[0]["company_name"];}
	if (isset($client_company_arr[0]["tel_num"])) { $destination_tel_num = $client_company_arr[0]["tel_num"];}
	if (isset($client_company_arr[0]["zip_code"])) { $destination_zip_code = $client_company_arr[0]["zip_code"];}
	if (isset($client_company_arr[0]["address1"])) { $destination_address = $client_company_arr[0]["prefecture"].$client_company_arr[0]["address1"];}
	if (isset($client_company_arr[0]["address2"])) { $destination_address2 = $client_company_arr[0]["address2"];}
	if (isset($client_company_arr[0]["position"])) { $destination_clerk = $client_company_arr[0]["department"].$client_company_arr[0]["position"]; }
	if (isset($client_company_arr[0]["addressee"])) { $destination_clerk2 = $client_company_arr[0]["addressee"]; }
	if (isset($client_company_arr[0]["email"])) { $destination_email = $client_company_arr[0]["email"]; }

}

//ユーザー企業の情報を取得する。
$flag = "company_data";
$words = "";
$company_one_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$flag="";
$words="";
$template_type = $company_one_arr[0]['template_type'];
$company_one_payment_id = $company_one_arr[0]['payment_id'];
//var_dump($company_one_arr);
$user_company_id = $company_one_arr[0]["company_id"];
//▼ユーザーの現在の総請求0書数を取得する▼
$sql = "SELECT max(cast(`invoice_code` as unsigned)) FROM `INVOICE_DATA_TABLE` WHERE `claimant_id` = ".$value[0]." AND `invoice_code` LIKE '91".date('Ymd')."%' ORDER BY cast(`invoice_code` as unsigned) DESC";
$invoice_auto_arr = $invoice_data_con -> invoice_data_sql($pdo,$company_id,$sql);
$sql = "";
//var_dump($invoice_auto_arr);

$max_num = "91".date('Ymd')."0".rand(10,99);

$invoice_auto_num = 1 + intval($max_num);
//▲ユーザーの現在の総請求書数を取得する▲

function random($length) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}

/**
 * ランダム文字列生成 (英数字)
 * $length: 生成する文字数
 */
function makeRandStr($length) {
    $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
    $r_str = null;
    for ($i = 0; $i < $length; $i++) {
        $r_str .= $str[rand(0, count($str))];
    }
    return $r_str;
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


?>

<script type="text/javascript">
	function preview(ele) {
	    if (!ele.files.length) return;  // ファイル未選択
	    
	    var file = ele.files[0];
	    if (!/^image\/(png|jpeg|gif|tif)$/.test(file.type)) return;  // typeプロパティでMIMEタイプを参照

	    var img = document.createElement('img');
	    var fr = new FileReader();
	    fr.onload = function() {
	        img.src = fr.result;  // 読み込んだ画像データをsrcにセット
	        img.style = "max-width:600px;max-height:auto;";
	        document.getElementById('preview_field').appendChild(img);
	        //document.getElementById('preview_field').style='margin-top:120px;';
	    }
	    fr.readAsDataURL(file);  // 画像読み込み

	    // 画像名・MIMEタイプ・ファイルサイズ
	    
	    document.getElementById('preview_field').innerHTML =
	    '';
	    /*    'file name: ' + file.name + '<br />' +
	        'file type: ' + file.type + '<br />' +
	        'file size: ' + file.size + '<br />';
		*/
	}
</script>

<script type="text/javascript">
function onLoadData() {
	for (var i = 1 ; i <= 15 ; i++ ) {
		Chen(i);
		if (i <= 10) {
				Calc("1"+i+"2");
				Chen("1"+i+"1");
				Chen("1"+i+"2");
				Chen("1"+i+"3");
				Chen("1"+i+"4");
				Chen("1"+i+"5");
				Chen("1"+i+"6");
				Chen("1"+i+"7");
		}
	}
	StartBgCalc();
}

function Chen(n) {
	try {
		if (n == 2 || n == 3 || n == 12) {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			if (Seikyu_No != "") {
				Seikyu_No = Seikyu_No.substring(0, 4)+"/"+Seikyu_No.substring(4, 6)+"/"+Seikyu_No.substring(6, 8);
				parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			}

		} else {
			var Seikyu_No = document.fm_left["seikyu_" + n].value;
			if (n == 4) {
				if (Seikyu_No != "") {
					if (document.fm_left["seikyu_9"].value == "" && document.fm_left["seikyu_8"].value == "") {
						Seikyu_No += "御中";
					}
				}
			} else if (n == 8) {
				if (Seikyu_No != "") {
					if (document.fm_left["seikyu_9"].value == "") {
						Seikyu_No += "御中";
					}
				}

			} else if(n == 9) {
				if (Seikyu_No != "") {
					Seikyu_No += "様";
				}
			} else if(n == 10) {
				if (Seikyu_No !=""){
					Seikyu_No = "件名：" + Seikyu_No;
				}
			}
			try {
				if ((Seikyu_No * 1) >= 0) {
					
				}
			} catch(e){}
			parent.invoice1.document.fm_right["seikyu_" + n].value = Seikyu_No;
			//parent.invoice1.document.getElementById("seikyu_" + n).innerHTML = Seikyu_No;
		}
	} catch (e) {
	}
	//console.log(Seikyu_No);
}

function Calc(n) {
	try {
		n = parseInt(n,10);
		document.fm_left["seikyu_" + (n+2)].value = document.fm_left["seikyu_" + n].value * document.fm_left["seikyu_" + (n+1)].value;
	} catch (e) {
	}
}

function StartBgCalc() {
	try {
		//parent.invoice1.document.fm_right["seikyu_" + n].value
		parent.invoice1.BgCalc();
	} catch (e) {
	}
}

function chCTitle() {
	for (var i = 4 ; i <= 9 ; i++ ) {
		Chen(i);
	}
}

function addRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==11){alert("これ以上登録する事はできません。");return;}
	var row = tblObj.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='hinmoku'><input type='text' id='hinmoku' onkeyup='Chen(1" + rowCnt + "1)' onblur='Chen(1" + rowCnt + "1);' name='seikyu_1" + rowCnt + "1' /></td>\n";
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='tanka'><input type='text' id='tanka' onfocus='if (this.value == \"0\") this.value = \"\";' onkeyup='chkCode(this);Chen(1" + rowCnt + "2)' onblur='Chen(1" + rowCnt + "2);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "2' /></td>\n";
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='suuryou'><input type='text' id='suuryou' onkeyup='chkCode(this);Chen(1" + rowCnt + "3)' onblur='Chen(1" + rowCnt + "3);Calc(1" + rowCnt + "2);Chen(1" + rowCnt + "4);StartBgCalc();' name='seikyu_1" + rowCnt + "3' /></td>\n";
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='goukei'><input type='text' id='goukei' onkeyup='Chen(1" + rowCnt + "4)' onblur='Chen(1" + rowCnt + "4)' name='seikyu_1" + rowCnt + "4'  readonly='readonly' /></td>\n";
	var cell = row.insertCell(4);
	cell.innerHTML = "<td id='shousai'><img src='../images/bluepen.png' id='shousai' name='seikyu_1" + rowCnt + "5' onclick='OpenIdetailFrame(" + rowCnt + ")'><input type='hidden' id='seikyu_1" + rowCnt + "6' name='seikyu_1" + rowCnt + "6' value='1' /><input type='hidden' id='seikyu_1" + rowCnt + "7" + rowCnt + "' name='seikyu_1" + rowCnt + "7' value='0' /></td>\n";

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	var row = tblFr.insertRow(rowCnt);

	var cell = row.insertCell(0);
	cell.innerHTML = "<td id='item'><input type='text' class='item' id='item" + rowCnt + "' name='seikyu_1" + rowCnt + "1' readonly='readonly' /></td>";
	var cell = row.insertCell(1);
	cell.innerHTML = "<td id='cost'><input type='text' class='js-characters-change' id='cost" + rowCnt + "' name='seikyu_1" + rowCnt + "2' readonly='readonly' /></td>";
	var cell = row.insertCell(2);
	cell.innerHTML = "<td id='amount'><input type='text' class='js-characters-change' id='amount" + rowCnt + "' name='seikyu_1" + rowCnt + "3' readonly='readonly' /></td>";
	var cell = row.insertCell(3);
	cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "4' readonly='readonly' /><input type='hidden' class='detail' id='detail" + rowCnt + "' name='seikyu_1" + rowCnt + "5' readonly='readonly' /><input type='hidden' class='sales_tax' id='sales_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "6' readonly='readonly' /><input type='hidden' class='withholding_tax' id='withholding_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "7' readonly='readonly' /></td>";

	//テンプレート側再計算
	onLoadData();
}

function templateConstructer() {
	var tblObjParent = document.getElementById("myTBL");
	var rowCntParent = tblObjParent.rows.length;
	for(var i = 1 ; i < rowCntParent - 1 ; i++ ) {
		var tblFr = invoice1.document.getElementById("frTBL");
		var rowCnt = tblFr.rows.length;
		var row = tblFr.insertRow(rowCnt);
		
		var cell = row.insertCell(0);
		cell.innerHTML = "<td id='item'><input type='text' class='item' id='item" + rowCnt + "' name='seikyu_1" + rowCnt + "1' readonly='readonly' /></td>";
		var cell = row.insertCell(1);
		cell.innerHTML = "<td id='cost'><input type='text' class='js-characters-change' id='cost" + rowCnt + "' name='seikyu_1" + rowCnt + "2' readonly='readonly' /></td>";
		var cell = row.insertCell(2);
		cell.innerHTML = "<td id='amount'><input type='text' class='js-characters-change' id='amount" + rowCnt + "' name='seikyu_1" + rowCnt + "3' readonly='readonly' /></td>";
		var cell = row.insertCell(3);
		cell.innerHTML = "<td id='price'><input type='text' class='price' id='price" + rowCnt + "' name='seikyu_1" + rowCnt + "4' readonly='readonly' /></td>";
		var cell = row.insertCell(4);
		cell.innerHTML = "<td id='detail'><input type='hidden' class='detail' id='detail" + rowCnt + "' name='seikyu_1" + rowCnt + "5' readonly='readonly' /><input type='hidden' class='sales_tax' id='sales_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "6' readonly='readonly' /><input type='hidden' class='withholding_tax' id='withholding_tax" + rowCnt + "' name='seikyu_1" + rowCnt + "7' readonly='readonly' /></td>";

	}
}



function delRow(){
	var tblObj = document.getElementById("myTBL");
	var rowCnt = tblObj.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblObj.deleteRow(-1);

	var tblFr = invoice1.document.getElementById("frTBL");
	var rowCnt = tblFr.rows.length;
	if(rowCnt==2){alert("これ以上削除できません。");return;}
	tblFr.deleteRow(-1);
	//テンプレート側再計算
	onLoadData();
}


function OpenCommonSearchFrame(){
	document.getElementById("CommonSearch").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}


function OpenChangeInvoiceFrame(){
	document.getElementById("ChangeInvoice").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}


function OpenIdetailFrame(n){
	document.getElementById("Idetail").style.display = "block";
	parent.Idetail.document.getElementById('row_num').innerHTML = n;
	parent.Idetail.getData(n);
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);
}





function OpenIntervalFrame(){
	document.getElementById("Interval").style.display="block";
	document.getElementById("fadeLayer").style.visibility = "visible";
	window.scrollTo(0,0);

}


function openClient(){
	document.getElementById("openClient").style.display="block";
}
function kaihei() {
	var elem = document.getElementById('openClient');
	elem.className = (elem.className == 'hide') ? 'show' : 'hide';
}

function DnameInsert() {
//	var dname = document.getElementById('select_dname').value;
//	document.getElementById('seikyu_4').value = dname;
//	Chen(4);
}

function TransClientId() {
	var client_code = document.getElementById('select_dname').value;
	document.getElementById('addressee_company_info').value = client_code;
}

function TransNo() {
	var destination_code = document.getElementById('Kyoutu_No').value;
	document.getElementById('company_info').value = destination_code;
}

function getCompanyInfo() {
	document.getElementById('destination_code_sender').submit();
}

function getAddresseeCompanyInfo() {
	document.getElementById('client_code_sender').submit();
}

function CheckBeforeSend(n) {
	var dest_name = document.getElementById('Kyoutu_No').value;
	var seikyu_no = document.getElementById('seikyu_1').value;
	var kyotu_no = document.getElementById('Kyotu_No_Invisible').value;
	var kanri_no = document.getElementById('send_code').value;
	var dest_email = document.getElementById('destination_email').value;
	
	
	if (dest_name == "" ) {
		alert('請求者名がありません');
		return false;
	}
	if (seikyu_no == "") {
		alert('請求書番号がありません');
		return false;
//	} else if (kyotu_no == "" && kanri_no == ""){
//		alert('共通コードと送付先コードのどちらかを入力してください');return false;
	} else {
		if (n == 0) {
			
			document.fm_left.submit();
			return true;
		}
		//e-mailが未入力の場合
		if (dest_email != "") {
			//n == 1でそのままメール送信
			if (n == 1) {
				//dest_emailは送信先メールアドレス
				if (dest_email != ""){
					document.getElementById('direct_send').value = 1;
					document.fm_left.submit();
					return true;
				} else {
					alert('メールアドレスがありません');
					return false;
				}
			}
		} else {
			alert('メールアドレスがありません');
			return false;
		}
		alert("finished");
	}
}

function saveRegularInvoice() {
	//請求書のテンプレート保存処理
	document.getElementById('save_template').value = 1;
	document.fm_left.submit();
}

function changeTemplate(n) {
	var t_type = document.getElementById("template_type").value = n;
	//alert(t_type);
}
<?php if ($user_company_id == "") {?>
<?php //if ($user_company_id == "005194575604") {?>
$(function(){
	parent.invoice1.location.href = './invoiceframe/meisai3';
	changeTemplate(<?php echo $template_type;?>);
});
<?php } else {?>
$(function(){
	parent.invoice1.location.href = './invoiceframe/invoice9';
	changeTemplate(<?php echo $template_type;?>);
});
<?php }?>
$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( ".seikyu_2" ).datepicker();
});

$(function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#seikyu_3" ).datepicker();
});

$(function(){
    $(".hankaku").change(function(){
        var str = $(this).val();
        str = str.replace( /[Ａ-Ｚａ-ｚ０-９－！”＃＄％＆’（）＝＜＞，．？＿［］｛｝＠＾～￥]/g, function(s) {
            return String.fromCharCode(s.charCodeAt(0) - 65248);
        });
        $(this).val(str);
    }).change();
});


function chkCode(id) {
  work='';
  for (lp=0;lp<id.value.length;lp++) {
    unicode=id.value.charCodeAt(lp);
    if ((0xff0f<unicode) && (unicode<0xff1a)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else if ((0xff20<unicode) && (unicode<0xff3b)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else if ((0xff40<unicode) && (unicode<0xff5b)) {
      work+=String.fromCharCode(unicode-0xfee0);
    } else {
      work+=String.fromCharCode(unicode);
    }
  }
  id.value=work; /* 半角処理のみ */
  //id.value=work.toUpperCase(); /* 大文字に統一する場合に使用 */
  //id.value=work.toLowerCase(); /* 小文字に統一する場合に使用 */
}

function checkCreditCard() {
	var card_check = 
	<?php 
	if (isset($company_one_payment_id)) {
		echo 1;
	} else {
		echo 0;
	}
	?>;
	if (card_check == 0) {
		alert("クレジットカードを登録をしてください。");
		return false;
	} else if (card_check == 1) {
		CheckBeforeSend(1);
	}
	
}


function OpenSouhuListFrame(){
	
	document.getElementById("SouhuList").style.display="block";
	document.getElementById("noneLayer").style.visibility = "visible";
	var x = parent.SouhuList.document.getElementById("result").clientHeight;
	document.getElementById("SouhuList").style.height= x + "px";
	window.scrollTo(0,0);
	
}


function FrameHeight(){
	window.setTimeout("window.parent.frames['CommonSearch'].sizingFrame()","600");
}

function FrameHeight2(){
	window.setTimeout("window.parent.frames['SouhuList'].sizingFrame()","580");
	window.setTimeout("measureFrame()","585");
	
}

function SouhuGrep(){
	var a = document.fm_left.destination_id_visible.value;
	parent.SouhuList.document.myForm.pattern.value=a;
	parent.SouhuList.grep(a);
}

function measureFrame(){
	var y = document.getElementById("SouhuList").clientHeight;
	y = y*1 + 121;
	document.getElementById("CommonSearch").style.marginTop = y + "px";
	
}

function CodeInsertTo4() {
	//プレビューに送る名前を変更する
	document.getElementById("seikyu_4").value = document.getElementById("Kyoutu_No").value;
	//送信先企業名の変更を記録する
	document.getElementById("name_flag").value = 1;
	//送信する共通コードを変更する
	//document.getElementById("Kyotu_No_Invisible").value = 0;
	Chen(4);
}

function modAddress() {
	//送信先住所１の変更を記録する
	document.getElementById("add_flag").value = 1;
}

function modOther() {
	//送信先企業名と住所１以外の変更を記録する
	document.getElementById("other_flag").value = 1;
}

function openPaidDate() {
	console.log(document.getElementById("paid_date"));
	//document.getElementById("paid_date").style = "display:block;";
	document.getElementById("paid_date").style.display = "block";
}

function closePaidDate() {
	console.log(document.getElementById("paid_date"));
	//document.getElementById("paid_date").style = "display:none;";
	document.getElementById("paid_date").style.display = "none";
	document.getElementById("seikyu_12").value = "";
}

function keyPaidDate() {
	document.getElementById("seikyu_2").value = document.getElementById("seikyu_12").value;
	document.getElementById("seikyu_3").value = document.getElementById("seikyu_12").value;
	Chen(2);
	Chen(3);
}

function hideArea() {
	var n = document.getElementById("hide_btn_stutas").value;
	if (n == 0) {
		document.getElementById("seikyu_0").type = "hidden";
		document.getElementById("seikyu_5").type = "hidden";
		document.getElementById("seikyu_6").type = "hidden";
		document.getElementById("seikyu_7").type = "hidden";
		document.getElementById("seikyu_8").type = "hidden";
		document.getElementById("seikyu_9").type = "hidden";
		document.getElementById("destination_email").type = "hidden";
		document.getElementById("industry_type_s").type = "hidden";
		for(i = 2; i <= 9; i++) {
			document.getElementById("h3_" + i).style.display="none";
		}
		document.getElementById("hide_btn_stutas").value = 1;
		document.getElementById("hide_btn").value = "＋";
	} else if (n == 1){
		document.getElementById("seikyu_0").type = "text";
		document.getElementById("seikyu_5").type = "text";
		document.getElementById("seikyu_6").type = "text";
		document.getElementById("seikyu_7").type = "text";
		document.getElementById("seikyu_8").type = "text";
		document.getElementById("seikyu_9").type = "text";
		document.getElementById("destination_email").type = "text";
		document.getElementById("industry_type_s").type = "text";
		for(i = 2; i <= 9; i++) {
			document.getElementById("h3_" + i).style.display = "";
		}
		document.getElementById("hide_btn_stutas").value = 0;
	}
}
$(document).ready(function(){
	hideArea();
});


function inTax() {
	document.getElementById("ex_tax");
	for (var i = 1 ; i <= 9 ; i++ ) {
		var obj = document.fm_left["seikyu_1" + i + "4"];
		if (obj) {
			document.fm_left["seikyu_1" + i + "1"].type="hidden";
			document.fm_left["seikyu_1" + i + "2"].type="hidden";
			document.fm_left["seikyu_1" + i + "3"].type="hidden";
			document.fm_left["seikyu_1" + i + "4"].type="hidden";
			document.fm_left["seikyu_1" + i + "5"].style.display = "none";
			document.getElementById("add_btn").type="hidden";
			document.getElementById("del_btn").type="hidden";
			document.getElementById("first").style.display = "none";
			document.getElementById("in_tax_title").style.display="";
			document.getElementById("in_tax_box").type="text";
		}
	}
	
}
function exTax() {
	document.getElementById("in_tax");
	for (var i = 1 ; i <= 9 ; i++ ) {
		var obj = document.fm_left["seikyu_1" + i + "4"];
		if (obj) {
			document.fm_left["seikyu_1" + i + "1"].type="text";
			document.fm_left["seikyu_1" + i + "2"].type="text";
			document.fm_left["seikyu_1" + i + "3"].type="text";
			document.fm_left["seikyu_1" + i + "4"].type="text";
			document.fm_left["seikyu_1" + i + "5"].style.display = "";
			document.getElementById("add_btn").type="button";
			document.getElementById("del_btn").type="button";
			document.getElementById("first").style.display = "";
			document.getElementById("in_tax_title").style.display="none";
			document.getElementById("in_tax_box").type="hidden";
		}
	}
}

function changeIntax() {
	
	var num = document.getElementById("in_tax_box").value;
	document.fm_left["seikyu_114"].value = num;
	document.fm_left["seikyu_112"].value = Math.round(num * 100 / 108);
	document.fm_left["seikyu_113"].value = 1;
	onLoadData();
}
	
	
	
//Ajaxで処理
function sendByAjax(num) {
	
	//POSTメソッドで送るデータを定義します var data = {パラメータ名 : 値};
	if (num == 1) {
		var data = {get_destination_id : $('#Kyoutu_No').val(),company_id : $('#company_id').val()};
	} else if (num == 2) {
		//var data = {get_client_id : $('#select_dname').val(),company_id : $('#company_id').val(),vendor : 'vendor'};
		var data = {get_client_id : $('#select_dname').val(),company_id : $('#company_id').val()};
	}
	    if (num == 1) {
	    	send_url = "send_company_id_receipt";
	    } else if (num == 2){
	    	send_url = "send_send_code";
	    }

	/**
	 * Ajax通信メソッド
	 * @param type  : HTTP通信の種類
	 * @param url   : リクエスト送信先のURL
	 * @param data  : サーバに送信する値
	 */
	
	$.ajax({
	    type: "POST",
	    	url: send_url,
	    data: data,
	    /**
	     * Ajax通信が成功した場合に呼び出されるメソッド
	     */
	    success: function(data, dataType) {
	        //successのブロック内は、Ajax通信が成功した場合に呼び出される

	        //PHPから返ってきたデータの表示
	        //document.getElementById('Kyoutu_No').innerHTML = data;
	        //alert(data);
	        var address_data = data.split(",");
	        //alert(document.getElementById('bikou').value);
	        for (i = 0 ;i <= 5;i++) {
	        	//alert(address_data[i]);
	        	document.getElementById('seikyu_' + (i + 4)).value = address_data[i];
	        }
	        //alert(address_data);
	        document.getElementById('seikyu_0').value = address_data[8];
	        document.getElementById('destination_email').value = address_data[7];
	        document.getElementById('Kyoutu_No').value = address_data[0];
	        document.getElementById('company_info').value = address_data[6];
	        document.getElementById("Kyotu_No_Invisible").value = document.getElementById('company_info').value;
	        if (num == 1){
	        	document.getElementById('addressee_company_info').value = "";
			}
	        onLoadData();
	        //document.write(data);
	    },
	    /**
	     * Ajax通信が失敗した場合に呼び出されるメソッド
	     */
	    error: function(XMLHttpRequest, textStatus, errorThrown) {
	        //通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、単純に通信に失敗した際の処理を記述します。

	        //this;
	        //thisは他のコールバック関数同様にAJAX通信時のオプションを示します。

	        //エラーメッセージの表示
	        alert('Error : ' + errorThrown);
	    }
	});
}


</script>


<?php 
echo "<p style='color:red;'>".$_SESSION['up_info_msg']."</p>";
$_SESSION['up_info_msg'] = "";


?>
<form id="destination_code_sender" name="destination_code_sender" action="" method="post">
	<input id="company_info" name="get_destination_id" type="hidden" value="" />
</form>
<form id="client_code_sender" name="client_code_sender" action="" method="post">
	<input id="addressee_company_info" name="get_client_id" type="hidden" value="" />
</form>
<input id="company_id" type="hidden" value="<?php $company_id;?>" />

<iframe id="ChangeInvoice" name="ChangeInvoice" src="./frame/ChangeInvoice"></iframe>
<iframe id="Idetail" name="Idetail" src="./frame/Idetail"></iframe>
<iframe id="Interval" name="Interval" src="./frame/Interval"></iframe>

<title>支払い登録 - Cloud Invoice</title>
<article id="makeinvoice">
<iframe id="CommonSearch" name="CommonSearch" src="./frame/CommonSearch" scrolling="no"></iframe>
<!--<iframe id="SouhuList" name="SouhuList" src="./frame/VendorList"></iframe>-->
<iframe id="SouhuList" name="SouhuList" src="./frame/SouhuList"></iframe>
	<form name="fm_left" action="" method="post" enctype="multipart/form-data" onSubmit="return CheckBeforeSend(0);">
		<section id="leftmi">
			<div id="headmi">
				<h2>支払い登録</h2>
			</div>
			
			<div id="kyoutu">
				<h3 id="h3_1">請求者（必須）</h3>
				<input type="text" id="Kyoutu_No" class="CommonSearch" name="destination_id_visible" onClick="OpenSouhuListFrame()" onfocus="OpenSouhuListFrame()" onkeyup="SouhuGrep();TransClientId();OpenCommonSearchFrame();FrameHeight();FrameHeight2();OpenSouhuListFrame();" onchange="CodeInsertTo4();" value="<?php if ($destination_company_id != "") {echo $destination_company_id;}?>"  placeholder="取引先名を入力する" autocomplete="off" />
				<input type="hidden" id="Kyotu_No_Invisible" name="destination_id" value="<?php echo $destination_company_id;?>" />
				<input type="hidden" id="select_dname" onkeyup="DnameInsert();TransClientId();" value="" />
				<input type="hidden" id="souhuname" autocomplete="off" onfocus="FrameHeight2();OpenSouhuListFrame()" onkeyup="TransClientId();" value="<?php if ($destination_client_id != "") {echo $destination_client_id;}?>" placeholder="送付先を入力" />
				<input type="hidden" id="company_addressee_flag" name="company_addressee_flag" value="3" />
				<input type="hidden" id="name_flag" name="name_flag" value="0" />
				<input type="hidden" id="add_flag" name="add_flag" value="0" />
				<input type="hidden" id="other_flag" name="other_flag" value="0" />
				<div id="clear"></div>
			</div>
			<div id="hide_box">
				<input type="button" id="hide_btn" onclick="hideArea();" value="－" style="display:inline;width:30px;height:30px;" />
				<input type="hidden" id="hide_btn_stutas" value="0" />
				<div class="normal">
					<div class="table">
						<h3 id="h3_2">電話番号（任意）</h3>
						<input type="text" onkeyup="Chen(0)" onblur="Chen(0)" name="destination_tel" id="seikyu_0" placeholder="000-000-0000" value="<?php echo $destination_tel_num;?>" onchange="" />
					</div>
					<div class="table">
						<h3 id="h3_3">郵便番号（任意）</h3>
						<input type="text" onkeyup="Chen(5)" onblur="Chen(5)" name="destination_zip_code" id="seikyu_5" placeholder="188-0001" value="<?php echo $destination_zip_code;?>" onchange="modOther();" />
					</div>
					<div id="clear"></div>
				</div>
				
				<div class="normal">
					<div class="table">
						<h3 id="h3_4">住所１（任意）</h3>
						<input type="text" onkeyup="Chen(6)" onblur="Chen(6)" name="destination_address" id="seikyu_6" placeholder="東京都西東京市谷戸町1-22" value="<?php echo $destination_address;?>" onchange="modAddress();" />
					</div>
					<div class="table">
						<h3 id="h3_5">住所２（任意）</h3>
						<input type="text" onkeyup="Chen(7)" onblur="Chen(7)" name="destination_address2" id="seikyu_7" placeholder="グリーンプラザひばりが丘南1-224 " value="<?php echo $destination_address2;?>" onchange="modOther();" />
					</div>
					<div id="clear"></div>
				</div>

				<div class="normal">
					<div class="table">
						<h3 id="h3_6">担当者部署（任意）</h3>
						<input type="text" onkeyup="chCTitle()" onblur="chCTitle()" name="destination_clerk" id="seikyu_8" placeholder="経理部" value="<?php echo $destination_clerk;?>" onchange="modOther();" />
					</div>
					<div class="table">
						<h3 id="h3_7">担当者名（任意）</h3>
						<input type="text" onkeyup="chCTitle()" onblur="chCTitle()" name="destination_clerk2" id="seikyu_9" placeholder="担当者様" value="<?php echo $destination_clerk2;?>" onchange="modOther();" />
					</div>
					<div id="clear"></div>
				</div>

				<div class="normal">
					<div class="table">
						<h3 id="h3_8">Eメールアドレス（任意）</h3>
						<input type="text" onkeyup="" onblur="" name="destination_email" id="destination_email" class="large" placeholder="Eメールアドレス" value="<?php echo $destination_email;?>" onchange="modOther();" />
						<h3 id="h3_9">分類</h3>
						<input type="text" id="industry_type_s" name="industry_type_s" placeholder="食事/バス/鉄道などの分類" />
					</div>
					<div id="clear"></div>
				</div>
			</div>
			<div id="template">
			</div>

			<div id="seikyuushobangou">
				

				<!--<div class="table">-->
					<!--<h3 style="">請求書番号</h3>-->
					<input type="hidden" onkeyup="Chen(1)" onblur="Chen(1)" name="invoice_code" id="seikyu_1" value="<?php echo $invoice_auto_num;?>" style="margin-left:0px;"/>
				<!--</div>-->
				<div class="table">
			<div>
				<h3>支払い請求書・領収書・レシート画像登録</h3>
				<input type="file" name="receipt_image" onchange="preview(this)" />
			</div>

					<h3>請求書名(任意)</h3>
					<input type="text" onkeyup="Chen(10)" onblur="Chen(10)" name="invoice_name" id="seikyu_10" placeholder="請求書名" />
				</div>
				<div id="clear"></div>
			</div>
			<div class="table">
				<div style="float:left;">
					<h3>未払/支払済</h3>
					<label><input type="radio" name="pay" checked="checked" value="6" onchange="closePaidDate()"/>未払い</label>  <label><input type="radio" name="pay" value="9" onchange="openPaidDate()" />支払済</label>
				</div>
				<div style="float:left;margin-left:20px;">
					<h3>支払証憑/人件費</h3>
					<label><input type="radio" name="paper_type" value="1" onchange="" checked />支払証憑</label>  <label><input type="radio" name="paper_type" value="2" onchange="" />人件費</label>
				</div>
				<div id="clear"></div>
			</div>
			<div id="seikyuubi">
				<h3>請求日</h3>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 1;
						var d=now.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input type="text" onkeyup="Chen(2)" onchange="Chen(2)" onblur="Chen(2)" name="billing_date" class="seikyu_2" id="seikyu_2" value="' + hiduke + '"/>');
					</script>

				<div id="clear"></div>
			</div>
			<div id="shiharaikigen">
				<h3>支払期限</h3>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 2;
						var d=now.getDate();
						var dt = new Date(y,m,0);
						var y=dt.getFullYear();
						var m=dt.getMonth()+1;
						var d=dt.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input type="text" onkeyup="Chen(3)" onblur="Chen(3)" onchange="Chen(3)" name="pay_date" id="seikyu_3" value="' + hiduke + '"/>');
					</script>
			</div>
			<div id="shiharaibi">
				<div id="paid_date" style="display:none;">
					<h3>支払日</h3>
					<script>
						var now=new Date();
						var y=now.getFullYear();
						var m=now.getMonth() + 1;
						var d=now.getDate();
						var hiduke=y*10000+m*100+d;
						document.write('<input type="text" onkeyup="Chen(12);keyPaidDate();" onchange="Chen(12);keyPaidDate();" onblur="Chen(12)" name="paid_date" class="seikyu_2" id="seikyu_12" value=""/>');
					</script>
				</div>
				<br/>
				
				<div>税抜<input type="radio" id="ex_tax" name="tax_type" checked="checked" value="1" onchange="exTax()"/>税込<input type="radio"  id="in_tax" name="tax_type" value="2" onchange="inTax()"/></div>
				
				<div id="clear"></div>
			
			</div>
			<div>
				<input id="template_type" type="hidden" name="template_type" value="1" />
			</div>
			<div id="clear"></div>

				<script>
					document.write('<input type="hidden" onkeyup="Chen(4)" onchange="Chen(4)" name="destination_name" id="seikyu_' + '4" value="<?php echo $destination_name;?>"  readonly="readonly" />');
				</script>
				<input type="hidden" name="send_code" id="send_code" value="<?php echo $destination_client_id;?>" />

			<div id="clear"></div>

			<div id="meisai">
				<div id="in_tax_title" style="font-size:12px;font-weight:bold;display:none;margin-top:20px;">税込金額　<input type="hidden" id="in_tax_box" name="in_tax_box" onchange="changeIntax();" /></div>
				<table id="myTBL">
					<tr id="first">
						<th id="hinmoku">品目</th>
						<th id="tanka">単価</th>
						<th id="suuryou">数量</th>
						<th id="goukei">金額</th>
						<th id="shousai">詳細</th>
					</tr>
					<tr>
						<td id="hinmoku">
							<script>
								document.write('<input type="text" onkeyup="Chen(111)" onblur="Chen(111)" id="hinmoku" name="seikyu_' + '111" />');
							</script>
						</td>
						<td id="tanka">
							<script>
								document.write('<input type="text" value="0" onfocus="if (this.value == \'0\') this.value = \'\';" onchange="Chen(112);Calc(112);Chen(114)" onkeyup="chkCode(this);Chen(112);Calc(112);Chen(114)" onblur="Chen(112);Calc(112);Chen(114);StartBgCalc();" id="tanka" name="seikyu_' + '112" />');
							</script>
						</td>
						<td id="suuryou">
							<script>
								document.write('<input type="text" onchange="Chen(112);Calc(112);Chen(114)" onkeyup="chkCode(this);Chen(113);Calc(112);Chen(114)" onblur="Chen(113);Calc(112);Chen(114);StartBgCalc();" id="suuryou" name="seikyu_' + '113" />');
							</script>
						</td>
						<td id="goukei">
							<script>
								document.write('<input type="text" onkeyup="Chen(114)" onblur="Chen(114)" id="goukei" name="seikyu_' + '114" readonly="true" />');
							</script>
						</td>
						<td id="shousai">
							<script>
								document.write('<img src="../images/bluepen.png" id="shousai" name="seikyu_' + '115" onclick="OpenIdetailFrame(1)" />');
								document.write('<input type="hidden" id="seikyu_116" name="seikyu_116" value="1" />');
								document.write('<input type="hidden" id="seikyu_117" name="seikyu_117" value="0" />');
							</script>
						</td>
					</tr>
				</table>
				<input type="button" id="add_btn" value="1行挿入" onclick="addRow()" />
				<input type="button" id="del_btn" value="1行削除" onclick="delRow()">
			</div>
			<div id="bikou">
				<h3>備考</h3>
				<script>
					document.write('<textarea onkeyup="Chen(11)" onblur="Chen(11)" id="bikou" name="seikyu_' + '11"></textarea>');
				</script>
			</div>
			<input type="button" value="支払い登録をする" class="save" onclick="CheckBeforeSend(0);" />
			<input type="hidden" id="template_name" name="template_name" value="" />
			<input type="hidden" id="save_template" name="save_template" value="" />
			<input type="hidden" id="template_cycle" name="template_cycle" value="" />
			<input type="hidden" id="reservation_date" name="reservation_date" value="" />
			<input type="hidden" id="template_maturity_cycle" name="template_maturity_cycle" value="" />
			<input type="hidden" id="template_maturity_date" name="template_maturity_date" value="" />
			<input type="hidden" id="start_date" name="start_date" value="" />
			<input type="hidden" id="end_date" name="end_date" value="" />
			<input type="hidden" id="next_creation_date" name="next_creation_date" value="" />
			<input type="hidden" id="next_payment_date" name="next_payment_date" value="" />
			<input type="hidden" id="send_flag" name="send_flag" value="1" />
			<input type="hidden" id="direct_send" name="direct_send" value="0" />
		</section>
	</form>
	<section id="rightmi">
		<?php echo $imgurl = $title_arr['imgurl'];?>
		<div id="preview_field" style="<?php if ($imgurl != '') {echo 'margin-top:120px;';}?>">
				<span class="name_tag4">
					<img src="<?php echo $imgurl; ?>" alt="<?php if ($imgurl != '') {echo '証憑画像';}?>" class="image-resize" />
				</span>
			
		</div>

		<iframe src="./invoiceframe/invoice1" scrolling="no" name="invoice1" id="invoice1"></iframe>
	</section>
	<div></div>
</article>


<div id="fadeLayer" onclick="CloseFrame(7)"></div>
<div id="noneLayer" onclick="CloseFrame(8)"></div>

<?php require("footer.php");?>