<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ユーザ詳細登録完了画面</title>
<script type="text/javascript">
function form_submit(){
	
	for (var i=0 ; i<7 ; i++){
	 var textvalue=[];
	 textvalue = document.getElementsByTagName('input')[i].value;
     document.write(textvalue,i);
		if ( textvalue == null || textvalue == "" ){
	      alert("項目名が無い行があります。\n項目名を入力してください。");
          return;
		}
	}
	//document.form1.submit();
}
</script>

<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	text-align:center;
}
#wrap{
	position:relative;
	overflow:hidden;
	width:400px;
	height:400px;
	margin-left:auto;
	margin-right:auto;
}
#frame{
	width:400px;
	height:400px;
	text-align:center;
	background-color:#66F;
	padding-top:20px;
	padding-bottom:20px;
	margin-left:20px;
}
#submit_b{
	margin-right:20px;
	text-align:right;
}
</style>
</head>
<body>
<?php
require("/var/www/storage.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once('/var/www/storage.cloudinvoice.co.jp/html/db_con/dbpath.class.php');
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$mail_address = htmlspecialchars($_GET['mail_address']);
//echo  "mail_address=".$mail_address."\n";
$user2_con = new user_control();
$user2_arr = $user2_con->user_select($pdo,$mail_address);

$user2_arr['real_name']  = htmlspecialchars($_GET['yourrealname']);
$user2_arr['user_img']   = htmlspecialchars($_GET['yourimg']);
$user2_arr['bank']       = htmlspecialchars($_GET['yourbank']);
$user2_arr['siten']      = htmlspecialchars($_GET['yourbranch']);
$user2_arr['kouza_type'] = htmlspecialchars($_GET['youraccounttype']);
$user2_arr['kouza_name'] = htmlspecialchars($_GET['youraccountnum']);

$result = $user2_con->user_update($pdo,$user2_arr);
//20140212 var_dump($user2_arr);

//echo "$result=".$result."\n";
?>

	
	
	<div id="wrap">
		<h1>ユーザ詳細登録完了画面</h1>
		<h2>詳細登録が完了しました。</h2><br/>
		<div id="frame">
			<form name="form3" method="get" action="http://hikaku.jp/invoice/login.php" >
			<input type="button" onclick="submit()" name="toroku" value="ログイン画面へ"></form>
		</div>
	</div>
</body>
</html>