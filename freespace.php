<?php
session_start();

//controlクラスファイルの読み込み
/*
include_once(dirname(__FILE__).'/../db_con/datadispcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datatitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/datacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/sitecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/sitetitlecontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/reviewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/viewcontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/boardcontrol.class.php');
*/
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/freespacecontrol.class.php');

//データベースに接続
$con_db = new dbpath();
$pdo = $con_db -> db_connect();
$free_con = new freespace_control();

//変数
$user_id;
$site_id;
$data_kbn;
$free_record_id;
$free_kbn;
$image_link;

$freespace_arr = array();
//site_idから
if(isset($_SESSION['user_id'])) {
	$user_id = $freespace_arr['user_id']  = htmlspecialchars($_SESSION['user_id'],ENT_QUOTES);
}

if (isset($_REQUEST['record_id'])) {
	$site_id = $freespace_arr['site_id']  = htmlspecialchars($_REQUEST['site_id'],ENT_QUOTES);
	$record_id = $freespace_arr['record_id'] = htmlspecialchars($_REQUEST['record_id'],ENT_QUOTES);
	$free_space_kbn = $freespace_arr['free_space_kbn'] = htmlspecialchars($_REQUEST['free_space_kbn'],ENT_QUOTES);
	$free_space_rank = $freespace_arr['free_space_rank'] = htmlspecialchars($_REQUEST['position'],ENT_QUOTES);
	$data_kbn = $freespace_arr['data_kbn'] = htmlspecialchars($_REQUEST['data_kbn'],ENT_QUOTES);
	if ($data_kbn == 4) {
		$article = $freespace_arr['article'] = $_REQUEST['article'];
	} else {
		$article = $freespace_arr['article'] = htmlspecialchars($_REQUEST['article'],ENT_QUOTES);
	}
	if ($data_kbn == 2) {
		$image_link = $freespace_arr['image_link'] = htmlspecialchars($_REQUEST['image_link'],ENT_QUOTES);
		$words = $freespace_arr['words'] = htmlspecialchars($_REQUEST['words'],ENT_QUOTES);
	}
	$title = $freespace_arr['title'] = htmlspecialchars($_REQUEST['title'],ENT_QUOTES);
}
//var_dump($freespace_arr);

//画像ファイルのアップロード処理
if ($_FILES['article']["tmp_name"]) {
	//ディレクトリの作成
	$path = "../images/". $user_id."/".$site_id. "/"."freespace";
	if ( mkdir( $path, 0755, true ) ) {
	  //echo "ディレクトリ作成成功！！";//開発確認用
	} else {
	  //echo "ディレクトリ作成失敗！！";//開発確認用
	}
}

//画像のアップロード
if (is_uploaded_file($_FILES['article']["tmp_name"])) {

  if (move_uploaded_file($_FILES['article']["tmp_name"], $path. "/" . $_FILES['article']["name"])) {
    chmod($path. "/" . $_FILES['article']["name"], 0644);
    //echo $path. "/" . $_FILES['article']["name"] . "をアップロードしました。";
	$article = $freespace_arr['article'] = $path. "/" . $_FILES['article']["name"];
  } else {
    //echo "ファイルをアップロードできません。";
  }
} 

if($freespace_arr['article'] != NULL) {
	//ポジションテーブルから当該カラム名とランク等の情報を取得
	echo $freespace_data = $free_con -> freespace_insert($pdo,$freespace_arr);
	echo '<input type="button" value="ウィンドウを閉じる" style="magin-left:auto;margin-right:auto;" onclick="window.close();" />';
	echo '<script type="text/javascript">window.close();</script>';
	exit();
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja" xmlns:og="http://ogp.me/ns#" xmlns:mixi="http://mixi-platform.com/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo $site_title_data['site_title']; ?>比較,<?php echo $site_title_data['titletag01'];echo ',';echo $site_title_data['titletag02'];echo ',';echo $site_title_data['titletag03'];echo ',';echo $site_title_data['titletag04'];echo ',';echo $site_title_data['titletag05'];echo ',';echo $site_title_data['update_date'];?>,ランキング,カフェ,家電,テレビ,買い物,ショッピング,パソコン,ノートパソコン,タブレット,iphone,食品,食べ物,果物,特産品,服,インテリア,雑貨,小物,金融,証券,FX,投資,冷蔵庫,洗濯機" /> 
		<meta name="description" content="<?php echo $site_title_data['introduction']; ?> - 世の中の全てものを誰でも簡単に自由に比較、ランキングすることができるキュレーションサイトです。誰もが自分の得意な事や好きな事を簡単かつスマートに比較してみんなでオススメしあって良いものの情報を世の中に広めましょう。" />		
		<title><?php echo $site_title_data['site_title']; ?>比較 - [比較.jp]</title>
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link rel="shortcut icon" href="http://hikaku.jp/common/images/favicon.jpg" />
		<link rel="canonical" href="http://hikaku.jp/" />
		<script src="http://hikaku.jp/common/js/jquery-1.8.3.min.js" type="text/javascript" charset="UTF-8"></script>
		<script src="http://hikaku.jp/sp/common/js/common.js" type="text/javascript" charset="UTF-8"></script>
		<meta name="viewport" content="width=device-width">
		<script type="text/javascript">
			$(function(){
				$("input#change").each(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
				$("input#change").click(function(){
					var chk = $(this).attr('checked');
					if(chk == true){
						$("table#change_list input,table#change_list textarea").attr('disabled','');
					}else{
						$("table#change_list input,table#change_list textarea").attr('disabled','disabled');
					}
					return true;
				});
			});
		</script>
		<script type="text/javascript">
			function changeBox(type) {
				
				if (type == "text") {
					document.getElementById('freeBox1').style.display='';
					document.getElementById('freeBox2').style.display='none';
					document.getElementById('freeBox3').style.display='none';
					document.getElementById('freeBox4').style.display='none';
				} else if(type == "image") {
					document.getElementById('freeBox1').style.display='none';
					document.getElementById('freeBox2').style.display='';
					document.getElementById('freeBox3').style.display='none';
					document.getElementById('freeBox4').style.display='none';

				} else if (type == "link") {
					document.getElementById('freeBox1').style.display='none';
					document.getElementById('freeBox2').style.display='none';
					document.getElementById('freeBox3').style.display='';
					document.getElementById('freeBox4').style.display='none';

				} else if (type == "frame") {
					document.getElementById('freeBox1').style.display='none';
					document.getElementById('freeBox2').style.display='none';
					document.getElementById('freeBox3').style.display='none';
					document.getElementById('freeBox4').style.display='';
				}
				
			}
		</script>
		
		
	<!--暫定のスタイルシート20140212報告用▽-->
	<style type="text/css">
	body {
		width:100%;
		height:100%;
	}
	#wrap {
		width:100%;
		height:100%;
	}
	#container {
		padding:10px;
		width:480;
		height:480;
		margin-left:auto;
		margin-right:auto;
	}
	.display_rank {
		text-align:center;
	}
	.display_rank_data{
		text-align:center;
	}
	.image-box{
		text-align:center;
	}
	.input_form{
		padding:5px;
	}
	.closeBtn {
		text-align:left;
		margin-left:auto;
		margin-right:auto;
		margin-top:10px;
	}
	</style>
	<!--暫定のスタイルシート△-->
	</head>
	<body>
		<div id="wrap">
			<div id="container">
				<input type="button" onclick="changeBox('text')" value="文を書く"/><input type="button" onclick="changeBox('image')" value="画像を貼る"/><input type="button" onclick="changeBox('link')" value="外部リンクする"/><input type="button" onclick="changeBox('frame')" value="動画コードを埋め込む"/>
	            <!--▽フリースペース投稿フォーム▽-->
	            <form id="freeBox1" style="display:none;" name="freeBox1" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;" onsubmit="return checkBlank();"  enctype="multipart/form-data">
					<input type="hidden" name="record_id" value="<?php echo $record_id; ?>">
					<input type="hidden" name="free_space_kbn" value="<?php echo $free_space_kbn; ?>">
					<input type="hidden" name="data_kbn" value="1">
					<input type="hidden" name="free_space_rank" value="<?php echo $free_space_rank; ?>">
			        <input type="hidden" name="title" value=" ">
	            	<p>文章を書く：<textarea id="article1" name="article" cols="50" rows="4"  ></textarea></p>
	            	<p class="tright" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="送信する"/></p>
	            </form>
	            <form id="freeBox2" style="display:none;" name="freeBox2" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;" onsubmit="return checkBlank();"  enctype="multipart/form-data">
					<input type="hidden" name="record_id" value="<?php echo $record_id; ?>">
					<input type="hidden" name="free_space_kbn" value="<?php echo $free_space_kbn; ?>">
					<input type="hidden" name="data_kbn" value="2">
					<input type="hidden" name="free_space_rank" value="<?php echo $free_space_rank; ?>">
	            	<p>画像のタイトル：<input type="text" id="title" name="title" size="40" value="画像" ></p>
				    <p>画像のリンク：<input type="text" id="image_link" name="image_link" size="40" value="リンク先" ></p>
	            	<p>画像を投稿する：<input type="file" id="article2" name="article" ></p>
				    <p>画像の説明文：<textarea id="words" name="words" cols="50" rows="4"  ></textarea></p>
	            	<p class="tright" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="送信する"/></p>
	            </form>
	            <form id="freeBox3" style="display:none;" name="freeBox3" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;" onsubmit="return checkBlank();"  enctype="multipart/form-data">
					<input type="hidden" name="record_id" value="<?php echo $record_id; ?>">
					<input type="hidden" name="free_space_kbn" value="<?php echo $free_space_kbn; ?>">
					<input type="hidden" name="data_kbn" value="3">
					<input type="hidden" name="free_space_rank" value="<?php echo $free_space_rank; ?>">
	            	<p>リンクに表示するタイトル：<input type="text" id="title" name="title" size="40" value="外部リンク" ></p>
	            	<p>外部リンクを貼る：<input type="text" id="article3" name="article" size="40"  ></p>
	            	<p class="tright" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="送信する"/></p>
	            </form>
	            <form id="freeBox4" style="display:none;" name="freeBox4" action="" method="post" style="margin-top:10px;margin-bottom:10px;line-height:1.5em;border-top:1px solid #999;padding-top:10px;" onsubmit="return checkBlank();"  enctype="multipart/form-data">
					<input type="hidden" name="record_id" value="<?php echo $record_id; ?>">
					<input type="hidden" name="free_space_kbn" value="<?php echo $free_space_kbn; ?>">
					<input type="hidden" name="data_kbn" value="4">
					<input type="hidden" name="free_space_rank" value="<?php echo $free_space_rank; ?>">
	            	<p>動画のタイトル：<input type="text" id="title" name="title" size="40" value="動画" ></p>
	            	<p>動画の埋め込みコード：<input type="text" id="article3" name="article" size="40"  ></p>
	            	<p class="tright" style="margin-top:10px;margin-bottom:10px;"><input type="submit" value="送信する"/></p>
	            </form>

				<div class="closeBtn">
					<input type="button" value="ウィンドウを閉じる" onclick="window.close();" />
				</div>
	            	
				<!--△フリースペース投稿フォーム△-->
			</div><!--container-->
		</div><!--wrap-->
	</body>
</html>