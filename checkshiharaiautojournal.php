<?php
session_start();
		
//必要なクラスの読み込み
include_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//自動仕訳用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//データベースに接続
$dbpath_con = new dbpath();
$pdo = $dbpath_con -> db_connect();
$invoice_data_con = new invoice_data_control();
$company_con = new company_control();

//自動仕訳用コントローラーの準備
$pdo_aj = $dbpath_con -> change_db_connect();
$auto_journalize_con = new auto_journalize_control();


if (isset($_SESSION['user_id'])) {
	$user_id = $company_id = $_SESSION['user_id'];
} else {
	header("Location:./logout");
	exit();
}
var_dump($_REQUEST);
var_dump($_POST);

//プランの確認
$flag = "company_data";
$words = "";
$company_data_num = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
$plan = $company_data_num[0]['plan_status'];

if ($plan == 3){
	//echo "プレミアムです<br/>\r\n";
} else if ($plan == 3) {
	//echo "スタンダードです<br/>\r\n";
	//header("Location:./main");
	//exit();
} else {
	//echo "フリーです<br/>\r\n";
	//header("Location:./main");
	//exit();
}



if ( $_POST['mode'] === 'download' ) {

	//変数
	$words = " and `status` >= 1 ";

	if ( $_REQUEST['koumoku'] == 1 ) {
		//明細別
		$flag1 = "invoice_count";
		$flag = "invoice_data";
	} else if ($_REQUEST['koumoku'] == 0) {
		//請求別
		$flag1 = "invoice_total_count";
		$flag = "invoice_total_data";
	}
	if ($_REQUEST['destination_id']) {
		$claimant_id = $_REQUEST['destination_id'];
		$words .= " AND `claimant_id` = ".$claimant_id." ";
	}
	if ($_REQUEST['update_from']){
			$upfrom = str_replace(" ","",substr($_REQUEST['update_from'],0,4)."/".substr($_REQUEST['update_from'],4,2)."/".substr($_REQUEST['update_from'],6,2));
			$words .= " and `update_date` >= '".$upfrom."' ";
	}
	if ($_REQUEST['update_to']){
			$upto = str_replace(" ","",substr($_REQUEST['update_to'],0,4)."/".substr($_REQUEST['update_to'],4,2)."/".substr($_REQUEST['update_to'],6,2));
			$words .= " and `update_date` <= '".$upto."' ";
	}
	if ($_REQUEST['paydate_from']) {
		$payfrom = $_REQUEST['paydate_from'];
		$words .= " and `pay_date` >= ".$payfrom." ";
	}
	if ($_REQUEST['paydate_to']) {
		$payto = $_REQUEST['paydate_to'];
		$words .= " and `pay_date` <= ".$payto." ";
	}
	$words .= "ORDER BY `invoice_code`";
	
	//INVOICE_DATA_TABLEからCSVにするデータを取得
	$invoice_data_num = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag1,$company_id,$words);
	$invoice_data_arr = $invoice_data_con -> invoice_data_sql_flag($pdo,$flag,$company_id,$words);




	//▼自動仕訳用企業DB登録▼
	//もし存在しなければ、企業品目テーブルと企業用レコードテーブルを作成
	$table_name = "".$company_id;
	try {
		$auto_journalize_con -> create_auto_journalize_table($pdo_aj,$table_name);
	} catch (PDOException $e) {
	    echo htmlspecialchars($e->getMessage(),ENT_QUOTES,'UTF-8');
	    $_SESSION['error_msg'] = $e->getMessage();
	}
	//▲自動仕訳用企業DB登録▲

	//var_dump($invoice_data_num);
	//var_dump($invoice_data_arr);
	//$csv_data .= "請求元共通コード,請求元会社名,請求日付,請求書番号,請求書名,当社担当者,支払日,振込口座,売上日付,品番,品名,単価,数量,単位,税抜き合計金額,消費税率,源泉徴収税,合計金額,備考1,備考2,備考3\n";
    //出力ファイル名の作成
    $csv_file = "invoice_data_". date ( "Ymd" ) .'.csv';
  
    //文字化けを防ぐ
    $csv_data = mb_convert_encoding ( $csv_data , "utf-8" , 'utf-8' );
      
    //MIMEタイプの設定
    //header("Content-Type: application/octet-stream");
    //名前を付けて保存のダイアログボックスのファイル名の初期値
    //header("Content-Disposition: attachment; filename={$csv_file}");
  
    // データの出力
    //echo($csv_data);
    //exit();
}
?>

<?php require("header.php");?>


<title>自動仕訳確認画面 - Cloud Invoice</title>

<article>
	<section>
		<form action="./shiharaiautojournalcsv" method="post">
			<?php
			$count_data_num = $invoice_data_num[0]["count(*)"];
			if ( $count_data_num > 0) {
			echo '<input type="button" onclick="submit()" value="データを送る" />';
				$invoice_code_checker = "";
				$claimant_id_checker = "";
			?>
				
				<?php 
					for ( $i = 0; $i < intval($count_data_num); $i++ ) {
						if ($invoice_code_checker != $invoice_data_arr[$i]['invoice_code'] || $claimant_id_checker != $invoice_data_arr[$i]["claimant_id"]) {
							$invoice_code_checker = $invoice_data_arr[$i]['invoice_code'];
							$claimant_id_checker = $invoice_data_arr[$i]["claimant_id"];
				?>
					<table id="invoice_table" >
						<tr><th>*</th><th>請求元共通コード</th><th>請求元会社名</th><th>請求日付</th><th>請求書番号</th><th></th><th>担当者名</th><th>支払日</th></tr>
						<tr>
							<td>*</td>
							<td><?php echo $invoice_data_arr[$i]["claimant_id"];?></td>
							<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
							<td>
							<?php
								//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
								$flag = "company_data";
								if ($claimant_id != 0 ) {
									$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
									//echo "<br/>claimant<br/>\r\n";
									echo $claimant_company_name[0]['company_name'];
								} else {}
							?>
							</td>
							<td><?php echo $invoice_data_arr[$i]["billing_date"];?></td>
							<td><?php echo $invoice_data_arr[$i]['invoice_code'];?></td>
							<td></td>
							<td><?php echo $invoice_data_arr[$i]["staff_name"];?></td>
							<td><?php echo $invoice_data_arr[$i]["pay_date"];?></td>
						</tr>
				</table>
				<table id="account_table">
					<tr><th></th><th></th><th>仕訳科目</th><th></th><th></th><th></th><th>合計金額</th><th>消費税額</th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>品名＋備考</th><th>品名</th></tr>
					<?php
						}
						
					?>
						<tr>
							<input type="hidden" name="claimant_id<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['claimant_id'];?>" />
							<?php $claimant_id = $invoice_data_arr[$i]["claimant_id"];?>
							<?php
								//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
								$flag = "company_data";
								if ($claimant_id != 0 ) {
									$claimant_company_name = $company_con -> company_sql_flag($pdo,$flag,$claimant_id,$words);
								} else {}
							?>
							<input type="hidden" name="company_name<?php echo $i;?>" value="<?php echo $claimant_company_name[0]['company_name'];?>">
							<?php
							/*
								//請求元のcompany_idを元に請求元の会社名を検索してCSVデータに挿入する。。
								$dest_id = $invoice_data_arr[$i]["destination_id"];
								$flag = "addressee_data_receive_client_id";
								$words = " AND company_id = ".$claimant_id."";
								$client_id_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
							*/
							/*
								if ($client_id_arr[0]['client_id'] == NULL) {
									$csv_data .= ",";
								} else {
									$csv_data .= $client_id_arr[0]['client_id'].",";
								}
							*/
							?>
							<input type="hidden" name="billing_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['billing_date'];?>" />
							<input type="hidden" name="invoice_code<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['invoice_code'];?>" />
							<input type="hidden" name="staff_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['staff_name'];?>" />
							<input type="hidden" name="pay_date<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['pay_date'];?>" />
							<td></td><td></td>
							<td>
							<?php
								//自動仕訳
								//ステップ１"請求元の共通コード 品名"
								
								$flag = "auto_journal_step1";
								$search_key = $invoice_data_arr[$i]["claimant_id"]." ".$invoice_data_arr[$i]["product_name"];
								$table_name="RECORD".$invoice_data_arr[$i]["destination_id"];
								$aj_words = $search_key;
								$auto_journal_data_arr = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
							?>
							<input type='text' id='debit_account_name<?php echo $i;?>' name='debit_account_name<?php echo $i;?>' value='<?php echo $auto_journal_data_arr[0]["debit_account_name"];?>' />
							<input type='hidden' id='count_flag<?php echo $i;?>' name='count_flag<?php echo $i;?>' value="1" />
							
							<?php
								$flag = "auto_journal_step2";
								$table_name = $invoice_data_arr[$i]["destination_id"];
								$aj_words = $invoice_data_arr[$i]["product_name"];
								$auto_journal_data_arr2 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
								
								if (count($auto_journal_data_arr2) > 0) {
							?>
									<select id='account_names<?php echo $i;?>' name='step2_data<?php echo $i;?>' onclick='chengeAccountName(<?php echo $i;?>,2)' >
							<?php
									for ($q = 0;$q < count($auto_journal_data_arr2);$q++) {
							?>
									<option value='<?php echo $auto_journal_data_arr2[$q]["account_name"];?>'><?php echo $auto_journal_data_arr2[$q]['account_name'];?></option>
							<?php
								}
							?>
									</select>
							<?php
								} else {
									$flag = "auto_journal_step3";
									$table_name = "MAIN";
									$aj_words = $invoice_data_arr[$i]["product_name"];
									$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);
									if (count($auto_journal_data_arr3) == 0) {
										unset($auto_journal_data_arr3);
										$flag = "auto_journal_step3";
										$table_name = "MAIN";
										$aj_words = "*";
										$auto_journal_data_arr3 = $auto_journalize_con -> auto_journalize_sql_flag($pdo_aj,$flag,$table_name,$aj_words);

									}
									if (count($auto_journal_data_arr3) > 0) {
							?>
									<select id='account_names<?php echo $i;?>' name='step3_data<?php echo $i;?>' onclick='chengeAccountName(<?php echo $i;?>,3);'>
							<?php
										for ($q = 0;$q < count($auto_journal_data_arr3);$q++) {
							?>
											<option value='<?php echo $auto_journal_data_arr3[$q]["account_name"];?>'><?php echo $auto_journal_data_arr3[$q]['account_name'];?></option>
							<?php
										}
							?>
										</select>
							<?php
									}
									//"<input type='text' name='auto_csv_data[]' value='".$auto_journal_data_arr3[$q]['account_name']."' />";
								}
							?>
							</td><td></td><td></td><td></td>
							<td>
							<?php
								//$csv_data .= $invoice_data_arr[$i]["bank_account"]."</td><td>";
								//$csv_data .= $invoice_data_arr[$i]["sale_date"]."</td><td>";
								//$csv_data .= $invoice_data_arr[$i]["product_code"]."</td><td>";
								
								//$csv_data .= $invoice_data_arr[$i]["unit_price"]."</td><td>";
								//$csv_data .= $invoice_data_arr[$i]["quantity"]."</td><td>";
								//$csv_data .= $invoice_data_arr[$i]["unit"]."</td><td>";
								if ( $invoice_data_arr[$i]['sum(total_price_excluding_tax)'] != NULL ) { 
									echo $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];
							?>
							<input type="hidden" name="sum(total_price_excluding_tax<?php echo $i;?>)" value="<?php echo $invoice_data_arr[$i]['sum(total_price_excluding_tax)'];?>" />
							</td>
							<td>
							<?php
								} else { 
									
									echo $invoice_data_arr[$i]["total_price"];
							?>
							<input type="hidden" name="total_price<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['total_price'];?>" />
							</td>
							<td>
							<?php
								}
								echo $invoice_data_arr[$i]["sales_tax"];
							?>
							<input type="hidden" name="sales_tax<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['sales_tax'];?>" />
							</td>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
							<td>
							<?php
							
							/*
							$csv_data .= $invoice_data_arr[$i]["withholding_tax"]."</td><td>";
							if ( $invoice_data_arr[0]['sum(total_price)'] != NULL ) { 
								$csv_data .= $invoice_data_arr[$i]['sum(total_price)']."</td><td>"; 
							} else { 
								$csv_data .= $invoice_data_arr[$i]["total_price"]."</td><td>";
							}
							*/
							echo $invoice_data_arr[$i]["product_name"].' '.$invoice_data_arr[$i]["remarks1"];
							?>
							<input type="hidden" name="remarks<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['product_name'].' '.$invoice_data_arr[$i]['remarks1'];?>" />
							</td>
							<td><?php echo $invoice_data_arr[$i]["product_name"];?><input type="hidden" name="product_name<?php echo $i;?>" value="<?php echo $invoice_data_arr[$i]['product_name'];?>" /></td></tr>
					<?php
					}
					?>
				
				</table>
			<?php
			}
			?>
			<input type="hidden" name="data_num" value="<?php echo $count_data_num;?>" />
		</form>
	</section>
</article>

<?php require("footer.php");?>