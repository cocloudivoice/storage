<?php session_start();

//必要なクラスファイルを読み込む
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');

//変数の宣言
$profile;
$sex;


//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();

if(isset($_SESSION['user_id'])){
	$user_id = $_SESSION['user_id'];
}else{
	header("Location:http://hikaku.jp/invoice/logout.php");
	exit();
}

//ユーザーテーブルのコントローラーを呼び出す
$user_con = new user_control();
$user_arr  = $user_con->user_select_id($pdo,$user_id);

if(isset($user_arr['nick_name'])){
	$nickname = $user_arr['nick_name'];
}
if(isset($user_arr['user_img'])){
	$image = $user_arr['user_img'];
}
if(isset($user_arr['kana'])){
	$sex = $user_arr['kana'];
}
if(isset($user_arr['profile'])){
	$profile = $user_arr['profile'];
}

if(isset($user_arr['real_name'])){
	$real_name = $user_arr['real_name'];
}

if(isset($user_arr['bank'])){
	$bank = $user_arr['bank'];
}

if(isset($user_arr['siten'])){
	$siten = $user_arr['siten'];
}

if(isset($user_arr['kouza_name'])){
	$kouza_name = $user_arr['kouza_name'];
}
if(isset($user_arr['kouza_type'])){
	$kouza_type = $user_arr['kouza_type'];
}
if(isset($user_arr['password'])){
	$password = $user_arr['password'];
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="Content-Type" />
<meta content="IE=edge" http-equiv="X-UA-Compatible" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<title>ユーザー情報変更画面</title>
<script type="text/javascript">
	function checkPass() {
		var p1 = form3.password.value;
		var p2 = form3.password2.value;
		
		if (p1 == p2) {
			form3.submit();
			return true;
			
		}else{
			alert('パスワードが一致していません');
			return false;
		}
		
	}

</script>
<style type="text/css">
html *{
	margin:0px
	padding:0px;
}
body{
	font-size:16px;
	line-height:1.2em;
	text-align:center;
	font-family: "Meiryo", "メイリオ", "ヒラギノ角ゴ Pro W3", sans-serif;
}

h1,h2 {
  font-size: 16px;
}

#wrap{
	position:relative;
	overflow:hidden;
	width:550px;
	height:auto;
	margin-left:auto;
	margin-right:auto;
}
.frame{
	width:360px;
	height:auto;
	text-align:left;
	background-color:#FCC;
	padding-top:10px;
	padding-bottom:20px;
	padding-left:30px;
	padding-right:20px;
	margin-left:auto;
	margin-right:auto;
}
#submit_b{
	margin-right:100px;
	text-align:right;
}
.name_tag{
	margin-right:20px;
	text-align:left;
}
.name_tag1{
	margin-top:20px;
	margin-right:5px;
	margin-left:5px;
	text-align:left;
}
.name_tag2{
	margin-right:5px;
	margin-left:5px;
	text-align:left;
}
.name_tag3{
	margin-top:20px;
	margin-right:5px;
	margin-left:5px;
	text-align:left;
	vertical-align:top;
}
#name_tag4{
	height:150px;
	margin-top:180px;
	margin-right:5px;
	margin-left:5px;
	text-align:left;
	vertical-align:middle;
}
.vmiddle{
	vertical-align:middle;
}
.mleft{
	margin-left:10px;
}
.mright{
	margin-right:10px;
}
.mtop{
	margin-top:10px;
}
.mbottom{
	margin-bottom:10px;
}
.fright{
	float:right;
}
#returnBtn{
	margin-right:70px;
}
.err {
	color:red;
	font-weight:bold;
}

</style>
</head>
<body>
	
	
	<div id="wrap">
		<h1>ユーザ詳細登録画面</h1>
		<h2>ユーザープロフィール</h2>
		<p class="fright" id="returnBtn"><a href="http://hikaku.jp/invoice/mypage.php">マイページにもどる</a></p>
		<br style="clear:both;" />
		<div class="frame">
			<form name="form1" method="post" action="http://hikaku.jp/invoice/mod_userinfo_up.php" enctype="multipart/form-data">
	            <p>企業名 :　<input name="nick_name" size="40" maxlength="40" type="text" value="<?php echo $nickname; ?>" /></p>
	    		
	            <p>フリガナ:　<SELECT  name="kana"><?php if(!$kana){ echo '<OPTION value="0" checked >男性</OPTION><OPTION value="1">女性</OPTION>';} else { echo '<OPTION value="1" checked >女性</OPTION><OPTION value="0" >男性</OPTION>'; }?></SELECT></p>
	            <p>プロフィール:　<textarea name="profile" cols="40" rows="4" style="background-color:#ffffff;"><?php echo $profile; ?></textarea></p>
	            <p class="name_tag1"><span class="name_tag3">CSVデータ:</span><span class="name_tag4"><img src="<?php if(isset($user_arr['csv_data'])){echo $user_arr['csv_data'];}else{echo '../images/base/no_image.jpg';} ?>" height="150px" /></span>
	            <input type="file" name="csv_data" style="width:300px" value="<?php echo $user_arr['csv_data']; ?>"/></p>
				<p id="submit_b"><input type="button" onclick="submit()" name="toroku" value="情報更新" /></p>
			</form>
		</div>
		
　　　　<h2>支払い情報登録</h2>
		<div class="frame">
			<form name="form2" method="post" action="http://hikaku.jp/invoice/mod_userinfo_up.php" >
				<p class="name_tag">口座の名義人名（本人）  :　<br/><input type="text" name="yourrealname" size="55" maxlength="40"  value="<?php echo $real_name; ?>" ></p>
	            <p class="name_tag">あなたの本名名義の銀行口座</br>
	            <p>銀 行 名 :　<input name="yourbank" size="40" maxlength="40" type="text" value="<?php echo $bank; ?>" />
	            <p>支 店 名 :　<input name="yourbranch" size="40" maxlength="20" type="text"  value="<?php echo $siten; ?>" /> 
	            <p>口座区分:　<SELECT  name="youraccounttype"><OPTION value="0" <?php if($kouza_type==0){echo selected;} ?>>普通</OPTION><OPTION value="1" <?php if($kouza_type==1){echo selected;} ?>>当座</OPTION></SELECT>
	            <p>口座番号:　<input name="youraccountnum" size="20" maxlength="7" type="text"  value="<?php echo $kouza_name; ?>" /> 
				<p id="submit_b"><input type="button" onclick="submit()" name="toroku" value="情報更新" /></p>   
			</form>
		</div>
		<h2>パスワードの変更</h2>
		<div class="frame">
			<form name="form3" method="post" action="http://hikaku.jp/invoice/mod_userinfo_up.php" onsubmit="return checkPass();false;"><?php if ($_SESSION['check_temp_password'] != "") {echo "<p class=\"err\">".$_SESSION['check_temp_password']."</p>";} ?>
	            <p>現在のパスワード :　<input name="temp_password" size="40" maxlength="40" type="password" value="" /></p>
	            <p>新しいパスワード :　<input name="password" size="40" maxlength="40" type="password" /></p>
	            <p>新しいパスワード(再) :　<input name="password2" size="40" maxlength="40" type="password" /></p>
				<p id="submit_b"><input type="button" onclick="return checkPass();false" name="toroku" value="情報更新" /></p>
			</form>
		</div>
	</div>
</body>
</html>
<?php echo $_SESSION['check_temp_password'] = "";?>