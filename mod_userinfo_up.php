<?php session_start();

//クラスファイル読み込み
require("/var/www/co.cloudinvoice.co.jp/html/db_con/usercontrol.class.php");
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');

//DBに接続
$db_con = new dbpath();
$pdo = $db_con->db_connect();

//変数の宣言
$mail_address;
$user_arr = array();
$user_con;

if(isset($_SESSION['user_id'])){
	$user_id = $_SESSION['user_id'];
}

//echo  "mail_address=".$mail_address."\n";
//ユーザーテーブルのオブジェクトを呼び出して情報を取得する
$user_con = new user_control();
$user_arr = $user_con->user_select_id($pdo,$user_id);

if(isset($_SESSION['user_id'])){
	$user_arr['user_id'] = $_SESSION['user_id'];
}
if(isset($_POST['profile'])){
	$user_arr['profile']  = htmlspecialchars($_POST['profile'],ENT_QUOTES);
}

if(isset($_POST['kana'])){
	$user_arr['kana']  = htmlspecialchars($_POST['kana'],ENT_QUOTES);
}

if(isset($_POST['nick_name'])){
	$user_arr['nick_name']  = htmlspecialchars($_POST['nick_name'],ENT_QUOTES);
}

if(isset($_POST['temp_password'])){
	$temp_password = md5(htmlspecialchars($_POST['temp_password'],ENT_QUOTES));
	if ($temp_password != $user_arr['password']) {
		$user_arr['password'];
		$_SESSION['check_temp_password'] = "現在のパスワードが間違っています。";
		header("Location:./mod_userinfo.php");
		exit();
	}
}

if(isset($_POST['password'])){
	$user_arr['password'] = md5(htmlspecialchars($_POST['password'],ENT_QUOTES));
	$_SESSION['check_temp_password'] = "パスワードを変更しました。";
}

if(isset($_POST['yourrealname'])){
	$user_arr['real_name']  = htmlspecialchars($_POST['yourrealname'],ENT_QUOTES);
}
if(isset($_POST['yourimg'])){
	$user_arr['user_img']   = htmlspecialchars($_POST['yourimg'],ENT_QUOTES);
}
if(isset($_POST['yourbank'])){
	$user_arr['bank']       = htmlspecialchars($_POST['yourbank'],ENT_QUOTES);
}
if(isset($_POST['yourbranch'])){
	$user_arr['siten']      = htmlspecialchars($_POST['yourbranch'],ENT_QUOTES);
}
if(isset($_POST['youraccounttype'])){
	$user_arr['kouza_type'] = htmlspecialchars($_POST['youraccounttype'],ENT_QUOTES);
}
if(isset($_POST['youraccountnum'])){
	$user_arr['kouza_name'] = htmlspecialchars($_POST['youraccountnum'],ENT_QUOTES);
}


//var_dump($_FILES['csv_data']);
if ($_FILES['csv_data']["tmp_name"]) {
	  $path = "../csvdata/" . $user_id. "/". "csv";
	if ( mkdir( $path, 0755, true ) ) {
	  echo "ディレクトリ作成成功！！";
	} else {
	  echo "ディレクトリ作成失敗！！";
	}
}
if (is_uploaded_file($_FILES['csv_data']["tmp_name"])) {

  if (move_uploaded_file($_FILES['csv_data']["tmp_name"], $path. "/" . $_FILES['csv_data']["name"])) {
    chmod($path. "/" . $_FILES['csv_data']["name"], 0644);
    //echo $path. "/" . $_FILES['user_img']["name"] . "をアップロードしました。";
	$user_arr['csv_data'] = $path. "/" . $_FILES['csv_data']["name"];
  } else {
    //echo "ファイルをアップロードできません。";
  }
} 


//ユーザーテーブルの情報を更新する
$result = $user_con->user_update($pdo,$user_arr);


//20140212 
//
header("Location:./user.php");
exit();

?>