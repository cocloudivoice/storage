<?php
/***
/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRResult
にある未処理のテキストファイル「〇〇〇_ci.txt」を分析・加工
して、「〇〇〇.txt」にする。
/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRResult
に処理済みのテキストファイル「〇〇〇.txt」があれば飛ばして
次のファイルの処理に移る。
「〇〇〇_ci.txt」のリストを取得して逆順で処理する。
***/

require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
//include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
//include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
//include_once(dirname(__FILE__)."/../pdf/CloudVisionTextDetection.class.php");
//自動仕分け用クラスの読み込み
//include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//テキスト分析加工用クラスの読み込み
include_once(dirname(__FILE__).'/editOCRText.class.php');
//include_once(dirname(__FILE__).'/editOCRTextAuto.class.php');

$user_id = "3308277917";
$path = "";
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
//$invoice_con = new invoice_data_control();
//$pdftoimg_con = new PdfToImg();
//$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
//$auto_journalize_con = new auto_journalize_control();

//OCR済みテキスト分析編集用コントローラーを起動
$edittext_con = new editOCRText();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRimages";
$upload_dir_path = $path;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;
$journalized_code = rand(1000000000,9999999999);
$txt_dir = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRMiddle";
$txt_res_dir = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRResult";
$txt_log_dir = "/var/www/storage.cloudinvoice.co.jp/html/pdf/png/OCRLog";
$txt_file_arr = array();
$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
/*
//$upload_dir_pathのフォルダが存在しない場合に作る。
if (!file_exists($txt_dir)) {
	recurse_chown_chgrp($upload_dir_path, $uid, $gid);
}
*/

$file_list_arr = getFileList($txt_dir);
$k = 0;
for($i = 0;$i < count($file_list_arr); $i++) {
	if (substr($file_list_arr[$i], -6, 6) == "ci.txt") {
		$txt_file_arr[$k] =  basename($file_list_arr[$i]);
		$k++;
	}
}

if ($txt_file_arr) {
	$counter = 0;
	$error_num = 0;
	//画像をテキスト化する工程
	for($m = 0;$m < count ($txt_file_arr) ;$m++) {
		//ファイル名変換処理
		$file_name_top = substr($txt_file_arr[$m], 0, -6);//echo ":top<br>\r\n";
		$file_extension = substr($txt_file_arr[$m], -4, 4);//echo ":ext<br>\r\n";
		$dlp = $file_name_top;//str_replace(array("年","月","日","時","分","秒"),"",$file_name_top);//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
		$filename = $dlp.$file_extension;
		$pdf_name = substr($filename, 0, -4);
		$file_path = $upload_dir_path."/";
		$txt_path = $txt_dir."/";
		$pdf_file = $file_path.$file_name_top.".pdf";
		$path = $file_path;
		
		if(!file_exists($txt_res_dir."/".$file_name_top.".txt")) {
			$edittext_con -> editText($pdo,$pdo_aj,$company_con,$keywords_con,$filename,$upload_dir_path,$txt_res_dir,$txt_dir,$txt_path,$af_id);
		} else {
			rename($txt_dir."/".$file_name_top."ci.txt",$txt_log_dir."/".$file_name_top."ci.txt");
		}
		$counter++;
	}
}

if ($_REQUEST["return_flag"] == 1) {
	header("Location:".$return_url,false);
	exit;
}

/**
* テキストファイルの文字コードを変換し保存する
* @param string $infname  入力ファイル名
* @param string $incode   入力ファイルの文字コード
* @param string $outfname 出力ファイル名
* @param string $outcode  出力ファイルの文字コード
* @param string $nl       出力ファイルの改行コード
* @return string メッセージ
*/
function convertCode($infname, $incode, $outfname, $outcode, $nl) {
    $instr = @file_get_contents($infname);
    if ($instr == FALSE) {
        return "変換失敗：{$infname} が見あたりません．";
    }
    $outstr = mb_convert_encoding($instr, $outcode, $incode);
    $outstr = str_replace(array("\r\n", "\n", "\r"), $nl, $outstr);

    $outfp = fopen($outfname, 'wb');
    if ($outfp == FALSE) {
        return "変換失敗：{$outfname} に書き込むことができません．";
    }
    fwrite($outfp, $outstr);
    fclose($outfp);

    return "変換成功：{$infname} => {$outfname}";
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>