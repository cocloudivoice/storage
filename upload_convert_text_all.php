<?php
session_start();
//必要なクラスを読み込む
require_once(dirname(__FILE__).'/../db_con/dbpath.class.php');
require_once(dirname(__FILE__)."/../db_con/usercontrol.class.php");
include_once(dirname(__FILE__).'/../db_con/companycontrol.class.php');
include_once(dirname(__FILE__).'/../db_con/invoicedatacontrol.class.php');
include_once(dirname(__FILE__)."/../pdf/PdfToImg.class.php");
include_once(dirname(__FILE__)."/../pdf/CloudVisionTextDetection.class.php");
//自動仕分け用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/autojournalizecontrol.class.php');

//企業名照合用クラスの読み込み
include_once(dirname(__FILE__).'/../db_con/searchkeywordscontrol.class.php');
//DBに接続する
$db_con = new dbpath();
$pdo = $db_con->db_connect();
$pdo_aj = $db_con -> change_db_connect();

//各テーブルのコントローラーを起動する
$user_con = new user_control();
$user_arr = $user_con -> user_select_id($pdo,$user_id);
$company_con = new company_control();
$invoice_con = new invoice_data_control();
$pdftoimg_con = new PdfToImg();
$cloudvision_con = new CloudVisionTextDetection();
$keywords_con = new search_keywords_control();
//自動仕分け用のコントローラーを起動する。
$auto_journalize_con = new auto_journalize_control();

//変数の宣言
$ahead_url = $_REQUEST['ahead_url'];
$return_url = $_REQUEST['return_url'];
$path = $_REQUEST['path'];
$file_name = $_FILES['upfile']['name'];
$upload_dir_path = $path;//dirname(__FILE__)."/../pdf/png/".$af_id;
$mail_flag = 0;
$data_count = 0;
$rand_num = rand(10,99);
$rand_words = random(12);
$uid = "apache";
$gid = "dev";
$DataNum = 0;
//header('Content-Type: text/plain; charset=utf-8');
$journalized_code = rand(1000000000,9999999999);
echo $txt_dir = $upload_dir_path;//dirname(__FILE__)."/../pdf/png/".$af_id;;

$top4 = substr($user_id,0,4);
$mid4 = substr($user_id,4,4);
//$path = "../files/" .$top4."/". $mid4."/".$user_id. "/". "csvfiles";

//ディレクトリが無ければ再帰的に作る。
recurse_chown_chgrp($upload_dir_path, $uid, $gid);


//setlocale(LC_ALL, 'ja_JP.UTF-8');
//setlocale(LC_ALL, 'ja_JP.UTF-16');
//setlocale(LC_ALL, 'ja_JP.Shift-JIS');

$_SESSION['file_name'] = $filename = $_FILES['upfile']['name'];
$_SESSION['colms'] = $_REQUEST['colms'];

if (isset($_SESSION['user_id'])) {
	$company_id = $user_id = (int)$_SESSION['user_id'];
}

/*
$file_list_arr = getFileList($upload_dir_path);
//var_dump($file_list_arr);
for($i = 0;$i < count($file_list_arr); $i++) {
	echo $file_list_arr[$i];
	echo "<br>";
}*/

/*
//▼企業データの取得▼
if ($company_id != "") {
	$flag = "company_data";
	$company_self_arr = $company_con -> company_sql_flag($pdo,$flag,$company_id,$words);
	$company_name = $company_self_arr[0]['company_name'];
}

//▲企業データの取得▲
*/

if ($_FILES['upfile']) {
	
	
	//アップロードされたファイルの情報を取得
    //$fileinfo = pathinfo($_FILES["uploadfile"]["name"]);
    //ファイルの拡張子を取得して大文字に変換
    //$fileext = strtoupper($fileinfo["extension"]);
	
	$counter = 0;
	$error_num = 0;
    $file_ary = reArrayFiles($_FILES['upfile']);
	$file_num = count($file_ary);
	//var_dump($file_ary);
	
	//txtだけを先に読み込む。ブサイクだけどとりあえず。
	foreach ($file_ary as $file) {
		//拡張子をそろえる
		$file['name'] = str_replace(array(".JPEG",".JPG",".jpeg"),".jpg",$file['name']);
		$file['name'] = str_replace(array(".PNG"),".png",$file['name']);
		$file['name'] = str_replace(array(".tiff"),".tif",$file['name']);

		if (substr($file['name'], -3, 3) == "txt"|| substr($file['name'], -3, 3) == "pdf"|| substr($file["name"], -3, 3) == "png" || substr($file["name"], -3, 3) == "jpg" || substr($file["name"], -3, 3) == "tif"|| substr($file["name"], -3, 3) == "zip") {
			//echo "テキストの処理\r\n";
			//txtファイルの保存	
			try {
				//ファイル名変換処理
				$file_name_top = substr($file["name"], 0, -4);//echo ":top<br>\r\n";
				$file_extension = substr($file["name"], -4, 4);//echo ":ext<br>\r\n";
				//$dlp = mb_convert_encoding($file_name_top,"SJIS","UTF-8");//md5($file_name_top.$rand_words);//echo ":dlp<br>\r\n";
				$dlp = $file_name_top;
				$filename = $dlp.$file_extension;//echo ":filename<br>\r\n";
				
				if (is_uploaded_file($file["tmp_name"])) {
					if (move_uploaded_file($file["tmp_name"], $txt_dir. "/" . $dlp.$file_extension)) {
					    chmod($txt_dir. "/" . $dlp.$file_extension, 0664);
					} else {
						$error_num++;
					}
				}
				
				if (substr($file['name'], -3, 3) == "zip" && $upload_dir_path != "" && $upload_dir_path != NULL) {
					shell_exec("unzip -j ".$upload_dir_path."/*.zip  -d ".$upload_dir_path." > /dev/null 2>&1");
				}
			} catch (Exception $e) { 
				echo $e -> getMessage;
			}
		}
		$counter++;
	}
}

if ($counter > 0) {
	$counter = $counter;
	$_SESSION['up_info_msg'] = $filename . "等 ".$file_num."件中".$counter."件のファイルをアップロードしました。";
}
if($error_num > 0) {
	$_SESSION['up_info_msg'] .= $error_num."件のファイルをアップロードできませんでした。";
}
//echo $counter;

header("Location:".$return_url,true);

echo "<html>";
echo "<script>location.href='http://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all'</script>";
echo "</html>";
echo "<input type='button' value='移動' onclick='location.href=\'./imgs_to_text_upload_all\''>";


//header("Location:https://storage.cloudinvoice.co.jp/invoice/imgs_to_text_upload_all",true);

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function recurse_chown_chgrp($mypath,$uid, $gid) {
	if (mkdir( $mypath, 0775, true ) ) {
		chmod( $mypath, 0775);
		chown( $mypath, $uid);
	    chgrp( $mypath, $gid);
	  //echo "ディレクトリ作成成功！！";
	} else {
	  //echo "ディレクトリ作成失敗！！";
	}
} 

function getFileList($dir) {
    $files = glob(rtrim($dir, '/') . '/*');
    $list = array();
    foreach ($files as $file) {
        if (is_file($file)) {
            $list[] = $file;
        }
        if (is_dir($file)) {
            $list = array_merge($list, getFileList($file));
        }
    }
 
    return $list;
}

function random($length = 12) {
    return base_convert(rand(pow(36, $length - 1), pow(36, $length) - 1), 10, 36);
    //return base_convert(mt_rand( $r1= rand(),$r1+rand()), 10, 36);
}


?>