<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
    <title>webmail</title>
</head>
<body>

<?php

    //-------------------------------
    //既読リスト取得
    //-------------------------------
    if(file_exists($arl_file)){
        $alreadyReadList = unserialize(file_get_contents($arl_file));
    }else{
        $alreadyReadList = array();
    }


	require_once dirname(__FILE__) . '/aaMailListInc.php';

    //-------------------------------
    //接続＆ログイン
    //-------------------------------
	require_once dirname(__FILE__) . '/aaMailListConnect.php';
	
	mb_language("ja");
	mb_internal_encoding("UTF-8");

    //-------------------------------
    //受信メール件数取得
    //-------------------------------
    fputs($fp, "STAT\r\n");
    $line = fgets($fp, 512);
    print $line . "<br>";    //STAT結果表示

    //STAT結果を変数に展開
    list($status, $cnt, $size) = explode(' ', $line);

    //受信メール件数チェック
    if(intVal($cnt) == 0){
        print "受信メール無し<br><br>";
    }else{
        print "受信メールあり<br><br>";
    }

    //テスト用に件数制限入れておきます（100件を超える場合は100件まで）
    $cnt = ($cnt > 100 ? 100 : $cnt);

    //-------------------------------
    //受信メール一覧表示
    //-------------------------------
    for($i = 1; $i <= $cnt; $i++){
        //メッセージヘッダ取得
        //「TOP」が使えない環境の人は「RETR」で代用してください
        //fputs($fp, "RETR $i\r\n");
        fputs($fp, "TOP $i 0\r\n");

        do{
            $line = fgets($fp, 512);

            //件名を表示する
            if(preg_match("/^Subject/", $line)){
                $subject = mb_decode_mimeheader($line);
                $subject = preg_replace("/^Subject:/", "", $subject);
                print '<a href="aaMailListDetail?no=' . $i . '" target="blank">';
                print $i . ":" . $subject;
                print '</a>';
                //print $i . ":" . mb_convert_encoding($subject, $enc, "auto") . "<br>";
                //print $i . ":" . mb_convert_encoding($subject, $enc, "auto");
            }
            
            //既読・未読を表示する
            if(preg_match("/^Message-Id/", $line)){
                //Message-Idを取得する
                preg_match('/<(.*)>/', $line, $msgIdAry);
                $messageId = $msgIdAry[1];

                //既読リストと比較
                if(!array_key_exists($messageId, $alreadyReadList)){
                    print "　未読";
                }
            }
        
           
        }while (!preg_match("/^\.\r\n/", $line));
        print "<br>";
    }

    //切断
    require_once dirname(__FILE__) . '/aaMailListDisconnect.php';
?>

</body>
</html>